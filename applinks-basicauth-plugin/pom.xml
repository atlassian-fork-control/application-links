<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.applinks</groupId>
        <artifactId>applinks-parent</artifactId>
        <version>7.1.5-SNAPSHOT</version>
    </parent>

    <artifactId>applinks-basicauth-plugin</artifactId>
    <packaging>atlassian-plugin</packaging>

    <name>Applinks - Plugin - Basic Authentication</name>
    <description>[PUBLIC] Plugin that provides Basic Authentication for Application Links</description>

    <properties>
        <!-- override these to support multiple instances without clashing -->
        <applinks.build.directory>${basedir}/target</applinks.build.directory>
    </properties>

    <dependencies>

        <!-- Applinks dependencies -->
        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-host</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-spi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-common</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- Atlassian dependencies -->
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.templaterenderer</groupId>
            <artifactId>atlassian-template-renderer-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-core</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-spring</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- Javax dependencies -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- 3rd party dependencies -->
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- Test dependencies - Atlassian -->
        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-plugin</artifactId>
            <version>${project.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-module</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Test dependencies - 3rd party -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <directory>${applinks.build.directory}</directory>
        <plugins>
            <!-- Runner plugins need to be explicitly defined here to pick up the parent configuration, because AMPS -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>amps-maven-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <extractDependencies>true</extractDependencies>
                    <output>${applinks.build.directory}/output.log</output>
                    <instructions>
                        <Bundle-SymbolicName>com.atlassian.applinks.applinks-basicauth-plugin</Bundle-SymbolicName>
                        <Import-Package>
                            com.atlassian.applinks.common.*,
                            com.atlassian.applinks.core.*,
                            com.atlassian.applinks.internal.*,
                            com.atlassian.applinks.ui.*,
                            com.atlassian.applinks.api.*,
                            com.atlassian.applinks.spi.*,
                            *
                        </Import-Package>
                        <_noee>true</_noee>
                    </instructions>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>api-compat</id>
            <activation>
                <property>
                    <name>api-compat</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>com.atlassian.maven.plugins</groupId>
                        <artifactId>api-compat-maven-plugin</artifactId>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
