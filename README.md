# Application Links

## Description

Application Links (AppLinks) is a bundled plugin that allows you to link your Jira, Confluence, FishEye, Crucible and Bamboo applications. Linking two applications allows you to share information and access one application's functions from within the other. For example, if you link Jira and Confluence, you can view Jira issues on a Confluence page via the Jira Issues macro. You can even link your individual projects, spaces and repositories with each other, across the different applications. Note that the Application Links plugin is bundled and shipped with the Atlassian applications.

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Branches

- master - branch for product versions which are compatible with Java11 and Platform v5
; and use AUI between 8.0.0 and the latest 8.x
; based on applinks-6.1.x branch with merged applinks-7.0.x branch, with AUI8+ support and BBS react rework.
- applinks-6.1.x - branch for product versions which are compatible with Java11 and Platform v5
; and use AUI between 5.9.13 and the latest 7.x
; with some BBS react rework that we didn't want to have in Jira 8.5 ER
; based on applinks-6.0.x branch without AUI8+ support
- applinks-6.0.x - branch for product versions which are compatible with Java11 and Platform v5
; and use AUI between 5.9.13 and the latest 7.x
- applinks-5.4.x - stable branch for product versions which are *not* compatible with Java11 and Platform v5
; and use AUI between 5.9.13 and the latest 7.x

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/APL)

## External User?

### Issues

Please raise any issues you find with this module in [Jira](https://ecosystem.atlassian.net/browse/APL)

### Documentation

[Application Links Documentation](https://developer.atlassian.com/display/DOCS/Application+Links)

## Running the plugin for running integration tests from in inteliJ
To run refapp with the applinks plugin installed

    mvn clean install &&
    cd applinks-tests && 
    mvn amps:run -Dproduct=refapp1

and in separate window

    cd applinks-tests && 
    mvn amps:run -Dproduct=refapp2

There are other configurations in the pom.xml for running other Atlassian products
