package com.atlassian.applinks.spi.link;

import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.assertEquals;

public class ApplicationLinkDetailsTest {
    @Test
    public void createDetailsGivenEmptyPathInRpcUrl() {
        ApplicationLinkDetails details = ApplicationLinkDetails.builder()
                .name("Test")
                .rpcUrl(URI.create("test.com:8990"))
                .displayUrl(URI.create("http://test.com:8990"))
                .build();

        assertEquals("Test", details.getName());
        assertEquals("test.com:8990", details.getRpcUrl().toString());
    }

    @Test
    public void createDetailsGivenEmptyPathInDisplayUrl() {
        ApplicationLinkDetails details = ApplicationLinkDetails.builder()
                .name("Test")
                .rpcUrl(URI.create("http://test.com:8990"))
                .displayUrl(URI.create("test.com:8990"))
                .build();

        assertEquals("Test", details.getName());
        assertEquals("test.com:8990", details.getDisplayUrl().toString());
    }

    @Test
    public void createDetailsGivenRpcUrlWithTrailingSlash() {
        ApplicationLinkDetails details = ApplicationLinkDetails.builder()
                .name("Test")
                .rpcUrl(URI.create("http://test.com:8990/some/"))
                .displayUrl(URI.create("http://test.com:8990"))
                .build();

        assertEquals("Test", details.getName());
        assertEquals("http://test.com:8990/some", details.getRpcUrl().toString());
    }

    @Test
    public void createDetailsGivenDisplayUrlWithTrailingSlash() {
        ApplicationLinkDetails details = ApplicationLinkDetails.builder()
                .name("Test")
                .rpcUrl(URI.create("http://test.com:8990"))
                .displayUrl(URI.create("http://test.com:8990/some/"))
                .build();

        assertEquals("Test", details.getName());
        assertEquals("http://test.com:8990/some", details.getDisplayUrl().toString());
    }
}
