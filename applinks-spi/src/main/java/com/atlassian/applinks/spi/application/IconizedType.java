package com.atlassian.applinks.spi.application;

import com.atlassian.annotations.PublicSpi;

import java.net.URI;
import javax.annotation.Nullable;

/**
 * Represents application or entity type that provides a high quality icon URI that will eventually supersedes
 * {@code ApplicationType#getIconUrl()}. Application and entity types should optionally implement this SPI to indicate
 * their ability to provide access to high quality icons representing those types.
 *
 * @see com.atlassian.applinks.api.ApplicationType#getIconUrl()
 * @since 4.3
 */
@PublicSpi
public interface IconizedType {
    /**
     * Provides URI to retrieve an icon for this application type
     * <p>
     * Note that there are no guarantees for the resolution or size of the icon, other than it is a square
     * (i.e. height is equal to width). It is responsibility of the clients that render icons provided via this
     * URI to scale the icons to the desired size (e.g. using CSS for HTML clients). In other words clients should
     * not rely on the size of the served icon as it may change in the
     * future.
     * </p>
     *
     * @return URI of a high resolution icon for this application type or {@code null} if no icon is available
     * @since 4.3
     */
    @Nullable
    URI getIconUri();
}
