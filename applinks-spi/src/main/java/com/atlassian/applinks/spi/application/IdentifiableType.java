package com.atlassian.applinks.spi.application;

import javax.annotation.Nonnull;

/**
 * @since 3.0
 */
public interface IdentifiableType {
    /**
     * @return the {@link TypeId} identifier for this type
     */
    @Nonnull
    TypeId getId();
}
