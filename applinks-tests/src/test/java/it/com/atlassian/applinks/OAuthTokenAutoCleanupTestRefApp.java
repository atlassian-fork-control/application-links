package it.com.atlassian.applinks;

import com.atlassian.applinks.fisheye.deploy.CheckConsumerTokenServletPage;
import com.atlassian.applinks.fisheye.deploy.ClearAllServiceProviderOAuthTokensServletPage;
import com.atlassian.applinks.fisheye.deploy.ThreeLeggedOAuthRequestTestServletPage;
import com.atlassian.webdriver.applinks.page.AuthIFrameTestPage;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.CreateAppLinkWith3LO2LORule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import static junit.framework.Assert.assertTrue;

@Category(RefappTest.class)
public class OAuthTokenAutoCleanupTestRefApp extends V2AbstractApplinksTest {
    @Rule
    public RuleChain ruleChain = CreateAppLinkWith3LO2LORule.forProducts(PRODUCT, PRODUCT2);

    @Before
    public void setUp() throws Exception {
        login(PRODUCT, PRODUCT2);
//        ApplicationLinkCreationTestHelper.createTwoWayLink(PRODUCT, PRODUCT2);
    }

    @Test
    public void verifyThatBadConsumerOAuthTokenGetsCleanUpAutomatically() throws Exception {
        // first, do the dance to create consumer and service provider token.
        AuthIFrameTestPage authIFrameTestPage = PRODUCT.visit(AuthIFrameTestPage.class) //
                .authenticate() //
                // wait for the oauth popup window
                .confirmHandlingWebLoginIfRequired(ADMIN_USERNAME, ADMIN_PASSWORD);

        // verify confirmation text displayed
        assertTrue(authIFrameTestPage.hasConfirmedAuthorization("applinks.refapp", ADMIN_USERNAME));
        // verify that the OAuth confirmation message has been displayed
        assertTrue(authIFrameTestPage.hasOAuthConfirmationMessage());
        assertTrue(authIFrameTestPage.hasOAuthAccessTokens());

        // NOTE: following checks use "contains()" instead of "equals" because sometimes webdriver wraps the output with Html tags.

        // OAuth request should be good.
        assertTrue(PRODUCT.visit(ThreeLeggedOAuthRequestTestServletPage.class).getContent().contains(ADMIN_USERNAME));
        // consumer token must be there.
        assertTrue(PRODUCT.visit(CheckConsumerTokenServletPage.class).getContent().contains("true"));

        // now delete all tokens on the remote side, then all the local tokens will be invalid.
        assertTrue(PRODUCT2.visit(ClearAllServiceProviderOAuthTokensServletPage.class).getContent().contains("done"));

        // OAuth request from now should look bad.
        assertTrue(PRODUCT.visit(ThreeLeggedOAuthRequestTestServletPage.class).getContent().contains("no oauth token"));

        // consumer token must have been deleted.
        assertTrue(PRODUCT.visit(CheckConsumerTokenServletPage.class).getContent().contains("false"));
    }
}
