package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.backdoor.SystemPropertiesBackdoor;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.test.rest.OAuthRest40CompatibilityFilter.PROP_OAUTH_40_COMPATIBILITY;
import static com.atlassian.applinks.internal.test.rest.OAuthRest40CompatibilityFilter.PROP_OAUTH_40_COMPATIBILITY_INVOCATIONS;
import static com.atlassian.applinks.internal.test.rest.OAuthRest40CompatibilityFilter.PROP_OAUTH_40_COMPATIBILITY_RETURN_OK;

/**
 * Rule to enable OAuth 4.x compatibility in selected tests.
 * <p>
 * See {@code OAuthRest40CompatibilityFilter} for more details.
 * </p>
 *
 * @since 5.0
 */
public class OAuthRest40CompatibilityRule extends TestWatcher {
    private final SystemPropertiesBackdoor backdoor;
    private final boolean enabledByDefault;

    public OAuthRest40CompatibilityRule(@Nonnull TestedInstance instance) {
        this(instance, true);
    }

    public OAuthRest40CompatibilityRule(@Nonnull TestedInstance instance, boolean enabledByDefault) {
        backdoor = new SystemPropertiesBackdoor(instance);
        this.enabledByDefault = enabledByDefault;
    }

    public void enable() {
        backdoor.setProperty(PROP_OAUTH_40_COMPATIBILITY, Boolean.TRUE.toString());
    }

    public void disable() {
        backdoor.clearProperty(PROP_OAUTH_40_COMPATIBILITY);
    }

    public void enableReturnOk() {
        backdoor.setProperty(PROP_OAUTH_40_COMPATIBILITY_RETURN_OK, Boolean.TRUE.toString());
    }

    public void disableReturnOk() {
        backdoor.clearProperty(PROP_OAUTH_40_COMPATIBILITY_RETURN_OK);
    }

    public int getInvocationCount() {
        String count = backdoor.getProperty(PROP_OAUTH_40_COMPATIBILITY_INVOCATIONS);
        return count != null ? Integer.parseInt(count) : 0;
    }

    public void cleanUp() {
        disableReturnOk();
        disable();
        resetInvocations();
    }

    public void resetInvocations() {
        backdoor.clearProperty(PROP_OAUTH_40_COMPATIBILITY_INVOCATIONS);
    }

    @Override
    protected void starting(Description description) {
        if (enabledByDefault) {
            enable();
        }
    }

    @Override
    protected void finished(Description description) {
        cleanUp();
    }
}
