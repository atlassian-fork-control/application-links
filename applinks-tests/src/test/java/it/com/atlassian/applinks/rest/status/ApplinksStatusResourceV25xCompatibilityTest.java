package it.com.atlassian.applinks.rest.status;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import org.junit.ClassRule;
import org.junit.experimental.categories.Category;

import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;

/**
 * Tests Status resource running against an instance without Status API, running Applinks 5.x. This requires making
 * authenticated requests and may result in additional error conditions.
 *
 * @since 5.0
 */
@Category(RefappTest.class)
public class ApplinksStatusResourceV25xCompatibilityTest extends AbstractStatusResourceV2CompatibilityTest {
    // Refapp2 setup rules to act as V2 5.x:
    //  - disable status API by default
    @ClassRule
    public static final ApplinksCapabilitiesRule REFAPP2_CAPABILITIES = new ApplinksCapabilitiesRule(REFAPP2)
            .withDisabledCapabilities(ApplinksCapabilities.STATUS_API);

}
