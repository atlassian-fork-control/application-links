package it.com.atlassian.applinks.testing.categories;

/**
 * JUnit Category to classify tests requiring only a single product.
 *
 * @since 4.3.0
 */
public interface SingleProductTest {
}
