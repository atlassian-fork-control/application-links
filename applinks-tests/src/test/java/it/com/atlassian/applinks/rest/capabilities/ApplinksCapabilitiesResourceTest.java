package it.com.atlassian.applinks.rest.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.test.rest.capabilities.ApplinksCapabilitiesRestTester;
import com.atlassian.applinks.test.rest.capabilities.ApplinksCapilitiesRequest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.authentication.TestAuthentication.anonymous;
import static com.atlassian.applinks.test.rest.specification.ApplinksCapabilitiesExpectations.expectAllCapabilities;
import static com.atlassian.applinks.test.rest.specification.ApplinksCapabilitiesExpectations.expectCapability;

@Category(RefappTest.class)
public class ApplinksCapabilitiesResourceTest {
    private static final ProductInstances TESTED_PRODUCT = ProductInstances.REFAPP1;

    @Rule
    public final RuleChain restTestRules = ApplinksRuleChain.forRestTests(TESTED_PRODUCT);
    @Rule
    public final ApplinksCapabilitiesRule applinksCapabilitiesRule = new ApplinksCapabilitiesRule(TESTED_PRODUCT);

    private final ApplinksCapabilitiesRestTester capabilitiesRestTester =
            new ApplinksCapabilitiesRestTester(TESTED_PRODUCT);

    @Test
    public void getAllCapabilitiesAnonymously() {
        capabilitiesRestTester.get(new ApplinksCapilitiesRequest.Builder()
                .authentication(anonymous())
                .specification(expectAllCapabilities())
                .build());
    }

    @Test
    public void getSpecificCapabilityAnonymously() {
        capabilitiesRestTester.get(new ApplinksCapilitiesRequest.Builder(ApplinksCapabilities.STATUS_API)
                .authentication(anonymous())
                .specification(expectCapability(ApplinksCapabilities.STATUS_API))
                .build());
    }

    @Test
    public void getDisabledCapability() {
        applinksCapabilitiesRule.disable(ApplinksCapabilities.STATUS_API);

        capabilitiesRestTester.get(new ApplinksCapilitiesRequest.Builder(ApplinksCapabilities.STATUS_API)
                .authentication(anonymous())
                .expectStatus(Status.NOT_FOUND)
                .build());
    }

    @Test
    public void getNonExistingCapability() {
        capabilitiesRestTester.get(new ApplinksCapilitiesRequest.Builder("nosuchcapability")
                .authentication(anonymous())
                .expectStatus(Status.NOT_FOUND)
                .build());
    }
}
