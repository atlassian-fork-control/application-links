package it.com.atlassian.applinks.smoke.analytics;

import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.webdriver.applinks.component.v3.StatusDetailsNotification;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v3.V3EditApplinkPage;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.smoke.AbstractDefaultSmokeTest;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.IgnoredProducts;
import it.com.atlassian.applinks.testing.rules.AnalyticsLogRule;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.pageobjects.LoginClient.loginAsSysadminAndGoTo;
import static com.atlassian.applinks.test.data.applink.config.ApplinkConfigurators.configureSide;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static it.com.atlassian.applinks.testing.product.TestedInstances.fromProductInstance;
import static it.com.atlassian.applinks.testing.specification.AnalyticsEventsExpectations.eventWithProperty;
import static it.com.atlassian.applinks.testing.specification.AnalyticsEventsExpectations.expectRetainedEvent;

/**
 * Smoke test for help links analytics events on relevant Applinks pages.
 *
 * @since 5.2
 */
@IgnoredProducts(value = SupportedProducts.REFAPP, reason = "Refapp does not support Analytics")
@Category(SmokeTest.class)
public class HelpLinksAnalyticsTest extends AbstractDefaultSmokeTest {
    @Rule
    public final ApplinksFeaturesRule applinksFeaturesRule;
    @Rule
    public final AnalyticsLogRule analyticsLogRule;

    private final ApplinksBackdoor applinksBackdoor;

    public HelpLinksAnalyticsTest(@Nonnull GenericTestedProduct mainProduct,
                                  @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        this.applinksFeaturesRule = new ApplinksFeaturesRule(fromProductInstance(mainInstance));
        this.analyticsLogRule = new AnalyticsLogRule(mainInstance);
        this.applinksBackdoor = new ApplinksBackdoor(mainInstance);
    }

    @Test
    public void testV2DocumentationPageLinkAnalytics() {

        applinksFeaturesRule.disable(ApplinksFeatures.V3_UI);
        ListApplicationLinkPage v2ApplinksPage = loginAsSysadminAndGoTo(mainProduct, ListApplicationLinkPage.class);
        v2ApplinksPage.clearDialogs();

        v2ApplinksPage.clickHelpLink("applinks.docs.administration.guide");

        analyticsLogRule.waitUntilLogsMatch(
                expectRetainedEvent("applinks.view.documentation",
                        eventWithProperty("linkKey", "applinks.docs.administration.guide"))
        );
    }

    @Test
    public void testV3TroubleshootingLinkAnalytics() {
        // create applink with OAuth config mismatch (OAuth enabled on the "from" side only)
        TestApplink applink = applinksBackdoor.create(backgroundInstance);
        configureSide(applink.from(), enableDefaultOAuth());

        V3ListApplicationLinksPage v3ApplinksPage = loginAsSysadminAndGoTo(mainProduct, V3ListApplicationLinksPage.class);
        v3ApplinksPage.clearDialogs();

        v3ApplinksPage.getApplinkRow(applink.from()).openDiagnosticsDialog().openTroubleshootingPage();

        expectAuthLevelMismatchHelpLinkAnalytics();
    }

    @Test
    public void testV3ApplinkEditTroubleshootingLinkAnalytics() {
        // create applink with OAuth config mismatch (OAuth enabled on the "from" side only)
        TestApplink applink = applinksBackdoor.create(backgroundInstance);
        configureSide(applink.from(), enableDefaultOAuth());

        V3EditApplinkPage v3EditPage = loginAsSysadminAndGoTo(mainProduct, V3EditApplinkPage.class, applink.from());
        v3EditPage.clearDialogs();

        // help link on status details notification
        StatusDetailsNotification statusDetails = v3EditPage.getStatusDetails();
        waitUntilTrue(statusDetails.isPresent());
        statusDetails.openTroubleshootingPage();
        expectAuthLevelMismatchHelpLinkAnalytics();

        // help link in incoming remote auth section
        analyticsLogRule.clearLogs();
        waitUntilTrue(v3EditPage.getIncomingRemoteAuthStatus().hasTroubleshootingLink());
        v3EditPage.getIncomingRemoteAuthStatus().openTroubleshootingPage();
        expectAuthLevelMismatchHelpLinkAnalytics();

        // help link in outgoing remote auth section
        analyticsLogRule.clearLogs();
        waitUntilTrue(v3EditPage.getOutgoingRemoteAuthStatus().hasTroubleshootingLink());
        v3EditPage.getOutgoingRemoteAuthStatus().openTroubleshootingPage();
        expectAuthLevelMismatchHelpLinkAnalytics();
    }

    private void expectAuthLevelMismatchHelpLinkAnalytics() {
        analyticsLogRule.waitUntilLogsMatch(
                expectRetainedEvent("applinks.view.documentation",
                        eventWithProperty("linkKey", "applinks.docs.diagnostics.troubleshoot.authlevelmismatch"))
        );
    }
}
