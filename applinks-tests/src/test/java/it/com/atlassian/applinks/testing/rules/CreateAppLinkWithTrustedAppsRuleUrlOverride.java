package it.com.atlassian.applinks.testing.rules;

import com.atlassian.pageobjects.TestedProduct;
import it.com.atlassian.applinks.testing.backdoor.AppLinksBackdoor;
import org.junit.rules.RuleChain;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * Creates new Trusted Apps links between the specified instances allowing the RPC/display URL to be overridden after the links have been created.
 *
 * @since 4.0
 */
public class CreateAppLinkWithTrustedAppsRuleUrlOverride extends AbstractProductLinkingRule {
    /**
     * Apply the rule to each product
     */
    public static RuleChain forProducts(String urlOverride, TestedProduct<?>... products) {
        RuleChain chain = RuleChain.emptyRuleChain();

        for (final TestedProduct<?> product : products) {
            // apply the rule to each localProduct, linking to the other products.
            chain = chain.around(new CreateAppLinkWithTrustedAppsRuleUrlOverride(urlOverride, product, getOtherProducts(product, products)));
        }
        return chain;
    }


    private final AppLinksBackdoor backdoor;
    private final String urlOverride;

    @Inject
    public CreateAppLinkWithTrustedAppsRuleUrlOverride(String urlOverride, @Nonnull TestedProduct<?> localProduct, final Iterable<TestedProduct<?>> remoteProducts) {
        super(localProduct, remoteProducts);
        this.urlOverride = urlOverride;
        this.backdoor = AppLinksBackdoor.forProduct(localProduct.getProductInstance());
    }

    @Override
    protected void createLink(final TestedProduct<?> localProduct, final TestedProduct<?> remoteProduct) {
        backdoor.createTrustedAppsLinkUrlOverride(remoteProduct.getProductInstance(), urlOverride);
    }
}


