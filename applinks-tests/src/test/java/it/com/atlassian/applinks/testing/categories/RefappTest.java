package it.com.atlassian.applinks.testing.categories;

import it.com.atlassian.applinks.testing.product.ProductInstances;

/**
 * JUnit Category to classify tests against Refapp. Those test should execute against
 * {@link ProductInstances#REFAPP1} and {@link ProductInstances#REFAPP2}.
 *
 * @since 4.3
 */
public interface RefappTest {
}
