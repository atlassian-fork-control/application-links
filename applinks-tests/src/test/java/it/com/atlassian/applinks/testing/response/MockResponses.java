package it.com.atlassian.applinks.testing.response;

import com.atlassian.applinks.core.rest.ManifestResource;
import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.status.ApplinkStatusResource;
import com.atlassian.applinks.test.response.MockResponseDefinition;
import com.atlassian.applinks.test.rest.model.RestRequestPredicate;
import net.oauth.OAuthMessage;

import javax.annotation.Nonnull;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;

import static com.atlassian.applinks.internal.rest.RestUrlBuilder.APPLINKS_REST_MODULE;
import static com.atlassian.applinks.test.rest.model.RestRequestPredicate.restRequestTo;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 * Common request predicates and response definitions for setting up tests simulating communication problems between
 * linked applications.
 *
 * @since 5.0
 */
public final class MockResponses {
    private static final String ANY_COMPONENT = ".+";

    private MockResponses() {
        // do not instantiate
    }

    @Nonnull
    public static RestRequestPredicate.Builder manifestResourceRequest() {
        return restRequestTo(APPLINKS_REST_MODULE.add(ANY_COMPONENT).add(ManifestResource.CONTEXT));
    }

    @Nonnull
    public static RestRequestPredicate.Builder oauthStatusRequest() {
        return restRequestTo(APPLINKS_REST_MODULE
                .add(ANY_COMPONENT)
                .add(ApplinkStatusResource.STATUS_PATH)
                .add(ANY_COMPONENT)
                .add(ApplinkStatusResource.OAUTH_PATH));
    }

    @Nonnull
    public static RestRequestPredicate.Builder v25xAuthenticationRequest() {
        return restRequestTo(RestUrl.forPath("applinks-oauth")
                .add(ANY_COMPONENT)
                .add("applicationlink")
                .add(ANY_COMPONENT)
                .add("authentication"));
    }

    @Nonnull
    public static RestRequestPredicate.Builder v24xAuthenticationRequest() {
        return restRequestTo(APPLINKS_REST_MODULE
                .add(ANY_COMPONENT)
                .add("applicationlink")
                .add(ANY_COMPONENT)
                .add("authentication"));
    }

    @Nonnull
    public static MockResponseDefinition unexpectedStatusResponse(@Nonnull Status status) {
        checkNotNull(status, "status");
        return new MockResponseDefinition.Builder(status).build();
    }

    @Nonnull
    public static MockResponseDefinition unexpectedStatusResponse(int statusCode) {
        return new MockResponseDefinition.Builder(statusCode).build();
    }

    @Nonnull
    public static MockResponseDefinition textPlainOkResponse(@Nonnull String contents) {
        checkNotNull(contents, "contents");
        return new MockResponseDefinition.Builder(Status.OK)
                .body(contents, MediaType.TEXT_PLAIN)
                .build();
    }

    @Nonnull
    public static MockResponseDefinition oAuthProblemResponse(@Nonnull String oauthProblem) {
        checkNotNull(oauthProblem, "oauthProblem");
        OAuthMessage message = new OAuthMessage(null, null, null);
        message.addParameter(ApplinksOAuth.OAUTH_PROBLEM, oauthProblem);

        try {
            return new MockResponseDefinition.Builder(UNAUTHORIZED)
                    .header(ApplinksOAuth.WWW_AUTHENTICATE, message.getAuthorizationHeader(null))
                    .build();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
