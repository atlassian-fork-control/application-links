package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.api.application.refapp.RefAppCharlieEntityType;
import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.webdriver.applinks.component.RelocateApplicationLinkDialog;
import com.atlassian.webdriver.applinks.component.v2.ConfigureUrlDialog;
import com.atlassian.webdriver.applinks.page.adg.entitylinks.ConfigureEntityLinksPage;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

import static com.atlassian.applinks.test.data.applink.config.ApplinkConfigurators.configure;
import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@Category(RefappTest.class)
public class XssProtectionTestRefApp extends V2AbstractApplinksTest {
    @Before
    public void setUp() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);
        OAuthApplinksClient.verifyConfiguredAppLinksTableIsEmpty(PRODUCT);
    }

    @Test
    public void linkPresentToAddFirstApplicationLink() {
        String addFirstApplicationLinkText = PRODUCT.visit(ListApplicationLinkPage.class) //
                .getAddFirstApplicationLinkText();
        assertEquals("You have no application links right now. To create an application link begin by entering the URL of application you wish to link to.", addFirstApplicationLinkText);
    }

    // APL-529
    @Test
    public void addApplicationLinkUrlErrorEscapesMessageForEmptyUrl() {
        String testUrl = " ";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog dialog = page.getConfigureUrlDialog();

        OAuthApplinksClient.verifyConfigureUrlDialogLayout(dialog, testUrl.trim(), null);

        assertThat(dialog.getWarning(), startsWith("Please enter a valid URL."));
    }

    // APL-529
    @Test
    public void addApplicationLinkUrlErrorEscapesMessage() {
        String testUrl = "https://";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog dialog = page.getConfigureUrlDialog();

        OAuthApplinksClient.verifyConfigureUrlDialogLayout(dialog, testUrl, null);

        assertThat(dialog.getWarning(), startsWith("Please enter a valid URL."));
    }

    // APL-531
    @Test
    public void relocationInfoBoxesEscapeTheirApplicationTitle() throws Exception {
        String name = "Test <i>XSS</i>";
        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, "http://localhost:1", name);
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        assertThat(listApplicationLinkPage.getPageWarning(),
                startsWith("Application 'Test <i>XSS</i>' seems to be offline."));
        assertThat(listApplicationLinkPage.getRelocateLinkText(name),
                startsWith("Click here to Relocate"));
    }

    // APL-424
    @Test
    public void messageContentsAreShownForRelocationDialogError() throws Exception {
        String name = "Test <i>XSS</i>";
        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, "http://localhost:1", name);

        RelocateApplicationLinkDialog relocateApplicationLinkDialog = PRODUCT.visit(ListApplicationLinkPage.class) //
                .relocate(name) //
                .setRelocateUrl("https://") //
                .nextExpectingError();

        assertThat(relocateApplicationLinkDialog.getRelocateErrorText(),
                startsWith("Failed to convert https:// to URI"));
    }

    // APL-737
    @Test
    public void projectNameIsEscapedInAddProjectLink() throws Exception {
        String charlieTestKey = "CHARLIE_TEST_KEY";

        TestApplink applink = new ApplinksBackdoor(INSTANCE1).create(INSTANCE2);
        configure(applink, enableTrustedApps());

        //Selenium will fail when an unexpected dialog pops up
        OAuthApplinksClient.createCharlie(PRODUCT2, "X", "<script>alert('ping')</script> Test 1", ADMIN_PASSWORD);
        OAuthApplinksClient.createCharlie(PRODUCT2, "Y", "<script>alert('ping')</script> Test 2", ADMIN_PASSWORD);
        OAuthApplinksClient.createCharlie(PRODUCT, charlieTestKey, "Charlie", ADMIN_PASSWORD);

        String name = PRODUCT.visit(ConfigureEntityLinksPage.class, RefAppCharlieEntityType.class, charlieTestKey)
                .clickAddLink()
                .selectApplication("RefApp")
                .selectProject("Test")
                .create()
                .getRows().get(0).getName();

        assertTrue(name.contains("<script>alert('ping')</script>"));
    }

    // APL-529
    @Test
    public void configureDisplayUrlErrorEscapesMessage() {
        String testUrl = "http://<i>localhost</i>";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog dialog = page.getConfigureUrlDialog();

        OAuthApplinksClient.verifyConfigureUrlDialogLayout(dialog, testUrl, null);
    }
}
