package it.com.atlassian.applinks.v1;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.applinks.externalpage.CharlieManagementPage;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import com.atlassian.webdriver.refapp.RefappTestedProduct;

import static it.com.atlassian.applinks.V2AbstractApplinksTest.ADMIN_PASSWORD;
import static it.com.atlassian.applinks.V2AbstractApplinksTest.PRODUCT;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class Creators {
    public static void createCharlie(final String key, final String name) {
        createCharlie(PRODUCT, key, name, ADMIN_PASSWORD);
    }

    public static void createCharlie(RefappTestedProduct product, final String key, final String name, final String webSudoPassword) {
        PageElement createdCharlieLink = product.visit(CharlieManagementPage.class) //
                .handleWebSudoIfRequired(webSudoPassword) //
                .add(key, name) //
                .getLandingPageLink(key);
        assertNotNull(createdCharlieLink);
    }

    public static void assertNoApplicationLinksAreConfigured() {
        int numApplinks = PRODUCT.visit(ListApplicationLinkPage.class) //
                .inApplicationLinks().allRows().size();
        assertEquals(0, numApplinks);
    }
}
