package it.com.atlassian.applinks.core.rest;

import com.atlassian.applinks.internal.common.rest.model.oauth.RestConsumer;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.oauth.OAuthApplicationLinkRestTester;
import com.atlassian.applinks.test.rest.oauth.OAuthConsumerRequest;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.json.JSONException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static com.atlassian.applinks.test.data.applink.config.OAuthConsumerApplinkConfigurator.addOAuthConsumer;
import static com.atlassian.applinks.test.data.applink.config.OAuthConsumerApplinkConfigurator.addOAuthConsumerAutoConfigure;
import static com.atlassian.applinks.test.mock.MockOAuthConsumerFactory.TEST_CONSUMER_KEY;
import static com.atlassian.applinks.test.mock.MockOAuthConsumerFactory.TEST_CONSUMER_NAME;
import static com.atlassian.applinks.test.mock.MockOAuthConsumerFactory.createRsaConsumer;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.expectBody;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

@Category(RefappTest.class)
public class OAuthApplicationLinkResourceTest {
    private static final TestAuthentication DEFAULT_AUTH_ADMIN = REFAPP_ADMIN_BETTY;

    private static final String URL_GENERIC_APP = "http://www.google.com";
    private static final String STATUS_MESSAGE = "status.message";

    private static final String CONSUMER_NAME = "consumers.consumers.name";
    private static final String CONSUMER_KEY = "consumers.consumers.key";
    private static final String CONSUMER_2LO_ALLOWED = "consumers.consumers.twoLOAllowed";
    private static final String CONSUMER_2LO_IMPERSONATION_ALLOWED = "consumers.consumers.twoLOImpersonationAllowed";

    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);

    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    private OAuthApplicationLinkRestTester oauthApplinkRestTesterApp1 = new OAuthApplicationLinkRestTester(REFAPP1);
    private OAuthApplicationLinkRestTester oauthApplinkRestTesterApp2 = new OAuthApplicationLinkRestTester(REFAPP2);
    private ApplinksBackdoor backdoorRefApp1 = new ApplinksBackdoor(REFAPP1);

    @Test
    public void putConsumerWithValidDataShouldPass()
            throws URISyntaxException, ResponseException, JSONException, InvalidKeySpecException, NoSuchAlgorithmException {
        applink.configureFrom(enableDefaultOAuth());
        applink.configureTo(enableDefaultOAuth(), addOAuthConsumer(new RestConsumer(createRsaConsumer(true, true, false))));

        oauthApplinkRestTesterApp2.get(new OAuthConsumerRequest.Builder(applink.to().id())
                .authentication(DEFAULT_AUTH_ADMIN)
                .expectStatus(OK)
                .specifications(
                        expectBody(CONSUMER_KEY, equalTo(TEST_CONSUMER_KEY)),
                        expectBody(CONSUMER_NAME, equalTo(TEST_CONSUMER_NAME)),
                        expectBody(CONSUMER_2LO_ALLOWED, equalTo("true")),
                        expectBody(CONSUMER_2LO_IMPERSONATION_ALLOWED, equalTo("false"))
                )
                .build());
    }

    @Test
    public void putConsumerWithAutoConfigureShouldPass()
            throws URISyntaxException, ResponseException, JSONException, InvalidKeySpecException, NoSuchAlgorithmException {
        applink.configureFrom(enableDefaultOAuth());
        applink.configureTo(enableDefaultOAuth(), addOAuthConsumerAutoConfigure());

        oauthApplinkRestTesterApp2.get(new OAuthConsumerRequest.Builder(applink.to().id())
                .authentication(DEFAULT_AUTH_ADMIN)
                .expectStatus(OK)
                .specifications(
                        expectBody(CONSUMER_NAME, equalTo("RefImpl"))
                )
                .build());
    }

    @Test
    public void getConsumerShouldFailFor3rdPartyWithoutRegisteredProvider()
            throws URISyntaxException, ResponseException, JSONException {
        TestApplink.Side genericAppLink = backdoorRefApp1.createGeneric(URL_GENERIC_APP);

        oauthApplinkRestTesterApp1.get(new OAuthConsumerRequest.Builder(genericAppLink.id())
                .authentication(DEFAULT_AUTH_ADMIN)
                .expectStatus(NOT_FOUND)
                .specification(expectBody(STATUS_MESSAGE, equalTo("A Consumer cannot be created for a 3rd Party Link without registering a Provider with the same Consumer Key first.")))
                .build());
    }

    @Test
    public void putConsumer3rdPartyOutgoingShouldPass() throws URISyntaxException, ResponseException, JSONException {
        TestApplink.Side genericAppLink = backdoorRefApp1.createGeneric(URL_GENERIC_APP);

        oauthApplinkRestTesterApp1.putConsumer(new OAuthConsumerRequest.Builder(genericAppLink.id())
                .authentication(DEFAULT_AUTH_ADMIN)
                .consumer(new RestConsumer(createRsaConsumer(true, false, false), false, null)) // outgoing is mandatory for generic applink consumer
                .expectStatus(CREATED)
                .build());
    }

    @Test
    public void putConsumer3rdPartyShouldPass() throws URISyntaxException, ResponseException, JSONException {
        TestApplink.Side genericAppLink = backdoorRefApp1.createGeneric(URL_GENERIC_APP);

        oauthApplinkRestTesterApp1.putConsumer(new OAuthConsumerRequest.Builder(genericAppLink.id())
                .authentication(DEFAULT_AUTH_ADMIN)
                .consumer(new RestConsumer(createRsaConsumer(true, false, false), true, "secret")) // outgoing is mandatory for generic applink consumer
                .expectStatus(CREATED)
                .build());
    }

    @Test
    public void putConsumer3rdPartyShouldFailForMissingSharedSecret()
            throws URISyntaxException, ResponseException, JSONException {
        TestApplink.Side genericAppLink = backdoorRefApp1.createGeneric(URL_GENERIC_APP);

        oauthApplinkRestTesterApp1.putConsumer(new OAuthConsumerRequest.Builder(genericAppLink.id())
                .authentication(DEFAULT_AUTH_ADMIN)
                .consumer(new RestConsumer(createRsaConsumer(true, false, false), true, null)) // outgoing is mandatory for generic applink consumer
                .expectStatus(BAD_REQUEST)
                .specifications(
                        expectBody(STATUS_MESSAGE, equalTo("Shared secret is required."))
                )
                .build());
    }

    @Test
    public void putConsumerShouldFailForMissingRequiredFields()
            throws URISyntaxException, ResponseException, JSONException {
        applink.configure(enableDefaultOAuth());

        oauthApplinkRestTesterApp1.put(new OAuthConsumerRequest.Builder(applink.from().id())
                .authentication(DEFAULT_AUTH_ADMIN)
                .consumer(new RestConsumer(ImmutableMap.<String, Object>of()))
                .expectStatus(BAD_REQUEST)
                .specifications(
                        expectBody(STATUS_MESSAGE, contains("Consumer key is required.", "Service provider name is required.", "A Public Key is required."))
                )
                .build());
    }

    @Test
    public void putConsumerShouldFailForInvalidPublicKey()
            throws URISyntaxException, ResponseException, JSONException {
        applink.configure(enableDefaultOAuth());

        RestConsumer restConsumer = new RestConsumer(createRsaConsumer(true, false, false));
        restConsumer.put(RestConsumer.PUBLIC_KEY, "abc"); // overrides publicKey with random value

        oauthApplinkRestTesterApp1.putConsumer(new OAuthConsumerRequest.Builder(applink.from().id())
                .authentication(DEFAULT_AUTH_ADMIN)
                .consumer(restConsumer)
                .expectStatus(BAD_REQUEST)
                .specifications(
                        expectBody(STATUS_MESSAGE, equalTo("The Public Key is invalid."))
                )
                .build());
    }
}