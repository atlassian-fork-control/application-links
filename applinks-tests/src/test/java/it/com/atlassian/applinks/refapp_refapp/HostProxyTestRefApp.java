package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.fisheye.deploy.HostProxyPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertTrue;

@Category(RefappTest.class)
public class HostProxyTestRefApp extends V2AbstractApplinksTest {
    @Before
    public void setUp() {
        login();
    }

    @Test
    public void testProxy() throws Exception {
        HostProxyPage page = PRODUCT.visit(HostProxyPage.class);
        assertTrue(page.isSuccessful());
    }

}
