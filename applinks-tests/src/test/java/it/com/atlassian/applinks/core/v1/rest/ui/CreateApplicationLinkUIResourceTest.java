package it.com.atlassian.applinks.core.v1.rest.ui;

import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.plugins.rest.common.security.jersey.XsrfResourceFilter;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.core.net.HttpClientResponse;
import it.com.atlassian.applinks.core.RestTestHelper;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.CleanupAppLinksRule;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import java.io.IOException;
import java.util.UUID;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Integration tests for Applink creation v1.0 REST endpoints.
 *
 * @since 4.3
 */
@Category(RefappTest.class)
public class CreateApplicationLinkUIResourceTest {
    protected static final ProductInstance PRODUCT = ProductInstances.REFAPP1;
    protected static final ProductInstance PRODUCT2 = ProductInstances.REFAPP2;

    private static final String REST_URL = "/rest";
    private static final String CAPABILITIES_REST_ENDPOINT = REST_URL + "/applinks/1.0/applicationlinkForm";

    private static final String PRODUCT_BASE_URL = PRODUCT.getBaseUrl();
    private static final String PRODUCT2_BASE_URL = PRODUCT2.getBaseUrl();
    private static final String PRODUCT_CAPABILITIES_REST_ENDPOINT = PRODUCT_BASE_URL + CAPABILITIES_REST_ENDPOINT;

    public static final String MANIFEST_REST_ENDPOINT = PRODUCT_CAPABILITIES_REST_ENDPOINT + "/manifest.json?url=http%3A%2F%2Flocalhost";

    @Rule
    public RuleChain ruleChain = CleanupAppLinksRule.forProducts(PRODUCT, PRODUCT2);

    @Test
    public void createAppLink() throws IOException, JSONException, ResponseException {

        String url = PRODUCT_CAPABILITIES_REST_ENDPOINT + "/createAppLink";

        boolean isPrimary = true;
        boolean trustEachOther = true;
        boolean sharedUserBase = true;
        UUID applicationId = UUID.randomUUID();
        JSONObject input = RestTestHelper.getTestCreateAppLinkSubmission(PRODUCT2_BASE_URL, RestTestHelper.getDefaultUser(), isPrimary, trustEachOther, sharedUserBase, applicationId, true);

        final HttpClientResponse manifestResponse = RestTestHelper.postRestResponse(RestTestHelper.getDefaultUser(), url, input.toString());

        assertThat(manifestResponse.getStatusCode(), is(200));

        JSONObject manifest = new JSONObject(manifestResponse.getResponseBodyAsString());
        assertThat(manifest.get("autoConfigurationSuccessful").toString(), is(Boolean.TRUE.toString()));

        assertTrue(manifest.has("applicationLink"));
        JSONObject applicationLink = (JSONObject) manifest.get("applicationLink");
        assertThat(applicationLink.get("typeId").toString(), is("refapp"));
        assertThat(applicationLink.get("rpcUrl").toString(), is(PRODUCT2_BASE_URL));

        //TODO more checks
    }

    @Test
    public void createAppLinkNoReciprocal() throws IOException, JSONException, ResponseException {
        String url = PRODUCT_CAPABILITIES_REST_ENDPOINT + "/createAppLink";

        boolean isPrimary = true;
        boolean trustEachOther = true;
        boolean sharedUserBase = true;
        UUID applicationId = UUID.randomUUID();
        JSONObject input = RestTestHelper.getTestCreateAppLinkSubmission(PRODUCT2_BASE_URL, RestTestHelper.getDefaultUser(), isPrimary, trustEachOther, sharedUserBase, applicationId, false);

        final HttpClientResponse manifestResponse = RestTestHelper.postRestResponse(RestTestHelper.getDefaultUser(), url, input.toString());

        assertThat(manifestResponse.getStatusCode(), is(200));

        JSONObject manifest = new JSONObject(manifestResponse.getResponseBodyAsString());
        assertThat(manifest.get("autoConfigurationSuccessful").toString(), is(Boolean.TRUE.toString()));

        assertTrue(manifest.has("applicationLink"));
        JSONObject applicationLink = (JSONObject) manifest.get("applicationLink");
        assertThat(applicationLink.get("typeId").toString(), is("refapp"));
        assertThat(applicationLink.get("rpcUrl").toString(), is(PRODUCT2_BASE_URL));

        //TODO more checks
    }

    @Test
    public void getManifestEndpointShouldRejectRequestsWithoutXsrfHeader() {
        final HttpClientResponse response = RestTestHelper.getRestResponse(RestTestHelper.getDefaultUser(), MANIFEST_REST_ENDPOINT);

        assertThat(response.getStatusCode(), is(403));
    }

    @Test
    public void getManifestEndpointShouldAcceptRequestsWithValidXsrfHeader() {
        final HttpGet request = new HttpGet(MANIFEST_REST_ENDPOINT);
        request.setHeader(XsrfResourceFilter.TOKEN_HEADER, XsrfResourceFilter.NO_CHECK);

        final HttpClientResponse response = RestTestHelper.callRestEndPoint(RestTestHelper.getDefaultUser(), request);

        assertThat(response.getStatusCode(), is(200));
    }
}
