package it.com.atlassian.applinks.rest.applink;

import com.atlassian.applinks.application.refapp.RefAppApplicationTypeImpl;
import com.atlassian.applinks.test.rest.applink.ApplinkV3Request;
import com.atlassian.applinks.test.rest.applink.ApplinksV3Request;
import com.atlassian.applinks.test.rest.applink.ApplinksV3RestTester;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import javax.ws.rs.core.Response;

import static com.atlassian.applinks.test.authentication.TestAuthentication.anonymous;
import static com.atlassian.applinks.test.data.applink.config.DisplayUrlApplinkConfigurator.setDisplayUrl;
import static com.atlassian.applinks.test.mock.TestApplinkIds.createRandomId;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withDisplayUrl;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withId;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withName;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withRpcUrl;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withType;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.restEntity;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.expectBody;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_USER_BARNEY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.SYSADMIN;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.hamcrest.Matchers.hasItem;

@Category(RefappTest.class)
public class ApplinksV3ResourceTest {
    private static final String TEST_DISPLAY_URL = "http://test-display-url.com";

    @Rule
    public final RuleChain restTestRules = ApplinksRuleChain.forRestTests(REFAPP1);
    @Rule
    public final TestApplinkRule testApplink = new TestApplinkRule.Builder(REFAPP1, REFAPP2)
            .name("V3 Test Applink")
            .configureFrom(setDisplayUrl(TEST_DISPLAY_URL))
            .build();

    private final ApplinksV3RestTester applinksRestTester = new ApplinksV3RestTester(REFAPP1);

    private final ApplinksBackdoor backdoor = new ApplinksBackdoor(REFAPP1);

    @Test
    public void getAllAsAnonymousUnauthorized() {
        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(anonymous())
                .expectStatus(Response.Status.UNAUTHORIZED)
                .build());
    }

    @Test
    public void getAllAsUserForbidden() {
        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_USER_BARNEY)
                .expectStatus(Response.Status.FORBIDDEN)
                .build());
    }

    @Test
    public void getAllAsAdminOk() {
        String randomId1 = backdoor.createRandomGeneric().id();
        String randomId2 = backdoor.createRandomGeneric().id();

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        withId(testApplink.from().id()),
                        withId(randomId1),
                        withId(randomId2)
                )))
                .specification(expectBody(hasItem(restEntity(
                        withId(testApplink.from().id()),
                        withType(RefAppApplicationTypeImpl.TYPE_ID),
                        withName("V3 Test Applink"),
                        withRpcUrl(REFAPP2.getBaseUrl()),
                        withDisplayUrl(TEST_DISPLAY_URL)
                ))))
                .build());
    }

    @Test
    public void getAllAsSysadminOk() {
        String randomId1 = backdoor.createRandomGeneric().id();
        String randomId2 = backdoor.createRandomGeneric().id();

        applinksRestTester.getAll(new ApplinksV3Request.Builder()
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectBody(containsInAnyOrder(
                        withId(testApplink.from().id()),
                        withId(randomId1),
                        withId(randomId2)
                )))
                .build());
    }

    @Test
    public void getSingleApplinkAsAnonymousUnauthorized() {
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(anonymous())
                .expectStatus(Response.Status.UNAUTHORIZED)
                .build());
    }

    @Test
    public void getSingleApplinkAsUserForbidden() {
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_USER_BARNEY)
                .expectStatus(Response.Status.FORBIDDEN)
                .build());
    }

    @Test
    public void getSingleApplinkNonExisting() {
        applinksRestTester.get(new ApplinkV3Request.Builder(createRandomId())
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.NOT_FOUND)
                .build());
    }

    @Test
    public void getSingleApplinkAsAdminOk() {
        // create extra applinks, make sure we get the right one
        backdoor.createRandomGeneric().id();
        backdoor.createRandomGeneric().id();

        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withType(RefAppApplicationTypeImpl.TYPE_ID),
                        withName("V3 Test Applink"),
                        withRpcUrl(REFAPP2.getBaseUrl()),
                        withDisplayUrl(TEST_DISPLAY_URL)
                )))
                .build());
    }

    @Test
    public void getSingleApplinkAsSysadminOk() {
        // create extra applinks, make sure we get the right one
        backdoor.createRandomGeneric().id();
        backdoor.createRandomGeneric().id();

        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withType(RefAppApplicationTypeImpl.TYPE_ID),
                        withName("V3 Test Applink"),
                        withRpcUrl(REFAPP2.getBaseUrl()),
                        withDisplayUrl(TEST_DISPLAY_URL)
                )))
                .build());
    }

    // remove after we drop Java7
    @SuppressWarnings("unchecked")
    static Matcher<?> containsInAnyOrder(Matcher<?>... matchers) {
        return (Matcher<?>) Matchers.containsInAnyOrder((Matcher[]) matchers);
    }
}
