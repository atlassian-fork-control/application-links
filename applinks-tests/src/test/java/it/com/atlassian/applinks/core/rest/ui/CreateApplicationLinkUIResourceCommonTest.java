package it.com.atlassian.applinks.core.rest.ui;

import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.plugins.rest.common.security.jersey.XsrfResourceFilter;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.core.net.HttpClientResponse;
import it.com.atlassian.applinks.core.RestTestHelper;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.CleanupAppLinksRule;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Integration tests for Applink creation REST endpoints.
 *
 * Tests functionality common to all API versions.
 *
 * @since v3.11
 */
@RunWith(Parameterized.class)
@Category(RefappTest.class)
public class CreateApplicationLinkUIResourceCommonTest {
    protected static final ProductInstance PRODUCT = ProductInstances.REFAPP1;
    protected static final ProductInstance PRODUCT2 = ProductInstances.REFAPP2;

    private static final String REST_URL = "/rest";
    private static final String CAPABILITIES_REST_ENDPOINT_FORMAT = REST_URL + "/applinks/%s/applicationlinkForm";
    private static final String PRODUCT_CAPABILITIES_REST_ENDPOINT_FORMAT = "%s" + CAPABILITIES_REST_ENDPOINT_FORMAT;

    private String productCapabilitiesRestEndPoint;
    private String product2BaseUrl;

    @Rule
    public RuleChain ruleChain = CleanupAppLinksRule.forProducts(PRODUCT, PRODUCT2);

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{"1.0"}, {"2.0"}, {"latest"}};
        return Arrays.asList(data);
    }

    public CreateApplicationLinkUIResourceCommonTest(String apiVersion) {
        productCapabilitiesRestEndPoint = String.format(PRODUCT_CAPABILITIES_REST_ENDPOINT_FORMAT, PRODUCT.getBaseUrl(), apiVersion);
        product2BaseUrl = PRODUCT2.getBaseUrl();
    }

    @Test
    public void manifest() throws JSONException, ResponseException {
        final HttpGet manifestGet = createGetRequestWithXsrfHeader(productCapabilitiesRestEndPoint + "/manifest?url=" + product2BaseUrl);
        final HttpClientResponse manifestResponse = RestTestHelper.callRestEndPoint(RestTestHelper.getDefaultUser(), manifestGet);

        assertThat(manifestResponse.getStatusCode(), is(200));
        JSONObject manifest = new JSONObject(manifestResponse.getResponseBodyAsString());
        assertTrue(manifest.has("id"));
        assertThat(manifest.get("typeId").toString(), is("refapp"));
        assertThat(manifest.get("buildNumber").toString(), is("123"));
        assertThat((JSONArray) manifest.get("inboundAuthenticationTypes"), any(JSONArray.class));
        assertThat((JSONArray) manifest.get("outboundAuthenticationTypes"), any(JSONArray.class));
    }

    @Test
    public void manifestForSelfUrl() throws IOException, JSONException, ResponseException {
        final HttpGet manifestGet = createGetRequestWithXsrfHeader(productCapabilitiesRestEndPoint + "/manifest?url=http://localhost:5990/refapp");
        final HttpClientResponse manifestResponse = RestTestHelper.callRestEndPoint(RestTestHelper.getDefaultUser(), manifestGet);
        JSONObject manifest = new JSONObject(manifestResponse.getResponseBodyAsString());
        assertFalse(manifest.has("id"));
    }

    @Test
    public void manifestForNonAtlassianUrlIsEmpty() throws IOException, JSONException, ResponseException {
        final HttpGet manifestGet = createGetRequestWithXsrfHeader(productCapabilitiesRestEndPoint + "/manifest?url=http://www.google.com");
        final HttpClientResponse manifestResponse = RestTestHelper.callRestEndPoint(RestTestHelper.getDefaultUser(), manifestGet);
        JSONObject manifest = new JSONObject(manifestResponse.getResponseBodyAsString());
        assertFalse(manifest.has("id"));
    }

    @Test
    public void createAppLinkNoReciprocal() throws IOException, JSONException, ResponseException {

        String url = productCapabilitiesRestEndPoint + "/createAppLink";

        boolean isPrimary = true;
        boolean trustEachOther = true;
        boolean sharedUserBase = true;
        UUID applicationId = UUID.randomUUID();
        JSONObject input = RestTestHelper.getTestCreateAppLinkSubmission(product2BaseUrl, RestTestHelper.getDefaultUser(), isPrimary, trustEachOther, sharedUserBase, applicationId, false);

        final HttpClientResponse response = RestTestHelper.postRestResponse(RestTestHelper.getDefaultUser(), url, input.toString());

        assertThat(response.getStatusCode(), is(200));

        JSONObject appLinksResponse = new JSONObject(response.getResponseBodyAsString());
        assertTrue(appLinksResponse.has("autoConfigurationSuccessful"));

        assertTrue(appLinksResponse.has("applicationLink"));
        JSONObject applicationLink = (JSONObject) appLinksResponse.get("applicationLink");
        assertThat(applicationLink.get("typeId").toString(), is("refapp"));
        assertThat(applicationLink.get("rpcUrl").toString(), is(product2BaseUrl));

        //TODO more checks
    }

    private HttpGet createGetRequestWithXsrfHeader(String url) {
        final HttpGet manifestGet = new HttpGet(url);
        manifestGet.setHeader(XsrfResourceFilter.TOKEN_HEADER, XsrfResourceFilter.NO_CHECK);
        return manifestGet;
    }
}
