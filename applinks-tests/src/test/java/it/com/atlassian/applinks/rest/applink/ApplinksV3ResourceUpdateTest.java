package it.com.atlassian.applinks.rest.applink;

import com.atlassian.applinks.internal.common.rest.model.applink.RestApplicationLink;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.applink.ApplinkV3Request;
import com.atlassian.applinks.test.rest.applink.ApplinksV3RestTester;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import javax.ws.rs.core.Response;

import java.net.URISyntaxException;

import static com.atlassian.applinks.test.authentication.TestAuthentication.anonymous;
import static com.atlassian.applinks.test.mock.TestApplinkIds.createRandomId;
import static com.atlassian.applinks.test.rest.data.applink.SystemLinkConfigurator.setSystemLink;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withDisplayUrl;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withId;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withName;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withPrimary;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationLinkMatchers.withRpcUrl;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.restError;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.singleError;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.withContext;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.withContextThat;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.withErrors;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.withSummary;
import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.withSummaryThat;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.restEntity;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.expectBody;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_USER_BARNEY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.SYSADMIN;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static java.lang.String.format;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.nullValue;

@Category(RefappTest.class)
public class ApplinksV3ResourceUpdateTest {
    private static final String TEST_NAME = "Updated applink";
    private static final String TEST_RPC_URL = "http://test-rpc-url.com";
    private static final String TEST_DISPLAY_URL = "http://test-display-url.com";

    @Rule
    public final RuleChain restTestRules = ApplinksRuleChain.forRestTests(REFAPP1);
    @Rule
    public final TestApplinkRule testApplink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    private final ApplinksV3RestTester applinksRestTester = new ApplinksV3RestTester(REFAPP1);

    private final ApplinksBackdoor backdoor = new ApplinksBackdoor(REFAPP1);

    @Test
    public void putAsAnonymousUnauthorized() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(anonymous())
                .applicationLink(createApplink(TEST_NAME))
                .expectStatus(Response.Status.UNAUTHORIZED)
                .build());
    }

    @Test
    public void putAsUserForbidden() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_USER_BARNEY)
                .applicationLink(createApplink(TEST_NAME))
                .expectStatus(Response.Status.FORBIDDEN)
                .build());
    }

    @Test
    public void putAsAdminValidEntity() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, TEST_RPC_URL, TEST_DISPLAY_URL))
                .expectStatus(Response.Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withName(TEST_NAME),
                        withRpcUrl(TEST_RPC_URL),
                        withDisplayUrl(TEST_DISPLAY_URL)
                )))
                .build());

        // verify that the update had been persisted
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withName(TEST_NAME),
                        withRpcUrl(TEST_RPC_URL),
                        withDisplayUrl(TEST_DISPLAY_URL)
                )))
                .build());
    }

    @Test
    public void putAsSysadminValidEntity() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(SYSADMIN)
                .applicationLink(createApplink(TEST_NAME, TEST_RPC_URL, TEST_DISPLAY_URL))
                .expectStatus(Response.Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withName(TEST_NAME),
                        withRpcUrl(TEST_RPC_URL),
                        withDisplayUrl(TEST_DISPLAY_URL)
                )))
                .build());

        // verify that the update had been persisted
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withName(TEST_NAME),
                        withRpcUrl(TEST_RPC_URL),
                        withDisplayUrl(TEST_DISPLAY_URL)
                )))
                .build());
    }

    @Test
    public void putAsAdminMakePrimary() {
        String url = testApplink.from().product().getBaseUrl();
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, url, url, true))
                .expectStatus(Response.Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withName(TEST_NAME),
                        withPrimary(true)
                )))
                .build());

        // verify that the update had been persisted
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.OK)
                .specification(expectBody(restEntity(
                        withId(testApplink.from().id()),
                        withName(TEST_NAME),
                        withPrimary(true)
                )))
                .build());
    }

    @Test
    public void putGivenSystemLink() {
        testApplink.configureFrom(setSystemLink());

        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(SYSADMIN)
                .applicationLink(createApplink(TEST_NAME, TEST_RPC_URL, TEST_DISPLAY_URL))
                .expectStatus(Response.Status.CONFLICT)
                .specification(expectBody(withErrors(restEntity(
                        withContextThat(nullValue()),
                        withSummary("System application links cannot be updated.")
                ))))
                .build());
    }

    @Test
    public void putGivenMissingName() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(null))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(withErrors(restEntity(
                        withContext("name"),
                        withSummaryThat(containsString("either missing or has invalid value"))
                ))))
                .build());
    }

    @Test
    public void putGivenMissingRpcUrl() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, null, TEST_DISPLAY_URL))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(withErrors(restEntity(
                        withContext("rpcUrl"),
                        withSummaryThat(containsString("either missing or has invalid value"))
                ))))
                .build());
    }

    @Test
    public void putGivenMissingDisplayUrl() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, TEST_RPC_URL, null))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(withErrors(restEntity(
                        withContext("displayUrl"),
                        withSummaryThat(containsString("either missing or has invalid value"))
                ))))
                .build());
    }

    @Test
    public void putGivenNonAbsoluteRpcUrl() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, "test.com", TEST_DISPLAY_URL))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(singleError("rpcUrl", "The URL must be absolute.")))
                .build());
    }

    @Test
    public void putGivenNonAbsoluteDisplayUrl() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, TEST_RPC_URL, "test.com"))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(singleError("displayUrl", "The URL must be absolute.")))
                .build());
    }

    @Test
    public void putGivenRpcUrlWithWrongScheme() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, "nosuchthing://test.com", TEST_DISPLAY_URL))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(singleError("rpcUrl", "Malformed URL.")))
                .build());
    }

    @Test
    public void putGivenDisplayUrlWithWrongScheme() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, TEST_RPC_URL, "nosuchthing://test.com"))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(singleError("displayUrl", "Malformed URL.")))
                .build());
    }

    @Test
    public void putGivenRpcUrlWithEmptySchemeAndPath() {
        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, "test:7990", TEST_DISPLAY_URL))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(singleError("rpcUrl", "Malformed URL.")))
                .build());
    }

    @Test
    public void putGivenDuplicateName() {
        TestApplink.Side generic = backdoor.createRandomGeneric();
        backdoor.setName(generic, TEST_NAME);

        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, TEST_RPC_URL, TEST_DISPLAY_URL))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(singleError("name",
                        format("There is already an application link with name '%s'.", TEST_NAME))))
                .build());
    }

    @Test
    public void putGivenDuplicateRpcUrl() {
        backdoor.createGeneric(TEST_RPC_URL);

        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, TEST_RPC_URL, TEST_DISPLAY_URL))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(singleError("rpcUrl",
                        "There is already an application link with the provided Application URL.")))
                .build());
    }

    @Test
    public void putGivenDuplicateDisplayUrl() {
        backdoor.createGeneric(TEST_DISPLAY_URL);

        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, TEST_RPC_URL, TEST_DISPLAY_URL))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(singleError("displayUrl",
                        "There is already an application link with the provided Display URL.")))
                .build());
    }

    @Test
    public void putWithMultipleErrors() {
        backdoor.createGeneric(TEST_DISPLAY_URL);

        applinksRestTester.put(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .applicationLink(createApplink(TEST_NAME, "test.com", TEST_DISPLAY_URL))
                .expectStatus(Response.Status.BAD_REQUEST)
                .specification(expectBody(withErrors(
                        restError("rpcUrl", "The URL must be absolute."),
                        restError("displayUrl", "There is already an application link with the provided Display URL.")
                )))
                .build());
    }

    @Test
    public void deleteAsAnonymousUnauthorized() {
        applinksRestTester.delete(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(anonymous())
                .expectStatus(Response.Status.UNAUTHORIZED)
                .build());
    }

    @Test
    public void deleteAsUserForbidden() {
        applinksRestTester.delete(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_USER_BARNEY)
                .expectStatus(Response.Status.FORBIDDEN)
                .build());
    }

    @Test
    public void deleteAsAdmin() {
        applinksRestTester.delete(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.NO_CONTENT)
                .build());

        // verify that the DELETE has been persisted
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.NOT_FOUND)
                .build());
    }

    @Test
    public void deleteAsSysadminValidEntity() {
        applinksRestTester.delete(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.NO_CONTENT)
                .build());

        // verify that the DELETE has been persisted
        applinksRestTester.get(new ApplinkV3Request.Builder(testApplink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.NOT_FOUND)
                .build());
    }

    @Test
    public void deleteNonExistentApplink() {
        applinksRestTester.delete(new ApplinkV3Request.Builder(createRandomId())
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.NOT_FOUND)
                .build());
    }

    @Test
    public void deleteApplinkInvalidId() {
        applinksRestTester.delete(new ApplinkV3Request.Builder("not-a-uuid")
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.BAD_REQUEST)
                .build());
    }

    private RestApplicationLink createApplink(String name, String rpcUrl, String displayUrl, boolean primary) {
        try {
              return new RestApplicationLink(name, rpcUrl, displayUrl, primary);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private RestApplicationLink createApplink(String name, String rpcUrl, String displayUrl) {
        return createApplink(name, rpcUrl, displayUrl, false);
    }

    private RestApplicationLink createApplink(String name) {
        String url = testApplink.from().product().getBaseUrl();
        return createApplink(name, url, url);
    }
}
