package it.com.atlassian.applinks.testing.backdoor;

import com.atlassian.pageobjects.ProductInstance;
import it.com.atlassian.applinks.ApplicationTestHelper;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import javax.inject.Inject;

/**
 * Backdoor to easily create, modify and delete applinks.
 *
 * @deprecated use {@link com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor} instead
 */
@Deprecated
public final class AppLinksBackdoor {
    private static final UsernamePasswordCredentials DEFAULT_SYSADMIN_CREDENTIALS = new UsernamePasswordCredentials("admin", "admin");

    public static AppLinksBackdoor forProduct(ProductInstance product) {
        return new AppLinksBackdoor(product);
    }

    private final String baseUrl;
    private final HttpClient httpClient = new DefaultHttpClient();

    public AppLinksBackdoor(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Inject
    public AppLinksBackdoor(ProductInstance product) {
        this(product.getBaseUrl());
    }

    /**
     * Cleans up all 'known' test resources.
     */
    public void cleanUp() {
        final HttpGet request = new HttpGet(baseUrl + "/rest/applinks-tests/latest/cleanup");
        request.addHeader(BasicScheme.authenticate(DEFAULT_SYSADMIN_CREDENTIALS, "UTF-8", false));
        execute(request);
    }

    /**
     * Create a reciprocal link with Trusted Apps auth.
     */
    public void createReciprocalTrustedAppsLink(ProductInstance remoteProduct) {
        StringBuffer entityBuffer = new StringBuffer();
        entityBuffer.append("{");
        entityBuffer.append("\"username\":\"");
        entityBuffer.append(ApplicationTestHelper.SYSADMIN_USERNAME);
        entityBuffer.append("\",");
        entityBuffer.append("\"password\":\"");
        entityBuffer.append(ApplicationTestHelper.SYSADMIN_PASSWORD);
        entityBuffer.append("\",");
        entityBuffer.append("\"baseUrl\":\"");
        entityBuffer.append(remoteProduct.getBaseUrl());
        entityBuffer.append("\",");
        entityBuffer.append("\"name\":\"");
        entityBuffer.append(remoteProduct.getInstanceId());
        entityBuffer.append("\",");
        // reciprocal
        entityBuffer.append("\"twoWay\":\"true\",");
        entityBuffer.append("\"sharedUserBase\":\"true\",");
        entityBuffer.append("\"trusted\":\"true\"");
        entityBuffer.append("}");

        try {
            this.createTrustedAppsLink(new StringEntity(entityBuffer.toString()));
        } catch (Exception e) {
            // TODO switch to RuntimeException, but at the moment the backdoor REST resource/RefApp is throwing errors even though it appears to be successful.
            e.printStackTrace();
        }
    }

    /**
     * Create a link with Trusted Apps auth.
     */
    public void createTrustedAppsLink(ProductInstance remoteProduct) {
        StringBuffer entityBuffer = new StringBuffer();
        entityBuffer.append("{");
        entityBuffer.append("\"username\":\"");
        entityBuffer.append(ApplicationTestHelper.SYSADMIN_USERNAME);
        entityBuffer.append("\",");
        entityBuffer.append("\"password\":\"");
        entityBuffer.append(ApplicationTestHelper.SYSADMIN_PASSWORD);
        entityBuffer.append("\",");
        entityBuffer.append("\"baseUrl\":\"");
        entityBuffer.append(remoteProduct.getBaseUrl());
        entityBuffer.append("\",");
        entityBuffer.append("\"name\":\"");
        entityBuffer.append(remoteProduct.getInstanceId());
        entityBuffer.append("\",");
        entityBuffer.append("\"twoWay\":\"false\",");
        entityBuffer.append("\"sharedUserBase\":\"true\",");
        entityBuffer.append("\"trusted\":\"true\"");
        entityBuffer.append("}");

        try {
            this.createTrustedAppsLink(new StringEntity(entityBuffer.toString()));
        } catch (Exception e) {
            // TODO switch to RuntimeException, but at the moment the backdoor REST resource/RefApp is throwing errors even though it appears to be successful.
            e.printStackTrace();
        }
    }

    /**
     * Create a reciprocal link with Trusted Apps auth but overrides the RPC and display URL of the remote product.
     */
    public void createTrustedAppsLinkUrlOverride(ProductInstance remoteProduct, String overrideUrl) {
        StringBuffer entityBuffer = new StringBuffer();
        entityBuffer.append("{");
        entityBuffer.append("\"username\":\"");
        entityBuffer.append(ApplicationTestHelper.SYSADMIN_USERNAME);
        entityBuffer.append("\",");
        entityBuffer.append("\"password\":\"");
        entityBuffer.append(ApplicationTestHelper.SYSADMIN_PASSWORD);
        entityBuffer.append("\",");
        entityBuffer.append("\"baseUrl\":\"");
        entityBuffer.append(remoteProduct.getBaseUrl());
        entityBuffer.append("\",");
        entityBuffer.append("\"urlOverride\":\"");
        entityBuffer.append(overrideUrl);
        entityBuffer.append("\",");
        entityBuffer.append("\"name\":\"");
        entityBuffer.append(remoteProduct.getInstanceId());
        entityBuffer.append("\",");
        entityBuffer.append("\"twoWay\":\"false\",");
        entityBuffer.append("\"sharedUserBase\":\"true\",");
        entityBuffer.append("\"trusted\":\"true\"");
        entityBuffer.append("}");

        try {
            this.createTrustedAppsLink(new StringEntity(entityBuffer.toString()));
        } catch (Exception e) {
            // TODO switch to RuntimeException, but at the moment the backdoor REST resource/RefApp is throwing errors even though it appears to be successful.
            e.printStackTrace();
        }
    }

    /**
     * Create a link with Trusted Apps auth using the provided config.
     */
    private void createTrustedAppsLink(StringEntity config) {
        try {
            HttpPost httpPost = new HttpPost(this.baseUrl + "/rest/applinks-tests/latest/applinks");
            httpPost.setEntity(config);
            httpPost.setHeader("content-type", "application/json");
            execute(httpPost);
        } catch (Exception e) {
            // TODO switch to RuntimeException, but at the moment the backdoor REST resource/RefApp is throwing errors even though it appears to be successful.
            e.printStackTrace();
        }
    }

    /**
     * Just deletes all application links in the target app.
     */
    public void deleteAllAppLinks() {
        final HttpDelete request = new HttpDelete(baseUrl + "/rest/applinks-tests/latest/applinks");
        request.addHeader(BasicScheme.authenticate(DEFAULT_SYSADMIN_CREDENTIALS, "UTF-8", false));
        execute(request);
    }

    private void execute(HttpUriRequest request) {
        try {
            httpClient.execute(request, new BasicResponseHandler());
        } catch (IOException e) {
            throw new RuntimeException("Unable to complete request " + request.getURI().toASCIIString(), e);
        }
    }
}
