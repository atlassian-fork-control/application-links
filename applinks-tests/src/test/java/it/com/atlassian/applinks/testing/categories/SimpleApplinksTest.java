package it.com.atlassian.applinks.testing.categories;

/**
 * JUnit Category to classify tests as working on the basic stripped down Applinks plugin.
 *
 * @since 5.0.0
 */
public interface SimpleApplinksTest {
}
