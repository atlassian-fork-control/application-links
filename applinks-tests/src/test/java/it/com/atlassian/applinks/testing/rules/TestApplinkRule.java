package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.data.applink.config.ApplinkConfigurators;
import com.atlassian.applinks.test.data.applink.config.TestApplinkConfigurator;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.backdoor.BackdoorCreateApplinkRequest;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.util.Arrays.asList;

/**
 * Creates a an applink on test start and configures it using provided configurators.
 * <p>
 * Note: does not clean up the links automatically. Use {@link CleanupAppLinksRule} for that.
 *
 * @since 4.3
 */
public class TestApplinkRule extends TestWatcher implements TestApplink {
    @Nonnull
    public static TestApplinkRule forProducts(@Nonnull TestedInstance from, @Nonnull TestedInstance to) {
        return new Builder(from, to).build();
    }

    private final TestedInstance from;
    private final TestedInstance to;
    private final String name;

    private final Iterable<TestApplinkConfigurator> fromConfigurators;
    private final Iterable<TestApplinkConfigurator> toConfigurators;
    private final Iterable<TestApplinkConfigurator> configurators;

    private TestApplink applink;

    private TestApplinkRule(Builder builder) {
        this.from = builder.from;
        this.to = builder.to;
        this.name = builder.name;
        this.fromConfigurators = ImmutableList.copyOf(builder.fromConfigurators);
        this.toConfigurators = ImmutableList.copyOf(builder.toConfigurators);
        this.configurators = ImmutableList.copyOf(builder.configurators);
    }

    private TestApplinkRule(Builder builder, TestApplink applink) {
        this(builder);
        this.applink = applink;
    }

    @Nonnull
    @Override
    public Side from() {
        return getApplink().from();
    }

    @Nonnull
    @Override
    public Side to() {
        return getApplink().to();
    }

    @Nonnull
    @Override
    public TestApplink reverse() {
        if (applink == null) {
            return Builder.reverse(this).build();
        } else {
            return new TestApplinkRule(Builder.reverse(this), applink.reverse());
        }
    }

    public TestApplinkRule configureFrom(@Nonnull Iterable<TestApplinkConfigurator> fromConfigurators) {
        checkApplink();
        ApplinkConfigurators.configureSide(applink.from(), fromConfigurators);

        return this;
    }

    public TestApplinkRule configureFrom(@Nonnull TestApplinkConfigurator... fromConfigurators) {
        return configureFrom(asList(fromConfigurators));
    }

    public TestApplinkRule configureTo(@Nonnull Iterable<TestApplinkConfigurator> toConfigurators) {
        checkApplink();
        ApplinkConfigurators.configureSide(applink.to(), toConfigurators);

        return this;
    }

    public TestApplinkRule configureTo(@Nonnull TestApplinkConfigurator... toConfigurators) {
        return configureTo(asList(toConfigurators));
    }

    public TestApplinkRule configure(@Nonnull Iterable<TestApplinkConfigurator> configurators) {
        checkApplink();
        ApplinkConfigurators.configure(applink, configurators);

        return this;
    }

    public TestApplinkRule configure(@Nonnull TestApplinkConfigurator... configurators) {
        return configure(asList(configurators));
    }

    @Override
    protected void starting(Description description) {
        applink = name != null ?
                new ApplinksBackdoor(from).create(new BackdoorCreateApplinkRequest.Builder(to)
                        .name(name)
                        .build()) :
                new ApplinksBackdoor(from).create(to);
        configure(configurators);
        configureFrom(fromConfigurators);
        configureTo(toConfigurators);
    }

    @Override
    protected void finished(Description description) {
        applink = null;
    }

    private TestApplink getApplink() {
        checkApplink();
        return applink;
    }

    private void checkApplink() {
        checkState(applink != null, "Applink has not been created. This method can only be called during test run");
    }

    public static final class Builder {
        static Builder reverse(TestApplinkRule rule) {
            return new Builder(rule.to, rule.from)
                    .configure(rule.configurators)
                    .configureFrom(rule.fromConfigurators)
                    .configureTo(rule.toConfigurators);
        }

        private final TestedInstance from;
        private final TestedInstance to;
        private String name;

        private final List<TestApplinkConfigurator> fromConfigurators = Lists.newArrayList();
        private final List<TestApplinkConfigurator> toConfigurators = Lists.newArrayList();
        private final List<TestApplinkConfigurator> configurators = Lists.newArrayList();

        public Builder(@Nonnull TestedInstance from, @Nonnull TestedInstance to) {
            this.from = checkNotNull(from, "from");
            this.to = checkNotNull(to, "to");
        }

        @Nonnull
        public Builder name(@Nullable String name) {
            this.name = name;
            return this;
        }

        @Nonnull
        public Builder configureFrom(@Nonnull Iterable<TestApplinkConfigurator> fromConfigurators) {
            checkNotNull(fromConfigurators, "fromConfigurators");
            this.fromConfigurators.clear();
            Iterables.addAll(this.fromConfigurators, fromConfigurators);
            return this;
        }

        @Nonnull
        public Builder configureFrom(@Nonnull TestApplinkConfigurator... fromConfigurators) {
            return configureFrom(asList(fromConfigurators));
        }

        @Nonnull
        public Builder configureTo(@Nonnull Iterable<TestApplinkConfigurator> toConfigurators) {
            checkNotNull(toConfigurators, "toConfigurators");
            this.toConfigurators.clear();
            Iterables.addAll(this.toConfigurators, toConfigurators);
            return this;
        }

        @Nonnull
        public Builder configureTo(@Nonnull TestApplinkConfigurator... toConfigurators) {
            return configureTo(asList(toConfigurators));
        }

        @Nonnull
        public Builder configure(@Nonnull Iterable<TestApplinkConfigurator> configurators) {
            checkNotNull(configurators, "configurators");
            this.configurators.clear();
            Iterables.addAll(this.configurators, configurators);
            return this;
        }

        @Nonnull
        public Builder configure(@Nonnull TestApplinkConfigurator... configurators) {
            return configure(asList(configurators));
        }

        @Nonnull
        public TestApplinkRule build() {
            return new TestApplinkRule(this);
        }
    }
}
