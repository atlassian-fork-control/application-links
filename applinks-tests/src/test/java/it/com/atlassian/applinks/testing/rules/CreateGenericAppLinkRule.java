package it.com.atlassian.applinks.testing.rules;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.pageobjects.TestedProduct;

import org.junit.rules.RuleChain;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Creates new Generic link prior to each test run.
 *
 * @since 4.0.15
 */
public class CreateGenericAppLinkRule extends TestWatcher {
    private final TestedProduct<?> product;
    private final String baseUrl;
    private final String name;

    /**
     * Add a generic link, specified by baseUrl and name, to each product.
     */
    public static RuleChain forProducts(final String baseUrl, final String name, TestedProduct<?>... products) {
        RuleChain chain = RuleChain.emptyRuleChain();

        for (final TestedProduct<?> product : products) {
            chain = chain.around(new CreateGenericAppLinkRule(product, baseUrl, name));
        }
        return chain;
    }

    @Inject
    public CreateGenericAppLinkRule(@Nonnull final TestedProduct<?> product, final String baseUrl, final String name) {
        this.product = product;
        this.baseUrl = baseUrl;
        this.name = name;
    }

    @Override
    protected void starting(Description description) {
        OAuthApplinksClient.createGenericAppLinkViaRest(product, baseUrl, name);
    }
}


