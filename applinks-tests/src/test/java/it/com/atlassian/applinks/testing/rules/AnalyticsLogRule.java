package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Supplier;
import com.jayway.restassured.specification.ResponseSpecification;
import it.com.atlassian.applinks.testing.backdoor.analytics.AnalyticsBackdoor;
import it.com.atlassian.applinks.testing.specification.AnalyticsEventsExpectations;
import org.hamcrest.Matchers;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.applinks.testing.product.ProductInstances.matches;
import static java.util.Objects.requireNonNull;

/**
 * Test watcher that enabled/disabled analytics collection before/after test (or test class) and allows to verify that
 * certain analytics events were published.
 *
 * @since 4.3
 */
public final class AnalyticsLogRule extends TestWatcher {
    private final AnalyticsBackdoor analyticsBackdoor;
    private final ProductInstance product;

    @Inject
    public AnalyticsLogRule(@Nonnull ProductInstance product) {
        this.product = requireNonNull(product, "product");
        this.analyticsBackdoor = new AnalyticsBackdoor(product);
    }

    public void clearLogs() {
        analyticsBackdoor.clearLogs();
    }

    /**
     * Verify that the logs since enabling capturing meet the expectations.
     *
     * @param expectations expectations for results
     * @see AnalyticsEventsExpectations
     */
    public void assertLogsMatch(@Nonnull ResponseSpecification... expectations) {
        analyticsBackdoor.getLogsExpecting(expectations);
    }

    /**
     * Wait until the logs since enabling capturing meet the expectations. Uses {@link TimeoutType#DEFAULT}. Use this
     * timed version if asserting for client-side analytics.
     *
     * @param expectations expectations for results
     * @see AnalyticsEventsExpectations
     */
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public void waitUntilLogsMatch(@Nonnull final ResponseSpecification... expectations) {
        waitUntil(getLogsQuery(expectations), Matchers.nullValue());
    }

    @Override
    protected void starting(Description description) {
        if (matches(SupportedProducts.FECRU, product)) {
            // unfortunately Fecru starts with Analytics disabled, so we have to make an extra request first
            analyticsBackdoor.enableAnalytics();
        }
        analyticsBackdoor.clearLogs();
        analyticsBackdoor.turnCapturingOn();
    }

    @Override
    protected void finished(Description description) {
        analyticsBackdoor.turnCapturingOff();
    }

    private TimedQuery<AssertionError> getLogsQuery(@Nonnull final ResponseSpecification... expectations) {
        return Queries.forSupplier(new DefaultTimeouts(), new Supplier<AssertionError>() {
            @Override
            public AssertionError get() {
                try {
                    assertLogsMatch(expectations);
                    return null;
                } catch (AssertionError e) {
                    return e;
                }
            }
        });
    }
}
