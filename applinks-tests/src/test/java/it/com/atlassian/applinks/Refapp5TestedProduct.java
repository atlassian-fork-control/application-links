package it.com.atlassian.applinks;

import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.refapp.RefappTestedProduct;

@Defaults(instanceId = "refapp5", contextPath = "/refapp", httpPort = 5995)
public class Refapp5TestedProduct extends RefappTestedProduct {
    public Refapp5TestedProduct(TestedProductFactory.TesterFactory<WebDriverTester> testerFactory, ProductInstance productInstance) {
        super(testerFactory, productInstance);
    }
}
