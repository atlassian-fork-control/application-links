package it.com.atlassian.applinks.auth.trust;

import com.atlassian.webdriver.applinks.page.TrustedAppsFailurePage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.categories.TrustedAppsTest;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static junit.framework.Assert.assertTrue;

/**
 * Tests for the simple case of requesting a url from a trusted application and getting a failure code.
 *
 * @since 3.11.0
 */
@Category({TrustedAppsTest.class, RefappTest.class})
public class TrustedApplinksRequestFailureTestRefApp extends V2AbstractApplinksTest {
    @Rule
    public TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    @Before
    public void setUp() throws Exception {
        applink.configure(enableTrustedApps());
        loginAsSysadmin(PRODUCT, PRODUCT2);
    }

    @Test
    public void willGetFailurePageFromTrustedApplication() throws Exception {
        final String response = PRODUCT.visit(TrustedAppsFailurePage.class).getTrustedResponse();
        assertTrue(response.contains("There was a problem with the request"));
    }
}