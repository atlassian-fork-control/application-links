package it.com.atlassian.applinks.rest;

import com.jayway.restassured.RestAssured;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.hamcrest.CoreMatchers.containsString;

@Category(RefappTest.class)
public class NoCacheCacheControlRestTest {
    private static final String REST_END_POINT = ProductInstances.REFAPP1.getBaseUrl() +
            "/rest/applinks/latest/manifest";

    @Test
    public void shouldContainNoCacheInCacheControlHeader() {
        RestAssured.when().get(REST_END_POINT).then().header("Cache-Control", containsString("no-cache"));
    }
}