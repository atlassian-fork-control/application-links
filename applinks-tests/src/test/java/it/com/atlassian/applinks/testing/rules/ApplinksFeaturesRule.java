package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.backdoor.SystemPropertiesBackdoor;
import com.atlassian.applinks.test.rest.feature.ApplinksFeaturesRestTester;
import it.com.atlassian.applinks.testing.ApplinksAuthentications;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import java.util.EnumSet;

import static com.atlassian.applinks.internal.feature.ApplinksSystemFeatures.featureKeyFor;
import static com.atlassian.applinks.test.rest.feature.ApplinksFeatureRequest.disableFeature;
import static com.atlassian.applinks.test.rest.feature.ApplinksFeatureRequest.forFeature;
import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forProduct;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Allows for manipulating Applinks features during tests. Restores all Applinks features to a default state after
 * each test.
 *
 * @since 4.3
 */
public class ApplinksFeaturesRule extends TestWatcher {
    private static final EnumSet<ApplinksFeatures> EMPTY_APPLINKS_FEATURES = EnumSet.noneOf(ApplinksFeatures.class);

    private final TestedInstance instance;

    private final ApplinksFeaturesRestTester featureRestTester;
    private final SystemPropertiesBackdoor systemPropertiesBackdoor;

    private final EnumSet<ApplinksFeatures> enabledFeatures;
    private final EnumSet<ApplinksFeatures> disabledFeatures;

    public ApplinksFeaturesRule(@Nonnull TestedInstance instance) {
        this(instance, EMPTY_APPLINKS_FEATURES, EMPTY_APPLINKS_FEATURES);
    }

    private ApplinksFeaturesRule(@Nonnull TestedInstance instance, EnumSet<ApplinksFeatures> enabledFeatures,
                                 EnumSet<ApplinksFeatures> disabledFeatures) {
        this.instance = checkNotNull(instance, "instance");
        this.featureRestTester = new ApplinksFeaturesRestTester(forProduct(instance),
                ApplinksAuthentications.SYSADMIN);
        this.systemPropertiesBackdoor = new SystemPropertiesBackdoor(instance);
        this.enabledFeatures = checkNotNull(enabledFeatures);
        this.disabledFeatures = checkNotNull(disabledFeatures);
    }

    @Nonnull
    public ApplinksFeaturesRule withDisabledFeatures(@Nonnull ApplinksFeatures first,
                                                     @Nonnull ApplinksFeatures... more) {
        return new ApplinksFeaturesRule(instance, enabledFeatures, EnumSet.of(first, more));
    }

    @Nonnull
    public ApplinksFeaturesRule withEnabledFeatures(@Nonnull ApplinksFeatures first,
                                                    @Nonnull ApplinksFeatures... more) {
        return new ApplinksFeaturesRule(instance, EnumSet.of(first, more), disabledFeatures);
    }

    public final void enable(@Nonnull ApplinksFeatures feature) {
        if (feature.isSystem()) {
            systemPropertiesBackdoor.setProperty(featureKeyFor(feature), Boolean.TRUE.toString());
        } else {
            featureRestTester.put(forFeature(feature));
        }
    }

    public final void disable(@Nonnull ApplinksFeatures feature) {
        if (feature.isSystem()) {
            systemPropertiesBackdoor.setProperty(featureKeyFor(feature), Boolean.FALSE.toString());
        } else {
            featureRestTester.delete(disableFeature(feature));
        }
    }

    @Override
    protected void starting(Description description) {
        for (ApplinksFeatures toEnable : enabledFeatures) {
            enable(toEnable);
        }
        for (ApplinksFeatures toDisable : disabledFeatures) {
            disable(toDisable);
        }
    }

    @Override
    protected void finished(Description description) {
        restoreFeatureDefaults();
    }

    private void restoreFeatureDefaults() {
        for (ApplinksFeatures feature : ApplinksFeatures.values()) {
            if (feature.isSystem()) {
                // for system features, just remove the corresponding property value
                systemPropertiesBackdoor.clearProperty(featureKeyFor(feature));
            } else {
                if (feature.getDefaultValue()) {
                    enable(feature);
                } else {
                    disable(feature);
                }
            }
        }
    }
}
