package it.com.atlassian.applinks.testing.product;

import com.atlassian.applinks.test.product.SupportedProducts;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.annotation.Nonnull;

/**
 * Allows tests run by {@code ApplinksSmokeTestRunner} to declare products that they want to ignore.
 *
 * @see it.com.atlassian.applinks.testing.runner.ApplinksSmokeTestRunner
 * @since 5.1
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Inherited
public @interface IgnoredProducts {
    /**
     * @return all product types that should be ignored for this test
     */
    @Nonnull SupportedProducts[] value();

    @Nonnull String reason() default "";
}