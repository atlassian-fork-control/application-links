package it.com.atlassian.applinks.smoke.analytics;

import com.atlassian.applinks.analytics.ApplinksAdminViewEvent;
import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.smoke.AbstractDefaultSmokeTest;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.IgnoredProducts;
import it.com.atlassian.applinks.testing.rules.AnalyticsLogRule;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.feature.ApplinksFeatures.V3_UI;
import static it.com.atlassian.applinks.testing.product.TestedInstances.fromProductInstance;
import static it.com.atlassian.applinks.testing.specification.AnalyticsEventsExpectations.expectRetainedEvent;

/**
 * Verifies that admin view analytics events are published when accessing Applinks admin both V2 and V3.
 *
 * @since 4.3
 */
@IgnoredProducts(value = SupportedProducts.REFAPP, reason = "Refapp does not support Analytics")
@Category(SmokeTest.class)
public class AdminViewAnalyticsTest extends AbstractDefaultSmokeTest {
    @Rule
    public final ApplinksFeaturesRule applinksFeaturesRule;
    @Rule
    public final AnalyticsLogRule analyticsLogRule;

    public AdminViewAnalyticsTest(@Nonnull GenericTestedProduct mainProduct,
                                  @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        this.applinksFeaturesRule = new ApplinksFeaturesRule(fromProductInstance(mainInstance));
        this.analyticsLogRule = new AnalyticsLogRule(mainInstance);
    }

    @Before
    public void setup() {
        LoginClient.loginAsSysadmin(mainProduct);
    }

    @Test
    public void visitingV3ListApplicationLinkPageShouldPublishAnalyticsEvent() {
        mainProduct.visit(V3ListApplicationLinksPage.class);

        analyticsLogRule.assertLogsMatch(
                expectRetainedEvent(ApplinksAdminViewEvent.class)
        );
    }

    @Test
    public void visitingV2ListApplicationLinkPageShouldPublishAnalyticsEvent() {
        applinksFeaturesRule.disable(V3_UI);

        mainProduct.visit(ListApplicationLinkPage.class);

        analyticsLogRule.assertLogsMatch(
                expectRetainedEvent(ApplinksAdminViewEvent.class)
        );
    }
}
