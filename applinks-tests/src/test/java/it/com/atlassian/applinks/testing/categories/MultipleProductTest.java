package it.com.atlassian.applinks.testing.categories;

/**
 * JUnit Category to classify tests requiring multiple products.
 *  WARNING: this category is not bound to any integration tests.
 *  if you just add this category to your test it <b>will not be executed</b>
 *
 * @since 4.3.0
 */
public interface MultipleProductTest {
}
