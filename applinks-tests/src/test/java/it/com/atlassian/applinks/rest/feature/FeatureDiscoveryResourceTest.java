package it.com.atlassian.applinks.rest.feature;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.SimpleRestRequest;
import com.atlassian.applinks.test.rest.feature.FeatureDiscoveryRequest;
import com.atlassian.applinks.test.rest.feature.FeatureDiscoveryRestTester;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.CleanupUserSettingsRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.test.rest.matchers.RestErrorsMatchers.singleError;
import static com.atlassian.applinks.test.rest.specification.FeatureDiscoveryExpectations.expectDiscoveredFeatures;
import static com.atlassian.applinks.test.rest.specification.FeatureDiscoveryExpectations.expectNoDiscoveredFeatures;
import static com.atlassian.applinks.test.rest.specification.FeatureDiscoveryExpectations.expectSingleDiscoveredFeature;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.expectBody;
import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forProduct;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.Matchers.containsInAnyOrder;

@Category(RefappTest.class)
public class FeatureDiscoveryResourceTest {
    private static final ProductInstances TESTED_PRODUCT = ProductInstances.REFAPP1;
    private static final ApplinksRestUrls REFAPP_URL = forProduct(TESTED_PRODUCT);

    private static final TestAuthentication TEST_USER = TestAuthentication.admin();

    private final FeatureDiscoveryRestTester featureDiscoveryRestTester = new FeatureDiscoveryRestTester(REFAPP_URL);

    @Rule
    public final CleanupUserSettingsRule cleanupUserSettingsRule =
            new CleanupUserSettingsRule(TESTED_PRODUCT, TEST_USER);

    @Test
    public void shouldNotAccessGetUnauthenticated() {
        featureDiscoveryRestTester.get(new FeatureDiscoveryRequest.Builder("test.feature")
                .authentication(TestAuthentication.anonymous())
                .expectStatus(UNAUTHORIZED)
                .specification(expectBody(singleError("You need to be authenticated to perform this operation.")))
                .build());

        featureDiscoveryRestTester.getAll(new SimpleRestRequest.Builder()
                .authentication(TestAuthentication.anonymous())
                .expectStatus(UNAUTHORIZED)
                .specification(expectBody(singleError("You need to be authenticated to perform this operation.")))
                .build());
    }

    @Test
    public void authenticatedAccessNoDiscoveredFeatures() {
        featureDiscoveryRestTester.get(new FeatureDiscoveryRequest.Builder("test.feature")
                .authentication(TEST_USER)
                .expectStatus(NOT_FOUND)
                .build());

        featureDiscoveryRestTester.getAll(new SimpleRestRequest.Builder()
                .authentication(TEST_USER)
                .expectStatus(OK)
                .specification(expectNoDiscoveredFeatures())
                .build());
    }

    @Test
    public void shouldNotAccessPutUnauthenticated() {
        featureDiscoveryRestTester.put(new FeatureDiscoveryRequest.Builder("test.feature")
                .authentication(TestAuthentication.anonymous())
                .expectStatus(UNAUTHORIZED)
                .build());
    }

    @Test
    public void featureDiscoveryAsUser() {
        // discover test.feature
        featureDiscoveryRestTester.put(new FeatureDiscoveryRequest.Builder("test.feature")
                .authentication(TEST_USER)
                .expectStatus(OK)
                .specification(expectSingleDiscoveredFeature("test.feature"))
                .build());

        // discover test-feature
        featureDiscoveryRestTester.put(new FeatureDiscoveryRequest.Builder("test-feature")
                .authentication(TEST_USER)
                .expectStatus(OK)
                .specification(expectSingleDiscoveredFeature("test-feature"))
                .build());

        // get test.feature
        featureDiscoveryRestTester.get(new FeatureDiscoveryRequest.Builder("test.feature")
                .authentication(TEST_USER)
                .expectStatus(OK)
                .specification(expectSingleDiscoveredFeature("test.feature"))
                .build());

        // get all
        featureDiscoveryRestTester.getAll(new SimpleRestRequest.Builder()
                .authentication(TEST_USER)
                .expectStatus(OK)
                .specification(expectDiscoveredFeatures(containsInAnyOrder("test.feature", "test-feature")))
                .build());
    }
}
