package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.junit.rules.RuleChain;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * Login to the specified instances.
 *
 * @since 5.0.0
 */
public class LoginRule extends TestWatcher {
    private final TestedProduct<WebDriverTester> product;

    /**
     * Apply the rule to each product.
     */
    public static RuleChain forProducts(TestedProduct<WebDriverTester>... products) {
        RuleChain chain = RuleChain.emptyRuleChain();

        for (final TestedProduct<WebDriverTester> product : products) {
            // apply the rule to each localProduct, linking to the other products.
            chain = chain.around(new LoginRule(product));
        }
        return chain;
    }

    @Inject
    public LoginRule(@Nonnull TestedProduct<WebDriverTester> product) {
        this.product = product;
    }

    @Override
    protected void starting(Description description) {
        this.login(this.product);
    }

    public void login(final TestedProduct<WebDriverTester> product) {
        LoginClient.loginAsSysadmin(product);
    }
}

