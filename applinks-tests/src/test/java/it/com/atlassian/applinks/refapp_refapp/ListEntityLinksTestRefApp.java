package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.api.application.refapp.RefAppCharlieEntityType;
import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.webdriver.applinks.page.adg.entitylinks.ConfigureEntityLinksPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category(RefappTest.class)
public class ListEntityLinksTestRefApp extends V2AbstractApplinksTest {
    @Before
    public void setUp() {
        loginAsSysadmin(PRODUCT, PRODUCT2);
        OAuthApplinksClient.createCharlie("sheen", "Hot Shots II");
    }

    @Test
    public void addEntityLinkButtonDoesNotAppearWhenNoAppLinksExist() {
        ConfigureEntityLinksPage entityLinksPage = PRODUCT.visit(ConfigureEntityLinksPage.class, RefAppCharlieEntityType.class, "sheen");

        assertFalse("Entity links page should not have 'add link' button", entityLinksPage.hasAddLink());
        assertTrue("Entity links page should have 'no app links' message", entityLinksPage.hasNoAppLinksMessage());
    }

    @Test
    public void addEntityLInkButtonDoesAppearWhenAppLinksExist() {
        OAuthApplinksClient.createReciprocal3LO2LOLinkViaRest(PRODUCT, PRODUCT2);

        ConfigureEntityLinksPage entityLinksPage = PRODUCT.visit(ConfigureEntityLinksPage.class, RefAppCharlieEntityType.class, "sheen");

        assertTrue("Entity links page should have 'add link' button", entityLinksPage.hasAddLink());
        assertFalse("Entity links page should not have 'no app links' message", entityLinksPage.hasNoAppLinksMessage());
    }
}
