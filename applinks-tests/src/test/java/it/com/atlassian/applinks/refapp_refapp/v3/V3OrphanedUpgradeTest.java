package it.com.atlassian.applinks.refapp_refapp.v3;

import com.atlassian.applinks.test.backdoor.OrphanedTrustBackdoor;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

@Category(RefappTest.class)
public class V3OrphanedUpgradeTest extends AbstractApplinksTest {
    private OrphanedTrustBackdoor orphanedTrustBackdoor1 = new OrphanedTrustBackdoor(INSTANCE1);
    private OrphanedTrustBackdoor orphanedTrustBackdoor2 = new OrphanedTrustBackdoor(INSTANCE2);

    @Test
    public void orphanedOAuthWarningInV3() {
        setUpOrphanedOAuth();

        V3ListApplicationLinksPage page = loginAndGoToV3Ui();

        waitUntilTrue("Should have orphaned trust warning", page.hasOrphanedTrustWarning());
        page.openOrphanedTrustDialog().deleteFirstEntry().confirm();
        waitUntilFalse("Should remove trust warning once deleted", page.hasOrphanedTrustWarning());
    }

    private void setUpOrphanedOAuth() {
        orphanedTrustBackdoor1.setUpOrphanedOAuth(orphanedTrustBackdoor2.getOrphanedOAuthId());
    }

    private V3ListApplicationLinksPage loginAndGoToV3Ui() {
        return loginAsSysadminAndGoTo(PRODUCT, V3ListApplicationLinksPage.class);
    }
}
