package it.com.atlassian.applinks.rest.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.test.rest.capabilities.ApplinksCapabilitiesRestTester;
import com.atlassian.applinks.test.rest.capabilities.ApplinksRemoteCapilitiesRequest;
import it.com.atlassian.applinks.testing.ApplinksAuthentications;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import java.util.concurrent.TimeUnit;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities.STATUS_API;
import static com.atlassian.applinks.test.authentication.TestAuthentication.anonymous;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationVersionMatchers.expectValidVersion;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationVersionMatchers.expectVersion;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationVersionMatchers.withMajorThat;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.containsAllEnumValues;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.containsEnumValue;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.applicationVersion;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.applinksVersion;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.capabilities;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.expectNoError;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.not;

@Category(RefappTest.class)
public class ApplinksRemoteCapabilitiesResourceTest {
    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);
    @Rule
    public final ApplinksCapabilitiesRule refapp2Capabilities = new ApplinksCapabilitiesRule(REFAPP2);
    @Rule
    public final ApplinksVersionOverrideRule refapp2Version = new ApplinksVersionOverrideRule(REFAPP2);

    private final ApplinksCapabilitiesRestTester capabilitiesRestTester = new ApplinksCapabilitiesRestTester(REFAPP1);

    @Test
    public void remoteCapabilitiesAsAnonymous() {
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(anonymous())
                .expectStatus(Status.UNAUTHORIZED)
                .build());
    }

    @Test
    public void remoteCapabilitiesAsUser() {
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(ApplinksAuthentications.REFAPP_USER_BARNEY)
                .expectStatus(Status.FORBIDDEN)
                .build());
    }

    @Test
    public void remoteCapabilitiesOk() {
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());
    }

    @Test
    public void remoteCapabilitiesRefreshOk() {
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());
    }

    @Test
    public void remoteCapabilitiesLargeMaxAgeOk() {
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(10)
                .timeUnit(TimeUnit.DAYS)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());
    }

    @Test
    public void remoteCapabilitiesChangeWithoutVersionsChange() {
        refapp2Capabilities.disable(STATUS_API);

        // capabilities should have been cached and since version has not changed, they should not have been updated
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());
    }

    @Test
    public void remoteCapabilitiesChangeWithVersionsChange() {
        refapp2Capabilities.disable(STATUS_API);
        refapp2Version.setApplinksVersion("4.3.5");

        // capabilities should have been cached before, so without maxAge we should get the existing (stale) value
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(withMajorThat(greaterThan(4)))) // original Applinks version is > 4.x
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());

        // now get fresh capabilities and make sure they have been updated
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectVersion(4, 3, 5)))
                .specification(capabilities(not(containsEnumValue(STATUS_API))))
                .specification(expectNoError())
                .build());
    }
}
