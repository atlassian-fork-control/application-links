package it.com.atlassian.applinks.smoke.v3;

import com.atlassian.applinks.internal.common.rest.model.applink.RestMinimalApplicationLink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.webdriver.applinks.component.v3.ApplinkRow;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.smoke.AbstractDefaultSmokeTest;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.pageobjects.LoginClient.loginAsSysadminAndGoTo;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@Category(SmokeTest.class)
public class ApplinksV3StatusUiSmokeTest extends AbstractDefaultSmokeTest {
    @Rule
    public final TestApplinkRule testApplink;

    private final ApplinksBackdoor applinksBackdoor;

    private V3ListApplicationLinksPage page;

    public ApplinksV3StatusUiSmokeTest(@Nonnull GenericTestedProduct mainProduct,
                                       @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        testApplink = new TestApplinkRule.Builder(mainInstance, backgroundInstance)
                .configure(enableDefaultOAuth())
                .build();
        applinksBackdoor = new ApplinksBackdoor(mainInstance);
    }

    @Before
    public void loginAndGoToV3Ui() {
        page = loginAsSysadminAndGoTo(mainProduct, V3ListApplicationLinksPage.class);
        waitUntil(page.getApplinksRows(), Matchers.<ApplinkRow>iterableWithSize(1));
        waitUntilTrue(page.getApplinkRow(testApplink.from().applicationId()).hasVersion());
    }

    @Test
    public void eachRowShouldContainApplicationLinkInformation() {
        ApplinkRow row = page.getApplinkRow(testApplink.from().applicationId());
        RestMinimalApplicationLink restApplink = applinksBackdoor.getApplink(testApplink.from());

        waitUntilTrue(row.isPrimary());
        assertThat(row.getName(), equalTo(restApplink.getName()));
        assertThat(row.getDisplayUrl(), equalTo("Display URL: " + restApplink.getDisplayUrl()));
        assertThat(row.getRpcUrl(), equalTo("Application URL: " + restApplink.getRpcUrl()));
        waitUntilTrue(row.getStatus().isWorking());
    }
}
