package it.com.atlassian.applinks.rest.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.test.response.MockResponseDefinition;
import com.atlassian.applinks.test.rest.capabilities.ApplinksCapabilitiesRestTester;
import com.atlassian.applinks.test.rest.capabilities.ApplinksRemoteCapilitiesRequest;
import com.atlassian.applinks.test.rule.MockResponseRule;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.data.applink.config.UrlApplinkConfigurator.setUrl;
import static com.atlassian.applinks.test.data.util.TestUrls.createRandomUrl;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationVersionMatchers.expectValidVersion;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.containsAllEnumValues;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.applicationVersion;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.applinksVersion;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.capabilities;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.expectError;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.expectErrorDetails;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.expectNoError;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static it.com.atlassian.applinks.testing.response.MockResponses.manifestResourceRequest;
import static it.com.atlassian.applinks.testing.response.MockResponses.unexpectedStatusResponse;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.openqa.selenium.net.PortProber.findFreePort;

@Category(RefappTest.class)
public class ApplinksRemoteCapabilitiesResourceConnectionErrorTest {
    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);
    @Rule
    public final MockResponseRule refapp2Responses = new MockResponseRule(REFAPP2);

    private final ApplinksCapabilitiesRestTester capabilitiesRestTester = new ApplinksCapabilitiesRestTester(REFAPP1);

    @Test
    public void remoteCapabilitiesConnectionRefused() {
        // invalid port
        applink.configureFrom(setUrl(baseUrlWithPort(REFAPP2, findFreePort())));

        // no error if no refresh as the original capabilities are cached
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());

        // refresh capabilities to get the latest state (maxAge=0)
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectError(ApplinkErrorType.CONNECTION_REFUSED))
                .build());
    }

    @Test
    public void remoteCapabilitiesUnresolvedHost() {
        // unresolved URL
        applink.configureFrom(setUrl(createRandomUrl()));

        // no error if no refresh as the original capabilities are cached
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());

        // refresh capabilities to get the latest state (maxAge=0)
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectError(ApplinkErrorType.UNKNOWN_HOST))
                .build());
    }

    @Test
    public void remoteCapabilitiesUnexpectedStatusFromManifest() {
        // request to manifest returns 400
        refapp2Responses.addMockResponse(manifestResourceRequest().build(),
                unexpectedStatusResponse(Status.BAD_REQUEST));

        // no error if no refresh as the original capabilities are cached
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());

        // refresh capabilities to get the latest state (maxAge=0)
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectError(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS))
                .specification(expectErrorDetails(is("400: Bad Request")))
                .build());
    }

    @Test
    public void remoteCapabilitiesUnexpectedUnsupportedStatusFromManifest() {
        // request to manifest returns 530 (not officially supported error code)
        refapp2Responses.addMockResponse(manifestResourceRequest().build(), unexpectedStatusResponse(530));

        // no error if no refresh as the original capabilities are cached
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());

        // refresh capabilities to get the latest state (maxAge=0)
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectError(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS))
                .specification(expectErrorDetails(is("530")))
                .build());
    }

    @Test
    public void remoteCapabilitiesUnexpectedResponseFromManifest() {
        // request to manifest returns 200 but invalid contents
        refapp2Responses.addMockResponse(
                manifestResourceRequest().build(),
                new MockResponseDefinition.Builder(Status.OK)
                        .body("Surprise!\nSurprise!", MediaType.TEXT_PLAIN)
                        .build());

        // no error if no refresh as the original capabilities are cached
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());

        // refresh capabilities to get the latest state (maxAge=0)
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectValidVersion()))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectError(ApplinkErrorType.UNEXPECTED_RESPONSE))
                .specification(expectErrorDetails(nullValue()))
                .build());
    }

    private String baseUrlWithPort(ProductInstances instance, int port) {
        return instance.getLoopbackUrl().replace(Integer.toString(instance.getHttpPort()), Integer.toString(port));
    }
}
