package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.webdriver.applinks.component.AppLinkAdminLogin;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.component.OauthIncomingAuthenticationWithAutoConfigSection;
import com.atlassian.webdriver.applinks.component.OauthOutgoingAuthenticationWithAutoConfigSection;
import com.atlassian.webdriver.applinks.externalcomponent.WebSudoPage;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebDriverException;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

@Category(RefappTest.class)
public class TwoLOOAuthConfigurationTestRefApp extends V2AbstractApplinksTest {
    private ApplicationDetailsSection detailsSection;

    @Test
    public void testDisablingEnablingOAuth() {
        loginAsSysadminAndCreateLink();
        OauthIncomingAuthenticationWithAutoConfigSection section = disableIncomingOAuth();
        section = section.enable();
        assertTrue(section.isConfigured());
    }

    @Test
    public void testEnabling2LO() throws Exception {
        loginAsSysadminAndCreateLink();
        OauthIncomingAuthenticationWithAutoConfigSection section = detailsSection.openIncomingOauthExpectingAutoConfig();

        // v2 Workflow 2LO must be present but is now on by default.
        waitUntilTrue(section.is2LOSectionPresentTimed());
        waitUntilTrue(section.is2LOEnabledTimed());

        // now disable 2LO.
        section = section.uncheck2LO();

        // there should be no any error/success message on screen
        assertNull(section.get2LOErrorMessage());
        assertNull(section.get2LOSuccessMessage());

        section = section.clickUpdate2LOConfig();
        assertNotNull(section.get2LOSuccessMessage());

        // now enable 2LO. all additional options must be off by default.
        section = section.check2LO();
        assertFalse(section.is2LOImpersonationEnabled());
        assertEquals(section.get2LOExecuteAs(), "");

        // save it now will NOT result in error since the v2 workflow allows anonymous 2LO.
        section = section.clickUpdate2LOConfig();
        assertNotNull(section.get2LOSuccessMessage());

        // enter invalid username will also result in error.
        section.set2LOExecuteAs("a_very_random_username");
        section = section.clickUpdate2LOConfig();
        assertEquals("A valid username must be supplied.", section.get2LOErrorMessage());
        assertNull(section.get2LOSuccessMessage());

        // enter sysadmin will also result in error.
        section.set2LOExecuteAs(SYSADMIN_USERNAME);
        section = section.clickUpdate2LOConfig();
        assertEquals("The user must not be a System Administrator.", section.get2LOErrorMessage());
        assertNull(section.get2LOSuccessMessage());

        // enter a good username will result in a success and the username must persist
        section.set2LOExecuteAs(ADMIN_USERNAME);
        section = section.clickUpdate2LOConfig();
        assertNull(section.get2LOErrorMessage());
        assertNotNull(section.get2LOSuccessMessage());
        assertEquals(section.get2LOExecuteAs(), ADMIN_USERNAME);
    }

    @Test
    public void testEnabling2LOImpersonation() throws Exception {
        loginAsSysadminAndCreateLink();
        OauthIncomingAuthenticationWithAutoConfigSection section = detailsSection.openIncomingOauthExpectingAutoConfig();

        // let's make 2LO on first.
        section = section.check2LO();
        section.set2LOExecuteAs(ADMIN_USERNAME);
        section = section.clickUpdate2LOConfig();

        // enable impersonation
        section = section.check2LOImpersonation();
        section = section.clickUpdate2LOConfig();
        assertNotNull(section.get2LOSuccessMessage());
        assertEquals(section.get2LOExecuteAs(), ADMIN_USERNAME);
        assertTrue(section.is2LOEnabled());
        assertTrue(section.is2LOImpersonationEnabled());
    }

    @Test
    public void testEnabling2LOImpersonationWhen2LODisabledIsNotPossible() throws Exception {
        loginAsSysadminAndCreateLink();
        OauthIncomingAuthenticationWithAutoConfigSection section = detailsSection.openIncomingOauthExpectingAutoConfig();

        // v2 Workflow 2LO must be present but is now on by default.
        waitUntilTrue(section.is2LOSectionPresentTimed());
        waitUntilTrue(section.is2LOEnabledTimed());

        // now disable 2LO.
        section = section.uncheck2LO();

        // there should be no any error/success message on screen
        assertNull(section.get2LOErrorMessage());
        assertNull(section.get2LOSuccessMessage());

        section = section.clickUpdate2LOConfig();

        try {
            section.check2LOImpersonation();
            fail("should not be able to turn on impersonation if 2LO is off");
        } catch (WebDriverException wde) {
            // expected
        }
    }

    @Test
    public void testDisable2LO() throws Exception {
        loginAsSysadminAndCreateLink();
        OauthIncomingAuthenticationWithAutoConfigSection section = detailsSection.openIncomingOauthExpectingAutoConfig();

        // first enable them all at once
        section = section.check2LO();
        section.set2LOExecuteAs(ADMIN_USERNAME);
        section = section.check2LOImpersonation();
        section = section.clickUpdate2LOConfig();

        // disable impersonation first.
        section = section.uncheck2LOImpersonation();
        section = section.clickUpdate2LOConfig();
        assertNotNull(section.get2LOSuccessMessage());
        assertNull(section.get2LOErrorMessage());

        // then disable 2LO. the message should show a success and the 2LO option must be off
        section = section.uncheck2LO();
        section = section.clickUpdate2LOConfig();
        assertNotNull(section.get2LOSuccessMessage());
        assertNull(section.get2LOErrorMessage());
        assertFalse(section.is2LOEnabled());
    }

    @Test
    public void testAdminCanSee2LOSectionWithNoImpersonationOption() throws Exception {
        loginAsAdminAndCreateLink();
        OauthIncomingAuthenticationWithAutoConfigSection section = detailsSection.openIncomingOauthExpectingAutoConfig();

        waitUntilTrue(section.is2LOSectionPresentTimed());
        waitUntilFalse(section.is2LOWithImpersonationSectionPresentTimed());
    }

    @Test
    public void testEnablingDisabling2LOPropagatesToOtherApplication() {
        loginAsSysadminAndCreateLink();
        OauthIncomingAuthenticationWithAutoConfigSection section1 = visitDetails(PRODUCT, PRODUCT2).openIncomingOauthExpectingAutoConfig();

        // enable 2LO and 2LOi in refapp1 incoming oauth
        section1 = section1.check2LO();
        section1.set2LOExecuteAs(ADMIN_USERNAME);
        section1 = section1.check2LOImpersonation();
        section1.clickUpdate2LOConfig();
        waitUntilTrue("2LO should be enabled in refapp1", section1.is2LOEnabledTimed());
        waitUntilTrue("2LOi should be enabled in refapp1", section1.is2LOImpersonationEnabledTimed());

        // check if it was propagated to refapp2 outgoing oauth
        OauthOutgoingAuthenticationWithAutoConfigSection section2 = visitDetails(PRODUCT2, PRODUCT).openOutgoingOauthExpectingAutoConfig();
        waitUntilTrue("2LO should be enabled in refapp2", section2.isOutgoing2LOEnabledTimed());
        waitUntilTrue("2LOi should be enabled in refapp2", section2.isOutgoing2LOiEnabledTimed());

        // disable 2LO and 2LOi in refapp2 incoming oauth
        section1 = visitDetails(PRODUCT2, PRODUCT).openIncomingOauthExpectingAutoConfig();
        section1 = section1.uncheck2LO();
        section1.clickUpdate2LOConfig();
        waitUntilFalse("2LO should be disabled in refapp1", section1.is2LOEnabledTimed());
        waitUntilFalse("2LOi should be disabled in refapp1", section1.is2LOImpersonationEnabledTimed());

        // check if it was propagated to refapp1 outgoing oauth
        section2 = visitDetails(PRODUCT, PRODUCT2).openOutgoingOauthExpectingAutoConfig();
        waitUntilFalse("2LO should be disabled in refapp1", section2.isOutgoing2LOEnabledTimed());
        waitUntilFalse("2LOi should be disabled in refapp1", section2.isOutgoing2LOiEnabledTimed());

        // enable 2LO and 2LOi in refapp2 incoming oauth
        section1 = visitDetails(PRODUCT2, PRODUCT).openIncomingOauthExpectingAutoConfig();
        section1 = section1.check2LO();
        section1.set2LOExecuteAs(ADMIN_USERNAME);
        section1 = section1.check2LOImpersonation();
        section1.clickUpdate2LOConfig();
        waitUntilTrue("2LO should be enabled in refapp2", section1.is2LOEnabledTimed());
        waitUntilTrue("2LOi should be enabled in refapp2", section1.is2LOImpersonationEnabledTimed());

        // check if it was propagated to refapp1 outgoing oauth
        section2 = visitDetails(PRODUCT, PRODUCT2).openOutgoingOauthExpectingAutoConfig();
        waitUntilTrue("2LO should be enabled in refapp1", section2.isOutgoing2LOEnabledTimed());
        waitUntilTrue("2LOi should be enabled in refapp1", section2.isOutgoing2LOiEnabledTimed());
    }

    private ApplicationDetailsSection visitDetails(RefappTestedProduct product, RefappTestedProduct linkedProduct) {
        ListApplicationLinkPage linkPage = product.visit(ListApplicationLinkPage.class);
        return linkPage.configureApplicationLink(linkedProduct.getProductInstance().getBaseUrl());
    }

    private void loginAsSysadminAndCreateLink() {
        String url = PRODUCT2.getProductInstance().getBaseUrl();
        loginAsSysadmin(PRODUCT, PRODUCT2);

        OAuthApplinksClient.createReciprocal3LO2LOLinkViaRest(PRODUCT, PRODUCT2);
        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        detailsSection = linkPage.configureApplicationLink(url);
    }

    private void loginAsAdminAndCreateLink() {
        String url = PRODUCT2.getProductInstance().getBaseUrl();
        login(PRODUCT, PRODUCT2);
        OAuthApplinksClient.createReciprocal3LO2LOLinkViaRest(PRODUCT, PRODUCT2);

        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        detailsSection = linkPage.configureApplicationLink(url);
    }

    private OauthIncomingAuthenticationWithAutoConfigSection disableIncomingOAuth() {
        OauthIncomingAuthenticationWithAutoConfigSection section = detailsSection.openIncomingOauthExpectingAutoConfig();
        assertTrue(section.isConfigured());
        section.disable();

        // handle the annoying both applink's own websudo and atlassian's websudo here.
        AppLinkAdminLogin<WebSudoPage> adminLogin = PRODUCT.getPageBinder().bind(AppLinkAdminLogin.class, new WebSudoPage());
        adminLogin.handleWebLoginIfRequired(SYSADMIN_USERNAME, SYSADMIN_PASSWORD);
        WebSudoPage webSudo = PRODUCT.getPageBinder().bind(WebSudoPage.class);
        webSudo.handleIfRequired(SYSADMIN_PASSWORD);
        section = PRODUCT.getPageBinder().bind(OauthIncomingAuthenticationWithAutoConfigSection.class);
        assertTrue(section.isNotConfigured());
        return section;
    }
}
