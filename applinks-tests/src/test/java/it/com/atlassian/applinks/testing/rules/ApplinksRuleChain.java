package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;
import it.com.atlassian.applinks.testing.product.TestedInstances;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.mockito.internal.util.collections.Sets;

import javax.annotation.Nonnull;

import java.util.Set;

import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toImmutableList;
import static com.atlassian.applinks.test.rule.TestRules.toRuleChain;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;

/**
 * Default rule chain for Applinks tests.
 *
 * 3.11.0
 */
public final class ApplinksRuleChain {
    private ApplinksRuleChain() {
        throw new AssertionError("Don't instantiate me");
    }

    static Set<String> jsErrorsToIgnore = Sets.newSet(
            "mutating the [[Prototype]] of an object will cause your code to run very slowly; instead create the object with the correct initial [[Prototype]] value using Object.create"
    );

    /**
     * Rules applicable for {@code WebDriver} tests. NOTE: this rule chain is meant to be used as per-test {@code @Rule}
     * and not a {@code @ClassRule}.
     *
     * @param products tested products under test
     * @return rule chain
     */
    @Nonnull
    @SafeVarargs
    public static RuleChain forProducts(@Nonnull TestedProduct<WebDriverTester>... products) {
        checkNotNull(products, "products");
        return RuleChain.emptyRuleChain()
                .around(new ElapsedTimeRule("Outermost Rule")) // grab timings for test + all rules
                .around(ConfluenceVersionSetRule.forProducts(toProductInstances(products)))
                .around(new BrowserWindowRule(asList(products)))
                .around(new WindowSizeRule())
                .around(new WebDriverScreenshotRule())
                .around(new CleanupBrowserSessionRule(products))
                .around(cleanUpApplinksFor(products))
                .around(setDefaultSettingsFor(products))
                .around(jsErrorsFor(products))
                .around(new ElapsedTimeRule("Innermost Rule")); // grab timings for test only
    }

    @Nonnull
    public static RuleChain forRestTests(@Nonnull ProductInstance... products) {
        checkNotNull(products, "products");
        return RuleChain.emptyRuleChain()
                .around(new ElapsedTimeRule("Outermost Rule")) // grab timings for test + all rules
                .around(CleanupAppLinksRule.forProducts(products))
                .around(new ElapsedTimeRule("Innermost Rule")); // grab timings for test only
    }

    @Nonnull
    public static RuleChain forRestTestsWithApplink(@Nonnull TestedInstance product1,
                                                    @Nonnull TestedInstance product2,
                                                    @Nonnull TestApplinkRule testApplinkRule) {
        return forRestTestsWithApplink(product1, product2, testApplinkRule, emptyList(), emptyList());
    }

    @Nonnull
    public static RuleChain forRestTestsWithApplink(@Nonnull TestedInstance product1,
                                                    @Nonnull TestedInstance product2,
                                                    @Nonnull TestApplinkRule testApplinkRule,
                                                    @Nonnull Iterable<TestRule> beforeApplinkCreated,
                                                    @Nonnull Iterable<TestRule> afterApplinkCreated) {
        checkNotNull(product1, "product1");
        checkNotNull(product2, "product2");
        checkNotNull(beforeApplinkCreated, "beforeApplinkCreated");
        checkNotNull(afterApplinkCreated, "afterApplinkCreated");


        return RuleChain.emptyRuleChain()
                .around(new ElapsedTimeRule("Outermost Rule")) // grab timings for test + all rules
                .around(toRuleChain(beforeApplinkCreated))
                .around(CleanupAppLinksRule.forTestedInstances(product1, product2))
                .around(testApplinkRule)
                .around(toRuleChain(afterApplinkCreated))
                .around(new ElapsedTimeRule("Innermost Rule")); // grab timings for test only
    }

    @SafeVarargs
    private static RuleChain cleanUpApplinksFor(TestedProduct<WebDriverTester>... products) {
        return CleanupAppLinksRule.forTestedInstances(toTestedInstances(products));
    }

    @SafeVarargs
    private static RuleChain setDefaultSettingsFor(TestedProduct<WebDriverTester>... products) {
        return DefaultSettingsRule.forProducts(toTestedInstances(products));
    }

    @SafeVarargs
    private static RuleChain jsErrorsFor(TestedProduct<WebDriverTester>... products) {
        RuleChain chain = RuleChain.emptyRuleChain();
        for (TestedProduct<WebDriverTester> product : products) {
            final JavaScriptErrorsRule javaScriptErrorsRule = new JavaScriptErrorsRule(product.getTester().getDriver())
                    .errorsToIgnore(jsErrorsToIgnore)
                    .failOnJavaScriptErrors(false);
            chain = chain.around(javaScriptErrorsRule);
        }
        return chain;
    }

    @SafeVarargs
    private static Iterable<TestedInstance> toTestedInstances(TestedProduct<WebDriverTester>... products) {
        return stream(products).map(TestedInstances::fromProduct).collect(toImmutableList());
    }

    @SafeVarargs
    private static Iterable<ProductInstance> toProductInstances(TestedProduct<WebDriverTester>... products) {
        return stream(products).map(TestedProduct::getProductInstance).collect(toImmutableList());
    }
}
