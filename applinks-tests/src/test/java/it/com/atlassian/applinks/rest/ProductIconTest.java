package it.com.atlassian.applinks.rest;

import com.atlassian.applinks.test.rest.backdoor.ApplinkTypeMetadata;
import com.atlassian.applinks.test.rest.backdoor.ApplinksTypesBackdoor;
import com.jayway.restassured.RestAssured;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.ws.rs.core.Response;

import static com.atlassian.applinks.test.rest.matchers.JaxRsToStatusCodeMatcher.withStatus;

@Category(RefappTest.class)
public class ProductIconTest {
    @Test
    public void allApplicationIconsShouldExist() {
        ApplinksTypesBackdoor backdoor = new ApplinksTypesBackdoor(ProductInstances.REFAPP1.getBaseUrl());
        for (ApplinkTypeMetadata metadata : backdoor.getApplicationTypeMetadata()) {
            if (metadata.getIconUri() != null) {
                RestAssured.when().get(metadata.getIconUrl()).then().statusCode(withStatus(Response.Status.OK));
            }
            if (metadata.getIconUrl() != null) {
                RestAssured.when().get(metadata.getIconUri()).then().statusCode(withStatus(Response.Status.OK));
            }
        }
    }

    @Test
    public void allEntityIconsShouldExist() {
        ApplinksTypesBackdoor backdoor = new ApplinksTypesBackdoor(ProductInstances.REFAPP1.getBaseUrl());
        for (ApplinkTypeMetadata metadata : backdoor.getEntityTypeMetadata()) {
            if (metadata.getIconUri() != null) {
                RestAssured.when().get(metadata.getIconUri()).then().statusCode(withStatus(Response.Status.OK));
            }
            if (metadata.getIconUrl() != null) {
                RestAssured.when().get(metadata.getIconUrl()).then().statusCode(withStatus(Response.Status.OK));
            }
        }
    }

}
