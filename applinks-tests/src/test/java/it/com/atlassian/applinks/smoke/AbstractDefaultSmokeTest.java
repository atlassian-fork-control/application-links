package it.com.atlassian.applinks.smoke;

import it.com.atlassian.applinks.testing.product.GenericTestedProduct;

import javax.annotation.Nonnull;

/**
 * Base class for default smoke tests that support all products, which means the {@code mainInstance} is of type
 * {@link GenericTestedProduct}.
 *
 * @see AbstractApplinksSmokeTest
 * @since 4.3
 */
public abstract class AbstractDefaultSmokeTest extends AbstractApplinksSmokeTest<GenericTestedProduct> {
    protected AbstractDefaultSmokeTest(@Nonnull GenericTestedProduct mainProduct,
                                       @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
    }
}
