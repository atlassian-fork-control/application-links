package it.com.atlassian.applinks.refapp_refapp.v3;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.webdriver.applinks.component.v3.ApplinkRow;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;

/**
 * Browser tests for special cases when the remote app is version 5.x and requires authentication to retrieve status.
 */
@Category(RefappTest.class)
public class V3UiV25xCompatibilityTest extends AbstractApplinksTest {
    // Refapp2 setup rules to act as V2 5.x:
    //  - disable status API by default
    @ClassRule
    public static final ApplinksCapabilitiesRule REFAPP2_CAPABILITIES = new ApplinksCapabilitiesRule(REFAPP2)
            .withDisabledCapabilities(ApplinksCapabilities.STATUS_API);

    @Rule
    public final TestApplinkRule testApplink = new TestApplinkRule.Builder(INSTANCE1, INSTANCE2).build();

    @Before
    public void logInToRefapp2() {
        // this is required because of REFAPP-480 (redirecting login when OAuth dancing breaks)
        loginAsSysadmin(PRODUCT2);
    }

    @Test
    public void statusRequiresRemoteAuthentication() {
        testApplink.configure(enableDefaultOAuth());

        ApplinkRow row = goToV3Ui().getApplinkRow(testApplink.from().applicationId());
        waitUntilTrue(row.getStatus().isRemoteAuthenticationRequired());
        row.getStatus().startRemoteAuthentication().confirmHandlingWebLoginIfRequired(TestAuthentication.admin());

        row = bindV3Ui().getApplinkRow(testApplink.from().applicationId());
        waitUntilTrue(row.getStatus().isWorking());
    }

    private V3ListApplicationLinksPage goToV3Ui() {
        V3ListApplicationLinksPage page = loginAsSysadminAndGoTo(PRODUCT,
                V3ListApplicationLinksPage.class);
        waitUntil(page.getApplinksRows(), Matchers.<ApplinkRow>iterableWithSize(1));
        waitUntilTrue(page.getApplinkRow(testApplink.from().applicationId()).hasVersion());
        return page;
    }

    private V3ListApplicationLinksPage bindV3Ui() {
        return PRODUCT.getPageBinder().bind(V3ListApplicationLinksPage.class);
    }
}
