package it.com.atlassian.applinks.rest.oauth;


import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator;
import com.atlassian.applinks.test.rest.oauth.ConsumerTokenRequest;
import com.atlassian.applinks.test.rest.oauth.ConsumerTokenRestTester;
import it.com.atlassian.applinks.testing.ApplinksAuthentications;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import javax.ws.rs.core.Response;

import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 * REST tests for ConsumerTokenResource with two linked products.
 *
 * @see com.atlassian.applinks.oauth.rest.ConsumerTokenResource
 */
@Category(RefappTest.class)
public class ConsumerTokenResourceTest {
    private static final TestAuthentication DEFAULT_AUTH_ADMIN = ApplinksAuthentications.REFAPP_ADMIN_BETTY;
    private static final String INVALID_APPLINK_ID = "abcd12345";

    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);

    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    private final ConsumerTokenRestTester refapp1statusTester = new ConsumerTokenRestTester(REFAPP1);

    // Default OAuth tests
    @Test
    public void shouldRemoveConsumerTokenForValidAppIdAndUser() {
        applink.configure(OAuthApplinkConfigurator.enableDefaultOAuth());
        deleteConsumerTokenForAuthenticatedUser(applink.from().id(), NO_CONTENT);
    }

    @Test
    public void shouldFailToRemoveConsumerTokenForAnonymousUser() {
        applink.configure(OAuthApplinkConfigurator.enableDefaultOAuth());
        deleteConsumerTokenForAnonymousUser(applink.from().id(), UNAUTHORIZED);
    }

    @Test
    public void shouldFailToRemoveConsumerTokenForInvalidAppId() {
        applink.configure(OAuthApplinkConfigurator.enableDefaultOAuth());
        deleteConsumerTokenForAuthenticatedUser(INVALID_APPLINK_ID, BAD_REQUEST);
    }

    // 2LOi tests
    @Test
    public void shouldRemoveConsumerTokenFor2LOi() {
        applink.configure(OAuthApplinkConfigurator.enableOAuthWithImpersonation());
        deleteConsumerTokenForAuthenticatedUser(applink.from().id(), NO_CONTENT);
    }

    @Test
    public void shouldFailToRemoveConsumerTokenForAnonymousUserAnd2LOi() {
        applink.configure(OAuthApplinkConfigurator.enableOAuthWithImpersonation());
        deleteConsumerTokenForAnonymousUser(applink.from().id(), UNAUTHORIZED);
    }

    @Test
    public void shouldFailToRemoveConsumerTokenForInvalidAppIdAnd2LOi() {
        applink.configure(OAuthApplinkConfigurator.enableOAuthWithImpersonation());
        deleteConsumerTokenForAuthenticatedUser(INVALID_APPLINK_ID, BAD_REQUEST);
    }

    // 3LO tests
    @Test
    public void shouldRemoveConsumerTokenFor3LO() {
        applink.configure(OAuthApplinkConfigurator.enable3LoOnly());
        deleteConsumerTokenForAuthenticatedUser(applink.from().id(), NO_CONTENT);
    }

    @Test
    public void shouldFailToRemoveConsumerTokenForInvalidAppIdAnd3LO() {
        applink.configure(OAuthApplinkConfigurator.enable3LoOnly());
        deleteConsumerTokenForAuthenticatedUser(INVALID_APPLINK_ID, BAD_REQUEST);
    }

    @Test
    public void shouldFailToRemoveConsumerTokenForAnonymousUserAnd3LO() {
        applink.configure(OAuthApplinkConfigurator.enable3LoOnly());
        deleteConsumerTokenForAnonymousUser(applink.from().id(), UNAUTHORIZED);
    }

    private void deleteConsumerTokenForAnonymousUser(String appLinkId, Response.Status expectedStatus) {
        refapp1statusTester.delete(new ConsumerTokenRequest.Builder(appLinkId)
                .expectStatus(expectedStatus)
                .build());
    }

    private void deleteConsumerTokenForAuthenticatedUser(String appLinkId, Response.Status expectedStatus) {
        refapp1statusTester.delete(new ConsumerTokenRequest.Builder(appLinkId)
                .authentication(DEFAULT_AUTH_ADMIN)
                .expectStatus(expectedStatus)
                .build());
    }
}
