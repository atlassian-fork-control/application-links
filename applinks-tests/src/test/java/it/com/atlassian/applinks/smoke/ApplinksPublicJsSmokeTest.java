package it.com.atlassian.applinks.smoke;

import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.applinks.page.test.WebResourcesTestPage;
import com.google.common.collect.ImmutableSet;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.IgnoredProducts;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.lang.String.format;

/**
 * This tests selected JS resources known to be consumed by plugins in various products and therefore in need to work
 * outside of Applinks page scope. Those can be easily broken outside of that scope e.g. by misconfigured dependencies
 * in the plugin descriptor.
 */
@Category({SmokeTest.class})
@IgnoredProducts(value = SupportedProducts.FECRU, reason = "Fecru fails to load web resources intermittently when running this test")
public class ApplinksPublicJsSmokeTest extends AbstractDefaultSmokeTest {
    @Rule
    public final ApplinksFeaturesRule applinksFeaturesRule;

    public ApplinksPublicJsSmokeTest(@Nonnull GenericTestedProduct mainProduct,
                                     @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        this.applinksFeaturesRule = new ApplinksFeaturesRule(mainInstance);
    }

    @Test
    public void applinksUtilJsShouldWorkOutsideOfApplinksPage() {
        WebResourcesTestPage testPage = testWebResource("com.atlassian.applinks.applinks-plugin:applinks-util-js");
        // applinks-util-js should define the AppLinks.UI and AppLinks SPI objects
        assertDefined(testPage, "AppLinks.UI");
        assertDefined(testPage, "AppLinks.SPI");
    }

    @Test
    public void applinksOAuthUiShouldWorkOutsideOfApplinksPage() {
        WebResourcesTestPage testPage = testWebResource("com.atlassian.applinks.applinks-plugin:applinks-oauth-ui");
        // applinks-oauth-ui should define the AppLinks.authenticateRemoteCredentials function
        assertType(testPage, "AppLinks.authenticateRemoteCredentials", "function");
    }

    @Test
    public void applinksPublicJsShouldWorkOutsideOfApplinksPage() {
        WebResourcesTestPage testPage = testWebResource("com.atlassian.applinks.applinks-plugin:applinks-public");
        // applinks-publix should define the ApplinksUtils object with 2 public functions
        assertType(testPage, "ApplinksUtils.createAuthRequestBanner", "function");
        assertType(testPage, "ApplinksUtils.createAuthRequestInline", "function");
    }

    private WebResourcesTestPage testWebResource(String webResourceKey) {
        return mainProduct.visit(WebResourcesTestPage.class, ImmutableSet.of(webResourceKey));
    }

    private void assertDefined(WebResourcesTestPage testPage, String global) {
        TimedQuery<Boolean> result = testPage.javascript().executeTimed(Boolean.class,
                format("return typeof %s !== 'undefined'", global));
        waitUntilTrue(global + " variable expected to be defined", result);
    }

    private void assertType(WebResourcesTestPage testPage, String global, String expectedType) {
        TimedQuery<String> result = testPage.javascript().executeTimed(String.class,
                format("return typeof %s", global));
        waitUntilEquals(global + " variable expected to be " + expectedType, expectedType, result);
    }
}
