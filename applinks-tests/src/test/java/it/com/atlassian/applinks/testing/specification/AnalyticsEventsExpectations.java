package it.com.atlassian.applinks.testing.specification;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.applinks.test.rest.matchers.BaseRestMatcher;
import com.google.common.collect.ImmutableList;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import java.util.Arrays;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;
import static junit.framework.TestCase.assertNotNull;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

/**
 * @since 4.3
 */
public final class AnalyticsEventsExpectations {
    public static final String EVENTS_LIST_FIELD = "events";

    public static final String PROPERTY_EVENT_NAME = "name";
    public static final String PROPERTY_EVENT_REMOVED = "removed";
    public static final String PROPERTY_EVENT_PROPERTIES = "properties";

    private AnalyticsEventsExpectations() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    /**
     * Expect that log will contain a retained event (i.e. removed==false) with {@code expectedEventName}.
     *
     * @param expectedEventName expected event name
     * @return response specification
     */
    @Nonnull
    public static ResponseSpecification expectRetainedEvent(@Nullable String expectedEventName) {
        return expectEvent(allOf(eventWithName(expectedEventName), retainedEvent()));
    }

    /**
     * Expect that log will contain a retained event (i.e. removed==false) with {@code expectedEvent}.
     *
     * @param expectedEvent expected class of the event
     * @return response specification
     */
    @SuppressWarnings("unchecked")
    @Nonnull
    public static ResponseSpecification expectRetainedEvent(@Nonnull Class<?> expectedEvent, Matcher<?>... extraMatchers) {
        Iterable allMatchers = ImmutableList.builder().add(eventWithName(getEventName(expectedEvent))).add(retainedEvent()).addAll(Arrays.asList(extraMatchers)).build();
        return expectEvent(allOf(allMatchers));
    }

    /**
     * Expect that log will contain a retained event (i.e. removed==false) with {@code expectedEvent}.
     *
     * @param expectedEvent expected name of the event
     * @param extraMatchers additional matchers to match
     * @return response specification
     */
    @SuppressWarnings("unchecked")
    @Nonnull
    public static ResponseSpecification expectRetainedEvent(String expectedEvent, Matcher<?>... extraMatchers) {
        Iterable allMatchers = ImmutableList.builder().add(eventWithName(expectedEvent)).add(retainedEvent()).addAll(Arrays.asList(extraMatchers)).build();
        return expectEvent(allOf(allMatchers));
    }


    /**
     * Expect that log will contain a retained event (i.e. removed==false) with {@code expectedEvent}.
     *
     * @param expectedEvent expected class of the event
     * @return response specification
     */
    @Nonnull
    public static ResponseSpecification expectRetainedEvent(@Nonnull Class<?> expectedEvent) {
        return expectEvent(allOf(eventWithName(getEventName(expectedEvent)), retainedEvent()));
    }

    /**
     * Expect that log will contain an event matching {@code eventMatcher}. Use one of the provided matchers, or
     * extend {@link BaseRestMatcher} for a custom matcher.
     *
     * @param eventMatcher for the expected event
     * @return response specification
     */
    @Nonnull
    @SuppressWarnings("unchecked")
    public static ResponseSpecification expectEvent(@Nullable Matcher<?> eventMatcher) {
        return RestAssured.expect().body(EVENTS_LIST_FIELD, Matchers.hasItem((Matcher) eventMatcher));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> eventWithName(@Nullable String expectedName) {
        return eventWithNameThat(is(expectedName));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> eventWithProperty(@Nonnull String expectedName, @Nullable String expectedValue) {
        return eventWithPropertyThat(expectedName, is(expectedValue));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> eventWithNameThat(@Nonnull Matcher<String> nameMatcher) {
        return new BaseRestMatcher(PROPERTY_EVENT_NAME, requireNonNull(nameMatcher, "nameMatcher"));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> eventWithPropertyThat(@Nonnull String propertyName, @Nonnull Matcher<String> propertyValueMatcher) {
        return new BaseRestMatcher(PROPERTY_EVENT_PROPERTIES, new BaseRestMatcher(propertyName, requireNonNull(propertyValueMatcher, "propertyMatcher")));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> removedEvent() {
        return eventWithRemovedThat(is(true));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> retainedEvent() {
        return eventWithRemovedThat(is(false));
    }

    @Nonnull
    private static Matcher<Map<String, Object>> eventWithRemovedThat(@Nonnull Matcher<Boolean> removedMatcher) {
        return new BaseRestMatcher(PROPERTY_EVENT_REMOVED, requireNonNull(removedMatcher, "removedMatcher"));
    }

    private static String getEventName(Class<?> eventClass) {
        EventName eventName = eventClass.getAnnotation(EventName.class);
        assertNotNull("Missing EventName annotation for class " + eventClass.getName(), eventName);
        return eventName.value();
    }

    // add more expectations and matchers as needed
}
