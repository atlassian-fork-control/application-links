package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.client.FeatureDiscoveryClient;
import com.atlassian.applinks.test.rest.backdoor.SalBackdoor;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;
import org.junit.rules.RuleChain;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;

/**
 * @since 5.0
 */
public class DefaultSettingsRule extends TestWatcher {
    @Nonnull
    public static RuleChain forProducts(@Nonnull TestedInstance... products) {
        checkNotNull(products, "products");
        return forProducts(asList(products));
    }

    @Nonnull
    public static RuleChain forProducts(@Nonnull Iterable<TestedInstance> products) {
        checkNotNull(products, "products");
        RuleChain ruleChain = RuleChain.emptyRuleChain();
        for (TestedInstance productInstance : products) {
            ruleChain = ruleChain.around(new DefaultSettingsRule(productInstance));
        }
        return ruleChain;
    }

    private final FeatureDiscoveryClient featureDiscoveryClient;
    private final SalBackdoor salBackdoor;

    public DefaultSettingsRule(TestedInstance productInstance) {
        checkNotNull(productInstance, "productInstance");
        this.featureDiscoveryClient = new FeatureDiscoveryClient(productInstance);
        this.salBackdoor = new SalBackdoor(productInstance);
    }

    @Override
    protected void starting(Description description) {
        this.featureDiscoveryClient.discoverFeature(ApplinksFeatures.V3_UI.name());
    }

    @Override
    protected void finished(Description description) {
        salBackdoor.cleanUpUserSettings(TestAuthentication.admin());
    }

}
