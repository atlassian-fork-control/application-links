package it.com.atlassian.applinks.testing.rules;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;

import com.atlassian.pageobjects.TestedProduct;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Abstract Rule with common functionality for linking products.
 *
 * @since 4.0.15
 */
public abstract class AbstractProductLinkingRule extends TestWatcher {
    /**
     * Get a list of Products other than the on e of interest.
     */
    protected static Iterable<TestedProduct<?>> getOtherProducts(final TestedProduct<?> productOfInterest, final TestedProduct<?>[] allProducts) {
        return Iterables.filter(Lists.newArrayList(allProducts), getOtherProductsFilter(productOfInterest));
    }

    /**
     * Filter to remove the current localProduct from the lits of all products..
     */
    protected static Predicate<TestedProduct<?>> getOtherProductsFilter(final TestedProduct<?> product) {
        return new Predicate<TestedProduct<?>>() {
            @Override
            public boolean apply(@Nullable final TestedProduct<?> input) {
                return !input.equals(product);
            }
        };
    }

    private final Iterable<TestedProduct<?>> remoteProducts;
    private final TestedProduct<?> localProduct;

    @Inject
    public AbstractProductLinkingRule(@Nonnull TestedProduct<?> localProduct, final Iterable<TestedProduct<?>> remoteProducts) {
        this.localProduct = localProduct;
        this.remoteProducts = remoteProducts;
    }

    @Override
    protected void starting(Description description) {
        // link the current localProduct to all remote products
        for (TestedProduct<?> remoteProduct : remoteProducts) {
            // link the current localProduct to the remote localProduct
            createLink(localProduct, remoteProduct);
        }
    }

    protected abstract void createLink(final TestedProduct<?> localProduct, final TestedProduct<?> remoteProduct);
}
