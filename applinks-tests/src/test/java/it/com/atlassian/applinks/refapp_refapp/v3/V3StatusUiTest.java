package it.com.atlassian.applinks.refapp_refapp.v3;

import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.internal.common.rest.model.applink.RestMinimalApplicationLink;
import com.atlassian.applinks.internal.test.application.twitter.TwitterApplicationType;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.backdoor.BackdoorCreateSideApplinkRequest;
import com.atlassian.webdriver.applinks.component.v3.ApplinkRow;
import com.atlassian.webdriver.applinks.component.v3.ApplinkStatusLozenge;
import com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.test.data.applink.config.ApplinkConfigurators.configureSide;
import static com.atlassian.applinks.test.data.util.TestUrls.createRandomUrl;
import static com.atlassian.applinks.test.rest.data.applink.SystemLinkConfigurator.setSystemLink;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown.DELETE_ACTION_ID;
import static com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown.GO_TO_REMOTE_ACTION_ID;
import static com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown.LEGACY_EDIT_ACTION_ID;
import static com.atlassian.webdriver.applinks.component.v3.V3ApplinkActionDropDown.MAKE_PRIMARY_ACTION_ID;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.hamcrest.Matchers.not;

@Category(RefappTest.class)
public class V3StatusUiTest extends AbstractApplinksTest {
    @Rule
    public final TestApplinkRule testApplink = new TestApplinkRule.Builder(INSTANCE1, INSTANCE2)
            .configure(enableDefaultOAuth())
            .build();

    private ApplinksBackdoor applinksBackdoor = new ApplinksBackdoor(REFAPP1);

    @Test
    public void eachRowShouldContainApplicationLinkInformation() {
        V3ListApplicationLinksPage page = loginAndGoToV3Ui();

        waitUntil(page.getApplinksRows(), iterableWithSize(1));
        ApplinkRow row = page.getApplinkRow(testApplink.from());
        RestMinimalApplicationLink restApplink = applinksBackdoor.getApplink(testApplink.from());

        assertThat(row.getName(), equalTo(restApplink.getName()));
        assertThat(row.getRpcUrl(), equalTo("Application URL: " + restApplink.getRpcUrl()));
        assertThat(row.getDisplayUrl(), equalTo("Display URL: " + restApplink.getDisplayUrl()));
        waitUntilTrue(row.isPrimary());
        waitUntilTrue(row.getStatus().isWorking());
    }

    @Test
    public void eachRowShouldContainWorkingAction() {
        // create 2 generic links, first one will be primary
        TestApplink.Side primaryGeneric = applinksBackdoor.createRandomGeneric();
        TestApplink.Side secondGeneric = applinksBackdoor.createRandomGeneric();

        V3ListApplicationLinksPage page = loginAndGoToV3Ui();

        // primary Refapp link should not contain "Make primary"
        ApplinkRow row = page.getApplinkRow(testApplink.from());
        inspectDropdownActions(row.openActionsDropdown(), contains(GO_TO_REMOTE_ACTION_ID, DELETE_ACTION_ID));

        // primary generic link should not contain "Make primary"
        row = page.getApplinkRow(primaryGeneric);
        inspectDropdownActions(row.openActionsDropdown(), contains(GO_TO_REMOTE_ACTION_ID, DELETE_ACTION_ID));

        // non-primary links should contain "Make primary"
        row = page.getApplinkRow(secondGeneric);
        inspectDropdownActions(row.openActionsDropdown(),
                contains(GO_TO_REMOTE_ACTION_ID, MAKE_PRIMARY_ACTION_ID, DELETE_ACTION_ID));
    }

    @Test
    public void rowsForStandardApplinksShouldContainEditLinkThatLinksToNewEditUi() {
        ApplinkRow row = loginAndGoToV3Ui().getApplinkRow(testApplink.from());
        row.edit(); // edit() will fail if edit button is absent, or goes to the wrong screen
    }

    @Test
    public void rowsForNonStandardApplinksShouldContainEditButtonForOpeningOldEditUi() throws Exception {
        TestApplink.Side genericLink = applinksBackdoor.createRandomGeneric();

        ApplinkRow row = loginAndGoToV3Ui().getApplinkRow(genericLink);
        waitUntilTrue(row.getStatus().hasStatus(ApplinkStatusLozenge.ERROR_NON_ATLASSIAN));
        row.legacyEdit(); // legacyEdit() will fail if edit button is absent, or does not open the legacy dialog
    }

    /**
     * The "legacyEdit=true" magic backdoor param should add legacy edit action to V3-editable links
     */
    @Test
    public void shouldHaveLegacyEditActionWhenLegacyEditParamPresent() {
        // generic and system links should not contain legacy edit action
        TestApplink.Side generic = applinksBackdoor.createRandomGeneric();
        TestApplink.Side systemJira = applinksBackdoor.createRandom(JiraApplicationType.class);
        configureSide(systemJira, setSystemLink());

        V3ListApplicationLinksPage page = loginAndGoToV3Ui(true);

        // main Refapp link should contain "Legacy edit"
        inspectDropdownActions(page.getApplinkRow(testApplink.from()).openActionsDropdown(),
                contains(GO_TO_REMOTE_ACTION_ID, LEGACY_EDIT_ACTION_ID, DELETE_ACTION_ID));
        //Reload page to close the dropdown blocking the next action
        page = PRODUCT.visit(V3ListApplicationLinksPage.class, true);

        // generic and system links should not contain "Legacy edit"
        inspectDropdownActions(page.getApplinkRow(generic).openActionsDropdown(),
                not(hasItem(LEGACY_EDIT_ACTION_ID)));

        inspectDropdownActions(page.getApplinkRow(systemJira).openActionsDropdown(),
                not(hasItem(LEGACY_EDIT_ACTION_ID)));
    }

    @Test
    public void shouldBeAbleToDeleteAnApplicationLink() {
        V3ListApplicationLinksPage page = loginAndGoToV3Ui();
        page.getApplinkRow(testApplink.from().applicationId()).delete();
        waitUntilTrue(page.isEmpty());
    }

    @Test
    public void shouldRenderCustomApplinkActions() {
        TestApplink.Side twitterLink = applinksBackdoor.createSide(
                new BackdoorCreateSideApplinkRequest.Builder(createRandomUrl())
                        .name("Twitter link")
                        .type(TwitterApplicationType.class)
                        .build());

        ApplinkRow row = loginAndGoToV3Ui().getApplinkRow(twitterLink);
        waitUntilTrue(row.getStatus().hasStatus(ApplinkStatusLozenge.ERROR_NON_ATLASSIAN));
        // due to Refapp non-standard rendering of web items the i18n key is unresolved (in Refapp only)
        waitUntil(row.openActionsDropdown().getActionNames(), hasItem("applinks-tests.actions.applink.twitter"));
    }

    private void inspectDropdownActions(V3ApplinkActionDropDown dropdown, Matcher test) {
        waitUntilTrue(dropdown.isOpen());
        waitUntil(dropdown.getActionIds(), test);
        dropdown.close();
    }

    private V3ListApplicationLinksPage loginAndGoToV3Ui() {
        return loginAndGoToV3Ui(false);
    }

    private V3ListApplicationLinksPage loginAndGoToV3Ui(boolean withLegacyEdit) {
        V3ListApplicationLinksPage page = loginAsSysadminAndGoTo(PRODUCT, V3ListApplicationLinksPage.class,
                withLegacyEdit);
        waitUntilTrue(page.getApplinkRow(testApplink.from()).hasVersion());

        return page;
    }
}
