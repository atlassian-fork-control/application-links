package it.com.atlassian.applinks.smoke.v3;

import com.atlassian.applinks.internal.common.rest.model.applink.RestMinimalApplicationLink;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.client.ApplinkOAuthStatusClient;
import com.atlassian.webdriver.applinks.component.v3.OAuthArrows;
import com.atlassian.webdriver.applinks.component.v3.RemoteAuthenticationStatus;
import com.atlassian.webdriver.applinks.component.v3.UiOAuthConfig;
import com.atlassian.webdriver.applinks.page.v3.ConnectivityDialog;
import com.atlassian.webdriver.applinks.page.v3.V3EditApplinkPage;
import it.com.atlassian.applinks.smoke.AbstractDefaultSmokeTest;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.pageobjects.LoginClient.loginAsSysadminAndGoTo;
import static com.atlassian.applinks.test.data.util.TestUrls.createRandomUrl;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusImpersonation;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.endsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@Category(SmokeTest.class)
public class ApplinksV3EditSmokeTest extends AbstractDefaultSmokeTest {
    @Rule
    public final TestApplinkRule testApplink;

    private final ApplinksBackdoor applinksBackdoor;

    private V3EditApplinkPage editPage;

    private ApplinkOAuthStatusClient oauthStatusClient = new ApplinkOAuthStatusClient();

    public ApplinksV3EditSmokeTest(@Nonnull GenericTestedProduct mainProduct,
                                   @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        testApplink = new TestApplinkRule.Builder(mainInstance, backgroundInstance)
                .configure(enableDefaultOAuth())
                .name("Test Applink")
                .build();
        applinksBackdoor = new ApplinksBackdoor(mainInstance);
    }

    @Before
    public void loginAndGoToEditScreen() {
        editPage = loginAsSysadminAndGoTo(mainProduct, V3EditApplinkPage.class, testApplink.from());
    }

    @Test
    public void testEditApplinkUrls() {
        assertEquals("Edit - Test Applink", editPage.getHeader());
        waitUntilTrue(editPage.getStatus().isWorking());
        waitUntilFalse(editPage.getStatus().isClickable()); // status non-clickable
        waitUntilFalse(editPage.getStatusDetails().isPresent()); // details should not be present for CONNECTED

        waitUntilEquals(backgroundInstance.getBaseUrl(), editPage.getApplicationUrl());
        waitUntilEquals(backgroundInstance.getBaseUrl(), editPage.getDisplayUrl());

        String applicationUrl = createRandomUrl();
        String displayUrl = createRandomUrl();
        editPage.setApplicationName("New applink name")
                .setApplicationUrl(applicationUrl)
                .setDisplayUrl(displayUrl)
                .submit();

        RestMinimalApplicationLink applink = applinksBackdoor.getApplink(testApplink.from());
        assertEquals("New applink name", applink.getName());
        assertEquals("Application URL mismatch", applicationUrl, applink.getRpcUrl().toString());
        assertEquals("Display URL mismatch", displayUrl, applink.getDisplayUrl().toString());
    }

    @Test
    public void testChangeLocalAuthAndSave() {
        // verify initial local and remote auth first
        waitUntilTrue(editPage.getOutgoingAuthDropdown().isSelected(UiOAuthConfig.OAUTH));
        waitUntilTrue(editPage.getIncomingAuthDropdown().isSelected(UiOAuthConfig.OAUTH));
        verifyRemoteAuth(editPage.getIncomingRemoteAuthStatus(), UiOAuthConfig.OAUTH);
        verifyRemoteAuth(editPage.getOutgoingRemoteAuthStatus(), UiOAuthConfig.OAUTH);
        assertArrowImages(editPage, OAuthArrows.RIGHT_MATCH, OAuthArrows.LEFT_MATCH);

        // update local auth
        editPage.getOutgoingAuthDropdown().select(UiOAuthConfig.OAUTH_IMPERSONATION);

        // The connectivity dialog when open seems to interfere with assertions for the connectivity arrows and thus introduces flakiness,therefore we dismiss it.
        editPage.getOutgoingConnectivityDialog().dismiss();

        editPage.getIncomingAuthDropdown().select(UiOAuthConfig.OAUTH_IMPERSONATION);
        editPage.getIncomingConnectivityDialog().dismiss();

        // should change to mismatch
        assertArrowImages(editPage, OAuthArrows.RIGHT_MISMATCH, OAuthArrows.LEFT_MISMATCH);

        editPage.submit();

        // make sure OAuth impersonation was saved in the server
        ApplinkOAuthStatus oAuthStatus = oauthStatusClient.getOAuthStatus(testApplink.from());
        assertThat(oAuthStatus, oAuthStatusImpersonation());
    }

    private static void verifyRemoteAuth(RemoteAuthenticationStatus remoteAuth, UiOAuthConfig expectedConfig) {
        waitUntilTrue(remoteAuth.hasApplicationIcon());
        waitUntilTrue(remoteAuth.hasRemoteLink());
        waitUntilTrue(remoteAuth.hasOAuthConfig());
        waitUntilEquals(expectedConfig, remoteAuth.getOAuthConfig());
        waitUntilFalse(remoteAuth.hasTroubleshootingLink()); // no error - no troubleshooting link
    }

    private static void assertArrowImages(V3EditApplinkPage editApplinkPage, OAuthArrows rightArrow, OAuthArrows leftArrow) {
        waitUntil(editApplinkPage.getOutgoingStatusImageSrc(), endsWith(rightArrow.image()));
        waitUntil(editApplinkPage.getIncomingStatusImageSrc(), endsWith(leftArrow.image()));
    }
}
