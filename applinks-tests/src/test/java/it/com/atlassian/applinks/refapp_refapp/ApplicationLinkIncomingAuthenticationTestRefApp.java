package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.webdriver.applinks.component.OauthIncomingAuthenticationSection;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category(RefappTest.class)
public class ApplicationLinkIncomingAuthenticationTestRefApp extends V2AbstractApplinksTest {
    private static final String VALID_CERTIFICATE = "-----BEGIN CERTIFICATE-----\nMIIDBDCCAm2gAwIBAgIJAK8dGINfkSTHMA0GCSqGSIb3DQEBBQUAMGAxCzAJBgNV\nBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzETMBEG\nA1UEChMKR29vZ2xlIEluYzEXMBUGA1UEAxMOd3d3Lmdvb2dsZS5jb20wHhcNMDgx\nMDA4MDEwODMyWhcNMDkxMDA4MDEwODMyWjBgMQswCQYDVQQGEwJVUzELMAkGA1UE\nCBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxEzARBgNVBAoTCkdvb2dsZSBJ\nbmMxFzAVBgNVBAMTDnd3dy5nb29nbGUuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GN\nADCBiQKBgQDQUV7ukIfIixbokHONGMW9+ed0E9X4m99I8upPQp3iAtqIvWs7XCbA\nbGqzQH1qX9Y00hrQ5RRQj8OI3tRiQs/KfzGWOdvLpIk5oXpdT58tg4FlYh5fbhIo\nVoVn4GvtSjKmJFsoM8NRtEJHL1aWd++dXzkQjEsNcBXwQvfDb0YnbQIDAQABo4HF\nMIHCMB0GA1UdDgQWBBSm/h1pNY91bNfW08ac9riYzs3cxzCBkgYDVR0jBIGKMIGH\ngBSm/h1pNY91bNfW08ac9riYzs3cx6FkpGIwYDELMAkGA1UEBhMCVVMxCzAJBgNV\nBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRMwEQYDVQQKEwpHb29nbGUg\nSW5jMRcwFQYDVQQDEw53d3cuZ29vZ2xlLmNvbYIJAK8dGINfkSTHMAwGA1UdEwQF\nMAMBAf8wDQYJKoZIhvcNAQEFBQADgYEAYpHTr3vQNsHHHUm4MkYcDB20a5KvcFoX\ngCcYtmdyd8rh/FKeZm2me7eQCXgBfJqQ4dvVLJ4LgIQiU3R5ZDe0WbW7rJ3M9ADQ\nFyQoRJP8OIMYW3BoMi0Z4E730KSLRh6kfLq4rK6vw7lkH9oynaHHWZSJLDAp17cP\nj+6znWkN9/g=\n-----END CERTIFICATE-----";
    protected static final String URL = "http://localhost:81";
    private static final String NAME = "Test1";

    @Test
    public void manualOAuthIncomingAuthenticationCanBeDeleted() throws Exception {
        loginAsSysadmin(PRODUCT);
        OauthIncomingAuthenticationSection section = setupLinkAndFillEssentialOAuthDetails().save();

        waitUntilTrue(section.isConfiguredTimed());
        section = section.delete();
        waitUntilTrue(section.isNotConfiguredTimed());
    }

    @Test
    public void twoLeggedOAuthCanBeEnabled() throws Exception {
        loginAsSysadmin(PRODUCT);
        OauthIncomingAuthenticationSection section = setupLinkAndFillEssentialOAuthDetails();

        assertTrue(section.isTwoLOSectionPresent());

        section.setTwoLOAllowed(true)
                .setExecutingTwoLOUser(ADMIN_USERNAME)
                .save();

        section = PRODUCT.visit(ListApplicationLinkPage.class).configureApplicationLink(URL).openIncomingOauth();

        // the save must be successful
        waitUntilTrue(section.isConfiguredTimed());
        waitUntilTrue(section.isTwoLOAllowedTimed());
        // TODO introduce timeouts!
        assertEquals(ADMIN_USERNAME, section.getExecutingTwoLOUser());
        assertFalse(section.getTwoLOImpersonationAllowed());
    }

    @Test
    public void twoLeggedOAuthCannotBeEnabledIfUserIsNotSupplied() throws Exception {
        loginAsSysadmin(PRODUCT);
        OauthIncomingAuthenticationSection section = setupLinkAndFillEssentialOAuthDetails()
                .setTwoLOAllowed(true)
                .setExecutingTwoLOUser("")
                .save();

        // the save must be unsuccessful
        section.ensureExecutingTwoLOUserErrorVisible();
        waitUntilTrue(section.isNotConfiguredTimed());
    }

    @Test
    public void twoLeggedOAuthCannotBeEnabledIfUserIsNotValid() throws Exception {
        loginAsSysadmin(PRODUCT);
        OauthIncomingAuthenticationSection section = setupLinkAndFillEssentialOAuthDetails()
                .setTwoLOAllowed(true)
                .setExecutingTwoLOUser("this_user_does_not_exist")
                .save();

        // the save must be unsuccessful
        section.ensureExecutingTwoLOUserErrorVisible();
        waitUntilTrue(section.isNotConfiguredTimed());
    }

    @Test
    public void twoLeggedOAuthCanBeEnabledIfUserIsEmpty() throws Exception {
        loginAsSysadmin(PRODUCT);
        OauthIncomingAuthenticationSection section = setupLinkAndFillEssentialOAuthDetails()
                .setTwoLOAllowed(true)
                .setExecutingTwoLOUser("")
                .save();

        // the save must be unsuccessful
        section.ensureExecutingTwoLOUserErrorVisible();
        waitUntilTrue(section.isNotConfiguredTimed());
    }

    @Test
    public void twoLeggedOAuthWithImpersonationCanBeEnabled() throws Exception {
        loginAsSysadmin(PRODUCT);
        setupLinkAndFillEssentialOAuthDetails()
                .setTwoLOAllowed(true)
                .setExecutingTwoLOUser(ADMIN_USERNAME)
                .setTwoLOImpersonationAllowed(true)
                .save();

        OauthIncomingAuthenticationSection section = PRODUCT.visit(ListApplicationLinkPage.class).configureApplicationLink(URL).openIncomingOauth();

        // the save must be successful
        waitUntilTrue(section.isConfiguredTimed());
        waitUntilTrue(section.isTwoLOAllowedTimed());
        // TODO introduce timeouts!
        assertEquals(ADMIN_USERNAME, section.getExecutingTwoLOUser());
        assertTrue(section.getTwoLOImpersonationAllowed());
    }

    @Test
    public void twoLeggedOAuthSectionIsNotVisibleToNonSysadmin() throws Exception {
        login(PRODUCT);
        OauthIncomingAuthenticationSection section = setupLinkAndFillEssentialOAuthDetails();
        assertFalse(section.isTwoLOSectionPresent());
    }

    private OauthIncomingAuthenticationSection setupLinkAndFillEssentialOAuthDetails() throws Exception {
        // This is merely a url that is not a valid atlassian product so that manifest cannot be retrieved
        // and thus the configuration falls back to generic mode.
        String url = "http://localhost:81";
        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, URL, NAME);
        final ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        return listApplicationLinkPage.configureApplicationLink(url)
                .openIncomingOauth()
                .setConsumerKey("TEST")
                .setConsumerName("TEST")
                .setPublicKey(VALID_CERTIFICATE);
    }
}
