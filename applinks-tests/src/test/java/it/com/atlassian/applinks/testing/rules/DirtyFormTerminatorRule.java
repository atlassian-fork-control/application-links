package it.com.atlassian.applinks.testing.rules;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.testing.rule.FailsafeExternalResource;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;

import static java.util.Arrays.asList;

/**
 * Closes all that awesome dirty warning alerts left when tests are failing.
 */
public class DirtyFormTerminatorRule extends FailsafeExternalResource {

    private final Iterable<TestedProduct<WebDriverTester>> products;

    public DirtyFormTerminatorRule(Iterable<TestedProduct<WebDriverTester>> products) {
        this.products = products;
    }

    public DirtyFormTerminatorRule(TestedProduct<WebDriverTester>... products) {
        this(asList(products));
    }

    @Override
    protected void before() throws Throwable {
        killThemAll();
    }

    @Override
    protected void after() {
        killThemAll();
    }

    private void killThemAll() {
        for (TestedProduct<WebDriverTester> product : products) {
            final WebDriver driver = product.getTester().getDriver();
            try {
                driver.switchTo().alert().dismiss();
            } catch (NoAlertPresentException ignoreNoAlert) {
            }
        }
    }
}
