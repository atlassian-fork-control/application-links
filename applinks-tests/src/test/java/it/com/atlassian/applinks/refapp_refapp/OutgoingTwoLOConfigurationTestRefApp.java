package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.webdriver.applinks.component.AppLinkAdminLogin;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.component.OauthOutgoingAuthenticationWithAutoConfigSection;
import com.atlassian.webdriver.applinks.externalcomponent.WebSudoPage;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

@Category(RefappTest.class)
public class OutgoingTwoLOConfigurationTestRefApp extends V2AbstractApplinksTest {
    @Test
    public void testEnableAndDisableOutgoing2LO() {
        ApplicationDetailsSection section = loginAsSysadminAndCreateLink();
        section.openOutgoingOauthExpectingAutoConfig();
        handleLoginScreen();
        OauthOutgoingAuthenticationWithAutoConfigSection oauthConfig = PRODUCT.getPageBinder().bind(OauthOutgoingAuthenticationWithAutoConfigSection.class);

        // v2 workflow it is enabled already

        // now disable it
        oauthConfig.uncheckOutgoing2LO();
        oauthConfig.clickUpdate2LOConfig();
        assertTrue(oauthConfig.isOutgoing2LOUpdateMessagePresent());
        assertFalse(oauthConfig.isDisablingOutgoing2LOSuggestionMessagePresent());

        // when we come back to this screen, the config should still hold
        oauthConfig = section.openOutgoingOauthExpectingAutoConfig();
        assertFalse(oauthConfig.isOutgoing2LOEnabled());
        assertFalse(oauthConfig.isDisablingOutgoing2LOSuggestionMessagePresent());

        // now enable it again
        oauthConfig.checkOutgoing2LO();
        oauthConfig.clickUpdate2LOConfig();
        assertTrue(oauthConfig.isOutgoing2LOUpdateMessagePresent());

        // in v2 workflow 2LO is already on at the other end.
        assertFalse(oauthConfig.isDisablingOutgoing2LOSuggestionMessagePresent());

        // when we come back to this screen, the config should still hold
        oauthConfig = section.openOutgoingOauthExpectingAutoConfig();
        assertTrue(oauthConfig.isOutgoing2LOEnabled());
        assertFalse(oauthConfig.isDisablingOutgoing2LOSuggestionMessagePresent());
    }

    /**
     * @since 4.0.15
     */
    @Test
    public void testEnableAndDisableOutgoing2LOi() {
        ApplicationDetailsSection section = loginAsSysadminAndCreateLink();
        section.openOutgoingOauthExpectingAutoConfig();
        handleLoginScreen();
        OauthOutgoingAuthenticationWithAutoConfigSection oauthConfig = PRODUCT.getPageBinder().bind(OauthOutgoingAuthenticationWithAutoConfigSection.class);

        // v2 workflow default 2LOi  is disabled

        // enable it
        oauthConfig.checkOutgoing2LOi();
        oauthConfig.clickUpdate2LOConfig();
        assertTrue(oauthConfig.isOutgoing2LOUpdateMessagePresent());
        assertTrue(oauthConfig.isDisablingOutgoing2LOiSuggestionMessagePresent());

        // now disable it
        oauthConfig.uncheckOutgoing2LOi();
        oauthConfig.clickUpdate2LOConfig();
        assertTrue(oauthConfig.isOutgoing2LOUpdateMessagePresent());
        assertFalse(oauthConfig.isDisablingOutgoing2LOiSuggestionMessagePresent());

        // when we come back to this screen, the config should still hold
        oauthConfig = section.openOutgoingOauthExpectingAutoConfig();
        assertFalse(oauthConfig.isOutgoing2LOiEnabled());
        assertFalse(oauthConfig.isDisablingOutgoing2LOiSuggestionMessagePresent());

        // now enable it again
        oauthConfig.checkOutgoing2LOi();
        oauthConfig.clickUpdate2LOConfig();
        assertTrue(oauthConfig.isOutgoing2LOUpdateMessagePresent());

        // in v2 workflow default 2LOi is already on at the other end.
        assertTrue(oauthConfig.isDisablingOutgoing2LOiSuggestionMessagePresent());

        // when we come back to this screen, the config should still hold
        oauthConfig = section.openOutgoingOauthExpectingAutoConfig();
        assertTrue(oauthConfig.isOutgoing2LOiEnabled());
        assertTrue(oauthConfig.isDisablingOutgoing2LOiSuggestionMessagePresent());
    }

    @Test
    public void testSuggestionToEnableOutgoing2LOMustShow() {
        loginAsSysadminAndCreateLink();

        // now look at the outgoing config of the local instance.
        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        ApplicationDetailsSection section = linkPage.configureApplicationLink(linkPage.getApplicationLinks().get(0).getApplicationUrl());
        section.openOutgoingOauthExpectingAutoConfig();
        handleLoginScreen();
        OauthOutgoingAuthenticationWithAutoConfigSection outgoingOAuthConfig = PRODUCT.getPageBinder().bind(OauthOutgoingAuthenticationWithAutoConfigSection.class);
        assertFalse(outgoingOAuthConfig.isEnablingOutgoing2LOSuggestionMessagePresent());

        // after it's turned off, the suggestion message should appear.
        outgoingOAuthConfig.uncheckOutgoing2LO();
        outgoingOAuthConfig.clickUpdate2LOConfig();
        assertTrue(outgoingOAuthConfig.isEnablingOutgoing2LOSuggestionMessagePresent());
    }

    private void handleLoginScreen() {
        AppLinkAdminLogin<WebSudoPage> adminLogin = PRODUCT2.getPageBinder().bind(AppLinkAdminLogin.class, new WebSudoPage());
        adminLogin.handleWebLoginIfRequired(SYSADMIN_USERNAME, SYSADMIN_PASSWORD);
        WebSudoPage webSudo = PRODUCT2.getPageBinder().bind(WebSudoPage.class);
        webSudo.handleIfRequired(SYSADMIN_PASSWORD);
    }

    private ApplicationDetailsSection loginAsSysadminAndCreateLink() {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        OAuthApplinksClient.createReciprocal3LO2LOLinkViaRest(PRODUCT, PRODUCT2);

        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);
        ListApplicationLinkPage.ApplicationLinkEntryRow newLink = page.getApplicationLinks().get(0);
        return page.configureApplicationLink(newLink.getApplicationUrl());
    }
}
