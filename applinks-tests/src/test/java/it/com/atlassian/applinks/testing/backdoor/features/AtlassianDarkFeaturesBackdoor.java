package it.com.atlassian.applinks.testing.backdoor.features;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.AbstractRestRequest;
import com.atlassian.applinks.test.rest.BaseRestTester;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrls.REST_PATH;
import static com.atlassian.applinks.test.rest.url.ApplinksRestUrls.VERSION_LATEST;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.core.Is.is;

/**
 * Backdoor for the Atlassian dark features REST resource. Currently only supports the site features
 *
 * @since 5.0
 */
public class AtlassianDarkFeaturesBackdoor {
    private final BaseRestTester siteDarkFeaturesTester;

    public AtlassianDarkFeaturesBackdoor(@Nonnull TestedInstance product) {
        this.siteDarkFeaturesTester = new BaseRestTester(darkFeaturesPath(product), TestAuthentication.admin());
    }

    public boolean isEnabled(@Nonnull String featureKey) {
        return siteDarkFeaturesTester.get(DarkFeaturesRequest.isEnabled(featureKey)).statusCode() == OK.getStatusCode();
    }

    public void enable(@Nonnull String featureKey) {
        siteDarkFeaturesTester.put(DarkFeaturesRequest.enable(featureKey));
    }

    public void disable(@Nonnull String featureKey) {
        siteDarkFeaturesTester.delete(DarkFeaturesRequest.disable(featureKey));
    }

    private static RestUrl darkFeaturesPath(@Nonnull TestedInstance product) {
        return RestUrl.forPath(product.getBaseUrl())
                .add(REST_PATH)
                .add("dark-features")
                .add(VERSION_LATEST)
                .add("site");
    }

    public static final class DarkFeaturesRequest extends AbstractRestRequest {
        public static DarkFeaturesRequest isEnabled(@Nonnull String featureKey) {
            return new Builder(featureKey).expectStatus(anyOf(is(NOT_FOUND), is(OK))).build();
        }

        public static DarkFeaturesRequest enable(@Nonnull String featureKey) {
            return new Builder(featureKey).expectStatus(Status.CREATED).build();
        }

        public static DarkFeaturesRequest disable(@Nonnull String featureKey) {
            return new Builder(featureKey).expectStatus(Status.NO_CONTENT).build();
        }

        private final String featureKey;

        private DarkFeaturesRequest(@Nonnull Builder builder) {
            super(builder);
            this.featureKey = builder.featureKey;
        }

        @Nonnull
        @Override
        protected RestUrl getPath() {
            return RestUrl.forPath(featureKey);
        }

        static class Builder extends AbstractBuilder<Builder, DarkFeaturesRequest> {
            private final String featureKey;

            Builder(@Nonnull String featureKey) {
                this.featureKey = checkNotNull(featureKey, "featureKey");
            }

            @Nonnull
            @Override
            public DarkFeaturesRequest build() {
                return new DarkFeaturesRequest(this);
            }

            @Nonnull
            @Override
            protected Builder self() {
                return this;
            }
        }
    }
}
