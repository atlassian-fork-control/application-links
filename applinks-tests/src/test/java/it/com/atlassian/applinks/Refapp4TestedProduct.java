package it.com.atlassian.applinks;

import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.refapp.RefappTestedProduct;

@Defaults(instanceId = "refapp4", contextPath = "/refapp", httpPort = 5994)
public class Refapp4TestedProduct extends RefappTestedProduct {
    public Refapp4TestedProduct(TestedProductFactory.TesterFactory<WebDriverTester> testerFactory, ProductInstance productInstance) {
        super(testerFactory, productInstance);
    }
}
