package it.com.atlassian.applinks.refapp_refapp.v3;

import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.rest.model.applink.RestMinimalApplicationLink;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.applinks.pageobjects.RefappWebsudoAwarePage;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.client.ApplinkOAuthStatusClient;
import com.atlassian.applinks.test.rule.MockResponseRule;
import com.atlassian.webdriver.applinks.component.v3.OAuthArrows;
import com.atlassian.webdriver.applinks.component.v3.RemoteAuthenticationStatus;
import com.atlassian.webdriver.applinks.component.v3.StatusDetailsNotification;
import com.atlassian.webdriver.applinks.component.v3.UiOAuthConfig;
import com.atlassian.webdriver.applinks.element.ClickableElement;
import com.atlassian.webdriver.applinks.externalcomponent.OAuthConfirmPage;
import com.atlassian.webdriver.applinks.page.v3.ConnectivityDialog;
import com.atlassian.webdriver.applinks.page.v3.V3EditApplinkPage;
import com.atlassian.webdriver.applinks.page.v3.V3ErrorPage;
import com.google.common.collect.Iterables;
import it.com.atlassian.applinks.AbstractApplinksTest;
import it.com.atlassian.applinks.testing.ApplinksAuthentications;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.RefappWebSudoRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import it.com.atlassian.applinks.testing.setup.EnableWebSudo;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;

import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toImmutableList;
import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toStream;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createThreeLoOnlyConfig;
import static com.atlassian.applinks.test.data.applink.config.UrlApplinkConfigurator.setUrl;
import static com.atlassian.applinks.test.data.util.TestUrls.createRandomUrl;
import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.defaultOAuthConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusDefault;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusWith;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthWithImpersonationConfig;
import static com.atlassian.applinks.test.mock.TestApplinkIds.createRandomId;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.disableOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuthWithImpersonation;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthDanceConfigurator.setUp3LoAccess;
import static com.atlassian.applinks.test.rest.data.applink.config.RemoteCapabilitiesConfigurator.refreshRemoteCapabilities;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.component.v3.ApplinkStatusLozenge.DISABLED;
import static com.atlassian.webdriver.applinks.component.v3.ApplinkStatusLozenge.ERROR_ACCESS;
import static com.atlassian.webdriver.applinks.component.v3.ApplinkStatusLozenge.ERROR_CONFIG;
import static com.atlassian.webdriver.applinks.component.v3.ApplinkStatusLozenge.ERROR_DEPRECATED;
import static com.atlassian.webdriver.applinks.component.v3.ApplinkStatusLozenge.ERROR_NETWORK;
import static com.atlassian.webdriver.applinks.matchers.LinkElementMatchers.withText;
import static com.atlassian.webdriver.applinks.matchers.LinkElementMatchers.withUrlThat;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static it.com.atlassian.applinks.testing.response.MockResponses.manifestResourceRequest;
import static it.com.atlassian.applinks.testing.response.MockResponses.oAuthProblemResponse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Category(RefappTest.class)
public class V3EditApplinkTest extends AbstractApplinksTest {
    private static final String TEST_APPLINK_NAME = "Test Applink";

    @Rule
    public final RefappWebSudoRule webSudoRule = new RefappWebSudoRule(INSTANCE1);
    @Rule
    public final TestApplinkRule testApplink = new TestApplinkRule.Builder(INSTANCE1, INSTANCE2)
            .configure(enableDefaultOAuth())
            .name(TEST_APPLINK_NAME)
            .build();
    @Rule
    public final ApplinksCapabilitiesRule refapp2Capabilities = new ApplinksCapabilitiesRule(REFAPP2);
    @Rule
    public final ApplinksVersionOverrideRule refapp2Version = new ApplinksVersionOverrideRule(REFAPP2);
    @Rule
    public final MockResponseRule refapp2Responses = new MockResponseRule(REFAPP2);

    private final ApplinksBackdoor applinksBackdoor = new ApplinksBackdoor(INSTANCE1);
    private final ApplinkOAuthStatusClient oauthStatusClient = new ApplinkOAuthStatusClient();


    // NOTE: some edit happy paths are covered in ApplinksV3EditSmokeTest and hence not repeated here

    @Test
    public void editNonExistingApplink() {
        V3ErrorPage errorPage = goToEditPageExpectingError(createRandomId());

        assertTrue("Expected 404 Not Found", errorPage.isError(404));
        assertThat(errorPage.getErrorMessage(), containsInOrder("Application with ID", "does not exist"));
    }

    @Test
    public void editIncompatibleApplink() {
        refapp2Version.setApplinksVersion("4.0.12");
        // refresh remote capabilities to fetch the "new" version
        testApplink.configureFrom(enableDefaultOAuth(), refreshRemoteCapabilities());

        V3ErrorPage errorPage = goToEditPageExpectingError(testApplink.from().id());

        // incompatible version Applinks should not be editable (as any INCOMPATIBLE applinks)
        assertTrue("Expected 409 Conflict", errorPage.isError(409));
        assertThat(errorPage.getErrorMessage(), containsInOrder(
                "Application link", "cannot be edited", "Unable to retrieve status"));
    }

    @Test
    public void editNonAtlassianApplink() {
        TestApplink.Side genericApplink = applinksBackdoor.createGeneric(createRandomUrl());
        V3ErrorPage errorPage = goToEditPageExpectingError(genericApplink.id());

        // generic Applinks should not be editable (as any NON_ATLASSIAN applinks)
        assertTrue("Expected 409 Conflict", errorPage.isError(409));
        assertThat(errorPage.getErrorMessage(), containsInOrder(
                "Application link", "cannot be edited", "Link to external third party"));
    }

    @Test
    public void editWithEmptyName() {
        V3EditApplinkPage editApplinkPage = goToEditPage();

        editApplinkPage.setApplicationName("").submitExpectingError();

        waitUntil(editApplinkPage.getApplicationNameError(), endsWith("missing or has invalid value."));
        // make sure the generic AJAX error dialog does not show
        waitUntilFalse(editApplinkPage.hasAjaxErrorDialog());
    }

    @Test
    public void editWithMalformedUrls() {
        V3EditApplinkPage editApplinkPage = goToEditPage();

        // non-absolute URLs
        editApplinkPage.setApplicationUrl("example.com").setDisplayUrl("example.com").submitExpectingError();

        waitUntilEquals("The URL must be absolute.", editApplinkPage.getApplicationUrlError());
        waitUntilEquals("The URL must be absolute.", editApplinkPage.getDisplayUrlError());
        // make sure the generic AJAX error dialog does not show
        waitUntilFalse(editApplinkPage.hasAjaxErrorDialog());
    }

    @Test
    public void editAndCancel() {
        V3EditApplinkPage editApplinkPage = goToEditPage();

        editApplinkPage.setApplicationUrl("http://example.com").setDisplayUrl("http://example.com").cancel();

        RestMinimalApplicationLink applink = applinksBackdoor.getApplink(testApplink.from());
        assertEquals("Application URL should not have changed", REFAPP2.getBaseUrl(), applink.getRpcUrl().toString());
        assertEquals("Display URL should not have changed", REFAPP2.getBaseUrl(), applink.getDisplayUrl().toString());
    }

    // default OAuth status CONNECTED is already covered by the smoke test
    @Test
    public void editApplinkStatusConnectedWithImpersonation() {
        testApplink.configure(enableOAuthWithImpersonation());

        V3EditApplinkPage editApplinkPage = goToEditPage();

        waitUntilTrue(editApplinkPage.getStatus().isWorking());
        waitUntilFalse(editApplinkPage.getStatus().isClickable()); // status non-clickable
        waitUntilFalse(editApplinkPage.getStatusDetails().isPresent()); // details should not be present for CONNECTED

        // verify auth table
        assertRemoteAuthWorking(editApplinkPage.getIncomingRemoteAuthStatus(), UiOAuthConfig.OAUTH_IMPERSONATION);
        assertRemoteAuthWorking(editApplinkPage.getOutgoingRemoteAuthStatus(), UiOAuthConfig.OAUTH_IMPERSONATION);

        assertArrowImages(editApplinkPage, OAuthArrows.RIGHT_MATCH, OAuthArrows.LEFT_MATCH);
    }

    @Test
    public void editApplinkStatusConfigErrorAuthLevelMismatch() {
        testApplink.configureTo(enableOAuthWithImpersonation());

        V3EditApplinkPage editApplinkPage = goToEditPage();

        waitUntilTrue(editApplinkPage.getStatus().hasStatus(ERROR_CONFIG));
        waitUntilFalse(editApplinkPage.getStatus().isClickable()); // status non-clickable

        // verify status details
        StatusDetailsNotification statusDetails = editApplinkPage.getStatusDetails();
        waitUntilTrue(statusDetails.isPresent());
        assertOAuthMismatchContents(statusDetails);
        assertOnlyRemoteConfigLinks(statusDetails);
        assertTroubleshootActionOnly(statusDetails);

        // verify auth table
        assertRemoteAuthError(editApplinkPage.getIncomingRemoteAuthStatus(), UiOAuthConfig.OAUTH_IMPERSONATION);
        assertRemoteAuthError(editApplinkPage.getOutgoingRemoteAuthStatus(), UiOAuthConfig.OAUTH_IMPERSONATION);
    }

    @Test
    @Ignore("flaky")
    public void editApplinkConfigErrorAuthLevelMismatchShouldHaveMismatchArrows() {
        testApplink.configureTo(enableOAuthWithImpersonation());

        V3EditApplinkPage editApplinkPage = goToEditPage();
        waitUntilTrue(editApplinkPage.getStatus().hasStatus(ERROR_CONFIG));

        assertArrowImages(editApplinkPage, OAuthArrows.RIGHT_MISMATCH, OAuthArrows.LEFT_MISMATCH);
        assertConnectivityDialog(editApplinkPage.triggerOutgoingConnectivityDialog());
        assertConnectivityDialog(editApplinkPage.triggerIncomingConnectivityDialog());
    }

    @Test
    public void editApplinkConfigErrorAuthLevelDisabledShouldHaveDisabledArrows() {
        testApplink.configure(disableOAuth());

        V3EditApplinkPage editApplinkPage = goToEditPage();
        waitUntilTrue(editApplinkPage.getStatus().hasStatus(DISABLED));
        assertArrowImages(editApplinkPage, OAuthArrows.RIGHT_DISABLED, OAuthArrows.LEFT_DISABLED);
    }

    @Test
    public void editApplinkConfigErrorOutgoingOAuthOnlyDisabledShouldHaveMismatchArrow() {
        testApplink.configureTo(disableOAuth());

        V3EditApplinkPage editApplinkPage = goToEditPage();
        waitUntilTrue(editApplinkPage.getStatus().hasStatus(ERROR_CONFIG));

        assertArrowImages(editApplinkPage, OAuthArrows.RIGHT_MISMATCH, OAuthArrows.LEFT_MISMATCH);
        assertConnectivityDialog(editApplinkPage.triggerOutgoingConnectivityDialog());
        assertConnectivityDialog(editApplinkPage.triggerIncomingConnectivityDialog());
    }

    @Test
    public void editApplinkStatusNetworkError() {
        // unresolved URL
        String brokenUrl = createRandomUrl();
        testApplink.configureFrom(setUrl(brokenUrl));

        V3EditApplinkPage editApplinkPage = goToEditPage();

        waitUntilTrue(editApplinkPage.getStatus().hasStatus(ERROR_NETWORK));
        waitUntilFalse(editApplinkPage.getStatus().isClickable()); // status non-clickable

        // verify status details
        StatusDetailsNotification statusDetails = editApplinkPage.getStatusDetails();
        waitUntilTrue(statusDetails.isPresent());
        assertUnresolvedUrlContents(statusDetails, brokenUrl);
        assertOnlyRemoteConfigLinks(statusDetails);
        assertTroubleshootActionOnly(statusDetails);

        // verify auth table
        assertRemoteAuthNetworkError(editApplinkPage.getIncomingRemoteAuthStatus());
        assertRemoteAuthNetworkError(editApplinkPage.getOutgoingRemoteAuthStatus());

        assertArrowImages(editApplinkPage, OAuthArrows.RIGHT_MISMATCH, OAuthArrows.LEFT_MISMATCH);
        // no inline dialog in the connectivity arrow for this scenario
    }

    @Test
    public void editApplinkStatusOAuthNetworkError() {
        // mock out OAuth request to manifest to return timestamp_refused
        refapp2Responses.addMockResponse(manifestResourceRequest().hasOAuthHeader(true).build(),
                oAuthProblemResponse(ApplinksOAuth.PROBLEM_TIMESTAMP_REFUSED));

        V3EditApplinkPage editApplinkPage = goToEditPage();

        waitUntilTrue(editApplinkPage.getStatus().hasStatus(ERROR_NETWORK));
        waitUntilFalse(editApplinkPage.getStatus().isClickable()); // status non-clickable

        // verify status details
        StatusDetailsNotification statusDetails = editApplinkPage.getStatusDetails();
        waitUntilTrue(statusDetails.isPresent());
        assertTimestampRefusedContents(statusDetails);
        assertOnlyRemoteConfigLinks(statusDetails);
        assertTroubleshootActionOnly(statusDetails);

        // verify auth table
        assertRemoteAuthError(editApplinkPage.getIncomingRemoteAuthStatus(), UiOAuthConfig.OAUTH);
        assertRemoteAuthError(editApplinkPage.getOutgoingRemoteAuthStatus(), UiOAuthConfig.OAUTH);
    }

    @Test
    public void editApplinkStatusRequiresRemoteAuthentication() {
        loginAsSysadmin(PRODUCT2); // login to avoid redirect-on-login bug in Refapp
        disableStatusApiOnRefapp2();

        V3EditApplinkPage editApplinkPage = goToEditPage();

        // should not show the link nor any details
        waitUntilFalse(editApplinkPage.getStatus().isPresent());
        waitUntilFalse(editApplinkPage.getStatusDetails().isPresent());

        waitUntilTrue(editApplinkPage.getIncomingRemoteAuthStatus().isRemoteAuthenticationRequired());
        waitUntilTrue(editApplinkPage.getOutgoingRemoteAuthStatus().isRemoteAuthenticationRequired());

        assertArrowImages(editApplinkPage, OAuthArrows.RIGHT_DISABLED, OAuthArrows.LEFT_DISABLED);
        // no inline dialog in the connectivity arrow for this scenario

        editApplinkPage = editApplinkPage.getIncomingRemoteAuthStatus()
                .startRemoteAuthentication()
                .confirmHandlingWebLoginIfRequired(TestAuthentication.admin());

        // should be back to CONNECTED after the OAuth dance
        waitUntilTrue(editApplinkPage.getStatus().isWorking());
        waitUntilFalse(editApplinkPage.getStatus().isClickable()); // status non-clickable
        waitUntilFalse(editApplinkPage.getStatusDetails().isPresent()); // details should not be present for CONNECTED
    }

    @Test
    public void editApplinkStatusInsufficientRemotePermission() {
        loginAsSysadmin(PRODUCT2); // login to avoid redirect-on-login bug in Refapp
        disableStatusApiOnRefapp2();
        // create access token from admin on Refapp1 to a non-admin on Refapp2
        testApplink.configure(setUp3LoAccess(TestAuthentication.admin(), ApplinksAuthentications.REFAPP_USER_BARNEY));

        V3EditApplinkPage editApplinkPage = goToEditPage();

        // should result in access error
        waitUntilTrue(editApplinkPage.getStatus().hasStatus(ERROR_ACCESS));
        waitUntilFalse(editApplinkPage.getStatus().isClickable()); // status non-clickable

        StatusDetailsNotification statusDetails = editApplinkPage.getStatusDetails();
        assertTroubleshootActionOnly(statusDetails);
        fixInsufficientPermission(statusDetails, editApplinkPage);

        // should be back to working status
        waitUntilTrue(bindEditPage().getStatus().isWorking());
    }

    @Test
    public void editUnsupportedOAuthLevel() {
        // incoming 3LO only, outgoing Impersonation
        testApplink.configureFrom(enableOAuth(createThreeLoOnlyConfig(), createOAuthWithImpersonationConfig()));

        V3EditApplinkPage editApplinkPage = goToEditPage();

        // should result in upgrade config error
        waitUntilTrue(editApplinkPage.getStatus().hasStatus(ERROR_DEPRECATED));
        waitUntilFalse(editApplinkPage.getStatus().isClickable()); // status non-clickable

        StatusDetailsNotification statusDetails = editApplinkPage.getStatusDetails();
        assertUnsupportedAuthLevelContents(statusDetails);
        assertTroubleshootActionOnly(statusDetails);

        // disabled for incoming (from 3LO only), impersonation preserved for outgoing
        waitUntilTrue(editApplinkPage.getIncomingAuthDropdown().isSelected(UiOAuthConfig.OAUTH_DISABLED));
        waitUntilTrue(editApplinkPage.getOutgoingAuthDropdown().isSelected(UiOAuthConfig.OAUTH_IMPERSONATION));
    }

    @Test
    public void testSaveOAuthConfigWithDifferentIncomingAndOutgoingOAuthLevels() {
        V3EditApplinkPage editApplinkPage = goToEditPage();

        waitUntilTrue(editApplinkPage.getOutgoingAuthDropdown().isSelected(UiOAuthConfig.OAUTH));
        waitUntilTrue(editApplinkPage.getIncomingAuthDropdown().isSelected(UiOAuthConfig.OAUTH));
        editApplinkPage.getIncomingAuthDropdown().select(UiOAuthConfig.OAUTH_IMPERSONATION);

        // The connectivity dialog when open seems to interfere with assertions and thus introduces flakiness, therefore we dismiss it first
        editApplinkPage.getIncomingConnectivityDialog().dismiss();
        editApplinkPage.submit();

        // make sure OAuth impersonation was saved in the server
        ApplinkOAuthStatus oAuthStatus = oauthStatusClient.getOAuthStatus(testApplink.from());
        assertThat(oAuthStatus, oAuthStatusWith(oAuthWithImpersonationConfig(), defaultOAuthConfig()));
    }

    @Test
    public void localAuthShouldNotBeSavedIfThereAreErrors() {
        V3EditApplinkPage editApplinkPage = goToEditPage();

        // default is OAuth enabled
        // verify outgoing local auth
        waitUntilTrue(editApplinkPage.getOutgoingAuthDropdown().isSelected(UiOAuthConfig.OAUTH));
        editApplinkPage.getOutgoingAuthDropdown().select(UiOAuthConfig.OAUTH_IMPERSONATION);

        // The connectivity dialog when open seems to interfere with assertions and thus introduces flakiness, therefore we dismiss it first
        editApplinkPage.getOutgoingConnectivityDialog().dismiss();

        // verify incoming local auth
        waitUntilTrue(editApplinkPage.getIncomingAuthDropdown().isSelected(UiOAuthConfig.OAUTH));
        editApplinkPage.getIncomingAuthDropdown().select(UiOAuthConfig.OAUTH_IMPERSONATION);
        editApplinkPage.getIncomingConnectivityDialog().dismiss();

        // non-absolute URL will trigger an error in the server side
        editApplinkPage.setApplicationUrl("example.com").setDisplayUrl("example.com");
        editApplinkPage.submitExpectingError();

        // make sure OAuth impersonation was _not_ saved in the server
        ApplinkOAuthStatus oAuthStatus = oauthStatusClient.getOAuthStatus(testApplink.from());
        assertThat(oAuthStatus, oAuthStatusDefault());
    }

    @Test
    @EnableWebSudo
    public void editingAppLinksIsWebSudoProtected() {
        LoginClient.login(SYSADMIN_USERNAME, SYSADMIN_PASSWORD, PRODUCT);
        final RefappWebsudoAwarePage page = PRODUCT.visit(RefappWebsudoAwarePage.class, new V3EditApplinkPage(testApplink.from()));
        assertNotNull(page);
    }

    private static void assertArrowImages(V3EditApplinkPage editApplinkPage, OAuthArrows rightArrow, OAuthArrows leftArrow) {
        waitUntil(editApplinkPage.getOutgoingStatusImageSrc(), endsWith(rightArrow.image()));
        waitUntil(editApplinkPage.getIncomingStatusImageSrc(), endsWith(leftArrow.image()));
    }

    private static void assertConnectivityDialog(ConnectivityDialog dialog) {
        waitUntilTrue(dialog.isVisible());
        waitUntil(dialog.getContents(), containsInOrder(
                "Local OAuth setting will take effect on save",
                "You will also need to make the same changes in the remote application to establish connection again"
        ));
        dialog.dismiss();
        waitUntilFalse(dialog.isVisible());
    }

    private static void assertOAuthMismatchContents(StatusDetailsNotification statusDetails) {
        assertEquals("OAuth mismatch", statusDetails.getTitle());
        Iterable<String> contents = statusDetails.getContents();
        assertThat(contents, iterableWithSize(3));
        assertThat(Iterables.get(contents, 0), containsInOrder(
                "local", "incoming connection", "OAuth", "however", "Test Applink", "OAuth with impersonation"
        ));
        assertThat(Iterables.get(contents, 1), containsInOrder(
                "local", "outgoing connection", "OAuth", "however", "Test Applink", "OAuth with impersonation"
        ));
    }

    private static void assertUnresolvedUrlContents(StatusDetailsNotification statusDetails, String url) {
        assertEquals("Unable to reach the remote application", statusDetails.getTitle());
        Iterable<String> contents = statusDetails.getContents();
        assertThat(contents, iterableWithSize(1));
        assertThat(Iterables.get(contents, 0), containsInOrder(
                "couldn't resolve the hostname",
                url,
                "DNS configuration problem",
                "Application URL",
                "may be incorrect"
        ));
    }

    private static void assertTimestampRefusedContents(StatusDetailsNotification statusDetails) {
        assertEquals("The system clocks are not synchronized", statusDetails.getTitle());
        Iterable<String> contents = statusDetails.getContents();
        assertThat(contents, iterableWithSize(1));
        assertThat(Iterables.get(contents, 0), containsInOrder(
                "different system time from",
                TEST_APPLINK_NAME,
                "prevents",
                "from authenticating"
        ));
    }

    private static void assertUnsupportedAuthLevelContents(StatusDetailsNotification statusDetails) {
        assertEquals("Unsupported OAuth level", statusDetails.getTitle());
        Iterable<String> contents = statusDetails.getContents();
        assertThat(contents, iterableWithSize(1));
        assertThat(Iterables.get(contents, 0), containsInOrder(
                "The current OAuth",
                "prevents integrations from working",
                "OAuth",
                "OAuth with impersonation"
        ));

        Iterable<String> docLinks = toStream(statusDetails.findContentLinks(By.className("help-link")))
                .map(ClickableElement::getText)
                .collect(toImmutableList());
        assertThat(docLinks, contains("OAuth", "OAuth with impersonation"));
    }

    private static void assertOnlyRemoteConfigLinks(StatusDetailsNotification statusDetails) {
        // status notification contents should only contain links to the remote app config
        assertThat(statusDetails.getContentLinks(), everyItem(allOf(
                withText(TEST_APPLINK_NAME),
                withUrlThat(startsWith(INSTANCE2.getBaseUrl()))
        )));
    }

    private static void assertTroubleshootActionOnly(StatusDetailsNotification statusDetails) {
        // only 1 action link - Troubleshoot
        assertThat(statusDetails.getActions(), contains(withText("Troubleshoot")));
    }

    private static void assertRemoteAuthWorking(RemoteAuthenticationStatus remoteAuth, UiOAuthConfig expectedConfig) {
        assertRemoteAuthAvailable(remoteAuth, expectedConfig);
        // no error - no troubleshooting link
        waitUntilFalse(remoteAuth.hasTroubleshootingLink());
    }

    private static void assertRemoteAuthError(RemoteAuthenticationStatus remoteAuth, UiOAuthConfig expectedConfig) {
        assertRemoteAuthAvailable(remoteAuth, expectedConfig);
        // mismatch means there should be troubleshooting link
        waitUntilTrue(remoteAuth.hasTroubleshootingLink());
    }

    private static void assertRemoteAuthNetworkError(RemoteAuthenticationStatus remoteAuth) {
        assertRemoteAuthLink(remoteAuth);
        waitUntilFalse(remoteAuth.hasOAuthConfig()); // network error remote auth not available
        waitUntilTrue(remoteAuth.hasTroubleshootingLink());
    }

    private static void assertRemoteAuthAvailable(RemoteAuthenticationStatus remoteAuth, UiOAuthConfig expectedConfig) {
        assertRemoteAuthLink(remoteAuth);
        waitUntilTrue(remoteAuth.hasOAuthConfig());
        waitUntilEquals(expectedConfig, remoteAuth.getOAuthConfig());
    }

    private static void assertRemoteAuthLink(RemoteAuthenticationStatus remoteAuth) {
        waitUntilTrue(remoteAuth.hasApplicationIcon());
        waitUntilTrue(remoteAuth.hasRemoteLink());
    }

    private void fixInsufficientPermission(StatusDetailsNotification statusDetails, V3EditApplinkPage editPage) {
        ClickableElement element = statusDetails.findContentLink(By.className("status-insufficient-remote-permission"));
        assertNotNull("Link to refresh OAuth access not found", element);
        element.follow();
        PRODUCT.getPageBinder().bind(OAuthConfirmPage.class, editPage)
                .confirmHandlingWebLoginIfRequired(TestAuthentication.admin());
    }

    private V3EditApplinkPage goToEditPage() {
        return loginAsSysadminAndGoTo(PRODUCT, V3EditApplinkPage.class, testApplink.from());
    }

    private V3ErrorPage goToEditPageExpectingError(String applinkId) {
        return loginAsSysadminAndGoTo(PRODUCT, V3ErrorPage.class, V3EditApplinkPage.getUrl(applinkId));
    }

    private V3EditApplinkPage bindEditPage() {
        return PRODUCT.getPageBinder().bind(V3EditApplinkPage.class, testApplink.from());
    }

    private void disableStatusApiOnRefapp2() {
        // disable STATUS_API and change version to force remote capabilities on Refapp1 to update
        refapp2Version.setApplinksVersion("5.0.5");
        refapp2Capabilities.disable(ApplinksCapabilities.STATUS_API);
        testApplink.configureFrom(refreshRemoteCapabilities());
    }
}
