package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.oauth.auth.ThreeLeggedOAuthRequest;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.categories.TrustedAppsTest;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static junit.framework.Assert.assertTrue;

@Category({TrustedAppsTest.class, RefappTest.class})
public class ApplicationLinkRequestFactoryPriorityTestRefApp extends V2AbstractApplinksTest {
    private String linkUrl;

    @Rule
    public TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    @Before
    public void setUp() {
        applink.configure(enableTrustedApps());
        // create the link
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // go to OAuth config screen of the second instance
        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        linkUrl = linkPage.getApplicationLinks().get(0).getApplicationUrl();
    }

    @Test
    public void verify3LOSelectedIfTrustedAppAnd2LOImpersonationNotConfigured() {
        unregister(TrustedAppsAuthenticationProvider.class);
        unregister(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);
        register(OAuthAuthenticationProvider.class);

        assertRequestType(ThreeLeggedOAuthRequest.class);
        assertImpersonatingRequestType(ThreeLeggedOAuthRequest.class);
    }

    private void register(Class<? extends AuthenticationProvider> clazz) {
        RegisterPage page = PRODUCT.visit(RegisterPage.class, linkUrl, clazz.getName());
        assertTrue(page.getContent().contains("done"));
    }

    private void unregister(Class<? extends AuthenticationProvider> clazz) {
        UnregisterPage page = PRODUCT.visit(UnregisterPage.class, linkUrl, clazz.getName());
        assertTrue(page.getContent().contains("done"));
    }

    private void assertRequestType(Class clazz) {
        GetRequestTypePage page = PRODUCT.visit(GetRequestTypePage.class, linkUrl);
        assertTrue(page.getContent().contains(clazz.getName()));
    }

    private void assertImpersonatingRequestType(Class clazz) {
        GetImpersonatingRequestTypePage page = PRODUCT.visit(GetImpersonatingRequestTypePage.class, linkUrl);
        assertTrue(page.getContent().contains(clazz.getName()));
    }

    private void assertNonImpersonatingRequestType(Class clazz) {
        GetNonImpersonatingRequestTypePage page = PRODUCT.visit(GetNonImpersonatingRequestTypePage.class, linkUrl);
        assertTrue(page.getContent().contains(clazz.getName()));
    }

    public static class RegisterPage extends ApplinkAbstractPage {
        private String rpcUrl;
        private String authenticationClass;

        public RegisterPage(String rpcUrl, String authenticationClass) {
            this.rpcUrl = rpcUrl;
            this.authenticationClass = authenticationClass;
        }

        public String getUrl() {
            return "/rest/applinks-tests/1/authentication/register?rpc-url=" + rpcUrl + "&authentication-class=" + authenticationClass;
        }

        public String getContent() {
            return driver.getPageSource();
        }
    }

    public static class UnregisterPage extends ApplinkAbstractPage {
        private String rpcUrl;
        private String authenticationClass;

        public UnregisterPage(String rpcUrl, String authenticationClass) {
            this.rpcUrl = rpcUrl;
            this.authenticationClass = authenticationClass;
        }

        public String getUrl() {
            return "/rest/applinks-tests/1/authentication/unregister?rpc-url=" + rpcUrl + "&authentication-class=" + authenticationClass;
        }

        public String getContent() {
            return driver.getPageSource();
        }
    }

    public static class GetRequestTypePage extends ApplinkAbstractPage {
        private String rpcUrl;

        public GetRequestTypePage(String rpcUrl) {
            this.rpcUrl = rpcUrl;
        }

        public String getUrl() {
            return "/rest/applinks-tests/1/authentication/getRequestType?rpc-url=" + rpcUrl;
        }

        public String getContent() {
            return driver.getPageSource();
        }
    }

    public static class GetImpersonatingRequestTypePage extends ApplinkAbstractPage {
        private String rpcUrl;

        public GetImpersonatingRequestTypePage(String rpcUrl) {
            this.rpcUrl = rpcUrl;
        }

        public String getUrl() {
            return "/rest/applinks-tests/1/authentication/getImpersonatingRequestType?rpc-url=" + rpcUrl;
        }

        public String getContent() {
            return driver.getPageSource();
        }
    }

    public static class GetNonImpersonatingRequestTypePage extends ApplinkAbstractPage {
        private String rpcUrl;

        public GetNonImpersonatingRequestTypePage(String rpcUrl) {
            this.rpcUrl = rpcUrl;
        }

        public String getUrl() {
            return "/rest/applinks-tests/1/authentication/getNonImpersonatingRequestType?rpc-url=" + rpcUrl;
        }

        public String getContent() {
            return driver.getPageSource();
        }
    }
}