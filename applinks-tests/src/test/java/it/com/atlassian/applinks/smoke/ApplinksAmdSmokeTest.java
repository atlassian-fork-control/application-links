package it.com.atlassian.applinks.smoke;

import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import com.atlassian.webdriver.applinks.util.AmdValidator;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.pageobjects.LoginClient.loginAsSysadmin;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

@Category({SmokeTest.class})
public class ApplinksAmdSmokeTest extends AbstractDefaultSmokeTest {
    @Rule
    public final ApplinksFeaturesRule applinksFeaturesRule;

    public ApplinksAmdSmokeTest(@Nonnull GenericTestedProduct mainProduct,
                                @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        this.applinksFeaturesRule = new ApplinksFeaturesRule(mainInstance);
    }

    @Test
    public void shouldLoadAmdDependenciesInV3Ui() {
        loginAsSysadmin(mainProduct);
        mainProduct.visit(V3ListApplicationLinksPage.class);

        AmdValidator amd = mainProduct.getPageBinder().bind(AmdValidator.class);
        waitUntilTrue(amd.amdModulesLoaded());
    }

    @Test
    public void shouldLoadAmdDependenciesInV2Ui() {
        applinksFeaturesRule.disable(ApplinksFeatures.V3_UI);
        loginAsSysadmin(mainProduct);
        mainProduct.visit(ListApplicationLinkPage.class);

        AmdValidator amd = mainProduct.getPageBinder().bind(AmdValidator.class);
        waitUntilTrue(amd.amdModulesLoaded());
    }

    @Test
    public void shouldLoadI18nInV2Ui() {
        applinksFeaturesRule.disable(ApplinksFeatures.V3_UI);
        loginAsSysadmin(mainProduct);
        mainProduct.visit(ListApplicationLinkPage.class);

        AmdValidator amd = mainProduct.getPageBinder().bind(AmdValidator.class);
        waitUntilTrue(amd.amdModulesLoaded());

        assertEquals("Jira", getClientSideI18n(amd, "getApplicationTypeName", "jira"));
        assertEquals("Bitbucket Server", getClientSideI18n(amd, "getApplicationTypeName", "stash"));

        assertEquals("Jira Project", getClientSideI18n(amd, "getEntityTypeName", "jira.project"));
        assertEquals("Jira Projects", getClientSideI18n(amd, "getPluralizedEntityTypeName", "jira.project"));
        assertEquals("Bitbucket Server Project", getClientSideI18n(amd, "getEntityTypeName", "stash.project"));
        assertEquals("Bitbucket Server Projects", getClientSideI18n(amd, "getPluralizedEntityTypeName", "stash.project"));

        // this requires the Applinks OAuth plugin to run
        // all 3 provider modules have i18n name "OAuth"
        assertEquals("OAuth", getClientSideI18n(amd, "getAuthenticationTypeName",
                "com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider"));
        assertEquals("OAuth", getClientSideI18n(amd, "getAuthenticationTypeName",
                "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider"));
        assertEquals("OAuth", getClientSideI18n(amd, "getAuthenticationTypeName",
                "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider"));
    }

    private static String getClientSideI18n(AmdValidator amd, String function, String arg) {
        return amd.execute(String.class, "applinks/common/i18n", function, format("'%s'", arg));
    }
}
