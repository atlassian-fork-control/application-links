package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.CreateAppLinkWith3LO2LORule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import static org.junit.Assert.fail;

@Category(RefappTest.class)
public class AuthConfigTabVisibilityTestRefApp extends V2AbstractApplinksTest {
    @Rule
    public RuleChain ruleChain = CreateAppLinkWith3LO2LORule.forProducts(PRODUCT, PRODUCT2);

    private String product2Url;

    @Before
    public void setUp() {
        product2Url = PRODUCT2.getProductInstance().getBaseUrl();
        loginAsSysadmin(PRODUCT, PRODUCT2);

        logout(PRODUCT, PRODUCT2);
    }

    @Test
    public void testSysadminCanSeeAllTabs() {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        ApplicationDetailsSection detailsSection = linkPage.configureApplicationLink(product2Url);

        detailsSection.openIncomingTrustedApplications();
        detailsSection.openIncomingOauth();
        detailsSection.openIncomingBasicAccess();
        detailsSection.openIncomingCors();
        detailsSection.openOutgoingTrustedApplications();
        detailsSection.openOutgoingOauthExpectingAutoConfig();
        detailsSection.openOutgoingBasicAccess();

        logout(PRODUCT, PRODUCT2);
    }

    @Test
    public void testAdminCanSeeOnlyOAuthTabs() {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        ApplicationDetailsSection detailsSection = linkPage.configureApplicationLink(product2Url);

        detailsSection.openIncomingOauth();
        detailsSection.openOutgoingOauthExpectingAutoConfig();
    }

    @Test
    public void testAdminCannotSeeTrustedAppTab() {
        login(PRODUCT, PRODUCT2);

        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        ApplicationDetailsSection detailsSection = linkPage.configureApplicationLink(product2Url);
        try {
            detailsSection.openIncomingTrustedApplications();
            fail("exception expected");
        } catch (IllegalStateException ise) {
            // good
        }
    }

    @Test
    public void testAdminCannotSeeOutgoingTrustedAppTab() {
        login(PRODUCT, PRODUCT2);

        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        ApplicationDetailsSection detailsSection = linkPage.configureApplicationLink(product2Url);
        try {
            detailsSection.openOutgoingTrustedApplications();
            fail("exception expected");
        } catch (IllegalStateException ise) {
            // good
        }
    }

    @Test
    public void testAdminCannotSeeIncomingCorsTab() {
        login(PRODUCT, PRODUCT2);

        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        ApplicationDetailsSection detailsSection = linkPage.configureApplicationLink(product2Url);
        try {
            detailsSection.openIncomingCors();
            fail("exception expected");
        } catch (IllegalStateException ise) {
            System.out.print(ise);
        }
    }

    @Test
    public void testAdminCannotSeeIncomingBasicTab() {
        login(PRODUCT, PRODUCT2);

        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        ApplicationDetailsSection detailsSection = linkPage.configureApplicationLink(product2Url);
        try {
            detailsSection.openIncomingBasicAccess();
            fail("exception expected");
        } catch (IllegalStateException ise) {
            System.out.print(ise);
        }
    }

    @Test
    public void testAdminCannotSeeOutgoingBasicTab() {
        login(PRODUCT, PRODUCT2);

        ListApplicationLinkPage linkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        ApplicationDetailsSection detailsSection = linkPage.configureApplicationLink(product2Url);
        try {
            detailsSection.openOutgoingBasicAccess();
            fail("exception expected");
        } catch (IllegalStateException ise) {
            System.out.print(ise);
        }
    }
}