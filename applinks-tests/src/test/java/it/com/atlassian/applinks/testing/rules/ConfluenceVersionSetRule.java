package it.com.atlassian.applinks.testing.rules;

import com.atlassian.pageobjects.ProductInstance;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import org.junit.rules.ExternalResource;
import org.junit.rules.RuleChain;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;
import static org.junit.Assert.fail;

public final class ConfluenceVersionSetRule extends ExternalResource {
    @Nonnull
    public static RuleChain forProducts(@Nonnull ProductInstance... products) {
        checkNotNull(products, "products");
        return forProducts(asList(products));
    }

    @Nonnull
    public static RuleChain forProducts(@Nonnull Iterable<ProductInstance> products) {
        checkNotNull(products, "products");
        RuleChain ruleChain = RuleChain.emptyRuleChain();
        for (ProductInstance productInstance : products) {
            ruleChain = ruleChain.around(new ConfluenceVersionSetRule(productInstance));
        }
        return ruleChain;
    }

    private final ProductInstance productInstance;

    public ConfluenceVersionSetRule(@Nonnull ProductInstance productInstance) {
        this.productInstance = checkNotNull(productInstance, "productInstance");
    }

    @Override
    protected void before() {
        if (ProductInstances.CONFLUENCE.matches(productInstance) && System.getProperty("confluence.version") == null) {
            /**
             * If the system property confluence.version is not set then
             * com.atlassian.confluence.it.maven.MavenDependencyHelper.resolve() will default to using the project
             * version from the current project pom.xml, i.e. the applinks version number when loading confluence's
             * test plugins, when the confluence Product instance is loaded. Funnily enough this will cause the tests
             * to fail.
             *
             * In Maven this has been set as a <confluence.version>${confluence.version}</confluence.version> for AMPS
             * In Intellij it will need setting as a VM option in the run configuration e.g. -Dconfluence.version=blah
             */
            fail("System property confluence.version must be set");
        }
    }
}
