package it.com.atlassian.applinks;

import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import org.junit.Rule;
import org.junit.rules.RuleChain;

import javax.annotation.Nonnull;

import static it.com.atlassian.applinks.testing.ApplinksAuthentications.SYSADMIN;

/**
 * Abstract Applinks browser test. Provides the default browser rule chain per-method - therefore use it only for tests
 * that refresh state per test method (rather than entire class).
 */
public abstract class AbstractApplinksTest extends AbstractRefappToRefappBrowserTest {
    protected static final String USER_USERNAME = "barney";
    protected static final String USER_PASSWORD = "barney";

    public static final String ADMIN_USERNAME = "betty";
    public static final String ADMIN_PASSWORD = "betty";

    public static final String SYSADMIN_USERNAME = "admin";
    public static final String SYSADMIN_PASSWORD = "admin";

    @Rule
    public final RuleChain testRules = ApplinksRuleChain.forProducts(PRODUCT, PRODUCT2);

    protected static void login() {
        LoginClient.loginAsSysadmin(PRODUCT);
    }

    protected static void login(RefappTestedProduct... products) {
        LoginClient.login(ADMIN_USERNAME, ADMIN_PASSWORD, products);
    }

    protected static void loginAsSysadmin(RefappTestedProduct... products) {
        LoginClient.login(SYSADMIN_USERNAME, SYSADMIN_PASSWORD, products);
    }

    protected static void loginAsUser(RefappTestedProduct... products) {
        LoginClient.login(USER_USERNAME, USER_PASSWORD, products);
    }

    protected static void login(String username, String password, RefappTestedProduct... products) {
        LoginClient.login(username, password, products);
    }

    @Nonnull
    protected static <P extends Page> P loginAsSysadminAndGoTo(@Nonnull Class<P> targetPage, @Nonnull Object... args) {
        return loginAsSysadminAndGoTo(PRODUCT, targetPage, args);
    }

    @Nonnull
    protected static <P extends Page> P loginAsSysadminAndGoTo(@Nonnull RefappTestedProduct refapp,
                                                               @Nonnull Class<P> targetPage, @Nonnull Object... args) {
        return LoginClient.loginAndGoTo(SYSADMIN, refapp, targetPage, args);
    }

    @SafeVarargs
    protected static void logout(@Nonnull TestedProduct<WebDriverTester>... products) {
        LoginClient.logout(products);
    }
}
