package it.com.atlassian.applinks.testing.product;

import com.atlassian.applinks.test.product.SupportedProducts;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.annotation.Nonnull;

/**
 * Allows tests run by {@code ApplinksSmokeTestRunner} to declare products that they support.
 *
 * @see it.com.atlassian.applinks.testing.runner.ApplinksSmokeTestRunner
 * @since 4.3
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Inherited
public @interface Products {
    /**
     * @return all product types that this test supports
     */
    @Nonnull SupportedProducts[] value();

    @Nonnull String reason() default "";
}