package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static org.hamcrest.Matchers.is;

@Category({RefappTest.class})
public class ConfigureApplicationLinkTestRefApp extends V2AbstractApplinksTest {
    @Before
    public void setUp() {
        login();
    }

    @Test
    public void testApplicationLinkNameTrimmed() {
        String url = "http://jira.thisdomainisnotregisteredbyanyone.invalid";
        String name = "  foo   ";

        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, url, name);
        ListApplicationLinkPage listApplicationLinks = PRODUCT.visit(ListApplicationLinkPage.class);

        ApplicationDetailsSection applicationDetailsSection = listApplicationLinks.configureApplicationLink(url);
        waitUntilEquals("foo", applicationDetailsSection.getApplicationNameTimed());
        applicationDetailsSection.cancel();
    }


    @Test
    public void testApplicationLinkShouldNotFailIfMaliciousUrlProvided() {
        String maliciousUrl = PRODUCT.getProductInstance().getBaseUrl() + "/plugins/servlet/applinks-tests/malicious-js?callback=??";
        //to reproduce the issue you should have both malicious
        //create generic link will fail if malicious-js will be executed 
        OAuthApplinksClient.createGenericLink(PRODUCT, maliciousUrl, "any string");
    }

    @Test
    public void testDuplicateApplicationName() {
        final String url1 = REFAPP1.getBaseUrl() + "ABCDEF";
        final String url2 = REFAPP1.getBaseUrl() + "GHIJKL";
        final String name1 = "Adam JIRA 1";
        final String name2 = "Adam JIRA 2";

        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, url1, name1);
        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, url2, name2);
        ListApplicationLinkPage listApplicationLinks = PRODUCT.visit(ListApplicationLinkPage.class);

        // Change the name of #1 to the name of #2
        ApplicationDetailsSection applicationDetailsSection = listApplicationLinks.configureApplicationLink(url1);
        applicationDetailsSection.setApplicationName(name2);
        applicationDetailsSection = applicationDetailsSection.saveExpectingError();
        waitUntil(applicationDetailsSection.getErrorMessagesTimed(),
                contains(is("There is already one Application Link with name '" + name2 + "'.")));
    }

    @Ignore("flaky")
    @Test
    public void testDisplayNameAndUrlIsConfigurable() {
        final String url1 = "http://jira.example.com";
        final String url2 = "http://jira.someotherdomain.com";
        final String type = "Generic Application";
        final String name = "My JIRA";
        final String newName = "My New JIRA Name";


        OAuthApplinksClient.createGenericAppLinkViaRest(PRODUCT, url1, name);
        ListApplicationLinkPage listApplicationLinks = PRODUCT.visit(ListApplicationLinkPage.class);

        ApplicationDetailsSection applicationDetailsSection = listApplicationLinks.configureApplicationLink(url1);
        applicationDetailsSection.setApplicationName(newName);
        applicationDetailsSection.setDisplayUrl(url2);
        applicationDetailsSection.save();

        //display name isn't visible on the listApplicationLinks screen. let's see it on the config screen instead.
        applicationDetailsSection = listApplicationLinks.configureApplicationLink(url2);
        waitUntilEquals(newName, applicationDetailsSection.getApplicationNameTimed());
        waitUntilEquals(url2, applicationDetailsSection.getDisplayUrlTimed());
        waitUntilEquals(url1, applicationDetailsSection.getApplicationUrlTimed());
        waitUntilEquals(type, applicationDetailsSection.getApplicationTypeTimed());
    }
}
