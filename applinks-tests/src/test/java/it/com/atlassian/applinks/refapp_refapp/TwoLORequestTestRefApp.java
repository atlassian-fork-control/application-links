package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.fisheye.deploy.TwoLeggedOAuthTestServletPage;
import com.atlassian.applinks.fisheye.deploy.TwoLeggedOAuthWithImpersonationTestServletPage;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.component.OauthIncomingAuthenticationWithAutoConfigSection;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.CreateAppLinkWith3LO2LORule;
import it.com.atlassian.applinks.testing.rules.ElapsedTimeRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

@Category(RefappTest.class)
public class TwoLORequestTestRefApp extends V2AbstractApplinksTest {
    @Rule
    public RuleChain ruleChain = testRules
            .around(CreateAppLinkWith3LO2LORule.forProducts(PRODUCT, PRODUCT2))
            .around(new ElapsedTimeRule("TwoLORequestTest"));

    @Before
    public void setUp() throws Exception {
        // create the link
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // go to OAuth config screen of the second instance
        ListApplicationLinkPage linkPage = PRODUCT2.visit(ListApplicationLinkPage.class);
        Poller.waitUntil(linkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT.getProductInstance().getBaseUrl())));
        String url = linkPage.getApplicationLinks().get(0).getApplicationUrl();
        ApplicationDetailsSection detailsSection = linkPage.configureApplicationLink(url);
        OauthIncomingAuthenticationWithAutoConfigSection section = detailsSection.openIncomingOauthExpectingAutoConfig();

        // enable 2LO
        section = section.check2LO();
        section.set2LOExecuteAs("barney");
        section.check2LOImpersonation();
        section = section.clickUpdate2LOConfig();
        assertNull("2LO must have been enabled successfully", section.get2LOErrorMessage());

        logout(PRODUCT, PRODUCT2);

        System.out.println("Before finished");
    }

    @Test
    public void test2LORequestShouldExecuteAsDesignatedUser() {
        login("betty", "betty", PRODUCT);
        TwoLeggedOAuthTestServletPage testPage = PRODUCT.visit(TwoLeggedOAuthTestServletPage.class);
        assertTrue(testPage.getContent().contains("barney"));

        System.out.println("Test finished");
    }

    @Test
    public void test2LOWithImpersonationRequestShouldExecuteAsRequestedUser() {
        login("betty", "betty", PRODUCT);
        TwoLeggedOAuthWithImpersonationTestServletPage testPage = PRODUCT.visit(TwoLeggedOAuthWithImpersonationTestServletPage.class);
        assertTrue(testPage.getContent().contains("betty"));

        System.out.println("Test finished");
    }

}
