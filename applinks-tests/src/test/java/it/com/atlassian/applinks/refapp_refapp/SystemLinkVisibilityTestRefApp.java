package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.webdriver.applinks.page.AbstractApplicationLinkPage.ApplicationLinkEntryRow;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import com.google.common.base.Predicate;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

import static com.atlassian.applinks.test.data.applink.config.ApplinkConfigurators.configureSide;
import static com.atlassian.applinks.test.rest.data.applink.SystemLinkConfigurator.setSystemLink;
import static com.google.common.collect.Iterables.find;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

@Category(RefappTest.class)
public class SystemLinkVisibilityTestRefApp extends V2AbstractApplinksTest {
    private final ApplinksBackdoor backdoor = new ApplinksBackdoor(INSTANCE1);

    private TestApplink.Side applink1;
    private TestApplink.Side applink2;

    @Before
    public void setUp() throws Exception {
        applink1 = backdoor.createRandomGeneric();
        applink2 = backdoor.createRandomGeneric();
        configureSide(applink1, setSystemLink());
    }

    @Test
    public void testSysAdminCanSeeAllApplicationLinks() {
        loginAsSysadmin(PRODUCT);

        ListApplicationLinkPage listPage = PRODUCT.visit(ListApplicationLinkPage.class);
        List<ApplicationLinkEntryRow> rows = listPage.getApplicationLinks();
        assertEquals(2, rows.size());

        ApplicationLinkEntryRow systemRow = find(rows, byApplinkId(applink1));
        ApplicationLinkEntryRow nonSystemRow = find(rows, byApplinkId(applink2));

        assertTrue(systemRow.isReadonly());
        assertFalse(nonSystemRow.isReadonly());
    }

    @Test
    public void testAdminCanSeeAllApplicationLinksButSystemLinkAsReadonly() {
        login(PRODUCT);

        ListApplicationLinkPage listPage = PRODUCT.visit(ListApplicationLinkPage.class);
        List<ApplicationLinkEntryRow> rows = listPage.getApplicationLinks();
        assertEquals(2, rows.size());

        ApplicationLinkEntryRow systemRow = find(rows, byApplinkId(applink1));
        ApplicationLinkEntryRow nonSystemRow = find(rows, byApplinkId(applink2));

        assertTrue(systemRow.isReadonly());
        assertFalse(nonSystemRow.isReadonly());
    }

    static Predicate<ApplicationLinkEntryRow> byApplinkId(final TestApplink.Side applink) {
        return new Predicate<ApplicationLinkEntryRow>() {
            @Override
            public boolean apply(ApplicationLinkEntryRow row) {
                return applink.id().equals(row.getApplicationId());
            }
        };
    }
}
