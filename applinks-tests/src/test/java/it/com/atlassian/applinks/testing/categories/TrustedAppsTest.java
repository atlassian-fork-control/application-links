package it.com.atlassian.applinks.testing.categories;

/**
 * JUnit Category to classify tests using Trusted Apps.
 *
 * @since 4.0.15
 * @deprecated Trusted Apps is deprecated and will be removed from Applinks (along with TA-specific applinks tests)
 * WARNING: this category is not bound to any integration tests.
 * if you just add this category to your test it <b>will not be executed</b>
 * in 6.0 or a later release
 */
@Deprecated
public interface TrustedAppsTest {
}
