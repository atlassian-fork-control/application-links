package it.com.atlassian.applinks.testing.product;

import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.refapp.RefappTestedProduct;

@Defaults(instanceId = "refapp1", contextPath = "/refapp1", httpPort = 5990)
public class Refapp1TestedProduct extends RefappTestedProduct {
    public Refapp1TestedProduct(TestedProductFactory.TesterFactory<WebDriverTester> testerFactory, ProductInstance productInstance) {
        super(testerFactory, productInstance);
    }
}
