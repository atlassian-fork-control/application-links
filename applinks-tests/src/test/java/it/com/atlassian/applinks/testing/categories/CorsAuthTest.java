package it.com.atlassian.applinks.testing.categories;

/**
 * JUnit Category to classify tests targeting Cors Auth functionality.
 *
 * @since SHPXXVII-43
 */
public interface CorsAuthTest {
}
