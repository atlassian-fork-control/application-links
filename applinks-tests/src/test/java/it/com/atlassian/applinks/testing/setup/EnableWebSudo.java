package it.com.atlassian.applinks.testing.setup;

import it.com.atlassian.applinks.testing.rules.RefappWebSudoRule;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Use on test classes/methods to enable web-sudo. Currently only supported for Refapp. Requires
 * {@link RefappWebSudoRule}.
 * </p>
 * <p>
 * NOTE: test using this annotation will not be able to authenticate using {@code AuthenticationBackdoor}.
 * </p>
 *
 * @see RefappWebSudoRule
 * @since 5.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface EnableWebSudo {
}