package it.com.atlassian.applinks.smoke.analytics;

import com.atlassian.applinks.analytics.ApplinksEditEvent;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator;
import com.atlassian.webdriver.applinks.page.v3.V3EditApplinkPage;
import it.com.atlassian.applinks.smoke.AbstractDefaultSmokeTest;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.IgnoredProducts;
import it.com.atlassian.applinks.testing.rules.AnalyticsLogRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.pageobjects.LoginClient.loginAsSysadminAndGoTo;
import static it.com.atlassian.applinks.testing.specification.AnalyticsEventsExpectations.eventWithProperty;
import static it.com.atlassian.applinks.testing.specification.AnalyticsEventsExpectations.expectRetainedEvent;

@Category(SmokeTest.class)
@IgnoredProducts(value = {SupportedProducts.REFAPP, SupportedProducts.FECRU},
        reason = "Refapp does not have Analytics. Fecru is not whitelisting the event at the moment and therefore test fails - it requires a more recent version of analytics plugin")
public class ApplinkEditAnalyticsTest extends AbstractDefaultSmokeTest {
    @Rule
    public final TestApplinkRule testApplinkRule;
    @Rule
    public final AnalyticsLogRule analyticsLogRule;

    public ApplinkEditAnalyticsTest(@Nonnull GenericTestedProduct mainProduct,
                                    @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        this.testApplinkRule = new TestApplinkRule.Builder(mainInstance, backgroundInstance)
                .configure(OAuthApplinkConfigurator.enableDefaultOAuth())
                .build();
        this.analyticsLogRule = new AnalyticsLogRule(mainInstance);
    }

    @Test
    public void shouldFireApplinkEditEventWhenPageIsLoaded() {
        loginAndGoToEditPage();

        analyticsLogRule.assertLogsMatch(expectRetainedEvent(ApplinksEditEvent.class,
                eventWithProperty("applicationId", testApplinkRule.from().id()),
                eventWithProperty("applicationType", testApplinkRule.to().product().getProduct().getApplicationTypeId())));
    }

    private V3EditApplinkPage loginAndGoToEditPage() {
        return loginAsSysadminAndGoTo(mainProduct, V3EditApplinkPage.class, testApplinkRule.from());
    }
}
