package com.atlassian.applinks.core.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

import com.atlassian.applinks.AbstractClient;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.pageobjects.TestedProduct;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.framework.Version;

/**
 * @since 4.0.15
 */
public class ManifestResourceClient extends AbstractClient {
    public static ManifestResourceClient forProduct(TestedProduct<?> product) {
        return new ManifestResourceClient(product);
    }

    public ManifestResourceClient(final String baseUrl) {
        super(baseUrl);
    }

    public ManifestResourceClient(TestedProduct<?> product) {
        super(product);
    }

    /**
     * Get the manifest of the current instance
     */
    public Manifest get() {
        final String url = this.baseUrl + "/rest/applinks/2.0/manifest.json";
        try {
            HttpGet httpGet = new HttpGet(url);
            String response = execute(httpGet);

            // no concrete implementations of Manifest
            final JSONObject responseObject = new JSONObject(response);

            return new Manifest() {
                @Override
                public ApplicationId getId() {
                    try {
                        return new ApplicationId(responseObject.getString("id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public String getName() {
                    try {
                        return responseObject.getString("name");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public TypeId getTypeId() {
                    try {
                        return new TypeId(responseObject.getString("typeId"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public String getVersion() {
                    try {
                        return responseObject.getString("version");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public Long getBuildNumber() {
                    try {
                        return Long.parseLong(responseObject.getString("buildNumber"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public URI getUrl() {
                    try {
                        return new URI(responseObject.getString("url"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public URI getIconUrl() {
                    try {
                        return new URI(responseObject.getString("iconUrl"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public URI getIconUri() {
                    try {
                        return new URI(responseObject.getString("iconUri"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public Version getAppLinksVersion() {
                    try {
                        return new Version(responseObject.getString("version"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public Set<Class<? extends AuthenticationProvider>> getInboundAuthenticationTypes() {
                    return null;
                }

                @Override
                public Set<Class<? extends AuthenticationProvider>> getOutboundAuthenticationTypes() {
                    return null;
                }

                @Override
                public Boolean hasPublicSignup() {
                    try {
                        return Boolean.parseBoolean(responseObject.getString("publicSignup"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };
        } catch (Exception e) {
            throw new RuntimeException(String.format("Request to '%s' failed.", url), e);
        }
    }
}
