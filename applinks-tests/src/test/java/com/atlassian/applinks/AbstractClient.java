package com.atlassian.applinks;

import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

import static java.util.Objects.requireNonNull;


/**
 * @since 4.0.15
 */
public abstract class AbstractClient {
    protected final String baseUrl;
    protected final HttpClient httpClient = new DefaultHttpClient();

    protected AbstractClient(final String baseUrl) {
        this.baseUrl = baseUrl;
    }

    protected AbstractClient(ProductInstance productInstance) {
        this.baseUrl = requireNonNull(productInstance, "productInstance").getBaseUrl();
    }

    /**
     * @param product the product to run against
     * @deprecated causes unnecessary browser start-up, use {@link #AbstractClient(ProductInstance)} instead
     */
    @Deprecated
    protected AbstractClient(TestedProduct<?> product) {
        this(product.getProductInstance());
    }

    protected String execute(HttpUriRequest request) {
        try {
            byte[] encodedAuthorization = org.apache.commons.codec.binary.Base64.encodeBase64("admin:admin".getBytes());
            request.setHeader("Authorization", "Basic " + new String(encodedAuthorization));
            return httpClient.execute(request, new BasicResponseHandler());
        } catch (IOException e) {
            throw new RuntimeException("Unable to complete request " + request.getURI().toASCIIString(), e);
        }
    }
}
