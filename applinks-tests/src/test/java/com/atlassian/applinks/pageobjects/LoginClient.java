package com.atlassian.applinks.pageobjects;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.applinks.page.EchoPage;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.google.common.collect.ImmutableMap;

import java.util.Map;
import javax.annotation.Nonnull;

import static com.atlassian.applinks.test.authentication.TestAuthentication.authenticatedWith;
import static com.google.common.base.Preconditions.checkNotNull;
import static it.com.atlassian.applinks.testing.product.ProductInstances.fromProductInstance;

/**
 * Helper class for using pageobjects to login and out of products.
 *
 * @since 5.0.0
 */
public class LoginClient {
    private static final Map<SupportedProducts, LoginStrategy> LOGIN_STRATEGIES =
            ImmutableMap.<SupportedProducts, LoginStrategy>of(
                    SupportedProducts.BITBUCKET, PageLogin.BITBUCKET,
                    SupportedProducts.BAMBOO, PageLogin.BAMBOO,
                    SupportedProducts.FECRU, PageLogin.FECRU
            );

    @SafeVarargs
    public static void loginAsSysadmin(TestedProduct<WebDriverTester>... products) {
        login(TestAuthentication.admin(), products);
    }

    @Nonnull
    public static <P extends Page> P loginAsSysadminAndGoTo(@Nonnull TestedProduct<WebDriverTester> product,
                                                            @Nonnull Class<P> targetPage,
                                                            @Nonnull Object... args) {
        return loginAndGoTo(TestAuthentication.admin(), product, targetPage, args);
    }

    @SafeVarargs
    public static void login(@Nonnull TestAuthentication authentication,
                             @Nonnull TestedProduct<WebDriverTester>... products) {
        checkNotNull(authentication, "authentication");
        checkNotNull(products, "products");

        for (TestedProduct<WebDriverTester> product : products) {
            getLoginStrategy(getSupportedProduct(product)).login(product, authentication);
        }
    }

    @Nonnull
    public static <P extends Page> P loginAndGoTo(@Nonnull TestAuthentication authentication,
                                                  @Nonnull TestedProduct<WebDriverTester> product,
                                                  @Nonnull Class<P> targetPage,
                                                  @Nonnull Object... args) {
        checkNotNull(authentication, "authentication");
        checkNotNull(product, "product");
        checkNotNull(targetPage, "targetPage");
        checkNotNull(args, "args");

        login(authentication, product);
        return product.visit(targetPage, args);
    }

    @SafeVarargs
    public static void login(@Nonnull String username, @Nonnull String password,
                             @Nonnull TestedProduct<WebDriverTester>... products) {
        login(authenticatedWith(username, password), products);
    }

    @SafeVarargs
    public static void logout(TestedProduct<WebDriverTester>... products) {
        if (products.length == 0) {
            throw new IllegalArgumentException("You did not provide any products to log out of. That's probably not what you want");
        }
        for (TestedProduct<WebDriverTester> product : products) {
            product.visit(EchoPage.class);
            product.getTester().getDriver().manage().deleteAllCookies();
        }
    }

    private static LoginStrategy getLoginStrategy(SupportedProducts product) {
        LoginStrategy login = LOGIN_STRATEGIES.get(product);
        return login == null ? BackdoorLogin.INSTANCE : login;
    }

    private static SupportedProducts getSupportedProduct(TestedProduct<WebDriverTester> product) {
        return fromProductInstance(product.getProductInstance()).getProduct();
    }
}
