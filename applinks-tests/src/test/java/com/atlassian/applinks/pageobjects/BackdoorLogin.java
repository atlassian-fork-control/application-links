package com.atlassian.applinks.pageobjects;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.backdoor.AuthenticationBackdoor;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.applinks.page.EchoPage;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.openqa.selenium.Cookie;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.test.rest.backdoor.AuthenticationBackdoor.AUTHENTICATION_COOKIE_NAME;

/**
 * Quick login using REST backdoor. Only works for products that create an authenticated session when logged in using
 * Basic auth, which strictly speaking is not the right behaviour. But hey it works and it's quick
 *
 * @since 5.0
 */
public class BackdoorLogin implements LoginStrategy {
    public static final BackdoorLogin INSTANCE = new BackdoorLogin();

    @Override
    public void login(@Nonnull TestedProduct<WebDriverTester> product, @Nonnull TestAuthentication authentication) {
        String authCookie = new AuthenticationBackdoor(product.getProductInstance().getBaseUrl())
                .getAuthentication(authentication);
        // adding cookie is only reliable if we're in the right domain
        EchoPage.goToIfNeeded(product);
        product.getTester().getDriver().manage().addCookie(new Cookie(
                AUTHENTICATION_COOKIE_NAME,
                authCookie,
                product.getProductInstance().getContextPath()));
    }
}
