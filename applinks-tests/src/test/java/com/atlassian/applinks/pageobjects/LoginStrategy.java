package com.atlassian.applinks.pageobjects;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import javax.annotation.Nonnull;

public interface LoginStrategy {
    void login(@Nonnull TestedProduct<WebDriverTester> product, @Nonnull TestAuthentication authentication);
}
