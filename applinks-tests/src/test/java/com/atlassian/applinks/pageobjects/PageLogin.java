package com.atlassian.applinks.pageobjects;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.applinks.externalpage.GenericLoginPage;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;

import static com.atlassian.webdriver.applinks.externalpage.GenericLoginPage.DEFAULT_WAIT_FOR_AFTER_LOGIN;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Login using loging page. Uses a generic, customizable login page that needs to know the URL and username/password
 * field locator.
 *
 * @since 4.3
 */
public class PageLogin implements LoginStrategy {
    public static final PageLogin BITBUCKET = new PageLogin("/login", "j_username", "j_password", "submit");
    public static final PageLogin BAMBOO = new PageLogin("/userlogin!default.action", "loginForm_os_username",
            "loginForm_os_password", "loginForm_save");
    // waiting for .article after login, which makes sure the "recent activity" AJAX requests have finished
    public static final PageLogin FECRU = new PageLogin("/login", By.id("username"), By.id("password"),
            By.id("loginButton"), By.className("article"));

    private final String loginUrl;
    private final By usernameLocator;
    private final By passwordLocator;
    private final By submitButtonLocator;
    private final By waitForAfterLogin;

    PageLogin(@Nonnull String loginUrl, @Nonnull By usernameLocator, @Nonnull By passwordLocator,
              @Nonnull By submitButtonLocator, @Nonnull By waitForAfterLogin) {
        this.loginUrl = checkNotNull(loginUrl, "loginUrl");
        this.usernameLocator = checkNotNull(usernameLocator, "usernameLocator");
        this.passwordLocator = checkNotNull(passwordLocator, "passwordLocator");
        this.submitButtonLocator = checkNotNull(submitButtonLocator, "submitButtonLocator");
        this.waitForAfterLogin = checkNotNull(waitForAfterLogin, "waitForAfterLogin");
    }

    PageLogin(@Nonnull String loginUrl, @Nonnull By usernameLocator, @Nonnull By passwordLocator,
              @Nonnull By submitButtonLocator) {
        this(loginUrl, usernameLocator, passwordLocator, submitButtonLocator, DEFAULT_WAIT_FOR_AFTER_LOGIN);
    }

    PageLogin(@Nonnull String loginUrl, @Nonnull String usernameId, @Nonnull String passwordId,
              @Nonnull String submitId) {
        this(loginUrl, By.id(usernameId), By.id(passwordId), By.id(submitId));
    }


    @Override
    @SuppressWarnings("ConstantConditions")
    public void login(@Nonnull TestedProduct<WebDriverTester> product, @Nonnull TestAuthentication authentication) {
        product.visit(GenericLoginPage.class, loginUrl, usernameLocator, passwordLocator, submitButtonLocator,
                waitForAfterLogin).login(authentication.getUsername(), authentication.getPassword());
    }
}
