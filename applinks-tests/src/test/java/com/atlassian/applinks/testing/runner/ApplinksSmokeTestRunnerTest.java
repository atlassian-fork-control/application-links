package com.atlassian.applinks.testing.runner;

import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.Tester;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.IgnoredProducts;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.product.Products;
import it.com.atlassian.applinks.testing.runner.ApplinksSmokeTestRunner;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;
import org.mockito.Mockito;

import java.util.Arrays;
import javax.annotation.Nonnull;

import static com.atlassian.applinks.testing.matchers.ErrorMatchers.withMessageThat;
import static com.atlassian.applinks.testing.matchers.JUnitDescriptionMatchers.withMethodName;
import static com.atlassian.applinks.testing.matchers.JUnitFailureMatchers.withExceptionThat;
import static com.atlassian.applinks.testing.matchers.SmokeTestDescriptionMatchers.smokeTestWith;
import static it.com.atlassian.applinks.testing.product.ProductInstances.CONFLUENCE;
import static it.com.atlassian.applinks.testing.product.ProductInstances.JIRA;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
public class ApplinksSmokeTestRunnerTest {
    private static final String APPLINKS_SMOKE_INSTANCE1 = "applinks.smoke.instance1";
    private static final String APPLINKS_SMOKE_INSTANCE2 = "applinks.smoke.instance2";

    @Test(expected = InitializationError.class)
    public void testInitErrorIfInstance1IsNull() throws Exception {
        setUpInstances(null, CONFLUENCE.getInstanceId());
        new ApplinksSmokeTestRunner(SmokeTestForTesting.class);
    }

    @Test(expected = InitializationError.class)
    public void testInitErrorIfInstance2IsNull() throws Exception {
        setUpInstances(JIRA.getInstanceId(), null);
        new ApplinksSmokeTestRunner(SmokeTestForTesting.class);
    }

    @Test(expected = InitializationError.class)
    public void testInitErrorIfInstance1IsBlank() throws Exception {
        setUpInstances("  ", CONFLUENCE.getInstanceId());
        new ApplinksSmokeTestRunner(SmokeTestForTesting.class);
    }

    @Test(expected = InitializationError.class)
    public void testInitErrorIfInstance2IsBlank() throws Exception {
        setUpInstances(JIRA.getInstanceId(), "   ");
        new ApplinksSmokeTestRunner(SmokeTestForTesting.class);
    }

    @Test(expected = InitializationError.class)
    public void testInitErrorIfInstance1IsInvalid() throws Exception {
        setUpInstances("nosuchinstance", CONFLUENCE.getInstanceId());
        new ApplinksSmokeTestRunner(SmokeTestForTesting.class);
    }

    @Test(expected = InitializationError.class)
    public void testInitErrorIfInstance2IsInvalid() throws Exception {
        setUpInstances(JIRA.getInstanceId(), "nosuchinstance");
        new ApplinksSmokeTestRunner(SmokeTestForTesting.class);
    }

    @Test
    public void testInitErrorGivenZeroArgTestConstructor() {
        setUpInstances(JIRA, CONFLUENCE);
        JUnitResultListener listener = runTest(SmokeTestWithZeroArgConstructor.class);
        assertThat(listener.getTestFailures(), contains(
                withExceptionThat(withMessageThat(containsInOrder("invalid number of arguments", "<2>", "<0>")))
        ));
    }

    @Test
    public void testInitErrorGivenOneArgValidTypeTestConstructor() {
        setUpInstances(JIRA, CONFLUENCE);
        JUnitResultListener listener = runTest(SmokeTestWithOneArgValidTypeConstructor.class);
        assertThat(listener.getTestFailures(), contains(
                withExceptionThat(withMessageThat(containsInOrder("invalid number of arguments", "<2>", "<1>")))
        ));
    }

    @Test
    public void testInitErrorGivenInvalidArgTypeTestConstructor() {
        setUpInstances(JIRA, CONFLUENCE);
        JUnitResultListener listener = runTest(SmokeTestWithSecondArgInvalidTypeConstructor.class);
        assertThat(listener.getTestFailures(), contains(
                withExceptionThat(withMessageThat(containsInOrder("Unsupported", "argument type", "java.lang.String")))
        ));
    }

    @Test
    public void testInitErrorGivenIgnoredAndSupportedProductTogetherOnTestClass() {
        setUpInstances(JIRA, CONFLUENCE);
        JUnitResultListener listener = runTest(SmokeTestWithIgnoredAndSupportedProduct.class);
        assertThat(listener.getTestFailures(), contains(
                withExceptionThat(withMessageThat(
                        containsString("Test class has both @Products and @IgnoredProducts annotation"))))
        );
    }

    @Test
    public void testInitErrorGivenIgnoredAndSupportedProductTogetherOnTestMethod() {
        setUpInstances(JIRA, CONFLUENCE);
        JUnitResultListener listener = runTest(SmokeTestWithIgnoredAndSupportedProductInMethod.class);
        assertThat(listener.getTestFailures(), contains(
                withExceptionThat(withMessageThat(
                        containsString("Test method 'badlyAnnotatedTest' has both @Products and @IgnoredProducts"))))
        );
    }

    @Test
    public void testConfluenceAndJira() {
        setUpInstances(JIRA, CONFLUENCE);

        JUnitResultListener result = runTest(SmokeTestForTesting.class);

        assertTrue("Smoke test run failed: " + result.getTestFailureDetails(), result.isSuccessful());
        assertThat(result.getSuccessfulTests(), Matchers.<Description>iterableWithSize(4));
        assertThat(result.getSuccessfulTests(), Matchers.hasItems(
                smokeTestWith("testForAllProducts", JIRA, CONFLUENCE),
                smokeTestWith("testForAllProducts", CONFLUENCE, JIRA),
                smokeTestWith("testForJira", JIRA, CONFLUENCE),
                smokeTestWith("testForConfluence", CONFLUENCE, JIRA)
        ));
    }

    @Test
    public void testRefappSupportedOnClassLevel() {
        setUpInstances(JIRA, CONFLUENCE);

        // run against test class that supports JIRA and REFAPP, however method annotations should take precedence
        JUnitResultListener result = runTest(SmokeTestForRefappAndJira.class);

        assertTrue("Smoke test run failed: " + result.getTestFailureDetails(), result.isSuccessful());
        assertThat(result.getSuccessfulTests(), Matchers.<Description>iterableWithSize(3));
        assertThat(result.getSuccessfulTests(), Matchers.hasItems(
                smokeTestWith("testForAllProducts", JIRA, CONFLUENCE),
                // confluence not supported on class level, CONF -> JIRA won't be run
                smokeTestWith("testForJira", JIRA, CONFLUENCE), // method annotation takes precedence
                smokeTestWith("testForConfluence", CONFLUENCE, JIRA) // method annotation takes precedence
        ));
    }

    @Test
    public void addsIgnoresOnlyForMethodsThatDoNotSupportAnyProduct() {
        setUpInstances(REFAPP1, REFAPP2);

        JUnitResultListener result = runTest(SmokeTestForTesting.class);

        assertTrue("Smoke test run failed: " + result.getTestFailureDetails(), result.isSuccessful());
        // assert successful tests - the one supporting any products
        assertThat(result.getSuccessfulTests(), Matchers.containsInAnyOrder(
                smokeTestWith("testForAllProducts", REFAPP1, REFAPP2),
                smokeTestWith("testForAllProducts", REFAPP2, REFAPP1)
        ));

        // tests for JIRA and Confluence should be marked ignored (assumption failed)
        assertThat(result.getFailedAssumptions(), Matchers.containsInAnyOrder(
                withMethodName("testForJira"),
                withMethodName("testForConfluence")
        ));
    }

    @Test
    public void testForJiraTestedProductOnly() {
        setUpInstances(JIRA, CONFLUENCE);

        JUnitResultListener result = runTest(SmokeTestForJiraTestedProductOnly.class);

        assertTrue("Smoke test run failed: " + result.getTestFailureDetails(), result.isSuccessful());
        // assert successful tests
        assertThat(result.getSuccessfulTests(), Matchers.containsInAnyOrder(
                smokeTestWith("testOne", JIRA, CONFLUENCE),
                smokeTestWith("testTwo", JIRA, CONFLUENCE)
        ));
    }

    @Test
    public void testForAllProductsTwoGenericTestedProducts() {
        setUpInstances(JIRA, CONFLUENCE);

        JUnitResultListener result = runTest(SmokeTestForAllProductsUsingGenericTestedProducts.class);

        assertTrue("Smoke test run failed: " + result.getTestFailureDetails(), result.isSuccessful());
        // assert successful tests
        assertThat(result.getSuccessfulTests(), Matchers.containsInAnyOrder(
                smokeTestWith("testOne", JIRA, CONFLUENCE),
                smokeTestWith("testOne", CONFLUENCE, JIRA),
                smokeTestWith("testTwo", JIRA, CONFLUENCE),
                smokeTestWith("testTwo", CONFLUENCE, JIRA)
        ));
    }

    @Test
    public void testIgnoredProducts() {
        setUpInstances(JIRA, CONFLUENCE);

        JUnitResultListener result = runTest(SmokeTestWithIgnoredConfluence.class);

        assertTrue("Smoke test run failed: " + result.getTestFailureDetails(), result.isSuccessful());
        assertThat(result.getSuccessfulTests(), Matchers.iterableWithSize(3));
        // 3 tests as testForAllProducts is ignored for CONFLUENCE
        assertThat(result.getSuccessfulTests(), Matchers.containsInAnyOrder(
                smokeTestWith("testForAllProducts", JIRA, CONFLUENCE),
                smokeTestWith("testForJira", JIRA, CONFLUENCE),
                smokeTestWith("testForConfluence", CONFLUENCE, JIRA)
        ));
    }

    private static void setUpInstances(String instance1Id, String instance2Id) {
        if (instance1Id != null) {
            System.setProperty(APPLINKS_SMOKE_INSTANCE1, instance1Id);
        } else {
            System.clearProperty(APPLINKS_SMOKE_INSTANCE1);
        }

        if (instance2Id != null) {
            System.setProperty(APPLINKS_SMOKE_INSTANCE2, instance2Id);
        } else {
            System.clearProperty(APPLINKS_SMOKE_INSTANCE2);
        }
    }

    private static void setUpInstances(ProductInstances instance1, ProductInstances instance2) {
        setUpInstances(instance1.getInstanceId(), instance2.getInstanceId());
    }

    private static JUnitResultListener runTest(Class<?> testClass) {
        JUnitResultListener result = new JUnitResultListener();
        JUnitCore core = new JUnitCore();
        core.addListener(result);
        core.run(testClass);

        return result;
    }

    private static Matcher<String> containsInOrder(String... substrings) {
        return stringContainsInOrder(Arrays.asList(substrings));
    }

    @RunWith(ApplinksSmokeTestRunner.class)
    public static class SmokeTestForTesting {
        protected final ProductInstance mainInstance;
        protected final ProductInstance backgroundInstance;

        public SmokeTestForTesting(ProductInstance mainInstance, ProductInstance backgroundInstance) {
            this.mainInstance = mainInstance;
            this.backgroundInstance = backgroundInstance;
        }

        @Test
        public void testForAllProducts() {
        }

        @Test
        @Products(SupportedProducts.JIRA)
        public void testForJira() {
        }

        @Test
        @Products(SupportedProducts.CONFLUENCE)
        public void testForConfluence() {
        }
    }

    @Products({SupportedProducts.REFAPP, SupportedProducts.JIRA})
    @RunWith(ApplinksSmokeTestRunner.class)
    public static class SmokeTestForRefappAndJira extends SmokeTestForTesting {
        public SmokeTestForRefappAndJira(ProductInstance mainInstance, ProductInstance backgroundInstance) {
            super(mainInstance, backgroundInstance);
        }
    }

    @Products(SupportedProducts.JIRA)
    @RunWith(SmokeTestRunnerNoWebDriver.class)
    public static class SmokeTestForJiraTestedProductOnly {
        public SmokeTestForJiraTestedProductOnly(JiraTestedProduct jira, ProductInstance otherProduct) {
        }

        @Test
        public void testOne() {
        }

        @Test
        public void testTwo() {
        }
    }

    @IgnoredProducts(SupportedProducts.CONFLUENCE)
    @RunWith(ApplinksSmokeTestRunner.class)
    public static class SmokeTestWithIgnoredConfluence extends SmokeTestForTesting {
        public SmokeTestWithIgnoredConfluence(ProductInstance mainInstance, ProductInstance backgroundInstance) {
            super(mainInstance, backgroundInstance);
        }
    }

    @Products(SupportedProducts.JIRA)
    @IgnoredProducts(SupportedProducts.CONFLUENCE)
    @RunWith(ApplinksSmokeTestRunner.class)
    public static class SmokeTestWithIgnoredAndSupportedProduct extends SmokeTestForTesting {
        public SmokeTestWithIgnoredAndSupportedProduct(ProductInstance mainInstance, ProductInstance backgroundInstance) {
            super(mainInstance, backgroundInstance);
        }
    }

    @RunWith(ApplinksSmokeTestRunner.class)
    public static class SmokeTestWithIgnoredAndSupportedProductInMethod extends SmokeTestForTesting {
        public SmokeTestWithIgnoredAndSupportedProductInMethod(ProductInstance mainInstance, ProductInstance backgroundInstance) {
            super(mainInstance, backgroundInstance);
        }

        @Products(SupportedProducts.JIRA)
        @IgnoredProducts(SupportedProducts.CONFLUENCE)
        public void badlyAnnotatedTest() {
        }
    }

    @RunWith(SmokeTestRunnerNoWebDriver.class)
    public static class SmokeTestForAllProductsUsingGenericTestedProducts {
        public SmokeTestForAllProductsUsingGenericTestedProducts(GenericTestedProduct from, GenericTestedProduct to) {
        }

        @Test
        public void testOne() {
        }

        @Test
        public void testTwo() {
        }
    }

    @RunWith(ApplinksSmokeTestRunner.class)
    public static class SmokeTestWithZeroArgConstructor {
        @Test
        public void someTest() {
        }
    }

    @RunWith(ApplinksSmokeTestRunner.class)
    @SuppressWarnings("UnusedParameters")
    public static class SmokeTestWithOneArgValidTypeConstructor {
        public SmokeTestWithOneArgValidTypeConstructor(ProductInstance instance) {
        }

        @Test
        public void someTest() {
        }
    }

    @RunWith(ApplinksSmokeTestRunner.class)
    @SuppressWarnings("UnusedParameters")
    public static class SmokeTestWithSecondArgInvalidTypeConstructor {
        public SmokeTestWithSecondArgInvalidTypeConstructor(ProductInstance instance, String invalidArg) {
        }

        @Test
        public void someTest() {
        }
    }

    /**
     * Overrides {@code ApplinksSmokeTestRunner} to create tested products without instantiating the browser.
     */
    public static class SmokeTestRunnerNoWebDriver extends ApplinksSmokeTestRunner {

        public SmokeTestRunnerNoWebDriver(Class<?> klass) throws InitializationError {
            super(klass);
        }

        @Nonnull
        @Override
        protected TestedProduct createTestedProduct(@Nonnull Class<? extends TestedProduct> testedProductClass,
                                                    @Nonnull ProductInstances instance) {
            AtlassianWebDriver webDriver = Mockito.mock(AtlassianWebDriver.class);
            WebDriverTester mockTester = Mockito.mock(WebDriverTester.class);
            when(mockTester.getDriver()).thenReturn(webDriver);
            return TestedProductFactory.create(testedProductClass, instance,
                    new TestedProductFactory.SingletonTesterFactory<Tester>(mockTester));

        }
    }
}


