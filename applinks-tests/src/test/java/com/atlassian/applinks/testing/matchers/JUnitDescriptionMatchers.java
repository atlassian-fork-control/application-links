package com.atlassian.applinks.testing.matchers;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.hamcrest.Matchers.is;

/**
 * @since 4.3
 */
public final class JUnitDescriptionMatchers {
    private JUnitDescriptionMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<Description> withTestClass(@Nullable Class<?> expectedTestClass) {
        return withTestClassThat(Matchers.<Class<?>>is(expectedTestClass));
    }

    @Nonnull
    public static Matcher<Description> withTestClassThat(@Nonnull Matcher<Class<?>> nameMatcher) {
        return new FeatureMatcher<Description, Class<?>>(nameMatcher, "name", "name that") {
            @Override
            protected Class<?> featureValueOf(Description actual) {
                return actual.getTestClass();
            }
        };
    }

    @Nonnull
    public static Matcher<Description> withMethodName(@Nullable String expectedName) {
        return withMethodNameThat(is(expectedName));
    }

    @Nonnull
    public static Matcher<Description> withMethodNameThat(@Nonnull Matcher<String> nameMatcher) {
        return new FeatureMatcher<Description, String>(nameMatcher, "name", "name that") {
            @Override
            protected String featureValueOf(Description actual) {
                return actual.getMethodName();
            }
        };
    }
}
