package com.atlassian.applinks.testing.matchers;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.Matchers.is;

/**
 * Matchers for Java {@code Throwable}s.
 *
 * @since 4.3
 */
public final class ErrorMatchers {
    private ErrorMatchers() {
        throw new UnsupportedOperationException("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<Throwable> withMessage(@Nonnull String expectedMessage) {
        return withMessageThat(is(expectedMessage));
    }

    @Nonnull
    public static Matcher<Throwable> withMessageThat(@Nonnull Matcher<String> messageMatcher) {
        return new FeatureMatcher<Throwable, String>(checkNotNull(messageMatcher, "messageMatcher"), "message",
                "message that") {
            @Override
            protected String featureValueOf(Throwable error) {
                return error.getMessage();
            }
        };
    }
}
