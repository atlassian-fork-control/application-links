package com.atlassian.applinks.testing.runner;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.collect.Iterables.transform;

/**
 * Collects JUnit results for verification.
 *
 * @since 4.3
 */
public final class JUnitResultListener extends RunListener {
    private static final Function<Failure, Description> GET_FAILURE_DESCRIPTION = new Function<Failure, Description>() {
        @Override
        public Description apply(Failure failure) {
            return failure.getDescription();
        }
    };

    private final Set<Description> succeeded = new HashSet<>();
    private final Set<Description> ignored = new HashSet<>();
    private final List<Failure> assumptionFailed = new ArrayList<>();
    private final List<Failure> failed = new ArrayList<>();

    @Override
    public void testStarted(Description description) throws Exception {
        succeeded.add(description);
    }

    @Override
    public void testIgnored(Description description) throws Exception {
        ignored.add(description);
    }

    @Override
    public void testAssumptionFailure(Failure failure) {
        assumptionFailed.add(failure);
        succeeded.remove(failure.getDescription());
    }

    @Override
    public void testFailure(Failure failure) throws Exception {
        failed.add(failure);
        succeeded.remove(failure.getDescription());
    }

    @Nonnull
    public Set<Description> getSuccessfulTests() {
        return Collections.unmodifiableSet(succeeded);
    }

    @Nonnull
    public Set<Description> getIgnoredTests() {
        return Collections.unmodifiableSet(ignored);
    }

    @Nonnull
    public Iterable<Failure> getAssumptionFailures() {
        return Collections.unmodifiableList(assumptionFailed);
    }

    @Nonnull
    public Iterable<Description> getFailedAssumptions() {
        return Collections.unmodifiableList(assumptionFailed.stream().map(GET_FAILURE_DESCRIPTION).collect(Collectors.toList()));
    }

    @Nonnull
    public Iterable<Failure> getTestFailures() {
        return Collections.unmodifiableList(failed);
    }

    @Nonnull
    public Iterable<String> getTestFailureDetails() {
        return ImmutableList.copyOf(Iterables.transform(getTestFailures(), new Function<Failure, String>() {
            @Nullable
            @Override
            public String apply(Failure failure) {
                return "\n\n" + failure.toString() + ":\n" + failure.getTrace() + "\n\n";
            }
        }));
    }

    @Nonnull
    public Iterable<Description> getFailedTests() {
        return ImmutableList.copyOf(transform(failed, GET_FAILURE_DESCRIPTION));
    }

    public boolean isSuccessful() {
        return !hasFailures();
    }

    public boolean hasFailures() {
        return !failed.isEmpty();
    }

    public boolean hasAssumptionFailures() {
        return !assumptionFailed.isEmpty();
    }
}
