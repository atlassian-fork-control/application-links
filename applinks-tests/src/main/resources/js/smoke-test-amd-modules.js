require([
    'applinks/lib/console',
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/lib/wrm',
    'applinks/lib/aui',
    'applinks/lib/backbone'
], function (
    console,
    $,
    _,
    WRM,
    AJS,
    Backbone
) {
    function logIfMissing(module, id) {
        if (!module) {
            console.error(id + ' is missing');
            return false;
        } else {
            return true;
        }
    }

    $(function () {
        var result = logIfMissing($, 'applinks/lib/jquery');
        result &= logIfMissing(_, 'applinks/lib/lodash');
        result &= logIfMissing(WRM, 'applinks/lib/wrm');
        result &= logIfMissing(AJS, 'applinks/lib/aui');
        result &= logIfMissing(Backbone, 'applinks/lib/backbone');

        if (result) {
            $('body').attr('data-amd-modules-loaded', 'true');
        }
    });
});
