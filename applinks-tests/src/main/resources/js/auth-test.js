
AJS.$(function () {

    function isRefreshDisabled() {
        return AJS.$('#disableRefresh:checked').val();
    }
    
    function handleAuthorizedEvent(eventObject, applinkProperties) {
        if (isRefreshDisabled()) {
            AJS.$('#statusMessage').html('<div class="success" id="result">User has authorized "' + applinkProperties.appName + '"; refresh disabled</div>');
            eventObject.preventDefault();
        }
    }
    
    function handleDeniedEvent(eventObject, applinkProperties) {
        AJS.$('#statusMessage').html('<div class="error" id="result">User has denied access to "' + applinkProperties.appName + '"</div>');
    }
    
    AJS.$(document).bind('applinks.auth.approved', handleAuthorizedEvent);
    AJS.$(document).bind('applinks.auth.denied', handleDeniedEvent);
    
    AJS.$('.applinks-auth-request').each(function() {
        var $e = AJS.$(this),
            params = {
                'id': $e.find('.applinkId').val(),
                'appName': $e.find('.appName').val(),
                'appUri': $e.find('.appUri').val(),
                'authUri': $e.find('.authUri').val()
            },
            useInlineFormat = (AJS.$('#useInlineFormat').val() == 'true'),
            inlineDescription = AJS.$('#inlineDescription').val(),
            $addDiv = AJS.$('<div class="test-auth-message created-from-js">message created from JavaScript: </div>');
        if (AJS.$('#useInlineFormat').val() == 'true') {
            $addDiv.append(ApplinksUtils.createAuthRequestInline(inlineDescription, params));
        }
        else {
            $addDiv.append(ApplinksUtils.createAuthRequestBanner(params));
        }
        $e.parent().after($addDiv);
    });
});
