package com.atlassian.applinks;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.internal.test.rest.TestExceptionInterceptor;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.IdentifiableType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.applinks.test.rest.model.RestProperty;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.websudo.WebSudoNotRequired;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.internal.common.rest.util.RestApplicationIdParser.parseApplicationId;

@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("applinks")
@InterceptorChain({TestExceptionInterceptor.class, NoCacheHeaderInterceptor.class})
public class ApplinksTestResource {
    private static final Logger log = LoggerFactory.getLogger(ApplinksTestResource.class);

    private final MutatingApplicationLinkService linkService;
    private final TypeAccessor typeAccessor;
    private final ManifestRetriever manifestRetriever;
    private final InternalHostApplication hostApplication;

    public ApplinksTestResource(MutatingApplicationLinkService linkService,
                                TypeAccessor typeAccessor,
                                ManifestRetriever manifestRetriever,
                                InternalHostApplication hostApplication) {
        this.linkService = linkService;
        this.typeAccessor = typeAccessor;
        this.manifestRetriever = manifestRetriever;
        this.hostApplication = hostApplication;
    }

    @POST
    @WebSudoNotRequired
    @XsrfProtectionExcluded
    public Response create(CreateRequestEntity entity)
            throws Exception {
        log.debug("Received applinks create request: " + entity);

        final URI uri = URI.create(entity.baseUrl);
        ApplicationType type = getApplicationType(uri, entity);
        if (entity.twoWay) {
            linkService.createReciprocalLink(uri, null, entity.username, entity.password);
        }
        ApplicationLink link = linkService.createApplicationLink(type, ApplicationLinkDetails.builder()
                .displayUrl(uri)
                .rpcUrl(uri)
                .name(entity.name)
                .isPrimary(entity.primary)
                .build());
        if (entity.urlOverride != null) {
            log.debug("Overriding display and RPC URL with " + entity.urlOverride);
            ApplicationLinkDetails newDetails = ApplicationLinkDetails.builder()
                    .name(link.getName())
                    .rpcUrl(URI.create(entity.urlOverride))
                    .displayUrl(URI.create(entity.urlOverride))
                    .build();
            ((MutableApplicationLink) link).update(newDetails);
        }
        if (entity.twoWay) {
            configureTwoWayAuth(entity, link);

        }
        return Response.ok(new CreateResponseEntity(link.getId(), hostApplication.getId())).build();
    }

    @AnonymousAllowed
    @GET
    public Response all() {
        final Iterable<ApplicationLink> links = linkService.getApplicationLinks();
        return Response.ok(Iterables.transform(links, new Function<ApplicationLink, ApplinkEntity>() {
            public ApplinkEntity apply(ApplicationLink link) {
                return new ApplinkEntity(link);
            }
        })).build();
    }

    @AnonymousAllowed
    @GET
    @Path("{applinkid}")
    public Response singleApplink(@PathParam("applinkid") String applinkId) throws TypeNotInstalledException {
        final ApplicationLink applink = linkService.getApplicationLink(new ApplicationId(applinkId));
        return Response.ok(new ApplinkEntity(applink)).build();
    }

    @DELETE
    @WebSudoNotRequired
    public Response cleanAll(DeleteAllRequestEntity request) {
        int count = 0;
        for (ApplicationLink link : linkService.getApplicationLinks()) {

            if (request.reciprocal) {
                try {
                    linkService.deleteReciprocatedApplicationLink(link);
                } catch (Exception e) {
                    linkService.deleteApplicationLink(link);
                }
            } else {
                linkService.deleteApplicationLink(link);
            }
            count++;
        }
        return Response.ok(new DeleteAllResponseEntity(count)).build();
    }

    @PUT
    @Path("{applinkId}/system")
    public Response setSystemLink(@PathParam("applinkId") String applinkId) {
        return setSystemFlag(applinkId, true);
    }

    @DELETE
    @Path("{applinkId}/system")
    public Response setNonSystemLink(@PathParam("applinkId") String applinkId) {
        return setSystemFlag(applinkId, false);
    }

    // allows for updating rpcUrl without too much drama
    @PUT
    @Path("{applinkId}")
    public Response update(@PathParam("applinkId") String applinkId, ApplinkEntity entity) {
        try {
            MutableApplicationLink link = linkService.getApplicationLink(new ApplicationId(applinkId));
            ApplicationLinkDetails.Builder builder = ApplicationLinkDetails.builder(link);
            if (entity.rpcUrl != null) {
                builder.rpcUrl(URI.create(entity.rpcUrl));
            }
            if (entity.displayUrl != null) {
                builder.displayUrl(URI.create(entity.displayUrl));
            }
            if (entity.name != null) {
                builder.name(entity.name);
            }
            // add more fields as necessary
            link.update(builder.build());
            return Response.ok(new ApplinkEntity(link)).build();
        } catch (TypeNotInstalledException e) {
            return Response.serverError()
                    .entity("Failed to set applink system flag for '" + applinkId + "': " + e)
                    .build();
        }
    }

    @PUT
    @Path("{applinkId}/property/{key}")
    public Response setProperty(@PathParam("applinkId") String applinkId, @PathParam("key") String key,
                                RestProperty restProperty)
            throws TypeNotInstalledException {
        ApplicationId id = parseApplicationId(applinkId);
        ApplicationLink applink = linkService.getApplicationLink(id);
        applink.putProperty(key, restProperty.getValue());
        return Response.created(URI.create("")).entity(restProperty).build();
    }

    @DELETE
    @Path("{applinkId}/property/{key}")
    public Response removeProperty(@PathParam("applinkId") String applinkId, @PathParam("key") String key)
            throws TypeNotInstalledException {
        ApplicationId id = parseApplicationId(applinkId);
        ApplicationLink applink = linkService.getApplicationLink(id);
        applink.removeProperty(key);
        return Response.noContent().build();
    }

    private ApplicationType getApplicationType(URI baseUrl, CreateRequestEntity entity) {
        if (entity.typeId != null) {
            return typeAccessor.loadApplicationType(new TypeId(entity.typeId));
        }
        try {
            final Manifest manifest = manifestRetriever.getManifest(baseUrl);
            return typeAccessor.loadApplicationType(manifest.getTypeId());
        } catch (Exception e) {
            // it might be a generic so don't worry yet
            log.info("Could not load manifest from remoteUri, creating a generic type link", e);
            return typeAccessor.loadApplicationType(new TypeId(GenericApplicationType.class.getCanonicalName()));
        }
    }

    private Response setSystemFlag(String applinkId, boolean value) {
        try {
            linkService.setSystem(new ApplicationId(applinkId), value);
            return Response.noContent().build();
        } catch (TypeNotInstalledException e) {
            return Response.serverError()
                    .entity("Failed to set applink system flag for '" + applinkId + "': " + e)
                    .build();
        }
    }

    private static String resolveAppType(ApplicationLink link) {
        final ApplicationType type = link.getType();
        if (type instanceof IdentifiableType) {
            return IdentifiableType.class.cast(type).getId().get();
        } else {
            return type.getClass().getName();
        }
    }

    private void configureTwoWayAuth(final ConfigureAuthEntity entity, ApplicationLink link)
            throws AuthenticationConfigurationException {
        linkService.configureAuthenticationForApplicationLink(
                link,
                new AuthenticationScenario() {
                    public boolean isCommonUserBase() {
                        return entity.sharedUserBase;
                    }

                    public boolean isTrusted() {
                        return entity.trusted;
                    }
                }, entity.username, entity.password);
    }

    public static class ConfigureAuthEntity {
        @JsonProperty
        protected boolean trusted;
        @JsonProperty
        protected boolean sharedUserBase;
        @JsonProperty
        protected String username;
        @JsonProperty
        protected String password;
    }

    public static final class CreateRequestEntity extends ConfigureAuthEntity {
        @JsonProperty
        private String name;
        @JsonProperty
        private String baseUrl;
        @JsonProperty
        private String typeId;
        @JsonProperty
        private boolean twoWay;
        @JsonProperty
        private String urlOverride;
        @JsonProperty
        private boolean primary;

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }

    @JsonSerialize
    public static final class CreateResponseEntity extends BaseRestEntity {
        public static final String LINK_ID = "linkId";
        public static final String LOCAL_ID = "localId";

        @SuppressWarnings("unused") // for unmarshalling
        public CreateResponseEntity() {
        }

        public CreateResponseEntity(ApplicationId linkId, ApplicationId localId) {
            this(linkId.get(), localId.get());
        }

        public CreateResponseEntity(String linkId, String localId) {
            put(LINK_ID, linkId);
            put(LOCAL_ID, localId);
        }
    }

    public static final class ApplinkEntity {
        @JsonProperty
        private String id;
        @JsonProperty
        private String name;
        @JsonProperty
        private String displayUrl;
        @JsonProperty
        private String rpcUrl;
        @Deprecated // use typeId instead
        @JsonProperty
        private String applicationType;
        @JsonProperty
        private String typeId;
        @JsonProperty // alias for typeId
        private String type;


        @SuppressWarnings("unused") // for Jackson
        public ApplinkEntity() {
        }

        public ApplinkEntity(ApplicationLink link) {
            this.id = link.getId().get();
            this.name = link.getName();
            this.displayUrl = link.getDisplayUrl().toASCIIString();
            this.rpcUrl = link.getRpcUrl().toASCIIString();
            this.applicationType = resolveAppType(link);
            this.typeId = this.applicationType;
            this.type = this.applicationType;
        }
    }

    public static final class DeleteAllRequestEntity {
        @JsonProperty
        private boolean reciprocal;
    }

    public static final class DeleteAllResponseEntity {
        @JsonProperty
        private int count;

        public DeleteAllResponseEntity(int count) {
            this.count = count;
        }
    }
}
