package com.atlassian.applinks;

import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.CorsAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * This is a very basic REST resource which returns a simple String message. The important bit here is the presence of
 * {@link CorsAllowed}, which enables this resource to be the basis of a functional CORS test.
 *
 * @since 3.7
 */
@AnonymousAllowed
@Path("cors")
public class CorsDemoResource {

    private final HostApplication application;

    public CorsDemoResource(HostApplication application) {
        this.application = application;
    }

    @CorsAllowed
    @GET
    public Response execute() {
        return Response.ok().entity("This text was provided via CORS from: " + application.getBaseUrl()).build();
    }
}
