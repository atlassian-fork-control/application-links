package com.atlassian.applinks.internal.test.utils;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.security.auth.trustedapps.Application;
import com.atlassian.security.auth.trustedapps.ApplicationRetriever;
import com.atlassian.security.auth.trustedapps.DefaultTrustedApplication;
import com.atlassian.security.auth.trustedapps.RequestConditions;
import com.atlassian.security.auth.trustedapps.TrustedApplication;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.util.NoSuchElementException;

import static java.util.Objects.requireNonNull;


/**
 * This class is simply a copy of TrustedConfigurator. Due to the way dependencies are managed
 * between the plugin, the api and the test plugin, it was not available. As such, this is a simple
 * copy with the required utility methods.
 */
public class TrustedUtils {
    /**
     * <p>
     * 10 seconds is the default certificate timeout. Note that this is our
     * only protection against replay attacks, but we cannot make it very small
     * or we'll run into network latency problems and clock synchronisation
     * issues.
     * </p>
     * See:
     * <ul>
     *  <li>http://confluence.atlassian.com/display/JIRAKB/Jira+Issues+Macro+Fails+to+Display+due+to+'Failed+to+Login+Trusted+Application'+Error</li>
     *  <li>https://paste.atlassian.com/view/1292</li>
     *  <li>https://paste.atlassian.com/view/1293</li>
     * </ul>
     */
    public static final long DEFAULT_CERTIFICATE_TIMEOUT = 10000L;
    public static final String TRUSTED_APPS_INCOMING_ID = "trustedapps.incoming.applicationId";


    protected final TrustedApplicationsConfigurationManager trustedAppsManager;
    protected final AuthenticationConfigurationManager configurationManager;

    public TrustedUtils(final AuthenticationConfigurationManager configurationManager,
                        final TrustedApplicationsConfigurationManager trustedAppsManager) {
        this.configurationManager = configurationManager;
        this.trustedAppsManager = trustedAppsManager;
    }

    public void updateInboundTrust(final ApplicationLink appLink, final RequestConditions requestConditions)
            throws ConfigurationException {
        final Application application = getApplicationCertificate(appLink);
        trustedAppsManager.addTrustedApplication(application, requestConditions);
        appLink.putProperty(TRUSTED_APPS_INCOMING_ID, application.getID());
    }

    public void issueInboundTrust(final ApplicationLink appLink) throws ConfigurationException {
        requireNonNull(appLink, "applicationLink");
        final Application application = getApplicationCertificate(appLink);
        addTrustedApplication(appLink, application);
    }

    /**
     * Configure offline TA using a fake application certificate
     *
     * @param appLink applink to configure
     * @throws ConfigurationException for configuration errors
     * @throws NoSuchProviderException for invalid providers
     * @throws NoSuchAlgorithmException for invalid algorithms
     */
    public void issueOfflineInboundTrust(final ApplicationLink appLink)
            throws ConfigurationException, NoSuchProviderException, NoSuchAlgorithmException {
        requireNonNull(appLink, "applicationLink");
        PublicKey publickey = KeyPairGenerator.getInstance("RSA").generateKeyPair().getPublic();
        final TrustedApplication application = new DefaultTrustedApplication(publickey, "trustedid",
                RequestConditions.builder().build());

        addTrustedApplication(appLink, application);
    }

    public boolean inboundTrustEnabled(ApplicationLink applicationLink) {
        return applicationLink.getProperty(TRUSTED_APPS_INCOMING_ID) != null;
    }


    public boolean outboundTrustEnabled(ApplicationLink applicationLink) {
        return configurationManager.isConfigured(applicationLink.getId(), TrustedAppsAuthenticationProvider.class);
    }

    public void revokeInboundTrust(final ApplicationLink appLink) {
        final Object value = appLink.getProperty(TRUSTED_APPS_INCOMING_ID);
        if (value != null) {
            trustedAppsManager.deleteApplication(value.toString());
        }
        appLink.removeProperty(TRUSTED_APPS_INCOMING_ID);
    }

    public void issueOutboundTrust(final ApplicationLink link) {
        requireNonNull(link, "applicationLink");
        configurationManager.registerProvider(link.getId(), TrustedAppsAuthenticationProvider.class, ImmutableMap.<String, String>of());
    }

    public void revokeOutboundTrust(final ApplicationLink link) {
        requireNonNull(link, "applicationLink");
        configurationManager.unregisterProvider(link.getId(), TrustedAppsAuthenticationProvider.class);
    }

    private void addTrustedApplication(ApplicationLink appLink, final Application application) {
        try {
            Iterables.find(trustedAppsManager.getTrustedApplications(), new Predicate<TrustedApplication>() {
                public boolean apply(final TrustedApplication input) {
                    return input.getID().equals(application.getID());
                }
            });
        } catch (NoSuchElementException ex) {
            trustedAppsManager.addTrustedApplication(application, RequestConditions
                    .builder()
                    .setCertificateTimeout(DEFAULT_CERTIFICATE_TIMEOUT)
                    .build());
        }
        appLink.putProperty(TRUSTED_APPS_INCOMING_ID, application.getID());
    }

    private Application getApplicationCertificate(final ApplicationLink appLink) throws ConfigurationException {
        requireNonNull(appLink, "applicationLink");
        try {
            return trustedAppsManager.getApplicationCertificate(appLink.getRpcUrl().toString());
        } catch (ApplicationRetriever.RetrievalException re) {
            throw new ConfigurationException("Unable to retrieve the application's certificate: " + re.getMessage(), re);
        }
    }

    public static class ConfigurationException extends Exception {
        public ConfigurationException(String message) {
            super(message);
        }

        public ConfigurationException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
