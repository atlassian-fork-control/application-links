package com.atlassian.applinks.internal.test.oauth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.test.rest.model.RestAccessToken;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.user.UserManager;
import org.apache.commons.lang3.RandomStringUtils;

import java.net.URI;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.test.utils.RestBackdoorErrors.notFound;
import static com.google.common.base.Preconditions.checkState;

/**
 * @since 4.3
 */
@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("oauth/access-token")
@InterceptorChain(NoCacheHeaderInterceptor.class)
public class AccessTokenBackdoorResource {
    private final ApplicationLinkService applicationLinkService;
    private final ServiceProviderStoreService serviceProviderStoreService;
    private final ServiceProviderTokenStore serviceProviderTokenStore;
    private final ConsumerTokenStoreService consumerTokenStoreService;
    private final ConsumerService consumerService;
    private final UserManager userManager;

    public AccessTokenBackdoorResource(ApplicationLinkService applicationLinkService,
                                       ServiceProviderTokenStore serviceProviderTokenStore,
                                       ServiceProviderStoreService serviceProviderStoreService,
                                       ConsumerTokenStoreService consumerTokenStoreService,
                                       ConsumerService consumerService,
                                       UserManager userManager) {
        this.applicationLinkService = applicationLinkService;
        this.serviceProviderTokenStore = serviceProviderTokenStore;
        this.serviceProviderStoreService = serviceProviderStoreService;
        this.consumerTokenStoreService = consumerTokenStoreService;
        this.consumerService = consumerService;
        this.userManager = userManager;
    }

    @GET
    @Path("service-provider/{applinkId}/{username}")
    public Response getServiceProviderAccessToken(@PathParam("applinkId") String applinkId,
                                                  @PathParam("username") String username)
            throws TypeNotInstalledException {
        Consumer consumer = getConsumerForApplink(new ApplicationId(applinkId));
        Iterable<ServiceProviderToken> tokens = serviceProviderTokenStore.getAccessTokensForUser(username);
        for (ServiceProviderToken token : tokens) {
            if (token.getConsumer().getKey().equals(consumer.getKey())) {
                return Response.ok(new RestAccessToken(token)).build();
            }
        }
        return Response.status(Status.NOT_FOUND).build();
    }

    @PUT
    @Path("service-provider/{applinkId}/{username}")
    public Response createServiceProviderAccessToken(@PathParam("applinkId") String applinkId,
                                                     @PathParam("username") String username)
            throws TypeNotInstalledException {

        ServiceProviderToken accessToken = ServiceProviderToken.newAccessToken(getRandomToken())
                .tokenSecret(getRandomToken())
                .authorizedBy(userManager.resolve(username))
                .consumer(getConsumerForApplink(new ApplicationId(applinkId)))
                .version(ServiceProviderToken.Version.V_1_0)
                .build();

        serviceProviderTokenStore.put(accessToken);

        return Response.created(URI.create(""))
                .entity(new RestAccessToken(accessToken))
                .build();
    }

    @DELETE
    @Path("service-provider/{applinkId}/{username}")
    public Response removeServiceProviderAccessTokens(@PathParam("applinkId") String applinkId,
                                                      @PathParam("username") String username)
            throws TypeNotInstalledException {
        Consumer consumer = getConsumerForApplink(new ApplicationId(applinkId));
        Iterable<ServiceProviderToken> tokens = serviceProviderTokenStore.getAccessTokensForUser(username);
        for (ServiceProviderToken token : tokens) {
            if (token.getConsumer().getKey().equals(consumer.getKey())) {
                serviceProviderTokenStore.removeAndNotify(token.getToken());
            }
        }
        return Response.noContent().build();
    }

    @GET
    @Path("consumer/{applinkId}/{username}")
    public Response getConsumerAccessTokens(@PathParam("applinkId") String applinkId,
                                            @PathParam("username") String username) throws TypeNotInstalledException {

        ConsumerToken token = consumerTokenStoreService.getConsumerToken(
                getApplicationLink(new ApplicationId(applinkId)), username);
        return Response.ok(new RestAccessToken(token)).build();
    }

    @PUT
    @Path("consumer/{applinkId}/{username}")
    public Response createConsumerAccessToken(@PathParam("applinkId") String applinkId,
                                              @PathParam("username") String username,
                                              RestAccessToken restToken) throws TypeNotInstalledException {
        ApplicationId applicationId = new ApplicationId(applinkId);
        ApplicationLink link = getApplicationLink(applicationId);
        ConsumerToken token = ConsumerToken.newAccessToken(restToken.getToken())
                .tokenSecret(restToken.getTokenSecret())
                .consumer(consumerService.getConsumer()) // this app's consumer
                .build();

        consumerTokenStoreService.removeConsumerToken(applicationId, username);
        consumerTokenStoreService.addConsumerToken(link, username, token);

        return Response.created(URI.create(""))
                .entity(new RestAccessToken(token))
                .build();
    }

    @DELETE
    @Path("consumer/{applinkId}/{username}")
    public Response removeConsumerAccessTokens(@PathParam("applinkId") String applinkId,
                                               @PathParam("username") String username) throws TypeNotInstalledException {
        consumerTokenStoreService.removeConsumerToken(new ApplicationId(applinkId), username);
        return Response.noContent().build();
    }

    private Consumer getConsumerForApplink(ApplicationId applinkId) throws TypeNotInstalledException {
        ApplicationLink link = getApplicationLink(applinkId);
        Consumer consumer = serviceProviderStoreService.getConsumer(link);
        checkState(consumer != null, "No consumer for link: " + link.getId());
        return consumer;
    }

    private ApplicationLink getApplicationLink(ApplicationId applicationId) throws TypeNotInstalledException {
        ApplicationLink link = applicationLinkService.getApplicationLink(applicationId);
        if (link == null) {
            throw new WebApplicationException(notFound("Applinks with ID " + applicationId + " not found"));
        }
        return link;
    }

    private static String getRandomToken() {
        return RandomStringUtils.randomAlphanumeric(32);
    }
}
