package com.atlassian.applinks.internal.test.response;

import com.atlassian.applinks.test.response.MockResponseDefinition;
import com.google.common.base.Predicate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.requireNonNull;


/**
 * Component for storing setup for mock responses.
 *
 * @see MockResponseFilter
 * @since 5.0
 */
public class MockResponseSetup {
    private final Map<Predicate<HttpServletRequest>, MockResponseDefinition> setUp = new ConcurrentHashMap<>();

    public void addResponseDefinition(@Nonnull Predicate<HttpServletRequest> predicate,
                                      @Nonnull MockResponseDefinition definition) {
        requireNonNull(predicate, "predicate");
        requireNonNull(definition, "definition");

        setUp.put(predicate, definition);
    }

    public void removeAll() {
        setUp.clear();
    }

    @Nullable
    public MockResponseDefinition getResponseDefinition(HttpServletRequest request) {
        for (Map.Entry<Predicate<HttpServletRequest>, MockResponseDefinition> definitionEntry : setUp.entrySet()) {
            if (definitionEntry.getKey().apply(request)) {
                return definitionEntry.getValue();
            }
        }
        return null;
    }
}
