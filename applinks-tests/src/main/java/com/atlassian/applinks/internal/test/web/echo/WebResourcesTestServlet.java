package com.atlassian.applinks.internal.test.web.echo;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * Web resources test servlet, loads web resources specified in a query parameter {@code resourceKey}
 * </p>
 * <p>
 * Bound under:
 * /plugins/servlet/applinks-tests/web-resources
 * </p>
 *
 * @since 4.3
 */
public class WebResourcesTestServlet extends HttpServlet {
    private static final String RESOURCE_KEY_PARAM = "resourceKeys";

    private final PageBuilderService pageBuilderService;

    public WebResourcesTestServlet(PageBuilderService pageBuilderService) {
        this.pageBuilderService = pageBuilderService;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        String[] webResourceToLoad = request.getParameterValues(RESOURCE_KEY_PARAM);
        if (webResourceToLoad != null) {
            for (String resourceKey : webResourceToLoad) {
                pageBuilderService.assembler().resources().requireWebResource(resourceKey);
            }
        }

        response.setContentType("text/html");
        response.getWriter().write("<html><head><title>Applinks - echo</title>");
        pageBuilderService.assembler().assembled().drainIncludedResources().writeHtmlTags(response.getWriter(), UrlMode.AUTO);
        response.getWriter().write("</head><body id=\"applinks-web-resources-test\">Web resources test</body></html>");
        response.flushBuffer();
    }
}
