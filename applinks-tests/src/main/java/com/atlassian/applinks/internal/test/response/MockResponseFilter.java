package com.atlassian.applinks.internal.test.response;

import com.atlassian.applinks.test.response.MockResponseDefinition;
import com.google.common.base.Predicate;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;

import static com.atlassian.applinks.internal.rest.RestUrl.forPath;
import static com.atlassian.applinks.test.response.RequestPredicates.restRequestTo;

/**
 * When activated, makes the application's OAuth REST resources act as in Applinks 4.x, by redirecting to the new
 * location of those resources.
 *
 * @since 5.1
 */
public class MockResponseFilter implements Filter {
    private static final Predicate<HttpServletRequest> MOCK_RESPONSE_REQUEST = restRequestTo(forPath("applinks-tests")
            .add(".+")
            .add("mock-response"));

    private final MockResponseSetup mockResponseSetup;

    public MockResponseFilter(MockResponseSetup mockResponseSetup) {
        this.mockResponseSetup = mockResponseSetup;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        if (MOCK_RESPONSE_REQUEST.apply(httpRequest)) {
            // never mock requests to the mock setup itself
            filterChain.doFilter(request, response);
            return;
        }

        MockResponseDefinition responseDefinition = mockResponseSetup.getResponseDefinition(httpRequest);
        if (responseDefinition != null) {
            for (Map.Entry<String, Collection<String>> headerEntry : responseDefinition.getHeaders().asMap().entrySet()) {
                for (String headerValue : headerEntry.getValue()) {
                    httpResponse.addHeader(headerEntry.getKey(), headerValue);
                }
            }
            httpResponse.setStatus(responseDefinition.getStatusCode());
            if (responseDefinition.getBody() != null && responseDefinition.getContentType() != null) {
                httpResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
                httpResponse.setContentType(responseDefinition.getContentType());
                httpResponse.getWriter().write(responseDefinition.getBody());
            }
            httpResponse.getWriter().flush();
        } else {
            filterChain.doFilter(request, response);
        }
    }

    public void init(final FilterConfig filterConfig) throws ServletException {
        // no-op
    }

    public void destroy() {
        // no-op
    }
}
