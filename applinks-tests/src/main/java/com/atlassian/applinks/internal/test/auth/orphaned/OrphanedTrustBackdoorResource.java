package com.atlassian.applinks.internal.test.auth.orphaned;

import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.test.rest.TestExceptionInterceptor;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Properties;

import static com.atlassian.applinks.internal.rest.model.BaseRestEntity.createSingleFieldEntity;
import static java.util.Collections.singletonList;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.serverError;

/**
 * Provides REST access and initialisation of OAuth and Trusted Applications certs
 */
@Path("orphan-test")
@AnonymousAllowed
@Produces(MediaType.APPLICATION_JSON)
@InterceptorChain({TestExceptionInterceptor.class, NoCacheHeaderInterceptor.class})
public class OrphanedTrustBackdoorResource {
    private static final String TEST_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvm5IQh1FihhXgpD0EVuK6F8C7vEdaeyHDetTezsmmuNjj5d0EEHkvcryyjlGWHnPhwHcSaihwntexFwR5iFLtUylhieIljcQ930gQJsBt3FScJ+quLKVAT2okXVFhuN2V78XUMPVKeMUC7kwe8kYxWw9kweubnw2FbhIOy4IJwqZZS6A+0+ik3jdV01UYwGyGPaRUbyWbRaQDp3Go6/PuF7LWVxUmw30LkNEfAojcW6IhYHg8EUKUT3mw4qRvoEUUHUdZ9aj0YIazup3DGHmAFUjdAhOU1H3SOAXNru+ZmpjNDi84bTxJRxPHw9ZCmnhWXGVciSzuXmKI/vcFvnrQwIDAQAB";

    private final PluginSettingsFactory pluginSettingsFactory;
    private final ServiceProviderConsumerStore serviceProviderConsumerStore;

    public OrphanedTrustBackdoorResource(PluginSettingsFactory pluginSettingsFactory,
                                         ServiceProviderConsumerStore serviceProviderConsumerStore) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.serviceProviderConsumerStore = serviceProviderConsumerStore;
    }

    /* (non-Javadoc)
     * Sets up incoming Trusted Apps and OAuth to a refapp instance using the supplied TA and/or OA ids.
     */
    @GET
    public Response execute(@QueryParam("refappTaId") final String refappTrustedAppsId,
                            @QueryParam("refappOaId") final String refappOAuthId) {
        final PluginSettings ps = getPluginSettings();

        if (refappTrustedAppsId != null) {
            ps.put("trustedapps", singletonList(refappTrustedAppsId));

            ps.put("trustedapp." + refappTrustedAppsId, createTrustedProperties());
        }

        if (refappOAuthId != null) {
            serviceProviderConsumerStore.put(new Consumer.InstanceBuilder(refappOAuthId)
                    .name("Test orphaned consumer")
                    .description("Atlassian RefImpl at http://localhost:5992/refapp2")
                    .publicKey(getTestOAuthKey())
                    .build());
        }

        return noContent().build();
    }

    /* (non-Javadoc)
     * Returns the Trusted Apps application id of this application
     */
    @GET
    @Path("trust")
    public Response getTrustedAppId() {
        final String id = (String) getPluginSettings().get("trustedapps.application-id");
        if (id == null) {
            return serverError().entity("No trusted apps id set").build();
        }
        return ok(createSingleFieldEntity("id", id)).build();
    }

    private Properties createTrustedProperties() {
        Properties props = new Properties();
        props.put("ips", "");
        props.put("timeout", "0");
        props.put("urls", "");
        props.put("public.key", TEST_KEY);
        return props;
    }

    private static PublicKey getTestOAuthKey() {
        try {
            return RSAKeys.fromPemEncodingToPublicKey(TEST_KEY);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    private PluginSettings getPluginSettings() {
        return pluginSettingsFactory.createGlobalSettings();
    }
}
