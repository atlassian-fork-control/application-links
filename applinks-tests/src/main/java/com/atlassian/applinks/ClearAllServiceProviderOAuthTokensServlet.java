package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.google.common.collect.Iterables;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class ClearAllServiceProviderOAuthTokensServlet extends HttpServlet {
    private final ApplicationLinkService applicationLinkService;
    private final ServiceProviderTokenStore serviceProviderTokenStore;

    public ClearAllServiceProviderOAuthTokensServlet(ApplicationLinkService applicationLinkService,
                                                     ServiceProviderTokenStore serviceProviderTokenStore) {
        this.applicationLinkService = applicationLinkService;
        this.serviceProviderTokenStore = serviceProviderTokenStore;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        ApplicationLink applicationLink;

        try {
            applicationLink = Iterables.get(applicationLinkService.getApplicationLinks(), 0);
        } catch (IndexOutOfBoundsException iobe) {
            throw new RuntimeException("Need one application link to perform the test");
        }

        serviceProviderTokenStore.removeByConsumer(getConsumerKey(applicationLink));

        resp.setContentType("text/plain");
        PrintWriter out = resp.getWriter();
        out.println("done");
        out.close();
    }

    private String getConsumerKey(final ApplicationLink applicationLink) {
        final Object storedConsumerKey = applicationLink.getProperty("oauth.incoming.consumerkey");
        if (storedConsumerKey == null) {
            throw new RuntimeException("the OAuth consumer must be present");
        }
        return storedConsumerKey.toString();
    }
}
