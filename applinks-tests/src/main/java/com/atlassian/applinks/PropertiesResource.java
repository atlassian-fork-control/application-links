package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.api.application.generic.GenericEntityType;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.ok;

/**
 * Rest resource for integration testing.
 *
 * @since v3.3
 */
@Path("properties")
public class PropertiesResource {
    private final MutatingApplicationLinkService applicationLinkService;
    private final EntityLinkService entityLinkService;

    public PropertiesResource(MutatingApplicationLinkService applicationLinkService, EntityLinkService entityLinkService) {
        this.applicationLinkService = applicationLinkService;
        this.entityLinkService = entityLinkService;
    }

    @GET
    @Path("setApplicationLink")
    public Response setApplicationLinkProperty(@QueryParam("propertyKey") String propertyKey, @QueryParam("propertyValue") String propertyValue) {
        final ApplicationLink applicationLink = applicationLinkService.getPrimaryApplicationLink(GenericApplicationType.class);
        applicationLink.putProperty(propertyKey, propertyValue);
        return ok("Properties updated!").build();
    }

    @GET
    @Path("getApplicationLink")
    public Response getApplicationLinkProperty(@QueryParam("propertyKey") String propertyKey) {
        final ApplicationLink applicationLink = applicationLinkService.getPrimaryApplicationLink(GenericApplicationType.class);
        Object propertyValue = applicationLink.getProperty(propertyKey);
        return ok("'" + propertyKey + "'='" + propertyValue + "'").build();

    }

    @GET
    @Path("setEntityLink")
    public Response setEntityLinkProperty(@QueryParam("entityKey") String entityKey, @QueryParam("propertyKey") String propertyKey, @QueryParam("propertyValue") String propertyValue) {
        EntityLink entityLink = entityLinkService.getPrimaryEntityLink(entityKey, GenericEntityType.class);
        entityLink.putProperty(propertyKey, propertyValue);
        return ok("Properties updated!").build();
    }

    @GET
    @Path("getEntityLink")
    public Response getEntityLinkProperty(@QueryParam("entityKey") String entityKey, @QueryParam("propertyKey") String propertyKey) {
        EntityLink entityLink = entityLinkService.getPrimaryEntityLink(entityKey, GenericEntityType.class);
        Object propertyValue = entityLink.getProperty(propertyKey);
        return ok("'" + propertyKey + "'='" + propertyValue + "'").build();
    }


}
