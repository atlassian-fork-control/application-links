package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.consumer.ConsumerTokenStore;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.collect.Iterables;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static java.util.Objects.requireNonNull;


public class CheckConsumerTokenServlet extends HttpServlet {
    private final ApplicationLinkService applicationLinkService;
    private final ConsumerTokenStore consumerTokenStore;
    private final UserManager userManager;

    public CheckConsumerTokenServlet(ApplicationLinkService applicationLinkService,
                                     ConsumerTokenStore consumerTokenStore,
                                     UserManager userManager) {
        this.applicationLinkService = applicationLinkService;
        this.consumerTokenStore = consumerTokenStore;
        this.userManager = userManager;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        ApplicationLink applicationLink;

        try {
            applicationLink = Iterables.get(applicationLinkService.getApplicationLinks(), 0);
        } catch (IndexOutOfBoundsException iobe) {
            throw new RuntimeException("Need one application link to perform the test");
        }

        ConsumerToken token = getConsumerToken(applicationLink, userManager.getRemoteUsername());

        resp.setContentType("text/plain");
        PrintWriter out = resp.getWriter();
        out.println(token != null);
        out.close();
    }

    public ConsumerToken getConsumerToken(final ApplicationLink applicationLink, final String username) {
        requireNonNull(username, "Username cannot be null!");
        requireNonNull(applicationLink, "Application Link cannot be null!");
        return consumerTokenStore.get(makeOAuthApplinksConsumerKey(username, applicationLink.getId().get()));
    }

    public static ConsumerTokenStore.Key makeOAuthApplinksConsumerKey(final String username, final String applicationLinkId) {
        requireNonNull(username, "Username cannot be null!");
        requireNonNull(applicationLinkId, "Application Link Id cannot be null!");
        return new ConsumerTokenStore.Key(applicationLinkId + ":" + username);
    }
}
