package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.host.OsgiServiceProxyFactory;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 *
 */
@Path("/")
public class HostProxyResource {
    private final BundleContext bundleContext;

    public HostProxyResource(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    @GET
    public Response execute() {
        OsgiServiceProxyFactory factory = new OsgiServiceProxyFactory(new OsgiServiceProxyFactory.ServiceTrackerFactory() {
            public ServiceTracker create(String name) {
                ServiceTracker tracker = new ServiceTracker(bundleContext, name, null);
                tracker.open();
                return tracker;
            }
        });
        testSuccessfulCall(factory);

        return Response.ok().entity("<html><body>Success</body></html>").build();
    }

    public void testSuccessfulCall(OsgiServiceProxyFactory factory) {
        ApplicationLinkService service = factory.createProxy(ApplicationLinkService.class, 10000);
        Iterable<ApplicationLink> it = service.getApplicationLinks();
        if (it == null) {
            throw new RuntimeException("Received null list of application links");
        }
    }
}
