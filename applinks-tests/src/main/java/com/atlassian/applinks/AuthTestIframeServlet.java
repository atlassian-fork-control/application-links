package com.atlassian.applinks;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.collect.ImmutableMap;

/**
 * Generates a page that contains an iframe (id "myframe") whose URL points to
 * {@link AuthTestServlet}.  It passes along all query string parameters to the
 * frame.  Also outputs the current system time in milliseconds (span ID "time")
 * so we can verify that the iframe can be refreshed without refreshing the parent
 * page.
 * <p>
 * Mapped to {@code /plugins/servlet/applinks/applinks-tests/auth-test-iframe}.
 *
 * @since 3.6
 */
@SuppressWarnings("serial")
public class AuthTestIframeServlet extends HttpServlet {
    private final TemplateRenderer templateRenderer;

    public AuthTestIframeServlet(TemplateRenderer templateRenderer) {
        this.templateRenderer = templateRenderer;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html");

        String queryParams = req.getQueryString();

        ImmutableMap<String, Object> context =
                ImmutableMap.<String, Object>of("time", String.valueOf(System.currentTimeMillis()),
                        "queryParams", (queryParams == null) ? "" : queryParams);

        templateRenderer.render("auth-test-iframe.vm", context, resp.getWriter());
    }
}
