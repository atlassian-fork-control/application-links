package com.atlassian.applinks;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;

import com.google.common.collect.Iterables;

public class ThreeLeggedOAuthRequestTestServlet extends HttpServlet {
    private final ApplicationLinkService applicationLinkService;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;

    public ThreeLeggedOAuthRequestTestServlet(ApplicationLinkService applicationLinkService,
                                              AuthenticationConfigurationManager authenticationConfigurationManager) {
        this.applicationLinkService = applicationLinkService;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        ApplicationLink applicationLink;

        try {
            applicationLink = Iterables.get(applicationLinkService.getApplicationLinks(), 0);
        } catch (IndexOutOfBoundsException iobe) {
            throw new RuntimeException("Need one application link to perform the test");
        }

        if (!authenticationConfigurationManager.isConfigured(applicationLink.getId(), OAuthAuthenticationProvider.class)) {
            throw new RuntimeException("Need OAuth configured");
        }

        ApplicationLinkRequestFactory requestFactory = applicationLink.createAuthenticatedRequestFactory(OAuthAuthenticationProvider.class);
        ApplicationLinkRequest request = null;
        try {
            request = requestFactory.createRequest(Request.MethodType.GET, "/plugins/servlet/applinks/whoami");
        } catch (CredentialsRequiredException e) {
            throw new RuntimeException(e);
        }

        String response;
        try {
            response = request.execute(new ApplicationLinkResponseHandler<String>() {
                public String handle(final Response response) throws ResponseException {
                    return response.getResponseBodyAsString();
                }

                public String credentialsRequired(final Response response) throws ResponseException {
                    return "no oauth token";
                }
            });
        } catch (ResponseException e) {
            throw new RuntimeException(e);
        }

        resp.setContentType("text/plain");
        PrintWriter out = resp.getWriter();
        out.println(response);
        out.close();
    }
}
