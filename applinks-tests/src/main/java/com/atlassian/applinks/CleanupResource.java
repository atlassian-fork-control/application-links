package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.consumer.ConsumerTokenStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.websudo.WebSudoNotRequired;
import com.atlassian.security.auth.trustedapps.TrustedApplication;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;
import com.google.common.collect.ImmutableList;

import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth.AUTH_CONFIG_CONSUMER_KEY_OUTBOUND;

@Path("cleanup")
@WebSudoNotRequired
@AnonymousAllowed
public class CleanupResource {
    private static final String CHARLIE_KEYS = "charlie.keys";
    private final MutatingApplicationLinkService linkService;
    private final TrustedApplicationsConfigurationManager trustedApplicationsConfigurationManager;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;

    // from the oauth plugin:
    private final ServiceProviderConsumerStore serviceProviderConsumerStore;
    private final ServiceProviderTokenStore serviceProviderTokenStore;
    private final ConsumerService consumerService;
    private final ConsumerTokenStore consumerTokenStore;

    public CleanupResource(final MutatingApplicationLinkService linkService,
                           final TrustedApplicationsConfigurationManager trustedApplicationsConfigurationManager,
                           final ServiceProviderConsumerStore serviceProviderConsumerStore,
                           final ServiceProviderTokenStore serviceProviderTokenStore,
                           final ConsumerService consumerService,
                           final PluginSettingsFactory pluginSettingsFactory,
                           final AuthenticationConfigurationManager authenticationConfigurationManager,
                           final ConsumerTokenStore consumerTokenStore) {
        this.linkService = linkService;
        this.trustedApplicationsConfigurationManager = trustedApplicationsConfigurationManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.serviceProviderConsumerStore = serviceProviderConsumerStore;
        this.serviceProviderTokenStore = serviceProviderTokenStore;
        this.consumerService = consumerService;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.consumerTokenStore = consumerTokenStore;
    }

    /**
     * Deletes all local applinks and stored trusted apps certificates.
     * Does not require login.
     * Not very RESTful with a GET, but easier to get to with selenium.
     */
    @GET
    public Response execute() {
        /**
         * {@link MutatingApplicationLinkService#deleteApplicationLink(com.atlassian.applinks.api.ApplicationLink)}
         * deletes all Trusted Apps and OAuth related state for its applink,
         * but let's also make sure we remove any orphaned state.
         */
        cleanTrustedApplications();

        // Do the same for any left-over OAuth state.
        cleanOAuth();

        for (final ApplicationLink link : ImmutableList.copyOf(linkService.getApplicationLinks())) {
            linkService.deleteApplicationLink(link);
        }

        // delete all charlies:
        pluginSettingsFactory.createGlobalSettings().remove(CHARLIE_KEYS);
        return Response.ok().build();
    }

    private void cleanTrustedApplications() {
        for (final TrustedApplication ta : ImmutableList.copyOf(
                trustedApplicationsConfigurationManager.getTrustedApplications())) {
            trustedApplicationsConfigurationManager.deleteApplication(ta.getID());
        }

        for (final ApplicationLink link : ImmutableList.copyOf(linkService.getApplicationLinks())) {
            authenticationConfigurationManager.unregisterProvider(link.getId(), TrustedAppsAuthenticationProvider.class);
        }
    }

    private void cleanOAuth() {
        String consumerKey = consumerService.getConsumer().getKey();
        final Map<ConsumerTokenStore.Key, ConsumerToken> consumerTokens = consumerTokenStore.getConsumerTokens(consumerKey);
        for (ConsumerTokenStore.Key key : consumerTokens.keySet()) {
            consumerTokenStore.remove(key);
        }

        for (final ApplicationLink link : ImmutableList.copyOf(linkService.getApplicationLinks())) {
            Map<String, String> config = authenticationConfigurationManager.getConfiguration(link.getId(), OAuthAuthenticationProvider.class);
            if (config != null && config.containsKey(AUTH_CONFIG_CONSUMER_KEY_OUTBOUND)) {
                final String key = config.get(AUTH_CONFIG_CONSUMER_KEY_OUTBOUND);
                consumerTokenStore.removeTokensForConsumer(key);
            }

            authenticationConfigurationManager.unregisterProvider(link.getId(), OAuthAuthenticationProvider.class);
        }

        // as a service provider (inbound): get rid of all the consumers
        for (final Consumer consumer : serviceProviderConsumerStore.getAll()) {
            serviceProviderTokenStore.removeByConsumer(consumer.getKey());
            serviceProviderConsumerStore.remove(consumer.getKey());
        }

        // as a consumer (outbound): get rid of all the service providers we talked to
        for (final Consumer consumer : consumerService.getAllServiceProviders()) {
            consumerService.removeConsumerByKey(consumer.getKey());
        }
    }
}
