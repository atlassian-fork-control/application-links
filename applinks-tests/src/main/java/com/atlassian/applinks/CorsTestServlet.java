package com.atlassian.applinks;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaults;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaultsModuleDescriptor;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 *     Uses the plugin module tracker to retrieve all registered {@link CorsDefaults} for testing.
 * </p>
 * Note: {@link CorsDefaults} instances are not published as services, so they cannot be injected as a constructor
 * parameter; they must be tracked instead.
 *
 * @since 3.7
 */
public class CorsTestServlet extends HttpServlet {
    /**
     * The complete key for the AppLinks {@link CorsDefaults} instance. If the key is changed in the
     * {@code applinks-plugin}'s {@code atlassian-plugin.xml} this will need to be updated as well.
     */
    public static final String MODULE_CORS_DEFAULTS = "com.atlassian.applinks.applinks-cors-plugin:appLinksCorsDefaults";
    private final PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> tracker;

    public CorsTestServlet(PluginAccessor pluginAccessor, PluginEventManager pluginEventManager) {
        tracker = new DefaultPluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor>(pluginAccessor, pluginEventManager, CorsDefaultsModuleDescriptor.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        String action = request.getParameter("action");
        if (action == null || "list".equalsIgnoreCase(action)) {
            list(response);
        } else if ("test".equals(action)) {
            test(request, response);
        } else if ("demo".equals(action)) {
            demo(request, response);
        } else {
            PrintWriter out = response.getWriter();
            out.print("<html><head><title>Unsupported Action</title></head><body><p>The requested action [");
            out.print(action);
            out.println("] is not supported. Supported actions are:</p>");
            out.println("<ul><li><b>demo</b> - Outputs a simple web page for use demoing a real CORS request<br/>");
            out.println("The following parameter is required, for <b>demo</b>:");
            out.println("<ul><li><i>target</i> - The URL of the CORS request target</li></ul></li>");
            out.println("<li><b>list</b> - Lists all registered <tt>CorsDefaults</tt> modules</li>");
            out.println("<li><b>test</b> - Tests the CORS configuration for an Application Link.<br/>");
            out.println("The following parameter is required, for <b>test</b>:");
            out.println("<ul><li><i>origin</i> - The origin URL to test</li></ul></li></ul>");
            out.println("</body></html>");
        }
    }

    private void demo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();

        String target = request.getParameter("target");
        if (target == null || target.trim().length() == 0) {
            out.println("<html>");
            out.println("<head><title>Demo - Missing required parameter</title></head>");
            out.println("<body><p>When invoking the <b>demo</b> action,");
            out.println("the <i>target</i> request parameter is required.</p>");
            out.println("</body></html>");
        } else {
            out.println("<html>\n" +
                    "<head>\n" +
                    "  <title>Demo - Cross-Origin Resource Sharing</title>\n" +
                    "  <script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js\"></script>\n" +
                    "  <script type=\"text/javascript\">\n" +
                    "    $(document).ready(function() { \n" +
                    "      $(\"body\").load(\"" + target + "/rest/applinks-tests/1/cors\"); \n" +
                    "    });\n" +
                    "  </script>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "If everything is working, this should be replaced with text from RefApp2...\n" +
                    "</body>\n" +
                    "</html>");
        }
    }

    /**
     * This function is in place to help troubleshoot the test function, if it fails because it cannot find the right
     * {@link CorsDefaults} instance. {@code action=list} will display a list of the instances installed.
     *
     * @param response the servlet response for writing the list
     * @throws IOException Thrown if the response cannot be written.
     */
    private void list(HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>Registered CorsDefaults</title></head><body><ul>");

        Iterable<CorsDefaultsModuleDescriptor> descriptors = tracker.getModuleDescriptors();
        for (CorsDefaultsModuleDescriptor descriptor : descriptors) {
            out.print("<li>");
            out.print(descriptor.getCompleteKey());
            out.println("</li>");
        }
        out.println("</ul></body></html>");
    }

    /**
     * Retrieves the {@code origin} parameter from the URL and tests the various {@link CorsDefaults} methods with it.
     * If the {@code origin} parameter is not found, an error page is output instead.
     *
     * @param request  the servlet request, for retrieving the origin parameter
     * @param response the servlet response, for writing test results
     * @throws IOException Thrown if the response cannot be written.
     */
    private void test(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><head>");

        String origin = request.getParameter("origin");
        if (origin == null || origin.trim().length() == 0) {
            out.print("<title>Test - Missing required parameter</title></head><body><p>When invoking the <b>test</b>");
            out.println("action, the <i>origin</i> request parameter is required.</p>");
        } else {
            CorsDefaults defaults = null;

            Iterable<CorsDefaultsModuleDescriptor> descriptors = tracker.getModuleDescriptors();
            for (CorsDefaultsModuleDescriptor descriptor : descriptors) {
                if (MODULE_CORS_DEFAULTS.equals(descriptor.getCompleteKey())) {
                    defaults = descriptor.getModule();

                    break;
                }
            }

            if (defaults == null) {
                out.print("<title>Test - Missing CorsDefaults</title></head><body><p>No CorsDefaults module has been ");
                out.print("registered with the key [" + MODULE_CORS_DEFAULTS + "]. Has the AppLinksCorsDefaults in ");
                out.println("applinks-plugin's atlassian-plugin.xml been renamed?</p>");
            } else {
                boolean allowsOrigin = defaults.allowsOrigin(origin);
                boolean allowsCredentials = false;
                Set<String> requestHeaders = Collections.emptySet();
                Set<String> responseHeaders = Collections.emptySet();
                if (allowsOrigin) {
                    allowsCredentials = defaults.allowsCredentials(origin);
                    requestHeaders = defaults.getAllowedRequestHeaders(origin);
                    responseHeaders = defaults.getAllowedResponseHeaders(origin);
                }

                out.print("<title>Test - Results</title></head><body><p>Results for <span id=\"origin\">");
                out.print(origin);
                out.println("</span>:</p><ul>");
                out.print("<li><b>allowsOrigin</b>: <span id=\"allows-origin\">");
                out.print(String.valueOf(allowsOrigin));
                out.println("</span></li>");
                out.print("<li><b>allowsCredentials</b>: <span id=\"allows-credentials\">");
                out.print(String.valueOf(allowsCredentials));
                out.println("</span></li>");
                out.print("<li><b>allowedRequestHeaders</b>: <span id=\"allowed-request-headers\">");
                out.print(StringUtils.join(requestHeaders.toArray(), ", "));
                out.println("</span></li>");
                out.print("<li><b>allowedResponseHeaders</b>: <span id=\"allowed-response-headers\">");
                out.print(StringUtils.join(responseHeaders.toArray(), ", "));
                out.println("</span></li>");
                out.println("</ul>");
            }
        }
        out.println("</body></html>");
    }
}
