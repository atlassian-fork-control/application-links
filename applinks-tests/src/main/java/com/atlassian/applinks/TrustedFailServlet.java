package com.atlassian.applinks;

import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;

/**
 * Trusted application links servlet that requests a failing url.
 *
 * @since 3.11.0
 */
public class TrustedFailServlet extends AbstractTrustedRequestServlet {
    public TrustedFailServlet(final MutatingApplicationLinkService applicationLinkService) {
        super(applicationLinkService);
        setUrl("/plugins/servlet/applinks/tests/failure/get?requestType=");
    }

}
