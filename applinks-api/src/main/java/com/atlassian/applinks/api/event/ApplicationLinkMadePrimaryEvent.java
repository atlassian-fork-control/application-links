package com.atlassian.applinks.api.event;

import com.atlassian.applinks.api.ApplicationLink;

/**
 * This event is broadcast after an application link has been made a primary application link.
 *
 * @since 3.10
 */
public class ApplicationLinkMadePrimaryEvent extends ApplicationLinkEvent {
    public ApplicationLinkMadePrimaryEvent(final ApplicationLink applicationLink) {
        super(applicationLink);
    }
}
