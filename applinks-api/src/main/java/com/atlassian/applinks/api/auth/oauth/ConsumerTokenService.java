package com.atlassian.applinks.api.auth.oauth;

/**
 * Service to deal with OAuth consumer tokens
 *
 * @since v3.11.0
 */
public interface ConsumerTokenService {
    /**
     * Deletes all the consumer tokens for the given user.
     *
     * This can be useful for cleaning-up tokens after a user is deleted or renamed, to avoid a new user being created
     * with the same username and inheriting tokens that don't belong to her. This method will loop through all
     * application links configured with outgoing oauth, and for each one will delete the tokens for the given user (if
     * any).
     *
     * @param username a valid username.
     */
    void removeAllTokensForUsername(String username);
}
