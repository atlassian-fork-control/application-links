package com.atlassian.applinks.api;

import com.atlassian.applinks.api.application.bamboo.BambooApplicationType;
import com.atlassian.applinks.api.application.bitbucket.BitbucketApplicationType;
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.api.application.crowd.CrowdApplicationType;
import com.atlassian.applinks.api.application.fecru.FishEyeCrucibleApplicationType;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.api.application.refapp.RefAppApplicationType;

import javax.annotation.Nonnull;

/**
 * Implements the visitor pattern for {@link ApplicationType types}.
 */
public interface ApplicationTypeVisitor<T> {

    T visit(@Nonnull BambooApplicationType type);

    T visit(@Nonnull BitbucketApplicationType type);

    T visit(@Nonnull ConfluenceApplicationType type);

    T visit(@Nonnull CrowdApplicationType type);

    T visit(@Nonnull FishEyeCrucibleApplicationType type);

    T visit(@Nonnull GenericApplicationType type);

    T visit(@Nonnull JiraApplicationType type);

    T visit(@Nonnull RefAppApplicationType type);

    T visitDefault(@Nonnull ApplicationType applicationType);
}
