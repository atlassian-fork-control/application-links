package com.atlassian.applinks.api.auth;

import com.atlassian.applinks.api.ApplicationLinkRequestFactory;

/**
 * A Non-Impersonating authentication provider means the authentication mode doesn't allow the request to be executed
 * in the context of user as specified by the caller. In general, the request will execute as a fixed, pre-designated
 * user on the remote machine. This mode of authentication should be used when concerning system or logged-in
 * public information and not to do with user-specific information.
 *
 * @since 3.0
 */
public interface NonImpersonatingAuthenticationProvider extends AuthenticationProvider {
    ApplicationLinkRequestFactory getRequestFactory();
}
