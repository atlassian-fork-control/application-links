package com.atlassian.applinks.api.auth.types;

import com.atlassian.applinks.api.auth.DependsOn;
import com.atlassian.applinks.api.auth.NonImpersonatingAuthenticationProvider;

/**
 * The Standard 2-Legged OAuth Authentication provider. 2-Legged OAuth is a variation of OAuth protocol as outlined
 * in http://oauth.googlecode.com/svn/spec/ext/consumer_request/1.0/drafts/1/spec.html. In general, 2-Legged OAuth
 * is used for system-to-system communication which doesn't concern user-specific information nor involve
 * user interaction.
 *
 * Pass this type to the {@link com.atlassian.applinks.api.ApplicationLink#createAuthenticatedRequestFactory(Class)}
 * method to obtain a {@link com.atlassian.sal.api.net.RequestFactory} instance
 * that uses 2 Legged OAuth authentication for authentication.
 *
 * @since 3.10
 */
@DependsOn(OAuthAuthenticationProvider.class)
public interface TwoLeggedOAuthAuthenticationProvider extends NonImpersonatingAuthenticationProvider {
}
