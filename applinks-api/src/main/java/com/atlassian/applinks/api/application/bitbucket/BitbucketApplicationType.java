package com.atlassian.applinks.api.application.bitbucket;

import com.atlassian.applinks.api.ApplicationType;

/**
 * @since 4.3
 */
public interface BitbucketApplicationType extends ApplicationType {
}