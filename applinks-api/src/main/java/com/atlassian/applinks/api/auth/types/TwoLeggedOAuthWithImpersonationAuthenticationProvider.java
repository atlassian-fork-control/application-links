package com.atlassian.applinks.api.auth.types;

import com.atlassian.applinks.api.auth.DependsOn;
import com.atlassian.applinks.api.auth.ImpersonatingAuthenticationProvider;

/**
 * The 2-Legged OAuth Authentication with Impersonation provider. 2-Legged OAuth Authentication with Impersonation
 * authentication can be used to access user-specific information without involving user-interaction (such as the
 * dance process in standard OAuth). The protocol is based on the standard 2-Legged OAuth
 * http://oauth.googlecode.com/svn/spec/ext/consumer_request/1.0/drafts/1/spec.html but with a small variation
 * inspired by how Google implemented the protocol https://developers.google.com/google-apps/gmail/oauth_protocol.
 *
 * Since the impersonation mechanism can be used to obtain escalated privileges on the remote application, consider
 * using this authentication mode only if the caller application can be fully trusted.
 *
 * Pass this type to the {@link com.atlassian.applinks.api.ApplicationLink#createAuthenticatedRequestFactory(Class)}
 * method to obtain a {@link com.atlassian.sal.api.net.RequestFactory} instance
 * that uses 2 Legged OAuth with Impersonation authentication for authentication.
 *
 * @since 3.10
 */
@DependsOn(OAuthAuthenticationProvider.class)
public interface TwoLeggedOAuthWithImpersonationAuthenticationProvider extends ImpersonatingAuthenticationProvider {
}
