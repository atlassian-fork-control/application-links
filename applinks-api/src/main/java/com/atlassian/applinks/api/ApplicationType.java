package com.atlassian.applinks.api;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.sal.api.message.I18nResolver;

import java.net.URI;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents the type of an {@link ApplicationLink}.
 * <p>
 * See the {@link com.atlassian.applinks.api.application} package for a list of {@link ApplicationType}s bundled
 * with the Unified Application Links plugin. Additional types can be added via the extension APIs in the
 * <strong>applinks-spi</strong> module.
 *
 * @since 3.0
 */
@PublicSpi
public interface ApplicationType {

    /**
     * @return the key of an internationalised display name of the type e.g. "FishEye / Crucible". You can resolve
     * this key using the {@link I18nResolver} component provided by the SAL plugin.
     */
    @Nonnull
    String getI18nKey();

    /**
     * @return the 16x16px icon URI for this type or {@code null} if an icon is not available.
     * @since 3.1
     * @deprecated for removal in 6.0. Application Types should implement {@code IconizedType} in {@code applinks-spi}
     * instead
     */
    @Deprecated
    @Nullable
    URI getIconUrl();

    /**
     * Accepts a visitor of the type {@link ApplicationTypeVisitor}.
     *
     * @param visitor visitor to accept
     * @return the result of the visitor for current applicationType.
     * @since 6.1
     */
    @Nullable
    default <T> T accept(@Nonnull ApplicationTypeVisitor<T> visitor) {
        return visitor.visitDefault(this);
    }
}
