/**
 * The standard {@link com.atlassian.applinks.api.ApplicationType} and {@link com.atlassian.applinks.api.EntityType}
 * classes that are bundled with the AppLinks plugin.
 *
 * You can add further types using the extension APIs provided in the applinks-spi module.
 *
 * @since 3.0
 */
package com.atlassian.applinks.api.application;