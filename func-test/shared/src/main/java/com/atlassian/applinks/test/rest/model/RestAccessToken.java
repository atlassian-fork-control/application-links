package com.atlassian.applinks.test.rest.model;

import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.oauth.Token;

import javax.annotation.Nonnull;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class RestAccessToken extends BaseRestEntity {
    public static final String TOKEN = "token";
    public static final String TOKEN_SECRET = "tokenSecret";

    public RestAccessToken(@Nonnull Token token) {
        requireNonNull(token, "token");
        put(TOKEN, token.getToken());
        put(TOKEN_SECRET, token.getTokenSecret());
    }

    @SuppressWarnings("unused") // for deserialization
    public RestAccessToken() {
    }

    public RestAccessToken(@Nonnull Map<String, Object> entity) {
        super(entity);
    }

    @Nonnull
    public String getToken() {
        return requiredValue(TOKEN, getString(TOKEN));
    }

    @Nonnull
    public String getTokenSecret() {
        return requiredValue(TOKEN_SECRET, getString(TOKEN_SECRET));
    }
}
