package com.atlassian.applinks.test.rest.model;

import com.atlassian.applinks.internal.rest.model.BaseRestEntity;

/**
 * @since 5.0
 */
public class RestTrustedAppsStatus extends BaseRestEntity {
    public static final String INCOMING = "incoming";
    public static final String OUTGOING = "outgoing";

    @SuppressWarnings("unused") // for deserialization
    public RestTrustedAppsStatus() {
    }

    public RestTrustedAppsStatus(boolean incoming, boolean outgoing) {
        put(INCOMING, incoming);
        put(OUTGOING, outgoing);
    }

    public boolean getIncoming() {
        return getBooleanValue(INCOMING);
    }

    public boolean getOutgoing() {
        return getBooleanValue(OUTGOING);
    }
}
