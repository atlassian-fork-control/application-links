package com.atlassian.applinks.test.rest.model;

import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.1
 */
public class RestAuthenticationConfiguration extends BaseRestEntity {
    public static final String SHARED_USERBASE = "sharedUserBase";
    public static final String TRUSTED = "trusted";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    @SuppressWarnings("unused") // for deserialization
    public RestAuthenticationConfiguration() {
    }

    public RestAuthenticationConfiguration(@Nonnull AuthenticationScenario authenticationScenario,
                                           @Nonnull String username, @Nonnull String password) {
        requireNonNull(authenticationScenario, "authenticationScenario");
        put(SHARED_USERBASE, authenticationScenario.isCommonUserBase());
        put(TRUSTED, authenticationScenario.isTrusted());
        put(USERNAME, requireNonNull(username, "username"));
        put(PASSWORD, requireNonNull(password, "password"));
    }

    public boolean isSharedUserbase() {
        return getBooleanValue(SHARED_USERBASE);
    }

    public boolean isTrusted() {
        return getBooleanValue(TRUSTED);
    }

    @Nonnull
    public AuthenticationScenario getAuthenticationScenario() {
        return new AuthenticationScenario() {
            @Override
            public boolean isCommonUserBase() {
                return isSharedUserbase();
            }

            @Override
            public boolean isTrusted() {
                return RestAuthenticationConfiguration.this.isTrusted();
            }
        };
    }

    @Nonnull
    public String getUsername() {
        return getRequiredString(USERNAME);
    }

    @Nonnull
    public String getPassword() {
        return getRequiredString(PASSWORD);
    }
}
