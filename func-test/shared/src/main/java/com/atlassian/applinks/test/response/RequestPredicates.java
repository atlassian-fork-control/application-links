package com.atlassian.applinks.test.response;

import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.rest.model.RestRequestPredicate;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.net.HttpHeaders;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.applinks.internal.rest.RestUrlBuilder.REST_CONTEXT;
import static com.google.common.base.Predicates.and;
import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public class RequestPredicates {
    @Nonnull
    public static Predicate<HttpServletRequest> restRequest() {
        return withServletPath(RestUrl.COMPONENT_SEPARATOR + REST_CONTEXT.toString());
    }

    @Nonnull
    public static Predicate<HttpServletRequest> restRequestTo(@Nonnull RestUrl path) {
        return and(restRequest(), withPathInfoMatching(RestUrl.COMPONENT_SEPARATOR + path.toString()));
    }

    @Nonnull
    public static Predicate<HttpServletRequest> hasOAuthHeader() {
        return new Predicate<HttpServletRequest>() {
            @Override
            public boolean apply(HttpServletRequest request) {
                String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
                return authHeader != null && authHeader.startsWith(ApplinksOAuth.OAUTH);
            }
        };
    }

    @Nonnull
    public static Predicate<HttpServletRequest> withServletPath(@Nonnull final String servletPath) {
        requireNonNull(servletPath, "servletPath");
        return new Predicate<HttpServletRequest>() {
            @Override
            public boolean apply(HttpServletRequest request) {
                return servletPath.equals(request.getServletPath());
            }
        };
    }

    @Nonnull
    public static Predicate<HttpServletRequest> withPathInfoMatching(@Nonnull final String pathInfoPattern) {
        requireNonNull(pathInfoPattern, "pathInfoPattern");
        return new Predicate<HttpServletRequest>() {
            @Override
            public boolean apply(HttpServletRequest request) {
                return request.getPathInfo().matches(pathInfoPattern);
            }
        };
    }

    @Nonnull
    @SuppressWarnings("ConstantConditions")
    public static Predicate<HttpServletRequest> forRestRequestPredicate(@Nonnull RestRequestPredicate restPredicate) {
        ImmutableList.Builder<Predicate<HttpServletRequest>> allPredicates = ImmutableList.builder();
        if (restPredicate.getBooleanValue(RestRequestPredicate.HAS_OAUTH_HEADER)) {
            allPredicates.add(hasOAuthHeader());
        }
        if (restPredicate.containsKey(RestRequestPredicate.SERVLET_PATH)) {
            allPredicates.add(withServletPath(restPredicate.getString(RestRequestPredicate.SERVLET_PATH)));
        }
        if (restPredicate.containsKey(RestRequestPredicate.PATH_INFO_PATTERN)) {
            allPredicates.add(withPathInfoMatching(restPredicate.getString(RestRequestPredicate.PATH_INFO_PATTERN)));
        }
        return and(allPredicates.build());
    }
}
