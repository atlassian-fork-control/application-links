package com.atlassian.applinks.test.data.applink;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.test.product.TestedInstance;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Represents an Applink between two tested products.
 *
 * @since 4.3
 */
public class DefaultTestApplink implements TestApplink {
    private final Side from;
    private final Side to;

    private DefaultTestApplink(@Nonnull Side from, @Nonnull Side to) {
        this.from = requireNonNull(from, "from");
        this.to = requireNonNull(to, "to");
    }

    @Nonnull
    public Side from() {
        return from;
    }

    @Nonnull
    public Side to() {
        return to;
    }

    @Nonnull
    public TestApplink reverse() {
        return new DefaultTestApplink(to, from);
    }

    public static final class DefaultSide implements Side {
        private final String id;
        private final ApplicationId applicationId;
        private final TestedInstance product;

        public DefaultSide(@Nonnull String id, @Nonnull TestedInstance product) {
            this.id = requireNonNull(id, "id");
            this.product = requireNonNull(product, "product");
            this.applicationId = new ApplicationId(id);
        }

        @Nonnull
        public String id() {
            return id;
        }

        @Nonnull
        @Override
        public ApplicationId applicationId() {
            return applicationId;
        }

        @Nonnull
        public TestedInstance product() {
            return product;
        }
    }

    public static final class Builder {
        private Side from;
        private Side to;

        @Nonnull
        public Builder from(@Nonnull String id, @Nonnull TestedInstance product) {
            from = new DefaultSide(id, product);
            return this;
        }

        @Nonnull
        public Builder to(@Nonnull String id, @Nonnull TestedInstance product) {
            to = new DefaultSide(id, product);
            return this;
        }

        @Nonnull
        public DefaultTestApplink build() {
            return new DefaultTestApplink(from, to);
        }
    }
}
