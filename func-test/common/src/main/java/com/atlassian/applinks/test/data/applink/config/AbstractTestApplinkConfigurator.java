package com.atlassian.applinks.test.data.applink.config;

import com.atlassian.applinks.test.data.applink.TestApplink;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Abstract base class for {@link TestApplinkConfigurator} that provides default implementation for
 * {@link TestApplinkConfigurator#configure(TestApplink)}, but invoking
 * {@link TestApplinkConfigurator#configureSide(TestApplink.Side)} on each side of the test applink.
 *
 * @since 4.3
 */
public abstract class AbstractTestApplinkConfigurator implements TestApplinkConfigurator {
    @Override
    public final void configure(@Nonnull TestApplink applink) {
        requireNonNull(applink, "applink");
        configureSide(applink.from());
        configureSide(applink.to());
    }
}
