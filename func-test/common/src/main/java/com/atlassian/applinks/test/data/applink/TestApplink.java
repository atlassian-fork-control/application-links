package com.atlassian.applinks.test.data.applink;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.test.product.TestedInstance;

import javax.annotation.Nonnull;

/**
 * Represents an Applink between two tested products. Stores the basic data required to access and modify the link on
 * both sides: for each side it is the tested instance and ID of the link on that instance.
 * <p>
 * The {@code from} and {@code to} sides are defined for the ease of representation, there is no actual requirement on
 * the directionality of the data flow in the actual link (i.e. the represented link may well be a bi-directional link).
 * </p>
 *
 * @since 4.3
 */
public interface TestApplink {
    @Nonnull
    Side from();

    @Nonnull
    Side to();

    /**
     * @return return a new test applink that is the reverse of this applink, i.e. with {@code from} and {@code to}
     * swapped
     */
    @Nonnull
    TestApplink reverse();

    /**
     * One side of the link in one of the products.
     *
     * @since 4.3
     */
    interface Side {
        /**
         * @return tested instance representing this side
         */
        @Nonnull
        TestedInstance product();

        /**
         * @return ID of the applink <i>on this side</i> of the link.
         */
        @Nonnull
        String id();

        /**
         * @return ID of the applink <i>on this side</i> of the link, as {@link }
         */
        @Nonnull
        ApplicationId applicationId();
    }
}
