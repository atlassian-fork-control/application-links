package com.atlassian.applinks.test.data.applink.config;

import com.atlassian.applinks.test.data.applink.TestApplink;

import javax.annotation.Nonnull;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

/**
 * Utilities to configure {@link TestApplink test applinks} using {@link TestApplinkConfigurator configurators}.
 *
 * @since 5.0
 */
public final class ApplinkConfigurators {
    private ApplinkConfigurators() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static TestApplink configure(@Nonnull TestApplink applink,
                                        @Nonnull Iterable<TestApplinkConfigurator> configurators) {
        requireNonNull(applink, "applink");
        requireNonNull(configurators, "configurators");

        for (TestApplinkConfigurator configurator : configurators) {
            configurator.configure(applink);
        }

        return applink;
    }

    @Nonnull
    public static TestApplink configure(@Nonnull TestApplink applink,
                                        @Nonnull TestApplinkConfigurator... configurators) {
        return configure(applink, asList(configurators));
    }

    @Nonnull
    public static TestApplink.Side configureSide(@Nonnull TestApplink.Side applinkSide,
                                                 @Nonnull Iterable<TestApplinkConfigurator> configurators) {
        requireNonNull(applinkSide, "applinkSide");
        requireNonNull(configurators, "configurators");

        for (TestApplinkConfigurator configurator : configurators) {
            configurator.configureSide(applinkSide);
        }

        return applinkSide;
    }

    @Nonnull
    public static TestApplink.Side configureSide(@Nonnull TestApplink.Side applinkSide,
                                                 @Nonnull TestApplinkConfigurator... configurators) {
        return configureSide(applinkSide, asList(configurators));
    }
}
