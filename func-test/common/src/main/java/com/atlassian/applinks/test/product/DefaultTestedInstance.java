package com.atlassian.applinks.test.product;

import com.google.common.base.Objects;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * @since 4.3
 */
public class DefaultTestedInstance implements TestedInstance {
    public static DefaultTestedInstance fromBaseUrl(@Nonnull String baseUrl) {
        SupportedProducts product = findProduct(baseUrl);
        return new DefaultTestedInstance(product.getProductId(), baseUrl, product);
    }

    private final String instanceId;
    private final String baseUrl;
    private final SupportedProducts product;

    public DefaultTestedInstance(@Nonnull String instanceId, @Nonnull String baseUrl,
                                 @Nonnull SupportedProducts product) {
        this.instanceId = requireNonNull(instanceId, "instanceId");
        this.baseUrl = requireNonNull(baseUrl, "baseUrl");
        this.product = requireNonNull(product, "product");
    }

    @Nonnull
    @Override
    public String getInstanceId() {
        return instanceId;
    }

    @Nonnull
    @Override
    public String getBaseUrl() {
        return baseUrl;
    }

    @Nonnull
    @Override
    public SupportedProducts getProduct() {
        return product;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(instanceId, baseUrl, product);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !TestedInstance.class.isInstance(obj)) {
            return false;
        }
        final TestedInstance that = (TestedInstance) obj;
        return instanceId.equals(that.getInstanceId())
                && baseUrl.equals(that.getBaseUrl())
                && product.equals(that.getProduct());
    }

    @Override
    public String toString() {
        return "DefaultTestedInstance{" +
                "instanceId='" + instanceId + '\'' +
                ", baseUrl='" + baseUrl + '\'' +
                ", product=" + product +
                '}';
    }

    private static SupportedProducts findProduct(@Nonnull String baseUrl) {
        for (SupportedProducts product : SupportedProducts.values()) {
            if (StringUtils.strip(baseUrl, "/").endsWith(product.getProductId())) {
                return product;
            }
        }
        return SupportedProducts.GENERIC;
    }
}
