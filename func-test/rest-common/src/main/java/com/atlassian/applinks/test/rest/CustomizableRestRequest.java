package com.atlassian.applinks.test.rest;

import com.atlassian.applinks.internal.rest.RestUrl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Generic REST request with customizable URL path, body and other properties.
 *
 * @since 4.3
 */
public class CustomizableRestRequest extends AbstractRestRequest {
    private final RestUrl path;
    private final Object body;

    private CustomizableRestRequest(Builder builder) {
        super(builder);
        this.path = builder.path;
        this.body = builder.body;
    }

    @Nullable
    @Override
    protected Object getBody() {
        return body;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return path;
    }

    public static class Builder extends AbstractBuilder<Builder, CustomizableRestRequest> {
        private RestUrl path = RestUrl.EMPTY;
        private Object body;

        @Nonnull
        public Builder body(@Nullable Object body) {
            this.body = body;

            return this;
        }

        @Nonnull
        public Builder addPath(@Nonnull String path) {
            this.path = this.path.add(path);

            return this;
        }

        @Nonnull
        public Builder queryParam(@Nonnull String key, @Nonnull Object value) {
            return super.queryParam(key, value);
        }

        @Nonnull
        @Override
        public CustomizableRestRequest build() {
            return new CustomizableRestRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
