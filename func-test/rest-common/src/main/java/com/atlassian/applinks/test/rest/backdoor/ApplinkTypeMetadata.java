package com.atlassian.applinks.test.rest.backdoor;

import java.util.Map;


public class ApplinkTypeMetadata {
    private static final String NAME = "name";
    private static final String ICON_URL = "iconUrl";
    private static final String ICON_URI = "iconUri";

    private String name;
    private String iconUrl;
    private String iconUri;

    public ApplinkTypeMetadata(Map<String, String> map) {
        this.name = map.get(NAME);
        this.iconUri = map.get(ICON_URI);
        this.iconUrl = map.get(ICON_URL);
    }

    public String getName() {
        return name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getIconUri() {
        return iconUri;
    }
}
