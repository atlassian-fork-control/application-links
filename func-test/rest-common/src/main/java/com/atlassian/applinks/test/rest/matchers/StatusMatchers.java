package com.atlassian.applinks.test.rest.matchers;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;

/**
 * Useful matchers for HTTP status codes represented by {@link Status}.
 *
 * @since 4.3
 */
public final class StatusMatchers {
    private StatusMatchers() {
        throw new UnsupportedOperationException("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<Status> ok() {
        return is(OK);
    }

    /**
     * @return status matcher for a common REST response pattern: OK or NOT_FOUND
     */
    @Nonnull
    public static Matcher<Status> okOrNotFound() {
        return anyOf(is(OK), is(NOT_FOUND));
    }

    @Nonnull
    public static Matcher<Status> noContent() {
        return is(NO_CONTENT);
    }

    @Nonnull
    public static Matcher<Status> badRequest() {
        return is(BAD_REQUEST);
    }

    @Nonnull
    public static Matcher<Status> successful() {
        return classifiesAs(Status.Family.SUCCESSFUL);
    }

    @Nonnull
    public static Matcher<Status> clientError() {
        return classifiesAs(Status.Family.CLIENT_ERROR);
    }

    @Nonnull
    public static Matcher<Status> classifiesAs(@Nonnull Status.Family expectedFamily) {
        requireNonNull(expectedFamily, "expectedFamily");
        return classifiesAs(is(expectedFamily));
    }

    @Nonnull
    public static Matcher<Status> classifiesAs(@Nonnull Matcher<Status.Family> familyMatcher) {
        return new FeatureMatcher<Status, Status.Family>(familyMatcher, "status family that", "status family") {
            @Override
            protected Status.Family featureValueOf(Status status) {
                return status.getFamily();
            }
        };
    }
}
