package com.atlassian.applinks.test.rest.data.applink.config;

import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.data.applink.config.AbstractTestApplinkConfigurator;
import com.atlassian.applinks.test.data.applink.config.TestApplinkConfigurator;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.status.ApplinkOAuthStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createThreeLoOnlyConfig;
import static java.util.Objects.requireNonNull;

/**
 * Configures OAuth for an applink.
 *
 * @since 4.3
 */
public class OAuthApplinkConfigurator extends AbstractTestApplinkConfigurator implements TestApplinkConfigurator {
    private final ApplinkOAuthStatus status;
    private final TestAuthentication authentication;

    public OAuthApplinkConfigurator(@Nonnull ApplinkOAuthStatus status, @Nullable TestAuthentication authentication) {
        this.status = status;
        this.authentication = authentication != null ? authentication : TestAuthentication.admin();
    }

    public OAuthApplinkConfigurator(@Nonnull ApplinkOAuthStatus status) {
        this(status, null);
    }

    /**
     * @return configurator to configure default OAuth level
     * @see ApplinkOAuthStatus#DEFAULT
     */
    @Nonnull
    public static OAuthApplinkConfigurator enableDefaultOAuth() {
        return new OAuthApplinkConfigurator(ApplinkOAuthStatus.DEFAULT);
    }

    /**
     * @return configurator to configure default OAuth with impersonation
     * @see ApplinkOAuthStatus#IMPERSONATION
     */
    @Nonnull
    public static OAuthApplinkConfigurator enableOAuthWithImpersonation() {
        return new OAuthApplinkConfigurator(ApplinkOAuthStatus.IMPERSONATION);
    }

    /**
     * @return configurator to configure the legacy 3LO-only OAuth
     */
    @Nonnull
    public static OAuthApplinkConfigurator enable3LoOnly() {
        return new OAuthApplinkConfigurator(new ApplinkOAuthStatus(createThreeLoOnlyConfig(), createThreeLoOnlyConfig()));
    }

    /**
     * @return configurator to disable OAuth
     * @see ApplinkOAuthStatus#OFF
     */
    @Nonnull
    public static OAuthApplinkConfigurator disableOAuth() {
        return new OAuthApplinkConfigurator(ApplinkOAuthStatus.OFF);
    }

    /**
     * @return configurator to configure custom OAuth levels {@code incoming} and {@code outgoing}
     */
    @Nonnull
    public static OAuthApplinkConfigurator enableOAuth(@Nonnull OAuthConfig incoming, @Nonnull OAuthConfig outgoing) {
        return new OAuthApplinkConfigurator(new ApplinkOAuthStatus(incoming, outgoing));
    }

    @Override
    public void configureSide(@Nonnull TestApplink.Side side) {
        requireNonNull(side, "side");
        getTester(side.product()).putOAuth(ApplinkOAuthStatusRequest.update(side.id(), status));
    }

    private ApplinkStatusRestTester getTester(TestedInstance productInstance) {
        return new ApplinkStatusRestTester(productInstance, authentication);
    }
}
