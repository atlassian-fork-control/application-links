package com.atlassian.applinks.test.rest.status;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.AbstractApplinkRequest;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

public class ApplinkStatusRequest extends AbstractApplinkRequest {
    /**
     * Default request with no extra expectations for given {@code applinkId}.
     *
     * @param applinkId ID of the application link to examine
     * @return request to the status resource for {@code applinkId}
     */
    @Nonnull
    public static ApplinkStatusRequest forApplinkId(@Nonnull String applinkId) {
        return new Builder(applinkId).build();
    }

    public ApplinkStatusRequest(@Nonnull Builder builder) {
        super(builder);
    }

    public static class Builder extends AbstractApplinkRequestBuilder<Builder, ApplinkStatusRequest> {
        public Builder(@Nonnull String applinkId) {
            super(applinkId);
            expectStatus(Status.OK);
        }

        public Builder(@Nonnull TestApplink.Side applink) {
            super(applink);
            expectStatus(Status.OK);
        }

        @Nonnull
        @Override
        public ApplinkStatusRequest build() {
            return new ApplinkStatusRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
