package com.atlassian.applinks.test.rest.data.applink.config;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.AccessTokenBackdoor;

import javax.annotation.Nonnull;

/**
 * Applink configurator that deletes a specific access token (consumer or service provider) for a given username.
 *
 * @since 5.0
 */
public class DeleteAccessTokenApplinkConfigurator extends AbstractOAuthAccessTokenApplinkConfigurator {
    @Nonnull
    @SuppressWarnings("ConstantConditions")
    public static DeleteAccessTokenApplinkConfigurator deleteConsumerToken(@Nonnull TestAuthentication authentication) {
        return deleteConsumerToken(authentication.getUsername());
    }

    @Nonnull
    public static DeleteAccessTokenApplinkConfigurator deleteConsumerToken(@Nonnull String username) {
        return new DeleteAccessTokenApplinkConfigurator(username, TokenType.CONSUMER);
    }

    @Nonnull
    @SuppressWarnings("ConstantConditions")
    public static DeleteAccessTokenApplinkConfigurator deleteServiceProviderToken(
            @Nonnull TestAuthentication authentication) {
        return deleteServiceProviderToken(authentication.getUsername());
    }

    @Nonnull
    public static DeleteAccessTokenApplinkConfigurator deleteServiceProviderToken(@Nonnull String username) {
        return new DeleteAccessTokenApplinkConfigurator(username, TokenType.SERVICE_PROVIDER);
    }

    private final AccessTokenBackdoor accessTokenBackdoor = new AccessTokenBackdoor();

    private DeleteAccessTokenApplinkConfigurator(@Nonnull String username, @Nonnull TokenType tokenType) {
        super(username, tokenType);
    }

    @Override
    public void configureSide(@Nonnull TestApplink.Side side) {
        if (tokenType == TokenType.CONSUMER) {
            accessTokenBackdoor.deleteConsumerToken(side, username);
        } else {
            accessTokenBackdoor.deleteServiceProviderToken(side, username);
        }
    }
}
