package com.atlassian.applinks.test.rest.data.applink;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.data.applink.config.AbstractTestApplinkConfigurator;
import com.atlassian.applinks.test.data.applink.config.TestApplinkConfigurator;
import com.atlassian.applinks.test.rest.applink.ApplinkV1Request;
import com.atlassian.applinks.test.rest.applink.ApplinksV1RestTester;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * {@link TestApplinkConfigurator} that deletes the applink. This will (by default) use the
 * {@link TestAuthentication#admin() default admin authentication}, which may not work for all tested instances.
 *
 * @since 4.3
 */
public class DeleteApplinkConfigurator extends AbstractTestApplinkConfigurator {
    @Nonnull
    public static DeleteApplinkConfigurator deleteApplink() {
        return new DeleteApplinkConfigurator(TestAuthentication.admin());
    }

    private final TestAuthentication authentication;

    public DeleteApplinkConfigurator(@Nonnull TestAuthentication authentication) {
        this.authentication = requireNonNull(authentication, "authentication");
    }

    @Override
    public void configureSide(@Nonnull TestApplink.Side side) {
        new ApplinksV1RestTester(side.product(), authentication).delete(ApplinkV1Request.forApplink(side));
    }
}
