package com.atlassian.applinks.test.rest.applink;

import com.atlassian.applinks.test.rest.AbstractRestRequest;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public abstract class AbstractApplinksV3Request extends AbstractRestRequest {
    protected static final String PARAM_DATA = "data";
    protected static final String PARAM_PROPERTY = "property";

    protected AbstractApplinksV3Request(AbstractApplinksV3Builder<?, ?> builder) {
        super(builder);
    }

    protected abstract static class AbstractApplinksV3Builder<B extends AbstractApplinksV3Builder<B, R>, R extends AbstractApplinksV3Request>
            extends AbstractBuilder<B, R> {
        @Nonnull
        public B data(@Nonnull String dataKey) {
            requireNonNull(dataKey, "dataKey");
            return queryParam(PARAM_DATA, dataKey);
        }

        @Nonnull
        public B property(@Nonnull String propertyKey) {
            requireNonNull(propertyKey, "propertyKey");
            return queryParam(PARAM_PROPERTY, propertyKey);
        }
    }
}
