package com.atlassian.applinks.test.rest.url;

import com.atlassian.applinks.core.v1.rest.ApplicationLinkResource;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.RestVersion;
import com.atlassian.applinks.internal.rest.applink.ApplicationLinkV3Resource;
import com.atlassian.applinks.internal.rest.migration.MigrateAuthenticationResource;

import javax.annotation.Nonnull;

/**
 * Common REST URLs for Applinks.
 *
 * @since 4.3
 */
public final class ApplinksRestUrls {
    public static final String REST_PATH = "rest";
    public static final String VERSION_LATEST = "latest";

    public static final String CAPABILITIES_PATH = "capabilities";
    public static final String CONSUMER_TOKEN = "consumer-token";
    public static final String FEATURES_PATH = "features";
    public static final String FEATURES_DISCOVERY_PATH = "feature-discovery";
    public static final String MIGRATION_PATH = MigrateAuthenticationResource.CONTEXT;
    public static final String STATUS_PATH = "status";
    public static final String VERSION_PATH = "version";

    public static final String REMOTE_PATH = "remote";
    public static final String OAUTH_PATH = "oauth";

    /**
     * @param baseUrl base URL of the tested application
     * @return URL to the {@link RestModules#DEFAULT default module} and {@link RestVersion#DEFAULT version} in the
     * tested application running at {@code baseUrl}
     */
    @Nonnull
    public static ApplinksRestUrls forDefaultRestModule(@Nonnull String baseUrl) {
        return forRestModule(baseUrl, RestModules.DEFAULT);
    }

    /**
     * @param baseUrl base URL of the tested application
     * @param module  the REST module to point to
     * @return URL to the specified module and {@link RestVersion#DEFAULT default version} in the tested application
     * running at {@code baseUrl}
     */
    @Nonnull
    public static ApplinksRestUrls forRestModule(@Nonnull String baseUrl, @Nonnull RestModules module) {
        return forRestModule(baseUrl, module, RestVersion.DEFAULT);
    }

    /**
     * @param baseUrl base URL of the tested application
     * @param module  the REST module to point to
     * @param version the REST module version to point to
     * @return URL to the specified module and version in the tested application running at {@code baseUrl}
     */
    @Nonnull
    public static ApplinksRestUrls forRestModule(@Nonnull String baseUrl, @Nonnull RestModules module,
                                                 @Nonnull RestVersion version) {
        return new ApplinksRestUrls(baseUrl, module, version);
    }

    private final RestUrl rootUrl;

    ApplinksRestUrls(@Nonnull String baseUrl, @Nonnull RestUrl module, @Nonnull RestUrl version) {

        this.rootUrl = RestUrl.forPath(baseUrl)
                .add(REST_PATH)
                .add(module)
                .add(version);
    }

    private ApplinksRestUrls(@Nonnull String baseUrl, @Nonnull RestModules module, @Nonnull RestVersion version) {
        this(baseUrl, RestUrl.forPath(module.path), version.getPath());
    }

    @Nonnull
    public RestUrl consumerToken() {
        return rootUrl.add(CONSUMER_TOKEN);
    }

    @Nonnull
    public RestUrl applicationLink() {
        return rootUrl.add(ApplicationLinkResource.CONTEXT);
    }

    @Nonnull
    public RestUrl applinks() {
        return rootUrl.add(ApplicationLinkV3Resource.CONTEXT);
    }

    @Nonnull
    public RestUrl applicationLink(@Nonnull String applinkId) {
        return applicationLink().add(applinkId);
    }

    @Nonnull
    public RestUrl capabilities() {
        return rootUrl.add(CAPABILITIES_PATH);
    }

    @Nonnull
    public RestUrl remoteCapabilities() {
        return capabilities().add(REMOTE_PATH);
    }

    @Nonnull
    public RestUrl remoteCapabilities(@Nonnull String applinkId) {
        return remoteCapabilities().add(applinkId);
    }

    /**
     * @return URL to the "features" resource
     */
    @Nonnull
    public RestUrl features() {
        return rootUrl.add(FEATURES_PATH);
    }

    /**
     * @return URL to the feature discovery resource
     */
    public RestUrl featureDiscovery() {
        return rootUrl.add(FEATURES_DISCOVERY_PATH);
    }

    /**
     * @return URL to a specific Applinks feature resource
     */
    public RestUrl feature(@Nonnull ApplinksFeatures feature) {
        return features().add(feature.name());
    }

    /**
     * @return URL to the OAuth path
     */
    @Nonnull
    public RestUrl oauth() {
        return rootUrl.add(OAUTH_PATH);
    }

    /**
     * @return URL to the Applink status resource
     */
    @Nonnull
    public RestUrl status() {
        return rootUrl.add(STATUS_PATH);
    }

    /**
     * @return URL to the Applink status resource for a specific Applink ID
     */
    @Nonnull
    public ApplinksStatusRestUrls status(@Nonnull String applinkId) {
        return new ApplinksStatusRestUrls(status().add(applinkId));
    }

    /**
     * @return URL to the "version" resource
     */
    @Nonnull
    public RestUrl version() {
        return rootUrl.add(VERSION_PATH);
    }

    /**
     * @return URL to the "migration" resource
     */
    @Nonnull
    public RestUrl migration() {
        return rootUrl.add(MIGRATION_PATH);
    }

    /**
     * @return URL to a custom path in a REST module represented by this URLs instance
     */
    @Nonnull
    public RestUrl path(@Nonnull String path) {
        return rootUrl.add(path);
    }

    /**
     * @return root URL to a specific REST module that can be used to build a custom REST resource URL within that
     * module
     */
    @Nonnull
    public RestUrl root() {
        return rootUrl;
    }

    public enum RestModules {
        /**
         * The default module "applinks" used for the majority of REST resources.
         */
        APPLINKS("applinks"),
        /**
         * {@code applinks-tests} module used by the Applinks Test Plugin
         */
        APPLINKS_TESTS("applinks-tests"),

        /**
         * Module used for AppLinks-OAuth Plugin
         */
        APPLINKS_OAUTH("applinks-oauth");

        public static RestModules DEFAULT = APPLINKS;

        private final String path;

        RestModules(String path) {
            this.path = path;
        }
    }
}
