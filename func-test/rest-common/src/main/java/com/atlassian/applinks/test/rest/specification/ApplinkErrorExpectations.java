package com.atlassian.applinks.test.rest.specification;

import com.atlassian.applinks.internal.rest.model.status.RestApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.rest.specification.RestExpectations.jsonPath;
import static com.google.common.base.Strings.nullToEmpty;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * @since 5.0
 */
public final class ApplinkErrorExpectations {
    @Nonnull
    public static ResponseSpecification expectErrorType(@Nullable String path, @Nonnull ApplinkErrorType errorType) {
        requireNonNull(errorType, "errorType");
        path = nullToEmpty(path);
        return RestAssured.expect()
                .body(path, notNullValue())
                .body(jsonPath(path, RestApplinkError.TYPE), is(errorType.name()))
                .body(jsonPath(path, RestApplinkError.CATEGORY), is(errorType.getCategory().name()));
    }

    @Nonnull
    public static ResponseSpecification expectErrorDetails(@Nullable String path, @Nonnull Matcher<?> detailsMatcher) {
        requireNonNull(detailsMatcher, "detailsMatcher");
        path = nullToEmpty(path);
        return RestAssured.expect()
                .body(path, notNullValue())
                .body(jsonPath(path, RestApplinkError.DETAILS), detailsMatcher);
    }
}
