package com.atlassian.applinks.test.rest;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static com.atlassian.applinks.test.rest.matchers.JaxRsToStatusCodeMatcher.withStatus;
import static com.atlassian.applinks.test.rest.matchers.StatusMatchers.successful;
import static com.jayway.restassured.RestAssured.expect;
import static java.util.Objects.requireNonNull;

/**
 * Base class for requests going through {@link BaseRestTester} (and its subclasses). Subclasses should provide
 * the request-specfic part of the path and optionally the body and query parameters to use when issuing this request.
 * <br>
 * Subclasses need to also implement a builder class by extending from {@link AbstractBuilder}.
 *
 * @see BaseRestTester
 * @since 4.3
 */
public abstract class AbstractRestRequest {
    private final TestAuthentication authentication;
    private final ResponseSpecification specification;
    private final Map<String, Collection<Object>> queryParams;

    protected AbstractRestRequest(AbstractBuilder<?, ?> builder) {
        authentication = builder.authentication;
        specification = builder.specification;
        queryParams = ImmutableMap.copyOf(builder.queryParams);
    }

    /**
     * @return part of the request path specific to this request (e.g. corresponding to an entity represented by this
     * request).
     */
    @Nonnull
    protected abstract RestUrl getPath();

    /**
     * Body of this request. This could be a {@code JSONObject}, a map, or any serializable bean.
     *
     * @return the request body, or {@code null} if this  request has no body. NOTE: only PUT and POST requests can
     * have a body
     */
    @Nullable
    protected Object getBody() {
        return null;
    }

    /**
     * @return query parameters to use when issuing this request
     */
    @Nonnull
    @SuppressWarnings("unchecked")
    protected final Map<String, Collection<?>> getQueryParams() {
        Map<String, Collection<Object>> params = Maps.newHashMap(queryParams);
        addExtraQueryParamsTo(params);
        return (Map) ImmutableMap.copyOf(params);
    }

    @Nullable
    protected TestAuthentication getAuthentication() {
        return authentication;
    }

    @Nonnull
    protected ResponseSpecification getSpecification() {
        return specification;
    }

    /**
     * Override this if you want to specify extra query parameters directly in the request, rather than via
     * {@link AbstractBuilder#queryParam(String, Object) builder}. The entries provided here will override any entries
     * provided from the builder, in case they keys clash.
     *
     * @return extra query parameters
     */
    @Nonnull
    protected Map<String, Object> getExtraQueryParams() {
        return Collections.emptyMap();
    }

    protected final boolean hasBody() {
        return getBody() != null;
    }

    protected final boolean hasQueryParams() {
        return !getQueryParams().isEmpty();
    }

    private void addExtraQueryParamsTo(Map<String, Collection<Object>> queryParams) {
        for (Map.Entry<String, Object> param : getExtraQueryParams().entrySet()) {
            Collection<Object> toAdd = Lists.newArrayList();
            if (param.getValue() instanceof Iterable) {
                Iterables.addAll(toAdd, Iterable.class.cast(param.getValue()));
            } else {
                toAdd.add(param.getValue());
            }

            if (!queryParams.containsKey(param.getKey())) {
                queryParams.put(param.getKey(), Lists.newArrayList());
            }
            queryParams.get(param.getKey()).addAll(toAdd);
        }
    }

    protected abstract static class AbstractBuilder<B extends AbstractBuilder<B, R>, R extends AbstractRestRequest> {
        private final ResponseSpecification specification = expect(); // empty response specification
        private final Map<String, Collection<Object>> queryParams = Maps.newHashMap();

        private TestAuthentication authentication;

        protected AbstractBuilder() {
            expectStatus(successful()); // any 2xx response code
        }

        @Nonnull
        public B authentication(@Nullable TestAuthentication value) {
            authentication = value;

            return self();
        }

        /**
         * Add extra {@link ResponseSpecification response specification} to this request. New instances of those can
         * be easily obtained using {@code RestAssured.expect()}. Specifications will be added cumulatively, with some
         * fields overridden and some fields merged, as per {@link ResponseSpecification#spec(ResponseSpecification)}.
         * <br>
         * Clients can also use {@link ResponseSpecBuilder} to build more complex specifications before passing on to
         * this method. Some built-in specifications are also provided in the {@code
         * com.atlassian.applinks.test.rest.specification} package.
         *
         * @param specification new specification to add
         * @return this builder instance
         * @see ResponseSpecification
         * @see ResponseSpecBuilder
         * @see com.atlassian.applinks.test.rest.specification
         */
        @Nonnull
        public B specification(@Nullable ResponseSpecification specification) {
            if (specification != null) {
                this.specification.spec(specification);
            }

            return self();
        }

        /**
         * Add multiple extra {@link ResponseSpecification response specifications} to this request.
         *
         * @param specifications new specifications to add
         * @return this builder instance
         * @see #specification(ResponseSpecification)
         * @see ResponseSpecification
         * @see ResponseSpecBuilder
         * @see com.atlassian.applinks.test.rest.specification
         */
        @Nonnull
        public B specifications(@Nonnull ResponseSpecification... specifications) {
            for (ResponseSpecification spec : specifications) {
                specification(spec);
            }

            return self();
        }

        /**
         * NOTE: expectations about the response status provided using this method <i>may</i> be overridden
         * by expectations provided by subsequently invoking {@link #specification(ResponseSpecification)} or any
         * variant of {@code expectStatus()}.
         *
         * @param status expected status
         * @return this builder instance
         */
        @Nonnull
        public B expectStatus(@Nonnull Response.Status status) {
            specification(expect().statusCode(requireNonNull(status, "status").getStatusCode()));

            return self();
        }

        /**
         * NOTE: expectations about the response status provided using this method <i>may</i> be overridden
         * by expectations provided by subsequently invoking {@link #specification(ResponseSpecification)} or any
         * variant of {@link #expectStatus(Response.Status)}.
         *
         * @param statusMatcher matcher for the expected status
         * @return this builder instance
         */
        @Nonnull
        public B expectStatus(@Nonnull Matcher<Response.Status> statusMatcher) {
            specification(expect().statusCode(withStatus(statusMatcher)));

            return self();
        }

        @Nonnull
        protected B queryParam(@Nonnull String key, @Nonnull Object value) {
            requireNonNull(key, "key");
            requireNonNull(value, "value");

            if (!queryParams.containsKey(key)) {
                queryParams.put(key, Lists.newArrayList());
            }
            queryParams.get(key).add(value);

            return self();
        }

        @Nonnull
        public abstract R build();

        @Nonnull
        protected abstract B self();
    }
}
