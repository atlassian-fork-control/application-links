package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.AbstractRestRequest;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls.RestModules;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

/**
 * Use to retrieve an authentication cookie for any tested product. This allows for skipping the traditional, slow
 * login process that also differes from product to product.
 *
 * @since 4.3
 */
public class AuthenticationBackdoor {
    public static final String AUTHENTICATION_COOKIE_NAME = "JSESSIONID";

    private static final String AUTH_RESOURCE_PATH = "auth";
    private static final String SESSION_ID = "sessionId";

    private final BaseRestTester authTester;
    private final String baseUrl;

    public AuthenticationBackdoor(@Nonnull String baseUrl) {
        this.baseUrl = requireNonNull(baseUrl, "baseUrl");
        this.authTester = new BaseRestTester(authRestUrl(baseUrl));
    }

    public AuthenticationBackdoor(@Nonnull TestedInstance testedInstance) {
        this(testedInstance.getBaseUrl());
    }

    /**
     * @param credentials credentials to log in with
     * @return authentication cookie value to use for subsequent interactions with the product
     */
    @Nonnull
    @SuppressWarnings("ConstantConditions")
    public String getAuthentication(@Nonnull TestAuthentication credentials) {
        Response response = authTester.get(AuthRequest.forCredentials(credentials));
        String authCookie = response.as(BaseRestEntity.class).getString(SESSION_ID);
        checkState(authCookie != null, "Auth cookie was not set for base URL: " + baseUrl);
        return authCookie;
    }

    private static RestUrl authRestUrl(String baseUrl) {
        return ApplinksRestUrls.forRestModule(baseUrl, RestModules.APPLINKS_TESTS).path(AUTH_RESOURCE_PATH);
    }

    private static final class AuthRequest extends AbstractRestRequest {
        static AuthRequest forCredentials(@Nonnull TestAuthentication credentials) {
            return new Builder()
                    .authentication(credentials)
                    .build();
        }

        AuthRequest(Builder builder) {
            super(builder);
        }

        @Nonnull
        @Override
        protected RestUrl getPath() {
            return RestUrl.EMPTY;
        }

        private static class Builder extends AbstractBuilder<Builder, AuthRequest> {

            @Nonnull
            @Override
            public AuthRequest build() {
                return new AuthRequest(this);
            }

            @Nonnull
            @Override
            protected Builder self() {
                return this;
            }
        }
    }
}
