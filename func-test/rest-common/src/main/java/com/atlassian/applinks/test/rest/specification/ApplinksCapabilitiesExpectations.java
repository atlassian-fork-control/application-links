package com.atlassian.applinks.test.rest.specification;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import java.util.EnumSet;
import java.util.Set;

import static com.atlassian.applinks.test.rest.matchers.RestMatchers.containsEnumValues;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

public class ApplinksCapabilitiesExpectations {

    private ApplinksCapabilitiesExpectations() {
        throw new AssertionError("Do not instantiate " + ApplinksCapabilitiesExpectations.class.getSimpleName());
    }

    @Nonnull
    public static ResponseSpecification expectCapability(@Nonnull ApplinksCapabilities expectedCapability) {
        return expectCapabilities(hasItem(expectedCapability.name()));
    }

    @Nonnull
    public static ResponseSpecification expectMissingCapability(@Nonnull ApplinksCapabilities capability) {
        return expectCapabilities(not(hasItem(capability.name())));
    }

    @Nonnull
    public static ResponseSpecification expectCapabilities(@Nonnull Set<ApplinksCapabilities> expectedCapabilities) {
        return expectCapabilities(containsEnumValues(expectedCapabilities));
    }

    @Nonnull
    public static ResponseSpecification expectAllCapabilities() {
        return expectCapabilities(EnumSet.allOf(ApplinksCapabilities.class));
    }

    @Nonnull
    public static ResponseSpecification expectNoCapabilities() {
        return expectCapabilities(EnumSet.noneOf(ApplinksCapabilities.class));
    }

    @Nonnull
    public static ResponseSpecification expectCapabilities(@Nonnull Matcher<?> expectedCapabilitiesMatcher) {
        return RestExpectations.expectBody(expectedCapabilitiesMatcher);
    }
}
