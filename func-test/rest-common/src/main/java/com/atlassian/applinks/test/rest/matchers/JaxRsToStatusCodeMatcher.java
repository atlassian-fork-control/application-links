package com.atlassian.applinks.test.rest.matchers;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.is;

/**
 * Translates JAX-RS {@link Status} matchers into "simple" integer matchers.
 *
 * @since 4.3
 */
public class JaxRsToStatusCodeMatcher extends FeatureMatcher<Integer, Status> {
    @Nonnull
    public static JaxRsToStatusCodeMatcher withStatus(@Nonnull Matcher<Status> statusMatcher) {
        requireNonNull(statusMatcher, "statusMatcher");
        return new JaxRsToStatusCodeMatcher(statusMatcher);
    }

    @Nonnull
    public static Matcher<Integer> withStatus(@Nonnull Status expectedStatus) {
        return is(requireNonNull(expectedStatus, "expectedStatus").getStatusCode());
    }

    JaxRsToStatusCodeMatcher(Matcher<? super Status> subMatcher) {
        super(subMatcher, "response status that", "status");
    }

    @Override
    protected Status featureValueOf(Integer statusCode) {
        return Status.fromStatusCode(statusCode);
    }
}
