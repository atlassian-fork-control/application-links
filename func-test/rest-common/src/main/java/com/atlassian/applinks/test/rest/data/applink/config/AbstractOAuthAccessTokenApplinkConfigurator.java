package com.atlassian.applinks.test.rest.data.applink.config;

import com.atlassian.applinks.test.data.applink.config.AbstractTestApplinkConfigurator;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Abstract class for access tokens configurators for Applinks that provide control over the 3LO access for specific
 * users.
 *
 * @since 5.0
 */
public abstract class AbstractOAuthAccessTokenApplinkConfigurator extends AbstractTestApplinkConfigurator {
    protected final String username;
    protected final TokenType tokenType;

    protected AbstractOAuthAccessTokenApplinkConfigurator(@Nonnull String username, @Nonnull TokenType tokenType) {
        this.username = requireNonNull(username, "username");
        this.tokenType = requireNonNull(tokenType, "tokenType");
    }

    protected enum TokenType {
        SERVICE_PROVIDER, CONSUMER
    }

}
