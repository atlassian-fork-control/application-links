package com.atlassian.applinks.test.rest.feature;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.SimpleRestRequest;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * REST tester for the feature discovery resource.
 *
 * @since 4.3
 */
public class FeatureDiscoveryRestTester extends BaseRestTester {
    public FeatureDiscoveryRestTester(@Nonnull ApplinksRestUrls restUrls) {
        super(restUrls.featureDiscovery());
    }

    public FeatureDiscoveryRestTester(@Nonnull ApplinksRestUrls restUrls,
                                      @Nullable TestAuthentication defaultAuthentication) {
        super(restUrls.featureDiscovery(), defaultAuthentication);
    }

    /**
     * Get all discovered features
     *
     * @param request request
     * @return response, which will contain a list of discovered features
     */
    @Nonnull
    public Response getAll(@Nonnull SimpleRestRequest request) {
        return super.get(request);
    }


    /**
     * Check whether a specific feature key has been discovered
     *
     * @param request feature discovery request
     * @return response
     */
    @Nonnull
    public Response get(@Nonnull FeatureDiscoveryRequest request) {
        return super.get(request);
    }

    /**
     * Discover a specific feature
     *
     * @param request feature discovery request
     * @return response
     */
    @Nonnull
    public Response put(@Nonnull FeatureDiscoveryRequest request) {
        return super.put(request);
    }
}
