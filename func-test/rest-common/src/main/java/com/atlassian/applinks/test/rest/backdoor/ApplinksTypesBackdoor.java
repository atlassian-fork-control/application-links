package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.CustomizableRestRequest;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrls.forRestModule;

public class ApplinksTypesBackdoor {
    static final String APPLINKS_TYPES_RESOURCE_PATH = "types";

    static final String ENTITY_TYPE_RESOURCE_PATH = "entity";
    static final String APPLICATION_TYPE_RESOURCE_PATH = "application";

    private final BaseRestTester tester;

    public ApplinksTypesBackdoor(@Nonnull String baseUrl) {
        // no need for authentication, the resource allows anonymous access
        this.tester = new BaseRestTester(getRestTypesPath(baseUrl));
    }

    private static RestUrl getRestTypesPath(@Nonnull String baseUrl) {
        return forRestModule(baseUrl, ApplinksRestUrls.RestModules.APPLINKS_TESTS).path(APPLINKS_TYPES_RESOURCE_PATH);
    }

    @SuppressWarnings("unchecked")
    public List<ApplinkTypeMetadata> getEntityTypeMetadata() {
        List<ApplinkTypeMetadata> entityList = new ArrayList<ApplinkTypeMetadata>();
        List<Map<String, String>> entityTypes = tester.get(new CustomizableRestRequest.Builder()
                .addPath(ENTITY_TYPE_RESOURCE_PATH)
                .build()).as(List.class);

        for (Map<String, String> entity : entityTypes) {
            entityList.add(new ApplinkTypeMetadata(entity));
        }

        return entityList;
    }

    @SuppressWarnings("unchecked")
    public List<ApplinkTypeMetadata> getApplicationTypeMetadata() {
        List<ApplinkTypeMetadata> applicationList = new ArrayList<ApplinkTypeMetadata>();
        List<Map<String, String>> applicationTypes = tester.get(new CustomizableRestRequest.Builder()
                .addPath(APPLICATION_TYPE_RESOURCE_PATH)
                .build()).as(List.class);

        for (Map<String, String> application : applicationTypes) {
            applicationList.add(new ApplinkTypeMetadata(application));
        }
        return applicationList;
    }
}
