package com.atlassian.applinks.test.rest;

import com.atlassian.applinks.internal.rest.RestUrl;

import javax.annotation.Nonnull;

/**
 * The simplest REST request: no body, no extra path and any customizable attributes other than authentication and the
 * response specification as provided by {@link AbstractRestRequest}.
 *
 * @see CustomizableRestRequest for a request with customizable body, path and query params
 * @since 4.3
 */
public class SimpleRestRequest extends AbstractRestRequest {
    /**
     * Default instance expecting an OK response.
     */
    public static final SimpleRestRequest DEFAULT_INSTANCE = new SimpleRestRequest.Builder().build();

    protected SimpleRestRequest(AbstractBuilder<?, ?> builder) {
        super(builder);
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.EMPTY;
    }

    public static class Builder extends AbstractBuilder<Builder, SimpleRestRequest> {
        @Nonnull
        @Override
        public SimpleRestRequest build() {
            return new SimpleRestRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
