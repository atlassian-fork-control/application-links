package com.atlassian.applinks.test.rest.specification;

import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;

/**
 * Specifications for expectations of results of feature discovery requests.
 *
 * @since 4.3
 */
public final class FeatureDiscoveryExpectations {
    private FeatureDiscoveryExpectations() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static ResponseSpecification expectSingleDiscoveredFeature(@Nullable String featureKey) {
        return expectDiscoveredFeatures(contains(featureKey));
    }

    @Nonnull
    public static ResponseSpecification expectDiscoveredFeatures(Matcher<?> featuresMatcher) {
        return RestExpectations.expectBody(featuresMatcher);
    }

    @Nonnull
    public static ResponseSpecification expectNoDiscoveredFeatures() {
        return expectDiscoveredFeatures(emptyIterable());
    }
}
