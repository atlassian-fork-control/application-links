package com.atlassian.applinks.test.rest.oauth;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forOAuthRestModule;

/**
 * REST tester for the OAuth applink consumer resource: {@code com.atlassian.applinks.oauth.rest.OAuthApplicationLinkResource} in {@code
 * applinks-oauth}.
 *
 * @since 5.0
 */
public class OAuthApplicationLinkRestTester extends BaseRestTester {
    public OAuthApplicationLinkRestTester(@Nonnull TestedInstance instance) {
        super(forOAuthRestModule(instance).applicationLink());
    }

    public OAuthApplicationLinkRestTester(@Nonnull TestedInstance instance,
                                          @Nullable TestAuthentication defaultAuthentication) {
        super(forOAuthRestModule(instance).applicationLink(), defaultAuthentication);
    }

    @Nonnull
    public Response getConsumers(@Nonnull OAuthConsumerRequest request) {
        return get(request);
    }

    @Nonnull
    public Response putConsumer(@Nonnull OAuthConsumerRequest request) {
        return put(request);
    }
}
