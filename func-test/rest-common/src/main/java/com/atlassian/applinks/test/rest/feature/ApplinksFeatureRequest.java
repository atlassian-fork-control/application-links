package com.atlassian.applinks.test.rest.feature;

import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.rest.AbstractRestRequest;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

import static java.util.Objects.requireNonNull;

/**
 * Request to the "feature" resource for a specific feature.
 *
 * @since 4.3
 */
public class ApplinksFeatureRequest extends AbstractRestRequest {
    private final ApplinksFeatures feature;

    /**
     * Create default request for a specific {@code feature}. This can only be used with GET/PUT as it expects a 200
     * response.
     *
     * @param feature feature to create the request for
     * @return feature request
     */
    @Nonnull
    public static ApplinksFeatureRequest forFeature(@Nonnull ApplinksFeatures feature) {
        return new Builder(feature).build();
    }

    /**
     * Create a request to disable a specific {@code feature} using DELETE, expecting 204 "No Content" response.
     *
     * @param feature feature to create the request for
     * @return disable feature request
     */
    @Nonnull
    public static ApplinksFeatureRequest disableFeature(@Nonnull ApplinksFeatures feature) {
        return new Builder(feature).expectStatus(Response.Status.NO_CONTENT).build();
    }

    private ApplinksFeatureRequest(Builder builder) {
        super(builder);
        this.feature = builder.feature;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.forPath(feature.name());
    }

    public static final class Builder extends AbstractBuilder<Builder, ApplinksFeatureRequest> {
        private final ApplinksFeatures feature;

        public Builder(@Nonnull ApplinksFeatures feature) {
            this.feature = requireNonNull(feature, "feature");
        }

        @Nonnull
        @Override
        public ApplinksFeatureRequest build() {
            return new ApplinksFeatureRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
