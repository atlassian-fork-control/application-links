package com.atlassian.applinks.test.rest.specification;

import com.atlassian.applinks.internal.rest.model.capabilities.RestRemoteApplicationCapabilities;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.test.rest.specification.ApplinkErrorExpectations.expectErrorType;
import static org.hamcrest.Matchers.nullValue;

public class ApplinksRemoteCapabilitiesExpectations {

    private ApplinksRemoteCapabilitiesExpectations() {
        throw new AssertionError("Do not instantiate " + ApplinksRemoteCapabilitiesExpectations.class.getSimpleName());
    }

    @Nonnull
    public static ResponseSpecification applinksVersion(@Nonnull Matcher<?> versionMatcher) {
        return RestAssured.expect().body(RestRemoteApplicationCapabilities.APPLINKS_VERSION, versionMatcher);
    }

    @Nonnull
    public static ResponseSpecification applicationVersion(@Nonnull Matcher<?> versionMatcher) {
        return RestAssured.expect().body(RestRemoteApplicationCapabilities.APPLICATION_VERSION, versionMatcher);
    }

    @Nonnull
    public static ResponseSpecification capabilities(@Nonnull Matcher<?> capabilitiesMatcher) {
        return RestAssured.expect().body(RestRemoteApplicationCapabilities.CAPABILITIES, capabilitiesMatcher);
    }

    @Nonnull
    public static ResponseSpecification expectError(@Nonnull ApplinkErrorType applinkErrorType) {
        return expectErrorType(RestRemoteApplicationCapabilities.ERROR, applinkErrorType);
    }

    @Nonnull
    public static ResponseSpecification expectErrorDetails(@Nonnull Matcher<?> detailsMatcher) {
        return ApplinkErrorExpectations.expectErrorDetails(RestRemoteApplicationCapabilities.ERROR, detailsMatcher);
    }

    @Nonnull
    public static ResponseSpecification expectNoError() {
        return RestAssured.expect().body(RestRemoteApplicationCapabilities.ERROR, nullValue());
    }
}
