package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.CustomizableRestRequest;
import com.atlassian.applinks.test.rest.model.RestAccessToken;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forTestRestModule;

/**
 * Backdoor to automate the token exchange for simulating 3LO dance. Requires the {@code applinks-tests} plugin to be
 * running on the tested instances.
 *
 * @since 5.0
 */
public class AccessTokenBackdoor {
    private static final RestUrl ACCESS_TOKEN_PATH = RestUrl.forPath("access-token");
    private static final RestUrl SERVICE_PROVIDER_PATH = RestUrl.forPath("service-provider");
    private static final RestUrl CONSUMER_PATH = RestUrl.forPath("consumer");

    @Nonnull
    public RestAccessToken createServiceProviderToken(@Nonnull TestApplink.Side side, @Nonnull String username) {
        Response response = createServiceProviderTester(side.product()).put(new CustomizableRestRequest.Builder()
                .addPath(side.id())
                .addPath(username)
                .expectStatus(Status.CREATED)
                .build());

        return response.as(RestAccessToken.class);
    }

    @Nonnull
    public RestAccessToken getServiceProviderAccessToken(@Nonnull TestApplink.Side side, @Nonnull String username) {
        Response response = createServiceProviderTester(side.product()).get(new CustomizableRestRequest.Builder()
                .addPath(side.id())
                .addPath(username)
                .expectStatus(Status.OK)
                .build());

        return response.as(RestAccessToken.class);
    }

    @Nonnull
    public RestAccessToken addConsumerToken(@Nonnull TestApplink.Side side, @Nonnull String username,
                                            @Nonnull RestAccessToken token) {
        Response response = createConsumerTester(side.product()).put(new CustomizableRestRequest.Builder()
                .addPath(side.id())
                .addPath(username)
                .body(token)
                .expectStatus(Status.CREATED)
                .build());

        return response.as(RestAccessToken.class);
    }

    /**
     * Equivalent to {@link #setUpAccessFor(TestApplink, String, String)} for the same {@code username} on both sides.
     *
     * @param applink  the test Applink
     * @param username username to set up 3LO access for
     */
    public void setUpAccessFor(@Nonnull TestApplink applink, @Nonnull String username) {
        setUpAccessFor(applink, username, username);
    }

    /**
     * Set-up 3LO access for {@code fromUsername} in {@code from} side of {@code applink} to {@code toUsername} in
     * {@code to} side of {@code applink}. After successfully executing this methods user {@code fromUsername} on the
     * {@code from} side should be able to retrieve data on the {@code to} side as {@code toUsername}. NOTE: The 3LO
     * authorisation is one-directional.
     *
     * @param applink      test test Applink
     * @param fromUsername username on the {@code from} side of the applink
     * @param toUsername   username on the {@code to} side of the applink
     */
    public void setUpAccessFor(@Nonnull TestApplink applink, @Nonnull String fromUsername, @Nonnull String toUsername) {
        RestAccessToken token = createServiceProviderToken(applink.to(), toUsername);
        addConsumerToken(applink.from(), fromUsername, token);
    }

    public void deleteServiceProviderToken(@Nonnull TestApplink.Side side, @Nonnull String username) {
        createServiceProviderTester(side.product()).delete(new CustomizableRestRequest.Builder()
                .addPath(side.id())
                .addPath(username)
                .expectStatus(Status.NO_CONTENT)
                .build());
    }

    public void deleteConsumerToken(@Nonnull TestApplink.Side side, @Nonnull String username) {
        createConsumerTester(side.product()).delete(new CustomizableRestRequest.Builder()
                .addPath(side.id())
                .addPath(username)
                .expectStatus(Status.NO_CONTENT)
                .build());
    }

    public RestAccessToken getConsumerToken(@Nonnull TestApplink.Side side, @Nonnull String username) {
        Response response = createConsumerTester(side.product()).get(new CustomizableRestRequest.Builder()
                .addPath(side.id())
                .addPath(username)
                .expectStatus(Status.OK)
                .build());

        return response.as(RestAccessToken.class);
    }

    BaseRestTester createServiceProviderTester(TestedInstance instance) {
        return new BaseRestTester(forTestRestModule(instance).oauth()
                .add(ACCESS_TOKEN_PATH)
                .add(SERVICE_PROVIDER_PATH));
    }

    BaseRestTester createConsumerTester(TestedInstance instance) {
        return new BaseRestTester(forTestRestModule(instance).oauth()
                .add(ACCESS_TOKEN_PATH)
                .add(CONSUMER_PATH));
    }
}
