package com.atlassian.applinks.test.rest.applink;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.RestVersion;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forDefaultRestModule;

/**
 * REST tester for the V2 Application Links REST API, which contains V1 plus extra methods for authentication
 * configuration. This requires admin privileges to interact with.
 *
 * @since 5.2
 */
public class ApplinksV2RestTester extends BaseRestTester {
    public ApplinksV2RestTester(@Nonnull TestedInstance instance) {
        super(getRestUrl(instance));
    }

    public ApplinksV2RestTester(@Nonnull TestedInstance instance, @Nullable TestAuthentication defaultAuthentication) {
        super(getRestUrl(instance), defaultAuthentication);
    }

    private static RestUrl getRestUrl(@Nonnull TestedInstance instance) {
        return forDefaultRestModule(instance, RestVersion.V2).applicationLink();
    }
}
