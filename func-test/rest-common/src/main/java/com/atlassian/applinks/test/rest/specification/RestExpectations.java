package com.atlassian.applinks.test.rest.specification;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.specification.ResponseSpecification;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;

import static com.google.common.collect.Iterables.filter;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * @since 4.3
 */
public final class RestExpectations {
    private static final Predicate<String> NOT_BLANK = new Predicate<String>() {
        public boolean apply(String value) {
            return StringUtils.isNotBlank(value);
        }
    };

    private RestExpectations() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static String jsonPath(@Nonnull String... paths) {
        requireNonNull(paths, "paths");
        return join(filter(ImmutableList.copyOf(paths), NOT_BLANK), '.');
    }

    @Nonnull
    public static ResponseSpecification allOf(@Nonnull ResponseSpecification... expectations) {
        requireNonNull(expectations, "expectations");
        ResponseSpecBuilder builder = new ResponseSpecBuilder();
        for (ResponseSpecification expectation : expectations) {
            builder.addResponseSpecification(expectation);
        }
        return builder.build();
    }

    @Nonnull
    public static ResponseSpecification expectBody(@Nonnull Matcher<?> bodyMatcher) {
        // the empty field ("") is required to make RestAssured parse the body
        return expectBody("", bodyMatcher);
    }

    @Nonnull
    public static ResponseSpecification expectBody(@Nonnull String property, @Nonnull Matcher<?> bodyMatcher) {
        requireNonNull(property, "property");
        requireNonNull(bodyMatcher, "bodyMatcher");
        return RestAssured.expect().body(property, bodyMatcher);
    }
}
