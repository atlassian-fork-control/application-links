package com.atlassian.applinks.test.rest;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.data.applink.TestApplink;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public abstract class AbstractApplinkRequest extends AbstractApplinksRestRequest {
    protected final String applinkId;

    public AbstractApplinkRequest(@Nonnull AbstractApplinkRequestBuilder<?, ?> builder) {
        super(builder);
        this.applinkId = builder.applinkId;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.forPath(applinkId);
    }

    protected static abstract class AbstractApplinkRequestBuilder<B extends AbstractApplinkRequestBuilder<B, R>,
            R extends AbstractApplinkRequest> extends AbstractApplinksBuilder<B, R> {
        private final String applinkId;

        protected AbstractApplinkRequestBuilder(@Nonnull String applinkId) {
            this.applinkId = requireNonNull(applinkId, "applinkId");
        }

        protected AbstractApplinkRequestBuilder(@Nonnull TestApplink.Side applink) {
            this.applinkId = requireNonNull(applink, "applink").id();
        }
    }
}
