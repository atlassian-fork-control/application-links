package com.atlassian.applinks.test.rest.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.rest.AbstractRestRequest;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Request to the "capabilities" resource.
 *
 * @since 5.0
 */
public class ApplinksCapilitiesRequest extends AbstractRestRequest {
    private final String capabilityName;

    private ApplinksCapilitiesRequest(Builder builder) {
        super(builder);
        this.capabilityName = builder.capabilityName;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return capabilityName != null ? RestUrl.forPath(capabilityName) : RestUrl.EMPTY;
    }

    public static final class Builder extends AbstractBuilder<Builder, ApplinksCapilitiesRequest> {
        private final String capabilityName;

        public Builder(@Nonnull ApplinksCapabilities capability) {
            this.capabilityName = requireNonNull(capability, "capability").name();
        }

        public Builder(@Nonnull String capabilityName) {
            this.capabilityName = requireNonNull(capabilityName, "capabilityName");
        }


        public Builder() {
            this.capabilityName = null;
        }

        @Nonnull
        @Override
        public ApplinksCapilitiesRequest build() {
            return new ApplinksCapilitiesRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
