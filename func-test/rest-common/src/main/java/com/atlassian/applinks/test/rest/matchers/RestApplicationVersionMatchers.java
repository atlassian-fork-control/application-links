package com.atlassian.applinks.test.rest.matchers;

import com.atlassian.applinks.internal.rest.model.capabilities.RestApplicationVersion;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

import static com.atlassian.applinks.test.rest.matchers.RestMatchers.hasPropertyThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.notNullValue;

/**
 * @since 5.0
 */
public final class RestApplicationVersionMatchers {
    private RestApplicationVersionMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<?> expectValidVersion() {
        return expectVersionThat(notNullValue(), notNullValue(), notNullValue(), notNullValue(), notNullValue());
    }

    @Nonnull
    public static Matcher<?> expectVersion(int major, int minor, int bugfix) {
        return expectVersionThat(
                is(String.format("%s.%s.%s", major, minor, bugfix)),
                is(major),
                is(minor),
                is(bugfix),
                isEmptyString());
    }

    @Nonnull
    public static Matcher<?> expectVersionThat(@Nonnull Matcher<?> versionString,
                                               @Nonnull Matcher<?> major,
                                               @Nonnull Matcher<?> minor,
                                               @Nonnull Matcher<?> bugfix,
                                               @Nonnull Matcher<?> suffix) {
        return allOf(withVersionStringThat(versionString),
                withMajorThat(major),
                withMinorThat(minor),
                withBugfixThat(bugfix),
                withSuffixThat(suffix));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withVersionStringThat(@Nonnull Matcher<?> versionStringMatcher) {
        return hasPropertyThat(RestApplicationVersion.VERSION_STRING, versionStringMatcher);
    }

    @Nonnull
    public static Matcher<?> withVersionString(@Nullable String expectedVersionString) {
        return withVersionStringThat(is(expectedVersionString));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withMajorThat(@Nonnull Matcher<?> majorMatcher) {
        return hasPropertyThat(RestApplicationVersion.MAJOR, majorMatcher);
    }

    @Nonnull
    public static Matcher<?> withMajor(int expectedMajor) {
        return withMajorThat(is(expectedMajor));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withMinorThat(@Nonnull Matcher<?> minorMatcher) {
        return hasPropertyThat(RestApplicationVersion.MINOR, minorMatcher);
    }

    @Nonnull
    public static Matcher<?> withMinor(int expectedMinor) {
        return withMinorThat(is(expectedMinor));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withBugfixThat(@Nonnull Matcher<?> bugfixMatcher) {
        return hasPropertyThat(RestApplicationVersion.BUGFIX, bugfixMatcher);
    }

    @Nonnull
    public static Matcher<?> withBugfix(int expectedBugfix) {
        return withBugfixThat(is(expectedBugfix));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withSuffixThat(@Nonnull Matcher<?> suffixMatcher) {
        return hasPropertyThat(RestApplicationVersion.SUFFIX, suffixMatcher);
    }

    @Nonnull
    public static Matcher<?> withSuffix(@Nullable String expectedSuffix) {
        return withSuffixThat(is(expectedSuffix));
    }
}
