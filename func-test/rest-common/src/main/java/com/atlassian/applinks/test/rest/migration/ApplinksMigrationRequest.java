package com.atlassian.applinks.test.rest.migration;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.AbstractRestRequest;

import javax.annotation.Nonnull;

public class ApplinksMigrationRequest extends AbstractRestRequest {
    private final String applinkId;

    protected ApplinksMigrationRequest(final Builder builder) {
        super(builder);
        this.applinkId = builder.id;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.forPath(applinkId);
    }

    public static final class Builder extends AbstractBuilder<Builder, ApplinksMigrationRequest> {
        private final String id;

        public Builder(@Nonnull TestApplink.Side applink) {
            this.id = applink.id();
        }

        @Nonnull
        @Override
        public ApplinksMigrationRequest build() {
            return new ApplinksMigrationRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }

}
