package com.atlassian.applinks.test.rest.applink;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.RestVersion;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * REST tester for the V1 Application Links REST API. This requires admin privileges to interact with.
 *
 * @since 4.3
 */
public class ApplinksV1RestTester extends BaseRestTester {
    public ApplinksV1RestTester(@Nonnull TestedInstance instance) {
        super(getRestUrl(instance));
    }

    public ApplinksV1RestTester(@Nonnull TestedInstance instance, @Nullable TestAuthentication defaultAuthentication) {
        super(getRestUrl(instance), defaultAuthentication);
    }

    private static RestUrl getRestUrl(@Nonnull TestedInstance instance) {
        return ApplinksRestUrlsFactory.forDefaultRestModule(instance, RestVersion.V1).applicationLink();
    }
}
