package com.atlassian.applinks.test.data.applink.config;

import com.atlassian.applinks.test.backdoor.TrustedAppsBackdoor;
import com.atlassian.applinks.test.data.applink.TestApplink;

import javax.annotation.Nonnull;

/**
 * @since 5.0
 */
public class TrustedAppsApplinkConfigurator extends AbstractTestApplinkConfigurator {
    @Nonnull
    public static TrustedAppsApplinkConfigurator enableTrustedApps() {
        return setUpTrustedApps(true, true);
    }

    @Nonnull
    public static TrustedAppsApplinkConfigurator disableTrustedApps() {
        return setUpTrustedApps(false, false);
    }

    @Nonnull
    public static TrustedAppsApplinkConfigurator setUpTrustedApps(boolean incoming, boolean outgoing) {
        return new TrustedAppsApplinkConfigurator(incoming, outgoing);
    }

    private final boolean incoming;
    private final boolean outgoing;

    private final TrustedAppsBackdoor backdoor = new TrustedAppsBackdoor();

    public TrustedAppsApplinkConfigurator(boolean incoming, boolean outgoing) {
        this.incoming = incoming;
        this.outgoing = outgoing;
    }

    @Override
    public void configureSide(@Nonnull TestApplink.Side side) {
        backdoor.setStatus(side, incoming, outgoing);
    }
}
