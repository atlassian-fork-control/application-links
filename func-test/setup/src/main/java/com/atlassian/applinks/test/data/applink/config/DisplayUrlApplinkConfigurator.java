package com.atlassian.applinks.test.data.applink.config;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * {@link TestApplinkConfigurator} that updates applink display URL.
 */
public class DisplayUrlApplinkConfigurator extends AbstractTestApplinkConfigurator {
    @Nonnull
    public static TestApplinkConfigurator setDisplayUrl(@Nonnull String url) {
        return new DisplayUrlApplinkConfigurator(url);
    }

    private final String url;

    public DisplayUrlApplinkConfigurator(@Nonnull String url) {
        this.url = requireNonNull(url, "url");
    }

    public void configureSide(@Nonnull TestApplink.Side side) {
        new ApplinksBackdoor(side.product()).setDisplayUrl(side, url);
    }
}
