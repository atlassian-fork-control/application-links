package com.atlassian.applinks.test.backdoor;

import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.CustomizableRestRequest;
import com.atlassian.applinks.test.rest.model.RestUser;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.authentication.TestAuthentication.admin;
import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forExternalRestModule;
import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

/**
 * Backdoor to create and remove users, only works against Refapp.
 *
 * @since 5.0
 */
public class RefappUserManagementBackdoor {
    private static final String REFAPP_AUTH_MODULE = "auth";
    private static final String USERS_PATH = "users";

    private final BaseRestTester usersTester;

    public RefappUserManagementBackdoor(@Nonnull TestedInstance testedInstance) {
        requireNonNull(testedInstance, "testedInstance");
        checkState(testedInstance.getProduct() == SupportedProducts.REFAPP, "Only Refapp supported");
        this.usersTester = new BaseRestTester(
                forExternalRestModule(testedInstance, REFAPP_AUTH_MODULE).path(USERS_PATH),
                admin());
    }

    @Nonnull
    public Response createUser(@Nonnull String name, @Nonnull RestUser user) {
        requireNonNull(name, "name");
        requireNonNull(user, "user");
        return usersTester.put(new CustomizableRestRequest.Builder()
                .addPath(name)
                .body(user)
                .expectStatus(Status.CREATED)
                .build());
    }

    @Nonnull
    public Response deleteUser(@Nonnull String name) {
        requireNonNull(name, "name");
        return usersTester.delete(new CustomizableRestRequest.Builder()
                .addPath(name)
                .expectStatus(Status.NO_CONTENT)
                .build());
    }
}
