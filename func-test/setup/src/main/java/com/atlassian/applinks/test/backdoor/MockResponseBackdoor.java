package com.atlassian.applinks.test.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.response.MockResponseDefinition;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.CustomizableRestRequest;
import com.atlassian.applinks.test.rest.SimpleRestRequest;
import com.atlassian.applinks.test.rest.model.RestMockResponseRequest;
import com.atlassian.applinks.test.rest.model.RestRequestPredicate;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forTestRestModule;

/**
 * Backdoor to manage mock response setup on the target application. Requires {@code applinks-tests} to work.
 *
 * @since 5.0
 */
public class MockResponseBackdoor {
    private static final RestUrl MOCK_RESPONSE_SETUP_PATH = RestUrl.forPath("mock-response");

    private final BaseRestTester mockResponseTester;

    public MockResponseBackdoor(@Nonnull TestedInstance testedInstance) {
        this.mockResponseTester = new BaseRestTester(forTestRestModule(testedInstance).root()
                .add(MOCK_RESPONSE_SETUP_PATH));
    }

    @Nonnull
    public Response addResponse(@Nonnull RestRequestPredicate predicate,
                                @Nonnull MockResponseDefinition responseDefinition) {
        return mockResponseTester.post(new CustomizableRestRequest.Builder()
                .body(new RestMockResponseRequest(predicate, responseDefinition))
                .expectStatus(Status.CREATED)
                .build());
    }

    public void reset() {
        mockResponseTester.delete(new SimpleRestRequest.Builder()
                .expectStatus(Status.NO_CONTENT)
                .build());
    }
}
