package com.atlassian.applinks.test.data.applink.config;

import com.atlassian.applinks.internal.common.rest.model.oauth.RestConsumer;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.oauth.OAuthApplicationLinkRestTester;
import com.atlassian.applinks.test.rest.oauth.OAuthConsumerRequest;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

import static java.util.Objects.requireNonNull;

/**
 * Configurator to add arbitrary OAuth consumers to the application links.
 *
 * @since 5.0
 */
public class OAuthConsumerApplinkConfigurator extends AbstractTestApplinkConfigurator {
    @Nonnull
    public static OAuthConsumerApplinkConfigurator addOAuthConsumerAutoConfigure() {
        return new OAuthConsumerApplinkConfigurator(new RestConsumer(), true);
    }

    @Nonnull
    public static OAuthConsumerApplinkConfigurator addOAuthConsumer(@Nonnull RestConsumer consumer) {
        return new OAuthConsumerApplinkConfigurator(consumer, false);
    }

    private final RestConsumer consumer;
    private final boolean autoConfigure;

    private OAuthConsumerApplinkConfigurator(RestConsumer consumer, boolean autoConfigure) {
        this.consumer = requireNonNull(consumer, "consumer");
        this.autoConfigure = autoConfigure;
    }

    @Override
    public void configureSide(@Nonnull TestApplink.Side side) {
        new OAuthApplicationLinkRestTester(side.product()).putConsumer(new OAuthConsumerRequest.Builder(side)
                .consumer(consumer)
                .autoConfigure(autoConfigure)
                .authentication(TestAuthentication.admin())
                .expectStatus(Response.Status.CREATED)
                .build());
    }
}
