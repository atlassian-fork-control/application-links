package com.atlassian.applinks.test.data.applink.config;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.backdoor.BasicAuthenticationBackdoor;
import com.atlassian.applinks.test.data.applink.TestApplink;

import javax.annotation.Nonnull;

/**
 * @since 5.2
 */
public class BasicAuthenticationApplinkConfigurator extends AbstractTestApplinkConfigurator {
    private final TestAuthentication authentication;

    @Nonnull
    public static BasicAuthenticationApplinkConfigurator enableBasic(TestAuthentication authentication) {
        return new BasicAuthenticationApplinkConfigurator(authentication);
    }

    private final BasicAuthenticationBackdoor backdoor = new BasicAuthenticationBackdoor();

    public BasicAuthenticationApplinkConfigurator(TestAuthentication testAuthentication) {
        authentication = testAuthentication;
    }

    @Override
    public void configureSide(@Nonnull TestApplink.Side side) {
        backdoor.setStatus(side, authentication);
    }
}
