package com.atlassian.applinks.test.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.CustomizableRestRequest;
import com.atlassian.applinks.test.rest.SimpleRestRequest;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forTestRestModule;
import static java.util.Objects.requireNonNull;

/**
 * Backdoor to set up fake orphaned trust relationships. NOTE: this will only work in Refapp to Refapp tests.
 *
 * @since 5.2
 */
public class OrphanedTrustBackdoor {
    private static final RestUrl ORPHAN_BACKDOOR_RESOURCE_PATH = RestUrl.forPath("orphan-test");

    private final BaseRestTester orphanedTester;
    private final BaseRestTester consumerInformationTester;

    public OrphanedTrustBackdoor(@Nonnull TestedInstance instance) {
        this.orphanedTester = createOrphanedTester(instance);
        this.consumerInformationTester = createConsumerInformationTester(instance);
    }

    @Nonnull
    public String getOrphanedOAuthId() {
        // get OAuth consumer key from the oauth/consumer-info resource
        String id = consumerInformationTester.get(SimpleRestRequest.DEFAULT_INSTANCE)
                .xmlPath().getString("consumer.key");
        return requireNonNull(id, "id");
    }

    @Nonnull
    public String getOrphanedTrustedAppsId() {
        String id = orphanedTester.get(new CustomizableRestRequest.Builder()
                .addPath("trust")
                .expectStatus(Status.OK)
                .build())
                .jsonPath().getString("id");
        return requireNonNull(id, "id");
    }

    public void setUpOrphanedOAuth(@Nonnull String remoteId) {
        requireNonNull(remoteId, "remoteId");
        orphanedTester.get(new CustomizableRestRequest.Builder()
                .queryParam("refappOaId", remoteId)
                .expectStatus(Status.NO_CONTENT)
                .build());
    }

    public void setUpOrphanedTrustedApps(@Nonnull String remoteId) {
        requireNonNull(remoteId, "remoteId");
        orphanedTester.get(new CustomizableRestRequest.Builder()
                .queryParam("refappTaId", remoteId)
                .expectStatus(Status.NO_CONTENT)
                .build());
    }

    private static BaseRestTester createOrphanedTester(TestedInstance instance) {
        return new BaseRestTester(forTestRestModule(instance).root().add(ORPHAN_BACKDOOR_RESOURCE_PATH));
    }

    private static BaseRestTester createConsumerInformationTester(TestedInstance instance) {
        return new BaseRestTester(RestUrl.forPath(instance.getBaseUrl())
                .add("plugins")
                .add("servlet")
                .add("oauth")
                .add("consumer-info"));
    }
}
