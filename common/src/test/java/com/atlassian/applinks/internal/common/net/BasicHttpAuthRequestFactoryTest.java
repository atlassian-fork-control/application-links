package com.atlassian.applinks.internal.common.net;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BasicHttpAuthRequestFactoryTest {
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final Request.MethodType METHOD_TYPE = Request.MethodType.GET;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private RequestFactory<VoidRequest> requestFactory;

    private BasicHttpAuthRequestFactory<VoidRequest> basicHttpAuthRequestFactory;

    @Before
    public void before() {
        basicHttpAuthRequestFactory = new BasicHttpAuthRequestFactory<VoidRequest>(requestFactory, USERNAME, PASSWORD);
    }

    @Test
    public void createRequestValidUrl() {
        final String url = "https://www.google.com.au/?gfe_rd=cr&ei=irvuVMz2DbLu8weok4DADw&gws_rd=ssl";
        final VoidRequest voidRequest = mock(VoidRequest.class);

        when(requestFactory.createRequest(METHOD_TYPE, url)).thenReturn(voidRequest);

        basicHttpAuthRequestFactory.createRequest(METHOD_TYPE, url);

        verify(voidRequest).addBasicAuthentication("www.google.com.au", USERNAME, PASSWORD);
    }

    @Test
    public void createRequestInvalidUrl() {
        final String crapUrl = "http:\\\\groogle.com/blargh";

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(crapUrl);

        basicHttpAuthRequestFactory.createRequest(METHOD_TYPE, crapUrl);
    }

    private interface VoidRequest extends Request<VoidRequest, VoidResponse> {
    }

    private interface VoidResponse extends Response {
    }
}
