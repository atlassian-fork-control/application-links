package com.atlassian.applinks.internal.common.permission;

import org.junit.Test;

import java.util.EnumSet;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class PermissionLevelTest {
    @Test
    public void validateNewPermissionLevelConstants() {
        EnumSet<PermissionLevel> allLevels = EnumSet.allOf(PermissionLevel.class);
        assertThat("PermissionLevel values and ordering does not match the expectations. If you added a new " +
                        "PermissionLevel constant, please add it to the tests too. If you changed ordering, consider reverting " +
                        "your change as PermissionLevel ordinals should not be manipulated",
                allLevels, contains(PermissionLevel.USER, PermissionLevel.ADMIN, PermissionLevel.SYSADMIN));
    }
}