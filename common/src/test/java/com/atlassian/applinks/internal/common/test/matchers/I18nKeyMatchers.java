package com.atlassian.applinks.internal.common.test.matchers;

import com.atlassian.applinks.internal.common.i18n.I18nKey;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.matcher.ApplinksMatchers.newFeatureMatcher;
import static org.hamcrest.Matchers.is;

/**
 * @since 5.2
 */
public final class I18nKeyMatchers {
    private I18nKeyMatchers() {
        // do not instantiate
    }

    @Nonnull
    public static Matcher<I18nKey> withKey(@Nullable String expectedKey) {
        return withKeyThat(is(expectedKey));
    }

    @Nonnull
    public static Matcher<I18nKey> withKeyThat(@Nonnull Matcher<String> keyMatcher) {
        return newFeatureMatcher("key", keyMatcher, I18nKey::getKey);
    }
}
