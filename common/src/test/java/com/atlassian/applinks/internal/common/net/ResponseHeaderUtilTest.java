package com.atlassian.applinks.internal.common.net;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;

import static com.atlassian.applinks.internal.common.net.ResponseHeaderUtil.HEADER_CONTENT_SECURITY_POLICY;
import static com.atlassian.applinks.internal.common.net.ResponseHeaderUtil.HEADER_XFRAME_OPTIONS;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ResponseHeaderUtilTest {
    @Mock
    private HttpServletResponse response;

    @Test
    public void testPreventCrossFrameClickJacking() throws Exception {
        ResponseHeaderUtil.preventCrossFrameClickJacking(response);

        verify(response).setHeader(HEADER_XFRAME_OPTIONS, "SAMEORIGIN");
        verify(response).setHeader(HEADER_CONTENT_SECURITY_POLICY, "frame-ancestors 'self'");
    }
}