package com.atlassian.applinks.internal.rest.interceptor;

import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.core.HttpResponseContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class NoCacheHeaderInterceptorTest {
    private static final int MAX_AGE = 5;

    @Mock
    private MethodInvocation invocation;
    @Mock
    private HttpContext context;
    @Mock
    private HttpResponseContext responseContext;

    private NoCacheHeaderInterceptor interceptor = new NoCacheHeaderInterceptor();

    @Test
    public void shouldAddNoCacheToCacheControlHeader() throws Exception {
        CacheControl existingCacheControl = new CacheControl();
        existingCacheControl.setNoStore(true);
        existingCacheControl.setMaxAge(MAX_AGE);
        Response response = Response.ok().cacheControl(existingCacheControl).build();

        when(invocation.getHttpContext()).thenReturn(context);
        when(context.getResponse()).thenReturn(responseContext);
        when(responseContext.getResponse()).thenReturn(response);

        interceptor.intercept(invocation);

        ArgumentCaptor<Response> argument = ArgumentCaptor.forClass(Response.class);
        verify(responseContext, times(1)).setResponse(argument.capture());
        assertThat(argument.getValue().getMetadata().get("Cache-Control").get(0).toString(), containsString("no-store"));
        assertThat(argument.getValue().getMetadata().get("Cache-Control").get(0).toString(), containsString("max-age=" + MAX_AGE));
        assertThat(argument.getValue().getMetadata().get("Cache-Control"), hasItem("no-cache"));
    }

}