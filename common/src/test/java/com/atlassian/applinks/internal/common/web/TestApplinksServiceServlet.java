package com.atlassian.applinks.internal.common.web;

import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static java.util.Arrays.asList;

/**
 * @since 5.1
 */
final class TestApplinksServiceServlet extends AbstractApplinksServiceServlet {
    private Iterable<String> webResourceContexts;

    private String moduleKey;
    private String templateName;
    private Map<String, Object> templateData;

    private String decorator;
    private String pageTitle;
    private String activeTab;
    private String pageInitializer;

    private ServletException servletException;
    private IOException ioException;
    private ServiceException serviceException;


    public TestApplinksServiceServlet(AppLinkPluginUtil appLinkPluginUtil,
                                      I18nResolver i18nResolver,
                                      InternalHostApplication internalHostApplication,
                                      LoginUriProvider loginProvider,
                                      SoyTemplateRenderer soyTemplateRenderer,
                                      PageBuilderService pageBuilderService) {
        super(appLinkPluginUtil, i18nResolver, internalHostApplication, loginProvider, soyTemplateRenderer, pageBuilderService);
    }

    TestApplinksServiceServlet setWebResourceContexts(Iterable<String> contexts) {
        this.webResourceContexts = contexts;
        return this;
    }

    TestApplinksServiceServlet setWebResourceContexts(String... contexts) {
        return setWebResourceContexts(asList(contexts));
    }

    TestApplinksServiceServlet setServletException(ServletException exception) {
        this.servletException = exception;
        return this;
    }

    TestApplinksServiceServlet setIoException(IOException exception) {
        this.ioException = exception;
        return this;
    }

    TestApplinksServiceServlet setServiceException(ServiceException exception) {
        this.serviceException = exception;
        return this;
    }

    TestApplinksServiceServlet setTemplate(String moduleKey, String templateName) {
        this.moduleKey = moduleKey;
        this.templateName = templateName;
        return this;
    }

    TestApplinksServiceServlet setTemplateData(Map<String, Object> templateData) {
        this.templateData = templateData;
        return this;
    }

    TestApplinksServiceServlet setDecorator(String decorator) {
        this.decorator = decorator;
        return this;
    }

    TestApplinksServiceServlet setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
        return this;
    }

    TestApplinksServiceServlet setActiveTab(String activeTab) {
        this.activeTab = activeTab;
        return this;
    }

    TestApplinksServiceServlet setPageInitializer(String pageInitializer) {
        this.pageInitializer = pageInitializer;
        return this;
    }

    @Override
    protected void doServiceGet(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response)
            throws IOException, ServiceException, ServletException {
        if (servletException != null) {
            throw servletException;
        }
        if (ioException != null) {
            throw ioException;
        }
        if (serviceException != null) {
            throw serviceException;
        }
        if (moduleKey != null && templateName != null) {
            render(request, response, moduleKey, templateName,
                    (templateData == null ? Collections.emptyMap() : templateData));
        }
    }

    @Nonnull
    @Override
    protected Iterable<String> webResourceContexts() {
        return webResourceContexts;
    }

    @Nullable
    @Override
    protected String getDecorator(@Nonnull HttpServletRequest request) {
        return decorator;
    }

    @Nullable
    @Override
    protected String getPageTitle(@Nonnull HttpServletRequest request) {
        return pageTitle;
    }

    @Nullable
    @Override
    protected String getActiveTab(@Nonnull HttpServletRequest request) {
        return activeTab;
    }

    @Nullable
    @Override
    protected String getPageInitializer(@Nonnull HttpServletRequest request) {
        return pageInitializer;
    }


}
