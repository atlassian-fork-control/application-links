package com.atlassian.applinks.internal.common.auth.oauth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.AuthorisationURIGenerator;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.exception.ConsumerInformationUnavailableException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.rest.model.oauth.RestConsumer;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.rest.model.auth.compatibility.RestAuthenticationProvider;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.test.mock.MatchingMockRequestAnswer;
import com.atlassian.applinks.test.mock.MockApplicationLinkRequest;
import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.mock.TestApplinkIds;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseStatusException;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.EnumSet;
import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDisabledConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withEntity;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withEntityThat;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withMethodType;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@SuppressWarnings({"unchecked", "Duplicates"})
public class DefaultOAuthAutoConfiguratorTest {
    private static final ApplicationId LOCAL_APPLICATION_ID = TestApplinkIds.createRandomApplicationId();

    @Mock
    private RemoteCapabilitiesService remoteCapabilitiesService;
    @Mock
    private InternalHostApplication hostApplication;
    @Spy
    private OAuthConfigurator oAuthConfigurator = new MockOAuthConfigurator();

    @Mock
    private RequestFactory requestFactory;
    @Mock
    private ApplicationLinkRequestFactory applicationLinkRequestFactory;
    @MockApplink
    private ApplicationLink applink;

    private MatchingMockRequestAnswer requestAnswer = new MatchingMockRequestAnswer();

    private DefaultOAuthAutoConfigurator oAuthAutoConfigurator;

    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);

    @Before
    public void setUpMockRequestAnswer() throws CredentialsRequiredException {
        when(requestFactory.createRequest(any(MethodType.class), any())).thenAnswer(requestAnswer);
        when(applicationLinkRequestFactory.createRequest(any(MethodType.class), any())).thenAnswer(requestAnswer);
    }

    @Before
    public void setUpLocalApplicationId() {
        when(hostApplication.getId()).thenReturn(LOCAL_APPLICATION_ID);
    }

    @Before
    public void createAutoConfigurator() {
        oAuthAutoConfigurator = new DefaultOAuthAutoConfigurator(hostApplication, remoteCapabilitiesService,
                oAuthConfigurator);
    }

    @Test
    public void enableDefaultOAuthGivenStatusApiPresentAndSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(true, "5.0.5");
        setUpStatusApiOAuthResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), applink, requestFactory);

        assertThat(requestAnswer.executedRequests(), contains(statusApiPutRequest(ApplinkOAuthStatus.DEFAULT)));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDefaultOAuthConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createDefaultOAuthConfig());
    }

    @Test
    public void enableDefaultIncomingAndImpersonationOutgoingOAuthGivenStatusApiPresentAndSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(true, "5.0.5");
        setUpStatusApiOAuthResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), createOAuthWithImpersonationConfig(), applink, applicationLinkRequestFactory);

        assertThat(requestAnswer.executedRequests(), contains(statusApiPutRequest(new ApplinkOAuthStatus(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig()))));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDefaultOAuthConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createOAuthWithImpersonationConfig());
    }

    @Test
    public void enableDisabledIncomingAndImpersonationOutgoingOAuthGivenStatusApiPresentAndSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(true, "5.0.5");
        setUpStatusApiOAuthResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createDisabledConfig(), createOAuthWithImpersonationConfig(), applink, applicationLinkRequestFactory);

        assertThat(requestAnswer.executedRequests(), contains(statusApiPutRequest(new ApplinkOAuthStatus(createOAuthWithImpersonationConfig(), createDisabledConfig()))));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDisabledConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createOAuthWithImpersonationConfig());
    }

    @Test
    public void enableOAuthImpersonationGivenStatusApiPresentAndSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(true, "5.0.5");
        setUpStatusApiOAuthResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createOAuthWithImpersonationConfig(), applink, requestFactory);

        assertThat(requestAnswer.executedRequests(), contains(statusApiPutRequest(ApplinkOAuthStatus.IMPERSONATION)));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createOAuthWithImpersonationConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createOAuthWithImpersonationConfig());
    }

    @Test
    public void enableDefaultOAuthGivenStatusApiErrorResponse() throws Exception {
        setUpRemoteCapabilities(true, "5.0.5");
        setUpStatusApiOAuthResponse(statusErrorResponse());

        enableOAuthExpectingError(createDefaultOAuthConfig());

        assertThat(requestAnswer.executedRequests(), contains(statusApiPutRequest(ApplinkOAuthStatus.DEFAULT)));
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test
    public void enableDefaultOAuthGivenStatusApiRedirectResponse() throws Exception {
        setUpRemoteCapabilities(true, "5.0.5");
        setUpStatusApiOAuthResponse(redirectResponse());

        enableOAuthExpectingError(createDefaultOAuthConfig());

        assertThat(requestAnswer.executedRequests(), contains(statusApiPutRequest(ApplinkOAuthStatus.DEFAULT)));
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test
    public void enableDefaultOAuthGivenNoStatusApi5xSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), applink, requestFactory);

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer5xPutRequest(true, false),
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthAuthenticationProvider.class)
        ));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDefaultOAuthConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createDefaultOAuthConfig());
    }

    @Test
    public void enableDefaultIncomingAndImpersonationOutgoingOAuthGivenNoStatusApi5xSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), createOAuthWithImpersonationConfig(), applink, applicationLinkRequestFactory);

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer5xPutRequest(true, true),
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthAuthenticationProvider.class)
        ));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDefaultOAuthConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createOAuthWithImpersonationConfig());
    }

    @Test
    public void enableDisabledIncomingAndImpersonationOutgoingOAuthGivenNoStatusApi5xSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createDisabledConfig(), createOAuthWithImpersonationConfig(), applink, applicationLinkRequestFactory);

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer5xPutRequest(true, true)
        ));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDisabledConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createOAuthWithImpersonationConfig());
    }

    @Test
    public void enableDefaultIncomingAndDisabledOutgoingOAuthGivenNoStatusApi5xSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), createDisabledConfig(), applink, applicationLinkRequestFactory);

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthAuthenticationProvider.class)
        ));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDefaultOAuthConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createDisabledConfig());
    }

    @Test
    public void enableImpersonationIncomingAndDefaultOutgoingOAuthGivenNoStatusApi5xSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig(), applink, applicationLinkRequestFactory);

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer5xPutRequest(true, false),
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class)
        ));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createOAuthWithImpersonationConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createDefaultOAuthConfig());
    }

    @Test(expected = AuthenticationConfigurationException.class)
    public void enableImpersonationIncomingAndDefaultOutgoingOAuthGiveCredentialsRequiredException() throws Exception {
        AuthorisationURIGenerator authorisationURIGenerator = mock(AuthorisationURIGenerator.class);
        doThrow(new CredentialsRequiredException(authorisationURIGenerator, "")).when(applicationLinkRequestFactory).createRequest(any(MethodType.class), any());
        setUpRemoteCapabilities(false, "5.0.3");
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig(), applink, applicationLinkRequestFactory);
    }

    @Test
    public void enableOAuthWithImpersonationGivenNoStatusApi5xSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createOAuthWithImpersonationConfig(), applink, requestFactory);

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer5xPutRequest(true, true),
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class)
        ));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createOAuthWithImpersonationConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createOAuthWithImpersonationConfig());
    }

    @Test
    public void enableDefaultOAuthGivenNoStatusApi5xConsumerRequestFailed() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUp5xAuthenticationConsumerResponse(statusErrorResponse());

        enableOAuthExpectingError(createDefaultOAuthConfig());

        assertThat(requestAnswer.executedRequests(), contains(authenticationConsumer5xPutRequest(true, false)));
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test
    public void enableDefaultOAuthGivenNoStatusApi5xProviderRequestFailed() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(statusErrorResponse());

        enableOAuthExpectingError(createDefaultOAuthConfig());

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer5xPutRequest(true, false),
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class)));
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test
    public void enableDefaultOAuthGivenNoStatusApi4xSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "4.3.10");
        setUp4xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), applink, requestFactory);

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer4xPutRequest(true, false),
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthAuthenticationProvider.class)
        ));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDefaultOAuthConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createDefaultOAuthConfig());
    }

    @Test
    public void enableOAuthWithImpersonationGivenNoStatusApi4xSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "4.3.10");
        setUp4xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createOAuthWithImpersonationConfig(), applink, requestFactory);

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer4xPutRequest(true, true),
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class)
        ));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createOAuthWithImpersonationConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createOAuthWithImpersonationConfig());
    }

    @Test
    public void enableDefaultOAuthGivenNoStatusApi4xConsumerRequestFailed() throws Exception {
        setUpRemoteCapabilities(false, "4.3.10");
        setUp4xAuthenticationConsumerResponse(statusErrorResponse());

        enableOAuthExpectingError(createDefaultOAuthConfig());

        assertThat(requestAnswer.executedRequests(), contains(authenticationConsumer4xPutRequest(true, false)));
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test
    public void enableDefaultOAuthGivenNoStatusApi4xProviderRequestFailed() throws Exception {
        setUpRemoteCapabilities(false, "4.3.10");
        setUp4xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(statusErrorResponse());

        enableOAuthExpectingError(createDefaultOAuthConfig());

        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer4xPutRequest(true, false),
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class)));
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test(expected = AuthenticationConfigurationException.class)
    public void enableDefaultOAuthGivenCapabilitiesAccessError() throws Exception {
        setUpRemoteCapabilitiesAccessError();
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        // should propagate the access error (note: should never happen in practice anyway)
        oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), applink, requestFactory);
    }

    @Test
    public void enableDefaultOAuthGivenNullApplinksVersion() throws Exception {
        setUpRemoteCapabilities(false, null);
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), applink, requestFactory);

        // should be treated as 5.x no status API
        assertThat(requestAnswer.executedRequests(), contains(
                authenticationConsumer5xPutRequest(true, false),
                authenticationProviderPutRequest(OAuthAuthenticationProvider.class),
                authenticationProviderPutRequest(TwoLeggedOAuthAuthenticationProvider.class)
        ));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDefaultOAuthConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createDefaultOAuthConfig());
    }

    @Test(expected = AuthenticationConfigurationException.class)
    public void enableOAuthWithImpersonationGivenCapabilitiesAccessError() throws Exception {
        setUpRemoteCapabilitiesAccessError();
        setUp5xAuthenticationConsumerResponse(noContentResponse());
        setUpAuthenticationProviderResponse(noContentResponse());

        // should propagate the access error (note: should never happen in practice anyway)
        oAuthAutoConfigurator.enable(createOAuthWithImpersonationConfig(), applink, requestFactory);
    }

    @Test
    public void enableGivenPre4x() throws Exception {
        setUpRemoteCapabilities(false, "3.10.1");

        oAuthAutoConfigurator.enable(createDefaultOAuthConfig(), applink, requestFactory);

        // for remote apps pre-4.0 OAuth should not be configured
        assertThat(requestAnswer.executedRequests(), emptyIterable());
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test
    public void disableOAuthGivenStatusApiPresentAndSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(true, "5.0.5");
        setUpStatusApiOAuthResponse(noContentResponse());

        oAuthAutoConfigurator.disable(applink, requestFactory);

        assertThat(requestAnswer.executedRequests(), contains(statusApiPutRequest(ApplinkOAuthStatus.OFF)));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDisabledConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createDisabledConfig());
    }

    @Test
    public void disableOAuthGivenStatusApiErrorResponse() throws Exception {
        setUpRemoteCapabilities(true, "5.0.5");
        setUpStatusApiOAuthResponse(statusErrorResponse());

        disableOAuthExpectingError();

        assertThat(requestAnswer.executedRequests(), contains(statusApiPutRequest(ApplinkOAuthStatus.OFF)));
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test
    public void disableOAuthGiven5xNoStatusApiSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUpAutoConfigurationDeleteResponse(noContentResponse());

        oAuthAutoConfigurator.disable(applink, requestFactory);

        assertThat(requestAnswer.executedRequests(), contains(autoConfigurationDeleteRequest()));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDisabledConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createDisabledConfig());
    }

    @Test
    public void disableOAuthGiven5xNoStatusApiErrorResponse() throws Exception {
        setUpRemoteCapabilities(false, "5.0.3");
        setUpAutoConfigurationDeleteResponse(statusErrorResponse());

        disableOAuthExpectingError();

        assertThat(requestAnswer.executedRequests(), contains(autoConfigurationDeleteRequest()));
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test
    public void disableOAuthGiven4xNoStatusApiSuccessfulResponse() throws Exception {
        setUpRemoteCapabilities(false, "4.3.10");
        setUpAutoConfigurationDeleteResponse(noContentResponse());

        oAuthAutoConfigurator.disable(applink, requestFactory);

        assertThat(requestAnswer.executedRequests(), contains(autoConfigurationDeleteRequest()));
        verify(oAuthConfigurator).updateIncomingConfig(applink, createDisabledConfig());
        verify(oAuthConfigurator).updateOutgoingConfig(applink, createDisabledConfig());
    }

    @Test
    public void disableOAuthGiven4xNoStatusApiErrorResponse() throws Exception {
        setUpRemoteCapabilities(false, "4.3.10");
        setUpAutoConfigurationDeleteResponse(statusErrorResponse());

        disableOAuthExpectingError();

        assertThat(requestAnswer.executedRequests(), contains(autoConfigurationDeleteRequest()));
        verifyZeroInteractions(oAuthConfigurator);
    }

    @Test(expected = AuthenticationConfigurationException.class)
    public void disableOAuthGivenRemoteCapabilitiesAccessError() throws Exception {
        setUpRemoteCapabilitiesAccessError();
        setUpAutoConfigurationDeleteResponse(noContentResponse());

        // should propagate the access error (note: should never happen in practice anyway)
        oAuthAutoConfigurator.disable(applink, requestFactory);
    }

    private static MockApplicationLinkResponse noContentResponse() {
        return new MockApplicationLinkResponse().setStatus(Response.Status.NO_CONTENT);
    }

    private static MockApplicationLinkResponse redirectResponse() {
        return new MockApplicationLinkResponse().setStatus(Response.Status.TEMPORARY_REDIRECT);
    }

    private static MockApplicationLinkResponse statusErrorResponse() {
        MockApplicationLinkResponse mockErrorResponse = new MockApplicationLinkResponse();
        return mockErrorResponse.setResponseException(new ResponseStatusException("Wrong status", mockErrorResponse));
    }

    private void enableOAuthExpectingError(OAuthConfig config) {
        try {
            oAuthAutoConfigurator.enable(config, applink, requestFactory);
            fail("Expected AuthenticationConfigurationException");
        } catch (AuthenticationConfigurationException e) {
            // success
        }
    }

    private void disableOAuthExpectingError() {
        try {
            oAuthAutoConfigurator.disable(applink, requestFactory);
            fail("Expected AuthenticationConfigurationException");
        } catch (AuthenticationConfigurationException e) {
            // success
        }
    }

    private void setUpRemoteCapabilities(boolean statusApi, String applinksVersion) throws NoAccessException {
        RemoteApplicationCapabilities remoteApplicationCapabilities = mock(RemoteApplicationCapabilities.class);
        EnumSet<ApplinksCapabilities> capabilities = statusApi ? EnumSet.of(ApplinksCapabilities.STATUS_API) :
                EnumSet.noneOf(ApplinksCapabilities.class);

        if (applinksVersion != null) {
            when(remoteApplicationCapabilities.getApplinksVersion()).thenReturn(ApplicationVersion.parse(applinksVersion));
        }
        when(remoteApplicationCapabilities.getCapabilities()).thenReturn(capabilities);

        when(remoteCapabilitiesService.getCapabilities(applink)).thenReturn(remoteApplicationCapabilities);
    }

    private void setUpRemoteCapabilitiesAccessError() throws NoAccessException {
        when(remoteCapabilitiesService.getCapabilities(applink)).thenThrow(NotAuthenticatedException.class);
    }

    private void setUpStatusApiOAuthResponse(MockApplicationLinkResponse response) {
        requestAnswer.addResponse(MethodType.PUT, applink.getRpcUrl() + "/rest/applinks/3.0/status/"
                + LOCAL_APPLICATION_ID + "/oauth", response);
    }

    private void setUp5xAuthenticationConsumerResponse(MockApplicationLinkResponse response) {
        requestAnswer.addResponse(MethodType.PUT, applink.getRpcUrl() + "/rest/applinks-oauth/latest/applicationlink/"
                + LOCAL_APPLICATION_ID + "/authentication/consumer?autoConfigure=true", response);
    }

    private void setUp4xAuthenticationConsumerResponse(MockApplicationLinkResponse response) {
        requestAnswer.addResponse(MethodType.PUT, applink.getRpcUrl() + "/rest/applinks/2.0/applicationlink/"
                + LOCAL_APPLICATION_ID + "/authentication/consumer?autoConfigure=true", response);
    }

    private void setUpAuthenticationProviderResponse(MockApplicationLinkResponse response) {
        requestAnswer.addResponse(MethodType.PUT, applink.getRpcUrl() + "/rest/applinks/2.0/applicationlink/"
                + LOCAL_APPLICATION_ID + "/authentication/provider", response);
    }

    private void setUpAutoConfigurationDeleteResponse(MockApplicationLinkResponse response) {
        requestAnswer.addResponse(MethodType.DELETE, applink.getRpcUrl() + "/plugins/servlet/applinks/auth/conf/" +
                "oauth/autoconfig/" + LOCAL_APPLICATION_ID, response);
    }

    private Matcher<MockApplicationLinkRequest> statusApiPutRequest(ApplinkOAuthStatus expectedStatus) {
        return allOf(withMethodType(MethodType.PUT),
                withUrl(applink.getRpcUrl() + "/rest/applinks/3.0/status/" + LOCAL_APPLICATION_ID + "/oauth"),
                withEntity(new RestApplinkOAuthStatus(expectedStatus)));
    }

    private Matcher<MockApplicationLinkRequest> authenticationConsumer5xPutRequest(boolean has2Lo, boolean has2Loi) {
        return allOf(
                withMethodType(MethodType.PUT),
                withUrl(applink.getRpcUrl() + "/rest/applinks-oauth/latest/applicationlink/" + LOCAL_APPLICATION_ID
                        + "/authentication/consumer?autoConfigure=true"),
                withEntityThat(allOf(hasEntry(RestConsumer.TWO_LO_ALLOWED, has2Lo),
                        hasEntry(RestConsumer.TWO_LO_IMPERSONATION_ALLOWED, has2Loi)))
        );
    }

    private Matcher<MockApplicationLinkRequest> authenticationConsumer4xPutRequest(boolean has2Lo, boolean has2Loi) {
        return allOf(
                withMethodType(MethodType.PUT),
                withUrl(applink.getRpcUrl() + "/rest/applinks/2.0/applicationlink/" + LOCAL_APPLICATION_ID
                        + "/authentication/consumer?autoConfigure=true"),
                withEntityThat(allOf(hasEntry(RestConsumer.TWO_LO_ALLOWED, has2Lo),
                        hasEntry(RestConsumer.TWO_LO_IMPERSONATION_ALLOWED, has2Loi)))
        );
    }

    private Matcher<MockApplicationLinkRequest> authenticationProviderPutRequest(
            Class<? extends AuthenticationProvider> providerClass) {
        return allOf(
                withMethodType(MethodType.PUT),
                withUrl(applink.getRpcUrl() + "/rest/applinks/2.0/applicationlink/" + LOCAL_APPLICATION_ID
                        + "/authentication/provider"),
                withEntityThat(Matchers.allOf(
                        Matchers.<String, Object>hasEntry(RestAuthenticationProvider.PROVIDER, providerClass.getCanonicalName()),
                        hasEntry(is(RestAuthenticationProvider.CONFIG), notNullValue())
                ))
        );
    }

    private Matcher<MockApplicationLinkRequest> autoConfigurationDeleteRequest() {
        return allOf(withMethodType(MethodType.DELETE),
                withUrl(applink.getRpcUrl() + "/plugins/servlet/applinks/auth/conf/oauth/autoconfig/" +
                        LOCAL_APPLICATION_ID));
    }

    private static class MockOAuthConfigurator extends OAuthConfigurator {
        public MockOAuthConfigurator() {
            super(null, null, null, null);
        }

        @Nonnull
        @Override
        public OAuthConfig getIncomingConfig(@Nonnull ApplicationLink applink) {
            return createDefaultOAuthConfig();
        }

        @Nonnull
        @Override
        public OAuthConfig getOutgoingConfig(@Nonnull ApplicationLink link) {
            return createDefaultOAuthConfig();
        }

        @Override
        public void updateOutgoingConfig(@Nonnull ApplicationLink applink, @Nonnull OAuthConfig outgoing) {
        }

        @Override
        public void updateIncomingConfig(@Nonnull ApplicationLink applink, @Nonnull OAuthConfig incoming)
                throws ConsumerInformationUnavailableException {
        }
    }
}
