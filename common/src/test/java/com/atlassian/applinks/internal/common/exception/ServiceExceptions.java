package com.atlassian.applinks.internal.common.exception;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.Set;
import javax.annotation.Nonnull;

import static com.google.common.collect.Sets.filter;

public final class ServiceExceptions {
    private ServiceExceptions() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Set<Class<? extends ServiceException>> allServiceExceptionClasses() {
        Reflections reflections = new Reflections(Package.getPackage("com.atlassian.applinks.internal"));

        Set<Class<? extends ServiceException>> all = reflections.getSubTypesOf(ServiceException.class);
        Set<Class<? extends MockServiceException>> mocks = ImmutableSet.<Class<? extends MockServiceException>>builder()
                .add(MockServiceException.class)
                .addAll(reflections.getSubTypesOf(MockServiceException.class))
                .build();

        return Sets.difference(all, mocks);
    }

    @Nonnull
    public static Set<Class<? extends ServiceException>> concreteServiceExceptionClasses() {
        return ImmutableSet.copyOf(filter(allServiceExceptionClasses(), new Predicate<Class<? extends ServiceException>>() {
            @Override
            public boolean apply(Class<? extends ServiceException> clazz) {
                return !Modifier.isAbstract(clazz.getModifiers());
            }
        }));
    }
}
