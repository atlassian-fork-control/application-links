package com.atlassian.applinks.internal.common.exception;

import com.atlassian.applinks.test.mock.MockI18nResolver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.Properties;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultServiceExceptionFactoryTest {
    private ServiceExceptionFactory serviceExceptionFactory = new DefaultServiceExceptionFactory(new MockI18nResolver());

    @Test
    public void testCreateWithDefaultMessage() {
        ServiceExceptionWithDefaultMessage exception = serviceExceptionFactory.create(
                ServiceExceptionWithDefaultMessage.class);

        assertEquals(exception.getMessage(), ServiceExceptionWithDefaultMessage.DEFAULT_MESSAGE);
    }

    @Test
    public void testCreateWithoutDefaultMessage() {
        ServiceExceptionWithoutDefaultMessage exception = serviceExceptionFactory.create(
                ServiceExceptionWithoutDefaultMessage.class);

        assertEquals(exception.getMessage(), "applinks.service.error.default.message.not.specified");
    }

    @Test
    public void testAllServiceExceptionsHaveMatchingI18n() throws Exception {
        testAllServiceExceptionsHaveMatchingI18n(
                "/com/atlassian/applinks/internal/common/applinks-service-common.properties");
    }

    public static void testAllServiceExceptionsHaveMatchingI18n(String... i18nResources) throws Exception {
        Properties serviceI18n = new Properties();
        for (String resource : i18nResources) {
            serviceI18n.load(DefaultServiceExceptionFactoryTest.class.getResourceAsStream(resource));
        }

        for (Class<? extends ServiceException> serviceException : ServiceExceptions.allServiceExceptionClasses()) {
            try {
                Field defaultMessage = serviceException.getField(DefaultServiceExceptionFactory.DEFAULT_MESSAGE_FIELD_NAME);
                Object key = defaultMessage.get(null);
                assertTrue(format("Service I18n file does not contain key '%s' from '%s'", key, serviceException.getName()),
                        serviceI18n.containsKey(key));
            } catch (NoSuchFieldException e) {
                // proceed, field not defined
            }
        }
    }

    @SuppressWarnings("unused")
    public static final class ServiceExceptionWithDefaultMessage extends MockServiceException {
        public static final String DEFAULT_MESSAGE = "my.default.message";

        public ServiceExceptionWithDefaultMessage(String message) {
            super(message);
        }

        public ServiceExceptionWithDefaultMessage(String message, Throwable cause) {
            super(message, cause);
        }
    }

    @SuppressWarnings("unused")
    public static final class ServiceExceptionWithoutDefaultMessage extends MockServiceException {
        public ServiceExceptionWithoutDefaultMessage(String message) {
            super(message);
        }

        public ServiceExceptionWithoutDefaultMessage(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
