package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for the common application link request object.  This will not be exhaustive
 * as the inter-dependencies are complex and exercised in an integration style by the applinks-tests project.
 */
public class ApplicationLinkRequestTest {
    private String url = "http://www.foo.com";
    @Mock
    private ApplicationLinkRequest wrappedRequest;
    @Mock
    private ApplicationId applicationId;

    @Before
    public void createMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void willSetFollowRedirectsFalseOnCreate() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);

        createRequest();

        verify(wrappedRequest).setFollowRedirects(false);
    }


    @Test
    public void willSetConnectionTimeout() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.setConnectionTimeout(100)).thenReturn(wrappedRequest);

        ApplicationLinkRequest request = createRequest();
        request.setConnectionTimeout(100);

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).setConnectionTimeout(100);
    }

    @Test
    public void willSetSoTimeout() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.setSoTimeout(100)).thenReturn(wrappedRequest);

        ApplicationLinkRequest request = createRequest();
        request.setSoTimeout(100);

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).setSoTimeout(100);
    }

    @Test
    public void willSetUrl() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.setUrl(url)).thenReturn(wrappedRequest);

        ApplicationLinkRequest request = createRequest();
        request.setUrl(url);

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).setUrl(url);
    }

    @Test
    public void willSetRequestBody() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.setRequestBody("The request body")).thenReturn(wrappedRequest);

        ApplicationLinkRequest request = createRequest();
        request.setRequestBody("The request body");

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).setRequestBody("The request body");
    }

    @Test
    public void willSetFiles() {
        List<RequestFilePart> parts = new ArrayList<RequestFilePart>();
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.setFiles(parts)).thenReturn(wrappedRequest);

        ApplicationLinkRequest request = createRequest();
        request.setFiles(parts);

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).setFiles(parts);
    }

    @Test
    public void willSetEntity() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.setEntity("The request entity")).thenReturn(wrappedRequest);

        ApplicationLinkRequest request = createRequest();
        request.setEntity("The request entity");

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).setEntity("The request entity");
    }

    @Test
    public void willAddListOfRequestParameters() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.addRequestParameters("key1", "value1")).thenReturn(wrappedRequest);

        ApplicationLinkRequest request = createRequest();
        request.addRequestParameters("key1", "value1");

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).addRequestParameters("key1", "value1");
    }

    @Test
    public void willAddHeader() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.addHeader("key", "value")).thenReturn(wrappedRequest);

        ApplicationLinkRequest request = createRequest();
        request.addHeader("key", "value");

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).addHeader("key", "value");
    }

    @Test
    public void willSetHeader() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.setHeader("key", "value")).thenReturn(wrappedRequest);

        ApplicationLinkRequest request = createRequest();
        request.setHeader("key", "value");

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).setHeader("key", "value");
    }

    @Test
    public void willGetHeaders() {
        Map<String, List<String>> headers = new HashMap<String, List<String>>();
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.getHeaders()).thenReturn(headers);

        ApplicationLinkRequest request = createRequest();
        request.getHeaders();

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest).getHeaders();
    }

    @Test
    public void willSetFollowRedirects() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);

        AbstractApplicationLinkRequest request = createRequest();
        request.setFollowRedirects(true);
        assertTrue(request.getFollowRedirects());
        verify(wrappedRequest, never()).setFollowRedirects(true);
    }

    private AbstractApplicationLinkRequest createRequest() {
        return new AbstractApplicationLinkRequest(url, wrappedRequest) {
            @Override
            protected void signRequest() throws ResponseException {

            }

            @Override
            public <R> R execute(ApplicationLinkResponseHandler<R> responseHandler) throws ResponseException {
                return null;
            }

            @Override
            public void execute(ResponseHandler<? super Response> responseHandler) throws ResponseException {

            }

            @Override
            public <RET> RET executeAndReturn(ReturningResponseHandler<? super Response, RET> responseHandler) throws ResponseException {
                return null;
            }
        };
    }
}
