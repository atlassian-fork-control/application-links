package com.atlassian.applinks.internal.common.lang;

import com.google.common.base.Function;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 5.0
 */
public final class ApplinksEnums {
    private ApplinksEnums() {
    }

    // NOTE: use Guava's Enums.stringConverter instead once we're on Platform3 in every product

    @Nonnull
    public static <E extends Enum<E>> Function<String, E> fromName(@Nonnull final Class<E> enumType) {
        return new Function<String, E>() {
            @Nullable
            @Override
            public E apply(String stringValue) {
                return Enum.valueOf(enumType, stringValue);
            }
        };
    }

    /**
     * Similar to {@link #fromName(Class)}, but will not throw an exception if the input is an unrecognized enum value
     * (including {@code null}, instead the function will return {@code null}.
     *
     * @param enumType enum type to convert
     * @param <E>      enum type parameter
     * @return function that will convert string names into instances of {@code E} and return {@code null} if the
     * conversion fails
     */
    @Nonnull
    public static <E extends Enum<E>> Function<String, E> fromNameSafe(@Nonnull final Class<E> enumType) {
        final Function<String, E> fromName = fromName(enumType);

        return new Function<String, E>() {
            @Nullable
            @Override
            public E apply(@Nullable String stringValue) {
                try {
                    return fromName.apply(stringValue);
                } catch (NullPointerException | IllegalArgumentException e) {
                    return null;
                }
            }
        };
    }

    @Nonnull
    public static Function<Enum<?>, String> toName() {
        return new Function<Enum<?>, String>() {
            @Nullable
            @Override
            public String apply(Enum<?> anEnum) {
                return anEnum.name();
            }
        };
    }
}
