package com.atlassian.applinks.internal.common.json;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.JsonableMarshaller;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import javax.annotation.Nullable;

/**
 * Uses Jackson to adapt JSON entities into {@link Jsonable}. Clients must ensure that entities are already convertible
 * to JSON via Jackson.
 *
 * @since 4.3
 */
public class JacksonJsonableMarshaller implements JsonableMarshaller {
    public static final JsonableMarshaller INSTANCE = new JacksonJsonableMarshaller();

    private static final ObjectWriter OBJECT_WRITER = createObjectWriter();

    private static ObjectWriter createObjectWriter() {
        // create writer that does not close the streams/writers it operates on
        return new ObjectMapper().configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false).writer();
    }

    @Override
    public Jsonable marshal(@Nullable final Object toJsonObj) {
        return writer -> OBJECT_WRITER.writeValue(writer, toJsonObj);
    }
}
