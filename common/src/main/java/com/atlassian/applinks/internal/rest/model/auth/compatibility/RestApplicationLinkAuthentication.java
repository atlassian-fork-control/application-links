package com.atlassian.applinks.internal.rest.model.auth.compatibility;

import com.atlassian.applinks.internal.common.rest.model.oauth.RestConsumer;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;

import static com.google.common.base.Predicates.notNull;
import static java.util.Collections.emptyList;

/**
 * @since 5.0
 */
public class RestApplicationLinkAuthentication extends BaseRestEntity {
    public static final String CONSUMERS = "consumers";
    public static final String CONFIGURED_AUTHENTICATION_PROVIDERS = "configuredAuthProviders";

    @SuppressWarnings("unused") // for Jackson
    public RestApplicationLinkAuthentication() {
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public Iterable<RestConsumer> getConsumers() {
        if (containsKey(CONSUMERS)) {
            Iterable consumers = (Iterable) get(CONSUMERS);
            Iterable filtered = Iterables.filter(consumers, notNull());
            return ImmutableList.copyOf(Iterables.transform(filtered, RestConsumer.REST_TRANSFORM));
        } else {
            return emptyList();
        }
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public Iterable<RestAuthenticationProvider> getConfiguredAuthenticationProviders() {
        if (containsKey(CONFIGURED_AUTHENTICATION_PROVIDERS)) {
            Iterable authProviders = (Iterable) get(CONFIGURED_AUTHENTICATION_PROVIDERS);
            Iterable filtered = Iterables.filter(authProviders, notNull());
            return ImmutableList.copyOf(Iterables.transform(filtered, RestAuthenticationProvider.REST_TRANSFORM));
        } else {
            return emptyList();
        }
    }
}
