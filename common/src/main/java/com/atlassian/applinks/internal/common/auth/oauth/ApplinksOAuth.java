package com.atlassian.applinks.internal.common.auth.oauth;

import com.atlassian.annotations.Internal;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;

import static java.lang.String.valueOf;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 * Internal constants and utilities used by Applinks components supporting OAuth.
 *
 * @since 4.3
 */
@Internal
public final class ApplinksOAuth {
    private ApplinksOAuth() {
    }

    // NOTE: those constants are used throughout multiple separate Applinks JARs, hence all are wrapped in
    // {@code String.valueOf} to prevent inlining

    /**
     * WWW-Authenticate response header containing the Auth challenge
     */
    public static final String WWW_AUTHENTICATE = valueOf("WWW-Authenticate");

    /**
     * OAuth string, can be used to verify whether the WWW-Authenticate challenge was issued by an OAuth
     * authentication engine.
     */
    public static final String OAUTH = valueOf("OAuth");

    /**
     * AUTH_CONFIG_ keys are used to store OAuth Applink authentication configuration
     * (see AuthenticationConfigurationManager)
     */
    public static final String AUTH_CONFIG_CONSUMER_KEY_OUTBOUND = valueOf("consumerKey.outbound");

    /**
     * PROPERTY_ keys are used to store OAuth configuration against Applinks properties (see PropertySet)
     */
    public static final String PROPERTY_INCOMING_CONSUMER_KEY = valueOf("oauth.incoming.consumerkey");

    public static final String SERVICE_PROVIDER_REQUEST_TOKEN_URL = valueOf("serviceProvider.requestTokenUrl");
    public static final String SERVICE_PROVIDER_ACCESS_TOKEN_URL = valueOf("serviceProvider.accessTokenUrl");
    public static final String SERVICE_PROVIDER_AUTHORIZE_URL = valueOf("serviceProvider.authorizeUrl");

    // oauth_problem parameter
    public static final String OAUTH_PROBLEM = valueOf("oauth_problem");
    // common OAuth Problems
    public static final String PROBLEM_CONSUMER_KEY_UNKNOWN = valueOf("consumer_key_unknown");
    public static final String PROBLEM_TIMESTAMP_REFUSED = valueOf("timestamp_refused");
    public static final String PROBLEM_SIGNATURE_INVALID = valueOf("signature_invalid");

    /**
     * @param response response to examine
     * @return {@code true}, if the {@code response} contains OAuth authentication challenge
     */
    public static boolean hasOAuthChallenge(@Nonnull Response response) {
        return response.getHeaders().containsKey(WWW_AUTHENTICATE) &&
                response.getHeader(WWW_AUTHENTICATE).trim().startsWith(OAUTH);
    }

    /**
     * Examine whether the response indicates that the OAuth level at which the request was made  (2LO or 2LOi) is
     * disabled in the remote instance. In such case the remote application (provided it runs Atlassian OAuth) will
     * respond with an empty 401 containing OAuth auth challenge.
     *
     * @param response response to examine
     * @return {@code true}, if the {@code response} indicates that the OAuth level at which the request was made
     * (2LO or 2LOi) is disabled in the remote instance
     */
    public static boolean isAuthLevelDisabled(@Nonnull Response response) throws ResponseException {
        return UNAUTHORIZED.getStatusCode() == response.getStatusCode() &&
                hasOAuthChallenge(response) &&
                StringUtils.isEmpty(response.getResponseBodyAsString());
    }
}
