package com.atlassian.applinks.internal.common.cache;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Request-local cache, useful to store results of expensive operations (e.g. DB, network) against the ongoing request.
 * Note: this cache will only be effective on web request threads, otherwise it (gracefully) degrades to an
 * always-cache-miss behaviour.
 *
 * @since 5.0
 */
public interface ApplinksRequestCache {
    /**
     * Get cache identified with {@code cacheName}. Note: if there is no request context, this will be a no-op cache.
     *
     * @param cacheName name of the cache, should be unique to the specific use of the cache to prevent conflicts
     * @param keyType   key type, needs to conform to proper {@code equals} and {@code hashCode} contracts, otherwise the
     *                  caching behaviour is not guaranteed
     * @param valueType value type
     * @param <K>       key type parameter
     * @param <V>       value type parameter
     * @return an instance of cache.
     */
    @Nonnull
    <K, V> Cache<K, V> getCache(@Nonnull String cacheName, @Nonnull Class<K> keyType, @Nonnull Class<V> valueType);

    interface Cache<K, V> {
        /**
         * Store {@code value} in the cache against {@code key}.
         *
         * @param key   cache key
         * @param value value to store
         */
        void put(@Nonnull K key, @Nonnull V value);

        /**
         * @param key key to get value for`
         * @return cached value, or {@code null} if the value was not found in cache
         */
        @Nullable
        V get(@Nonnull K key);
    }


}
