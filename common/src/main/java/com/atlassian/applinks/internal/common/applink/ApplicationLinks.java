package com.atlassian.applinks.internal.common.applink;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.internal.common.application.ApplicationTypes;

import javax.annotation.Nonnull;
import java.net.URI;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public final class ApplicationLinks {
    private ApplicationLinks() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Predicate<ReadOnlyApplicationLink> isSystemLink() {
        return link -> link != null && link.isSystem();
    }

    @Nonnull
    public static Predicate<ReadOnlyApplicationLink> isAtlassianLink() {
        return link -> link != null && ApplicationTypes.isAtlassian(link.getType());
    }

    @Nonnull
    public static Predicate<ReadOnlyApplicationLink> withId(@Nonnull final ApplicationId applicationId) {
        requireNonNull(applicationId, "applicationId");
        return link -> link != null && applicationId.equals(link.getId());
    }

    @Nonnull
    public static Predicate<ReadOnlyApplicationLink> withName(@Nonnull final String name) {
        requireNonNull(name, "name");
        return link -> link != null && name.equals(link.getName());
    }

    @Nonnull
    public static Predicate<ReadOnlyApplicationLink> withRpcUrl(@Nonnull final URI rpcUrl) {
        requireNonNull(rpcUrl, "rpcUrl");
        return link -> link != null && rpcUrl.equals(link.getRpcUrl());
    }

    @Nonnull
    public static Predicate<ReadOnlyApplicationLink> withDisplayUrl(@Nonnull final URI displayUrl) {
        requireNonNull(displayUrl, "displayUrl");
        return link -> link != null && displayUrl.equals(link.getDisplayUrl());
    }
}
