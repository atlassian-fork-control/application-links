package com.atlassian.applinks.internal.common.web;

import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.status.error.ApplinkErrors;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserRole;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.Map;

import static com.atlassian.applinks.internal.common.application.ApplicationTypes.resolveApplicationTypeId;
import static com.atlassian.applinks.internal.common.net.ResponseHeaderUtil.preventCrossFrameClickJacking;
import static com.atlassian.applinks.internal.common.net.ServiceExceptionHttpMapper.getStatus;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.MediaType.TEXT_HTML;

/**
 * Abstract Applinks servlet that provides support for rendering the response using Soy, as well as handles all
 * {@link ServiceException service exceptions} by mapping the status and rendering the error message in a consistent
 * way. NOTE: this servlet does not provide XSRF protection, so it should generally only be used for non-mutating
 * {@code GET} requests unless the extending class will take care of XSRF protection itself.
 *
 * @since 5.1
 */
public abstract class AbstractApplinksServiceServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(AbstractApplinksServiceServlet.class);

    protected static final String DEFAULT_CONTENT_TYPE = ContentType.create(TEXT_HTML, UTF_8).toString();
    protected static final String MODULE_PAGE_COMMON = "page-common";

    // injected data params that will be available in the templates rendered using this servlet as "$ij.<paramname>"
    protected static final String IJ_PARAM_APPLICATION_TYPE = "applicationType";

    // common page params
    protected static final String PARAM_DECORATOR = "decorator";
    protected static final String PARAM_TITLE = "title";
    protected static final String PARAM_ACTIVE_TAB = "activeTab";
    protected static final String PARAM_PAGE_INITIALIZER = "pageInitializer";
    protected static final String PARAM_ERROR_MESSAGE = "errorMessage";

    protected final AppLinkPluginUtil appLinkPluginUtil;
    protected final I18nResolver i18nResolver;
    protected final InternalHostApplication internalHostApplication;
    protected final LoginUriProvider loginProvider;
    protected final SoyTemplateRenderer soyTemplateRenderer;
    protected final PageBuilderService pageBuilderService;

    protected AbstractApplinksServiceServlet(AppLinkPluginUtil appLinkPluginUtil,
                                             I18nResolver i18nResolver,
                                             InternalHostApplication internalHostApplication,
                                             LoginUriProvider loginProvider,
                                             SoyTemplateRenderer soyTemplateRenderer,
                                             PageBuilderService pageBuilderService) {
        this.appLinkPluginUtil = appLinkPluginUtil;
        this.i18nResolver = i18nResolver;
        this.internalHostApplication = internalHostApplication;
        this.loginProvider = loginProvider;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.pageBuilderService = pageBuilderService;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            preventCrossFrameClickJacking(response);
            doServiceGet(request, response);
        } catch (Exception e) {
            ServiceException serviceException = ApplinkErrors.findCauseOfType(e, ServiceException.class);
            if (serviceException == null || response.isCommitted()) {
                // unexpected error, or too late to change the response
                Throwables.propagateIfInstanceOf(e, ServletException.class);
                Throwables.propagateIfInstanceOf(e, IOException.class);
                Throwables.propagateIfInstanceOf(e, RuntimeException.class);
                throw new ServletException(e);
            } else {
                log.debug("Service exception while processing request to {}: {}", request.getRequestURI(),
                        serviceException);
                preventCrossFrameClickJacking(response);
                Status status = getStatus(serviceException);
                if (status == Status.UNAUTHORIZED) {
                    response.sendRedirect(getLoginRedirectUrl(request));
                } else {
                    response.setStatus(status.getStatusCode());
                    renderError(status, request, response, serviceException);
                }
            }
        }
    }

    protected final void render(@Nonnull HttpServletRequest request,
                                @Nonnull HttpServletResponse response,
                                @Nonnull String moduleKey,
                                @Nonnull String templateName,
                                @Nonnull Map<String, Object> data) throws IOException, ServletException {
        renderInternal(response, moduleKey, templateName, createData(request, data));
    }

    protected abstract void doServiceGet(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response)
            throws IOException, ServiceException, ServletException;

    @Nonnull
    protected abstract Iterable<String> webResourceContexts();

    @Nullable
    protected String getDecorator(@Nonnull HttpServletRequest request) {
        return null;
    }

    @Nullable
    protected String getPageTitle(@Nonnull HttpServletRequest request) {
        return null;
    }

    /**
     * @param request current request
     * @return active tab to highlight, only for admin pages
     */
    @Nullable
    protected String getActiveTab(@Nonnull HttpServletRequest request) {
        return null;
    }

    @Nullable
    protected String getPageInitializer(@Nonnull HttpServletRequest request) {
        return null;
    }

    private void renderInternal(@Nonnull HttpServletResponse response,
                                @Nonnull String moduleKey,
                                @Nonnull String templateName,
                                @Nonnull Map<String, Object> data) throws IOException, ServletException {
        requireNonNull(response, "response");
        requireNonNull(moduleKey, "moduleKey");
        requireNonNull(templateName, "templateName");
        requireNonNull(data, "data");

        requireResources();
        response.setContentType(DEFAULT_CONTENT_TYPE);
        try {
            soyTemplateRenderer.render(response.getWriter(), appLinkPluginUtil.completeModuleKey(moduleKey),
                    templateName, data, createInjectedData());
        } catch (SoyException e) {
            throw new ServletException(e);
        }
    }

    // skip some subclass params when rendering "common" error page (e.g. AMD initializer)
    // also provide a generic error header
    private Map<String, Object> createErrorData(Status status, String errorMessage) {
        Map<String, Object> data = Maps.newHashMap();

        data.put(PARAM_TITLE, getErrorPageTitle(status));
        data.put(PARAM_ERROR_MESSAGE, errorMessage);

        return data;
    }

    private Map<String, Object> createData(HttpServletRequest request, Map<String, Object> mainData) {
        Map<String, Object> data = Maps.newHashMap();

        data.put(PARAM_ACTIVE_TAB, getActiveTab(request));
        data.put(PARAM_DECORATOR, getDecorator(request));
        data.put(PARAM_TITLE, getPageTitle(request));
        data.put(PARAM_PAGE_INITIALIZER, getPageInitializer(request));
        data.putAll(mainData);

        return data;
    }

    /**
     * @return default injected data for the rendered Soy templates, containing common data that is not worth passing
     * around to each template
     */
    protected final Map<String, Object> createInjectedData() {
        return Collections.singletonMap(
                IJ_PARAM_APPLICATION_TYPE, resolveApplicationTypeId(internalHostApplication.getType())
        );
    }

    private void requireResources() {
        pageBuilderService.assembler().resources()
                .requireWebResource(appLinkPluginUtil.completeModuleKey(MODULE_PAGE_COMMON));
        for (String context : webResourceContexts()) {
            pageBuilderService.assembler().resources().requireContext(context);
        }
    }

    private void renderError(Status status, HttpServletRequest request, HttpServletResponse response,
                             ServiceException serviceException) throws IOException, ServletException {
        renderInternal(response, MODULE_PAGE_COMMON, "applinks.page.common.error",
                createErrorData(status, serviceException.getLocalizedMessage()));
    }

    private String getErrorPageTitle(Status status) {
        return i18nResolver.getText("applinks.common.error.title", status.getStatusCode());
    }

    private String getLoginRedirectUrl(HttpServletRequest request) {
        final StringBuffer callback = request.getRequestURL();
        if (!StringUtils.isBlank(request.getQueryString())) {
            callback.append("?").append(request.getQueryString());
        }
        return loginProvider.getLoginUriForRole(URI.create(callback.toString()), UserRole.ADMIN).toASCIIString();
    }
}
