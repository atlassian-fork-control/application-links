package com.atlassian.applinks.internal.rest.model.status;

import com.atlassian.applinks.internal.common.rest.model.status.RestOAuthConfig;
import com.atlassian.applinks.internal.rest.model.IllegalRestRepresentationStateException;
import com.atlassian.applinks.internal.rest.model.RestRepresentation;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nonnull;

/**
 * REST representation of {@link ApplinkOAuthStatus}
 *
 * @since 4.3
 */
@JsonSerialize
public class RestApplinkOAuthStatus implements RestRepresentation<ApplinkOAuthStatus> {
    public static final String INCOMING = "incoming";
    public static final String OUTGOING = "outgoing";

    public RestOAuthConfig incoming;
    public RestOAuthConfig outgoing;

    @SuppressWarnings("unused") // for Jackson
    public RestApplinkOAuthStatus() {
    }
    
    public RestApplinkOAuthStatus(@Nonnull final ApplinkOAuthStatus status) {
        this.incoming = new RestOAuthConfig(status.getIncoming());
        this.outgoing = new RestOAuthConfig(status.getOutgoing());
    }


    /**
     * This method is used just for tests, they're not designed for efficiency
     * @param o object to compare with
     * @return true if equal
     */
    @Override
    public boolean equals(Object o) {
        return this == o || o instanceof RestApplinkOAuthStatus &&
                this.asDomain().equals(((RestApplinkOAuthStatus) o).asDomain());
    }

    @Override
    public int hashCode(){
        return this.asDomain().hashCode();
    }

    @Nonnull
    @Override
    public ApplinkOAuthStatus asDomain() {
        if (incoming == null) {
            throw new IllegalRestRepresentationStateException(INCOMING);
        }
        if (outgoing == null) {
            throw new IllegalRestRepresentationStateException(OUTGOING);
        }
        return new ApplinkOAuthStatus(incoming.asDomain(), outgoing.asDomain());
    }
}
