package com.atlassian.applinks.internal.authentication;

import com.atlassian.applinks.api.AuthorisationURIGenerator;

import javax.annotation.Nonnull;

/**
 * Entity that provides access to its {@link AuthorisationURIGenerator}.
 *
 * @since 4.3
 */
public interface AuthorisationUriAware {
    @Nonnull
    AuthorisationURIGenerator getAuthorisationUriGenerator();
}
