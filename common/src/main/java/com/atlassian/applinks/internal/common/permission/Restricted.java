package com.atlassian.applinks.internal.common.permission;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.annotation.Nonnull;

/**
 * Applies to service API (types or methods) to indicate that certain service or service method has restricted access at
 * the specified {@code level}. All services and other business components should have their permission level documented
 * using this annotation - or {@code Unrestricted}).
 *
 * @see PermissionLevel
 * @see Unrestricted
 * @since 4.3
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Restricted {
    /**
     * Permission levels enforced by this API. Most commonly it will be a single level for the entire
     * class/method, but some APIs can also enforce multiple levels depending on the input or state of the system,
     * which should be documented in the {@code reason} field or API documentation.
     *
     * @return permission level(s) enforced by this service API
     */
    @Nonnull PermissionLevel[] value();

    /**
     * @return optional reason for the restriction
     */
    @Nonnull String reason() default "";
}