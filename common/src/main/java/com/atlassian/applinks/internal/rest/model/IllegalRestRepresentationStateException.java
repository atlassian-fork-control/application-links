package com.atlassian.applinks.internal.rest.model;

import javax.annotation.Nullable;

/**
 * Thrown if a {@link RestRepresentation} is in a state that does not allow it to construct a
 * {@link RestRepresentation#asDomain() domain object} out of it.
 *
 * @since 5.0
 */
public class IllegalRestRepresentationStateException extends IllegalStateException {
    private final String context;

    public IllegalRestRepresentationStateException(@Nullable String context) {
        super(String.format("Required value '%s' not present", context));
        this.context = context;
    }

    public IllegalRestRepresentationStateException(@Nullable String context, @Nullable String message) {
        super(message);
        this.context = context;
    }

    public IllegalRestRepresentationStateException(@Nullable String context, @Nullable String message,
                                                   @Nullable Throwable cause) {
        super(message, cause);
        this.context = context;
    }

    @Nullable
    public String getContext() {
        return context;
    }
}
