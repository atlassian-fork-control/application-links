package com.atlassian.applinks.internal.common.web.data;

import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.internal.common.json.JacksonJsonableMarshaller;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import static com.atlassian.applinks.internal.common.application.ApplicationTypes.resolveApplicationTypeId;

/**
 * Injects application types i18n into the page for client-side usage.
 *
 * @see <a href="https://developer.atlassian.com/docs/advanced-topics/adding-data-providers-to-your-plugin">
 * Data provider documentation</a>
 * @since 5.2
 */
public class ApplicationTypesDataProvider implements WebResourceDataProvider {
    private final I18nResolver i18nResolver;
    private final TypeAccessor typeAccessor;

    public ApplicationTypesDataProvider(I18nResolver i18nResolver, TypeAccessor typeAccessor) {
        this.i18nResolver = i18nResolver;
        this.typeAccessor = typeAccessor;
    }

    @Override
    public Jsonable get() {
        return JacksonJsonableMarshaller.INSTANCE.marshal(getAllTypes());
    }

    private BaseRestEntity getAllTypes() {
        BaseRestEntity.Builder allTypes = new BaseRestEntity.Builder();
        for (final ApplicationType applicationType : typeAccessor.getEnabledApplicationTypes()) {
            allTypes.add(resolveApplicationTypeId(applicationType), i18nResolver.getText(applicationType.getI18nKey()));
        }
        return allTypes.build();
    }
}
