package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Raised when an error occurs while attempting to modify entity data (create or update).
 *
 * @since 4.3
 */
public abstract class EntityModificationException extends ServiceException {
    protected EntityModificationException(@Nullable String message) {
        super(message);
    }

    protected EntityModificationException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
