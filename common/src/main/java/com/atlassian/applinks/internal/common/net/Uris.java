package com.atlassian.applinks.internal.common.net;

import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.strip;
import static org.apache.commons.lang3.StringUtils.stripEnd;

/**
 * Utilities for manipulating {@link URI} objects.
 *
 * @since 5.0
 */
public final class Uris {
    private static final Pattern REDUNDANT_SLASHES = Pattern.compile("//+");

    private Uris() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static String concatenate(@Nonnull String base, @Nonnull String... paths) {
        requireNonNull(base, "base");
        requireNonNull(paths, "paths");
        return stripEnd(base, "/") + removeRedundantSlashes("/" + StringUtils.join(paths, "/"));
    }

    @Nonnull
    public static URI concatenate(@Nonnull URI base, @Nonnull String... paths) throws URISyntaxException {
        requireNonNull(base, "base");
        return new URI(concatenate(base.toASCIIString(), paths));
    }

    /**
     * A variant of {@link #concatenate(String, String...)} that expects the resulting URI to be a valid URI, and
     * throws {@code IllegalArgumentException} otherwise. NOTE: this should only be used if the caller is
     * <strong>certain</strong> that the base and path will form a valid URI when concatenated.
     *
     * @param base       base URI
     * @param components components to add
     * @return concatenated URI
     */
    public static URI uncheckedConcatenate(final URI base, final String... components) throws IllegalArgumentException {
        try {
            return concatenate(base, components);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(format("Failed to concatenate %s to form URI (%s)", base, e.getReason()),
                    e);
        }
    }

    /**
     * Reduces sequences of more than one consecutive forward slash ("/") to a single slash.
     *
     * @param path any string, including {@code null} (e.g. {@code "foo//bar"})
     * @return the input string, with all sequences of more than one consecutive slash removed
     * (e.g. {@code "foo/bar"}), or {@code null} if input was {@code null}
     */
    @Nullable
    public static String removeRedundantSlashes(@Nullable String path) {
        return path == null ? null : REDUNDANT_SLASHES.matcher(path).replaceAll("/");
    }

    /**
     * Split an URI into path components.
     *
     * @param uri any string, including {@code null} (e.g. {@code "foo//bar"})
     * @return a list of path components contained in {@code uri} (separated by "/"), may be empty if the input was
     * {@code null} or empty
     */
    @Nonnull
    public static Iterable<String> toComponents(@Nullable String uri) {
        String processed = strip(removeRedundantSlashes(uri), "/");
        if (StringUtils.isEmpty(processed)) {
            return Collections.emptyList();
        } else {
            return Collections.unmodifiableList(Arrays.asList(processed.split("/")));
        }
    }

    /**
     * URL encode {@code string} using {@code UTF-8}.
     *
     * @param string string to encode
     */
    @Nonnull
    public static String utf8Encode(@Nonnull String string) {
        requireNonNull(string, "string");
        try {
            return URLEncoder.encode(string, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("UTF-8 not installed", e);
        }
    }

    /**
     * URL encode {@code uri} using {@code UTF-8}.
     *
     * @param uri URI to encode
     */
    @Nonnull
    public static String utf8Encode(@Nonnull URI uri) {
        return utf8Encode(uri.toASCIIString());
    }
}
