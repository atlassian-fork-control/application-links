package com.atlassian.applinks.internal.common.auth.oauth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.core.ServletPathConstants;
import com.atlassian.applinks.core.v1.rest.ApplicationLinkResource;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.exception.ConsumerInformationUnavailableException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.rest.model.oauth.RestConsumer;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.RestUrlBuilder;
import com.atlassian.applinks.internal.rest.RestVersion;
import com.atlassian.applinks.internal.rest.model.auth.compatibility.RestAuthenticationProvider;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDisabledConfig;
import static com.atlassian.applinks.ui.XsrfProtectedServlet.OVERRIDE_HEADER_NAME;
import static com.atlassian.applinks.ui.XsrfProtectedServlet.OVERRIDE_HEADER_VALUE;

/**
 * @since 5.1
 */
public class DefaultOAuthAutoConfigurator implements OAuthAutoConfigurator {
    private static final Logger log = LoggerFactory.getLogger(DefaultOAuthAutoConfigurator.class);

    private static final String APPLINKS_OAUTH_REST_MODULE = "applinks-oauth";

    private final InternalHostApplication internalHostApplication;
    private final RemoteCapabilitiesService capabilitiesService;

    private final OAuthConfigurator oAuthConfigurator;

    @Autowired
    public DefaultOAuthAutoConfigurator(AuthenticationConfigurationManager authenticationConfigurationManager,
                                        ConsumerTokenStoreService consumerTokenStoreService,
                                        InternalHostApplication internalHostApplication,
                                        RemoteCapabilitiesService capabilitiesService,
                                        ServiceProviderStoreService serviceProviderStoreService,
                                        ServiceExceptionFactory serviceExceptionFactory) {
        this(internalHostApplication, capabilitiesService, new OAuthConfigurator(
                authenticationConfigurationManager,
                consumerTokenStoreService,
                serviceProviderStoreService,
                serviceExceptionFactory));
    }

    @VisibleForTesting
    DefaultOAuthAutoConfigurator(InternalHostApplication internalHostApplication,
                                 RemoteCapabilitiesService capabilitiesService,
                                 OAuthConfigurator oAuthConfigurator) {
        this.internalHostApplication = internalHostApplication;
        this.capabilitiesService = capabilitiesService;
        this.oAuthConfigurator = oAuthConfigurator;
    }

    @Override
    public void enable(@Nonnull final OAuthConfig authLevel, @Nonnull final ApplicationLink applink,
                       @Nonnull final RequestFactory requestFactory) throws AuthenticationConfigurationException {
        if (!authLevel.isEnabled()) {
            // disable if auth level indicates OFF
            disable(applink, requestFactory);
        }

        RequestFactoryAdapter adapter = new RequestFactoryAdapter(requestFactory);
        enable(authLevel, authLevel, applink, adapter);
    }

    @Override
    public void enable(@Nonnull final OAuthConfig incoming, @Nonnull final OAuthConfig outgoing,
                       @Nonnull final ApplicationLink applink, @Nonnull final ApplicationLinkRequestFactory requestFactory) throws AuthenticationConfigurationException {
        enable(incoming, outgoing, applink, new RequestFactoryAdapter(requestFactory));
    }

    private void enable(@Nonnull final OAuthConfig incoming, @Nonnull final OAuthConfig outgoing,
                        @Nonnull final ApplicationLink applink, @Nonnull final RequestFactoryAdapter requestFactoryAdapter) throws AuthenticationConfigurationException {
        RemoteApplicationCapabilities capabilities = getCapabilitiesUnsecured(applink);
        if (isApplinksPre40(capabilities)) {
            log.info("Remote Applinks version {} of applink '{}' is too old (pre-4.0). Skipping OAuth auto-configuration",
                    capabilities.getApplinksVersion(), applink.getId());
            return;
        }

        if (capabilities.getCapabilities().contains(ApplinksCapabilities.STATUS_API)) {
            remoteEnableUsingStatusApi(outgoing, incoming, applink, requestFactoryAdapter);
        } else {
            remoteEnableUsingAuthenticationApi(outgoing, incoming, applink, requestFactoryAdapter, capabilities);
        }
        setLocalOAuthConfig(applink, incoming, outgoing);
    }

    @Override
    public void disable(@Nonnull final ApplicationLink applink, @Nonnull final RequestFactory requestFactory) throws AuthenticationConfigurationException {
        disableInternal(applink, new RequestFactoryAdapter(requestFactory));
    }

    private void disableInternal(@Nonnull ApplicationLink applink, @Nonnull RequestFactoryAdapter requestFactoryAdapter) throws AuthenticationConfigurationException {
        RemoteApplicationCapabilities capabilities = getCapabilitiesUnsecured(applink);
        if (capabilities.getCapabilities().contains(ApplinksCapabilities.STATUS_API)) {
            remoteDisableUsingStatusApi(applink, requestFactoryAdapter);
        } else {
            remoteDisableUsingAutoConfigurationServlet(applink, requestFactoryAdapter);
        }
        setLocalOAuthConfig(applink, createDisabledConfig(), createDisabledConfig());
    }

    private RemoteApplicationCapabilities getCapabilitiesUnsecured(ApplicationLink applink) throws AuthenticationConfigurationException {
        try {
            return capabilitiesService.getCapabilities(applink);
        } catch (Exception e) {
            // should not happen given we execute with elevated permissions
            throw new AuthenticationConfigurationException("Unexpected error when retrieving capabilities", e);
        }
    }

    private void setLocalOAuthConfig(ApplicationLink applink, OAuthConfig incoming, OAuthConfig outgoing)
            throws AuthenticationConfigurationException {
        try {
            oAuthConfigurator.updateIncomingConfig(applink, incoming);
            oAuthConfigurator.updateOutgoingConfig(applink, outgoing);
        } catch (ConsumerInformationUnavailableException e) {
            throw new AuthenticationConfigurationException(e);
        }
    }

    private void remoteEnableUsingStatusApi(OAuthConfig incoming, OAuthConfig outgoing, ApplicationLink applink,
                                            RequestFactoryAdapter requestFactoryAdapter) throws AuthenticationConfigurationException {
        try {
            setRemoteStatus(new ApplinkOAuthStatus(incoming, outgoing), applink, requestFactoryAdapter);
        } catch (ResponseException | CredentialsRequiredException e) {
            throw new AuthenticationConfigurationException(e);
        }
    }

    private void remoteEnableUsingAuthenticationApi(OAuthConfig incoming,
                                                    OAuthConfig outgoing,
                                                    ApplicationLink applink,
                                                    RequestFactoryAdapter requestFactoryAdapter,
                                                    RemoteApplicationCapabilities capabilities)
            throws AuthenticationConfigurationException {
        try {
            // enable consumer (incoming)
            if (incoming.isEnabled()) {
                createDefaultJsonRequest(requestFactoryAdapter, MethodType.PUT, getAuthenticationConsumerResourceUrl(applink, capabilities))
                        .setEntity(getRestConsumer(incoming))
                        .executeAndReturn(LoggingReturningResponseHandler.INSTANCE);
            }

            if (outgoing.isEnabled()) {
                String authenticationProviderUrl = getAuthenticationProviderResourceUrl(applink);
                // enable providers (outgoing)
                for (Class<? extends AuthenticationProvider> providerClass : getProviders(outgoing)) {
                    createDefaultJsonRequest(requestFactoryAdapter, MethodType.PUT, authenticationProviderUrl)
                            .setEntity(new RestAuthenticationProvider(providerClass))
                            .executeAndReturn(LoggingReturningResponseHandler.INSTANCE);
                }
            }
        } catch (ResponseException | CredentialsRequiredException e) {
            throw new AuthenticationConfigurationException(e);
        }
    }

    private void remoteDisableUsingStatusApi(ApplicationLink applink, RequestFactoryAdapter requestFactoryAdapter)
            throws AuthenticationConfigurationException {
        try {
            setRemoteStatus(ApplinkOAuthStatus.OFF, applink, requestFactoryAdapter);
        } catch (ResponseException | CredentialsRequiredException e) {
            throw new AuthenticationConfigurationException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private void remoteDisableUsingAutoConfigurationServlet(ApplicationLink applink, RequestFactoryAdapter requestFactoryAdapter)
            throws AuthenticationConfigurationException {
        try {
            createDefaultRequest(requestFactoryAdapter, MethodType.DELETE, getAutoConfigServletUrl(applink)).executeAndReturn(LoggingReturningResponseHandler.INSTANCE);
        } catch (ResponseException | CredentialsRequiredException e) {
            throw new AuthenticationConfigurationException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private void setRemoteStatus(ApplinkOAuthStatus status, ApplicationLink applink, RequestFactoryAdapter requestFactoryAdapter)
            throws ResponseException, CredentialsRequiredException {
        createDefaultJsonRequest(requestFactoryAdapter, MethodType.PUT, getStatusResourceUrl(applink))
                .setEntity(new RestApplinkOAuthStatus(status))
                .executeAndReturn(LoggingReturningResponseHandler.INSTANCE);
    }

    private String getStatusResourceUrl(ApplicationLink applink) {
        return new RestUrlBuilder().to(applink)
                .version(RestVersion.V3)
                .addPath("status")
                .addApplicationId(internalHostApplication.getId())
                .addPath("oauth")
                .toString();
    }

    private String getAuthenticationConsumerResourceUrl(ApplicationLink applink,
                                                        RemoteApplicationCapabilities capabilities) {
        RestUrlBuilder url = new RestUrlBuilder().to(applink);

        if (isApplinks5OrLater(capabilities)) {
            // in 5.x the authentication resource has been moved to the Applinks OAuth plugin
            url = url.module(APPLINKS_OAUTH_REST_MODULE).version(RestVersion.LATEST);
        } else {
            // if there's in 4.x the authentication resource was in the Applinks Core plugin
            // if the version is unavailable for any reason, assume we are talking to 4.x or lower and make best effort
            // to complete the set up
            url = url.version(RestVersion.V2);
        }

        return url.addPath(ApplicationLinkResource.CONTEXT)
                .addApplicationId(internalHostApplication.getId())
                .addPath("authentication")
                .addPath("consumer")
                .queryParam("autoConfigure", Boolean.TRUE.toString())
                .toString();
    }

    private String getAuthenticationProviderResourceUrl(ApplicationLink applink) {
        return new RestUrlBuilder()
                .to(applink)
                .version(RestVersion.V2)
                .addPath(ApplicationLinkResource.CONTEXT)
                .addApplicationId(internalHostApplication.getId())
                .addPath("authentication")
                .addPath("provider")
                .toString();
    }

    private String getAutoConfigServletUrl(ApplicationLink applink) {
        return RestUrl.forPath(applink.getRpcUrl().toASCIIString())
                .add(ServletPathConstants.APPLINKS_CONFIG_SERVLET_URL)
                .add("oauth")
                .add("autoconfig")
                .add(internalHostApplication.getId().toString())
                .toString();
    }

    private static Request<?, ?> createDefaultRequest(RequestFactoryAdapter requestFactoryAdapter, MethodType methodType, String url) throws CredentialsRequiredException {
        return requestFactoryAdapter.createRequest(methodType, url)
                .setFollowRedirects(true)
                .addHeader(OVERRIDE_HEADER_NAME, OVERRIDE_HEADER_VALUE);
    }


    private static Request<?, ?> createDefaultJsonRequest(RequestFactoryAdapter requestFactoryAdapter, MethodType methodType, String url) throws CredentialsRequiredException {
        return createDefaultRequest(requestFactoryAdapter, methodType, url)
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .addHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
    }

    private static boolean isApplinks5OrLater(RemoteApplicationCapabilities capabilities) {
        ApplicationVersion applinksVersion = capabilities.getApplinksVersion();
        // treat null as 5.x (consistently with NullCapabilities)
        return applinksVersion == null || applinksVersion.getMajor() >= 5;
    }

    private static boolean isApplinksPre40(RemoteApplicationCapabilities capabilities) {
        ApplicationVersion applinksVersion = capabilities.getApplinksVersion();
        return applinksVersion != null && applinksVersion.getMajor() < 4;
    }

    private static Iterable<Class<? extends AuthenticationProvider>> getProviders(OAuthConfig authLevel) {
        ImmutableList.Builder<Class<? extends AuthenticationProvider>> providers = ImmutableList.builder();
        if (authLevel.isEnabled()) {
            providers.add(OAuthAuthenticationProvider.class);
        }
        if (authLevel.isTwoLoEnabled()) {
            providers.add(TwoLeggedOAuthAuthenticationProvider.class);
        }
        if (authLevel.isTwoLoImpersonationEnabled()) {
            providers.add(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);
        }
        return providers.build();
    }

    private static RestConsumer getRestConsumer(OAuthConfig config) {
        RestConsumer consumer = new RestConsumer();
        consumer.put(RestConsumer.TWO_LO_ALLOWED, config.isTwoLoEnabled());
        consumer.put(RestConsumer.TWO_LO_IMPERSONATION_ALLOWED, config.isTwoLoImpersonationEnabled());
        return consumer;
    }

    private static final class LoggingReturningResponseHandler implements ReturningResponseHandler<Response, String> {
        static final LoggingReturningResponseHandler INSTANCE = new LoggingReturningResponseHandler();

        @Override
        public String handle(Response response) throws ResponseException {
            String body = response.getResponseBodyAsString();
            Status status = Status.fromStatusCode(response.getStatusCode());
            if (status == null || status.getFamily() != Status.Family.SUCCESSFUL) {
                log.warn("Unexpected response status: {}, body:\n\n{}", response.getStatusCode(), body);
                throw new ResponseStatusException("Unexpected response status: " + response.getStatusCode(), response);
            }
            return body;
        }
    }

    private final class RequestFactoryAdapter {
        private final Object requestFactory;

        RequestFactoryAdapter(RequestFactory<?> requestFactory) {
            this.requestFactory = requestFactory;
        }

        RequestFactoryAdapter(ApplicationLinkRequestFactory requestFactory) {
            this.requestFactory = requestFactory;
        }

        public Request<?, ?> createRequest(MethodType methodType, String url) throws CredentialsRequiredException {
            if (requestFactory instanceof RequestFactory) {
                return ((RequestFactory) requestFactory).createRequest(methodType, url);
            }
            return ((ApplicationLinkRequestFactory) requestFactory).createRequest(methodType, url);
        }

    }
}
