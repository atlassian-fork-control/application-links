package com.atlassian.applinks.internal.rest.model;

import javax.annotation.Nonnull;

/**
 * REST entity that represents a domain object of type {@code T}, for both serialization and deserialization purposes.
 * In addition to the API described in this contract, a REST entity that implements this contract is expected to provide
 * the copy constructor required by {@link ReadOnlyRestRepresentation}, as well as the following constructors:
 * <ul>
 * <li>default no-arg constructor such that the marshalling framework can deserialize the object</li>
 * <li>if required by the deserialization framework, a copy constructor that allows to "inflate" this entity from the
 * original deserialized state. E.g. for a {@link BaseRestEntity map-based entity} this would be constructor accepting
 * a {@link java.util.Map}</li>
 * </ul>
 *
 * @param <T> type of the domain object
 * @since 4.3
 */
public interface RestRepresentation<T> extends ReadOnlyRestRepresentation<T> {
    /**
     * Create an instance of {@code T} with data contained in this entity. If this object does not carry data satisfying
     * all invariants of {@code T}, {@code IllegalRestRepresentationStateException} should be raised.
     *
     * @return new domain object corresponding to this REST entity
     * @throws IllegalRestRepresentationStateException if the data carried by this entity is not sufficient to create
     *                                                 a valid instance of {@code T}
     */
    @Nonnull
    T asDomain() throws IllegalRestRepresentationStateException;
}
