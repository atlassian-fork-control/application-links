package com.atlassian.applinks.internal.common.exception;

import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;

import java.io.Serializable;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.internal.common.i18n.I18nKey.newI18nKey;
import static com.google.common.base.Preconditions.checkState;

/**
 * @since 5.1
 */
public class ValidationExceptionBuilder {
    private static final String DEFAULT_MESSAGE = "applinks.service.error.validation";

    private final I18nResolver i18nResolver;

    private Object origin;
    private String originName;
    private Throwable cause;
    private final SimpleDetailedErrors.Builder errorsBuilder = new SimpleDetailedErrors.Builder();

    public ValidationExceptionBuilder(I18nResolver i18nResolver) {
        this.i18nResolver = i18nResolver;
    }

    @Nonnull
    public ValidationExceptionBuilder origin(@Nonnull Object origin) {
        this.origin = origin;
        return this;
    }

    @Nonnull
    public ValidationExceptionBuilder originName(@Nullable String originName) {
        this.originName = originName;
        return this;
    }

    @Nonnull
    public ValidationExceptionBuilder cause(@Nullable Throwable cause) {
        this.cause = cause;
        return this;
    }

    @Nonnull
    public ValidationExceptionBuilder error(@Nullable String context, @Nonnull I18nKey summaryKey,
                                            @Nullable String details) {
        errorsBuilder.error(context, i18nResolver.getText(summaryKey), details);
        return this;
    }

    @Nonnull
    public ValidationExceptionBuilder error(@Nullable String context, @Nonnull String summaryKey, Serializable... args) {
        return error(context, newI18nKey(summaryKey, args), null);
    }

    @Nonnull
    public ValidationExceptionBuilder error(@Nonnull String summaryKey, Serializable... args) {
        return error(null, summaryKey, args);
    }

    public boolean hasErrors() {
        return errorsBuilder.hasErrors();
    }

    /**
     * @return new {@link ValidationException}
     * @throws IllegalStateException if {@code origin} was not set, or there were no errors - which can be checked using
     *                               {@link #hasErrors()}
     */
    @Nonnull
    public ValidationException build() {
        checkState(origin != null, "Origin was not set");
        checkState(hasErrors(), "There were no errors");

        DetailedErrors errors = errorsBuilder.build();
        String originName = this.originName == null ? origin.getClass().getSimpleName() : this.originName;
        String message = i18nResolver.getText(DEFAULT_MESSAGE, originName, Iterables.size(errors.getErrors()));

        return new ValidationException(origin, errors.getErrors(), message, cause);
    }
}
