package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Represents any error that is due to lack of access to specific resource or operation.
 *
 * @since 4.3
 */
public abstract class NoAccessException extends ServiceException {
    protected NoAccessException(@Nullable String message) {
        super(message);
    }

    protected NoAccessException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
