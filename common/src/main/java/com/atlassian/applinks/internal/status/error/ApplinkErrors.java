package com.atlassian.applinks.internal.status.error;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

/**
 * @since 4.3
 */
public final class ApplinkErrors {
    private ApplinkErrors() {
        throw new UnsupportedOperationException("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static String toDetails(@Nonnull Throwable error) {
        return String.format("%s: %s", error.getClass().getName(), error.getMessage());
    }

    @Nullable
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public static Throwable findCauseMatching(@Nonnull Throwable error,
                                              @Nonnull Predicate<? super Throwable> matcher) {
        requireNonNull(error, "error");
        requireNonNull(matcher, "matcher");

        Throwable cause = error;
        while (cause != null) {
            if (matcher.apply(cause)) {
                return cause;
            }
            cause = cause.getCause();
        }
        return null;
    }

    @Nullable
    public static <E extends Throwable> E findCauseOfType(@Nonnull Throwable error, @Nonnull Class<E> expected) {
        requireNonNull(expected, "expected");
        return expected.cast(findCauseMatching(error, Predicates.instanceOf(expected)));
    }
}
