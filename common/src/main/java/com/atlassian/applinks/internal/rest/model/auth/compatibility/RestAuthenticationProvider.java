package com.atlassian.applinks.internal.rest.model.auth.compatibility;

import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.google.common.base.Function;

import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 5.0
 */
public class RestAuthenticationProvider extends BaseRestEntity {
    public static final Function<Object, RestAuthenticationProvider> REST_TRANSFORM =
            new Function<Object, RestAuthenticationProvider>() {
                @Nullable
                @Override
                @SuppressWarnings("unchecked")
                public RestAuthenticationProvider apply(@Nullable Object object) {
                    if (object == null) {
                        return null;
                    }
                    if (object instanceof RestAuthenticationProvider) {
                        return (RestAuthenticationProvider) object;
                    } else if (object instanceof Map) {
                        return new RestAuthenticationProvider((Map<String, Object>) object);
                    }
                    throw new IllegalArgumentException("Cannot instantiate RestAuthenticationProvider from " + object);
                }
            };

    public static final String MODULE = "module";
    public static final String PROVIDER = "provider";
    public static final String CONFIG = "config";

    @SuppressWarnings("unused") // for Jackson
    public RestAuthenticationProvider() {
    }

    public RestAuthenticationProvider(@Nonnull Class<? extends AuthenticationProvider> clazz) {
        put(PROVIDER, clazz.getCanonicalName());
        put(CONFIG, new BaseRestEntity());
    }

    @SuppressWarnings("unused") // RestRepresentation
    public RestAuthenticationProvider(Map<String, Object> original) {
        super(original);
    }

    @Nullable
    public String getProvider() {
        return getString(PROVIDER);
    }
}
