package com.atlassian.applinks.internal.common.docs;

import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.Version;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;
import java.util.Properties;

import static com.atlassian.applinks.internal.common.net.Uris.uncheckedConcatenate;
import static java.util.Objects.requireNonNull;

public class DefaultDocumentationLinker implements DocumentationLinker {
    public static final String HELP_PATH_RESOURCE = "com/atlassian/applinks/ual-help-paths.properties";

    private final Properties helpPathProperties;
    private final URI documentationBaseUrl;

    @Autowired
    public DefaultDocumentationLinker(AppLinkPluginUtil applinkPluginUtil) {
        this(applinkPluginUtil, HELP_PATH_RESOURCE);
    }

    @VisibleForTesting
    DefaultDocumentationLinker(AppLinkPluginUtil applinkPluginUtil, String helpPathsResource) {
        this.documentationBaseUrl = createDocumentationBaseUrl(applinkPluginUtil);
        this.helpPathProperties = loadHelpPaths(helpPathsResource);
    }

    @Nonnull
    @Override
    public URI getLink(final String pageKey) {
        return getLink(pageKey, null);
    }

    @Nonnull
    @Override
    public URI getLink(final String pageKey, final String sectionKey) {
        String pageName = helpPathProperties.getProperty(pageKey);
        if (!StringUtils.isEmpty(sectionKey)) {
            final String sectionPrefix = StringUtils.remove(pageName, "+");
            pageName += "#" + sectionPrefix + "-" + sectionKey;
        }
        return uncheckedConcatenate(getDocumentationBaseUrl(), "/" + pageName);
    }

    @Override
    @Nonnull
    public URI getDocumentationBaseUrl() {
        return documentationBaseUrl;
    }

    @Nonnull
    @Override
    public Map<String, String> getAllLinkMappings() {
        return Maps.fromProperties(helpPathProperties);
    }

    private Properties loadHelpPaths(String helpPathsResource) {
        Properties props = new Properties();
        InputStream propertiesFile = this.getClass().getClassLoader().getResourceAsStream(helpPathsResource);
        requireNonNull(propertiesFile, "Could not find help paths at: " + helpPathsResource);
        try {
            props.load(propertiesFile);
        } catch (IOException e) {
            throw new RuntimeException("Could not find help paths at: " + helpPathsResource, e);
        }
        return props;
    }

    private URI createDocumentationBaseUrl(AppLinkPluginUtil applinkPluginUtil) {
        Version version = applinkPluginUtil.getVersion();

        String documentationSpaceKey = String.format("APPLINKS-%02d%d/", version.getMajor(), version.getMinor());
        return URI.create("https://confluence.atlassian.com/display/" + documentationSpaceKey);
    }
}
