package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Represents permission error whereby the caller is not authorized to perform a specific operation.
 *
 * @since 4.3
 */
public class PermissionException extends NoAccessException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.access.nopermission";

    public PermissionException(@Nullable String message) {
        super(message);
    }

    public PermissionException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
