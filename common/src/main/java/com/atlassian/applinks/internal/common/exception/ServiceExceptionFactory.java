package com.atlassian.applinks.internal.common.exception;

import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.applinks.internal.common.permission.Unrestricted;

import java.io.Serializable;
import javax.annotation.Nonnull;

/**
 * API to create and raise service exceptions for Applinks, ensuring all messages are i18n-ed.
 *
 * @since 4.3
 */
@Unrestricted("Internal component with no security implications")
public interface ServiceExceptionFactory {
    /**
     * Raise service exception of {@code exceptionClass}.
     *
     * @param exceptionClass exception class
     * @param args           extra args to interpolate with the message
     * @param <E>            type parameter of the exception
     * @return nothing will ever be returned from this method, the return signature is for convenient client usage
     * @see #create(Class, Serializable...)
     */
    @Nonnull
    <E extends ServiceException> E raise(@Nonnull Class<E> exceptionClass, Serializable... args) throws E;

    /**
     * Raise service exception using the provided {@code i18nKey}.
     *
     * @param exceptionClass exception class
     * @param i18nKey        to use when generating the error message
     * @param <E>            type parameter of the exception
     * @return nothing will ever be returned from this method, the return signature is for convenient client usage
     * @see #create(Class, I18nKey)
     */
    @Nonnull
    <E extends ServiceException> E raise(@Nonnull Class<E> exceptionClass, @Nonnull I18nKey i18nKey) throws E;

    /**
     * Raise service exception using the provided {@code i18nKey} and {@code cause}.
     *
     * @param exceptionClass exception class
     * @param i18nKey        to use when generating the error message
     * @param cause          the original cause of the exception
     * @param <E>            type parameter of the exception
     * @return nothing will ever be returned from this method, the return signature is for convenient client usage
     * @see #create(Class, I18nKey, Throwable)
     */
    @Nonnull
    <E extends ServiceException> E raise(@Nonnull Class<E> exceptionClass, @Nonnull I18nKey i18nKey,
                                         @Nonnull Throwable cause) throws E;

    /**
     * Create service exception using the default message. The exception must declare a public {@code DEFAULT_MESSAGE}
     * constant containing the i18n key of the default message for this to work.
     *
     * @param exceptionClass exception class
     * @param args           extra args to interpolate with the message
     * @param <E>            type parameter of the exception
     * @return new exception instance with the default message
     */
    @Nonnull
    <E extends ServiceException> E create(@Nonnull Class<E> exceptionClass, Serializable... args);

    /**
     * Create service exception using the provided {@code i18nKey}.
     *
     * @param exceptionClass exception class
     * @param i18nKey        to use when generating the error message
     * @param <E>            type parameter of the exception
     * @return new exception instance with the message specified by {@code i18nKey}
     */
    @Nonnull
    <E extends ServiceException> E create(@Nonnull Class<E> exceptionClass, @Nonnull I18nKey i18nKey);

    /**
     * Create service exception using the provided {@code i18nKey} and {@code cause}.
     *
     * @param exceptionClass exception class
     * @param i18nKey        to use when generating the error message
     * @param cause          the original cause of the exception
     * @param <E>            type parameter of the exception
     * @return new exception instance with message and cause
     */
    @Nonnull
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    <E extends ServiceException> E create(@Nonnull Class<E> exceptionClass, @Nonnull I18nKey i18nKey,
                                          @Nonnull Throwable cause);
}
