package com.atlassian.applinks.internal.common.auth.oauth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.applinks.internal.common.net.Uris;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.security.xml.SecureXmlParserFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;
import javax.xml.parsers.DocumentBuilder;

/**
 * @since 5.0
 */
public class ConsumerInformationHelper {
    // this endpoint is provided by the OAuth plugin
    private static final String CONSUMER_INFO_PATH = "/plugins/servlet/oauth/consumer-info";

    /**
     * Fetches consumer information from a remote applinked application, assuming it is running the OAuth plugin.
     *
     * @param applicationLink link to fetch the information from
     * @return Consumer object representing the linked application
     * @throws ResponseException if the fetch fails for any reason
     */
    @Nonnull
    public static Consumer fetchConsumerInformation(@Nonnull ApplicationLink applicationLink) throws ResponseException {
        final Request<?, ?> request = Anonymous.createAnonymousRequest(applicationLink, Request.MethodType.GET,
                Uris.uncheckedConcatenate(applicationLink.getRpcUrl(), CONSUMER_INFO_PATH).toString());
        request.setHeader("Accept", "application/xml");
        final ConsumerInformationResponseHandler handler = new ConsumerInformationResponseHandler();
        request.execute(handler);
        return handler.getConsumer();
    }

    private static class ConsumerInformationResponseHandler implements ResponseHandler<Response> {
        private Consumer consumer;

        public void handle(Response response) throws ResponseException {
            if (response.getStatusCode() != Status.OK.getStatusCode()) {
                throw new ResponseException("Server responded with an error");
            }
            final String contentTypeHeader = response.getHeader("Content-Type");
            if (contentTypeHeader != null && !contentTypeHeader.toLowerCase().startsWith("application/xml")) {
                throw new ResponseException("Server sent an invalid response");
            }
            try {
                final DocumentBuilder docBuilder = SecureXmlParserFactory.newDocumentBuilder();
                final Document doc = docBuilder.parse(response.getResponseBodyAsStream());

                final String consumerKey = doc.getElementsByTagName("key").item(0).getTextContent();
                final String name = doc.getElementsByTagName("name").item(0).getTextContent();
                final PublicKey publicKey = RSAKeys.fromPemEncodingToPublicKey(
                        doc.getElementsByTagName("publicKey").item(0).getTextContent());

                String description = null;
                if (doc.getElementsByTagName("description").getLength() > 0) {
                    description = doc.getElementsByTagName("description").item(0).getTextContent();
                }
                URI callback = null;
                if (doc.getElementsByTagName("callback").getLength() > 0) {
                    callback = new URI(doc.getElementsByTagName("callback").item(0).getTextContent());
                }

                consumer = Consumer.key(consumerKey)
                        .name(name)
                        .publicKey(publicKey)
                        .description(description)
                        .callback(callback)
                        .build();
            } catch (SAXException | IOException | DOMException e) {
                throw new ResponseException("Unable to parse consumer information", e);
            } catch (URISyntaxException e) {
                throw new ResponseException("Unable to parse consumer information, callback is not a valid URL", e);
            } catch (NoSuchAlgorithmException e) {
                throw new ResponseException("Unable to parse consumer information, no RSA providers are installed", e);
            } catch (InvalidKeySpecException e) {
                throw new ResponseException("Unable to parse consumer information, the public key is not a validly encoded RSA public key", e);
            }
        }

        public Consumer getConsumer() {
            return consumer;
        }
    }
}
