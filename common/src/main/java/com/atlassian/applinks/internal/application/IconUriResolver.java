package com.atlassian.applinks.internal.application;

import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.spi.application.IconizedType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;

import static java.util.Objects.requireNonNull;

/**
 * @since 4.3
 */
public final class IconUriResolver {
    private IconUriResolver() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nullable
    public static URI resolveIconUri(@Nonnull ApplicationType type) {
        requireNonNull(type, "type");
        if (type instanceof IconizedType) {
            return ((IconizedType) type).getIconUri();
        } else {
            return type.getIconUrl();
        }
    }

    @Nullable
    public static URI resolveIconUri(@Nonnull EntityType type) {
        requireNonNull(type, "type");
        if (type instanceof IconizedType) {
            return ((IconizedType) type).getIconUri();
        } else {
            return type.getIconUrl();
        }
    }

}
