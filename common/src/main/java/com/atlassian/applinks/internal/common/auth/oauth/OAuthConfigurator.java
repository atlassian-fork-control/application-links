package com.atlassian.applinks.internal.common.auth.oauth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.internal.common.exception.ConsumerInformationUnavailableException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.permission.Unrestricted;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.status.oauth.OAuthConfigs;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.oauth.Consumer;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collections;

import static com.atlassian.applinks.internal.common.auth.oauth.util.Consumers.consumerBuilder;
import static java.util.Objects.requireNonNull;

/**
 * @since 5.1
 */
@Unrestricted("Consumers of this component need to enforce appropriate permission level")
public class OAuthConfigurator {
    // NOTE: this is unit tested in DefaultOAuthStatusServiceTest

    private static final Logger log = LoggerFactory.getLogger(OAuthConfigurator.class);

    private final ConsumerTokenStoreService consumerTokenStoreService;
    private final ServiceProviderStoreService serviceProviderStoreService;
    private final ServiceExceptionFactory serviceExceptionFactory;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;

    public OAuthConfigurator(
            AuthenticationConfigurationManager authenticationConfigurationManager,
            ConsumerTokenStoreService consumerTokenStoreService,
            ServiceProviderStoreService serviceProviderStoreService,
            ServiceExceptionFactory serviceExceptionFactory) {
        this.consumerTokenStoreService = consumerTokenStoreService;
        this.serviceProviderStoreService = serviceProviderStoreService;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.serviceExceptionFactory = serviceExceptionFactory;
    }

    @Nonnull
    public OAuthConfig getIncomingConfig(@Nonnull ApplicationLink applink) {
        requireNonNull(applink, "applink");
        return OAuthConfigs.fromConsumer(serviceProviderStoreService.getConsumer(applink));
    }

    @Nonnull
    public OAuthConfig getOutgoingConfig(@Nonnull ApplicationLink link) {
        requireNonNull(link, "link");
        final boolean is3LoConfigured = authenticationConfigurationManager.isConfigured(link.getId(),
                OAuthAuthenticationProvider.class);
        final boolean is2LoConfigured = authenticationConfigurationManager.isConfigured(link.getId(),
                TwoLeggedOAuthAuthenticationProvider.class);
        final boolean is2LoIConfigured = authenticationConfigurationManager.isConfigured(link.getId(),
                TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);

        return OAuthConfig.fromConfig(is3LoConfigured, is2LoConfigured, is2LoIConfigured);
    }

    public void updateOutgoingConfig(@Nonnull ApplicationLink applink, @Nonnull OAuthConfig outgoing) {
        requireNonNull(applink, "link");
        requireNonNull(outgoing, "outgoing");

        if (!outgoing.isEnabled()) {
            tryRemoveConsumerTokens(applink);
        }
        toggleProvider(applink, OAuthAuthenticationProvider.class, outgoing.isEnabled());
        toggleProvider(applink, TwoLeggedOAuthAuthenticationProvider.class, outgoing.isTwoLoEnabled());
        toggleProvider(applink, TwoLeggedOAuthWithImpersonationAuthenticationProvider.class,
                outgoing.isTwoLoImpersonationEnabled());
    }

    public void updateIncomingConfig(@Nonnull ApplicationLink applink, @Nonnull OAuthConfig incoming)
            throws ConsumerInformationUnavailableException {
        requireNonNull(applink, "applink");
        requireNonNull(incoming, "incoming");

        if (!incoming.isEnabled()) {
            try {
                serviceProviderStoreService.removeConsumer(applink);
            } catch (IllegalStateException e) {
                log.debug("Attempting to remove non-existing consumer for Application Link '{}'", applink);
                log.trace("Stack trace for link '{}'", applink, e);
            }
        } else {
            Consumer updatedConsumer = consumerBuilder(getOrFetchConsumer(applink))
                    .twoLOAllowed(incoming.isTwoLoEnabled())
                    .twoLOImpersonationAllowed(incoming.isTwoLoImpersonationEnabled())
                    .build();

            serviceProviderStoreService.addConsumer(updatedConsumer, applink);
        }
    }

    private void tryRemoveConsumerTokens(@Nonnull ApplicationLink applink) {
        try {
            consumerTokenStoreService.removeAllConsumerTokens(applink);
        } catch (IllegalStateException ignored) {
            // ignore, this is expected when there is no pre-existing outgoing providers configured
        }
    }

    private void toggleProvider(ApplicationLink link, Class<? extends AuthenticationProvider> providerClass,
                                boolean enabled) {
        if (enabled) {
            authenticationConfigurationManager.registerProvider(link.getId(), providerClass,
                    Collections.emptyMap());
        } else {
            authenticationConfigurationManager.unregisterProvider(link.getId(), providerClass);
        }
    }

    private Consumer getOrFetchConsumer(ApplicationLink link) throws ConsumerInformationUnavailableException {
        Consumer consumer = serviceProviderStoreService.getConsumer(link);
        if (consumer != null) {
            return consumer;
        } else {
            return fetchConsumerInformation(link);
        }
    }

    @VisibleForTesting
    protected Consumer fetchConsumerInformation(ApplicationLink link) throws ConsumerInformationUnavailableException {
        try {
            return ConsumerInformationHelper.fetchConsumerInformation(link);
        } catch (ResponseException e) {
            throw serviceExceptionFactory.create(ConsumerInformationUnavailableException.class, link.getName());
        }
    }
}
