package com.atlassian.applinks.internal.common.permission;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.annotation.Nonnull;

/**
 * Applicable to service or API (types or methods) to indicate that certain service or service method has unrestricted
 * access, that is it can be access by authenticated and anonymous users alike. All services and other business
 * components should have their permission level documented using this annotation - or {@code Restricted}.
 *
 * @see Restricted
 * @since 4.3
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Unrestricted {
    /**
     * @return mandatory reason for why the service API has unrestricted access
     */
    @Nonnull String value();
}