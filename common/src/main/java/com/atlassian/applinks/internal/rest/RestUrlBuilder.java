package com.atlassian.applinks.internal.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.google.common.base.Joiner;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Constructs REST URLs to Applinks REST APIs
 *
 * @since 4.3
 */
public class RestUrlBuilder {
    public static final RestUrl REST_CONTEXT = RestUrl.forPath("rest");
    public static final RestUrl APPLINKS_REST_MODULE = RestUrl.forPath("applinks");

    private URI baseUrl;

    private RestUrl module = APPLINKS_REST_MODULE;
    private RestVersion version = RestVersion.DEFAULT;
    private RestUrl path = RestUrl.EMPTY;

    private final List<String> queryParams = new ArrayList<>();

    public RestUrlBuilder() {
    }

    public RestUrlBuilder(@Nonnull URI baseUrl) {
        this.baseUrl = validateBaseUrl(baseUrl);
    }

    /**
     * @param baseUrl base URL, will be converted into {@code URI}
     * @throws IllegalArgumentException if conversion to {@code URI} fails
     * @see URI#create(String)
     */
    public RestUrlBuilder(@Nonnull String baseUrl) {
        this(URI.create(baseUrl));
    }

    public RestUrlBuilder(@Nonnull RestUrlBuilder other) {
        this(requireNonNull(other, "other").baseUrl);
        this.module = other.module;
        this.version = other.version;
        this.path = other.path;
    }

    @Nonnull
    public RestUrlBuilder baseUrl(@Nonnull URI baseUrl) {
        this.baseUrl = validateBaseUrl(baseUrl);

        return this;
    }

    @Nonnull
    public RestUrlBuilder to(@Nonnull ApplicationLink applicationLink) {
        return baseUrl(applicationLink.getRpcUrl());
    }

    /**
     * @param baseUrl base URL, will be converted into {@code URI}
     * @return this builder instance
     * @throws IllegalArgumentException if conversion to {@code URI} fails
     * @see URI#create(String)
     */
    @Nonnull
    public RestUrlBuilder baseUrl(@Nonnull String baseUrl) {
        return baseUrl(URI.create(baseUrl));
    }

    @Nonnull
    public RestUrlBuilder module(@Nonnull RestUrl module) {
        this.module = requireNonNull(module, "module");

        return this;
    }

    @Nonnull
    public RestUrlBuilder module(@Nonnull String modulePath) {
        return module(RestUrl.forPath(modulePath));
    }

    @Nonnull
    public RestUrlBuilder version(@Nonnull RestVersion version) {
        this.version = requireNonNull(version, "version");

        return this;
    }

    @Nonnull
    public RestUrlBuilder addPath(@Nonnull RestUrl path) {
        this.path = this.path.add(path);

        return this;
    }

    @Nonnull
    public RestUrlBuilder addPath(@Nonnull String path) {
        this.path = this.path.add(path);

        return this;
    }

    @Nonnull
    public RestUrlBuilder addApplicationId(@Nonnull ApplicationId applicationId) {
        return addPath(requireNonNull(applicationId, "applicationId").toString());
    }

    @Nonnull
    public RestUrlBuilder addApplink(@Nonnull ApplicationLink applicationLink) {
        return addApplicationId(requireNonNull(applicationLink, "applicationLink").getId());
    }

    @Nonnull
    public RestUrlBuilder queryParam(@Nonnull String name, @Nonnull String value) {
        requireNonNull(name, "name");
        requireNonNull(value, "value");

        queryParams.add(name + "=" + value);

        return this;
    }

    /**
     * @return complete URI pointing to the REST resource of choice
     * @throws IllegalArgumentException if URI could not be created - one of the arguments passed to this builder was
     *                                  not a valid URI component
     */
    @Nonnull
    public URI build() {
        String url = getBaseUrl()
                .add(REST_CONTEXT)
                .add(module)
                .add(version.getPath())
                .add(path)
                .toString();
        if (!isAbsolute()) {
            // prefix with '/' if not absolute
            url = RestUrl.COMPONENT_SEPARATOR + url;
        }
        if (!queryParams.isEmpty()) {
            url = url + "?" + Joiner.on("&").join(queryParams);
        }

        return URI.create(url);
    }

    @Override
    public String toString() {
        return build().toString();
    }

    private static URI validateBaseUrl(URI baseUrl) {
        requireNonNull(baseUrl, "baseUrl");
        Validate.isTrue(baseUrl.isAbsolute(), "Base URL was not an absolute URI: " + baseUrl);

        return baseUrl;
    }

    private boolean isAbsolute() {
        return baseUrl != null;
    }

    private RestUrl getBaseUrl() {
        return baseUrl != null ? RestUrl.forPath(baseUrl.toASCIIString()) : RestUrl.EMPTY;
    }
}
