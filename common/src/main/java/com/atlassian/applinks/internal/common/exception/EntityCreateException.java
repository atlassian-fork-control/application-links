package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Raised specifically when an entity creation fails.
 *
 * @since 4.3
 */
public class EntityCreateException extends EntityModificationException {
    public EntityCreateException(@Nullable String message) {
        super(message);
    }

    public EntityCreateException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
