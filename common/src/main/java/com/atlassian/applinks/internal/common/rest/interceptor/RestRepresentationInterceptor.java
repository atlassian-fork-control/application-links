package com.atlassian.applinks.internal.common.rest.interceptor;

import com.atlassian.applinks.internal.common.rest.util.RestResponses;
import com.atlassian.applinks.internal.rest.model.IllegalRestRepresentationStateException;
import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.atlassian.plugins.rest.common.interceptor.ResourceInterceptor;
import com.atlassian.sal.api.message.I18nResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.lang.reflect.InvocationTargetException;

/**
 * REST interceptor that transforms {@link IllegalRestRepresentationStateException} into a "bad request" response.
 *
 * @since 5.0
 */
public class RestRepresentationInterceptor implements ResourceInterceptor {
    private static final Logger log = LoggerFactory.getLogger(RestRepresentationInterceptor.class);

    private final I18nResolver i18nResolver;

    public RestRepresentationInterceptor(I18nResolver i18nResolver) {
        this.i18nResolver = i18nResolver;
    }

    @Override
    public void intercept(MethodInvocation invocation) throws IllegalAccessException, InvocationTargetException {
        try {
            invocation.invoke();
        } catch (InvocationTargetException e) {
            IllegalRestRepresentationStateException restException = findRestConversionError(e);
            if (restException != null) {
                invocation.getHttpContext().getResponse().setResponse(createResponse(restException));
            } else {
                // propagate the exception
                throw e;
            }
        }
    }

    private static IllegalRestRepresentationStateException findRestConversionError(InvocationTargetException e) {
        Throwable cause = e.getCause();
        while (cause != null && !IllegalRestRepresentationStateException.class.isInstance(cause)) {
            cause = cause.getCause();
        }

        return (IllegalRestRepresentationStateException) cause;
    }

    private Response createResponse(IllegalRestRepresentationStateException e) {
        log.warn("Converting REST representation into a domain object failed with " +
                "IllegalRestRepresentationStateException: {}:{}", e.getContext(), e.getMessage());
        log.debug("Stack trace for IllegalRestRepresentationStateException with context {}", e.getContext(), e);

        return RestResponses.badRequest(e.getContext(),
                i18nResolver.getText("applinks.rest.invalidrepresentation", e.getContext()));
    }
}
