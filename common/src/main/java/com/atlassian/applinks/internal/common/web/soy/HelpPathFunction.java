package com.atlassian.applinks.internal.common.web.soy;

import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * Based off JIRA Service desk's HelpUrlFunction.java, but adapted to how Applinks does help links.
 *
 * NOTE: client-side this relies on help paths data being loaded on the page, which necessitates depending on
 * {@code applinks-common} web resource.
 */
public class HelpPathFunction implements SoyServerFunction<String>, SoyClientFunction {
    private final DocumentationLinker documentationLinker;

    public HelpPathFunction(DocumentationLinker documentationLinker) {
        this.documentationLinker = documentationLinker;
    }

    /**
     * Generates a javascript expression that is evaluated to produce the documentation link in soy templates evaluated on the client side
     *
     * @param args an array containing a single expression which is the documentation link key to look up
     * @return a javascript expression that when evaluated produces the documentation link corresponding to the argument
     */
    @Override
    public JsExpression generate(JsExpression... args) {
        checkArguments(args.length);

        String arg1 = args[0].getText();
        String arg2 = args.length == 2 ? args[1].getText() : "undefined";
        return new JsExpression("require('applinks/common/help-paths').getFullPath(" + arg1 + "," + arg2 + ")");
    }

    @Override
    public String getName() {
        return "getHelpUrl";
    }

    @Override
    public String apply(Object... args) {
        checkArguments(args.length);
        requireNonNull(args[0], "page key");
        if (args.length == 1) {
            return documentationLinker.getLink(args[0].toString()).toString();
        } else {
            requireNonNull(args[1], "section key");
            return documentationLinker.getLink(args[0].toString(), args[1].toString()).toString();
        }
    }

    private void checkArguments(int actual) {
        if (actual < 1 || actual > 2) {
            throw new IllegalArgumentException("Wrong number of arguments: expected <1 or 2>, was: " + actual);
        }
    }

    @Override
    public Set<Integer> validArgSizes() {
        return ImmutableSet.of(1, 2);
    }
}
