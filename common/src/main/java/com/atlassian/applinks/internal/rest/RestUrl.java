package com.atlassian.applinks.internal.rest;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * @since 4.3
 */
public final class RestUrl {
    public static final RestUrl EMPTY = new RestUrl(Collections.<String>emptyList());

    public static final String COMPONENT_SEPARATOR = "/";

    private final List<String> components;

    @Nonnull
    public static RestUrl forPath(@Nonnull String path) {
        return EMPTY.add(path);
    }

    private RestUrl(List<String> components) {
        this.components = components;
    }

    @Nonnull
    public RestUrl add(@Nonnull String component) {
        requireNonNull(component, "component");

        String sanitized = StringUtils.strip(component);
        sanitized = StringUtils.strip(sanitized, COMPONENT_SEPARATOR);

        return sanitized.isEmpty() ? this : new RestUrl(ImmutableList.<String>builder()
                .addAll(components)
                .add(sanitized).build());
    }

    @Nonnull
    public RestUrl add(@Nonnull RestUrl other) {
        requireNonNull(other, "other");

        return new RestUrl(ImmutableList.<String>builder().addAll(components).addAll(other.components).build());
    }

    @Nonnull
    public String toString() {
        return StringUtils.join(components, COMPONENT_SEPARATOR);
    }
}
