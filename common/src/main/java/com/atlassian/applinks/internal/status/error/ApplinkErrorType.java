package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.internal.common.i18n.I18nKey;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.common.i18n.I18nKey.newI18nKey;
import static java.lang.String.format;

/**
 * Represents specific type of Application Link error. Each specific error type belongs to a particuer
 * {@link #getCategory() category}.
 *
 * @see ApplinkErrorCategory
 * @since 4.3
 */
public enum ApplinkErrorType {
    /**
     * An unrecognized error occurred while trying to obtain the status. This will be logged and the error details
     * provided to the client.
     */
    UNKNOWN(ApplinkErrorCategory.UNKNOWN),


    // see ApplinkErrorCategory#INCOMPATIBLE
    /**
     * The remote system requires authentication to obtain the status (does not support Status API), but there's no
     * compatible outgoing authentication configured. Compatible authentication includes any of the OAuth impersonating
     * providers.
     */
    NO_OUTGOING_AUTH(ApplinkErrorCategory.INCOMPATIBLE),
    /**
     * Remote Applinks version is too old, minimum supported version is currently 4.0.13.
     */
    REMOTE_VERSION_INCOMPATIBLE(ApplinkErrorCategory.INCOMPATIBLE),


    // see ApplinkErrorCategory#NON_ATLASSIAN
    GENERIC_LINK(ApplinkErrorCategory.NON_ATLASSIAN), // generic link
    NON_ATLASSIAN(ApplinkErrorCategory.NON_ATLASSIAN), // non-Atlassian link


    // system link, see ApplinkErrorCategory#SYSTEM
    SYSTEM_LINK(ApplinkErrorCategory.SYSTEM),


    // see ApplinkErrorCategory#NETWORK_ERROR
    CONNECTION_REFUSED(ApplinkErrorCategory.NETWORK_ERROR),
    UNKNOWN_HOST(ApplinkErrorCategory.NETWORK_ERROR),
    SSL_UNTRUSTED(ApplinkErrorCategory.NETWORK_ERROR),
    SSL_HOSTNAME_UNMATCHED(ApplinkErrorCategory.NETWORK_ERROR),
    SSL_UNMATCHED(ApplinkErrorCategory.NETWORK_ERROR),
    /**
     * Generic OAuth Problem. Error details should contain the specific problem details.
     */
    OAUTH_PROBLEM(ApplinkErrorCategory.NETWORK_ERROR),
    /**
     * OAuth timestamp refused. Suggests server times out of sync.
     */
    OAUTH_TIMESTAMP_REFUSED(ApplinkErrorCategory.NETWORK_ERROR),
    /**
     * OAuth signature is invalid. This could be due to bad reverse proxying or redirects.
     */
    OAUTH_SIGNATURE_INVALID(ApplinkErrorCategory.NETWORK_ERROR),
    /**
     * The system was able to contact the remote application, but it responded with unexpected contents. May indicate
     * a firewall or a proxy, or unsupported version of the remote application.
     */
    UNEXPECTED_RESPONSE(ApplinkErrorCategory.NETWORK_ERROR),
    /**
     * The system was able to contact the remote application, but it responded with unexpected status code. May indicate
     * a firewall or a proxy, or unsupported version of the remote application.
     */
    UNEXPECTED_RESPONSE_STATUS(ApplinkErrorCategory.NETWORK_ERROR),


    // see ApplinkErrorCategory#ACCESS_ERROR
    /**
     * The remote system requires authentication to obtain the status (does not support the Status API), but the current
     * user does not have a valid local authentication token. The error will contain URL to obtain authentication from.
     */
    LOCAL_AUTH_TOKEN_REQUIRED(ApplinkErrorCategory.ACCESS_ERROR),
    /**
     * The remote system requires authentication to obtain the status (does not support the Status API), but the current
     * user does not have a valid remote authentication token. The error will contain URL to obtain authentication from.
     */
    REMOTE_AUTH_TOKEN_REQUIRED(ApplinkErrorCategory.ACCESS_ERROR),
    /**
     * The remote system requires admin authentication to obtain the status (does not support Status API), but the
     * current user, while being logged in, does not have sufficient permissions on the remote system.
     */
    INSUFFICIENT_REMOTE_PERMISSION(ApplinkErrorCategory.ACCESS_ERROR),


    // see ApplinkErrorCategory#CONFIG_ERROR
    /**
     * The remote application does not have a link to this application.
     */
    NO_REMOTE_APPLINK(ApplinkErrorCategory.CONFIG_ERROR),
    /**
     * Authentication levels on the local system do not match the remote system.
     */
    AUTH_LEVEL_MISMATCH(ApplinkErrorCategory.CONFIG_ERROR),


    // see ApplinkErrorCategory#DEPRECATED
    /**
     * Authentication level configuration is unsupported. This is when OAuth is configured as 3LO only.
     */
    AUTH_LEVEL_UNSUPPORTED(ApplinkErrorCategory.DEPRECATED),
    /**
     * Automatic migration from trusted/basic is possible.
     */
    LEGACY_UPDATE(ApplinkErrorCategory.DEPRECATED),
    /**
     * Both local and remote already has Oauth. Automatic removal of trusted/basic remotely is possible
     */
    LEGACY_REMOVAL(ApplinkErrorCategory.DEPRECATED),
    /**
     * We need to manually migrate from trusted/basic to Oauth.
     */
    MANUAL_LEGACY_UPDATE(ApplinkErrorCategory.DEPRECATED),
    /**
     * Both local and remote already has Oauth. We need to manually remove trusted/basic remotely.
     */
    MANUAL_LEGACY_REMOVAL(ApplinkErrorCategory.DEPRECATED),

    /**
     * Both local and remote already has Oauth but remote still uses old edit screen. We cannot remove our local legacy
     * because this will confuse the old edit screen on the remote. We inform the user to manually remove it remotely.
     */
    MANUAL_LEGACY_REMOVAL_WITH_OLD_EDIT(ApplinkErrorCategory.DEPRECATED),

    /**
     * Authenticated communication between the linked applications is disabled
     */
    DISABLED(ApplinkErrorCategory.DISABLED);


    private static final String I18N_TEMPLATE = "applinks.status.%s.title";

    private final ApplinkErrorCategory category;

    ApplinkErrorType(ApplinkErrorCategory category) {
        this.category = category;
    }

    /**
     * @return the error category corresponding to this error
     */
    @Nonnull
    public ApplinkErrorCategory getCategory() {
        return category;
    }

    /**
     * @return i18n key for a human-readable summary of the problem
     */
    @Nonnull
    public I18nKey getI18nKey() {
        return newI18nKey(format(I18N_TEMPLATE, name().toLowerCase()));
    }
}