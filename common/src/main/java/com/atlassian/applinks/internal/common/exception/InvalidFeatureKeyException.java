package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Raised if a feature key was invalid.
 *
 * @since 4.3
 */
public class InvalidFeatureKeyException extends InvalidArgumentException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.feature.invalidkey";

    public InvalidFeatureKeyException(@Nullable String message) {
        super(message);
    }

    public InvalidFeatureKeyException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
