package com.atlassian.applinks.internal.rest.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.Validate;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.atlassian.applinks.internal.rest.model.RestRepresentations.fromDomainFunction;
import static com.atlassian.applinks.internal.rest.model.RestRepresentations.fromDomainObject;
import static com.google.common.collect.Iterables.transform;
import static java.lang.String.format;
import static java.util.Collections.singletonMap;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * <p>
 * Base REST entity extending from a map, to avoid performance penalty associated with reflection while
 * marshalling/unmarshalling to and from JSON.
 * </p>
 * <p>
 * This can be extended by specific entities, or used directly as is via one of the constructors, factory methods and
 * builders.
 * </p>
 * <p>
 * This entity is meant to be used with the {@code Jackson} serializer, but can also be used with any serialization
 * library that supports marshalling to and unmarshalling from {@code Map}.
 * </p>
 * <p>
 * Note: unlike its superclass, this class does not support {@code null} keys.
 * </p>
 *
 * @since 4.3
 */
@JsonSerialize
public class BaseRestEntity extends LinkedHashMap<String, Object> {
    @Nonnull
    public static BaseRestEntity createSingleFieldEntity(@Nonnull String fieldName, @Nullable Object value) {
        return new BaseRestEntity(singletonMap(requireNonNull(fieldName, "fieldName"), value));
    }

    @SuppressWarnings("unused") // for deserialization
    public BaseRestEntity() {
    }

    public BaseRestEntity(@Nullable Map<String, Object> values) {
        if (values != null) {
            putAll(values);
        }
    }

    @Nonnull
    public Object getRequired(@Nonnull String key) {
        requireNonNull(key, "key");
        return requiredValue(key, get(key));
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public Map<String, Object> getJson(@Nonnull String key) {
        requireNonNull(key, "key");
        return expectedType(key, get(key), Map.class);
    }

    @Nonnull
    public Map<String, Object> getRequiredJson(@Nonnull String key) {
        requireNonNull(key, "key");
        return requiredValue(key, getJson(key));
    }

    /**
     * Get a string field value as a {@code Boolean}.
     *
     * @param key field name
     * @return matching {@code Boolean} value or {@code null} if the field was not set
     * @throws IllegalRestRepresentationStateException if the field is not a {@code Boolean} or a string representing a
     *                                                 {@code Boolean}
     */
    @Nullable
    public final Boolean getBoolean(@Nonnull String key) {
        requireNonNull(key, "key");
        return expectedType(key, get(key), Boolean.class);
    }

    /**
     * Get a string field value as a {@code boolean}.
     *
     * @param key field name
     * @return matching {@code boolean} value or {@code false} if the field was not set
     * @throws IllegalRestRepresentationStateException if the field is not a {@code Boolean} or a string representing a
     *                                                 {@code Boolean}
     */
    public final boolean getBooleanValue(@Nonnull String key) {
        return Boolean.TRUE.equals(getBoolean(key));
    }

    @Nullable
    public final String getString(@Nonnull String key) {
        requireNonNull(key, "key");
        return expectedType(key, get(key), String.class);
    }

    @Nonnull
    public final String getRequiredString(@Nonnull String key) {
        String value = getString(key);
        return validValue(key, value, isNotBlank(value));
    }

    @Nullable
    public final Integer getInt(@Nonnull String key) {
        Number number = expectedType(key, get(key), Number.class);
        return number != null ? number.intValue() : null;
    }

    public final int getRequiredInt(@Nonnull String key) {
        Integer value = getInt(key);
        return requiredValue(key, value);
    }

    @Nullable
    public final URI getUri(@Nonnull String key) throws IllegalRestRepresentationStateException {
        return asUri(key, getString(key));
    }

    @Nonnull
    @SuppressWarnings("ConstantConditions")
    public final URI getRequiredUri(@Nonnull String key) throws IllegalRestRepresentationStateException {
        return asUri(key, getRequiredString(key));
    }

    /**
     * Get a string field value as an {@code enum} value.
     *
     * @param key      field name
     * @param enumType type of the {@code enum}
     * @param <V>      type param
     * @return instance of the matching {@code enum}, or {@code null} if the field is not set
     * @throws IllegalRestRepresentationStateException if the field is not a string or there is no corresponding
     *                                                 {@code enum} value.
     */
    @Nullable
    public final <V extends Enum<V>> V getEnum(@Nonnull String key, @Nonnull Class<V> enumType) {
        requireNonNull(key, "key");
        requireNonNull(enumType, "enumType");

        Object val = get(key);
        if (val == null || enumType.isInstance(val)) {
            return enumType.cast(val);
        }
        if (val instanceof String) {
            try {
                return Enum.valueOf(enumType, (String) val);
            } catch (IllegalArgumentException e) {
                throw new IllegalRestRepresentationStateException(key, format("Failed to convert %s to enum", val), e);
            }
        }

        throw new IllegalRestRepresentationStateException(key,
                "Unexpected type not convertible to enum: " + val.getClass().getName());
    }

    /**
     * Get a JSON-object field as an instance of a corresponding {@link RestRepresentation} entity.
     *
     * @param key        field name
     * @param entityType type of the matching REST entity
     * @param <V>        type param
     * @return instance of the matching entity, or {@code null} if the field is not set
     * @throws IllegalStateException if the field is not a deserialized JSON object
     */
    @Nullable
    @SuppressWarnings("unchecked")
    public final <V extends RestRepresentation<?>> V getRestEntity(@Nonnull String key, @Nonnull Class<V> entityType) {
        requireNonNull(key, "key");
        requireNonNull(entityType, "entityType");

        Object val = get(key);
        if (val == null || entityType.isInstance(val)) {
            return entityType.cast(val);
        }
        if (val instanceof Map) {
            return RestRepresentations.fromMap((Map<String, Object>) val, entityType);
        }

        throw new IllegalRestRepresentationStateException(key, format("Unexpected type not convertible to %s: %s",
                entityType.getName(), val.getClass().getName()));
    }

    @Nonnull
    public final <V extends RestRepresentation<?>> V getRequiredRestEntity(@Nonnull String key, @Nonnull Class<V> entityType) {
        return requiredValue(key, getRestEntity(key, entityType));
    }

    /**
     * Get a JSON-object field as an instance of a target domain oject corresponding to a {@link RestRepresentation}
     * entity.
     *
     * @param key        field name
     * @param entityType type of the matching REST entity
     * @param <V>        type param
     * @return instance of the matching domain object, or {@code null} if the field is not set
     * @throws IllegalStateException if the field is not a deserialized JSON object
     * @see RestRepresentation
     */
    @Nullable
    @SuppressWarnings("unchecked")
    public final <D, V extends RestRepresentation<D>> D getDomain(@Nonnull String key, @Nonnull Class<V> entityType) {
        requireNonNull(key, "key");
        requireNonNull(entityType, "entityType");

        V restRepresentation = getRestEntity(key, entityType);
        return restRepresentation != null ? restRepresentation.asDomain() : null;
    }

    @Nonnull
    public final <D, V extends RestRepresentation<D>> D getRequiredDomain(@Nonnull String key,
                                                                          @Nonnull Class<V> entityType) {
        return requiredValue(key, getDomain(key, entityType));
    }

    public final void putIfNotNull(@Nonnull String key, @Nullable Object value) {
        if (value != null) {
            put(key, value);
        }
    }

    /**
     * Put {@code value} as its REST representation under {@code key}, if non-{@code null}.
     *
     * @param key                property key
     * @param value              domain object value
     * @param restRepresentation REST representation class of {@code value}
     * @param <T>                domain object type param
     * @param <R>                REST representation type param
     * @throws IllegalArgumentException if {@code key} is blank
     * @throws IllegalStateException    if {@code R} does not provide the required constructor
     * @see ReadOnlyRestRepresentation
     */
    public final <T, R extends ReadOnlyRestRepresentation<T>> void putAs(@Nonnull String key, @Nullable T value,
                                                                         @Nonnull Class<R> restRepresentation) {
        putIfNotNull(key, fromDomainObject(value, restRepresentation));
    }

    public final <E extends Enum<E>> void putEnum(@Nonnull String key, @Nullable E value) {
        if (value != null) {
            put(key, value.name());
        }
    }

    public final void putMap(@Nonnull String key, @Nullable Map<?, ?> value) {
        if (value != null && !value.isEmpty()) {
            put(key, value);
        }
    }

    public final void putIterable(@Nonnull String key, @Nullable Iterable<?> value) {
        if (value != null && value.iterator().hasNext()) {
            put(key, value);
        }
    }

    /**
     * Similar to {@link #putAs(String, Object, Class)}, but works on iterables of type {@code T} that has a
     * {@link ReadOnlyRestRepresentation} {@code R}.
     *
     * @param key                property key
     * @param value              iterable of values of type {@code T}
     * @param restRepresentation REST representation type of {@code T}
     * @throws IllegalArgumentException if {@code key} is blank
     * @throws IllegalStateException    if {@code R} does not provide the required constructor
     * @see ReadOnlyRestRepresentation
     */
    public final <T, R extends ReadOnlyRestRepresentation<T>> void putIterableOf(@Nonnull String key,
                                                                                 @Nullable Iterable<T> value,
                                                                                 @Nonnull Class<R> restRepresentation) {
        if (value != null) {
            putIterable(key, ImmutableList.copyOf(transform(value, fromDomainFunction(restRepresentation))));
        }
    }

    public final void putAsString(@Nonnull String key, @Nullable Object value) {
        if (value != null) {
            put(key, value.toString());
        }
    }

    @Override
    public Object put(String key, Object value) {
        checkKey(key);
        return super.put(key, value);
    }

    @Override
    public void putAll(Map<? extends String, ?> map) {
        for (String key : map.keySet()) {
            checkKey(key);
        }
        super.putAll(map);
    }

    protected static void checkKey(String key) {
        requireNonNull(key, "key");
        Validate.isTrue(isNotBlank(key), "key was blank");
    }

    @Nullable
    protected static <T> T expectedType(@Nonnull String key, @Nullable Object value, @Nonnull Class<T> expectedType) {
        if (value == null) {
            return null;
        }
        if (!expectedType.isInstance(value)) {
            throw new IllegalRestRepresentationStateException(key,
                    format("Value '%s' is not a %s", value, expectedType.getSimpleName()));
        }
        return expectedType.cast(value);
    }

    /**
     * @param key   key at which {@code value} was found
     * @param value value to validate
     * @param <T>   value type
     * @return {@code value} after validating that it's non-{@code null}
     * @throws IllegalRestRepresentationStateException if the validation fails
     */
    @Nonnull
    protected static <T> T requiredValue(@Nonnull String key, @Nullable T value)
            throws IllegalRestRepresentationStateException {
        return validValue(key, value, value != null);
    }

    /**
     * @param key   key at which {@code value} was found
     * @param value value to validate
     * @param <T>   value type
     * @return {@code value} after validating that it is non-{@code null} and passes the {@code test} - and therefore is
     * valid
     * @throws IllegalRestRepresentationStateException if the validation fails (i.e. {@code test} is false)
     */
    @Nonnull
    protected static <T> T validValue(@Nonnull String key, @Nullable T value, boolean test)
            throws IllegalRestRepresentationStateException {
        if (!test || value == null) {
            throw new IllegalRestRepresentationStateException(key);
        }
        return value;
    }

    private static URI asUri(@Nonnull String key, @Nullable String value) {
        try {
            return value != null ? new URI(value) : null;
        } catch (URISyntaxException e) {
            throw new IllegalRestRepresentationStateException(key, format("Failed to convert '%s' to URI", value), e);
        }
    }

    /**
     * Builder class to quickly create custom JSON entities on the fly.
     */
    public static class Builder {
        protected final Map<String, Object> fields = Maps.newLinkedHashMap();

        @Nonnull
        public Builder add(@Nonnull String fieldName, @Nullable Object value) {
            requireNonNull(fieldName, "fieldName");
            fields.put(fieldName, value);
            return this;
        }

        @Nonnull
        public BaseRestEntity build() {
            return new BaseRestEntity(fields);
        }
    }
}
