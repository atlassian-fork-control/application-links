package com.atlassian.applinks.internal.common.capabilities;

import com.google.common.annotations.VisibleForTesting;

/**
 * Enumeration of Applink capabilities. Capabilities are simple keys that identify specific pieces of functionality
 * that the application supports.
 *
 * @since 5.0
 */
public enum ApplinksCapabilities {
    /**
     * Indicates that the Status API is implemented by this Applinks version. This allows for querying for the status
     * of any applink on this application, both in-process and using REST.
     */
    STATUS_API,

    /**
     * Indicates that Migration API is implemented in this version. This allows the user to migrate their legacy authentication
     * such as Trusted and Basic to OAuth. This should also indicate we have the migration user interface.
     *
     * @since 5.2
     */
    MIGRATION_API;

    private static final String CAPABILITY_SYSPROP_PREFIX = "atlassian.applinks.capability.";

    // key to disable the capability
    // by default all existing capabilities are on
    @VisibleForTesting
    public final String key;


    ApplinksCapabilities() {
        this.key = CAPABILITY_SYSPROP_PREFIX + name();
    }
}