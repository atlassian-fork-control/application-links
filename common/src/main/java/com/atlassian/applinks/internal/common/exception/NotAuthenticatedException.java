package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Raised when there is no user context where user context is required.
 *
 * @since 4.3
 */
public class NotAuthenticatedException extends NoAccessException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.access.notauthenticated";

    public NotAuthenticatedException(@Nullable String message) {
        super(message);
    }

    public NotAuthenticatedException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
