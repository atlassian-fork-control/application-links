package com.atlassian.applinks.internal.common.lang;

import java.util.function.Predicate;
import java.util.function.Supplier;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Utilities to help use Java {@code @FunctionalInterface} types, such as functions and predicates.
 *
 * @since 5.2
 */
public final class FunctionalInterfaces {
    private FunctionalInterfaces() {
        // do not instantiate
    }

    @Nonnull
    public static <T> Supplier<T> supplierOf(@Nullable T instance) {
        return () -> instance;
    }

    /**
     * Syntactic sugar to turn lambdas into suppliers without awkward casts.
     *
     * @param supplier supplier
     * @param <T>      supplied type
     * @return the same supplier
     */
    @Nullable
    public static <T> Supplier<T> asSupplier(@Nullable Supplier<T> supplier) {
        return supplier;
    }

    /**
     * Negates {@code original}, use with function references and lambda expressions that cannot be readily negated due
     * to implicit cast to an interface type.
     *
     * @param original original predicate
     * @param <T>      element type
     * @return negated {@code original}
     */
    @Nonnull
    public static <T> Predicate<T> not(@Nonnull Predicate<T> original) {
        return original.negate();
    }

    /**
     * @param original Java8 predicate
     * @param <T>      predicate type
     * @return Guava predicate to use with legacy APIs
     */
    @Nonnull
    @SuppressWarnings("Guava")
    public static <T> com.google.common.base.Predicate<T> toGuavaPredicate(@Nonnull Predicate<T> original) {
        return original::test;
    }
}
