package com.atlassian.applinks.internal.common.application;

import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.spi.application.IdentifiableType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 4.3
 */
public final class ApplicationTypes {
    private ApplicationTypes() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static String resolveApplicationTypeId(@Nonnull ApplicationType type) {
        return isIdentifiable(type) ? asIdentifiableType(type).getId().get() : type.getI18nKey();
    }

    @Nonnull
    public static String resolveEntityTypeId(@Nonnull EntityType type) {
        return isIdentifiable(type) ? asIdentifiableType(type).getId().get() : type.getI18nKey();
    }

    public static boolean isIdentifiable(@Nullable Object type) {
        return type instanceof IdentifiableType;
    }

    public static IdentifiableType asIdentifiableType(@Nullable Object type) {
        return (IdentifiableType) type;
    }

    public static boolean isBuiltIn(@Nullable Object type) {
        return type instanceof BuiltinApplinksType;
    }

    public static boolean isGeneric(@Nullable Object type) {
        return type instanceof GenericApplicationType;
    }

    /**
     * @param type application type to examine
     * @return {@code true}, if {@code type} represents an Atlassian application, i.e. is
     * {@link #isBuiltIn(Object) built-in} and is <i>not</i> {@link GenericApplicationType}
     */
    public static boolean isAtlassian(@Nullable Object type) {
        return isBuiltIn(type) && !isGeneric(type);
    }
}
