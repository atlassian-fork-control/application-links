package com.atlassian.applinks.internal.common.net;

import com.atlassian.applinks.internal.common.exception.EntityModificationException;
import com.atlassian.applinks.internal.common.exception.InvalidArgumentException;
import com.atlassian.applinks.internal.common.exception.InvalidEntityStateException;
import com.atlassian.applinks.internal.common.exception.NoSuchEntityException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;

import java.util.Map;
import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

/**
 * Maps {@link ServiceException}s to HTTP status codes.
 *
 * @since 5.1
 */
public final class ServiceExceptionHttpMapper {
    private static final Response.Status FALLBACK_STATUS = Response.Status.BAD_REQUEST;

    @VisibleForTesting
    static final Map<Class<?>, Response.Status> ERROR_TO_CODE = ImmutableMap.<Class<?>, Response.Status>builder()
            .put(ServiceException.class, FALLBACK_STATUS)
            .put(InvalidArgumentException.class, Response.Status.BAD_REQUEST)
            .put(PermissionException.class, Response.Status.FORBIDDEN)
            .put(NotAuthenticatedException.class, Response.Status.UNAUTHORIZED)
            .put(NoSuchEntityException.class, Response.Status.NOT_FOUND)
            .put(EntityModificationException.class, Response.Status.CONFLICT)
            .put(InvalidEntityStateException.class, Response.Status.CONFLICT)
            .build();

    private ServiceExceptionHttpMapper() {
        // do not instantiate
    }

    @Nonnull
    public static Response.Status getStatus(@Nonnull ServiceException serviceException) {
        Class<?> errorClass = serviceException.getClass();
        while (errorClass != null) {
            if (ERROR_TO_CODE.containsKey(errorClass)) {
                return ERROR_TO_CODE.get(errorClass);
            } else {
                errorClass = errorClass.getSuperclass();
            }
        }
        // this should not happen _if_ ServiceException is in the ERROR_TO_CODE map
        return FALLBACK_STATUS;
    }
}
