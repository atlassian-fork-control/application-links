package com.atlassian.applinks.internal.common.web.data;

import com.atlassian.applinks.core.plugin.AuthenticationProviderModuleDescriptor;
import com.atlassian.applinks.internal.common.json.JacksonJsonableMarshaller;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import java.util.List;

/**
 * Injects authentication type i18n into the page for client-side usage.
 *
 * @see <a href="https://developer.atlassian.com/docs/advanced-topics/adding-data-providers-to-your-plugin">
 * Data provider documentation</a>
 * @since 5.2
 */
public class AuthenticationTypesDataProvider implements WebResourceDataProvider {
    private final I18nResolver i18nResolver;
    private final PluginAccessor pluginAccessor;

    public AuthenticationTypesDataProvider(I18nResolver i18nResolver, PluginAccessor pluginAccessor) {
        this.i18nResolver = i18nResolver;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public Jsonable get() {
        return JacksonJsonableMarshaller.INSTANCE.marshal(getAllTypes());
    }

    private BaseRestEntity getAllTypes() {
        BaseRestEntity.Builder allTypes = new BaseRestEntity.Builder();
        for (final AuthenticationProviderModuleDescriptor authProvider : getEnabledAuthModules()) {
            final String key = authProvider.getModule().getAuthenticationProviderClass().getName();
            allTypes.add(key, i18nResolver.getText(authProvider.getI18nNameKey()));
        }
        return allTypes.build();
    }

    private List<AuthenticationProviderModuleDescriptor> getEnabledAuthModules() {
        return pluginAccessor.getEnabledModuleDescriptorsByClass(AuthenticationProviderModuleDescriptor.class);
    }
}
