package com.atlassian.applinks.internal.common.net;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseProtocolException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

/**
 * Raised when the response content does not conform to the expected protocol. E.g. a plain text response where JSON
 * object was expected, or a JSON object with missing keys. The invalid response that triggered the error is available
 * for inspection.
 *
 * @since 5.0
 */
public class ResponseContentException extends ResponseProtocolException {
    private final Response response;

    public ResponseContentException(@Nonnull Response response, @Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
        this.response = requireNonNull(response, "response");
    }

    public ResponseContentException(@Nonnull Response response, @Nullable String message) {
        this(response, message, null);
    }

    public ResponseContentException(@Nonnull Response response, @Nullable Throwable cause) {
        this(response, null, cause);
    }

    public ResponseContentException(@Nonnull Response response) {
        this(response, null, null);
    }


    @Nonnull
    public Response getResponse() {
        return response;
    }
}
