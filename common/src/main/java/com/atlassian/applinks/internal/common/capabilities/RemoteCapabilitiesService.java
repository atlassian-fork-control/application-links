package com.atlassian.applinks.internal.common.capabilities;

import com.atlassian.annotations.Internal;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.exception.InvalidArgumentException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Restricted;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

/**
 * Provides access to {@link RemoteApplicationCapabilities} for a given application ID. Capabilities may be cached
 * locally to avoid unnecessary network load. To specify how stale the data can be, clients can use
 * {@link #getCapabilities(ApplicationId, long, TimeUnit)} with the {@code maxAge} argument set to an acceptable value
 * for the maximum age of the cache entry.
 * <p>
 * This API is restricted to {@link PermissionLevel#ADMIN administrators} as calls to it may result in significant
 * network IO.
 * </p>
 *
 * @since 5.0
 */
@Internal
@Restricted(PermissionLevel.ADMIN)
public interface RemoteCapabilitiesService {
    /**
     * Get the remote capabilities, using local cache. This is <i>likely</i> to not make any requests to the remote
     * application, but in some cases might be required to.
     *
     * @param applicationId application ID to inspect
     * @return remote capabilities
     * @throws NoSuchApplinkException if the application ID does not correspond to an existing applink
     * @throws NoAccessException      if the user has no {@link PermissionLevel#ADMIN Administrator} permission
     */
    @Nonnull
    RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationId applicationId)
            throws NoSuchApplinkException, NoAccessException;

    /**
     * @param applicationLink application link to inspect
     * @return remote capabilities for {@code applicationLink}
     * @throws NoAccessException if the user has no {@link PermissionLevel#ADMIN Administrator} permission
     * @see #getCapabilities(ApplicationId)
     */
    @Nonnull
    RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationLink applicationLink) throws NoAccessException;

    /**
     * Get the remote capabilities, using local cache or making a remote request depending on whether the cached value
     * meets the requirements of {@code maxAge}: if there is a cache entry that is "younger" than {@code maxAge}
     * (as expressed in {@code units}), it will be used, otherwise a fresh value will be retrieved. Use {@code 0} to
     * trigger a cache refresh and get the latest value (note: this will potentially trigger multiple network requests
     * and as such is likely to be a time-consuming operation).
     *
     * @param applicationId application ID to inspect
     * @param maxAge        specifies the oldest acceptable cache value
     * @param units         time unit for {@code maxAge}
     * @return remote capabilities
     * @throws NoSuchApplinkException   if the application ID does not correspond to an existing applink
     * @throws InvalidArgumentException if {@code maxAge} is lower than 0
     * @throws NoAccessException        if the user has no {@link PermissionLevel#ADMIN Administrator} permission
     */
    @Nonnull
    RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationId applicationId, long maxAge,
                                                  @Nonnull TimeUnit units)
            throws NoSuchApplinkException, NoAccessException, InvalidArgumentException;

    /**
     * @param applicationLink application link to inspect
     * @param maxAge          specifies the oldest acceptable cache value
     * @param units           time unit for {@code maxAge}
     * @return remote capabilities
     * @throws InvalidArgumentException if {@code maxAge} is lower than 0
     * @throws NoAccessException        if the user has no {@link PermissionLevel#ADMIN Administrator} permission
     * @see #getCapabilities(ApplicationId, long, TimeUnit)
     */
    @Nonnull
    RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationLink applicationLink, long maxAge,
                                                  @Nonnull TimeUnit units)
            throws InvalidArgumentException, NoAccessException;
}
