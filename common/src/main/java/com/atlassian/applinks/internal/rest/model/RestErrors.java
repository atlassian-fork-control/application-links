package com.atlassian.applinks.internal.rest.model;

import com.atlassian.applinks.internal.common.exception.DetailedErrors;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;

/**
 * JSON representation of an application error list.
 *
 * @see RestError
 * @since 4.3
 */
public class RestErrors extends BaseRestEntity {
    public static final String STATUS = "status";
    public static final String ERRORS = "errors";

    public RestErrors(@Nonnull Status status, @Nonnull Iterable<RestError> errors) {
        put(STATUS, requireNonNull(status, "status").getStatusCode());
        put(ERRORS, requireNonNull(errors, "errors"));
    }

    public RestErrors(@Nonnull Status status, @Nonnull DetailedErrors errors) {
        put(STATUS, requireNonNull(status, "status").getStatusCode());
        putIterableOf(ERRORS, requireNonNull(errors, "errors").getErrors(), RestError.class);
    }

    public RestErrors(@Nonnull Status status, @Nonnull RestError error) {
        this(status, singletonList(error));
    }

    /**
     * Create instance of REST errors with a single error in it, described by {@code summary}
     *
     * @param status  status
     * @param summary error summary
     * @see RestError#RestError(String)
     */
    public RestErrors(@Nonnull Status status, @Nonnull String summary) {
        this(status, new RestError(summary));
    }

    /**
     * Create instance of REST errors with a single error in it, representing {@code javaError}
     *
     * @param status    status
     * @param javaError Java specific exception
     * @see RestError#RestError(Exception)
     */
    public RestErrors(@Nonnull Status status, @Nonnull Exception javaError) {
        this(status, new RestError(javaError));
    }
}
