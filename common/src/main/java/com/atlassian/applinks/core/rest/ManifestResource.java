package com.atlassian.applinks.core.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.rest.model.ManifestEntity;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.rest.RestUrlBuilder;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkOAuthStatus;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.ApplicationProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.core.rest.util.RestUtil.badRequest;
import static com.atlassian.applinks.core.rest.util.RestUtil.notFound;
import static com.atlassian.applinks.core.rest.util.RestUtil.ok;

/**
 * @since v3.0
 */
@Api
@Path(ManifestResource.CONTEXT)
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@InterceptorChain({ContextInterceptor.class, NoCacheHeaderInterceptor.class})
public class ManifestResource {
    private static final Logger LOG = LoggerFactory.getLogger(ManifestResource.class);

    public static final String CONTEXT = "manifest";

    @Nonnull
    public static RestUrlBuilder manifestUrl() {
        return new RestUrlBuilder().addPath(CONTEXT);
    }

    private final InternalHostApplication internalHostApplication;
    private final ManifestRetriever manifestRetriever;
    private final ApplicationLinkService applicationLinkService;
    private final AppLinkPluginUtil pluginUtil;
    private final ApplicationProperties applicationProperties;

    public ManifestResource(final InternalHostApplication internalHostApplication, final ManifestRetriever manifestRetriever,
                            final ApplicationLinkService applicationLinkService, final ApplicationProperties applicationProperties,
                            final AppLinkPluginUtil pluginUtil) {
        this.internalHostApplication = internalHostApplication;
        this.manifestRetriever = manifestRetriever;
        this.applicationLinkService = applicationLinkService;
        this.applicationProperties = applicationProperties;
        this.pluginUtil = pluginUtil;
    }

    @GET
    @ApiOperation(
            value = "Returns the manifest for this application",
            response = ManifestEntity.class)
    @ApiResponse(code = 200, message = "Successful")
    @AnonymousAllowed
    public Response getManifest() {
        return ok(new ManifestEntity(internalHostApplication, applicationProperties, pluginUtil));
    }

    @GET
    @ApiOperation(
            value = "Returns the manifest for an applink with a given ID",
            response = ManifestEntity.class
    )
    @ApiResponses({@ApiResponse(code = 404, message = "Manifest not found on remote host"),
            @ApiResponse(code = 400, message = "No application link exists with provided ID"),
            @ApiResponse(code = 200, message = "Successful")
    })
    @Path("{id}")
    public Response getManifestFor(@PathParam("id") final String id) {
        final ApplicationId applicationId = new ApplicationId(id);

        ApplicationLink applicationLink = null;

        try {
            applicationLink = applicationLinkService.getApplicationLink(applicationId);
        } catch (TypeNotInstalledException e) {
            // ignore, type not installed is considered equivalent to non-existent
        }

        if (applicationLink == null) {
            return badRequest(String.format("No application link with id %s", applicationId));
        }

        final Manifest manifest;
        try {
            manifest = manifestRetriever.getManifest(applicationLink.getRpcUrl(), applicationLink.getType());
        } catch (ManifestNotFoundException e) {
            return notFound(String.format("Couldn't retrieve manifest for link with id %s", applicationId));
        }

        return response(manifest);
    }

    private Response response(final Manifest manifest) {
        return ok(new ManifestEntity(manifest));
    }
}
