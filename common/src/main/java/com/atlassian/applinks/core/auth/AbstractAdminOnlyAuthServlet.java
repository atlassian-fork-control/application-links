package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.ui.AbstractAppLinksAdminOnlyServlet;
import com.atlassian.applinks.ui.AbstractApplinksServlet;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

import static java.util.Collections.singletonList;

/**
 * <p> Base class used by several authentication provider servlets. </p> <p> Extracts the application link ID from the servlet path,
 * retrieves the {@link com.atlassian.applinks.api.ApplicationLink} instance and makes it available to the callback methods ({@link
 * #doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}, {@link
 * #doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}, etc). </p> <p> This class requires the
 * Application Link ID to be the first node in {@link javax.servlet.http.HttpServletRequest#getPathInfo()}. </p> <p> If the application link
 * ID that is on the servlet path does not correspond to any of our locally configured peers, {@link
 * #getRequiredApplicationLink(javax.servlet.http.HttpServletRequest)} throws a {@link com.atlassian.applinks.ui.AbstractApplinksServlet.NotFoundException}.
 * </p>
 *
 * @since v3.0
 */
public abstract class AbstractAdminOnlyAuthServlet extends AbstractAppLinksAdminOnlyServlet {
    public static String HOST_URL_PARAM = "hostUrl";
    private final ApplicationLinkService applicationLinkService;

    protected AbstractAdminOnlyAuthServlet(final I18nResolver i18nResolver,
                                           final MessageFactory messageFactory,
                                           final TemplateRenderer templateRenderer,
                                           final WebResourceManager webResourceManager,
                                           final ApplicationLinkService applicationLinkService,
                                           final AdminUIAuthenticator adminUIAuthenticator,
                                           final DocumentationLinker documentationLinker,
                                           final LoginUriProvider loginUriProvider,
                                           final InternalHostApplication internalHostApplication,
                                           final XsrfTokenAccessor xsrfTokenAccessor,
                                           final XsrfTokenValidator xsrfTokenValidator) {
        super(i18nResolver, messageFactory, templateRenderer, webResourceManager, adminUIAuthenticator,
                documentationLinker, loginUriProvider, internalHostApplication, xsrfTokenAccessor, xsrfTokenValidator);
        this.applicationLinkService = applicationLinkService;
    }

    protected ApplicationLink getRequiredApplicationLink(final HttpServletRequest request) throws
            AbstractApplinksServlet.NotFoundException, AbstractApplinksServlet.BadRequestException {
        return AuthServletUtils.getApplicationLink(applicationLinkService, messageFactory, request);
    }

    @Override
    protected List<String> getRequiredWebResources() {
        return singletonList("com.atlassian.applinks.applinks-plugin:auth-config-css");
    }
}
