package com.atlassian.applinks.core.util;

import com.atlassian.sal.api.net.Response;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Redirect helper responsible for determining if a redirect should be followed
 * and where the next location is.
 *
 * @since 3.11.0
 */
public class RedirectHelper {
    private static final Logger log = LoggerFactory.getLogger(RedirectHelper.class);
    private static final int MAX_REDIRECTS = 3;

    private int redirects = 0;
    private String url;

    public RedirectHelper(String url) {
        this.url = url;
    }

    public String getNextRedirectLocation(final Response response) {
        final String location = response.getHeader("location");
        //Make sure the URL is absolute
        if (UriBuilder.fromUri(location).build().isAbsolute()) {
            url = location;
        } else {
            URI uri = UriBuilder.fromUri(url).build();
            StringBuilder builder = new StringBuilder(uri.getScheme())
                    .append("://")
                    .append(uri.getHost());
            if (isCustomPort(uri.getScheme(), uri.getPort())) {
                builder.append(":").append(uri.getPort());
            }
            url = builder.append(StringUtils.prependIfMissing(location, "/")).toString();
        }

        redirects++;
        return url;
    }

    public boolean responseShouldRedirect(final Response response) {
        return isRedirectStatusCode(response) && hasLocation(response) && notExceededMaximumRedirects();
    }

    private boolean hasLocation(final Response response) {
        final String location = response.getHeader("location");
        if (isBlank(location)) {
            log.warn("HTTP response returned redirect code {} but did not provide a location header",
                    response.getStatusCode());
        }

        return isNotBlank(location);
    }

    private boolean isCustomPort(String scheme, int port) {
        if (port == -1 || port == 80 && "http".equalsIgnoreCase(scheme) ||
                port == 443 && "https".equalsIgnoreCase(scheme)) {
            return false;
        }
        return true;
    }

    private boolean isRedirectStatusCode(final Response response) {
        return (response.getStatusCode() >= 300 && response.getStatusCode() < 400);
    }

    private boolean notExceededMaximumRedirects() {
        if (redirects >= MAX_REDIRECTS) {
            log.warn("Maximum of {} redirects reached", MAX_REDIRECTS);
            return false;
        }

        return true;
    }
}
