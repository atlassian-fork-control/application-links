package com.atlassian.applinks.core;

import com.atlassian.applinks.internal.rest.RestUrl;

/**
 * @version 3.0
 */
public final class ServletPathConstants {
    private ServletPathConstants() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    public static final RestUrl APPLINKS_SERVLETS_URL = RestUrl.forPath("plugins")
            .add("servlet")
            .add("applinks");

    public static final RestUrl APPLINKS_CONFIG_SERVLET_URL = APPLINKS_SERVLETS_URL
            .add("auth")
            .add("conf");

    public static final String APPLINKS_SERVLETS_PATH = "/" + APPLINKS_SERVLETS_URL.toString();
    public static final String APPLINKS_CONFIG_SERVLET_PATH = "/" + APPLINKS_CONFIG_SERVLET_URL.toString();
}
