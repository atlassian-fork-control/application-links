package com.atlassian.applinks.core.rest.model;

import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a list of configured Consumers for an ApplicationLink
 *
 * @since v3.12
 */
@XmlRootElement(name = "consumers")
public class ConsumerEntityListEntity {
    @XmlElement(name = "consumers")
    private List<? extends ConsumerEntity> consumers;

    public ConsumerEntityListEntity() {
    }

    public ConsumerEntityListEntity(List<ConsumerEntity> consumers) {
        this.consumers = consumers;
    }

    @Nullable
    public List<? extends ConsumerEntity> getConsumers() {
        return consumers;
    }
}