package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.ui.AbstractApplinksServlet;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import javax.servlet.http.HttpServletRequest;

final class AuthServletUtils {
    private AuthServletUtils() {
    }

    private static final Logger logger = LoggerFactory.getLogger(AuthServletUtils.class);

    /**
     * Extracts the {@link ApplicationLink} from the request url of auth servlet.
     *
     * @param applicationLinkService application link service
     * @param messageFactory         message factory
     * @param request                http request
     * @return application link, never null
     * @throws AbstractApplinksServlet.NotFoundException   if the link doesn't exist
     * @throws AbstractApplinksServlet.BadRequestException if the request url is not of auth servlet
     */
    protected static ApplicationLink getApplicationLink(final ApplicationLinkService applicationLinkService,
                                                        final MessageFactory messageFactory,
                                                        final HttpServletRequest request)
            throws AbstractApplinksServlet.NotFoundException, AbstractApplinksServlet.BadRequestException {
        // remove any excess slashes
        final String pathInfo = URI.create(request.getPathInfo()).normalize().toString();
        final String[] elements = StringUtils.split(pathInfo, '/');
        if (elements.length > 0) {
            final ApplicationId id = new ApplicationId(elements[0]);
            try {
                final ApplicationLink link = applicationLinkService.getApplicationLink(id);
                if (link != null) {
                    return link;
                } else {
                    final AbstractApplinksServlet.NotFoundException exception = new AbstractApplinksServlet.NotFoundException();
                    exception.setTemplate("com/atlassian/applinks/ui/auth/applink-missing.vm");
                    throw exception;
                }
            } catch (TypeNotInstalledException e) {
                logger.warn(String.format("Unable to load ApplicationLink %s due to uninstalled type definition (%s).", id.toString(), e.getType()), e);
            }
            throw new AbstractApplinksServlet.NotFoundException(messageFactory.newI18nMessage("auth.config.applink.notfound", id.toString()));
        }
        throw new AbstractApplinksServlet.BadRequestException(messageFactory.newI18nMessage("auth.config.applinkpath.missing"));
    }
}