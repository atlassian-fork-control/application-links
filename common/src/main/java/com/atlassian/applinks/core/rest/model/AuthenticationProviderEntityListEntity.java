package com.atlassian.applinks.core.rest.model;

import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a list of registered Authentication providers for an ApplicationLink
 *
 * @since v3.12
 */
@XmlRootElement(name = "authenticationProviders")
public class AuthenticationProviderEntityListEntity {
    @XmlElement(name = "authenticationProviders")
    private List<AuthenticationProviderEntity> authenticationProviders;

    public AuthenticationProviderEntityListEntity() {
    }

    public AuthenticationProviderEntityListEntity(List<AuthenticationProviderEntity> authenticationProviders) {
        this.authenticationProviders = authenticationProviders;
    }

    @Nullable
    public List<AuthenticationProviderEntity> getAuthenticationProviders() {
        return authenticationProviders;
    }
}