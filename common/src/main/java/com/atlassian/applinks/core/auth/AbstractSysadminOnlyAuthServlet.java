package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.ui.AbstractAppLinksSysadminOnlyServlet;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static java.util.Collections.singletonList;

public abstract class AbstractSysadminOnlyAuthServlet extends AbstractAppLinksSysadminOnlyServlet {
    public static String HOST_URL_PARAM = "hostUrl";
    private final ApplicationLinkService applicationLinkService;

    protected AbstractSysadminOnlyAuthServlet(final I18nResolver i18nResolver,
                                              final MessageFactory messageFactory,
                                              final TemplateRenderer templateRenderer,
                                              final WebResourceManager webResourceManager,
                                              final ApplicationLinkService applicationLinkService,
                                              final AdminUIAuthenticator adminUIAuthenticator,
                                              final DocumentationLinker documentationLinker,
                                              final LoginUriProvider loginUriProvider,
                                              final InternalHostApplication internalHostApplication,
                                              final XsrfTokenAccessor xsrfTokenAccessor,
                                              final XsrfTokenValidator xsrfTokenValidator,
                                              final UserManager userManager) {
        super(i18nResolver, messageFactory, templateRenderer, webResourceManager, adminUIAuthenticator,
                documentationLinker, loginUriProvider, internalHostApplication,
                xsrfTokenAccessor, xsrfTokenValidator, userManager);
        this.applicationLinkService = applicationLinkService;
    }

    protected ApplicationLink getRequiredApplicationLink(final HttpServletRequest request) throws
            NotFoundException, BadRequestException {
        return AuthServletUtils.getApplicationLink(applicationLinkService, messageFactory, request);
    }

    @Override
    protected List<String> getRequiredWebResources() {
        return singletonList("com.atlassian.applinks.applinks-plugin:auth-config-css");
    }
}
