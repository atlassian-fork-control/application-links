package com.atlassian.applinks.core.auth;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;

import com.google.common.base.Preconditions;

import static java.util.Objects.requireNonNull;

/**
 * @since v3.0
 */
public class ApplicationLinkRequestAdaptor implements ApplicationLinkRequest {
    private Request request;

    public ApplicationLinkRequestAdaptor(Request request) {
        this.request = requireNonNull(request, "request");
    }

    public ApplicationLinkRequest addHeader(String headerName, String headerValue) {
        return setDelegate(request.addHeader(headerName, headerValue));
    }

    public ApplicationLinkRequest addRequestParameters(final String... params) {
        return setDelegate(request.addRequestParameters(params));
    }

    @Override
    public ApplicationLinkRequest addBasicAuthentication(final String hostname, final String username, final String password) {
        return setDelegate(request.addBasicAuthentication(hostname, username, password));
    }

    public String execute()
            throws ResponseException {
        return request.execute();
    }

    public void execute(final ResponseHandler responseHandler)
            throws ResponseException {
        request.execute(responseHandler);
    }

    @Override
    public <RET> RET executeAndReturn(final ReturningResponseHandler<? super Response, RET> responseHandler)
            throws ResponseException {
        return (RET) request.executeAndReturn(responseHandler);
    }

    public <R> R execute(final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler)
            throws ResponseException {
        return (R) request.executeAndReturn(applicationLinkResponseHandler);
    }

    public Map<String, List<String>> getHeaders() {
        return request.getHeaders();
    }

    public ApplicationLinkRequest setConnectionTimeout(final int connectionTimeout) {
        return setDelegate(request.setConnectionTimeout(connectionTimeout));
    }

    public ApplicationLinkRequest setEntity(final Object entity) {
        return setDelegate(request.setEntity(entity));
    }

    public ApplicationLinkRequest setHeader(final String headerName, final String headerValue) {
        return setDelegate(request.setHeader(headerName, headerValue));
    }

    public ApplicationLinkRequest setRequestBody(String requestBody) {
        return setDelegate(request.setRequestBody(requestBody));
    }

    public ApplicationLinkRequest setRequestBody(final String requestBody, final String contentType) {
        return setDelegate(request.setRequestBody(requestBody, contentType));
    }

    public ApplicationLinkRequest setFiles(List<RequestFilePart> files) {
        return setDelegate(request.setFiles(files));
    }

    public ApplicationLinkRequest setSoTimeout(int soTimeout) {
        return setDelegate(request.setSoTimeout(soTimeout));
    }

    public ApplicationLinkRequest setUrl(String url) {
        return setDelegate(request.setUrl(url));
    }

    private ApplicationLinkRequest setDelegate(Request request) {
        this.request = requireNonNull(request, "Method chaining response");
        return this;
    }

    public ApplicationLinkRequest setFollowRedirects(boolean follow) {
        return setDelegate(request.setFollowRedirects(follow));
    }
}
