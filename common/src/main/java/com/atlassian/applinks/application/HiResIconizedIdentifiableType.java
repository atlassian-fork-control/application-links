package com.atlassian.applinks.application;

import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.spi.application.IconizedType;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import java.net.URI;
import java.net.URISyntaxException;
import javax.annotation.Nullable;

/**
 * Provides default implementation of the {@link IconizedType} SPI.
 *
 * @since 4.3
 */
public abstract class HiResIconizedIdentifiableType extends IconizedIdentifiableType implements IconizedType {
    public HiResIconizedIdentifiableType(AppLinkPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider) {
        super(pluginUtil, webResourceUrlProvider);
    }

    @Nullable
    public URI getIconUri() {
        try {
            return new URI(webResourceUrlProvider.getStaticPluginResourceUrl(pluginUtil.getPluginKey() +
                    ":applinks-images", "images", UrlMode.ABSOLUTE)
                    + "/config/logos/128x128/128" + getIconKey() + ".png");
        } catch (URISyntaxException e) {
            LOG.warn("Unable to find the icon for this application type.", e);
            return null;
        }
    }
}
