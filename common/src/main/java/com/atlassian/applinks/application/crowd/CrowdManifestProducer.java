package com.atlassian.applinks.application.crowd;

import java.util.Set;

import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.core.manifest.AppLinksManifestDownloader;
import com.atlassian.applinks.core.manifest.AppLinksManifestProducer;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;

import com.google.common.collect.ImmutableSet;

/**
 * Manifest Producer for Crowd
 *
 * @since v3.11
 */
public class CrowdManifestProducer extends AppLinksManifestProducer {
    protected CrowdManifestProducer(final RequestFactory<Request<Request<?, Response>, Response>> requestFactory, final AppLinksManifestDownloader downloader, final WebResourceManager webResourceManager, final AppLinkPluginUtil AppLinkPluginUtil) {
        super(requestFactory, downloader, webResourceManager, AppLinkPluginUtil);
    }

    @Override
    protected TypeId getApplicationTypeId() {
        return CrowdApplicationTypeImpl.TYPE_ID;
    }

    @Override
    protected String getApplicationName() {
        return "Crowd";
    }

    @Override
    protected Set<Class<? extends AuthenticationProvider>> getSupportedInboundAuthenticationTypes() {
        return ImmutableSet.of(
                BasicAuthenticationProvider.class,
                TrustedAppsAuthenticationProvider.class,
                OAuthAuthenticationProvider.class
        );
    }

    @Override
    protected Set<Class<? extends AuthenticationProvider>> getSupportedOutboundAuthenticationTypes() {
        return ImmutableSet.of(
                BasicAuthenticationProvider.class,
                TrustedAppsAuthenticationProvider.class,
                OAuthAuthenticationProvider.class
        );
    }
}
