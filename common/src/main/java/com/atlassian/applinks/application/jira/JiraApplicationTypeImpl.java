package com.atlassian.applinks.application.jira;

import com.atlassian.applinks.api.ApplicationTypeVisitor;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.application.HiResIconizedIdentifiableType;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Used for both:
 * <ul>
 * <li>a UAL-enabled JIRA instance; and</li>
 * <li>a JIRA instance that predates UAL but does has have Trusted Applications support.</li>
 * </ul>
 *
 * @since v3.0
 */
public class JiraApplicationTypeImpl extends HiResIconizedIdentifiableType
        implements JiraApplicationType, NonAppLinksApplicationType, BuiltinApplinksType {
    static final TypeId TYPE_ID = new TypeId("jira");

    public JiraApplicationTypeImpl(AppLinkPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider) {
        super(pluginUtil, webResourceUrlProvider);
    }

    @Nonnull
    public TypeId getId() {
        return TYPE_ID;
    }

    @Nonnull
    public String getI18nKey() {
        return "applinks.jira";
    }

    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplicationTypeVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
