package com.atlassian.applinks.ui.auth;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.ui.XsrfProtectedServlet;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserRole;

/**
 * Servlet filter for urls that require sysadmin privilege to access.
 *
 * @since 3.10
 */
public class SysAdminFilter extends AdminFilter {
    private I18nResolver i18nResolver;

    public SysAdminFilter(AdminUIAuthenticator uiAuthenticator, I18nResolver i18nResolver, LoginUriProvider loginUriProvider, ApplicationProperties applicationProperties) {
        super(uiAuthenticator, loginUriProvider, applicationProperties);
        this.i18nResolver = i18nResolver;
    }

    @Override
    UserRole getForRole() {
        return UserRole.SYSADMIN;
    }

    @Override
    boolean checkAccess(String username, String password, AdminUIAuthenticator.SessionHandler sessionHandler) {
        return uiAuthenticator.checkSysadminUIAccessBySessionOrPasswordAndActivateSysadminSession(username, password, sessionHandler);
    }

    @Override
    protected void handleAccessDenied(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // If the request came from application link creation wizard, we must return and error instead of redirection
        // since older AppLink plugins (prior to 3.10) wrongly treats redirection code as successful.
        //
        // HACK: This is the only way we could figure out if the request came from application link creation wizard.
        //       This header is only used in such scenario in AppLinks plugin context.
        if (XsrfProtectedServlet.OVERRIDE_HEADER_VALUE.equals(request.getHeader(XsrfProtectedServlet.OVERRIDE_HEADER_NAME))) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, i18nResolver.getText("applinks.error.only.sysadmin.operation"));
        } else {
            // if the request doesn't come from link creation wizard (most of the time, this will be a request from
            // user's browser), just do the default behavior which is redirecting to a login page.
            super.handleAccessDenied(request, response);
        }
    }
}