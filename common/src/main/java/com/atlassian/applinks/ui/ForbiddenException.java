package com.atlassian.applinks.ui;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.core.util.Message;

public class ForbiddenException extends RequestException {

    public ForbiddenException(final Message message) {
        super(HttpServletResponse.SC_FORBIDDEN, message);
    }
}