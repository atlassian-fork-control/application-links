package com.atlassian.applinks.ui;

import com.atlassian.applinks.core.util.Message;

public class RequestException extends RuntimeException {
    private final int status;
    private final Message message;
    protected String template;

    public RequestException(final int status, final Message message, final Throwable cause) {
        super(cause);
        this.message = message;
        this.status = status;
    }

    public RequestException(final int status, final Message message) {
        this.message = message;
        this.status = status;
    }

    public RequestException(final int status) {
        this(status, null);
    }

    public int getStatus() {
        return status;
    }

    /**
     * @return the name of the velocity template that should be rendered
     * when this exception is thrown. When {@code null} is returned, the
     * default template is used.
     */
    public String getTemplate() {
        return template;
    }

    public void setTemplate(final String template) {
        this.template = template;
    }

    public String getMessage() {
        return message == null ? null : message.toString();
    }
}

