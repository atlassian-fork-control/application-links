package com.atlassian.applinks.ui;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.core.util.RendererContextBuilder;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.sal.api.message.I18nResolver;

public final class RenderContextBuilderUtils {
    public static RendererContextBuilder createContextBuilder(final ApplicationLink applicationLink, final I18nResolver i18nResolver, final InternalHostApplication internalHostApplication) {
        final RendererContextBuilder builder = new RendererContextBuilder()
                .put("localApplicationName", internalHostApplication.getName())
                .put("localApplicationType", i18nResolver.getText(internalHostApplication.getType().getI18nKey()))
                .put("remoteApplicationName", applicationLink.getName())
                .put("remoteApplicationType", i18nResolver.getText(applicationLink.getType().getI18nKey()));
        return builder;
    }

}
