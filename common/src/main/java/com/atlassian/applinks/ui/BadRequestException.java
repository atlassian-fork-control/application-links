package com.atlassian.applinks.ui;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.core.util.Message;

public class BadRequestException extends RequestException {
    public BadRequestException() {
        this(null);
    }

    public BadRequestException(final Message message) {
        super(HttpServletResponse.SC_BAD_REQUEST, message);
    }

    public BadRequestException(final Message message, final Throwable cause) {
        super(HttpServletResponse.SC_BAD_REQUEST, message, cause);
    }
}