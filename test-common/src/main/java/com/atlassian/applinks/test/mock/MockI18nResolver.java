package com.atlassian.applinks.test.mock;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.core.message.AbstractI18nResolver;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

/**
 * Mock implementation of {@link I18nResolver} that just returns keys (and optionally appended args).
 * <br>
 * Use Mockito's {@code @Spy} to override the methods left unimplemented.
 *
 * @since 4.3
 */
public class MockI18nResolver extends AbstractI18nResolver {
    private final boolean appendArgs;

    public MockI18nResolver() {
        this(false);
    }

    public MockI18nResolver(boolean appendArgs) {
        this.appendArgs = appendArgs;
    }

    @Override
    public String resolveText(String key, Serializable[] arguments) {
        if (appendArgs) {
            return key + stream(arguments).map(Object::toString).collect(joining(",", "[", "]"));
        } else {
            return key;
        }
    }

    @Override
    public String resolveText(Locale locale, String key, Serializable[] arguments) {
        return resolveText(key, arguments);
    }

    @Override
    public String getRawText(String key) {
        return key;
    }

    @Override
    public String getRawText(Locale locale, String key) {
        return key;
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(String prefix) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(String prefix, Locale locale) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
