package com.atlassian.applinks.test.mock;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class MockApplicationLinkRequest implements ApplicationLinkRequest {
    private final Request.MethodType methodType;
    private String url;

    private String requestBody;
    private Object requestEntity;
    private String requestContentType;

    private final Map<String, List<String>> headers = Maps.newHashMap();
    private final List<String> requestParameters = Lists.newArrayList();

    private int connectionTimeout;
    private int soTimeout;

    private String basicHost;
    private String basicUser;
    private String basicPassword;

    private boolean followRedirects = true;

    private MockApplicationLinkResponse response;

    public MockApplicationLinkRequest(final MethodType methodType, final String url) {
        this.methodType = methodType;
        this.url = url;
    }

    public MockApplicationLinkRequest setConnectionTimeout(final int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
        return this;
    }

    public MockApplicationLinkRequest setSoTimeout(final int soTimeout) {
        this.soTimeout = soTimeout;
        return this;
    }

    public MockApplicationLinkRequest setUrl(final String url) {
        this.url = url;
        return this;
    }

    public MockApplicationLinkRequest setRequestBody(final String requestBody) {
        this.requestBody = requestBody;
        return this;
    }

    @Override
    public ApplicationLinkRequest setRequestBody(String requestBody, String contentType) {
        this.requestBody = requestBody;
        this.requestContentType = contentType;

        return this;
    }

    public MockApplicationLinkRequest setFiles(final List<RequestFilePart> files) {
        throw new UnsupportedOperationException();
    }

    public MockApplicationLinkRequest setEntity(Object entity) {
        this.requestEntity = entity;
        return this;
    }

    public MockApplicationLinkRequest addRequestParameters(final String... params) {
        requestParameters.addAll(Arrays.asList(params));
        return this;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    public MockApplicationLinkRequest addHeader(final String headerName, final String headerValue) {
        List<String> list = headers.get(headerName);
        if (list == null) {
            list = new ArrayList<>();
            headers.put(headerName, list);
        }
        list.add(headerValue);
        return this;
    }

    public MockApplicationLinkRequest setHeader(final String headerName, final String headerValue) {
        headers.put(headerName, new ArrayList<>(Arrays.asList(headerValue)));
        return this;
    }

    public MockApplicationLinkRequest setFollowRedirects(boolean follow) {
        this.followRedirects = follow;
        return this;
    }

    public MockApplicationLinkRequest addBasicAuthentication(String host, String username, String password) {
        basicHost = host;
        basicUser = username;
        basicPassword = password;
        return this;
    }

    public MockApplicationLinkRequest setResponse(MockApplicationLinkResponse response) {
        this.response = response;
        return this;
    }

    public void execute(final ResponseHandler<? super Response> responseHandler) throws ResponseException {
        checkResponse();
        responseHandler.handle(response);
    }

    public String execute() throws ResponseException {
        checkResponse();
        return response.getResponseBodyAsString();
    }

    @Override
    public <R> R executeAndReturn(ReturningResponseHandler<? super Response, R> responseHandler) throws ResponseException {
        checkResponse();
        return responseHandler.handle(response);
    }

    @Override
    public <R> R execute(ApplicationLinkResponseHandler<R> responseHandler) throws ResponseException {
        checkResponse();
        if (response.isCredentialsRequired()) {
            return responseHandler.credentialsRequired(response);
        } else {
            return responseHandler.handle(response);
        }
    }

    public MethodType getMethodType() {
        return methodType;
    }

    public String getUrl() {
        return url;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public int getSoTimeout() {
        return soTimeout;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public String getRequestContentType() {
        return requestContentType;
    }

    public Object getEntity() {
        return requestEntity;
    }

    public List<String> getRequestParameters() {
        return requestParameters;
    }

    public List<String> getHeader(String headerName) {
        return headers.get(headerName);
    }

    public String getBasicHost() {
        return basicHost;
    }

    public String getBasicUser() {
        return basicUser;
    }

    public String getBasicPassword() {
        return basicPassword;
    }

    public boolean isFollowRedirects() {
        return followRedirects;
    }

    public Response getResponse() {
        return response;
    }

    private void checkResponse() throws ResponseException {
        if (response == null) {
            response = new MockApplicationLinkResponse();
        }
        if (response.getResponseException() != null) {
            throw response.getResponseException();
        }
    }
}
