package com.atlassian.applinks.test.mock;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.TypeId;
import org.osgi.framework.Version;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.util.Set;

public class MockManifest implements Manifest {
    private ApplicationId id;
    private String version;
    private Version applinksVersion;

    public MockManifest() {
    }

    public MockManifest(ApplicationId id) {
        this.id = id;
    }

    public MockManifest(TestApplinkIds id) {
        this(id.applicationId());
    }

    @Override
    public ApplicationId getId() {
        return id;
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public TypeId getTypeId() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nullable
    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public Long getBuildNumber() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public URI getUrl() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nullable
    @Override
    public URI getIconUrl() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nullable
    @Override
    public URI getIconUri() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nullable
    @Override
    public Version getAppLinksVersion() {
        return applinksVersion;
    }

    @Override
    public Set<Class<? extends AuthenticationProvider>> getInboundAuthenticationTypes() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<Class<? extends AuthenticationProvider>> getOutboundAuthenticationTypes() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Boolean hasPublicSignup() {
        throw new UnsupportedOperationException("Not implemented");
    }

    public MockManifest setId(@Nullable ApplicationId id) {
        this.id = id;
        return this;
    }

    public MockManifest setVersion(@Nullable String version) {
        this.version = version;
        return this;
    }

    public MockManifest setApplinksVersion(@Nullable Version applinksVersion) {
        this.applinksVersion = applinksVersion;
        return this;
    }

    public MockManifest setApplinksVersion(@Nonnull String applinksVersion) {
        this.applinksVersion = new Version(applinksVersion);
        return this;
    }
}
