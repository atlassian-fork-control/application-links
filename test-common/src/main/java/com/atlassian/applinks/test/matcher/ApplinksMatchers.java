package com.atlassian.applinks.test.matcher;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import java.util.function.Function;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;
import static java.util.stream.StreamSupport.stream;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

/**
 * Extra Hamcrest matchers for Applinks
 *
 * @since 5.0
 */
public final class ApplinksMatchers {
    private ApplinksMatchers() {
        // do not instantiate
    }

    @Nonnull
    public static <T> Function<T, Matcher<T>> asMatcher() {
        return Matchers::is;
    }

    @Nonnull
    public static <T> Function<T, Matcher<T>> asMatcher(@Nonnull Class<T> valueClass) {
        requireNonNull(valueClass);
        return asMatcher();
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <T> Matcher<? super T>[] toMatchers(@Nonnull Iterable<T> objects) {
        return stream(objects.spliterator(), false).map(asMatcher()).toArray(Matcher[]::new);
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <T> Matcher<Object> instanceOfMatching(@Nonnull Class<T> targetType, @Nonnull Matcher<T> matcher) {
        requireNonNull(targetType, "targetType");
        requireNonNull(matcher, "matcher");

        return (Matcher) allOf(instanceOf(targetType), matcher);
    }

    @Nonnull
    public static <T, U> FeatureMatcher<T, U> newFeatureMatcher(@Nonnull String name, @Nullable String description,
                                                                @Nonnull Matcher<? super U> subMatcher,
                                                                @Nonnull Function<T, U> getFeature) {
        requireNonNull(name, "name");
        requireNonNull(subMatcher, "subMatcher");
        requireNonNull(getFeature, "getFeature");

        return new FeatureMatcher<T, U>(subMatcher, name, description) {
            @Override
            protected U featureValueOf(T actual) {
                return getFeature.apply(actual);
            }
        };
    }

    @Nonnull
    public static <T, U> FeatureMatcher<T, U> newFeatureMatcher(@Nonnull String name,
                                                                @Nonnull Matcher<? super U> subMatcher,
                                                                @Nonnull Function<T, U> getFeature) {
        return newFeatureMatcher(name, name, subMatcher, getFeature);
    }
}
