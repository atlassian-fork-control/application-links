package com.atlassian.applinks.test.matcher;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.hamcrest.Matchers.is;

/**
 * @since 5.0
 */
public final class ThrowableMatchers {
    private ThrowableMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static <E extends Throwable> Matcher<E> withMessageThat(@Nonnull Matcher<String> messageMatcher) {
        return new FeatureMatcher<E, String>(messageMatcher, "message that", "message") {
            @Override
            protected String featureValueOf(Throwable error) {
                return error.getMessage();
            }
        };
    }

    @Nonnull
    public static <E extends Throwable> Matcher<E> withMessage(@Nullable String expectedMessage) {
        return withMessageThat(is(expectedMessage));
    }
}
