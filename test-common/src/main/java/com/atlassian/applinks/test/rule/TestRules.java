package com.atlassian.applinks.test.rule;

import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public class TestRules {
    private TestRules() {
    }

    @Nonnull
    public static RuleChain toRuleChain(@Nonnull Iterable<TestRule> rules) {
        requireNonNull(rules, "rules");

        RuleChain ruleChain = RuleChain.emptyRuleChain();
        for (TestRule rule : rules) {
            ruleChain.around(rule);
        }

        return ruleChain;
    }
}
