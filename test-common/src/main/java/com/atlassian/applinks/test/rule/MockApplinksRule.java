package com.atlassian.applinks.test.rule;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.test.mock.MockApplink;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.mockito.Mock;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.Set;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Predicates.not;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isNoneEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.reflections.ReflectionUtils.getFields;
import static org.reflections.ReflectionUtils.withAnnotation;
import static org.reflections.ReflectionUtils.withTypeAssignableTo;

/**
 * Test rule that initializes mock application links for unit tests.
 *
 * @since 4.3
 */
public class MockApplinksRule extends TestWatcher {
    private final Object test;

    public MockApplinksRule(@Nonnull Object test) {
        this.test = requireNonNull(test, "test");
    }

    @Override
    protected void starting(Description description) {
        try {
            processMockitoMocks();
            processApplinkMocks();
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Unable to initialize application link mocks", e);
        }
    }

    private void processMockitoMocks() throws IllegalAccessException {
        // go through @Mock fields and "enhance" mock applinks injected by Mockito
        for (Field candidate : getMockitoInjectedApplinkFields()) {
            candidate.setAccessible(true);
            ReadOnlyApplicationLink mockValue = (ReadOnlyApplicationLink) candidate.get(test);
            requireNonNull(mockValue, format("'%s' field value was null, but should have been injected by Mockito. " +
                    "Did you forget to use MockitoJUnitRunner?", candidate.getName()));
            stub(mockValue, candidate.getAnnotation(MockApplink.class));
        }
    }

    @SuppressWarnings("unchecked")
    private void processApplinkMocks() throws IllegalAccessException {
        // go through @MockApplink fields that were not processed by Mockito
        for (Field candidate : getMockApplinkOnlyFields()) {
            Class<? extends ReadOnlyApplicationLink> targetType =
                    (Class<? extends ReadOnlyApplicationLink>) candidate.getType();
            checkState(targetType.isInterface(), format("Type %s of field %s is not an interface, you can only annotate " +
                    "interface types with @MockApplink", targetType.getName(), candidate.getName()));

            ReadOnlyApplicationLink mockValue = mock(targetType);
            candidate.setAccessible(true);
            candidate.set(test, mockValue);
            stub(mockValue, candidate.getAnnotation(MockApplink.class));
        }
    }

    @SuppressWarnings("unchecked")
    private Set<Field> getMockitoInjectedApplinkFields() {
        // ReadOnlyApplicationLink (and all assignable types) fields annotated with @Mock, which means Mockito has
        // already injected mocks into them
        return getFields(test.getClass(),
                withTypeAssignableTo(ReadOnlyApplicationLink.class), withAnnotation(Mock.class));
    }

    @SuppressWarnings("unchecked")
    private Set<Field> getMockApplinkOnlyFields() {
        // ReadOnlyApplicationLink (and all assignable types) fields annotated with @MockApplink but not @Mock - hence
        // not processed by Mockito
        return getFields(test.getClass(),
                withTypeAssignableTo(ReadOnlyApplicationLink.class),
                withAnnotation(MockApplink.class),
                not(withAnnotation(Mock.class))
        );
    }

    private static void stub(ReadOnlyApplicationLink mockValue, MockApplink mockApplink) {
        String id = getId(mockApplink);
        ApplicationType applicationType = getType(mockApplink);

        when(mockValue.getId()).thenReturn(new ApplicationId(id));
        when(mockValue.getName()).thenReturn(getName(mockApplink, id));
        when(mockValue.getDisplayUrl()).thenReturn(URI.create(getUrl(mockApplink, id)));
        when(mockValue.getRpcUrl()).thenReturn(URI.create(getRpcUrl(mockApplink, id)));
        when(mockValue.getType()).thenReturn(applicationType);
    }

    private static String getId(MockApplink mockApplink) {
        return mockApplink != null ? mockApplink.id().id() : UUID.randomUUID().toString();
    }

    private static String getName(MockApplink mockApplink, String id) {
        return (mockApplink != null && isNotEmpty(mockApplink.name())) ? mockApplink.name()
                : "Test applink " + id;
    }

    private static String getUrl(MockApplink mockApplink, String id) {
        return (mockApplink != null && isNotBlank(mockApplink.url())) ? mockApplink.url()
                : "http://" + id;
    }

    private static String getRpcUrl(MockApplink mockApplink, String id) {
        if (mockApplink != null && isNotEmpty(mockApplink.rpcUrl())) {
            return mockApplink.rpcUrl();
        } else if (mockApplink != null && isNoneEmpty(mockApplink.url())) {
            return mockApplink.url();
        } else {
            return "http://" + id;
        }
    }

    private static ApplicationType getType(MockApplink mockApplink) {
        if (mockApplink != null) {
            return mock(mockApplink.applicationType());
        } else {
            return mock(ApplicationType.class);
        }
    }
}
