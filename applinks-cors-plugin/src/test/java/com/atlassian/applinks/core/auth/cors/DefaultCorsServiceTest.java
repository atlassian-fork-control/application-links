package com.atlassian.applinks.core.auth.cors;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.auth.types.CorsAuthenticationProvider;
import com.atlassian.applinks.cors.auth.DefaultCorsService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultCorsServiceTest {
    private static final Map<String, String> ALLOWED = ImmutableMap.of(DefaultCorsService.KEY_ALLOWS_CREDENTIALS, "true");
    private static final URI ATLASSIAN_COM = URI.create("http://www.atlassian.com");
    private static final URI CONFLUENCE = URI.create("http://localhost:9090/confluence");
    private static final Map<String, String> DISALLOWED = ImmutableMap.of(DefaultCorsService.KEY_ALLOWS_CREDENTIALS, "false");
    private static final URI EXAMPLE_ORG = URI.create("http://example.org");
    private static final String ID = UUID.randomUUID().toString();
    private static final URI JIRA = URI.create("http://localhost:9090/jira");
    private static final URI UNI_EDU = URI.create("http://www.uni.edu:8080");

    @Mock
    private ApplicationLinkService applicationLinkService;
    @Mock
    private AuthenticationConfigurationManager authenticationConfigurationManager;
    @InjectMocks
    private DefaultCorsService corsService;

    @Test
    @SuppressWarnings("unchecked")
    public void testAllowsCredentials() {
        ApplicationId atlassianId = new ApplicationId(ID);

        ApplicationLink atlassian = mock(ApplicationLink.class);
        when(atlassian.getId()).thenReturn(atlassianId);

        when(authenticationConfigurationManager.getConfiguration(same(atlassianId), same(CorsAuthenticationProvider.class)))
                .thenReturn(ALLOWED, DISALLOWED, null);

        assertTrue(corsService.allowsCredentials(atlassian));
        assertFalse(corsService.allowsCredentials(atlassian));
        assertFalse(corsService.allowsCredentials(atlassian));
    }

    @Test
    public void testDisableCredentials() {
        ApplicationId atlassianId = new ApplicationId(ID);

        ApplicationLink atlassian = mock(ApplicationLink.class);
        when(atlassian.getId()).thenReturn(atlassianId);

        corsService.disableCredentials(atlassian);

        verify(authenticationConfigurationManager).unregisterProvider(same(atlassianId), same(CorsAuthenticationProvider.class));
    }

    @Test
    public void testEnableCredentials() {
        ApplicationId atlassianId = new ApplicationId(ID);

        ApplicationLink atlassian = mock(ApplicationLink.class);
        when(atlassian.getId()).thenReturn(atlassianId);

        corsService.enableCredentials(atlassian);

        verify(authenticationConfigurationManager).registerProvider(same(atlassianId), same(CorsAuthenticationProvider.class),
                argThat(configuration -> configuration != null &&
                        configuration.size() == 1 &&
                        "true".equals(configuration.get(DefaultCorsService.KEY_ALLOWS_CREDENTIALS))));
    }

    @Test
    public void testGetApplicationLinksByOrigin() {
        ApplicationLink atlassian = mock(ApplicationLink.class);
        when(atlassian.getRpcUrl()).thenReturn(ATLASSIAN_COM);

        ApplicationLink example = mock(ApplicationLink.class);
        when(example.getRpcUrl()).thenReturn(EXAMPLE_ORG);

        ApplicationLink uni = mock(ApplicationLink.class);
        when(uni.getRpcUrl()).thenReturn(UNI_EDU);

        when(applicationLinkService.getApplicationLinks()).thenReturn(Arrays.asList(atlassian, example, uni));

        assertApplicationLinks(corsService.getApplicationLinksByOrigin("http://www.atlassian.com"), atlassian); //Implicit port
        assertApplicationLinks(corsService.getApplicationLinksByOrigin("http://www.atlassian.com:80"), atlassian); //Explicit default port
        assertApplicationLinks(corsService.getApplicationLinksByOrigin("http://www.atlassian.com:8080")); //Explicit mismatched port
        assertApplicationLinks(corsService.getApplicationLinksByOrigin("http://example.org/test"), example); //Context information is ignored
        assertApplicationLinks(corsService.getApplicationLinksByOrigin("http://www.uni.edu:8080"), uni); //Explicit port match
        assertApplicationLinks(corsService.getApplicationLinksByOrigin("http://www.uni.edu")); //Implicit port doesn't match
        assertApplicationLinks(corsService.getApplicationLinksByOrigin("http://somehost.xxx")); //Host is not found at all
    }

    //Electron (for example) sends an invalid URI ("file://") for its "Origin" header. Rather than throwing
    //an exception complaining that the URI is invalid, getApplicationLinksByOrigin should return an empty
    //collection (since it's not possible for an invalid URI to match any links).
    @Test
    public void testGetApplicationLinksByOriginHandlesInvalidUris() {
        assertApplicationLinks(corsService.getApplicationLinksByOrigin("file://"));

        verifyZeroInteractions(applicationLinkService, authenticationConfigurationManager);
    }

    @Test
    public void testGetApplicationLinksByUri() {
        ApplicationLink confluence = mock(ApplicationLink.class);
        when(confluence.getRpcUrl()).thenReturn(CONFLUENCE);

        ApplicationLink jira = mock(ApplicationLink.class);
        when(jira.getRpcUrl()).thenReturn(JIRA);

        when(applicationLinkService.getApplicationLinks()).thenReturn(Arrays.asList(confluence, jira));

        assertApplicationLinks(corsService.getApplicationLinksByUri(CONFLUENCE), confluence, jira);
        assertApplicationLinks(corsService.getApplicationLinksByUri(JIRA), confluence, jira);
    }

    @Test
    public void testGetRequiredApplicationLinksByOrigin() {
        ApplicationLink atlassian = mock(ApplicationLink.class);
        when(atlassian.getRpcUrl()).thenReturn(ATLASSIAN_COM);

        when(applicationLinkService.getApplicationLinks()).thenReturn(Collections.singletonList(atlassian));

        assertApplicationLinks(corsService.getRequiredApplicationLinksByOrigin(ATLASSIAN_COM.toString()), atlassian);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetRequiredApplicationLinksByOriginThrowsIfNothingMatches() {
        ApplicationLink atlassian = mock(ApplicationLink.class);
        when(atlassian.getRpcUrl()).thenReturn(ATLASSIAN_COM);

        when(applicationLinkService.getApplicationLinks()).thenReturn(Collections.singletonList(atlassian));

        corsService.getRequiredApplicationLinksByOrigin(EXAMPLE_ORG.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetRequiredApplicationLinksByOriginThrowsIfUriIsInvalid() {
        try {
            corsService.getRequiredApplicationLinksByOrigin("file://");
        } finally {
            verifyZeroInteractions(applicationLinkService, authenticationConfigurationManager);
        }
    }

    private static void assertApplicationLinks(Collection<ApplicationLink> actual, ApplicationLink... expected) {
        assertNotNull(actual);
        if (expected == null || expected.length == 0) {
            assertTrue(actual.isEmpty());
        } else {
            assertArrayEquals(expected, actual.toArray());
        }
    }
}
