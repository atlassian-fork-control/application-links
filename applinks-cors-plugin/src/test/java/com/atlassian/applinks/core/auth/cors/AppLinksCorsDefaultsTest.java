package com.atlassian.applinks.core.auth.cors;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.cors.auth.AppLinksCorsDefaults;
import com.atlassian.applinks.cors.auth.CorsService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AppLinksCorsDefaultsTest {
    private static final String ATLASSIAN_COM = "http://www.atlassian.com";
    private static final String ID = UUID.randomUUID().toString();
    private static final String LOCALHOST = "http://localhost:9090";
    private static final String UNI_EDU = "http://www.uni.edu:8080";

    @Mock
    private CorsService corsService;
    @InjectMocks
    private AppLinksCorsDefaults defaults;

    @Test
    public void testAllowsCredentials() {
        ApplicationLink atlassian = mock(ApplicationLink.class);

        when(corsService.allowsCredentials(same(atlassian))).thenReturn(true, false);
        when(corsService.getRequiredApplicationLinksByOrigin(eq(ATLASSIAN_COM)))
                .thenReturn(Collections.singletonList(atlassian));

        assertTrue(defaults.allowsCredentials(ATLASSIAN_COM));
        assertFalse(defaults.allowsCredentials(ATLASSIAN_COM));
    }

    @Test
    public void testAllowsCredentialsWithMultipleLinks() {
        ApplicationLink confluence = mock(ApplicationLink.class);
        ApplicationLink jira = mock(ApplicationLink.class);

        when(corsService.allowsCredentials(same(confluence))).thenReturn(false, true, true);
        when(corsService.allowsCredentials(same(jira))).thenReturn(false, true);
        when(corsService.getRequiredApplicationLinksByOrigin(eq(LOCALHOST)))
                .thenReturn(Arrays.asList(confluence, jira));

        assertFalse(defaults.allowsCredentials(LOCALHOST)); //Confluence says no, no check for JIRA
        assertFalse(defaults.allowsCredentials(LOCALHOST)); //Confluence says yes, JIRA says no
        assertTrue(defaults.allowsCredentials(LOCALHOST)); //Confluence and JIRA both say yes
    }

    @Test
    public void testAllowsOrigin() {
        ApplicationLink atlassian = mock(ApplicationLink.class);

        when(corsService.getApplicationLinksByOrigin(eq(ATLASSIAN_COM))).thenReturn(Collections.singletonList(atlassian));
        when(corsService.getApplicationLinksByOrigin(eq(UNI_EDU))).thenReturn(Collections.emptyList());

        assertTrue(defaults.allowsOrigin(ATLASSIAN_COM)); //Implicit port
        assertFalse(defaults.allowsOrigin(UNI_EDU));
    }

    @Test
    public void testGetAllowedRequestHeaders() {
        ApplicationLink atlassian = mock(ApplicationLink.class);

        when(corsService.getRequiredApplicationLinksByOrigin(eq(ATLASSIAN_COM)))
                .thenReturn(Collections.singletonList(atlassian));
        when(corsService.allowsCredentials(same(atlassian))).thenReturn(Boolean.FALSE, Boolean.TRUE);

        Set<String> headers = defaults.getAllowedRequestHeaders(ATLASSIAN_COM);
        assertNotNull(headers);
        assertTrue(headers.isEmpty());

        headers = defaults.getAllowedRequestHeaders(ATLASSIAN_COM);
        assertNotNull(headers);
        assertEquals(1, headers.size());
        assertEquals("Authorization", headers.iterator().next());
    }

    @Test
    public void testGetAllowedResponseHeaders() {
        ApplicationLink atlassian = mock(ApplicationLink.class);

        when(corsService.getRequiredApplicationLinksByOrigin(eq(ATLASSIAN_COM)))
                .thenReturn(Collections.singletonList(atlassian));

        assertSame(Collections.emptySet(), defaults.getAllowedResponseHeaders(ATLASSIAN_COM));
    }
}
