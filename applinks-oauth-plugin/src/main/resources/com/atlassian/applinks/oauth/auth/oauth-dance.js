AJS.$(document).bind(AppLinks.Event.READY, function() {
    (function($) {
    	var success = ($('.error').length == 0) && (AJS.$("#applink-authorized").val() == "true");

        if (window.opener && window.opener.oauthCallback) {
            if (success) {
                window.opener.oauthCallback.success();
            }
            else {
                $('#continue-link').on('click', function() {
                    window.opener.oauthCallback.failure();
                });
            }
        }

    })(AJS.$)
});
