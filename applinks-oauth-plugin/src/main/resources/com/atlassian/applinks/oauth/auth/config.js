function deleteConsumerInformation(title, message, cancel) {
    if (!cancel) {
        cancel = function() {};
    }
    var popup = new AJS.Dialog({
        width: 450,
        height: 180,
        id: "confirm-dialog",
        onCancel: cancel
    });

    popup.addHeader(title);
    popup.addPanel(title);
    popup.getCurrentPanel().html(message);

    popup.addButton(AJS.I18n.getText('applinks.delete'), function() {
        AJS.$('#oauth-incoming-enabled').val('false');
        popup.remove();
        AJS.$('#add-consumer-manually').submit();
    }, "confirm");
    popup.addButton(AJS.I18n.getText('applinks.cancel'), function() {
        popup.remove();
    }, 'cancel');

    popup.show();
    popup.popup.element.addClass('aui-dialog-content-ready');
    return false;
}

function deleteServiceProviderInformation(title, message, confirm, cancel) {
    if (!cancel) {
        cancel = function() {};
    }
    var popup = new AJS.Dialog({
        width: 450,
        height: 180,
        id: "confirm-dialog",
        onCancel: cancel
    });

    popup.addHeader(title);
    popup.addPanel(title);
    popup.getCurrentPanel().html(message);

    popup.addButton("delete", function() {
        AJS.$("#oauth-outgoing-enabled").val("false");
        popup.remove();
        AJS.$("#add-serviceprovider").submit();
    }, "confirm");
    popup.addButton("cancel", function() {
        popup.remove();
    }, "cancel");

    popup.show();
    popup.popup.element.addClass("aui-dialog-content-ready");
    return false;
}


function enableDisableOAuthInRemoteApp(url)
{
    var enabled = AJS.$('#oauth-incoming-enabled').val();
    var remoteURL = url.replace(/ENABLE_DISABLE_OAUTH_PARAM/g, enabled);
    window.location = remoteURL;
    return false;
}

function enableDisableTwoLeggedOAuthInRemoteApp(url)
{
    var enabled = AJS.$('#oauth-incoming-enabled').val();
    var outgoing2LOEnabled = AJS.$('#outgoing-two-lo-enabled').is(":checked");
    var existingOutgoing2LOState = "true" == AJS.$('#existing-outgoing-2lo-state').val();
    var outgoing2LOiEnabled = AJS.$('#outgoing-two-loi-enabled').is(":checked");
    var existingOutgoing2LOiState = "true" == AJS.$('#existing-outgoing-2loi-state').val();

    var remoteURL;
    remoteURL = url.replace(/ENABLE_DISABLE_OAUTH_PARAM/g, enabled == 'false');
    remoteURL = remoteURL.replace(/ENABLE_DISABLE_OUTGOING_TWO_LEGGED_OAUTH_PARAM/g, outgoing2LOEnabled);
    remoteURL = remoteURL.replace(/ENABLE_DISABLE_OUTGOING_TWO_LEGGED_I_OAUTH_PARAM/g, outgoing2LOiEnabled);

    if(outgoing2LOEnabled != existingOutgoing2LOState
        || outgoing2LOiEnabled != existingOutgoing2LOiState) {
        window.location = remoteURL;
    }

    return false;
}