package com.atlassian.applinks.oauth.auth.twolo.impersonation;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.oauth.auth.twolo.TwoLeggedOAuthRequest;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.annotations.VisibleForTesting;

import static com.atlassian.applinks.core.util.URIUtil.utf8Encode;
import static java.util.Collections.singletonList;

/**
 * Represents a 2-legged OAuth with Impersonation request which shares the common signing mechanism with
 * {@link TwoLeggedOAuthRequest} plus supplying xoauth_requestor_id parameter which is also included
 * in the signature calculation.
 *
 * @since 3.10
 */
public class TwoLeggedOAuthWithImpersonationRequest extends TwoLeggedOAuthRequest {
    /**
     * requestor id parameter for 2LO impersonation.
     */
    @VisibleForTesting
    static final String XOAUTH_REQUESTOR_ID = "xoauth_requestor_id";

    private String username;

    public TwoLeggedOAuthWithImpersonationRequest(final String url,
                                                  final MethodType methodType,
                                                  final Request wrappedRequest,
                                                  final ServiceProvider serviceProvider,
                                                  final ConsumerService consumerService,
                                                  final ApplicationId applicationId,
                                                  final String username) {
        super(url, methodType, wrappedRequest, serviceProvider, consumerService, applicationId);
        this.username = username;
    }

    @Override
    protected void signRequest() throws ResponseException {
        // the username goes as request param, the same as Google https://developers.google.com/google-apps/gmail/oauth_protocol
        wrappedRequest.setUrl(addUsernameToUrl(url));
        // the username must be included in the signature calculation http://oauth.net/core/1.0/#signing_process
        this.parameters.put(XOAUTH_REQUESTOR_ID, singletonList(username));

        super.signRequest();
    }

    private String addUsernameToUrl(String url) {
        if (url.contains("?")) {
            return new StringBuilder().append(url).append("&").append(XOAUTH_REQUESTOR_ID).append("=").append(utf8Encode(username)).toString();
        } else {
            return new StringBuilder().append(url).append("?").append(XOAUTH_REQUESTOR_ID).append("=").append(utf8Encode(username)).toString();
        }
    }
}