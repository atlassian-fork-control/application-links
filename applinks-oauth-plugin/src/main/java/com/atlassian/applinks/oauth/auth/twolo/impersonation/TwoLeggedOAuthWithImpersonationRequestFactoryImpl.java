package com.atlassian.applinks.oauth.auth.twolo.impersonation;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.oauth.auth.ServiceProviderUtil;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;

import java.net.URI;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Request factory for creating 2-Legged OAuth with Impersonation requests. This factory helps setup all the
 * necessary OAuth headers.
 *
 * @since 3.10
 */
public class TwoLeggedOAuthWithImpersonationRequestFactoryImpl implements ApplicationLinkRequestFactory {
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ApplicationLink applicationLink;
    private final ConsumerService consumerService;
    private final RequestFactory requestFactory;
    private final String username;

    public TwoLeggedOAuthWithImpersonationRequestFactoryImpl(final ApplicationLink applicationLink,
                                                             final AuthenticationConfigurationManager authenticationConfigurationManager,
                                                             final ConsumerService consumerService,
                                                             final RequestFactory requestFactory,
                                                             final String username) {
        this.applicationLink = requireNonNull(applicationLink);
        this.authenticationConfigurationManager = requireNonNull(authenticationConfigurationManager);
        this.consumerService = requireNonNull(consumerService);
        this.requestFactory = requireNonNull(requestFactory);
        this.username = requireNonNull(username, "username");
    }

    public ApplicationLinkRequest createRequest(final Request.MethodType methodType, final String uri) throws CredentialsRequiredException {
        final Map<String, String> config = authenticationConfigurationManager
                .getConfiguration(applicationLink.getId(), OAuthAuthenticationProvider.class);

        if (config == null) {
            throw new IllegalStateException(String.format(
                    "OAuth Authentication is not configured for application link %s", applicationLink));
        }

        final ServiceProvider serviceProvider = ServiceProviderUtil.getServiceProvider(config, applicationLink);
        final Request request = requestFactory.createRequest(methodType, uri);
        return new TwoLeggedOAuthWithImpersonationRequest(uri, methodType, request, serviceProvider, consumerService, applicationLink.getId(), username);
    }

    public URI getAuthorisationURI() {
        return null;
    }

    public URI getAuthorisationURI(final URI callback) {
        return null;
    }
}
