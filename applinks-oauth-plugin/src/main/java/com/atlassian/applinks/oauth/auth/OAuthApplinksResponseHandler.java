package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import com.atlassian.applinks.internal.common.net.AuthenticationAwareApplicationLinkResponseHandler;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 3.2
 */
public class OAuthApplinksResponseHandler<R> extends OAuthRedirectingApplicationLinkResponseHandler implements AuthenticationAwareApplicationLinkResponseHandler<R> {
    private final AuthenticationAwareApplicationLinkResponseHandler<R> applicationLinkResponseHandler;

    public OAuthApplinksResponseHandler(
            final String url,
            final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler,
            final ConsumerTokenStoreService consumerTokenStoreService,
            final OAuthRequest wrappedRequest,
            final ApplicationId applicationId,
            final String username,
            boolean followRedirects) {
        super(url, wrappedRequest, consumerTokenStoreService, applicationId, username, followRedirects);
        this.applicationLinkResponseHandler = getAuthenticationAwareApplicationLinkResponseHandler(applicationLinkResponseHandler);
    }

    public OAuthApplinksResponseHandler(
            final String url,
            final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler,
            final OAuthRequest wrappedRequest,
            final ApplicationId applicationId,
            final boolean followRedirects) {
        super(url, wrappedRequest, null, applicationId, null, followRedirects);
        this.applicationLinkResponseHandler = getAuthenticationAwareApplicationLinkResponseHandler(applicationLinkResponseHandler);
    }

    public R credentialsRequired(Response response) throws ResponseException {
        return applicationLinkResponseHandler.credentialsRequired(response);
    }

    @Override
    @Nonnull
    public R credentialsRequired(@Nonnull Response response, @Nullable String problem, @Nullable String problemAdvice)
            throws ResponseException {
        return applicationLinkResponseHandler.credentialsRequired(response, problem, problemAdvice);
    }

    @Override
    @Nonnull
    public R authenticationFailed(@Nonnull Response response, @Nullable String problem, @Nullable String problemAdvice)
            throws ResponseException {
        return applicationLinkResponseHandler.authenticationFailed(response, problem, problemAdvice);
    }

    @Override
    public R handle(Response response) throws ResponseException {
        checkForOAuthProblemAndRemoveConsumerTokenIfNecessary(response);
        if (hasTokenProblems) {
            return applicationLinkResponseHandler.credentialsRequired(response, authenticationProblem,
                    authenticationProblemAdvice);
        }

        if (authenticationProblem != null) {
            return applicationLinkResponseHandler.authenticationFailed(response, authenticationProblem,
                    authenticationProblemAdvice);
        }

        if (followRedirects && redirectHelper.responseShouldRedirect(response)) {
            wrappedRequest.setUrl(redirectHelper.getNextRedirectLocation(response));
            return wrappedRequest.execute(this);
        }

        return applicationLinkResponseHandler.handle(response);
    }

    /**
     * Get a handler potentially capable of intepreting authentication failures.
     */
    private AuthenticationAwareApplicationLinkResponseHandler<R> getAuthenticationAwareApplicationLinkResponseHandler(
            final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler) {
        return new AuthenticationAwareApplicationLinkResponseHandler<R>() {
            @Override
            @Nonnull
            public R authenticationFailed(@Nonnull Response response, @Nullable String reason,
                                          @Nullable String authenticationProblemAdvice) throws ResponseException {
                if (AuthenticationAwareApplicationLinkResponseHandler.class.isInstance(applicationLinkResponseHandler)) {
                    return ((AuthenticationAwareApplicationLinkResponseHandler<R>) applicationLinkResponseHandler)
                            .authenticationFailed(response, reason, authenticationProblemAdvice);
                } else {
                    throw new OAuthMessageProblemException("OAuth authentication failed: " + reason,
                            reason,
                            authenticationProblemAdvice,
                            OAuthParameters.asMap(allParameters));
                }
            }

            @Override
            public R credentialsRequired(Response response) throws ResponseException {
                return applicationLinkResponseHandler.credentialsRequired(response);
            }

            @Override
            @Nonnull
            public R credentialsRequired(@Nonnull Response response, @Nullable String problem,
                                         @Nullable String problemAdvice)
                    throws ResponseException {
                if (AuthenticationAwareApplicationLinkResponseHandler.class.isAssignableFrom(applicationLinkResponseHandler.getClass())) {
                    return ((AuthenticationAwareApplicationLinkResponseHandler<R>) applicationLinkResponseHandler)
                            .credentialsRequired(response, problem, problemAdvice);
                }

                return applicationLinkResponseHandler.credentialsRequired(response);
            }

            @Override
            public R handle(Response response) throws ResponseException {
                return applicationLinkResponseHandler.handle(response);
            }
        };
    }
}
