package com.atlassian.applinks.oauth.auth.servlets.serviceprovider;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.core.ServletPathConstants;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.oauth.auth.servlets.AbstractOAuthConfigServlet;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>This servlet behaves similar to {@link AddConsumerReciprocalServlet} in that it is the servlet that reacts to the form submit operation
 * from {@link AddConsumerByUrlServlet} served by the remote instance. However this servlet only concerns about turning on/off outgoing 2LO
 * requests.
 * </p>
 * <p>This servlet takes to following url parameters:</p>
 * <ul> <li>callback=[absolute-url]</li> </ul>
 *
 * <p>When redirecting back to <em>callback</em>, the following parameters are sent:</p>
 * <ul>
 *     <li>outgoing_2lo_success=[true|false]</li>
 *     <li>message=[description] -- optional parameter used to describe the error</li>
 * </ul>
 */
public class ConfigureOutgoingTwoLeggedOAuthReciprocalServlet extends AbstractOAuthConfigServlet {
    public static final String ENABLE_OUTGOING_2LO_AUTHENTICATION_PARAMETER = "enable-outgoing-2lo";
    public static final String ENABLE_OUTGOING_2LOI_AUTHENTICATION_PARAMETER = "enable-outgoing-2loi";
    public static final String OUTGOING_2LO_SUCCESS_PARAM = "outgoing_2lo_success";
    public static final String CALLBACK_PARAM = "callback";

    private static final Iterable<Class<? extends AuthenticationProvider>> TWO_LEGGED_OAUTH_AUTHENTICATION_PROVIDERS = ImmutableSet.of(TwoLeggedOAuthAuthenticationProvider.class, TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);

    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final WebSudoManager webSudoManager;
    private static final Logger LOG = LoggerFactory.getLogger(ConfigureOutgoingTwoLeggedOAuthReciprocalServlet.class);

    protected ConfigureOutgoingTwoLeggedOAuthReciprocalServlet(final I18nResolver i18nResolver,
                                                               final MessageFactory messageFactory,
                                                               final TemplateRenderer templateRenderer,
                                                               final WebResourceManager webResourceManager,
                                                               final ApplicationLinkService applicationLinkService,
                                                               final AdminUIAuthenticator adminUIAuthenticator,
                                                               final DocumentationLinker documentationLinker,
                                                               final LoginUriProvider loginUriProvider,
                                                               final InternalHostApplication internalHostApplication,
                                                               final XsrfTokenAccessor xsrfTokenAccessor,
                                                               final XsrfTokenValidator xsrfTokenValidator,
                                                               final AuthenticationConfigurationManager authenticationConfigurationManager,
                                                               final WebSudoManager webSudoManager) {
        super(i18nResolver,
                messageFactory,
                templateRenderer,
                webResourceManager,
                applicationLinkService,
                adminUIAuthenticator,
                documentationLinker,
                loginUriProvider,
                internalHostApplication,
                xsrfTokenAccessor,
                xsrfTokenValidator);
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.webSudoManager = webSudoManager;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse resp) throws ServletException, IOException {
        try {
            // Enable web sudo protection if needed and if the app we are running in supports it
            webSudoManager.willExecuteWebSudoRequest(request);

            final ApplicationLink applicationLink;
            try {
                applicationLink = getRequiredApplicationLink(request);
            } catch (NotFoundException ex) {
                resp.sendRedirect(createRedirectUrl(request, true, null));
                return;
            }

            // If ENABLE_OUTGOING_2LOI_AUTHENTICATION_PARAMETER is not present then the remote instance does not support separating config for 2LO/2LOi
            if (StringUtils.isEmpty(request.getParameter(ENABLE_OUTGOING_2LOI_AUTHENTICATION_PARAMETER))) {
                LOG.debug("Remote instance for link [{}] does not support independent configuration of 2LO/2LOi.", applicationLink.getId());
                Reconfigure2LOAnd2LOiInTandem(request, resp, applicationLink);
            } else {
                Reconfigure2LOAnd2LOiIndependently(request, resp, applicationLink);
            }


        } catch (WebSudoSessionException wse) {
            webSudoManager.enforceWebSudoProtection(request, resp);
        }
    }

    /**
     * Register or unregister 2LO and 2LOi authentication providers in tandem. This is the pre 4.0.15 approach
     *
     * @since 4.0.15
     */
    private void Reconfigure2LOAnd2LOiInTandem(final HttpServletRequest request, final HttpServletResponse resp, final ApplicationLink applicationLink)
            throws IOException {
        final boolean enable = Boolean.parseBoolean(request.getParameter(ENABLE_OUTGOING_2LO_AUTHENTICATION_PARAMETER));
        try {
            if (enable) {
                for (final Class<? extends AuthenticationProvider> authenticationProvider : TWO_LEGGED_OAUTH_AUTHENTICATION_PROVIDERS) {
                    authenticationConfigurationManager.registerProvider(
                            applicationLink.getId(),
                            authenticationProvider,
                            Collections.<String, String>emptyMap());
                }
                resp.sendRedirect(createRedirectUrl(request, true, i18nResolver.getText("auth.oauth.config.serviceprovider.outgoing.2lo.enabled")));
            } else {
                for (final Class<? extends AuthenticationProvider> authenticationProvider : TWO_LEGGED_OAUTH_AUTHENTICATION_PROVIDERS) {
                    authenticationConfigurationManager.unregisterProvider(
                            applicationLink.getId(),
                            authenticationProvider);
                }
                resp.sendRedirect(createRedirectUrl(request, true, i18nResolver.getText("auth.oauth.config.serviceprovider.outgoing.2lo.disabled")));
            }
        } catch (Exception e) {
            LOG.error("Error occurred when trying to " + (enable ? "enable" : "disable") + " outgoing 2-Legged OAuth authentication configuration for application link '" + applicationLink + "'", e);
            final String message = (enable ? i18nResolver.getText("auth.oauth.config.error.reciprocal.outgoing.2lo.config.enable") : i18nResolver.getText("auth.oauth.config.error.reciprocal.outgoing.2lo.config.disable"));
            resp.sendRedirect(createRedirectUrl(request, false, message));
        }
    }

    /**
     * Register or unregister 2LO and 2LOi authentication providers independently. This is the 4.0.15 and post approach
     *
     * @since 4.0.15
     */
    private void Reconfigure2LOAnd2LOiIndependently(final HttpServletRequest request, final HttpServletResponse resp, final ApplicationLink applicationLink)
            throws IOException {
        final boolean enable2LO = Boolean.parseBoolean(request.getParameter(ENABLE_OUTGOING_2LO_AUTHENTICATION_PARAMETER));
        final boolean enable2LOi = Boolean.parseBoolean(request.getParameter(ENABLE_OUTGOING_2LOI_AUTHENTICATION_PARAMETER));

        try {
            String message = "";
            if (enable2LO) {
                authenticationConfigurationManager.registerProvider(
                        applicationLink.getId(),
                        TwoLeggedOAuthAuthenticationProvider.class,
                        Collections.<String, String>emptyMap());
                message += i18nResolver.getText("auth.oauth.config.serviceprovider.outgoing.2lo.enabled");
            } else {
                authenticationConfigurationManager.unregisterProvider(
                        applicationLink.getId(),
                        TwoLeggedOAuthAuthenticationProvider.class);
                message += i18nResolver.getText("auth.oauth.config.serviceprovider.outgoing.2lo.disabled");
            }

            if (enable2LOi) {
                authenticationConfigurationManager.registerProvider(
                        applicationLink.getId(),
                        TwoLeggedOAuthWithImpersonationAuthenticationProvider.class,
                        Collections.<String, String>emptyMap());
                message += i18nResolver.getText("auth.oauth.config.serviceprovider.outgoing.2loi.enabled");
            } else {
                authenticationConfigurationManager.unregisterProvider(
                        applicationLink.getId(),
                        TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);
                message += i18nResolver.getText("auth.oauth.config.serviceprovider.outgoing.2loi.disabled");
            }

            resp.sendRedirect(createRedirectUrl(request, true, message));
        } catch (Exception e) {
            LOG.error("Error occurred when trying to " + (enable2LO ? "enable" : "disable") + " outgoing 2LO and " + (enable2LOi ? "enable" : "disable") + " outgoing 2LOi authentication configuration for application link '" + applicationLink + "'", e);
            final String message = (enable2LO ? i18nResolver.getText("auth.oauth.config.error.reciprocal.outgoing.2lo.config.enable") : i18nResolver.getText("auth.oauth.config.error.reciprocal.outgoing.2lo.config.disable"))
                    + (enable2LOi ? i18nResolver.getText("auth.oauth.config.error.reciprocal.outgoing.2loi.config.enable") : i18nResolver.getText("auth.oauth.config.error.reciprocal.outgoing.2loi.config.disable"));
            resp.sendRedirect(createRedirectUrl(request, false, message));
        }
    }

    public static String getReciprocalServletUrl(final URI baseUrl, final ApplicationId applicationId, final String callbackUrl, final String actionParamValue, final String actionParamValue2) {
        final URI enableOAuthURL = URIUtil.uncheckedConcatenate(baseUrl, ServletPathConstants.APPLINKS_CONFIG_SERVLET_PATH + "/oauth/outbound/apl-2lo/" + applicationId + "?callback=" + callbackUrl
                + "&" + ConfigureOutgoingTwoLeggedOAuthReciprocalServlet.ENABLE_OUTGOING_2LO_AUTHENTICATION_PARAMETER + "=" + actionParamValue
                + "&" + ConfigureOutgoingTwoLeggedOAuthReciprocalServlet.ENABLE_OUTGOING_2LOI_AUTHENTICATION_PARAMETER + "=" + actionParamValue2);
        return enableOAuthURL.toString();
    }

    private String createRedirectUrl(final HttpServletRequest req, final boolean success, final String message) {
        String callbackUrl = getRequiredParameter(req, CALLBACK_PARAM);
        if (callbackUrl.indexOf("?") == -1) {
            callbackUrl += "?";
        }
        String redirectUrl = String.format("%s&" + OUTGOING_2LO_SUCCESS_PARAM + "=%s", callbackUrl, success);
        if (!StringUtils.isBlank(message)) {
            redirectUrl += "&" + MESSAGE_PARAM + "=" + URIUtil.utf8Encode(message);
        }
        return redirectUrl;
    }
}