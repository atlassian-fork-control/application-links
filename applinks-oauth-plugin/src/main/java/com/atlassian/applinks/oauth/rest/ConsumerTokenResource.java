package com.atlassian.applinks.oauth.rest;

import com.atlassian.applinks.core.rest.auth.AdminApplicationLinksInterceptor;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.sal.api.user.UserManager;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.internal.common.rest.util.RestApplicationIdParser.parseApplicationId;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Path("consumer-token")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Singleton
@InterceptorChain({ContextInterceptor.class, AdminApplicationLinksInterceptor.class, NoCacheHeaderInterceptor.class})
public class ConsumerTokenResource {
    private final ConsumerTokenStoreService consumerTokenStoreService;
    private final UserManager userManager;

    public ConsumerTokenResource(final ConsumerTokenStoreService consumerTokenStoreService, final UserManager userManager) {
        this.consumerTokenStoreService = consumerTokenStoreService;
        this.userManager = userManager;
    }

    @DELETE
    @Path("{id}")
    public Response removeConsumerToken(@PathParam("id") final String id) {
        if (userManager.getRemoteUserKey() != null) {
            consumerTokenStoreService.removeConsumerToken(parseApplicationId(id), userManager.getRemoteUser().getUsername());
            return RestUtil.noContent();
        }
        return RestUtil.unauthorized("User is not authorized");
    }
}
