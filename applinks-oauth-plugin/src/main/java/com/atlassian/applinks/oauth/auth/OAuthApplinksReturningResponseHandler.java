package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @since 3.2
 */
public class OAuthApplinksReturningResponseHandler<R>
        extends OAuthRedirectingApplicationLinkResponseHandler implements ReturningResponseHandler<Response, R> {
    private static final Logger log = LoggerFactory.getLogger(OAuthApplinksReturningResponseHandler.class);

    private ReturningResponseHandler<? super Response, R> returningResponseHandler;

    public OAuthApplinksReturningResponseHandler(
            final String url,
            final ReturningResponseHandler<Response, R> returningResponseHandler,
            final ConsumerTokenStoreService consumerTokenStoreService,
            final ApplicationLinkRequest wrappedRequest,
            final ApplicationId applicationId,
            final String username,
            final boolean followRedirects) {
        super(url, wrappedRequest, consumerTokenStoreService, applicationId, username, followRedirects);
        this.returningResponseHandler = returningResponseHandler;
    }

    public OAuthApplinksReturningResponseHandler(
            final String url,
            final ReturningResponseHandler<? super Response, R> returningResponseHandler,
            ApplicationLinkRequest wrappedRequest,
            ApplicationId applicationId,
            boolean followRedirects) {
        super(url, wrappedRequest, null, applicationId, null, followRedirects);
        this.returningResponseHandler = returningResponseHandler;
    }

    public R handle(final Response response) throws ResponseException {
        checkForOAuthProblemAndRemoveConsumerTokenIfNecessary(response);
        if (followRedirects && redirectHelper.responseShouldRedirect(response)) {
            wrappedRequest.setUrl(redirectHelper.getNextRedirectLocation(response));
            return wrappedRequest.executeAndReturn(this);
        }

        return returningResponseHandler.handle(response);
    }
}
