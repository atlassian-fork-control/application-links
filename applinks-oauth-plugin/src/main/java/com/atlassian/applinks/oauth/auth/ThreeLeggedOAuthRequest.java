package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.oauth.Request;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.sal.api.net.ResponseException;
import net.oauth.OAuthMessage;

import java.io.IOException;
import java.net.URI;

/**
 * Represents a standard 3-legged OAuth request.
 *
 * @since 3.10
 */
public class ThreeLeggedOAuthRequest extends OAuthRequest {
    private final ConsumerToken consumerToken;
    private final ConsumerTokenStoreService consumerTokenStoreService;
    private final String username;

    public ThreeLeggedOAuthRequest(
            final String url,
            final MethodType methodType,
            final com.atlassian.sal.api.net.Request wrappedRequest,
            final ServiceProvider serviceProvider,
            final ConsumerService consumerService,
            final ConsumerToken consumerToken,
            final ConsumerTokenStoreService consumerTokenStoreService,
            final ApplicationId applicationId,
            final String username) {
        super(url, methodType, wrappedRequest, applicationId, serviceProvider, consumerService);
        this.consumerToken = consumerToken;
        this.consumerTokenStoreService = consumerTokenStoreService;
        this.username = username;
    }

    @Override
    protected Request createUnsignedRequest() {
        // a 3LO request needs a valid OAuth token.
        return new com.atlassian.oauth.Request(toOAuthMethodType(methodType), URI.create(url),
                toOAuthParameters(consumerToken.getToken()));
    }

    @Override
    public <R> R execute(final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler) throws ResponseException {
        signRequest();
        return wrappedRequest.execute(new OAuthApplinksResponseHandler<>(url, applicationLinkResponseHandler,
                consumerTokenStoreService, this, applicationId, username, followRedirects));
    }

    @Override
    protected void signRequest() throws ResponseException {
        final com.atlassian.oauth.Request oAuthRequest = createUnsignedRequest();
        final com.atlassian.oauth.Request signedRequest = consumerService.sign(oAuthRequest, serviceProvider, consumerToken);
        final OAuthMessage oAuthMessage = OAuthHelper.asOAuthMessage(signedRequest);
        try {
            wrappedRequest.setHeader("Authorization", oAuthMessage.getAuthorizationHeader(null));
        } catch (IOException e) {
            throw new ResponseException("Unable to generate OAuth Authorization request header.", e);
        }
    }
}