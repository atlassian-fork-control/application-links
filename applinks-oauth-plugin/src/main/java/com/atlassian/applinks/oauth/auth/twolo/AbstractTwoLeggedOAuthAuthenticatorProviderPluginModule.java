package com.atlassian.applinks.oauth.auth.twolo;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.core.util.RequestUtil;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.oauth.auth.OAuthAuthenticatorProviderPluginModule;
import com.atlassian.applinks.oauth.auth.OAuthHelper;
import com.atlassian.applinks.oauth.auth.servlets.serviceprovider.AddConsumerByUrlServlet;
import com.atlassian.applinks.spi.auth.AuthenticationDirection;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.applinks.spi.auth.IncomingTrustAuthenticationProviderPluginModule;
import org.osgi.framework.Version;

import javax.servlet.http.HttpServletRequest;

public abstract class AbstractTwoLeggedOAuthAuthenticatorProviderPluginModule
        implements AuthenticationProviderPluginModule, IncomingTrustAuthenticationProviderPluginModule

{
    private final InternalHostApplication hostApplication;

    public AbstractTwoLeggedOAuthAuthenticatorProviderPluginModule(InternalHostApplication hostApplication) {
        this.hostApplication = hostApplication;
    }

    public String getConfigUrl(ApplicationLink link, Version applicationLinksVersion, AuthenticationDirection direction,
                               HttpServletRequest request) {
        final boolean supportsAppLinks = applicationLinksVersion != null;

        //If the application is has the OAuth Plugin installed, we can use the same screen as for applications that have UAL installed.
        final boolean oAuthPluginInstalled = OAuthHelper.isOAuthPluginInstalled(link);

        if (direction == AuthenticationDirection.INBOUND) {
            if (supportsAppLinks || oAuthPluginInstalled) {
                return RequestUtil.getBaseURLFromRequest(request, hostApplication.getBaseUrl()) +
                        OAuthAuthenticatorProviderPluginModule.ADD_CONSUMER_BY_URL_SERVLET_LOCATION +
                        link.getId().toString() + "?" + AddConsumerByUrlServlet.UI_POSITION + "=local";
            } else {
                return RequestUtil.getBaseURLFromRequest(request, hostApplication.getBaseUrl()) +
                        OAuthAuthenticatorProviderPluginModule.ADD_CONSUMER_MANUALLY_SERVLET_LOCATION +
                        link.getId().toString();
            }
        }

        return null;
    }

    public Class<? extends AuthenticationProvider> getAuthenticationProviderClass() {
        return OAuthAuthenticationProvider.class;
    }
}