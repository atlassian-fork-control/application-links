package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.DependsOn;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.api.event.ApplicationLinkAuthConfigChangedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkEvent;
import com.atlassian.applinks.core.link.DefaultApplicationLink;
import com.atlassian.applinks.core.property.ApplicationLinkProperties;
import com.atlassian.applinks.core.property.PropertyService;
import com.atlassian.applinks.oauth.auth.twolo.TwoLeggedOAuthAuthenticatorProviderPluginModule;
import com.atlassian.applinks.oauth.auth.twolo.impersonation.TwoLeggedOAuthWithImpersonationAuthenticatorProviderPluginModule;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationDirection;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.event.api.EventPublisher;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.osgi.framework.Version;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class AuthenticationConfigurationManagerImplTest {
    ApplicationLinkService applicationLinkService;
    PropertyService propertyService;
    AuthenticationConfigurationManager manager;
    EventPublisher eventPublisher;
    ApplicationType type;
    ApplicationLinkProperties applicationLinkProperties;
    ApplicationLinkRequestFactoryFactory requestFactoryFactory;
    AuthenticatorAccessor authenticatorAccessor;

    @Before
    public void setUp() {
        applicationLinkService = mock(ApplicationLinkService.class);
        propertyService = mock(PropertyService.class);
        eventPublisher = mock(EventPublisher.class);
        type = mock(ApplicationType.class);
        applicationLinkProperties = mock(ApplicationLinkProperties.class);
        requestFactoryFactory = mock(ApplicationLinkRequestFactoryFactory.class);
        authenticatorAccessor = mock(AuthenticatorAccessor.class);

        manager = new AuthenticationConfigurationManagerImpl(applicationLinkService, authenticatorAccessor, propertyService, eventPublisher);
    }

    @Test
    public void testEventPublishedWhenProviderRegistered() throws Exception {
        ApplicationId applicationId = new ApplicationId(UUID.randomUUID().toString());
        Map<String, String> config = new HashMap<String, String>();
        ApplicationLink applicationLink = new DefaultApplicationLink(applicationId, type, applicationLinkProperties, requestFactoryFactory, eventPublisher);
        when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(applicationLink);
        ApplicationLinkProperties applicationLinkProperties = mock(ApplicationLinkProperties.class);
        when(propertyService.getApplicationLinkProperties(applicationId)).thenReturn(applicationLinkProperties);
        manager.registerProvider(applicationId, BasicAuthenticationProvider.class, config);
        verify(applicationLinkProperties).setProviderConfig(BasicAuthenticationProvider.class.getName(), config);
        verify(eventPublisher).publish(argThat(new IsCorrectEvent<ApplicationLinkAuthConfigChangedEvent>(applicationId)));
    }

    @Test
    public void testEventPublishedWhenProviderUnregistered() throws TypeNotInstalledException {
        ApplicationId applicationId = new ApplicationId(UUID.randomUUID().toString());
        ApplicationLink applicationLink = new DefaultApplicationLink(applicationId, type, applicationLinkProperties, requestFactoryFactory, eventPublisher);
        when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(applicationLink);
        ApplicationLinkProperties applicationLinkProperties = mock(ApplicationLinkProperties.class);
        when(propertyService.getApplicationLinkProperties(applicationId)).thenReturn(applicationLinkProperties);
        when(authenticatorAccessor.getAllAuthenticationProviderPluginModules()).thenReturn(ImmutableList.<AuthenticationProviderPluginModule>of());
        manager.unregisterProvider(applicationId, BasicAuthenticationProvider.class);
        verify(applicationLinkProperties).removeProviderConfig(BasicAuthenticationProvider.class.getName());
        verify(eventPublisher).publish(argThat(new IsCorrectEvent<ApplicationLinkAuthConfigChangedEvent>(applicationId)));
    }

    @Test
    public void unregisterProviderDeletesDependentProviderKeys() throws Exception {
        ApplicationId applicationId = new ApplicationId(UUID.randomUUID().toString());
        ApplicationLink applicationLink = new DefaultApplicationLink(applicationId, type, applicationLinkProperties, requestFactoryFactory, eventPublisher);
        when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(applicationLink);
        ApplicationLinkProperties applicationLinkProperties = mock(ApplicationLinkProperties.class);
        when(propertyService.getApplicationLinkProperties(applicationId)).thenReturn(applicationLinkProperties);
        when(authenticatorAccessor.getAllAuthenticationProviderPluginModules()).thenReturn(ImmutableList.<AuthenticationProviderPluginModule>of(
                mockModule(TestProvider2.class),
                mockModule(TestProvider3.class),
                mockModule(TestProvider4.class),
                mockModule(TestProvider5.class),
                mockModule(TestProvider6.class),
                mockModule(TestProvider7.class)));
        manager.unregisterProvider(applicationId, TestProvider1.class);
        verify(applicationLinkProperties, times(1)).removeProviderConfig(TestProvider1.class.getName());
        verify(applicationLinkProperties, times(1)).removeProviderConfig(TestProvider2.class.getName());
        verify(applicationLinkProperties, times(1)).removeProviderConfig(TestProvider3.class.getName());
        verify(applicationLinkProperties, times(1)).removeProviderConfig(TestProvider4.class.getName());
        verify(applicationLinkProperties, times(1)).removeProviderConfig(TestProvider5.class.getName());
        verify(applicationLinkProperties, never()).removeProviderConfig(TestProvider6.class.getName());
        verify(applicationLinkProperties, never()).removeProviderConfig(TestProvider7.class.getName());
        verifyNoMoreInteractions(applicationLinkProperties);
    }

    @Test
    public void unregisterOAuthProviderDeletes2LOAnd2LOiProviderKeys() throws Exception {
        ApplicationId applicationId = new ApplicationId(UUID.randomUUID().toString());
        ApplicationLink applicationLink = new DefaultApplicationLink(applicationId, type, applicationLinkProperties, requestFactoryFactory, eventPublisher);
        when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(applicationLink);
        ApplicationLinkProperties applicationLinkProperties = mock(ApplicationLinkProperties.class);
        when(propertyService.getApplicationLinkProperties(applicationId)).thenReturn(applicationLinkProperties);

        AuthenticationConfigurationManager authenticationConfigurationManager = mock(AuthenticationConfigurationManager.class);
        when(authenticationConfigurationManager.isConfigured(same(applicationId), any(Class.class))).thenReturn(true);
        when(authenticatorAccessor.getAllAuthenticationProviderPluginModules()).thenReturn(ImmutableList.<AuthenticationProviderPluginModule>of(
                new TwoLeggedOAuthAuthenticatorProviderPluginModule(authenticationConfigurationManager, null, null, null, null),
                new TwoLeggedOAuthWithImpersonationAuthenticatorProviderPluginModule(authenticationConfigurationManager, null, null, null, null, null, null)
        ));
        manager.unregisterProvider(applicationId, OAuthAuthenticationProvider.class);
        verify(applicationLinkProperties, times(1)).removeProviderConfig(OAuthAuthenticationProvider.class.getName());
        verify(applicationLinkProperties, times(1)).removeProviderConfig(TwoLeggedOAuthAuthenticationProvider.class.getName());
        verify(applicationLinkProperties, times(1)).removeProviderConfig(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class.getName());
        verifyNoMoreInteractions(applicationLinkProperties);
    }

    private AuthenticationProviderPluginModule mockModule(final Class<? extends AuthenticationProvider> providerClass) {
        // somehow mocking getAuthenticationProviderClass() does not work :|
        return new AuthenticationProviderPluginModule() {

            @Override
            public AuthenticationProvider getAuthenticationProvider(final ApplicationLink link) {
                return mock(providerClass);
            }

            @Override
            public String getConfigUrl(final ApplicationLink link, final Version applicationLinksVersion, final AuthenticationDirection direction, final HttpServletRequest request) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public Class<? extends AuthenticationProvider> getAuthenticationProviderClass() {
                return providerClass;
            }
        };
    }

    private interface TestProvider1 extends AuthenticationProvider {
    }

    @DependsOn(TestProvider1.class)
    private interface TestProvider2 extends AuthenticationProvider {
    }

    @DependsOn({TestProvider6.class, TestProvider1.class})
    private interface TestProvider3 extends AuthenticationProvider {
    }

    // indirectly depending on 1
    @DependsOn(TestProvider3.class)
    private interface TestProvider4 extends AuthenticationProvider {
    }

    // indirectly depending on 1
    @DependsOn(TestProvider4.class)
    private interface TestProvider5 extends AuthenticationProvider {
    }

    private interface TestProvider6 extends AuthenticationProvider {
    }

    @DependsOn(TestProvider6.class)
    private interface TestProvider7 extends AuthenticationProvider {
    }

    class IsCorrectEvent<T extends ApplicationLinkEvent> implements ArgumentMatcher {
        private final ApplicationId applicationId;

        public IsCorrectEvent(ApplicationId applicationId) {
            this.applicationId = applicationId;
        }

        public boolean matches(Object event) {
            return ((T) event).getApplicationId().equals(applicationId);
        }
    }
}
