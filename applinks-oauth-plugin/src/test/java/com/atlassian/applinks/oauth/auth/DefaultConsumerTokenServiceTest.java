package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link com.atlassian.applinks.oauth.auth.DefaultConsumerTokenService}
 *
 * @since v3.11.0
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultConsumerTokenServiceTest {
    private DefaultConsumerTokenService service;

    @Mock
    private ConsumerTokenStoreService consumerTokenStoreService;
    @Mock
    private ApplicationLinkService applicationLinkService;

    @Before
    public void setUp() throws Exception {
        service = new DefaultConsumerTokenService(consumerTokenStoreService, applicationLinkService);
    }

    @Test
    public void testRemoveAllTokensForUser_noApplinks() throws Exception {
        when(applicationLinkService.getApplicationLinks()).thenReturn(Collections.EMPTY_LIST);

        service.removeAllTokensForUsername("foo");

        verifyZeroInteractions(consumerTokenStoreService);
    }

    @Test
    public void testRemoveAllTokensForUser_noApplinkWithOAuth() throws Exception {
        final ApplicationLink link1 = link();
        final ApplicationLink link2 = link();
        when(applicationLinkService.getApplicationLinks()).thenReturn(ImmutableList.of(link1, link2));
        when(consumerTokenStoreService.isOAuthOutgoingEnabled(link1.getId())).thenReturn(false);
        when(consumerTokenStoreService.isOAuthOutgoingEnabled(link2.getId())).thenReturn(false);

        service.removeAllTokensForUsername("foo");

        verify(consumerTokenStoreService).isOAuthOutgoingEnabled(link1.getId());
        verify(consumerTokenStoreService).isOAuthOutgoingEnabled(link2.getId());
        verifyNoMoreInteractions(consumerTokenStoreService);
    }

    @Test
    public void testRemoveAllTokensForUser_someApplinksWithOAuth() throws Exception {
        final ApplicationLink link1 = link();
        final ApplicationLink link2 = link();
        final ApplicationLink link3 = link();
        final ApplicationLink link4 = link();
        when(applicationLinkService.getApplicationLinks()).thenReturn(ImmutableList.of(link1, link2, link3, link4));
        when(consumerTokenStoreService.isOAuthOutgoingEnabled(link1.getId())).thenReturn(false);
        when(consumerTokenStoreService.isOAuthOutgoingEnabled(link2.getId())).thenReturn(true);
        when(consumerTokenStoreService.isOAuthOutgoingEnabled(link3.getId())).thenReturn(false);
        when(consumerTokenStoreService.isOAuthOutgoingEnabled(link4.getId())).thenReturn(true);

        service.removeAllTokensForUsername("foo");

        verify(consumerTokenStoreService).isOAuthOutgoingEnabled(link1.getId());
        verify(consumerTokenStoreService).isOAuthOutgoingEnabled(link2.getId());
        verify(consumerTokenStoreService).isOAuthOutgoingEnabled(link3.getId());
        verify(consumerTokenStoreService).isOAuthOutgoingEnabled(link4.getId());
        verify(consumerTokenStoreService).removeConsumerToken(link2.getId(), "foo");
        verify(consumerTokenStoreService).removeConsumerToken(link4.getId(), "foo");
        verifyNoMoreInteractions(consumerTokenStoreService);
    }

    private ApplicationLink link() {
        ApplicationLink link = mock(ApplicationLink.class);
        ApplicationId id = new ApplicationId(UUID.randomUUID().toString());
        when(link.getId()).thenReturn(id);
        return link;
    }
}
