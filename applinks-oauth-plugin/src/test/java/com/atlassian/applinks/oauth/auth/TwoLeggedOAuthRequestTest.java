package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.oauth.auth.twolo.TwoLeggedOAuthRequest;
import com.atlassian.applinks.test.mock.MockApplicationLinkRequest;
import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import com.atlassian.applinks.test.mock.TestApplinkIds;
import com.atlassian.oauth.Request.HttpMethod;
import com.atlassian.oauth.Request.Parameter;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import javax.ws.rs.core.HttpHeaders;

import static com.atlassian.applinks.test.mockito.MockitoAnswers.withFirstArgument;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TwoLeggedOAuthRequestTest {
    private static final String HTTP_TEST_COM_URL = "http://test.com";
    private static final URI HTTP_TEST_COM = URI.create(HTTP_TEST_COM_URL);
    private static final TestApplinkIds APPLINK_ID = TestApplinkIds.DEFAULT_ID;

    @Mock
    private ConsumerService consumerService;

    @Captor
    private ArgumentCaptor<com.atlassian.oauth.Request> oAuthRequestCaptor;

    private ServiceProvider serviceProvider = new ServiceProvider(HTTP_TEST_COM, HTTP_TEST_COM, HTTP_TEST_COM);

    @Before
    public void setUpSigning() {
        when(consumerService.sign(any(com.atlassian.oauth.Request.class), same(serviceProvider)))
                .thenAnswer(withFirstArgument(com.atlassian.oauth.Request.class));
    }

    @Test
    public void testSimpleGet() throws ResponseException {
        Request<?, ?> wrappedRequest = new MockApplicationLinkRequest(Request.MethodType.GET, HTTP_TEST_COM_URL)
                .setResponse(new MockApplicationLinkResponse().setResponseBody("Test"));

        TwoLeggedOAuthRequest request = new TwoLeggedOAuthRequest(HTTP_TEST_COM_URL, Request.MethodType.GET,
                wrappedRequest, serviceProvider, consumerService, APPLINK_ID.applicationId());

        assertEquals("Test", request.execute());
        assertThat(wrappedRequest.getHeaders().get(HttpHeaders.AUTHORIZATION), contains("OAuth oauth_token=\"\""));

        // signRequest is called twice for execute() which is a bug, but let's not dare fixing it unless all
        // OAuthRequest subclasses have thorough unit test coverage
        verify(consumerService, times(2)).sign(oAuthRequestCaptor.capture(), same(serviceProvider));
        com.atlassian.oauth.Request oAuthRequest = oAuthRequestCaptor.getValue();
        assertEquals(HttpMethod.GET, oAuthRequest.getMethod());
        assertEquals(HTTP_TEST_COM, oAuthRequest.getUri());
        assertThat(oAuthRequest.getParameters(), Matchers.<Parameter>iterableWithSize(1));
        assertNotNull(oAuthRequest.getParameter("oauth_token"));
    }

    @Test
    public void testSimpleHead() throws ResponseException {
        Request<?, ?> wrappedRequest = new MockApplicationLinkRequest(Request.MethodType.HEAD, HTTP_TEST_COM_URL)
                .setResponse(new MockApplicationLinkResponse().setResponseBody("Test"));

        TwoLeggedOAuthRequest request = new TwoLeggedOAuthRequest(HTTP_TEST_COM_URL, Request.MethodType.HEAD,
                wrappedRequest, serviceProvider, consumerService, APPLINK_ID.applicationId());

        assertEquals("Test", request.execute());
        assertThat(wrappedRequest.getHeaders().get(HttpHeaders.AUTHORIZATION), contains("OAuth oauth_token=\"\""));

        // signRequest is called twice for execute() which is a bug, but let's not dare fixing it unless all
        // OAuthRequest subclasses have thorough unit test coverage
        verify(consumerService, times(2)).sign(oAuthRequestCaptor.capture(), same(serviceProvider));
        com.atlassian.oauth.Request oAuthRequest = oAuthRequestCaptor.getValue();
        assertEquals(HttpMethod.HEAD, oAuthRequest.getMethod());
        assertEquals(HTTP_TEST_COM, oAuthRequest.getUri());
        assertThat(oAuthRequest.getParameters(), Matchers.<Parameter>iterableWithSize(1));
        assertNotNull(oAuthRequest.getParameter("oauth_token"));
    }

    @Test
    public void testSimplePut() throws ResponseException {
        Request<?, ?> wrappedRequest = new MockApplicationLinkRequest(Request.MethodType.PUT, HTTP_TEST_COM_URL)
                .setResponse(new MockApplicationLinkResponse().setResponseBody("Test"));

        TwoLeggedOAuthRequest request = new TwoLeggedOAuthRequest(HTTP_TEST_COM_URL, Request.MethodType.PUT,
                wrappedRequest, serviceProvider, consumerService, APPLINK_ID.applicationId());

        assertEquals("Test", request.execute());
        assertThat(wrappedRequest.getHeaders().get(HttpHeaders.AUTHORIZATION), contains("OAuth oauth_token=\"\""));

        // signRequest is called twice for execute() which is a bug, but let's not dare fixing it unless all
        // OAuthRequest subclasses have thorough unit test coverage
        verify(consumerService, times(2)).sign(oAuthRequestCaptor.capture(), same(serviceProvider));
        com.atlassian.oauth.Request oAuthRequest = oAuthRequestCaptor.getValue();
        assertEquals(HttpMethod.PUT, oAuthRequest.getMethod());
        assertEquals(HTTP_TEST_COM, oAuthRequest.getUri());
        assertThat(oAuthRequest.getParameters(), Matchers.<Parameter>iterableWithSize(1));
        assertNotNull(oAuthRequest.getParameter("oauth_token"));
    }

    @Test
    public void testSimplePost() throws ResponseException {
        Request<?, ?> wrappedRequest = new MockApplicationLinkRequest(Request.MethodType.POST, HTTP_TEST_COM_URL)
                .setResponse(new MockApplicationLinkResponse().setResponseBody("Test"));

        TwoLeggedOAuthRequest request = new TwoLeggedOAuthRequest(HTTP_TEST_COM_URL, Request.MethodType.POST,
                wrappedRequest, serviceProvider, consumerService, APPLINK_ID.applicationId());

        assertEquals("Test", request.execute());
        assertThat(wrappedRequest.getHeaders().get(HttpHeaders.AUTHORIZATION), contains("OAuth oauth_token=\"\""));

        // signRequest is called twice for execute() which is a bug, but let's not dare fixing it unless all
        // OAuthRequest subclasses have thorough unit test coverage
        verify(consumerService, times(2)).sign(oAuthRequestCaptor.capture(), same(serviceProvider));
        com.atlassian.oauth.Request oAuthRequest = oAuthRequestCaptor.getValue();
        assertEquals(HttpMethod.POST, oAuthRequest.getMethod());
        assertEquals(HTTP_TEST_COM, oAuthRequest.getUri());
        assertThat(oAuthRequest.getParameters(), Matchers.<Parameter>iterableWithSize(1));
        assertNotNull(oAuthRequest.getParameter("oauth_token"));
    }
}
