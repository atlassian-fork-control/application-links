package com.atlassian.applinks.oauth.auth.servlets.serviceprovider;

import com.atlassian.applinks.core.util.Message;
import com.atlassian.templaterenderer.TemplateRenderer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;

abstract class AbstractConsumerServletTest {
    static class ErrorMessage implements Message {
        private String error;

        ErrorMessage(String error) {
            this.error = error;
        }

        @Override
        public String toString() {
            return error;
        }
    }

    void doAnswerWithExpectedErrorMessage(final TemplateRenderer templateRenderer, final String message) throws IOException {
        doAnswer(invocation -> {
            Map paramMap = (Map) invocation.getArguments()[1];

            assertTrue(paramMap.containsKey("message"));
            assertEquals(message, paramMap.get("message"));
            return null;
        }).when(templateRenderer).render(any(), any(), any(PrintWriter.class));
    }
}
