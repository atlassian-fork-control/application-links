package com.atlassian.applinks.oauth.auth.twolo.impersonation;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.oauth.Request;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.sal.testresources.net.MockRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.applinks.oauth.auth.twolo.impersonation.TwoLeggedOAuthWithImpersonationRequest.*;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.Silent.class)
public class TwoLeggedOAuthWithImpersonationRequestTest {
    @Mock
    ConsumerService consumerService;
    @Mock
    ApplicationId applicationId;
    @Mock
    List<com.atlassian.oauth.Request.Parameter> paramList;

    private final static String URI_STRING = "www.atlassian.com";
    private final static String URI_STRING_WITH_URL_PARAM = "www.atlassian.com?param=value";
    private final static String REQUEST_TOKEN_URI_STRING = "www.atlassian.com/RequestToken";
    private final static String USERNAME_WITH_SPACES = "This name has Spaces";
    private final static String USERNAME_NO_SPACES = "ThisIsUserName";
    private final static String AUTHORIZE_URI_STRING = "www.atlassian.com/RequestToken";
    private final static String ACCESS_Token_URI_STRING = "www.atlassian.com/RequestToken";
    private final static com.atlassian.sal.api.net.Request.MethodType METHOD_TYPE = com.atlassian.sal.api.net.Request.MethodType.GET;
    private final static Request.HttpMethod HTTP_METHOD = Request.HttpMethod.GET;

    private TwoLeggedOAuthWithImpersonationRequest twoLeggedOAuthWithImpersonationRequest;
    private ServiceProvider serviceProvider;
    private MockRequest wrappedRequest;

    @Before
    public void setUp() throws URISyntaxException {
        wrappedRequest = new MockRequest(METHOD_TYPE, URI_STRING);
        final URI uri = new URI(URI_STRING);
        final URI requestToken = new URI(REQUEST_TOKEN_URI_STRING);
        final URI authorizeToken = new URI(AUTHORIZE_URI_STRING);
        final URI accessToken = new URI(ACCESS_Token_URI_STRING);
        serviceProvider = new ServiceProvider(requestToken, authorizeToken, accessToken);
        paramList = new ArrayList<com.atlassian.oauth.Request.Parameter>();
        final com.atlassian.oauth.Request oauthRequest = new com.atlassian.oauth.Request(HTTP_METHOD, uri, paramList);

        when(consumerService.sign(isA(com.atlassian.oauth.Request.class), isA(ServiceProvider.class))).thenReturn(oauthRequest);
    }

    @Test
    public void testSignRequestEscapesNameWithSpaces() throws Exception {
        twoLeggedOAuthWithImpersonationRequest = buildTwoLeggedOAuthWithImpersonationRequest(URI_STRING, USERNAME_WITH_SPACES);
        String appendedURL = URI_STRING + "?" + XOAUTH_REQUESTOR_ID + "=" + USERNAME_WITH_SPACES.replace(" ", "+");

        twoLeggedOAuthWithImpersonationRequest.signRequest();

        Assert.assertEquals(wrappedRequest.getUrl(), appendedURL);
    }

    @Test
    public void testSignRequestNameReturnsExpectedURL() throws Exception {
        twoLeggedOAuthWithImpersonationRequest = buildTwoLeggedOAuthWithImpersonationRequest(URI_STRING, USERNAME_NO_SPACES);
        String appendedURL = URI_STRING + "?" + XOAUTH_REQUESTOR_ID + "=" + USERNAME_NO_SPACES;

        twoLeggedOAuthWithImpersonationRequest.signRequest();

        Assert.assertEquals(wrappedRequest.getUrl(), appendedURL);
    }

    @Test
    public void testSignRequestWhenURlHasQuestionMarkAndEscapesNameWithSpaces() throws Exception {
        twoLeggedOAuthWithImpersonationRequest = buildTwoLeggedOAuthWithImpersonationRequest(URI_STRING_WITH_URL_PARAM, USERNAME_WITH_SPACES);
        String appendedURL = URI_STRING_WITH_URL_PARAM + "&" + XOAUTH_REQUESTOR_ID + "=" + USERNAME_WITH_SPACES.replace(" ", "+");

        twoLeggedOAuthWithImpersonationRequest.signRequest();

        Assert.assertEquals(wrappedRequest.getUrl(), appendedURL);
    }

    @Test
    public void testSignRequestWhenURlHasQuestionMark() throws Exception {
        twoLeggedOAuthWithImpersonationRequest = buildTwoLeggedOAuthWithImpersonationRequest(URI_STRING_WITH_URL_PARAM, USERNAME_NO_SPACES);
        String appendedURL = URI_STRING_WITH_URL_PARAM + "&" + XOAUTH_REQUESTOR_ID + "=" + USERNAME_NO_SPACES;

        twoLeggedOAuthWithImpersonationRequest.signRequest();

        Assert.assertEquals(wrappedRequest.getUrl(), appendedURL);
    }


    private TwoLeggedOAuthWithImpersonationRequest buildTwoLeggedOAuthWithImpersonationRequest(String url, String username) {
        return twoLeggedOAuthWithImpersonationRequest = new TwoLeggedOAuthWithImpersonationRequest(
                url, METHOD_TYPE, wrappedRequest, serviceProvider, consumerService, applicationId, username);
    }

}


