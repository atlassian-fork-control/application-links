package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.core.RedirectController;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.applinks.ui.validators.CallbackParameterValidator;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Lists;
import net.oauth.OAuth;
import net.oauth.OAuthProblemException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.APPLICATION_LINK_ID_PARAM;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.ADMIN_ERROR_MESSAGE;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.ERROR;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.ERROR_DETAILS;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.USER_ERROR_MESSAGE;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.WARNING_MESSAGE;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.WARNING_TITLE;
import static com.atlassian.applinks.oauth.auth.OAuthParameters.asMap;
import static net.oauth.OAuth.Problems.CONSUMER_KEY_UNKNOWN;
import static net.oauth.OAuth.Problems.OAUTH_PROBLEM_ADVICE;
import static net.oauth.OAuth.Problems.TOKEN_REJECTED;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests covering OAuthApplinksServlet
 *
 * @since v3.11.1
 */
@SuppressWarnings("unchecked")
public class OAuthApplinksServletTest {
    private final static String APPLICATION_URL = "http://foo.com";
    private final static String CONSUMER_KEY = "key-1";
    private final static String CONSUMER_NAME = "name-1";
    private final static String USER_NAME = "user";
    private static final String HOST_NAME = "Jira";
    private static final String REMOTE_NAME = "Stash";
    private final static UUID APPLICATION_UUID = UUID.randomUUID();

    // uer facing errors
    private static final String ADMIN_ERROR_CHECK_CONFIG =
            String.format("[applinks.admin.error.message.check.for.misconfig][%s]", HOST_NAME);
    private static final String ADMIN_ERROR_CHECK_LINK =
            String.format("[applinks.admin.error.message.check.link][%s][%s]", HOST_NAME, REMOTE_NAME);
    private static final String USER_ERROR_NOT_LOGGED_IN =
            String.format("[applinks.user.error.message.not.logged.in][%s]", HOST_NAME);
    private static final String USER_ERROR_ACCESS_DENIED =
            String.format("[applinks.user.error.message.access.denied][%s][%s]", HOST_NAME, REMOTE_NAME);
    private static final String X_FRAME_OPTIONS = "X-Frame-Options";
    private static final String SAMEORIGIN = "SAMEORIGIN";

    private OAuthApplinksServlet servlet;
    // mock services
    @Mock
    private ApplicationLinkService applicationLinkService;
    @Mock
    private ConsumerTokenStoreService consumerTokenStoreService;
    @Mock
    private OAuthTokenRetriever oAuthTokenRetriever;
    @Mock
    private UserManager userManager;
    @Mock
    private UserProfile userProfile;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private WebResourceManager webResourceManager;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private AuthenticationConfigurationManager authenticationConfigurationManager;
    @Mock
    private ConsumerService consumerService;
    @Mock
    private InternalHostApplication internalHostApplication;
    @Mock
    private MessageFactory messageFactory;
    @Mock
    private DocumentationLinker documentationLinker;
    @Mock
    private LoginUriProvider loginUriProvider;

    @Mock
    private AdminUIAuthenticator adminUIAuthenticator;
    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;
    // mock data
    @Mock
    private ApplicationId applicationId;
    @Mock
    private ApplicationLink applicationLink;
    @Mock
    private ApplicationType applicationType;
    @Mock
    private CallbackParameterValidator callbackParameterValidator;

    @Mock
    private RedirectController redirectController;

    private ArgumentCaptor<Map> contextArgument = ArgumentCaptor.forClass(Map.class);

    @Before
    public void setup() throws IOException, TypeNotInstalledException, URISyntaxException {
        MockitoAnnotations.initMocks(this);

        servlet = new OAuthApplinksServlet(consumerTokenStoreService,
                oAuthTokenRetriever,
                userManager,
                i18nResolver,
                webResourceManager,
                templateRenderer,
                authenticationConfigurationManager,
                consumerService,
                internalHostApplication,
                callbackParameterValidator,
                applicationLinkService,
                redirectController);

        // default mocks

        // configure data
        when(applicationId.get()).thenReturn(APPLICATION_UUID.toString());
        when(applicationLink.getId()).thenReturn(applicationId);
        when(applicationLink.getRpcUrl()).thenReturn(new URI(APPLICATION_URL));
        when(applicationLink.getDisplayUrl()).thenReturn(new URI(APPLICATION_URL));

        // can't mock final Consumer so build by hand
        Consumer.InstanceBuilder builder = Consumer.key(CONSUMER_KEY);
        Consumer consumer = builder.name(CONSUMER_NAME).signatureMethod(Consumer.SignatureMethod.HMAC_SHA1).build();

        when(request.getParameter(APPLICATION_LINK_ID_PARAM)).thenReturn(APPLICATION_UUID.toString());

        // configure services
        when(userManager.getRemoteUser(request)).thenReturn(userProfile);
        when(userProfile.getUsername()).thenReturn(USER_NAME);
        when(applicationLinkService.getApplicationLink(any(ApplicationId.class))).thenReturn(applicationLink);
        when(authenticationConfigurationManager.isConfigured(applicationId, OAuthAuthenticationProvider.class)).thenReturn(true);
        when(consumerService.getConsumer()).thenReturn(consumer);

        // mock i18n to return the key.
        when(i18nResolver.getText(any(String.class))).thenAnswer(getText());
        when(i18nResolver.getText(any(String.class), any(String.class))).thenAnswer(getText());
        when(i18nResolver.getText(any(String.class), any(String.class), any(String.class))).thenAnswer(getText());

        when(internalHostApplication.getName()).thenReturn(HOST_NAME);
        when(applicationLink.getName()).thenReturn(REMOTE_NAME);
        when(applicationLink.getType()).thenReturn(applicationType);
    }

    private Answer<String> getText() {
        return new Answer<String>() {
            public String answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                StringBuilder answer = new StringBuilder();
                for (Object arg : args) {
                    answer.append("[");
                    answer.append(arg);
                    answer.append("]");
                }
                return answer.toString();
            }
        };
    }

    @Test
    public void verifyNullRemoteUserErrorIsHandled() throws ServletException, IOException, TypeNotInstalledException {
        // setup
        when(userManager.getRemoteUser(request)).thenReturn(null);

        // execute
        servlet.doGet(request, response);

        // check the username was requested
        verify(userManager, times(1)).getRemoteUser(request);

        // check the process did not progress.
        verify(applicationLinkService, never()).getApplicationLink(any());

        // check the correct errors are in the context
        final String notLoggedInError = "[auth.oauth.config.error.not.loggedin]";
        Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        assertContextContainsError(context, notLoggedInError);
        assertContextContainsUserError(context, USER_ERROR_NOT_LOGGED_IN);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    @Test
    public void verifyNullApplicationIdErrorIsHandled() throws ServletException, IOException, TypeNotInstalledException {
        // setup
        when(request.getParameter(APPLICATION_LINK_ID_PARAM)).thenReturn(null);

        // execute
        servlet.doGet(request, response);

        // check the value was requested
        verify(request, times(1)).getParameter(APPLICATION_LINK_ID_PARAM);

        // check the process did not progress.
        verify(applicationLinkService, never()).getApplicationLink(any());

        // check the correct errors are in the context
        final Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        final String linkIdEmptyError = "[auth.oauth.config.error.link.id.empty]";
        assertContextContainsError(context, linkIdEmptyError);
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_CONFIG);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    @Test
    public void verifyTypeNotInstalledErrorIsHandled() throws ServletException, IOException, TypeNotInstalledException {
        // setup
        final String type = "applinkType";
        when(applicationLinkService.getApplicationLink(any()))
                .thenThrow(new TypeNotInstalledException(type, null, null));

        // execute
        servlet.doGet(request, response);

        // check the correct errors are in the context
        final String typeNotInstalledError = String.format("[auth.oauth.config.error.link.type.not.loaded][%s][%s]",
                APPLICATION_UUID.toString(), type);
        final Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        assertContextContainsError(context, typeNotInstalledError);
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_CONFIG);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }


    @Test
    public void verifyNullApplicationLinkErrorIsHandled() throws ServletException, IOException, TypeNotInstalledException {
        // setup
        when(applicationLinkService.getApplicationLink(any())).thenReturn(null);

        // execute
        servlet.doGet(request, response);

        // check the value was requested
        verify(applicationLinkService, times(1)).getApplicationLink(any());

        // check the process did not progress.
        verify(authenticationConfigurationManager, never()).isConfigured(applicationId, OAuthAuthenticationProvider.class);

        // check the correct errors are in the context
        final Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        final String linkIdNullError = String.format("[auth.oauth.config.error.link.id][%s]",
                APPLICATION_UUID.toString());
        assertContextContainsError(context, linkIdNullError);
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_CONFIG);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    @Test
    public void verifyOauthNotConfiguredCaseIsHandled() throws ServletException, IOException {
        // setup
        when(authenticationConfigurationManager.isConfigured(applicationId, OAuthAuthenticationProvider.class))
                .thenReturn(false);

        // execute
        servlet.doGet(request, response);

        // check the correct errors are in the context
        final String oauthNotConfiguredError = String.format("[auth.oauth.config.error.not.configured][%s]",
                applicationLink.toString());
        final Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        assertContextContainsError(context, oauthNotConfiguredError);
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_LINK);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    @Test
    public void verifyConsumerKeyUnknownCaseIsHandled() throws ResponseException, IOException, ServletException {
        // setup
        setupGetRequestTokenToThrow(new RuntimeException(new OAuthProblemException(CONSUMER_KEY_UNKNOWN)));

        // execute
        servlet.doGet(request, response);

        // check the correct errors are in the context
        final String consumerKeyUnknownError =
                String.format("[auth.oauth.config.error.dance.oauth.problem.consumer.unknown][%s][%s]",
                        HOST_NAME, REMOTE_NAME);
        final Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        assertContextContainsError(context, consumerKeyUnknownError);
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_LINK);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    @Test
    public void verifyTokenRejectedCaseIsHandled() throws ResponseException, ServletException, IOException {
        // setup
        setupGetRequestTokenToThrow(new RuntimeException(new OAuthProblemException(TOKEN_REJECTED)));

        // execute
        servlet.doGet(request, response);

        // check the correct errors are in the context
        final String tokenRejectedError = "[auth.oauth.config.error.dance.oauth.problem.token.rejected]";
        final Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        assertContextContainsError(context, tokenRejectedError);
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_LINK);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    @Test
    public void verifyGeneralOauthProblemCaseIsHandled() throws ResponseException, ServletException, IOException {
        // setup

        // we only handle CONSUMER_KEY_UNKNOWN and TOKEN_REJECTED explicitly, so this will trigger the general case
        setupGetRequestTokenToThrow(new RuntimeException(new OAuthProblemException(OAUTH_PROBLEM_ADVICE)));

        // execute
        servlet.doGet(request, response);

        // check the correct errors are in the context
        final String oauthProblemError = String.format("[auth.oauth.config.error.dance.oauth.problem][%s][%s]",
                applicationLink.toString(), OAuth.Problems.OAUTH_PROBLEM_ADVICE);
        final Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        assertContextContainsError(context, oauthProblemError);
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_LINK);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    @Test
    public void verifyPermissionDeniedCaseIsHandled() throws ResponseException, ServletException, IOException {
        // setup
        setupGetRequestTokenToThrow(new OAuthPermissionDeniedException("some message"));

        // execute
        servlet.doGet(request, response);

        // check the correct errors/warnings are in the context
        final String warningTitle = "[auth.oauth.config.dance.denied.title]";
        final String warningMessage = String.format("[auth.oauth.config.dance.denied.message][%s]",
                applicationLink.getName());
        final Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        assertContextContainsWarning(context, warningTitle, warningMessage);
        assertContextContainsUserError(context, USER_ERROR_ACCESS_DENIED);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }


    @Test
    public void verifyOauthDanceThrowingAnOAuthMessageProblemExceptionIsHandled()
            throws ServletException, IOException, ResponseException {
        // setup
        final List<OAuth.Parameter> oauthParameters = Lists.newArrayList(new OAuth.Parameter("param1", "value1"));
        final String keyValueStr = "param1: 'value1'";
        setupGetRequestTokenToThrow(new OAuthMessageProblemException("some oauth problem", asMap(oauthParameters)));

        // execute
        servlet.doGet(request, response);

        // check the context contains the correct errors
        Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        final String danceError = String.format("[auth.oauth.config.error.dance][%s]", applicationLink.toString());
        assertContextContainsError(context, danceError);
        assertContextContainsErrorDetails(context, Lists.newArrayList(keyValueStr));
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_LINK);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    @Test
    public void verifyOauthDanceThrowingAResponseExceptionIsHandled()
            throws ServletException, IOException, ResponseException {
        // setup
        final String message = "message";
        setupGetRequestTokenToThrow(new ResponseException(message));

        // execute
        servlet.doGet(request, response);

        // check the context contains the correct errors
        Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        final String danceError = String.format("[auth.oauth.config.error.dance][%s]", applicationLink.toString());
        assertContextContainsError(context, danceError);
        assertContextContainsErrorDetails(context, Lists.newArrayList(message));
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_LINK);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    @Test
    public void verifyOauthDanceThrowingAGeneralExceptionIsHandled()
            throws ServletException, IOException, ResponseException {
        // setup
        setupGetRequestTokenToThrow(new RuntimeException());

        // execute
        servlet.doGet(request, response);

        // check the context contains the correct errors
        Map context = verifyTheRendererWasCalledAndCaptureTheContext();
        final String danceError = String.format("[auth.oauth.config.error.dance][%s]", applicationLink.toString());
        assertContextContainsError(context, danceError);
        assertContextContainsAdminError(context, ADMIN_ERROR_CHECK_LINK);
        verify(response, times(1)).setHeader(X_FRAME_OPTIONS, SAMEORIGIN);
    }

    private void assertContextContainsWarning(final Map context, final String warningTitle, final String warningMessage) {
        assertThat(context.get(WARNING_TITLE.key), Matchers.is(warningTitle));
        assertThat(context.get(WARNING_MESSAGE.key), Matchers.is(warningMessage));
    }

    private void setupGetRequestTokenToThrow(final Throwable t) throws ResponseException {
        when(oAuthTokenRetriever.getRequestToken(any(ServiceProvider.class), any(), any())).thenThrow(t);
    }

    private void assertContextContainsUserError(final Map context, final String userError) {
        assertThat(context.get(USER_ERROR_MESSAGE.key), Matchers.is(userError));
    }

    private void assertContextContainsAdminError(final Map context, final String adminError) {
        assertThat(context.get(ADMIN_ERROR_MESSAGE.key), Matchers.is(adminError));
    }

    private void assertContextContainsErrorDetails(final Map context, final List<String> errorDetails) {
        assertThat(context.get(ERROR_DETAILS.key), Matchers.is(errorDetails));
    }

    private void assertContextContainsError(final Map context, final String typeNotInstalledError) {
        assertThat(context.get(ERROR.key), Matchers.is(typeNotInstalledError));
    }

    private Map verifyTheRendererWasCalledAndCaptureTheContext() throws IOException {
        verify(templateRenderer, times(1)).render(contains("com/atlassian/applinks/oauth/auth/oauth_dance.vm"),
                contextArgument.capture(), any());
        return contextArgument.getValue();
    }
}
