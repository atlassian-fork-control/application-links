package com.atlassian.applinks.oauth.auth;


import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import com.atlassian.applinks.oauth.auth.twolo.TwoLeggedOAuthRequest;
import com.atlassian.applinks.oauth.test.matchers.OAuthExceptionMatchers;
import com.atlassian.applinks.test.mock.MockApplicationLinkRequest;
import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import com.atlassian.applinks.test.mock.TestApplinkIds;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.ResponseException;
import net.oauth.OAuth;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;

import static com.atlassian.applinks.oauth.auth.OAuthRedirectingApplicationLinkResponseHandler.WWW_AUTH_HEADER;
import static java.lang.String.format;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OAuthApplinksResponseHandlerTest {
    private static final String BASE_URL = "http://localhost:8080/";
    private static final String REQUEST_URL = BASE_URL + "servlet";
    private static final URI HTTP_TEST_COM = URI.create("http://test.com");

    @Mock
    private ConsumerTokenStoreService consumerTokenStoreService;
    @Mock
    private ConsumerService consumerService;

    private ServiceProvider serviceProvider = new ServiceProvider(HTTP_TEST_COM, HTTP_TEST_COM, HTTP_TEST_COM);

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @SuppressWarnings("unchecked")
    @Test
    public void shouldThrowResponseExceptionGivenOAuthProblemAndNonAuthenticationAware() throws ResponseException {
        expectedException.expect(OAuthMessageProblemException.class);
        expectedException.expect(OAuthExceptionMatchers.withOAuthProblem(OAuth.Problems.CONSUMER_KEY_UNKNOWN));

        ApplicationLinkResponseHandler<String> nonAuthAwareHandler = Mockito.mock(ApplicationLinkResponseHandler.class);
        OAuthApplinksResponseHandler<String> responseHandler = new OAuthApplinksResponseHandler<String>(
                REQUEST_URL,
                nonAuthAwareHandler,
                consumerTokenStoreService,
                createTestRequest("http://test.com", MethodType.GET),
                TestApplinkIds.DEFAULT_ID.applicationId(),
                "test", true
        );

        MockApplicationLinkResponse response = new MockApplicationLinkResponse();
        response.setHeader(WWW_AUTH_HEADER, format("OAuth oauth_problem=\"%s\"", OAuth.Problems.CONSUMER_KEY_UNKNOWN));

        responseHandler.handle(response);
    }

    private OAuthRequest createTestRequest(String url, MethodType type) {
        return new TwoLeggedOAuthRequest(url,
                type,
                new MockApplicationLinkRequest(type, url),
                serviceProvider,
                consumerService,
                TestApplinkIds.DEFAULT_ID.applicationId());
    }
}
