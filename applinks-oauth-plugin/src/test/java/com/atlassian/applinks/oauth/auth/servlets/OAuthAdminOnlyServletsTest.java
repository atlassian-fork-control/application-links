package com.atlassian.applinks.oauth.auth.servlets;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.RedirectController;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.oauth.auth.servlets.consumer.AddAtlassianServiceProviderServlet;
import com.atlassian.applinks.oauth.auth.servlets.serviceprovider.AddConsumerReciprocalServlet;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.applinks.ui.validators.CallbackParameterValidator;
import com.atlassian.applinks.ui.velocity.ListApplicationLinksContext;
import com.atlassian.applinks.ui.velocity.VelocityContextFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Theories.class)
public class OAuthAdminOnlyServletsTest {
    @DataPoints
    public static AdminOnlyTestServlet[] testAdminOnlyServlets = AdminOnlyTestServlet.values();

    static I18nResolver i18nResolver;
    static MessageFactory messageFactory;
    static TemplateRenderer templateRenderer;
    static WebResourceManager webResourceManager;
    static ApplicationLinkService applicationLinkService;
    static ManifestRetriever manifestRetriever;
    static PluginAccessor pluginAccessor;
    static AdminUIAuthenticator adminUIAuthenticator;
    static InternalHostApplication internalHostApplication;
    static DocumentationLinker documentationLinker;
    static LoginUriProvider loginUriProvider;
    static WebSudoManager webSudoManager;
    static VelocityContextFactory velocityContextFactory;
    static AuthenticationConfigurationManager configurationManager;
    static ConsumerTokenStoreService consumerTokenStoreService;
    static XsrfTokenAccessor xsrfTokenAccessor;
    static XsrfTokenValidator xsrfTokenValidator;
    static UserManager userManager;
    static CallbackParameterValidator callbackParameterValidator;
    static RedirectController redirectController;

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;
    PrintWriter writer;

    ListApplicationLinksContext listApplicationLinksContext;


    @Before
    public void setUp() {
        i18nResolver = mock(I18nResolver.class);
        messageFactory = mock(MessageFactory.class);
        templateRenderer = mock(TemplateRenderer.class);
        webResourceManager = mock(WebResourceManager.class);
        applicationLinkService = mock(ApplicationLinkService.class);
        manifestRetriever = mock(ManifestRetriever.class);
        pluginAccessor = mock(PluginAccessor.class);
        adminUIAuthenticator = mock(AdminUIAuthenticator.class);
        internalHostApplication = mock(InternalHostApplication.class);
        documentationLinker = mock(DocumentationLinker.class);
        loginUriProvider = mock(LoginUriProvider.class);
        webSudoManager = mock(WebSudoManager.class);
        velocityContextFactory = mock(VelocityContextFactory.class);
        configurationManager = mock(AuthenticationConfigurationManager.class);
        consumerTokenStoreService = mock(ConsumerTokenStoreService.class);
        xsrfTokenAccessor = mock(XsrfTokenAccessor.class);
        xsrfTokenValidator = mock(XsrfTokenValidator.class);
        userManager = mock(UserManager.class);
        callbackParameterValidator = mock(CallbackParameterValidator.class);
        redirectController = mock(RedirectController.class);

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        writer = mock(PrintWriter.class);
        listApplicationLinksContext = mock(ListApplicationLinksContext.class);

        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(request.getServletPath()).thenReturn("servlet-path");
        when(xsrfTokenValidator.validateFormEncodedToken(request)).thenReturn(true);
        when(userManager.getRemoteUsername()).thenReturn("admin");
        when(userManager.isSystemAdmin(any(String.class))).thenReturn(true);
        when(velocityContextFactory.buildListApplicationLinksContext(request)).thenReturn(listApplicationLinksContext);
    }

    @Theory
    public void verifyThatAdminAccessIsCheckedForAdminOnlyServlet(AdminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsAdmin();
        servlet.getServlet().service(request, response);

        verify(adminUIAuthenticator).checkAdminUIAccessBySessionOrCurrentUser(request);
    }

    @Theory
    public void verifyThatWebSudoRequestIsExecutedForAdminOnlyServlet(AdminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsAdmin();
        servlet.getServlet().service(request, response);

        verify(webSudoManager).willExecuteWebSudoRequest(request);
    }

    @Theory
    public void verifyWebSudoGivenControlWhenRequestRequiresSudoAuth(AdminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsAdmin();
        doThrow(new WebSudoSessionException("blah")).when(webSudoManager).willExecuteWebSudoRequest(request);
        servlet.getServlet().service(request, response);

        verify(webSudoManager).enforceWebSudoProtection(request, response);
    }

    private void whenUserIsLoggedInAsAdmin() {
        when(adminUIAuthenticator.checkAdminUIAccessBySessionOrCurrentUser(request)).thenReturn(true);
    }

    private void whenUserIsLoggedInAsSysadmin() {
        when(adminUIAuthenticator.checkSysadminUIAccessBySessionOrCurrentUser(request)).thenReturn(true);
    }

    enum AdminOnlyTestServlet {
        OAUTH_ADD_ATLASSIAN_SERVICE_PROVIDER_SERVLET_GET {
            @Override
            HttpServlet getServlet() {
                return new AddAtlassianServiceProviderServlet(i18nResolver, messageFactory, templateRenderer,
                        webResourceManager, applicationLinkService, adminUIAuthenticator, configurationManager,
                        consumerTokenStoreService, internalHostApplication, loginUriProvider,
                        documentationLinker, webSudoManager, xsrfTokenAccessor, xsrfTokenValidator);
            }

            @Override
            String getMethod() {
                return "GET";
            }
        },

        OAUTH_ADD_ATLASSIAN_SERVICE_PROVIDER_SERVLET_POST {
            @Override
            HttpServlet getServlet() {
                return new AddAtlassianServiceProviderServlet(i18nResolver, messageFactory, templateRenderer,
                        webResourceManager, applicationLinkService, adminUIAuthenticator, configurationManager,
                        consumerTokenStoreService, internalHostApplication, loginUriProvider,
                        documentationLinker, webSudoManager, xsrfTokenAccessor, xsrfTokenValidator);
            }

            @Override
            String getMethod() {
                return "POST";
            }
        },

        OAUTH_ADD_CONSUMER_RECIPROCAL_SERVLET_GET {
            @Override
            HttpServlet getServlet() {
                return new AddConsumerReciprocalServlet(i18nResolver, messageFactory, templateRenderer,
                        webResourceManager, applicationLinkService, adminUIAuthenticator, configurationManager,
                        consumerTokenStoreService, internalHostApplication, loginUriProvider,
                        documentationLinker, webSudoManager, xsrfTokenAccessor, xsrfTokenValidator,
                        callbackParameterValidator, redirectController);
            }

            @Override
            String getMethod() {
                return "GET";
            }
        };

        abstract HttpServlet getServlet();

        abstract String getMethod();
    }
}