package com.atlassian.applinks.oauth.auth.servlets.serviceprovider;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Theories.class)
public class AddConsumerManuallyServletTest extends AbstractConsumerServletTest {
    @DataPoints
    public static String[] methods = new String[]{"GET", "POST"};

    I18nResolver i18nResolver;
    MessageFactory messageFactory;
    TemplateRenderer templateRenderer;
    WebResourceManager webResourceManager;
    ApplicationLinkService applicationLinkService;
    AdminUIAuthenticator adminUIAuthenticator;
    InternalHostApplication internalHostApplication;
    DocumentationLinker documentationLinker;
    LoginUriProvider loginUriProvider;
    WebSudoManager webSudoManager;
    RequestFactory requestFactory;
    ServiceProviderStoreService serviceProviderStoreService;
    XsrfTokenAccessor xsrfTokenAccessor;
    XsrfTokenValidator xsrfTokenValidator;
    UserManager userManager;

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;
    PrintWriter writer;

    HttpServlet servlet;

    @Before
    public void setUp() {
        i18nResolver = mock(I18nResolver.class);
        messageFactory = mock(MessageFactory.class);
        templateRenderer = mock(TemplateRenderer.class);
        webResourceManager = mock(WebResourceManager.class);
        applicationLinkService = mock(ApplicationLinkService.class);
        adminUIAuthenticator = mock(AdminUIAuthenticator.class);
        internalHostApplication = mock(InternalHostApplication.class);
        documentationLinker = mock(DocumentationLinker.class);
        loginUriProvider = mock(LoginUriProvider.class);
        webSudoManager = mock(WebSudoManager.class);
        requestFactory = mock(RequestFactory.class);
        serviceProviderStoreService = mock(ServiceProviderStoreService.class);
        xsrfTokenAccessor = mock(XsrfTokenAccessor.class);
        xsrfTokenValidator = mock(XsrfTokenValidator.class);
        userManager = mock(UserManager.class);

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        writer = mock(PrintWriter.class);

        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(request.getServletPath()).thenReturn("servlet-path");
        when(xsrfTokenValidator.validateFormEncodedToken(request)).thenReturn(true);

        servlet = new AddConsumerManuallyServlet(i18nResolver, messageFactory, templateRenderer, webResourceManager,
                applicationLinkService, adminUIAuthenticator, requestFactory, serviceProviderStoreService,
                internalHostApplication, loginUriProvider, documentationLinker,
                webSudoManager, xsrfTokenAccessor, xsrfTokenValidator, userManager);
    }

    @Theory
    public void verifyThatAdminAccessIsCheckedForAdminOnlyServlet(String method) throws Exception {
        when(request.getMethod()).thenReturn(method);
        whenUserIsLoggedInAsAdmin();
        servlet.service(request, response);

        verify(adminUIAuthenticator).checkAdminUIAccessBySessionOrCurrentUser(request);
    }

    @Theory
    public void verifyThatWebSudoRequestIsExecutedForAdminOnlyServlet(String method) throws Exception {
        when(request.getMethod()).thenReturn(method);
        whenUserIsLoggedInAsAdmin();

        servlet.service(request, response);

        verify(webSudoManager).willExecuteWebSudoRequest(request);
    }

    @Theory
    public void verifyWebSudoGivenControlWhenRequestRequiresSudoAuth(String method) throws Exception {
        when(request.getMethod()).thenReturn(method);
        whenUserIsLoggedInAsAdmin();

        doThrow(new WebSudoSessionException("blah")).when(webSudoManager).willExecuteWebSudoRequest(request);
        servlet.service(request, response);

        verify(webSudoManager).enforceWebSudoProtection(request, response);
    }

    @Test
    public void verifyAdminCannotAlter2LOOptions() throws Exception {
        final ApplicationLink applicationLink = mock(ApplicationLink.class);
        when(request.getMethod()).thenReturn("POST");
        whenUserIsLoggedInAsAdmin();
        when(applicationLinkService.getApplicationLink(any(ApplicationId.class))).thenReturn(applicationLink);
        when(request.getParameter("oauth-incoming-enabled")).thenReturn("true");
        when(request.getParameter("two-lo-enabled")).thenReturn("true");
        when(userManager.getRemoteUsername()).thenReturn("betty");
        when(userManager.isSystemAdmin("betty")).thenReturn(false);
        when(messageFactory.newI18nMessage("applinks.error.only.sysadmin.operation")).thenReturn(new ErrorMessage("sysadmin only message"));
        doAnswerWithExpectedErrorMessage(templateRenderer, "sysadmin only message");

        servlet.service(request, response);
        verify(response).setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    private void whenUserIsLoggedInAsAdmin() {
        when(adminUIAuthenticator.checkAdminUIAccessBySessionOrCurrentUser(request)).thenReturn(true);
    }
}