package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.oauth.auth.OAuthResponseHandler;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 * Tests for the OAuth response handler.
 */
public class OAuthResponseHandlerTest {
    private static final String BASE_URL = "http://localhost:8080/";
    private static final String REQUEST_URL = BASE_URL + "servlet";
    private ResponseHandler<Response> applicationLinkResponseHandler;
    private ApplicationLinkRequest wrappedRequest;
    private ApplicationId applicationId;

    @Before
    public void createMocks() {
        applicationLinkResponseHandler = mock(ResponseHandler.class);
        wrappedRequest = mock(ApplicationLinkRequest.class);
    }

    @Test
    public void shouldHandleNormalResponse() throws ResponseException {
        OAuthResponseHandler handler = new OAuthResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, applicationId, true);
        Response response = mock(Response.class);
        when(response.getStatusCode()).thenReturn(200);

        handler.handle(response);

        verify(applicationLinkResponseHandler).handle(response);
    }

    @Test
    public void shouldHandleRedirectResponse() throws ResponseException {
        OAuthResponseHandler handler = new OAuthResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, applicationId, true);

        Response response = mock(Response.class);
        when(response.getHeader("location")).thenReturn("redirected");
        when(response.getStatusCode()).thenReturn(302);

        handler.handle(response);

        verify(wrappedRequest).setUrl(BASE_URL + "redirected");
        verify(wrappedRequest).execute(handler);
    }

    @Test
    public void shouldHandleResponseNormallyForEmptyLocationRedirect() throws ResponseException {
        OAuthResponseHandler handler = new OAuthResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, applicationId, true);

        Response response = mock(Response.class);
        when(response.getStatusCode()).thenReturn(302);
        when(response.getHeader("location")).thenReturn(null);

        handler.handle(response);

        verify(applicationLinkResponseHandler).handle(response);
        verify(wrappedRequest, never()).setUrl("new location");
        verify(wrappedRequest, never()).execute(handler);
    }

    @Test
    public void shouldNotFollowRedirectsWhenFollowRedirectsIsFalse() throws ResponseException {
        OAuthResponseHandler handler = new OAuthResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, applicationId, false);

        Response response = mock(Response.class);
        when(response.getHeader("location")).thenReturn("new location");
        when(response.getStatusCode()).thenReturn(302);

        handler.handle(response);

        verify(applicationLinkResponseHandler).handle(response);
        verify(wrappedRequest, never()).setUrl("new location");
        verify(wrappedRequest, never()).execute(handler);
    }

    @Test
    public void shouldNotFollowRedirectsWhenMaximumReached() throws ResponseException {
        OAuthResponseHandler handler = new OAuthResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, applicationId, true);

        Response response = mock(Response.class);
        when(response.getHeader("location")).thenReturn("redirected");
        when(response.getStatusCode()).thenReturn(302);

        handler.handle(response);
        handler.handle(response);
        handler.handle(response);
        handler.handle(response);

        // Note, the final response handler is called once, after the redirect behaviour is invoked 3 times.
        verify(applicationLinkResponseHandler, times(1)).handle(response);
        verify(wrappedRequest, times(3)).setUrl(BASE_URL + "redirected");
        verify(wrappedRequest, times(3)).execute(handler);
    }
}