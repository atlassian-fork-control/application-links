// assertion-error does not support AMD define() flavour
define('applinks/test/assertion-error', [
    'assertion-error'
], function(
    AssertionErrorLoaded
) {
    return window.AssertionError;
});