define('applinks/test/mock-declaration', [
    'applinks/lib/lodash',
    'object-path'
], function(
    _,
    objectPath
) {
    function createMockObject(stubber, properties, original) {
        var mock = _.extend({}, _.isFunction(original) ? original() : original);
        _.each(properties, function(key) {
            var propertyValue = objectPath.get(mock, key);
            if (_.isFunction(propertyValue)) {
                stubber.spy(propertyValue);
            } else if (_.isUndefined(propertyValue)) {
                objectPath.set(mock, key, stubber.stub());
            } else {
                throw new Error('Expected function or undefined at mock.' + key + ', but was: '
                + objectPath.get(mock, key));
            }
        });
        return mock;
    }

    function MockDeclaration(properties, original) {
        if (!_.isArray(properties)) {
            throw new Error('Expected an array, was: ' + properties);
        }
        this._properties = properties;
        this._original = original || {};
    }

    MockDeclaration.prototype.withConstructor = function() {
        this._withConstructor = true;
        return this;
    };

    MockDeclaration.prototype.createMock = function(stubber) {
        if (this._withConstructor) {
            var mock = createMockObject(stubber, this._properties, this._original);
            var mockConstructor = stubber.stub();
            mockConstructor.answer = mock;
            mockConstructor.returns(mock);
            return mockConstructor;
        } else {
            return createMockObject(stubber, this._properties, this._original);
        }
    };

    return MockDeclaration;
});