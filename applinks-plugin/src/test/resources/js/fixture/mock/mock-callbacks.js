define('applinks/test/mock-callbacks', [
    'applinks/lib/lodash',
    'applinks/test/assertion-error',
    'applinks/test/hamjest'
], function(
    _,
    AssertionError,
    __
) {
    function _getRegisterArgsAt(assert, callbackStub, index) {
        assert.assertThat(callbackStub, __.calledTimes(__.greaterThan(index)));
        var call = callbackStub.getCall(index);
        assert.assertThat(call.args[0], __.func());
        return callbackStub.getCall(index).args;
    }

    function _getEventRegisterArgsById(MockEventBus, eventId) {
        var call = _.find(MockEventBus.on.args, function(callArgs) {
            return callArgs[0] == eventId;
        });
        if (!call) {
            throw new AssertionError('call to on() with event ID "' + eventId + '" not found');
        }
        return call;
    }

    return {

        assertEventCallback: function(assert, MockEventBus, eventId) {
            assert.assertThat(MockEventBus.on, __.calledWith(eventId, __.func()));
        },

        getCallback: function(assert, callbackStub) {
            return this.getCallbackAt(assert, callbackStub, 0);
        },

        getCallbackAt: function(assert, callbackStub, index) {
            return _getRegisterArgsAt(assert, callbackStub, index)[0];
        },

        getEventCallbackById: function(MockEventBus, eventId) {
            return _getEventRegisterArgsById(MockEventBus, eventId)[1];
        },

        runCallback: function(assert, callbackStub, args) {
            this.runCallbackAt(assert, callbackStub, 0, args);
        },

        runCallbacks: function(assert, callbackStub, args) {
            this.runCallbacksAt(assert, callbackStub, 0, args);
        },

        /**
         * Run callback provided on `index`-th call to the `callbackStub`, using `args` as arguments to the callback
         *
         * @param assert QUnit assert
         * @param callbackStub callback stub to examine
         * @param index index of the call to register the callback
         * @param args args to use to call back
         */
        runCallbackAt: function(assert, callbackStub, index, args) {
            var registerArgs = _getRegisterArgsAt(assert, callbackStub, index);
            registerArgs[0].apply(this, args);
        },

        runCallbacksAt: function(assert, callbackStub, index, args) {
            var registerArgs = _getRegisterArgsAt(assert, callbackStub, index);
            _.each(registerArgs, function(callback) {
                callback.apply(this, args);
            });
        },

        runEventCallback: function(assert, MockEventBus, eventId, args) {
            var registerArgs = _getEventRegisterArgsById(MockEventBus, eventId);
            var context = registerArgs.length > 2 ? registerArgs[2] : this;
            registerArgs[1].apply(context, args);
        },

        runAllCallbacks: function(assert, callbackStub, callbackCount, args) {
            for (i = 0; i < callbackCount; i++) {
                this.runCallbacksAt(assert, callbackStub, i, args)
            }
        }
    }
});