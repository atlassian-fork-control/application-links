define('applinks/test/hamjest-string', [
    'applinks/lib/lodash',
    'hamjest'
], function(
    _,
    __
) {
    function StringContainsInOrder() {
        var expectedSubstrings = _.toArray(arguments);
        _.each(expectedSubstrings, function(arg) {
            __.assertThat(arg, __.is(__.string()));
        });

        return _.create(new __.string(), {
            matchesSafely: function (actual) {
                var fromIndex = 0;
                _.each(expectedSubstrings, function(substring) {
                    if (fromIndex == -1) {
                        return;
                    }
                    fromIndex = actual.indexOf(substring, fromIndex);
                });

                return fromIndex >= 0;
            },
            describeTo: function (description) {
                description
                    .append('a string containing in order: ')
                    .appendValue(expectedSubstrings);
            },
            describeMismatchSafely: function (actual, description) {
                description
                    .append('was ')
                    .appendValue(actual);
            },
            getExpectedForDiff: function () { return expectedSubstrings.toString(); },
            formatActualForDiff: function (actual) { return actual; }
        });
    }

    return {
        stringContainsInOrder: StringContainsInOrder
    };
});