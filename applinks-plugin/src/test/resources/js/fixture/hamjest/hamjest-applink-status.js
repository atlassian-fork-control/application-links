define('applinks/test/hamjest-applink-status', [
    'applinks/lib/lodash',
    'hamjest'
], function(
    _,
    __
) {
    var STATUS_LOADED = 'statusLoaded';
    var STATUS = 'status';
    
    function applinkWithStatusLoading() {
        return __.allOf(
            __.hasProperty(STATUS_LOADED, false), 
            __.hasProperty(STATUS, __.undefined())
        );
    }
    
    function applinkWithStatus(statusMatcher) {
        return __.allOf(
            __.hasProperty(STATUS_LOADED, true), 
            __.hasProperty(STATUS, statusMatcher)
        );
    }

    function statusWorking() {
        return __.allOf(
            __.hasProperty('working', true), 
            __.hasProperty('error', __.undefined())
        );
    }
    
    function statusError(expectedError) {
        return __.allOf(
            __.hasProperty('working', false), 
            __.hasProperty('error', errorMatching(expectedError))
        );
    }

    function errorMatching(expectedError) {
        return __.allOf(
            __.hasProperty('type', expectedError.type),
            __.hasProperty('category', expectedError.category)
        );
    }

    /**
     * Common matchers for Applink Status.
     */
    return {
        // applink matchers
        applinkWithStatusLoading: applinkWithStatusLoading,
        applinkWithStatus: applinkWithStatus,

        // status matchers
        statusWorking: statusWorking,
        statusError: statusError
    };
});