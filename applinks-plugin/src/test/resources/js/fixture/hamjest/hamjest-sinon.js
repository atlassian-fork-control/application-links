define('applinks/test/hamjest-sinon', [
    'applinks/lib/lodash',
    'hamjest',
    'applinks/test/hamjest-aggregate-matchers'
], function(
    _,
    __,
    applyForMatcherArray
) {

    /**
     * @param args expected call args (values or matchers), can be an array of matchers
     * @returns {Matcher} Sinon stub matcher to verify that there was a call with `args`
     */
    function stubCalledWith(args) {
        var arrayContains = applyForMatcherArray(__.contains).apply(this, _.toArray(arguments));
        return new __.FeatureMatcher(__.hasItem(arrayContains), 'call with args: ', 'args');
    }

    // "called with" applicable to a single Sinon call, where "args" are specific call arguments
    function _callCalledWith() {
        return new __.FeatureMatcher(applyForMatcherArray(__.contains).apply(this, _.toArray(arguments)),
            'call with args: ', 'args');
    }

    /**
     * @param index {number} index of the expected call
     * @param args expected call args (values or matchers), can be an array of matchers
     * @returns {Matcher} Sinon stub matcher to verify that call at `index` was performed with `args`
     */
    function calledWithAt(index, args) {
        var call = arguments[0];
        if (!_.isNumber(call)) {
            throw new Error('Expected a number as first argument, was: ' + call);
        }
        var itemsOrMatchers = _.toArray(arguments).slice(1, arguments.length);
        return new __.FeatureMatcher(_callCalledWith.apply(this, itemsOrMatchers),
            'Sinon call [' + call + ']:', 'call[' + call + ']',
            function(actual) {
                var sinonCall = actual.getCall(call);
                if (!sinonCall) {
                    // return "mock" call to let assertions run
                    return {args: []};
                } else {
                    return sinonCall;
                }
            });
    }

    function calledWithNew() {
        return new __.FeatureMatcher(__.truthy(), 'Sinon spy calledWithNew', 'calledWithNew', function(actual) {
            return actual.calledWithNew()
        });
    }

    function calledTimes(callCountValueOrMatcher) {
        return __.hasProperty('callCount', callCountValueOrMatcher);
    }

    function notCalled() {
        return calledTimes(0);
    }

    function calledOnce() {
        return calledTimes(1);
    }

    function calledTwice() {
        return calledTimes(2);
    }

    function calledThrice() {
        return calledTimes(3);
    }

    function calledTimesWith() {
        var callCount = arguments[0];
        if (!_.isNumber(callCount)) {
            throw new Error('Expected a number as first argument, was: ' + callCount);
        }
        var itemsOrMatchers = _.toArray(arguments).slice(1, arguments.length);

        var allMatchers = [calledTimes(callCount)];
        for (i = 0; i < callCount; i++) {
            var calledWithArgs = [i].concat(itemsOrMatchers);
            allMatchers.push(calledWithAt.apply(this, calledWithArgs));
        }
        return applyForMatcherArray(__.allOf)(allMatchers);
    }

    function calledOnceWith() {
        return calledTimesWith.apply(this, [1].concat(_.toArray(arguments)));
    }

    function calledTwiceWith() {
        return calledTimesWith.apply(this, [2].concat(_.toArray(arguments)));
    }

    function calledThriceWith() {
        return calledTimesWith.apply(this, [3].concat(_.toArray(arguments)));
    }

    return {
        calledWith: stubCalledWith,
        calledWithNew: calledWithNew,
        calledWithAt: calledWithAt,

        calledTimes: calledTimes,
        notCalled: notCalled,
        calledOnce: calledOnce,
        calledTwice: calledTwice,
        calledThrice: calledThrice,

        calledTimesWith: calledTimesWith,
        calledOnceWith: calledOnceWith,
        calledTwiceWith: calledTwiceWith,
        calledThriceWith: calledThriceWith
    };
});