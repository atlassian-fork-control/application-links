define([
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest'
], function(
    _,
    QUnit,
    __
) {
    QUnit.module('applinks/lib/version', {
        require: {
            main: 'applinks/lib/version'
        }
    });

    QUnit.test('Invalid major', function(assert, Version) {
        function invalidConstructor() {
            new Version('not an int', 4, 3);
        }
        assert.assertThat(invalidConstructor, __.throws(__.error('major: expected a number, was: <not an int>')));
    });

    QUnit.test('Invalid minor', function(assert, Version) {
        function invalidConstructor() {
            new Version(5, 'not an int', 3);
        }
        assert.assertThat(invalidConstructor, __.throws(__.error('minor: expected a number, was: <not an int>')));
    });

    QUnit.test('Invalid bugfix', function(assert, Version) {
        function invalidConstructor() {
            new Version(5, 4, 'not an int');
        }
        assert.assertThat(invalidConstructor, __.throws(__.error('bugfix: expected a number, was: <not an int>')));
    });

    // NOTE: we can use __.equalTo because it does deep equals comparison using major/minor/bugfix Version fields

    QUnit.test('Missing bugfix treated as 0', function(assert, Version) {
        assert.assertThat(new Version(5, 4), __.equalTo(new Version(5, 4, 0)));
    });

    QUnit.test('Missing bugfix and minor treated as 0', function(assert, Version) {
        assert.assertThat(new Version(5), __.equalTo(new Version(5, 0)));
        assert.assertThat(new Version(5), __.equalTo(new Version(5, 0, 0)));
    });

    QUnit.test('Float version component', function(assert, Version) {
        assert.assertThat(new Version(5, 4, 3.2), __.equalTo(new Version(5, 4, 3)));
        assert.assertThat(new Version(5, 4.3, 3), __.equalTo(new Version(5, 4, 3)));
        assert.assertThat(new Version(5.4, 4, 3), __.equalTo(new Version(5, 4, 3)));
    });

    QUnit.test('Invalid version strings', function(assert, Version) {
        assert.assertThat(_.partial(Version.parse, 'NaN.2.3'), __.throws(__.error('major: expected a number, was: <NaN>')));
        assert.assertThat(_.partial(Version.parse, '1.NaN.3'), __.throws(__.error('minor: expected a number, was: <NaN>')));
        assert.assertThat(_.partial(Version.parse, '1.2.NaN'), __.throws(__.error('bugfix: expected a number, was: <NaN>')));
        assert.assertThat(_.partial(Version.parse, undefined),
            __.throws(__.error('versionString: expected a non-empty string')));
        assert.assertThat(_.partial(Version.parse, 'invalid', 'my version'),
            __.throws(__.error('my version: expected <major>.<minor>.<bugfix> string, was: <invalid>')));
        assert.assertThat(_.partial(Version.parse, '1.2', 'my version'),
            __.throws(__.error('my version: expected <major>.<minor>.<bugfix> string, was: <1.2>')));
        assert.assertThat(_.partial(Version.parse, '1.2.3.4', 'my version'),
            __.throws(__.error('my version: expected <major>.<minor>.<bugfix> string, was: <1.2.3.4>')));
    });

    QUnit.test('compareTo() invalid arguments', function(assert, Version) {
        assert.assertThat(_.partial(new Version(5, 4, 3).compareTo, 'invalid'),
            __.throws(__.error('that: expected a Version object, was: <invalid>')));
        assert.assertThat(_.partial(new Version(5, 4, 3).compareTo, undefined),
            __.throws(__.error('that: expected a Version object, was: <undefined>')));
    });

    QUnit.test('compareTo() greater than', function(assert, Version) {
        assert.assertThat(new Version(5, 4, 3).compareTo(new Version(5, 4, 2)), __.equalTo(1));
        assert.assertThat(new Version(5, 4, 3).compareTo(new Version(5, 4)), __.equalTo(1));
        assert.assertThat(new Version(5, 4).compareTo(new Version(5)), __.equalTo(1));
        assert.assertThat(new Version(5, 4, 3).compareTo(new Version(4, 5, 6)), __.equalTo(1));
    });

    QUnit.test('compareTo() equals', function(assert, Version) {
        assert.assertThat(new Version(5, 4, 3).compareTo(new Version(5, 4, 3)), __.equalTo(0));
        assert.assertThat(new Version(5, 4, 0).compareTo(new Version(5, 4)), __.equalTo(0));
        assert.assertThat(new Version(5, 0, 0).compareTo(new Version(5)), __.equalTo(0));
        assert.assertThat(new Version(5, 0).compareTo(new Version(5)), __.equalTo(0));
    });

    QUnit.test('compareTo() less than', function(assert, Version) {
        assert.assertThat(new Version(5, 4, 2).compareTo(new Version(5, 4, 3)), __.equalTo(-1));
        assert.assertThat(new Version(5, 4).compareTo(new Version(5, 4, 3)), __.equalTo(-1));
        assert.assertThat(new Version(5).compareTo(new Version(5, 4)), __.equalTo(-1));
        assert.assertThat(new Version(4, 5, 6).compareTo(new Version(5, 4, 3)), __.equalTo(-1));
    });

    QUnit.test('equals()', function(assert, Version) {
        assert.assertThat(new Version(5, 4, 3).equals(new Version(5, 4, 3)), __.truthy());
        assert.assertThat(new Version(5, 4, 0).equals(new Version(5, 4)), __.truthy());
        assert.assertThat(new Version(5, 0, 0).equals(new Version(5)), __.truthy());
        assert.assertThat(new Version(5, 0).equals(new Version(5)), __.truthy());
    });

    QUnit.test('greaterThan()', function(assert, Version) {
        assert.assertThat(new Version(5, 4, 3).greaterThan(new Version(5, 4, 2)), __.truthy());
        assert.assertThat(new Version(5, 4, 3).greaterThan(new Version(5, 4)), __.truthy());
        assert.assertThat(new Version(5, 4).greaterThan(new Version(5)), __.truthy());
        assert.assertThat(new Version(5, 4, 3).greaterThan(new Version(4, 5, 6)), __.truthy());
    });

    QUnit.test('lessThan()', function(assert, Version) {
        assert.assertThat(new Version(5, 4, 2).lessThan(new Version(5, 4, 3)), __.truthy());
        assert.assertThat(new Version(5, 4).lessThan(new Version(5, 4, 3)), __.truthy());
        assert.assertThat(new Version(5).lessThan(new Version(5, 4)), __.truthy());
        assert.assertThat(new Version(4, 5, 6).lessThan(new Version(5, 4, 3)), __.truthy());
    });

    QUnit.test('toMajor()', function(assert, Version) {
        assert.assertThat(new Version(5, 4, 2).toMajor(), __.equalTo(new Version(5)));
        assert.assertThat(new Version(5, 4).toMajor(), __.equalTo(new Version(5)));
        assert.assertThat(new Version(5).toMajor(), __.equalTo(new Version(5)));
    });

    QUnit.test('toMinor()', function(assert, Version) {
        assert.assertThat(new Version(5, 4, 2).toMinor(), __.equalTo(new Version(5, 4)));
        assert.assertThat(new Version(5, 4).toMinor(), __.equalTo(new Version(5, 4)));
        assert.assertThat(new Version(5).toMinor(), __.equalTo(new Version(5)));
    });

    QUnit.test('comparator() invalid arguments', function(assert, Version) {
        assert.assertThat(_.partial(Version.comparator, 'invalid', new Version(5, 4, 3)),
            __.throws(__.error('versionOne: expected a Version object, was: <invalid>')));
        assert.assertThat(_.partial(Version.comparator, undefined, new Version(5, 4, 3)),
            __.throws(__.error('versionOne: expected a Version object, was: <undefined>')));

        assert.assertThat(_.partial(Version.comparator, new Version(5, 4, 3), 'invalid'),
            __.throws(__.error('versionTwo: expected a Version object, was: <invalid>')));
        assert.assertThat(_.partial(Version.comparator, new Version(5, 4, 3), undefined),
            __.throws(__.error('versionTwo: expected a Version object, was: <undefined>')));
    });

    QUnit.test('comparator()', function(assert, Version) {
        assert.assertThat(Version.comparator(new Version(5, 4, 3), new Version(5, 4, 2)), __.equalTo(1));
        assert.assertThat(Version.comparator(new Version(5, 4, 3), new Version(4, 5, 6)), __.equalTo(1));
        assert.assertThat(Version.comparator(new Version(5, 4, 3), new Version(5, 4, 3)), __.equalTo(0));
        assert.assertThat(Version.comparator(new Version(5, 4, 2), new Version(5, 4, 3)), __.equalTo(-1));
        assert.assertThat(Version.comparator(new Version(4, 5, 6), new Version(5, 4, 3)), __.equalTo(-1));
    });
});