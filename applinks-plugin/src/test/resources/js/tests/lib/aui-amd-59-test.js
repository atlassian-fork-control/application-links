define([
    'applinks/test/qunit',
    'applinks/test/hamjest'
], function(
    QUnit,
    __
) {
    QUnit.module('applinks/lib/aui', {
        require: {
            main: 'applinks/lib/aui',
            Version: 'applinks/lib/version'
        },
        mocks: {
            window: QUnit.moduleMock('applinks/lib/window', function() {return {AJS: {version: '5.9.13'}};})
        }
    });

    QUnit.test('AUI 5.9.13', function(assert, AJS) {
        assert.assertThat(AJS.versionDetails.equals(new this.Version(5, 9, 13)), __.truthy());
        assert.assertThat(AJS.versionDetails.is58, __.falsy());
        assert.assertThat(AJS.versionDetails.is59, __.truthy());
    });
});