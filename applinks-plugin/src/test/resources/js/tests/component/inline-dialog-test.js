define([
    'applinks/lib/jquery',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/test/mock-callbacks'
], function(
    $,
    QUnit,
    __,
    MockDeclaration
) {

    QUnit.module('applinks/component/inline-dialog', {
        require: {
            main: 'applinks/component/inline-dialog'
        },
        mocks: {
            AJS: QUnit.moduleMock('applinks/lib/aui'),
            skate: QUnit.moduleMock('applinks/lib/skate', new MockDeclaration(['init'])),
            dialogFocus: QUnit.moduleMock('applinks/common/dialog-focus', new MockDeclaration(['defaultFocus']))
        },
        templates: {
            "applinks.inlineDialog.popup" : function(options) {
               return '<div id="' + options.id + '" data-auiVersion="' + options.auiVersion + '">' + options.contents + '</div>';
            }
        },
        beforeEach: function() {
            $('body').empty();
        }
    });

    QUnit.test('Should call defaultFocus on dialog show for AUI 5.8', function(assert, InlineDialog, mocks) {
        mocks.AJS.versionDetails = { is58: true, is59: false };
        var inlineDialog = new InlineDialog('dialog-123', '<div id="content"/>', {});
        inlineDialog.$popup.trigger('aui-layer-show');
        assert.assertThat(mocks.dialogFocus.defaultFocus, __.calledOnce());
    });

    QUnit.test('Should call defaultFocus on dialog show for AUI 5.9', function(assert, InlineDialog, mocks) {
        mocks.AJS.versionDetails = { is58: false, is59: true };
        var inlineDialog = new InlineDialog('dialog-123', '<div id="content"/>', {});
        inlineDialog.$popup.trigger('aui-show');
        assert.assertThat(mocks.dialogFocus.defaultFocus, __.calledOnce());
    });

    QUnit.test('Should call defaultFocus on dialog show for AUI post 5.9', function(assert, InlineDialog, mocks) {
        mocks.AJS.versionDetails = { is58: false, is59: false };
        var inlineDialog = new InlineDialog('dialog-123', '<div id="content"/>', {});
        inlineDialog.$popup.trigger('aui-show');
        assert.assertThat(mocks.dialogFocus.defaultFocus, __.calledOnce());
    });
});
