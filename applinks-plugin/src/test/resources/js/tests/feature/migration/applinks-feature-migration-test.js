define([
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/test/mock-callbacks'
], function(
    $,
    _,
    QUnit,
    __,
    MockDeclaration,
    MockCallbacks
) {
    QUnit.module('applinks/feature/migration', {
        require: {
            main: 'applinks/feature/migration',
            jquery: 'applinks/lib/jquery'
        },
        mocks: {
            main: QUnit.mock(function(){
              return {
                  id: '1234',
                  getAdminUrl: function() {
                      return 'admin.url';
                  },
                  get: function() {
                      return 'name';
                  }
              }
            }),
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['contextPath', 'dialog2'])),
            dialogHelper :  QUnit.moduleMock('applinks/common/dialog-helper', new MockDeclaration(['isAnyDialogOpen', 'getContainer'])),
            dialog2: QUnit.mock(new MockDeclaration(['show', 'hide', 'remove'])),
            console: QUnit.moduleMock('applinks/lib/console', new MockDeclaration(['debug'])),
            ApplinksRest: QUnit.moduleMock('applinks/common/rest', new MockDeclaration(['V3.migration'])),
            RestRequest: QUnit.mock(new MockDeclaration(['post'])),
            promise: QUnit.mock(new MockDeclaration(['done', 'fail']))
        },
        templates: {
            "applinks.migration.localRemovalDialogContent": function(data) {
                return '<div id="' + data.id + '"><button id="' + data.id + '-close-button' +  '"></button><button id="' + data.id + '-ok-button' +  '"></button>';
            }
        },
        beforeEach: function(assert, Migration, applink, mocks) {
            this.fixture.append('<div id="dialog-test-container"></div>');

            mocks.dialogHelper.isAnyDialogOpen.returns(false);
            mocks.dialogHelper.getContainer.returns('#dialog-test-container');

            mocks.AJS.dialog2.returns(mocks.dialog2);
            mocks.AJS.contextPath.returns("http://server.org");

            applink.fetchStatus = this.sandbox.stub();
            applink.set = this.sandbox.stub();

            mocks.ApplinksRest.V3.migration.returns(mocks.RestRequest);
            mocks.RestRequest.post.returns(mocks.promise);
            mocks.promise.done.returns(mocks.promise);
            mocks.promise.fail.returns(mocks.promise);

            mocks.ajax = this.sandbox.stub(this.jquery, 'ajax');
            mocks.ajax.responsePromise = new MockDeclaration(['always']).createMock(this.sandbox);
            mocks.ajax.responsePromise.always.returns(mocks.ajax.responsePromise);
            mocks.ajax.returns(mocks.ajax.responsePromise);
        }
    });

    QUnit.test('should migrate and refresh applink', function(assert, Migration, applink, mocks) {
        Migration.migrate(applink);
        assert.assertThat(applink.set, __.calledOnceWith({statusLoaded: false}));
        assert.assertThat(mocks.ApplinksRest.V3.migration, __.calledOnceWith(applink.id));
        MockCallbacks.runCallback(assert, mocks.promise.done, [{
            incoming: {
                trusted: false,
                oauth: false
            }
        }]);
        assert.assertThat(applink.fetchStatus, __.calledOnce());
    });

    QUnit.test('should delete local trusted after migrate if remote has MIGRATION_API', function(assert, Migration, applink, mocks) {
        Migration.migrate(applink);
        assert.assertThat(applink.set, __.calledOnceWith({statusLoaded: false}));
        assert.assertThat(mocks.ApplinksRest.V3.migration, __.calledOnceWith(applink.id));
        MockCallbacks.runCallback(assert, mocks.promise.done, [{
            incoming: {
                trusted: true,
                oauth: true
            },
            capabilities: ['STATUS_API', 'MIGRATION_API']
        }]);
        MockCallbacks.runCallback(assert, mocks.ajax.responsePromise.always);

        assert.assertThat(mocks.ajax, __.calledOnceWith({
            url: 'http://server.org/plugins/servlet/applinks/auth/conf/trusted/autoconfig/' + applink.id,
            type: 'DELETE'
        }));
        assert.assertThat(applink.fetchStatus, __.calledOnce());
    });

    QUnit.test('should delete local trusted for manual removal if remote has MIGRATION_API', function(assert, Migration, applink, mocks) {
        Migration.removeLocal(applink);
        assert.assertThat(mocks.dialog2.show, __.calledOnce());
        var $okButton = this.fixture.find('#removal-dialog-' + applink.id + '-ok-button');

        $okButton.trigger('click');

        assert.assertThat(mocks.ApplinksRest.V3.migration, __.calledOnceWith(applink.id));
        MockCallbacks.runCallback(assert, mocks.promise.done, [{
            capabilities: ['STATUS_API', 'MIGRATION_API']
        }]);
        MockCallbacks.runCallback(assert, mocks.ajax.responsePromise.always);

        assert.assertThat(mocks.ajax, __.calledOnceWith({
            url: 'http://server.org/plugins/servlet/applinks/auth/conf/trusted/autoconfig/' + applink.id,
            type: 'DELETE'
        }));
        assert.assertThat(applink.fetchStatus, __.calledOnce());
    });

    QUnit.test('should not delete local trusted for manual removal if remote does not have MIGRATION_API', function(assert, Migration, applink, mocks) {
        Migration.removeLocal(applink);
        assert.assertThat(mocks.dialog2.show, __.calledOnce());
        var $okButton = this.fixture.find('#removal-dialog-' + applink.id + '-ok-button');

        $okButton.trigger('click');

        assert.assertThat(mocks.ApplinksRest.V3.migration, __.calledOnceWith(applink.id));
        MockCallbacks.runCallback(assert, mocks.promise.done, [{
            capabilities: ['STATUS_API']
        }]);
        assert.assertThat(mocks.ajax, __.notCalled());
        assert.assertThat(applink.fetchStatus, __.calledOnce());
    });

    QUnit.test('should not delete local trusted when closing dialog', function(assert, Migration, applink, mocks) {
        Migration.removeLocal(applink);
        assert.assertThat(mocks.dialog2.show, __.calledOnce());
        this.fixture.find('#removal-dialog-' + applink.id + '-close-button').trigger('click');

        assert.assertThat(mocks.ApplinksRest.V3.migration, __.notCalled());
        assert.assertThat(mocks.ajax, __.notCalled());
        assert.assertThat(applink.fetchStatus, __.notCalled());
    });
});
