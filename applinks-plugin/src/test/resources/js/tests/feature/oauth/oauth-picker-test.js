define([
    'applinks/lib/jquery',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/feature/oauth-picker-model'
], function(
    $,
    QUnit,
    __,
    MockDeclaration,

    // OAuthPickerModel should be inside Qunit module 'require' section usually but because I need access to it
    // for the templates it must be here instead.
    OAuthPickerModel
) {
    QUnit.module('applinks/feature/oauth-picker', {
        require: {
            main: 'applinks/feature/oauth-picker'
        },
        mocks: {
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['I18n.getText', 'contextPath'])),
            AUISelect: QUnit.moduleMock('aui/select', new MockDeclaration([]))
        },
        templates: {
            "applinks.feature.oauth.dropdown": function (params) {
                return ('<aui-select id="@componentId-id" name="@componentId" class="@componentId-dropdown oauth-dropdown">' +
                            '<aui-option value="0">Disabled</aui-option>' +
                            '<aui-option value="1">OAuth</aui-option>' +
                            '<aui-option value="2">OAuth (Impersonation)</aui-option>' +
                        '</aui-select>')
                            .replace(/@componentId/g, params.componentId);
            }
        },
        beforeEach: function(assert, Initializer, mocks) {
            this.fixture.append('<div id="test-container"></div>');
            var OAuthPicker = this.main;

            // OAuthConfig parameter can be anything for the test since the real soy template is not being used
            this.dropdown = new OAuthPicker.View('#test-container', 'outgoing-auth', new OAuthPickerModel(true, true, false));
        },
        setSelectedValue: function(oauthId) {
            var dropdown = this.dropdown.getDropdown();
            dropdown[0].value = oauthId + '';
        }
    });

    QUnit.test('Test when OAuth impersonation config is selected', function(assert) {
        this.setSelectedValue(OAuthPickerModel.OAUTH_IMPERSONATION_ID);
        assert.assertThat(this.dropdown.getSelectedValue(), __.is(OAuthPickerModel.OAUTH_IMPERSONATION_ID + ''));
        assert.assertThat(this.dropdown.getModel().equals(new OAuthPickerModel(true, true, true)), __.truthy());
    });

    QUnit.test('Test when OAuth config is selected', function(assert) {
        this.setSelectedValue(OAuthPickerModel.OAUTH_ID);
        assert.assertThat(this.dropdown.getSelectedValue(), __.is(OAuthPickerModel.OAUTH_ID + ''));
        assert.assertThat(this.dropdown.getModel().equals(new OAuthPickerModel(true, true, false)), __.truthy());
    });

    QUnit.test('Test when OAuth disabled config is selected', function(assert) {
        this.setSelectedValue(OAuthPickerModel.OAUTH_DISABLED_ID);
        assert.assertThat(this.dropdown.getSelectedValue(), __.is(OAuthPickerModel.OAUTH_DISABLED_ID + ''));
        assert.assertThat(this.dropdown.getModel().equals(new OAuthPickerModel(false, false, false)), __.truthy());
    });
});