define([
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-common',
    'applinks/test/mock-declaration',
    'applinks/test/mock-model',
    'applinks/test/template-params'
], function(
    QUnit,
    __,
    ApplinksCommonMocks,
    MockDeclaration,
    ApplinksModelMocks,
    TemplateParams
) {
    QUnit.module('applinks/feature/v3/list-views', {
        require: {
            main: 'applinks/feature/v3/list-views',
            ApplinkModel: 'applinks/model/applink'
        },
        mocks: {
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['I18n.getText'])),
            window: QUnit.moduleMock('applinks/lib/window', function() {return {location: {}}}),
            ApplinksProducts: ApplinksCommonMocks.mockProductsModule(QUnit),
            ApplinksUrls: QUnit.moduleMock('applinks/common/urls', new MockDeclaration(['Local.edit'])),
            // mocks for ApplinkModel
            ApplinksRest: ApplinksCommonMocks.mockRestModule(QUnit),
            ResponseHandlers: QUnit.moduleMock('applinks/common/response-handlers',
                new MockDeclaration(['done', 'fail']))
        },
        templates: {
            "applinks.feature.v3.list.templates.row": TemplateParams.storeParamsIn(
                '<tr class="applink-row"><td class="template-params-container"></td></tr>')
        },
        
        beforeEach: function(assert, V3ListViews, mocks) {
            mocks.applink = ApplinksModelMocks.setUpModel(this, new this.ApplinkModel.Applink({id: '123', type: 'jira'}));
            mocks.ApplinksUrls.Local.edit.returns('edit_url');
        }
    });

    QUnit.test('Should not include legacy edit action by default with no query params',
        function(assert, V3ListViews, mocks) {
            mocks.window.location.search = '';

            var rowHtml = new V3ListViews.Row({model: mocks.applink}).render();

            var templateParams = TemplateParams.getTemplateParams(assert, rowHtml);
            assertApplinkParams(assert, templateParams);
            assert.assertThat(templateParams, __.hasProperty('includeLegacyEdit', false));
        });

    QUnit.test('Should not include legacy edit action by default with unrelated query params',
        function(assert, V3ListViews, mocks) {
            mocks.window.location.search = '?foo=bar';

            var rowHtml = new V3ListViews.Row({model: mocks.applink}).render();

            var templateParams = TemplateParams.getTemplateParams(assert, rowHtml);
            assertApplinkParams(assert, templateParams);
            assert.assertThat(templateParams, __.hasProperty('includeLegacyEdit', false));
        });

    QUnit.test('Should render legacy edit action when legacyEdit query param is present', 
        function(assert, V3ListViews, mocks) {
        mocks.window.location.search = '?foo=bar&legacyEdit=true';

            var rowHtml = new V3ListViews.Row({model: mocks.applink}).render();

            var templateParams = TemplateParams.getTemplateParams(assert, rowHtml);
            assertApplinkParams(assert, templateParams);
            assert.assertThat(templateParams, __.hasProperty('includeLegacyEdit', true));
    });

    function assertApplinkParams(assert, params) {
        assert.assertThat(params, __.hasProperty('editUrl', 'edit_url'));
    }
});