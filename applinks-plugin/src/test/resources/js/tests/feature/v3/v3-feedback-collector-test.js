define([
    'applinks/lib/jquery',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    $,
    QUnit,
    __,
    MockDeclaration
) {
    QUnit.module('applinks/feature/v3/feedback-collector', {
        require: {
            main: 'applinks/feature/v3/feedback-collector'
        },
        mocks: {
            window: QUnit.moduleMock('applinks/lib/window', new MockDeclaration(['setTimeout']))
        },
        beforeEach: function(assert, FeedbackCollector, mocks) {
            $.ajax = function(settings) {
                settings.success();
            };

            mocks.window.document = $('<div id="mock-document"></div>');
            this.fixture.append($('<button id="applinks-feedback-button">'));
            this.fixture.append(mocks.window.document);
            FeedbackCollector.init();
        }
    });

    QUnit.test('ATL_JQ_PAGE_PROPS are defined', function(assert, FeedbackCollector, mocks) {
        assert.assertThat(mocks.window.ATL_JQ_PAGE_PROPS, __.is(__.defined()));
        assert.assertThat(mocks.window.ATL_JQ_PAGE_PROPS['35047165'], __.is(__.defined()));
    });

    QUnit.test('Verify callback for 35047165 handler', function(assert, FeedbackCollector, mocks) {
        var callback = this.sandbox.stub();
        assert.assertThat(mocks.window.ATL_JQ_PAGE_PROPS['35047165'].triggerFunction, __.is(__.func()));

        mocks.window.ATL_JQ_PAGE_PROPS['35047165'].triggerFunction(callback);
        $('#applinks-feedback-button').click();

        assert.assertThat(callback, __.calledOnce());
    });
});
