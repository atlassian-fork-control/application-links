define([
    'applinks/lib/jquery',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    $,
    QUnit,
    __,
    MockDeclaration
) {
    QUnit.module('applinks/feature/v3/ui', {
        require: {
            main: 'applinks/feature/v3/ui'
        },
        mocks: {
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['trigger']))
        }
    });

    QUnit.test('V3 UI is off', function(assert, V3Ui) {
        // #ual but not V3
        this.fixture.append($('<div id="ual" class="applinks-admin-v2">'));

        assert.assertThat(V3Ui.isOn(), __.falsy());
    });

    QUnit.test('V3 UI is on', function(assert, V3Ui) {
        // V3 container
        this.fixture.append($('<div id="ual" class="applinks-admin-v3">'));

        assert.assertThat(V3Ui.isOn(), __.truthy());
    });
});