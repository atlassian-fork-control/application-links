define([
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/test/template-params'
], function(
    QUnit,
    __,
    MockDeclaration,
    TemplateParams
) {
    QUnit.module('applinks/feature/status', {
        require: {
            main: 'applinks/feature/status',
            ApplinkErrors: 'applinks/feature/status/errors'
        },
        mocks: {
            main: QUnit.mock(function() { return {statusLoaded: false, status: undefined} }), // status not loaded to start with
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['contextPath']))
        },
        templates: {
            "applinks.status.loading": TemplateParams.storeParamsIn('<div class="loading" />'),
            "applinks.status.working": TemplateParams.storeParamsIn('<div class="working" />'),
            "applinks.status.error": TemplateParams.storeParamsIn('<div class="error" />')
        }
    });

    QUnit.test('Undefined options', function(assert, ApplinkStatus) {
        var brokenConstructor = function() {
            new ApplinkStatus.View();
        };

        assert.assertThat(brokenConstructor, __.throws(__.error('options: expected a value')));
    });

    QUnit.test('Undefined options.applink', function(assert, ApplinkStatus) {
        var brokenConstructor = function() {
            new ApplinkStatus.View({
                container: this.fixture
            });
        };

        assert.assertThat(brokenConstructor, __.throws(__.error('options.applink: expected a value')));
    });

    QUnit.test('Undefined options.container', function(assert, ApplinkStatus, mockApplink) {
        var brokenConstructor = function() {
            new ApplinkStatus.View({
                applink: mockApplink
            });
        };

        assert.assertThat(brokenConstructor, __.throws(__.error('options.container: expected a value')));
    });

    QUnit.test('Status loading', function(assert, ApplinkStatus, mockApplink) {
        new ApplinkStatus.View({
            applink: mockApplink,
            container: this.fixture
        });

        assert.assertThat(this.fixture.find('div.loading'), __.hasSize(1));
        assert.assertThat(this.fixture.attr('data-status-loaded'), __.equalTo('false'));
    });

    QUnit.test('Status working', function(assert, ApplinkStatus, mockApplink) {
        mockApplink.statusLoaded = true;
        mockApplink.status = {
            working: true
        };

        new ApplinkStatus.View({
            applink: mockApplink,
            container: this.fixture
        });

        assert.assertThat(this.fixture.find('div.working'), __.hasSize(1));
        assert.assertThat(this.fixture.attr('data-status-loaded'), __.equalTo('true'));
    });

    QUnit.test('Status error', function(assert, ApplinkStatus, mockApplink) {
        var error = this.ApplinkErrors.CONNECTION_REFUSED;
        mockApplink.statusLoaded = true;
        mockApplink.status = {
            working: false,
            error: error
        };

        new ApplinkStatus.View({
            applink: mockApplink,
            container: this.fixture
        });

        var $error = this.fixture.find('div.error');
        var templateParams = TemplateParams.getTemplateParams(assert, $error);
        assert.assertThat($error, __.hasSize(1));
        assert.assertThat(templateParams,
            __.hasProperty('status', statusWithError(this.ApplinkErrors.CONNECTION_REFUSED)));
        assert.assertThat('Should render as clickable by default', templateParams,
            __.hasProperty('clickable', __.truthy()));
        assert.assertThat(this.fixture.attr('data-status-loaded'), __.equalTo('true'));
    });

    QUnit.test('Status error explicitly clickable', function(assert, ApplinkStatus, mockApplink) {
        var error = this.ApplinkErrors.CONNECTION_REFUSED;
        mockApplink.statusLoaded = true;
        mockApplink.status = {
            working: false,
            error: error
        };

        new ApplinkStatus.View({
            applink: mockApplink,
            container: this.fixture,
            clickable: true
        });

        var $error = this.fixture.find('div.error');
        var templateParams = TemplateParams.getTemplateParams(assert, $error);
        assert.assertThat($error, __.hasSize(1));
        assert.assertThat(templateParams,
            __.hasProperty('status', statusWithError(this.ApplinkErrors.CONNECTION_REFUSED)));
        assert.assertThat('Should render as clickable when explicitly specified clickable=true', templateParams,
            __.hasProperty('clickable', __.truthy()));
        assert.assertThat(this.fixture.attr('data-status-loaded'), __.equalTo('true'));
    });

    QUnit.test('Status error non-clickable', function(assert, ApplinkStatus, mockApplink) {
        var error = this.ApplinkErrors.CONNECTION_REFUSED;
        mockApplink.statusLoaded = true;
        mockApplink.status = {
            working: false,
            error: error
        };

        new ApplinkStatus.View({
            applink: mockApplink,
            container: this.fixture,
            clickable: false
        });

        var $error = this.fixture.find('div.error');
        var templateParams = TemplateParams.getTemplateParams(assert, $error);
        assert.assertThat($error, __.hasSize(1));
        assert.assertThat(templateParams,
            __.hasProperty('status', statusWithError(this.ApplinkErrors.CONNECTION_REFUSED)));
        assert.assertThat('Should render as non-clickable with clickable=false', templateParams,
            __.hasProperty('clickable', __.falsy()));
        assert.assertThat(this.fixture.attr('data-status-loaded'), __.equalTo('true'));
    });

    QUnit.test('Status working extra classes', function(assert, ApplinkStatus, mockApplink) {
        mockApplink.statusLoaded = true;
        mockApplink.status = {
            working: true
        };

        new ApplinkStatus.View({
            applink: mockApplink,
            container: this.fixture,
            extraClasses: 'foo bar'
        });

        var statusElement = this.fixture.find('div.working');
        var templateParams = TemplateParams.getTemplateParams(assert, statusElement);
        assert.assertThat(statusElement, __.hasSize(1));
        assert.assertThat(templateParams, __.hasProperty('status', {working: true}));
        assert.assertThat('Should render extra classes', templateParams, __.hasProperty('extraClasses', 'foo bar'));
        assert.assertThat(this.fixture.attr('data-status-loaded'), __.equalTo('true'));
    });

    QUnit.test('Status loading extra classes', function(assert, ApplinkStatus, mockApplink) {
        new ApplinkStatus.View({
            applink: mockApplink,
            container: this.fixture,
            extraClasses: 'foo bar'
        });

        var statusElement = this.fixture.find('div.loading');
        var templateParams = TemplateParams.getTemplateParams(assert, statusElement);
        assert.assertThat(statusElement, __.hasSize(1));
        assert.assertThat(templateParams, __.not(__.hasProperty('status')));
        assert.assertThat('Should render extra classes', templateParams, __.hasProperty('extraClasses', 'foo bar'));
    });

    QUnit.test('Status error extra classes', function(assert, ApplinkStatus, mockApplink) {
        var error = this.ApplinkErrors.CONNECTION_REFUSED;
        mockApplink.statusLoaded = true;
        mockApplink.status = {
            working: false,
            error: error
        };

        new ApplinkStatus.View({
            applink: mockApplink,
            container: this.fixture,
            extraClasses: 'foo bar'
        });

        var statusElement = this.fixture.find('div.error');
        var templateParams = TemplateParams.getTemplateParams(assert, statusElement);
        assert.assertThat(statusElement, __.hasSize(1));
        assert.assertThat(templateParams,
            __.hasProperty('status', statusWithError(this.ApplinkErrors.CONNECTION_REFUSED)));
        assert.assertThat('Should render extra classes', templateParams, __.hasProperty('extraClasses', 'foo bar'));
        assert.assertThat(this.fixture.attr('data-status-loaded'), __.equalTo('true'));
    });

    function statusWithError(statusError) {
        return __.hasProperty('error', __.allOf(
            __.hasProperty('type', statusError.type),
            __.hasProperty('category', statusError.category)
        ));
    }
});