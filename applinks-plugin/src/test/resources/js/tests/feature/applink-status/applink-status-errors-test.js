define([
    'applinks/test/qunit',
    'applinks/test/hamjest'
], function(
    QUnit,
    __
) {
    QUnit.module('applinks/feature/status/errors', {
        require: {
            main: 'applinks/feature/status/errors'
        }
    });

    QUnit.test('equals()', function(assert, StatusErrors) {
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.equals(undefined), __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.equals(null), __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.equals({}), __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.equals({error: {category: 'NETWORK_ERROR'}}), __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.equals({category: 'NETWORK_ERROR'}), __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.equals(StatusErrors.UNKNOWN_HOST), __.falsy());

        assert.assertThat(StatusErrors.CONNECTION_REFUSED.equals({type: 'CONNECTION_REFUSED', category: 'NETWORK_ERROR'}),
            __.truthy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.equals(StatusErrors.CONNECTION_REFUSED), __.truthy());
    });

    QUnit.test('matches() for invalid values', function(assert, StatusErrors) {
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches(undefined), __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches(null), __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches({}), __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches({error: {category: 'NETWORK_ERROR'}}), __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches({category: 'NETWORK_ERROR'}), __.falsy());
    });

    QUnit.test('matches() for errors', function(assert, StatusErrors) {
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches({type: 'UNKNOWN_HOST', category: 'NETWORK_ERROR'}),
            __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches(StatusErrors.UNKNOWN_HOST), __.falsy());

        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches({type: 'CONNECTION_REFUSED', category: 'NETWORK_ERROR'}),
            __.truthy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches(StatusErrors.CONNECTION_REFUSED), __.truthy());
    });

    QUnit.test('matches() for statuses', function(assert, StatusErrors) {
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches({error: {type: 'UNKNOWN_HOST', category: 'NETWORK_ERROR'}}),
            __.falsy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches({error: StatusErrors.UNKNOWN_HOST}), __.falsy());

        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches({error: {type: 'CONNECTION_REFUSED', category: 'NETWORK_ERROR'}}),
            __.truthy());
        assert.assertThat(StatusErrors.CONNECTION_REFUSED.matches({error: StatusErrors.CONNECTION_REFUSED}), __.truthy());
    });

    QUnit.test('find() for invalid values', function(assert, StatusErrors) {
        assert.assertThat(StatusErrors.find(undefined), __.undefined());
        assert.assertThat(StatusErrors.find(null), __.undefined());
        assert.assertThat(StatusErrors.find({}), __.undefined());

        assert.assertThat(StatusErrors.find({error: {category: 'NETWORK_ERROR'}}), __.undefined());
        assert.assertThat(StatusErrors.find({category: 'NETWORK_ERROR'}), __.undefined());
        assert.assertThat(StatusErrors.find({error: {type: 'CONNECTION_REFUSED'}}), __.undefined());
        assert.assertThat(StatusErrors.find({type: 'CONNECTION_REFUSED'}), __.undefined());
    });

    QUnit.test('find() non existing error', function(assert, StatusErrors) {
        assert.assertThat(StatusErrors.find({error: {type: 'NO_SUCH_ERROR'}}), __.undefined());
        assert.assertThat(StatusErrors.find({type: 'NO_SUCH_ERROR'}), __.undefined());
    });

    QUnit.test('find() existing error', function(assert, StatusErrors) {
        assert.assertThat(StatusErrors.find(StatusErrors.CONNECTION_REFUSED),
            __.equalTo(StatusErrors.CONNECTION_REFUSED));
        assert.assertThat(StatusErrors.find({error: {type: 'CONNECTION_REFUSED', category: 'IRRELEVANT'}}),
            __.equalTo(StatusErrors.CONNECTION_REFUSED));
        assert.assertThat(StatusErrors.find({type: 'CONNECTION_REFUSED', category: 'IRRELEVANT'}),
            __.equalTo(StatusErrors.CONNECTION_REFUSED));
    });

    QUnit.test('isAccessTokenError() false values', function(assert, StatusErrors) {
        assert.assertThat(StatusErrors.isAccessTokenError(undefined), __.falsy());
        assert.assertThat(StatusErrors.isAccessTokenError(null), __.falsy());
        assert.assertThat(StatusErrors.isAccessTokenError({}), __.falsy());
        assert.assertThat(StatusErrors.isAccessTokenError({error: {category: 'ACCESS_ERROR'}}), __.falsy());
        assert.assertThat(StatusErrors.isAccessTokenError({category: 'ACCESS_ERROR'}), __.falsy());

        assert.assertThat(StatusErrors.isAccessTokenError({error: {type: 'INSUFFICIENT_REMOTE_PERMISSION', category: 'ACCESS_ERROR'}}),
            __.falsy());
        assert.assertThat(StatusErrors.isAccessTokenError({type: 'INSUFFICIENT_REMOTE_PERMISSION', category: 'ACCESS_ERROR'}),
            __.falsy());
        assert.assertThat(StatusErrors.isAccessTokenError(StatusErrors.INSUFFICIENT_REMOTE_PERMISSION),
            __.falsy());
    });

    QUnit.test('isAccessTokenError() matching values', function(assert, StatusErrors) {
        assert.assertThat(StatusErrors.isAccessTokenError({error: {type: 'LOCAL_AUTH_TOKEN_REQUIRED', category: 'ACCESS_ERROR'}}),
            __.truthy());
        assert.assertThat(StatusErrors.isAccessTokenError({type: 'LOCAL_AUTH_TOKEN_REQUIRED', category: 'ACCESS_ERROR'}),
            __.truthy());
        assert.assertThat(StatusErrors.isAccessTokenError(StatusErrors.LOCAL_AUTH_TOKEN_REQUIRED),
            __.truthy());

        assert.assertThat(StatusErrors.isAccessTokenError({error: {type: 'REMOTE_AUTH_TOKEN_REQUIRED', category: 'ACCESS_ERROR'}}),
            __.truthy());
        assert.assertThat(StatusErrors.isAccessTokenError({type: 'REMOTE_AUTH_TOKEN_REQUIRED', category: 'ACCESS_ERROR'}),
            __.truthy());
        assert.assertThat(StatusErrors.isAccessTokenError(StatusErrors.REMOTE_AUTH_TOKEN_REQUIRED),
            __.truthy());
    });
});