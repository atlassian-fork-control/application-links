define([
    'applinks/test/qunit',
    'applinks/test/hamjest'
], function(
    QUnit,
    __
) {
    QUnit.module('applinks/common/browser', {
        require: {
            main: 'applinks/common/browser'
        },
        mocks: {
            Window: QUnit.moduleMock('applinks/lib/window', function() {
                return { navigator : { userAgent: '' } }
                })
        }
    });

    QUnit.test('isMicrosoft() should be true for version < 11 (IE 10)', function(assert, Browser, mocks) {
        mocks.Window.navigator.userAgent = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
        assert.assertThat(Browser.isMicrosoft(), __.truthy());
    });

    QUnit.test('isMicrosoft() should be true for IE 11', function(assert, Browser, mocks) {
        mocks.Window.navigator.userAgent = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
        assert.assertThat(Browser.isMicrosoft(), __.truthy());
    });

    QUnit.test('isMicrosoft() should be true for IE Edge', function(assert, Browser, mocks) {
        mocks.Window.navigator.userAgent = 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10136';
        assert.assertThat(Browser.isMicrosoft(), __.truthy());
    });

    QUnit.test('isMicrosoft() should be false for non-IE browsers (Firefox)', function(assert, Browser, mocks) {
        mocks.Window.navigator.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0';
        assert.assertThat(Browser.isMicrosoft(), __.falsy());
    });

});
