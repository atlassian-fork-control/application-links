define([
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/test/mock-callbacks'
], function(
    _,
    QUnit,
    __,
    MockDeclaration,
    MockCallbacks
) {

    QUnit.module('applinks/common/features', {
        require: {
            main: 'applinks/common/features'
        },
        mocks: {
            wrm: QUnit.moduleMock('applinks/lib/wrm', new MockDeclaration(['data.claim'])),
            modules: QUnit.moduleMock('applinks/common/modules', new MockDeclaration(['dataFqn'])),
            context: QUnit.moduleMock('applinks/common/context', new MockDeclaration(['validateCurrentUser'])),
            rest: QUnit.moduleMock('applinks/common/rest', new MockDeclaration(['V3.features'])),
            restRequest: QUnit.mock(new MockDeclaration(['put', 'del'])),
            promise: QUnit.mock(new MockDeclaration(['done']))
        },
        beforeEach: function(assert, ApplinksFeatures, mocks) {
            // set up enabled & discovered features
            mocks.modules.dataFqn.withArgs('applinks-common', 'applinks-features').returns('enabled-features');
            mocks.modules.dataFqn.withArgs('applinks-common', 'applinks-discovered-features').returns('discovered-features');

            mocks.wrm.data.claim.withArgs('enabled-features').returns({
                V3_UI: false,
                V3_UI_OPT_IN: true,
                BITBUCKET_REBRAND: true
            });
            mocks.wrm.data.claim.withArgs('discovered-features').returns(['V3_UI']);

            // REST requests
            mocks.rest.V3.features.returns(mocks.restRequest);
            mocks.restRequest.put.returns(mocks.promise);
            mocks.restRequest.del.returns(mocks.promise);
            mocks.promise.done.returns(mocks.promise);
        }
    });

    QUnit.test('Initial features state', function(assert, ApplinksFeatures, mocks) {

        assert.assertThat(ApplinksFeatures.isEnabled(ApplinksFeatures.V3_UI), __.falsy());
        assert.assertThat(ApplinksFeatures.isEnabled(ApplinksFeatures.V3_UI_OPT_IN), __.truthy());
        assert.assertThat(ApplinksFeatures.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND), __.truthy());
        assert.assertThat(ApplinksFeatures.isEnabled('unknown feature'), __.falsy());
    });

    QUnit.test('Successful enable feature ', function(assert, ApplinksFeatures, mocks) {
        var result = ApplinksFeatures.enable(ApplinksFeatures.V3_UI);
        // run the PUT request success callback
        MockCallbacks.runCallback(assert, mocks.promise.done, [{
            V3_UI: true
        }]);

        assert.assertThat(result, __.is(mocks.promise));
        assert.assertThat(ApplinksFeatures.isEnabled(ApplinksFeatures.V3_UI), __.truthy());
        assert.assertThat(mocks.context.validateCurrentUser, __.calledOnce());
        assert.assertThat(mocks.rest.V3.features, __.calledOnce());
        assert.assertThat(mocks.restRequest.put, __.calledOnce());
    });

    QUnit.test('Failed enable feature ', function(assert, ApplinksFeatures, mocks) {
        var result = ApplinksFeatures.enable(ApplinksFeatures.V3_UI);

        // don't call success callback - feature should remain disabled
        assert.assertThat(result, __.is(mocks.promise));
        assert.assertThat(ApplinksFeatures.isEnabled(ApplinksFeatures.V3_UI), __.falsy());
        assert.assertThat(mocks.context.validateCurrentUser, __.calledOnce());
        assert.assertThat(mocks.rest.V3.features, __.calledOnce());
        assert.assertThat(mocks.restRequest.put, __.calledOnce());
    });

    QUnit.test('Successful disable feature ', function(assert, ApplinksFeatures, mocks) {
        var result = ApplinksFeatures.disable(ApplinksFeatures.V3_UI_OPT_IN);
        // run the DELETE request success callback
        MockCallbacks.runCallback(assert, mocks.promise.done);

        assert.assertThat(result, __.is(mocks.promise));
        assert.assertThat(ApplinksFeatures.isEnabled(ApplinksFeatures.V3_UI_OPT_IN), __.falsy());
        assert.assertThat(mocks.context.validateCurrentUser, __.calledOnce());
        assert.assertThat(mocks.rest.V3.features, __.calledOnceWith(ApplinksFeatures.V3_UI_OPT_IN));
        assert.assertThat(mocks.restRequest.del, __.calledOnce());
    });

    QUnit.test('Failed disable feature ', function(assert, ApplinksFeatures, mocks) {
        var result = ApplinksFeatures.disable(ApplinksFeatures.V3_UI_OPT_IN);

        // don't call success callback - feature should remain enabled
        assert.assertThat(result, __.is(mocks.promise));
        assert.assertThat(ApplinksFeatures.isEnabled(ApplinksFeatures.V3_UI_OPT_IN), __.truthy());
        assert.assertThat(mocks.context.validateCurrentUser, __.calledOnce());
        assert.assertThat(mocks.rest.V3.features, __.calledOnceWith(ApplinksFeatures.V3_UI_OPT_IN));
        assert.assertThat(mocks.restRequest.del, __.calledOnce());
    });
});