define([
    'applinks/test/qunit',
    'applinks/test/hamjest'
], function(
    QUnit,
    __
) {
    QUnit.module('applinks/common/response-status', {
        require: {
            main: 'applinks/common/response-status'
        }
    });

    QUnit.test('Status family INFORMATIONAL', function(assert, ResponseStatus) {
        var mockMatchingReq = {
            status: 100
        };
        var mockMismatchingReq = {
            status: 200
        };

        assert.assertThat('Should match status number',
            ResponseStatus.Family.INFORMATIONAL.matches(100), __.truthy());
        assert.assertThat('Should match status object',
            ResponseStatus.Family.INFORMATIONAL.matches(mockMatchingReq), __.truthy());

        assert.assertThat(ResponseStatus.Family.INFORMATIONAL.matches(200), __.falsy());
        assert.assertThat(ResponseStatus.Family.INFORMATIONAL.matches(300), __.falsy());
        assert.assertThat(ResponseStatus.Family.INFORMATIONAL.matches(400), __.falsy());
        assert.assertThat(ResponseStatus.Family.INFORMATIONAL.matches(500), __.falsy());
        assert.assertThat(ResponseStatus.Family.INFORMATIONAL.matches(mockMismatchingReq), __.falsy());
    });

    QUnit.test('Status family SUCCESSFUL', function(assert, ResponseStatus) {
        var mockMatchingReq = {
            status: 200
        };
        var mockMismatchingReq = {
            status: 400
        };

        assert.assertThat('Should match status number 200',
            ResponseStatus.Family.SUCCESSFUL.matches(200), __.truthy());
        assert.assertThat('Should match status number 201',
            ResponseStatus.Family.SUCCESSFUL.matches(201), __.truthy());
        assert.assertThat('Should match status object',
            ResponseStatus.Family.SUCCESSFUL.matches(mockMatchingReq), __.truthy());

        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(100), __.falsy());
        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(300), __.falsy());
        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(400), __.falsy());
        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(500), __.falsy());
        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(mockMismatchingReq), __.falsy());
    });

    QUnit.test('Status family REDIRECTION', function(assert, ResponseStatus) {
        var mockMatchingReq = {
            status: 300
        };
        var mockMismatchingReq = {
            status: 200
        };

        assert.assertThat('Should match status number',
            ResponseStatus.Family.REDIRECTION.matches(300), __.truthy());
        assert.assertThat('Should match status object',
            ResponseStatus.Family.REDIRECTION.matches(mockMatchingReq), __.truthy());

        assert.assertThat(ResponseStatus.Family.REDIRECTION.matches(100), __.falsy());
        assert.assertThat(ResponseStatus.Family.REDIRECTION.matches(200), __.falsy());
        assert.assertThat(ResponseStatus.Family.REDIRECTION.matches(400), __.falsy());
        assert.assertThat(ResponseStatus.Family.REDIRECTION.matches(500), __.falsy());
        assert.assertThat(ResponseStatus.Family.REDIRECTION.matches(mockMismatchingReq), __.falsy());
    });

    QUnit.test('Status family CLIENT ERROR', function(assert, ResponseStatus) {
        var mockMatchingReq = {
            status: 400
        };
        var mockMismatchingReq = {
            status: 200
        };

        assert.assertThat('Should match status number 400',
            ResponseStatus.Family.CLIENT_ERROR.matches(400), __.truthy());
        assert.assertThat('Should match status number 404',
            ResponseStatus.Family.CLIENT_ERROR.matches(404), __.truthy());
        assert.assertThat('Should match status object',
            ResponseStatus.Family.CLIENT_ERROR.matches(mockMatchingReq), __.truthy());

        assert.assertThat(ResponseStatus.Family.CLIENT_ERROR.matches(100), __.falsy());
        assert.assertThat(ResponseStatus.Family.CLIENT_ERROR.matches(200), __.falsy());
        assert.assertThat(ResponseStatus.Family.CLIENT_ERROR.matches(300), __.falsy());
        assert.assertThat(ResponseStatus.Family.CLIENT_ERROR.matches(500), __.falsy());
        assert.assertThat(ResponseStatus.Family.CLIENT_ERROR.matches(mockMismatchingReq), __.falsy());
    });

    QUnit.test('Status family SERVER ERROR', function(assert, ResponseStatus) {
        var mockMatchingReq = {
            status: 500
        };
        var mockMismatchingReq = {
            status: 200
        };

        assert.assertThat('Should match status number 500',
            ResponseStatus.Family.SERVER_ERROR.matches(500), __.truthy());
        assert.assertThat('Should match status number 503',
            ResponseStatus.Family.SERVER_ERROR.matches(503), __.truthy());
        assert.assertThat('Should match status object',
            ResponseStatus.Family.SERVER_ERROR.matches(mockMatchingReq), __.truthy());

        assert.assertThat(ResponseStatus.Family.SERVER_ERROR.matches(100), __.falsy());
        assert.assertThat(ResponseStatus.Family.SERVER_ERROR.matches(200), __.falsy());
        assert.assertThat(ResponseStatus.Family.SERVER_ERROR.matches(300), __.falsy());
        assert.assertThat(ResponseStatus.Family.SERVER_ERROR.matches(400), __.falsy());
        assert.assertThat(ResponseStatus.Family.SERVER_ERROR.matches(mockMismatchingReq), __.falsy());
    });

    QUnit.test('Status OK', function(assert, ResponseStatus) {
        var mockMatchingReq = {
            status: 200
        };
        var mockMismatchingReq = {
            status: 404
        };

        assert.assertThat('Should match status number 200', ResponseStatus.OK.matches(200), __.truthy());
        assert.assertThat('Should match status object', ResponseStatus.OK.matches(mockMatchingReq), __.truthy());

        assert.assertThat(ResponseStatus.OK.matches(100), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(201), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(204), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(400), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(404), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(500), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(mockMismatchingReq), __.falsy());
    });

    QUnit.test('Status NOT_FOUND', function(assert, ResponseStatus) {
        var mockMatchingReq = {
            status: 404
        };
        var mockMismatchingReq = {
            status: 200
        };

        assert.assertThat('Should match status number 404', ResponseStatus.NOT_FOUND.matches(404), __.truthy());
        assert.assertThat('Should match status object', ResponseStatus.NOT_FOUND.matches(mockMatchingReq), __.truthy());

        assert.assertThat(ResponseStatus.NOT_FOUND.matches(100), __.falsy());
        assert.assertThat(ResponseStatus.NOT_FOUND.matches(201), __.falsy());
        assert.assertThat(ResponseStatus.NOT_FOUND.matches(204), __.falsy());
        assert.assertThat(ResponseStatus.NOT_FOUND.matches(400), __.falsy());
        assert.assertThat(ResponseStatus.NOT_FOUND.matches(401), __.falsy());
        assert.assertThat(ResponseStatus.NOT_FOUND.matches(405), __.falsy());
        assert.assertThat(ResponseStatus.NOT_FOUND.matches(500), __.falsy());
        assert.assertThat(ResponseStatus.NOT_FOUND.matches(mockMismatchingReq), __.falsy());
    });

    QUnit.test('Status Family should handle invalid values', function(assert, ResponseStatus) {
        var mockInvalidReq = {
            status: 'wrong'
        };

        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(undefined), __.falsy());
        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(null), __.falsy());
        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(''), __.falsy());
        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(1456), __.falsy());
        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches('wrong'), __.falsy());
        assert.assertThat(ResponseStatus.Family.SUCCESSFUL.matches(mockInvalidReq), __.falsy());
    });

    QUnit.test('Status should handle invalid values', function(assert, ResponseStatus) {
        var mockInvalidReq = {
            status: 'wrong'
        };

        assert.assertThat(ResponseStatus.OK.matches(undefined), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(null), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(''), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(1456), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches('wrong'), __.falsy());
        assert.assertThat(ResponseStatus.OK.matches(mockInvalidReq), __.falsy());
    });
});