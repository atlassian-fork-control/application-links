define([
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    _,
    QUnit,
    __,
    MockDeclaration
) {
    function applinksModulesConstants() {
        return {
            COMMON_EXPORTED: 'applinks-common-exported'
        };
    }

    QUnit.module('applinks/common/help-paths', {
        require: {
            main: 'applinks/common/help-paths'
        },
        mocks: {
            console: QUnit.moduleMock('applinks/lib/console', new MockDeclaration(['warn'])),
            WRM: QUnit.moduleMock('applinks/lib/wrm', new MockDeclaration(['data.claim'])),
            ApplinksModules: QUnit.moduleMock('applinks/common/modules',
                new MockDeclaration(['dataFqn'], applinksModulesConstants))
        },
        beforeEach: function(assert, Initializer, mocks) {
            // set up enabled & discovered features
            mocks.ApplinksModules.dataFqn.withArgs(mocks.ApplinksModules.COMMON_EXPORTED, 'applinks-help-paths')
                .returns('help-paths');
        }
    });

    QUnit.test('Help paths object has no "entries" field', function(assert, ApplinksHelpPaths, mocks) {
        setUpHelpPaths(mocks, {one: 'one', two: 'two'});

        assert.assertEquals(ApplinksHelpPaths.baseUrl(), 'applinks.docs.root');
        assert.assertEquals(ApplinksHelpPaths.getPath('some.key'), 'some.key');
        assert.assertEquals(ApplinksHelpPaths.getFullPath('some.key'), 'applinks.docs.root/some.key');
        assert.assertThat(mocks.console.warn,
            __.calledOnceWith('Help paths not found, all help links are likely to be broken.'));
    });

    QUnit.test('Valid help paths with trailing slash', function(assert, ApplinksHelpPaths, mocks) {
        setUpHelpPathsEntries(mocks, {one: 'page-one', two: 'page-two'}, 'http://docs.com/');

        assert.assertEquals(ApplinksHelpPaths.baseUrl(), 'http://docs.com/');
        assert.assertEquals(ApplinksHelpPaths.getPath('one'), 'page-one');
        assert.assertEquals(ApplinksHelpPaths.getFullPath('two'), 'http://docs.com/page-two');
        assert.assertThat(mocks.console.warn, __.notCalled());
    });

    QUnit.test('Valid help paths without trailing slash', function(assert, ApplinksHelpPaths, mocks) {
        setUpHelpPathsEntries(mocks, {one: 'page-one', two: 'page-two'}, 'http://docs.com');

        assert.assertEquals(ApplinksHelpPaths.baseUrl(), 'http://docs.com');
        assert.assertEquals(ApplinksHelpPaths.getPath('one'), 'page-one');
        assert.assertEquals(ApplinksHelpPaths.getFullPath('two'), 'http://docs.com/page-two');
        assert.assertThat(mocks.console.warn, __.notCalled());
    });

    QUnit.test('Get path with section key', function(assert, ApplinksHelpPaths, mocks) {
        setUpHelpPathsEntries(mocks, {one: 'Page+With+Anchors', two: 'Page+Two+With+Anchors'}, 'http://docs.com/');

        assert.assertEquals(ApplinksHelpPaths.getPath('one', 'anchorone'),
            'Page+With+Anchors#PageWithAnchors-anchorone');
        assert.assertEquals(ApplinksHelpPaths.getFullPath('two', 'anchortwo'),
            'http://docs.com/Page+Two+With+Anchors#PageTwoWithAnchors-anchortwo');
    });

    QUnit.test('Missing help path key', function(assert, ApplinksHelpPaths, mocks) {
        setUpHelpPathsEntries(mocks, {one: 'page-one', two: 'page-two'}, 'http://docs.com');

        assert.assertEquals(ApplinksHelpPaths.getPath('missing.key'), 'missing.key');
        assert.assertEquals(ApplinksHelpPaths.getFullPath('missing.key'), 'http://docs.com/missing.key');
        assert.assertThat(mocks.console.warn, __.notCalled());
    });

    function setUpHelpPaths(mocks, paths) {
        mocks.WRM.data.claim.withArgs('help-paths').returns(paths);
    }

    function setUpHelpPathsEntries(mocks, entries, baseUrl) {
        if (baseUrl) {
            entries = _.extend(entries, {
                "applinks.docs.root": baseUrl
            })
        }
        setUpHelpPaths(mocks, {entries: entries});
    }
});