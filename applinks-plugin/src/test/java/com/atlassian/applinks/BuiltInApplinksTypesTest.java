package com.atlassian.applinks;

import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.application.HiResIconizedIdentifiableType;
import com.atlassian.applinks.application.IconizedIdentifiableType;
import com.atlassian.applinks.application.bitbucket.BitbucketApplicationTypeImpl;
import com.atlassian.applinks.application.bitbucket.BitbucketProjectEntityTypeImpl;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.spi.application.IconizedType;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.reflections.Reflections;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BuiltInApplinksTypesTest {
    private static final String PLUGIN_KEY = "applinks";

    @Mock
    public AppLinkPluginUtil pluginUtil;

    @Mock
    public WebResourceUrlProvider webResourceUrlProvider;

    @Before
    public void setup() {
        when(pluginUtil.getPluginKey()).thenReturn(PLUGIN_KEY);
        when(webResourceUrlProvider.getStaticPluginResourceUrl(PLUGIN_KEY +
                ":applinks-images", "images", UrlMode.ABSOLUTE)).thenReturn("http://somehost.net/assets");
    }

    @Test
    public void builtInHiResTypesHaveNonNullIconUri() throws Exception {
        Reflections reflections = new Reflections(BuiltinApplinksType.class.getPackage().getName());
        for (Class<? extends BuiltinApplinksType> type : reflections.getSubTypesOf(BuiltinApplinksType.class)) {
            if (!IconizedType.class.isAssignableFrom(type)) {
                // only test for types providing hi-res icons
                continue;
            }
            if (isBitbucket(type)) {
                // we test BB types separately, see BitbucketApplicationAndProjectTypeTest
                continue;
            }
            assertNotNull(((HiResIconizedIdentifiableType) type.getDeclaredConstructor(AppLinkPluginUtil.class, WebResourceUrlProvider.class).
                    newInstance(pluginUtil, webResourceUrlProvider)).getIconUri());
        }
    }

    @Test
    public void builtinTypesHaveNonNullIconUrl() throws Exception {
        Reflections reflections = new Reflections(BuiltinApplinksType.class.getPackage().getName());
        for (Class<? extends BuiltinApplinksType> type : reflections.getSubTypesOf(BuiltinApplinksType.class)) {
            if (isBitbucket(type)) {
                // we test BB types separately, see BitbucketApplicationAndProjectTypeTest
                continue;
            }
            assertNotNull(((IconizedIdentifiableType) type.getDeclaredConstructor(AppLinkPluginUtil.class, WebResourceUrlProvider.class).
                    newInstance(pluginUtil, webResourceUrlProvider)).getIconUrl());
        }
    }

    private boolean isBitbucket(Class<? extends BuiltinApplinksType> type) {
        return type == BitbucketApplicationTypeImpl.class || type == BitbucketProjectEntityTypeImpl.class;
    }
}
