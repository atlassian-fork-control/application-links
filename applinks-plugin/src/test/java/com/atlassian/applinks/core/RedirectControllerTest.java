package com.atlassian.applinks.core;

import com.atlassian.applinks.ui.validators.CallbackParameterValidator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RedirectControllerTest {

    private RedirectController redirectController;

    @Mock
    private WebResourceManager webResourceManager;
    @Mock
    private CallbackParameterValidator callbackParameterValidator;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private HttpServletResponse response;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        redirectController = new RedirectController(callbackParameterValidator, templateRenderer, webResourceManager);
    }

    @Test
    public void shouldRedirectWhenUrlIsValid() throws Exception {
        //given
        final String redirectUrl = "redirectUrl";

        when(callbackParameterValidator.isCallbackUrlValid(redirectUrl))
                .thenReturn(true);

        //when
        redirectController.redirectOrPrintRedirectionWarning(response, redirectUrl);

        //then
        verify(response).sendRedirect(redirectUrl);
    }

    @Test
    public void shouldPrintWarningMessageWhenUrlIsNotValid() throws Exception {
        //given
        final String redirectUrl = "redirectUrl";

        when(callbackParameterValidator.isCallbackUrlValid(redirectUrl))
                .thenReturn(false);

        final PrintWriter mockResponseWriter = mock(PrintWriter.class);
        when(response.getWriter())
                .thenReturn(mockResponseWriter);

        //when
        redirectController.redirectOrPrintRedirectionWarning(response, redirectUrl);

        //then
        verify(response, times(0)).sendRedirect(redirectUrl);
        verify(templateRenderer).render(eq(RedirectController.REDIRECT_WARNING_TEMPLATE),
                argThat(map -> !map.containsValue(redirectUrl)), eq(mockResponseWriter));
    }


}
