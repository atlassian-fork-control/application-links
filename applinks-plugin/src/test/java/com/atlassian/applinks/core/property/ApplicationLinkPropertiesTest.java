package com.atlassian.applinks.core.property;

import com.atlassian.applinks.api.PropertySet;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ApplicationLinkPropertiesTest {
    private static final URI CONF_URL;
    private static final URI LOCAL_URL;
    private static final TypeId CONF_TYPE_ID = new TypeId("confluence");
    private PluginSettings pluginSettings;
    private PropertySet adminApplinksPropertySet;
    private ApplicationLinkProperties applicationLinkProperties;
    private PropertySet customApplinksProperySet;

    static {
        try {
            CONF_URL = new URI("http://confluence");
            LOCAL_URL = new URI("http://192.168.10.1");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        pluginSettings = new MockPluginSettingsPropertySet();
        adminApplinksPropertySet = new SalPropertySet(pluginSettings, ".general");
        customApplinksProperySet = new SalPropertySet(pluginSettings, ".custom");
        applicationLinkProperties = new ApplicationLinkProperties(adminApplinksPropertySet, customApplinksProperySet);
    }

    @Test
    public void testSetProperties() throws Exception {
        applicationLinkProperties.setIsPrimary(true);
        applicationLinkProperties.setName("bob");
        applicationLinkProperties.setDisplayUrl(CONF_URL);
        applicationLinkProperties.setRpcUrl(LOCAL_URL);
        applicationLinkProperties.setType(CONF_TYPE_ID);

        assertEquals("bob", applicationLinkProperties.getName());
        assertEquals(CONF_URL, applicationLinkProperties.getDisplayUrl());
        assertEquals(LOCAL_URL, applicationLinkProperties.getRpcUrl());
        assertEquals(CONF_TYPE_ID, applicationLinkProperties.getType());
    }

    @Test
    public void testDeleteProperties() throws Exception {
        applicationLinkProperties.setIsPrimary(true);
        applicationLinkProperties.setName("bob");
        applicationLinkProperties.setDisplayUrl(CONF_URL);
        applicationLinkProperties.setRpcUrl(LOCAL_URL);
        applicationLinkProperties.setType(CONF_TYPE_ID);

        assertEquals("bob", applicationLinkProperties.getName());
        assertEquals(CONF_URL, applicationLinkProperties.getDisplayUrl());
        assertEquals(LOCAL_URL, applicationLinkProperties.getRpcUrl());
        assertEquals(CONF_TYPE_ID, applicationLinkProperties.getType());
        assertEquals(true, applicationLinkProperties.isPrimary());
        assertEquals("true", adminApplinksPropertySet.getProperty(ApplicationLinkProperties.Property.PRIMARY.key()));

        applicationLinkProperties.remove();

        assertEquals(null, applicationLinkProperties.getName());
        assertEquals(null, applicationLinkProperties.getDisplayUrl());
        assertEquals(null, applicationLinkProperties.getRpcUrl());
        assertEquals(null, applicationLinkProperties.getType());
        assertEquals(null, adminApplinksPropertySet.getProperty(ApplicationLinkProperties.Property.PRIMARY.key()));
        assertEquals(false, applicationLinkProperties.isPrimary());
    }

    @Test
    public void testSetAuthProviderConfig() throws Exception {
        applicationLinkProperties.setIsPrimary(true);
        applicationLinkProperties.setName("bob");
        applicationLinkProperties.setDisplayUrl(CONF_URL);
        applicationLinkProperties.setRpcUrl(LOCAL_URL);
        applicationLinkProperties.setType(CONF_TYPE_ID);

        final HashMap<String, String> config = new HashMap<String, String>();
        config.put("name", "myName");
        applicationLinkProperties.setProviderConfig("seraph", config);

        final Map<String, String> readConfig = applicationLinkProperties.getProviderConfig("seraph");
        assertEquals("myName", readConfig.get("name"));
        assertEquals("bob", applicationLinkProperties.getName());
    }

    @Test
    public void testRemoveAuthProviderConfig() throws Exception {
        applicationLinkProperties.setIsPrimary(true);
        applicationLinkProperties.setName("bob");
        applicationLinkProperties.setDisplayUrl(CONF_URL);
        applicationLinkProperties.setRpcUrl(LOCAL_URL);
        applicationLinkProperties.setType(CONF_TYPE_ID);

        final HashMap<String, String> config = new HashMap<String, String>();
        config.put("name", "myName");
        applicationLinkProperties.setProviderConfig("seraph", config);

        final Map<String, String> readConfig = applicationLinkProperties.getProviderConfig("seraph");
        assertEquals("myName", readConfig.get("name"));
        assertEquals("bob", applicationLinkProperties.getName());

        applicationLinkProperties.removeProviderConfig("seraph");
        assertEquals(null, applicationLinkProperties.getProviderConfig("seraph"));
    }

    @Test
    public void testRemoveApplicationLinkConfiguration() throws Exception {
        applicationLinkProperties.setIsPrimary(true);
        applicationLinkProperties.setName("bob");
        applicationLinkProperties.setDisplayUrl(CONF_URL);
        applicationLinkProperties.setRpcUrl(LOCAL_URL);
        applicationLinkProperties.setType(CONF_TYPE_ID);

        final HashMap<String, String> config = new HashMap<String, String>();
        config.put("name", "myName");
        applicationLinkProperties.setProviderConfig("seraph", config);

        final Map<String, String> readConfig = applicationLinkProperties.getProviderConfig("seraph");
        assertEquals("myName", readConfig.get("name"));
        assertEquals("bob", applicationLinkProperties.getName());

        applicationLinkProperties.remove();
        assertEquals(null, applicationLinkProperties.getProviderConfig("seraph"));
    }

    @Test
    public void testSetCustomProperty() throws Exception {
        applicationLinkProperties.putProperty("name", "bob");
        assertEquals("bob", applicationLinkProperties.getProperty("name"));
    }

    @Test
    public void testRemoveCustomProperty() throws Exception {
        applicationLinkProperties.putProperty("name", "bob");
        assertEquals("bob", applicationLinkProperties.getProperty("name"));
        final List<String> properties = (List<String>) adminApplinksPropertySet.getProperty("propertyKeys");
        assertTrue(properties.contains("name"));
        applicationLinkProperties.removeProperty("name");
        assertEquals(null, applicationLinkProperties.getProperty("name"));
        final List<String> remainingProperties = (List<String>) adminApplinksPropertySet.getProperty("propertyKeys");
        assertFalse(remainingProperties.contains("name"));
    }

    @Test
    public void testSystemKeyInPropertiesCanControlIsSystem() {
        assertFalse(applicationLinkProperties.isSystem());
        applicationLinkProperties.putProperty("system", "true");
        assertTrue(applicationLinkProperties.isSystem());
    }

    @Test
    public void testSetSystemWorks() {
        applicationLinkProperties.setSystem(true);
        assertTrue(applicationLinkProperties.isSystem());

        applicationLinkProperties.setSystem(false);
        assertFalse(applicationLinkProperties.isSystem());
    }

    @Test
    public void testSetSystemCanOverrideDirectSystemKeyInProperties() {
        applicationLinkProperties.putProperty("system", "true");
        assertTrue(applicationLinkProperties.isSystem());

        applicationLinkProperties.setSystem(false);
        assertFalse(applicationLinkProperties.isSystem());
    }

    @Test
    public void testSystemFlagRemovedOnePropertiesRemoved() {
        applicationLinkProperties.setSystem(true);
        assertTrue(applicationLinkProperties.isSystem());

        applicationLinkProperties.remove();
        assertFalse(applicationLinkProperties.isSystem());
    }

    @Test
    public void testSettingDisplayURLWithInvalidProtocolFail() {
        expectedException.expect(IllegalArgumentException.class);
        final URI displayUri = URI.create("htt://test.com");
        applicationLinkProperties.setDisplayUrl(displayUri);
    }

    @Test
    public void testSettingRpcURLWithInvalidProtocolFail() {
        expectedException.expect(IllegalArgumentException.class);
        final URI rpcUri = URI.create("abc://test.com");
        applicationLinkProperties.setRpcUrl(rpcUri);
    }

    @Test
    public void testSettingDisplayURLWithJSProtocolFail() {
        expectedException.expect(IllegalArgumentException.class);
        final URI displayUri = URI.create("javascript:someFunction()");
        applicationLinkProperties.setDisplayUrl(displayUri);
    }

    @Test
    public void testSettingRpcURLWithJSProtocolFail() {
        expectedException.expect(IllegalArgumentException.class);
        final URI rpcUri = URI.create("javascript:alert(window.location)");
        applicationLinkProperties.setRpcUrl(rpcUri);
    }

    @Test
    public void testSettingDisplayURLWithMissingProtocolFail() {
        expectedException.expect(IllegalArgumentException.class);
        final URI displayUri = URI.create("test.com");
        applicationLinkProperties.setDisplayUrl(displayUri);
    }

    @Test
    public void testSettingRpcURLWithMissingProtocolFail() {
        expectedException.expect(IllegalArgumentException.class);
        final URI rpcUri = URI.create("://test.com");
        applicationLinkProperties.setRpcUrl(rpcUri);
    }

    @Test
    public void testSettingValidDisplayURLPass() {
        final URI displayUri = URI.create("http://test.com");
        applicationLinkProperties.setDisplayUrl(displayUri);
    }

    @Test
    public void testSettingValidRpcURLPass() {
        final URI rpcUri = URI.create("https://test.com");
        applicationLinkProperties.setRpcUrl(rpcUri);
    }

}
