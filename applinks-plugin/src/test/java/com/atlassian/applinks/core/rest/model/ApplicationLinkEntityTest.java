package com.atlassian.applinks.core.rest.model;

import java.net.URI;
import java.util.UUID;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.application.generic.GenericApplicationTypeImpl;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugins.rest.common.Link;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class ApplicationLinkEntityTest {
    private static final URI URI1 = URI.create("http://localhost");

    @Test
    public void testSystemLinkEntityWillNotExposeRpcUrl() throws Exception {
        ApplicationLinkEntity entity = new ApplicationLinkEntity(mock(ApplicationId.class), mock(TypeId.class),
                "name", URI1, URI1, URI1, URI1, false, true, mock(Link.class));
        assertNull(entity.getRpcUrl());
    }

    @Test
    public void testNonSystemLinkEntityCanExposeRpcUrl() throws Exception {
        ApplicationLinkEntity entity = new ApplicationLinkEntity(mock(ApplicationId.class), mock(TypeId.class),
                "name", URI1, URI1, URI1, URI1, false, false, mock(Link.class));
        assertEquals(URI1, entity.getRpcUrl());
    }

    @Test
    public void serializationRegression() throws Exception {
        UUID id = UUID.randomUUID();
        ApplicationId applicationId = new ApplicationId(id.toString());
        TypeId typeId = TypeId.getTypeId(new GenericApplicationTypeImpl(null, null));
        String entityName = "entityName";
        boolean isSystem = false;
        boolean isPrimary = false;
        URI displayUrl = URI.create("http://localhost/A");
        URI rpcUrl = URI.create("http://localhost/B");
        URI iconUrl = URI.create("http://localhost/C");
        URI iconUri = URI.create("http://localhost/D");

        ApplicationLinkEntity entity = new ApplicationLinkEntity(
                applicationId,
                typeId,
                entityName,
                displayUrl,
                iconUrl,
                iconUri,
                rpcUrl,
                isSystem,
                isPrimary,
                null);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setAnnotationIntrospector(new JaxbAnnotationIntrospector(TypeFactory.defaultInstance()));

        String result = mapper.writeValueAsString(entity);

        assertNotNull(result);

        assertThat(result, containsString("\"id\":" + "\"" + id.toString() + "\""));
        assertThat(result, containsString("\"typeId\":" + "\"" + typeId.toString() + "\""));
        assertThat(result, containsString("\"name\":" + "\"" + entityName + "\""));
        assertThat(result, containsString("\"displayUrl\":" + "\"" + displayUrl.toASCIIString() + "\""));
        assertThat(result, containsString("\"rpcUrl\":" + "\"" + rpcUrl.toASCIIString() + "\""));
        assertThat(result, containsString("\"iconUrl\":" + "\"" + iconUrl.toASCIIString() + "\""));
        assertThat(result, containsString("\"isSystem\":" + isSystem));
        assertThat(result, containsString("\"isPrimary\":" + isPrimary));
    }

    @Test
    public void deserializationRegression() throws Exception {
        UUID id = UUID.randomUUID();
        ApplicationId applicationId = new ApplicationId(id.toString());
        TypeId typeId = TypeId.getTypeId(new GenericApplicationTypeImpl(null, null));
        String entityName = "entityName";
        boolean isSystem = false;
        boolean isPrimary = false;
        URI displayUrl = URI.create("http://localhost/A");
        URI rpcUrl = URI.create("http://localhost/B");
        URI iconUrl = URI.create("http://localhost/C");

        final StringBuffer jsonBuffer = new StringBuffer();
        jsonBuffer.append("{");
        jsonBuffer.append("\"id\":\"" + applicationId + "\"");
        jsonBuffer.append(",");
        jsonBuffer.append("\"typeId\":\"" + typeId + "\"");
        jsonBuffer.append(",");
        jsonBuffer.append("\"name\":\"" + entityName + "\"");
        jsonBuffer.append(",");
        jsonBuffer.append("\"displayUrl\":\"" + displayUrl + "\"");
        jsonBuffer.append(",");
        jsonBuffer.append("\"rpcUrl\":\"" + rpcUrl + "\"");
        jsonBuffer.append(",");
        jsonBuffer.append("\"iconUrl\":\"" + iconUrl + "\"");
        jsonBuffer.append(",");
        jsonBuffer.append("\"isSystem\":\"" + isSystem + "\"");
        jsonBuffer.append(",");
        jsonBuffer.append("\"isPrimary\":\"" + isPrimary + "\"");
        jsonBuffer.append("}");

        ObjectMapper mapper = new ObjectMapper();
        mapper.setAnnotationIntrospector(new JaxbAnnotationIntrospector(TypeFactory.defaultInstance()));

        ApplicationLinkEntity result = mapper.readValue(jsonBuffer.toString(), ApplicationLinkEntity.class);

        assertNotNull(result);

        assertThat(result.getId(), is(applicationId));
        assertThat(result.getTypeId(), is(typeId));
        assertThat(result.getName(), is(entityName));
        assertThat(result.getDisplayUrl(), is(displayUrl));
        assertThat(result.getRpcUrl(), is(rpcUrl));
        assertThat(result.getIconUrl(), is(iconUrl));
        assertThat(result.isSystem(), is(isSystem));
        assertThat(result.isPrimary(), is(isPrimary));

    }
}
