package com.atlassian.applinks.core.auth;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for the simple string response handler.
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class ApplicationLinksStringReturningResponseHandlerTest {
    @Mock
    private Response response;

    @Test
    public void willHandleSuccessfully() throws ResponseException {
        when(response.isSuccessful()).thenReturn(true);
        when(response.getResponseBodyAsString()).thenReturn("body");

        ApplicationLinksStringReturningResponseHandler handler = new ApplicationLinksStringReturningResponseHandler();
        String result = handler.handle(response);

        assertEquals("body", result);
        verify(response).isSuccessful();
        verify(response).getResponseBodyAsString();
    }

    @Test
    public void willNotHandleDueToResponseException() {
        when(response.isSuccessful()).thenReturn(false);
        ApplicationLinksStringReturningResponseHandler handler = new ApplicationLinksStringReturningResponseHandler();
        try {
            handler.handle(response);
            fail("Expected response exception.");
        } catch (ResponseException e) {
            assertTrue(true);
        }

        verify(response).isSuccessful();
    }
}
