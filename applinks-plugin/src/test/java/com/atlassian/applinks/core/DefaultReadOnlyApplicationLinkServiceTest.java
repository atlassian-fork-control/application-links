
package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.api.application.bamboo.BambooApplicationType;
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.api.event.ApplicationLinkAddedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkDeletedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkDetailsChangedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkMadePrimaryEvent;
import com.atlassian.applinks.api.event.ApplicationLinksIDChangedEvent;
import com.atlassian.applinks.core.auth.ApplicationLinkRequestFactoryFactory;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.LinkedList;
import java.util.UUID;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultReadOnlyApplicationLinkServiceTest {
    private static final ApplicationId EXISTING_APPLICATION_ID = new ApplicationId(UUID.randomUUID().toString());
    private static final ApplicationId ANOTHER_APPLICATION_ID = new ApplicationId(UUID.randomUUID().toString());
    private static final ApplicationId NOT_EXISTING_APPLICATION_ID = new ApplicationId(UUID.randomUUID().toString());
    private static final String APPLICATION_LINK_NAME = "Application Link Name";
    private static final ApplicationType JIRA_APPLICATION_TYPE = mock(JiraApplicationType.class);
    private static final ApplicationType CONFLUENCE_APPLICATION_TYPE = mock(ConfluenceApplicationType.class);
    private static final URI APPLICATION_LINK_DISPLAY_URL = URI.create("http://display.url");
    private static final URI APPLICATION_LINK_RPC_URL = URI.create("http://rpc.url");
    private static final boolean IS_PRIMARY = true;
    private static final boolean NOT_PRIMARY = false;

    @Mock
    private ApplicationLinkService applicationLinkService;
    @Mock
    private ApplicationLinkRequestFactoryFactory requestFactoryFactory;
    @Mock
    private TransactionTemplate transactionTemplate;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private ApplicationLinkAddedEvent applicationLinkAddedEvent;
    @Mock
    private ApplicationLinkDetailsChangedEvent applicationLinkDetailsChangedEvent;
    @Mock
    private ApplicationLinkDeletedEvent applicationLinkDeletedEvent;
    @Mock
    private ApplicationLinkMadePrimaryEvent applicationLinkMadePrimary;
    @Mock
    private ApplicationLinksIDChangedEvent applicationLinkIdChangedEvent;


    private DefaultReadOnlyApplicationLinkService service;

    @Before
    public void setUp() throws Exception {
        when(transactionTemplate.execute(any(TransactionCallback.class))).thenAnswer(new Answer<Object>() {
            @Override
            @SuppressWarnings("unchecked")
            public Object answer(InvocationOnMock invocation) throws Throwable {
                TransactionCallback<Iterable<ReadOnlyApplicationLink>> callback = (TransactionCallback<Iterable<ReadOnlyApplicationLink>>) invocation.getArguments()[0];
                return callback.doInTransaction();
            }
        });
        givenApplicationLinks(createApplicationLink());
        service = createService();
    }

    @Test
    public void getApplicationLinks() {
        final Iterable<ReadOnlyApplicationLink> applicationLinks = service.getApplicationLinks();
        assertThat(applicationLinks, Matchers.<ReadOnlyApplicationLink>hasItem(withApplicationId(EXISTING_APPLICATION_ID)));
    }

    @Test
    public void getExistingApplicationLink() {
        final ReadOnlyApplicationLink applicationLink = service.getApplicationLink(EXISTING_APPLICATION_ID);
        assertNotNull(applicationLink);
        assertThat(applicationLink.getId(), is(EXISTING_APPLICATION_ID));
        assertThat(applicationLink.getType(), is(JIRA_APPLICATION_TYPE));
        assertThat(applicationLink.getName(), is(APPLICATION_LINK_NAME));
        assertThat(applicationLink.getDisplayUrl(), is(APPLICATION_LINK_DISPLAY_URL));
        assertThat(applicationLink.getRpcUrl(), is(APPLICATION_LINK_RPC_URL));
        assertTrue(applicationLink.isPrimary());
        assertTrue(applicationLink.isSystem());
    }

    @Test
    public void getNotExistingApplicationLink() {
        assertNull(service.getApplicationLink(NOT_EXISTING_APPLICATION_ID));
    }

    @Test
    public void getExistingApplicationLinksByType() {
        final Iterable<ReadOnlyApplicationLink> applicationLinks = service.getApplicationLinks(ApplicationType.class);
        assertThat(applicationLinks, Matchers.<ReadOnlyApplicationLink>contains(withApplicationId(EXISTING_APPLICATION_ID)));
    }

    @Test
    public void getExistingApplicationLinksByTypePrimaryAlwaysFirst() {
        givenApplicationLinks(
                createApplicationLink(EXISTING_APPLICATION_ID, NOT_PRIMARY),
                createApplicationLink(ANOTHER_APPLICATION_ID, IS_PRIMARY)
        );
        final LinkedList<ReadOnlyApplicationLink> orderedApplicationLinks = Lists.newLinkedList(service.getApplicationLinks(ApplicationType.class));
        assertThat(orderedApplicationLinks.getFirst(), is(withApplicationId(ANOTHER_APPLICATION_ID)));
    }

    @Test
    public void getApplicationsNonPrimaryAfterPrimary() {

        givenApplicationLinks(
                createApplicationLink(ANOTHER_APPLICATION_ID, IS_PRIMARY),
                createApplicationLink(EXISTING_APPLICATION_ID, NOT_PRIMARY)
        );
        final LinkedList<ReadOnlyApplicationLink> orderedApplicationLinks = Lists.newLinkedList(service.getApplicationLinks(ApplicationType.class));
        assertThat(orderedApplicationLinks.getFirst(), is(withApplicationId(ANOTHER_APPLICATION_ID)));
    }

    @Test
    public void getApplicationsByType() {
        givenApplicationLinks(
                createApplicationLink(ANOTHER_APPLICATION_ID, JIRA_APPLICATION_TYPE),
                createApplicationLink(EXISTING_APPLICATION_ID, CONFLUENCE_APPLICATION_TYPE)
        );
        assertThat(service.getApplicationLinks(JiraApplicationType.class), contains(withApplicationId(ANOTHER_APPLICATION_ID)));
        assertThat(service.getApplicationLinks(ConfluenceApplicationType.class), contains(withApplicationId(EXISTING_APPLICATION_ID)));
    }

    @Test
    public void getNotExistingApplicationLinksByType() {
        final Iterable<ReadOnlyApplicationLink> applicationLinks = service.getApplicationLinks(BambooApplicationType.class);
        assertThat(applicationLinks, Matchers.emptyIterable());
    }

    @Test(expected = IllegalStateException.class)
    public void getPrimaryApplicationLinkThrowsExceptionWhenPrimaryIsMissing() {
        givenApplicationLinks(createApplicationLink(NOT_PRIMARY));
        service.getPrimaryApplicationLink(ApplicationType.class);
    }

    @Test
    public void getPrimaryApplicationLinkReturnsNullWhenThereAreNoApplicationLinksForType() {
        assertNull(service.getPrimaryApplicationLink(BambooApplicationType.class));
    }

    @Test
    public void getPrimaryApplicationLink() {
        assertThat(service.getPrimaryApplicationLink(ApplicationType.class), is(withApplicationId(EXISTING_APPLICATION_ID)));
    }

    @Test
    public void cacheInvalidatedOnApplicationLinkAddedEvent() {
        service.getApplicationLinks();
        service.onApplicationLinkAddedEvent(applicationLinkAddedEvent);
        service.getApplicationLinks();
        verify(applicationLinkService, times(2)).getApplicationLinks();
    }

    @Test
    public void cacheInvalidatedOnApplicationLinkDetailsChangedEvent() {
        service.getApplicationLinks();
        service.onApplicationLinkDetailsChangedEvent(applicationLinkDetailsChangedEvent);
        service.getApplicationLinks();
        verify(applicationLinkService, times(2)).getApplicationLinks();
    }

    @Test
    public void cacheInvalidatedOnApplicationLinkIdChangedEvent() {
        service.getApplicationLinks();
        service.onApplicationLinkIdChangedEvent(applicationLinkIdChangedEvent);
        service.getApplicationLinks();
        verify(applicationLinkService, times(2)).getApplicationLinks();
    }

    @Test
    public void cacheInvalidatedOnApplicationLinkDeletedEvent() {
        service.getApplicationLinks();
        service.onApplicationLinkDeletedEvent(applicationLinkDeletedEvent);
        service.getApplicationLinks();
        verify(applicationLinkService, times(2)).getApplicationLinks();
    }

    @Test
    public void cacheInvalidatedOnApplicationLinkMadePrimary() {
        service.getApplicationLinks();
        service.onApplicationLinkMadePrimaryEvent(applicationLinkMadePrimary);
        service.getApplicationLinks();
        verify(applicationLinkService, times(2)).getApplicationLinks();
    }

    private Matcher<ReadOnlyApplicationLink> withApplicationId(final ApplicationId existingApplicationId) {
        return new FeatureMatcher<ReadOnlyApplicationLink, ApplicationId>(equalTo(existingApplicationId), "an immutable application link with application id", "applicationId") {

            @Override
            protected ApplicationId featureValueOf(final ReadOnlyApplicationLink actual) {
                return actual.getId();
            }
        };
    }

    private void givenApplicationLinks(final ApplicationLink... applicationLinks) {
        doReturn(ImmutableList.copyOf(applicationLinks)).when(applicationLinkService).getApplicationLinks();
    }

    private static ApplicationLink createApplicationLink() {
        return createApplicationLink(IS_PRIMARY);
    }

    private static ApplicationLink createApplicationLink(final boolean isPrimary) {
        return createApplicationLink(EXISTING_APPLICATION_ID, isPrimary);
    }

    private static ApplicationLink createApplicationLink(final ApplicationId applicationId, final boolean isPrimary) {
        return createApplicationLink(applicationId, isPrimary, JIRA_APPLICATION_TYPE);
    }

    private static ApplicationLink createApplicationLink(final ApplicationId applicationId, final ApplicationType applicationType) {
        return createApplicationLink(applicationId, false, applicationType);
    }

    private static ApplicationLink createApplicationLink(final ApplicationId applicationId, final boolean isPrimary, final ApplicationType applicationType) {
        final ApplicationLink applicationLink = mock(ApplicationLink.class);
        when(applicationLink.getId()).thenReturn(applicationId);
        when(applicationLink.getType()).thenReturn(applicationType);
        when(applicationLink.getName()).thenReturn(APPLICATION_LINK_NAME);
        when(applicationLink.getDisplayUrl()).thenReturn(APPLICATION_LINK_DISPLAY_URL);
        when(applicationLink.getRpcUrl()).thenReturn(APPLICATION_LINK_RPC_URL);
        when(applicationLink.isPrimary()).thenReturn(isPrimary);
        when(applicationLink.isSystem()).thenReturn(true);
        return applicationLink;
    }

    private DefaultReadOnlyApplicationLinkService createService() throws Exception {
        return new DefaultReadOnlyApplicationLinkService(applicationLinkService, requestFactoryFactory, transactionTemplate, eventPublisher, new MemoryCacheManager());
    }
}
