package com.atlassian.applinks.application.bitbucket;

import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@SuppressWarnings("ConstantConditions")
public class BitbucketApplicationAndProjectTypeTest {
    @Mock
    private AppLinkPluginUtil appLinkPluginUtil;
    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;
    @Mock
    private ApplinksFeatureService applinksFeatureService;

    @InjectMocks
    private BitbucketApplicationTypeImpl bitbucketApplicationType;
    @InjectMocks
    private BitbucketProjectEntityTypeImpl bitbucketProjectEntityType;

    @Test
    public void bitbucketApplicationI18nGivenBitbucketRebrandOn() {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND)).thenReturn(true);

        assertEquals("applinks.bitbucket", bitbucketApplicationType.getI18nKey());
    }

    @Test
    public void bitbucketApplicationIconsGivenBitbucketRebrandOn() {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND)).thenReturn(true);

        assertThat(bitbucketApplicationType.getIconUrl().toString(), containsString("bitbucket"));
        assertThat(bitbucketApplicationType.getIconUri().toString(), containsString("bitbucket"));
    }

    @Test
    public void bitbucketEntityI18nGivenBitbucketRebrandOn() {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND)).thenReturn(true);

        assertEquals("applinks.bitbucket.project", bitbucketProjectEntityType.getI18nKey());
        assertEquals("applinks.bitbucket.project.plural", bitbucketProjectEntityType.getPluralizedI18nKey());
        assertEquals("applinks.bitbucket.project.short", bitbucketProjectEntityType.getShortenedI18nKey());
    }

    @Test
    public void bitbucketEntityIconsGivenBitbucketRebrandOn() {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND)).thenReturn(true);

        assertThat(bitbucketProjectEntityType.getIconUrl().toString(), containsString("bitbucket"));
        assertThat(bitbucketProjectEntityType.getIconUri().toString(), containsString("bitbucket"));
    }

    @Test
    public void stashApplicationI18nGivenBitbucketRebrandOff() {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND)).thenReturn(false);

        assertEquals("applinks.stash", bitbucketApplicationType.getI18nKey());
    }

    @Test
    public void stashApplicationIconsGivenBitbucketRebrandOff() {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND)).thenReturn(false);

        assertThat(bitbucketApplicationType.getIconUrl().toString(), containsString("stash"));
        assertThat(bitbucketApplicationType.getIconUri().toString(), containsString("stash"));
    }

    @Test
    public void stashEntityI18nGivenBitbucketRebrandOff() {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND)).thenReturn(false);

        assertEquals("applinks.stash.project", bitbucketProjectEntityType.getI18nKey());
        assertEquals("applinks.stash.project.plural", bitbucketProjectEntityType.getPluralizedI18nKey());
        assertEquals("applinks.stash.project.short", bitbucketProjectEntityType.getShortenedI18nKey());
    }

    @Test
    public void stashEntityIconsGivenBitbucketRebrandOn() {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND)).thenReturn(false);

        assertThat(bitbucketProjectEntityType.getIconUrl().toString(), containsString("stash"));
        assertThat(bitbucketProjectEntityType.getIconUri().toString(), containsString("stash"));
    }
}
