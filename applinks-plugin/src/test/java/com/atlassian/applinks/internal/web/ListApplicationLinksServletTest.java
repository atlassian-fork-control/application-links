package com.atlassian.applinks.internal.web;

import com.atlassian.applinks.analytics.ApplinksAdminViewEvent;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.applinks.ui.velocity.ListApplicationLinksContext;
import com.atlassian.applinks.ui.velocity.VelocityContextFactory;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServlet;
import java.io.Writer;
import java.util.Map;

import static com.atlassian.applinks.internal.common.web.ServletAssertions.assertClickjackingPrevention;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the Application Link admin screen
 *
 * <p>
 * Note: permission enforcement is covered by {@code AdminOnlyServletsTest}
 *
 * @since 4.3
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class ListApplicationLinksServletTest {
    protected static final String HTTP_GET = "GET";

    @Mock
    protected AdminUIAuthenticator adminUiAuthenticator;
    @Mock
    protected ApplinksFeatureService applinksFeatureService;
    @Mock
    protected EventPublisher eventPublisher;
    @Mock
    protected TemplateRenderer templateRenderer;
    @Mock
    protected VelocityContextFactory velocityContextFactory;
    @Mock
    protected WebResourceManager webResourceManager;
    @Mock
    protected WebSudoManager webSudoManager;
    @Mock
    protected ListApplicationLinksContext listApplicationLinksContext;

    @InjectMocks
    private ListApplicationLinksServlet servlet;

    @Captor
    protected ArgumentCaptor<Object> eventCaptor;

    protected final MockHttpServletRequest request = new MockHttpServletRequest();
    protected final MockHttpServletResponse response = new MockHttpServletResponse();

    @Test
    @SuppressWarnings("unchecked")
    public void shouldRenderV3TemplateWhenV3Enabled() throws Exception {
        stubGetRequest();
        stubAdminUser();
        stubV3Ui(true);
        stubRender();

        request.setContextPath("/test");
        doService();
        verify(templateRenderer).render(eq(ListApplicationLinksServlet.V3_TEMPLATE_PATH), any(Map.class), any(Writer.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldRenderV2TemplateWhenV3NotEnabled() throws Exception {
        stubGetRequest();
        stubAdminUser();
        stubV3Ui(false);
        stubRender();

        request.setContextPath("/test");
        doService();
        verify(templateRenderer).render(eq(ListApplicationLinksServlet.V2_TEMPLATE_PATH), anyMap(), any(Writer.class));
    }

    @Test
    public void testPublishesAdminViewEvent() throws Exception {
        stubDefaultRequestAndRender();

        doService();

        captureAllPublishedEvents();
        assertThat(eventCaptor.getAllValues(), hasItem(instanceOf(ApplinksAdminViewEvent.class)));
    }

    protected HttpServlet getServlet() {
        return servlet;
    }

    protected final void doService() throws Exception {
        getServlet().service(request, response);
        assertClickjackingPrevention(response);
    }

    protected final void captureAllPublishedEvents() {
        verify(eventPublisher, atLeastOnce()).publish(eventCaptor.capture());
    }

    protected final void stubAdminUser() {
        when(adminUiAuthenticator.checkAdminUIAccessBySessionOrCurrentUser(request)).thenReturn(true);
    }

    protected final void stubDefaultRequestAndRender() {
        stubGetRequest();
        stubAdminUser();
        stubRender();
    }

    protected final void stubGetRequest() {
        request.setMethod(HTTP_GET);
    }

    protected final void stubRender() {
        when(velocityContextFactory.buildListApplicationLinksContext(request)).thenReturn(listApplicationLinksContext);
    }

    protected final void stubV3Ui(boolean enabled) {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.V3_UI)).thenReturn(enabled);
    }
}
