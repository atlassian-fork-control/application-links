package com.atlassian.applinks.internal.status;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.AuthorisationURIGenerator;
import com.atlassian.applinks.core.ApplinkStatus;
import com.atlassian.applinks.core.DefaultApplinkStatusService;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.capabilities.DefaultRemoteCapabilities;
import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.SimpleApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.OAuthStatusService;
import com.atlassian.applinks.internal.status.oauth.remote.OAuthConnectionVerifier;
import com.atlassian.applinks.internal.status.oauth.remote.RemoteOAuthStatusService;
import com.atlassian.applinks.internal.status.remote.ApplinkStatusAccessException;
import com.atlassian.applinks.internal.status.remote.NoOutgoingAuthenticationException;
import com.atlassian.applinks.internal.status.remote.NoRemoteApplinkException;
import com.atlassian.applinks.internal.status.remote.RemoteNetworkException;
import com.atlassian.applinks.internal.status.remote.RemoteOAuthException;
import com.atlassian.applinks.internal.status.remote.RemoteStatusUnknownException;
import com.atlassian.applinks.internal.status.remote.RemoteVersionIncompatibleException;
import com.atlassian.applinks.internal.status.support.ApplinkStatusValidationService;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.mock.PermissionValidationMocks;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseTransportException;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.net.ConnectException;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.defaultOAuthConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusDefault;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusOff;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusWith;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthWithImpersonationConfig;
import static com.atlassian.applinks.test.mock.TestApplinkIds.DEFAULT_ID;
import static com.atlassian.applinks.test.mock.TestApplinkIds.ID2;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@SuppressWarnings("unchecked")
public class DefaultApplinkStatusServiceTest {
    private static final ApplicationId TEST_ID = DEFAULT_ID.applicationId();

    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);

    @Mock
    private ApplinkHelper applinkHelper;
    @Mock
    private OAuthConnectionVerifier oAuthConnectionVerifier;
    @Mock
    private OAuthStatusService localOAuthStatusService;
    @Mock
    private RemoteOAuthStatusService remoteOAuthStatusService;
    @Mock
    private RemoteCapabilitiesService remoteCapabilitiesService;
    @Mock
    private PermissionValidationService permissionValidationService;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private ApplinkStatusValidationService validationService;

    @MockApplink
    private ApplicationLink applicationLink;

    @InjectMocks
    private PermissionValidationMocks permissionValidationMocks;
    @InjectMocks
    private DefaultApplinkStatusService statusService;

    @Before
    public void setUpApplinks() throws Exception {
        when(applinkHelper.getApplicationLink(TEST_ID)).thenReturn(applicationLink);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullValuesNotAccepted() throws Exception {
        statusService.getApplinkStatus(null);
    }

    @Test(expected = PermissionException.class)
    public void noAccessForNonAdmins() throws Exception {
        permissionValidationMocks.failedValidateAdmin();

        statusService.getApplinkStatus(TEST_ID);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getStatusForNonExistingApplinkId() throws Exception {
        when(applinkHelper.getApplicationLink(ID2.applicationId())).thenThrow(new NoSuchApplinkException("test"));

        statusService.getApplinkStatus(ID2.applicationId());
    }

    @Test
    public void localOAuthStatusFailsResultsInLocalAuthenticationOff() throws Exception {
        // this should generally not happen but the service should nevertheless be resilient to it
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenThrow(new IllegalStateException());

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);

        assertErrorStatus(status, ApplinkErrorType.UNKNOWN, "java.lang.IllegalStateException: null");
        assertSame(status.getLocalAuthentication(), ApplinkOAuthStatus.OFF);
        assertErrorAuthentication(status, oAuthStatusOff());
        assertOAuthNotVerified();
    }

    @Test
    public void noRemoteApplink() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenThrow(new NoRemoteApplinkException(""));

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);

        assertErrorStatus(status, ApplinkErrorType.NO_REMOTE_APPLINK, null);
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthNotVerified();
    }

    @Test
    public void noLocalAuthenticationTokenOAuthWorking() throws Exception {
        final AuthorisationURIGenerator uriGenerator = mock(AuthorisationURIGenerator.class);
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenThrow(new ApplinkStatusAccessException(
                ApplinkErrorType.LOCAL_AUTH_TOKEN_REQUIRED, uriGenerator, ""));

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);

        assertErrorStatus(status, ApplinkErrorType.LOCAL_AUTH_TOKEN_REQUIRED, null);
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthVerified();
    }

    @Test
    public void noLocalAuthenticationTokenOAuthProblem() throws Exception {
        final AuthorisationURIGenerator uriGenerator = mock(AuthorisationURIGenerator.class);
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenThrow(new ApplinkStatusAccessException(
                ApplinkErrorType.LOCAL_AUTH_TOKEN_REQUIRED, uriGenerator, ""));
        doThrow(new RemoteOAuthException(ApplinkErrorType.OAUTH_PROBLEM, "Test",
                new OAuthMessageProblemException("test", ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN, null, null)))
                .when(oAuthConnectionVerifier).verifyOAuthConnection(applicationLink);

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);

        assertErrorStatus(status, ApplinkErrorType.OAUTH_PROBLEM, ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN);
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthVerified();
    }

    @Test
    public void noRemoteAuthenticationToken() throws Exception {
        final AuthorisationURIGenerator uriGenerator = mock(AuthorisationURIGenerator.class);
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenThrow(new ApplinkStatusAccessException(
                ApplinkErrorType.REMOTE_AUTH_TOKEN_REQUIRED, uriGenerator, ""));

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.REMOTE_AUTH_TOKEN_REQUIRED, null);
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthVerified();
    }

    @Test
    public void noRemoteAuthenticationTokenOAuthProblem() throws Exception {
        final AuthorisationURIGenerator uriGenerator = mock(AuthorisationURIGenerator.class);
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenThrow(new ApplinkStatusAccessException(
                ApplinkErrorType.REMOTE_AUTH_TOKEN_REQUIRED, uriGenerator, ""));
        doThrow(new RemoteOAuthException(ApplinkErrorType.OAUTH_PROBLEM, "Test",
                new OAuthMessageProblemException("test", ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN, null, null)))
                .when(oAuthConnectionVerifier).verifyOAuthConnection(applicationLink);

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.OAUTH_PROBLEM, ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN);
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthVerified();
    }

    @Test
    public void noOutgoingAuthentication() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenThrow(new NoOutgoingAuthenticationException(""));

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.NO_OUTGOING_AUTH, null);
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthNotVerified();
    }

    @Test
    public void remoteConnectionRefused() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink))
                .thenThrow(new RemoteNetworkException(ApplinkErrorType.CONNECTION_REFUSED, ConnectException.class, "Test",
                        new ResponseTransportException(new ConnectException("Connection refused"))));

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.CONNECTION_REFUSED, "java.net.ConnectException: Connection refused");
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthNotVerified();
    }

    @Test
    public void untrustedSslCertificate() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink))
                .thenThrow(new RemoteNetworkException(ApplinkErrorType.SSL_UNTRUSTED, SSLHandshakeException.class, "Test",
                        new ResponseException(new SSLHandshakeException("Handshake exception"))));

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertSame(status.getLocalAuthentication(), ApplinkOAuthStatus.DEFAULT);
        assertErrorStatus(status, ApplinkErrorType.SSL_UNTRUSTED, "javax.net.ssl.SSLHandshakeException: Handshake exception");
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthNotVerified();
    }

    @Test
    public void hostnameUnmatchedSslCertificate() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink))
                .thenThrow(new RemoteNetworkException(ApplinkErrorType.SSL_HOSTNAME_UNMATCHED, SSLPeerUnverifiedException.class, "Test",
                        new ResponseException(new SSLPeerUnverifiedException("Unverified Peer"))));

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertSame(status.getLocalAuthentication(), ApplinkOAuthStatus.DEFAULT);
        assertErrorStatus(status, ApplinkErrorType.SSL_HOSTNAME_UNMATCHED, "javax.net.ssl.SSLPeerUnverifiedException: Unverified Peer");
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthNotVerified();
    }

    @Test
    public void certificateUnmatchedSslCertificate() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink))
                .thenThrow(new RemoteNetworkException(ApplinkErrorType.SSL_UNMATCHED, SSLException.class, "Test",
                        new ResponseException(new SSLException("Unknown SSL Certificate mismatch"))));

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertSame(status.getLocalAuthentication(), ApplinkOAuthStatus.DEFAULT);
        assertErrorStatus(status, ApplinkErrorType.SSL_UNMATCHED, "javax.net.ssl.SSLException: Unknown SSL Certificate mismatch");
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthNotVerified();
    }

    @Test
    public void unknownRemoteStatus() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenThrow(new RemoteStatusUnknownException());

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertSame(status.getLocalAuthentication(), ApplinkOAuthStatus.DEFAULT);
        assertErrorStatus(status, ApplinkErrorType.UNKNOWN, null);
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthNotVerified();
    }

    @Test
    public void remoteVersionIncompatible() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        doThrow(new RemoteVersionIncompatibleException()).when(validationService).checkVersionCompatibility(applicationLink);

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE);
        assertErrorAuthentication(status, oAuthStatusDefault());
        assertOAuthNotVerified();
    }

    @Test
    public void oAuthConfigMismatch() throws Exception {
        ApplinkOAuthStatus local = new ApplinkOAuthStatus(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig());
        ApplinkOAuthStatus remote = new ApplinkOAuthStatus(createDefaultOAuthConfig(), createDefaultOAuthConfig());

        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(local);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenReturn(remote);
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.AUTH_LEVEL_MISMATCH))
                .when(validationService).checkOAuthMismatch(local, remote);

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.AUTH_LEVEL_MISMATCH);
        assertAuthentication(status, oAuthStatusWith(oAuthWithImpersonationConfig(), defaultOAuthConfig()), oAuthStatusDefault());
        assertOAuthNotVerified();
    }

    @Test
    public void workingApplinkNoStatusApi() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS))
                .thenReturn(capabilitiesWithNoStatusApi());

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);

        assertSame(applicationLink, status.getLink());
        assertTrue(status.isWorking());
        assertAuthentication(status, oAuthStatusDefault(), oAuthStatusDefault());
        verify(validationService, times(2)).checkOAuthSupportedCompatibility(ApplinkOAuthStatus.DEFAULT);
        assertOAuthNotVerified();
    }

    @Test
    public void workingApplinkWithStatusApi() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS))
                .thenReturn(capabilitiesWithStatusApi());

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);

        assertSame(applicationLink, status.getLink());
        assertTrue(status.isWorking());
        assertAuthentication(status, oAuthStatusDefault(), oAuthStatusDefault());
        verify(validationService, times(2)).checkOAuthSupportedCompatibility(ApplinkOAuthStatus.DEFAULT);
        assertOAuthVerified();
    }

    @Test
    public void workingApplinkWithStatusApiOAuthProblem() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS))
                .thenReturn(capabilitiesWithStatusApi());
        doThrow(new RemoteOAuthException(ApplinkErrorType.OAUTH_PROBLEM, "Test",
                new OAuthMessageProblemException("test", ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN, null, null)))
                .when(oAuthConnectionVerifier).verifyOAuthConnection(applicationLink);


        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);

        assertSame(applicationLink, status.getLink());
        assertErrorStatus(status, ApplinkErrorType.OAUTH_PROBLEM, ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN);
        assertAuthentication(status, oAuthStatusDefault(), oAuthStatusDefault());
        verify(validationService, times(2)).checkOAuthSupportedCompatibility(ApplinkOAuthStatus.DEFAULT);
        assertOAuthVerified();
    }

    @Test
    public void legacyAuthenticationApplink() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.OFF);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.OFF);
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.LEGACY_UPDATE)).when(validationService).checkLegacyAuthentication(applicationLink, ApplinkOAuthStatus.OFF, ApplinkOAuthStatus.OFF);

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);

        assertSame(applicationLink, status.getLink());
        assertErrorStatus(status, ApplinkErrorType.LEGACY_UPDATE);
        assertAuthentication(status, oAuthStatusOff(), oAuthStatusOff());
        assertOAuthNotVerified();
    }

    @Test
    public void disabledApplink() throws Exception {
        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.OFF);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.OFF);
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.DISABLED)).when(validationService).checkDisabled(ApplinkOAuthStatus.OFF, ApplinkOAuthStatus.OFF);

        ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);

        assertSame(applicationLink, status.getLink());
        assertErrorStatus(status, ApplinkErrorType.DISABLED);
        assertAuthentication(status, oAuthStatusOff(), oAuthStatusOff());
        assertOAuthNotVerified();
        verify(validationService).checkLegacyAuthentication(applicationLink, ApplinkOAuthStatus.OFF, ApplinkOAuthStatus.OFF);
    }

    @Test
    public void localCompatibilityExceptionShouldOccurBeforeNetworkError() throws NoAccessException, NoSuchApplinkException {
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.GENERIC_LINK))
                .when(validationService).checkLocalCompatibility(applicationLink);
        doThrow(createConnectionRefusedError()).when(remoteOAuthStatusService).fetchOAuthStatus(applicationLink);

        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);

        final ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.GENERIC_LINK);
    }

    @Test
    public void networkExceptionShouldOccurBeforeOAuthUnsupportedException() throws NoAccessException, NoSuchApplinkException {
        doThrow(createConnectionRefusedError()).when(remoteOAuthStatusService).fetchOAuthStatus(applicationLink);
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED)).when(validationService)
                .checkOAuthSupportedCompatibility(any(ApplinkOAuthStatus.class));

        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);

        final ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.CONNECTION_REFUSED);
    }

    @Test
    public void versionCompatibilityExceptionShouldOccurBeforeOAuthUnsupportedException() throws NoAccessException, NoSuchApplinkException {
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE))
                .when(validationService).checkVersionCompatibility(applicationLink);
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED))
                .when(validationService).checkOAuthSupportedCompatibility(any(ApplinkOAuthStatus.class));

        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);

        final ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE);
    }

    @Test
    public void oAuthUnsupportedExceptionShouldOccurBeforeOAuthMismatchException() throws NoAccessException, NoSuchApplinkException {
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED))
                .when(validationService).checkOAuthSupportedCompatibility(any(ApplinkOAuthStatus.class));

        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.IMPERSONATION);

        final ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED);
    }

    @Test
    public void legacyAuthenticationExceptionShouldOccurBeforeDisabled() throws NoAccessException, NoSuchApplinkException {
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.LEGACY_UPDATE)).when(validationService).checkLegacyAuthentication(applicationLink, ApplinkOAuthStatus.OFF, ApplinkOAuthStatus.OFF);
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.DISABLED)).when(validationService).checkDisabled(ApplinkOAuthStatus.OFF, ApplinkOAuthStatus.OFF);

        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.OFF);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.OFF);

        final ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.LEGACY_UPDATE);
    }

    @Test
    public void oauthMismatchExceptionShouldOccurBeforeLegacyAuthentication() throws NoAccessException, NoSuchApplinkException {
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.LEGACY_UPDATE)).when(validationService).checkLegacyAuthentication(applicationLink, ApplinkOAuthStatus.DEFAULT, ApplinkOAuthStatus.OFF);
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.AUTH_LEVEL_MISMATCH)).when(validationService).checkOAuthMismatch(ApplinkOAuthStatus.DEFAULT, ApplinkOAuthStatus.OFF);

        when(localOAuthStatusService.getOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.DEFAULT);
        when(remoteOAuthStatusService.fetchOAuthStatus(applicationLink)).thenReturn(ApplinkOAuthStatus.OFF);

        final ApplinkStatus status = statusService.getApplinkStatus(TEST_ID);
        assertErrorStatus(status, ApplinkErrorType.AUTH_LEVEL_MISMATCH);
    }


    private static DefaultRemoteCapabilities capabilitiesWithNoStatusApi() {
        return new DefaultRemoteCapabilities.Builder().capabilities(EnumSet.noneOf(ApplinksCapabilities.class)).build();
    }

    private static DefaultRemoteCapabilities capabilitiesWithStatusApi() {
        return new DefaultRemoteCapabilities.Builder().capabilities(EnumSet.allOf(ApplinksCapabilities.class)).build();
    }

    private static void assertErrorAuthentication(ApplinkStatus status, Matcher<ApplinkOAuthStatus> localAuthMatcher) {
        assertAuthentication(status, localAuthMatcher, Matchers.nullValue(ApplinkOAuthStatus.class));
    }

    private static void assertAuthentication(ApplinkStatus status, Matcher<ApplinkOAuthStatus> localAuthMatcher,
                                             Matcher<ApplinkOAuthStatus> remoteAuthMatcher) {
        MatcherAssert.assertThat(status.getLocalAuthentication(), localAuthMatcher);
        MatcherAssert.assertThat(status.getRemoteAuthentication(), remoteAuthMatcher);
    }

    private static RemoteNetworkException createConnectionRefusedError() {
        return new RemoteNetworkException(ApplinkErrorType.CONNECTION_REFUSED, ConnectException.class, "Boom");
    }

    @SuppressWarnings("ConstantConditions")
    private void assertErrorStatus(ApplinkStatus status, ApplinkErrorType error) {
        assertErrorStatusBase(status, error);
        assertNull(status.getError().getDetails());
    }

    @SuppressWarnings("ConstantConditions")
    private void assertErrorStatus(ApplinkStatus status, ApplinkErrorType error, String details) {
        assertErrorStatusBase(status, error);
        assertEquals(details, status.getError().getDetails());
    }

    private void assertErrorStatusBase(ApplinkStatus status, ApplinkErrorType error) {
        assertSame(applicationLink, status.getLink());
        assertFalse("Expected error applink, was working", status.isWorking());
        assertNotNull(status.getError());
        assertEquals(error, status.getError().getType());
    }

    private void assertOAuthVerified() {
        verify(oAuthConnectionVerifier).verifyOAuthConnection(applicationLink);
    }

    private void assertOAuthNotVerified() {
        verifyZeroInteractions(oAuthConnectionVerifier);
    }
}
