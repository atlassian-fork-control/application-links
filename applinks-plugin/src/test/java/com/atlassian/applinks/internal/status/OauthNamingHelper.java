package com.atlassian.applinks.internal.status;

import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDisabledConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createThreeLoOnlyConfig;

/**
 * The methods below are only created to help readability
 */
public class OauthNamingHelper {
    private OauthNamingHelper() {
        throw new UnsupportedOperationException("Do not instantiate");
    }

    public static OAuthConfig incoming(OAuthConfig config) {
        return config;
    }

    public static OAuthConfig outgoing(OAuthConfig config) {
        return config;
    }

    public static ApplinkOAuthStatus config(OAuthConfig incoming, OAuthConfig outgoing) {
        return new ApplinkOAuthStatus(incoming, outgoing);
    }

    public static OAuthConfig disabled() {
        return createDisabledConfig();
    }

    public static OAuthConfig threeLo() {
        return createThreeLoOnlyConfig();
    }

    public static OAuthConfig defaultConfig() {
        return createDefaultOAuthConfig();
    }

    public static OAuthConfig impersonation() {
        return createOAuthWithImpersonationConfig();
    }

    public static ApplinkOAuthStatus current(OAuthConfig incoming, OAuthConfig outgoing) {
        return new ApplinkOAuthStatus(incoming, outgoing);
    }

    public static ApplinkOAuthStatus other(OAuthConfig incoming, OAuthConfig outgoing) {
        return new ApplinkOAuthStatus(incoming, outgoing);
    }
}
