package com.atlassian.applinks.internal.applink;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.test.mock.SimpleServiceExceptionFactory;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;

import static com.atlassian.applinks.test.mock.TestApplinkIds.DEFAULT_ID;
import static com.atlassian.applinks.test.mock.TestApplinkIds.ID2;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultApplinkHelperTest {
    @MockApplink
    private MutableApplicationLink applicationLink;
    @Mock
    private ApplicationLinkService applicationLinkService;
    @Mock
    private MutatingApplicationLinkService mutatingApplicationLinkService;
    @Mock
    private ReadOnlyApplicationLinkService readOnlyApplicationLinkService;
    @Spy // for @InjectMocks
    private ServiceExceptionFactory serviceExceptionFactory = new SimpleServiceExceptionFactory();

    @InjectMocks
    private DefaultApplinkHelper applinkHelper;

    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);

    @Before
    public void setUpApplinks() throws Exception {
        when(applicationLinkService.getApplicationLink(DEFAULT_ID.applicationId())).thenReturn(applicationLink);
        when(mutatingApplicationLinkService.getApplicationLink(DEFAULT_ID.applicationId())).thenReturn(applicationLink);
        when(readOnlyApplicationLinkService.getApplicationLink(DEFAULT_ID.applicationId())).thenReturn(applicationLink);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void getApplicationLinkGivenNullApplinkId() throws NoSuchApplinkException {
        applinkHelper.getApplicationLink(null);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getApplicationLinkGivenNonExistingApplink() throws NoSuchApplinkException {
        applinkHelper.getApplicationLink(ID2.applicationId());
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getApplicationLinkGivenTypeNotInstalled() throws Exception {
        when(applicationLinkService.getApplicationLink(ID2.applicationId())).thenThrow(new TypeNotInstalledException(
                "test type", "test name", URI.create("http://test.com")));

        applinkHelper.getApplicationLink(ID2.applicationId());
    }

    @Test
    public void getApplicationLinkGivenExistingValidApplink() throws NoSuchApplinkException {
        assertSame(applicationLink, applinkHelper.getApplicationLink(DEFAULT_ID.applicationId()));
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void getMutableApplicationLinkGivenNullApplinkId() throws NoSuchApplinkException {
        applinkHelper.getMutableApplicationLink(null);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getMutableApplicationLinkGivenNonExistingApplink() throws NoSuchApplinkException {
        applinkHelper.getMutableApplicationLink(ID2.applicationId());
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getMutableApplicationLinkApplinkGivenTypeNotInstalled() throws Exception {
        when(mutatingApplicationLinkService.getApplicationLink(ID2.applicationId())).thenThrow(
                new TypeNotInstalledException("test type", "test name", URI.create("http://test.com")));

        applinkHelper.getMutableApplicationLink(ID2.applicationId());
    }

    @Test
    public void getMutableApplicationLinkGivenExistingValidApplink() throws NoSuchApplinkException {
        assertSame(applicationLink, applinkHelper.getMutableApplicationLink(DEFAULT_ID.applicationId()));
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void getReadOnlyApplicationLinkGivenNullApplinkId() throws NoSuchApplinkException {
        applinkHelper.getReadOnlyApplicationLink(null);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getReadOnlyApplicationLinkGivenNonExistingApplink() throws NoSuchApplinkException {
        applinkHelper.getReadOnlyApplicationLink(ID2.applicationId());
    }

    @Test
    public void getReadOnlyApplicationLinkGivenExistingValidApplink() throws NoSuchApplinkException {
        assertSame(applicationLink, applinkHelper.getReadOnlyApplicationLink(DEFAULT_ID.applicationId()));
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void makePrimaryGivenNullApplinkId() throws NoSuchApplinkException {
        applinkHelper.makePrimary(null);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void makePrimaryGivenNonExistingApplink() throws Exception {
        doThrow(IllegalArgumentException.class).when(mutatingApplicationLinkService).makePrimary(ID2.applicationId());

        applinkHelper.makePrimary(ID2.applicationId());
    }

    @Test(expected = NoSuchApplinkException.class)
    public void makePrimaryGivenTypeNotInstalled() throws Exception {
        doThrow(new TypeNotInstalledException("test type", "test name", URI.create("http://test.com")))
                .when(mutatingApplicationLinkService).makePrimary(ID2.applicationId());

        applinkHelper.makePrimary(ID2.applicationId());
    }

    @Test
    public void makePrimaryGivenExistingValidApplink() throws Exception {
        applinkHelper.makePrimary(DEFAULT_ID.applicationId());

        verify(mutatingApplicationLinkService).makePrimary(DEFAULT_ID.applicationId());
    }
}
