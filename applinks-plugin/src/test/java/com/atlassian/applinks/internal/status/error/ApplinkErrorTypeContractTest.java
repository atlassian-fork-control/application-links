package com.atlassian.applinks.internal.status.error;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.entryToMap;
import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toImmutableSet;
import static com.atlassian.applinks.test.matcher.ApplinksMatchers.toMatchers;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.join;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasKey;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Enforces that client-side applink errors definitions contained in {@code applink-status-errors.js} match the "source
 * of truth" that is the {@link ApplinkErrorType} enum.
 */
public class ApplinkErrorTypeContractTest {
    private static final String STATUS_ERRORS_CONTRACT = "applinks/internal/feature/applink-status/applink-status-errors.js";
    private static final String STATUS_ERRORS_I18N = "com/atlassian/applinks/applinks-status.properties";
    private static final String HELP_PATHS_KEYS = "com/atlassian/applinks/ual-help-paths.properties";

    /**
     * Error types that we don't render a popup for so they don't have to be mapped in JS.
     */
    private static final EnumSet<ApplinkErrorType> EXCLUDED_TYPES = EnumSet.of(
            ApplinkErrorType.DISABLED
    );

    /**
     * Error types that don't have a help link associated with them.
     */
    private static final EnumSet<ApplinkErrorType> ERRORS_WITHOUT_HELP_LINK = EnumSet.of(
            ApplinkErrorType.NO_REMOTE_APPLINK,
            ApplinkErrorType.LOCAL_AUTH_TOKEN_REQUIRED,
            ApplinkErrorType.REMOTE_AUTH_TOKEN_REQUIRED,
            ApplinkErrorType.SYSTEM_LINK,
            ApplinkErrorType.GENERIC_LINK,
            ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE,
            ApplinkErrorType.NON_ATLASSIAN
    );

    /**
     * Fields in {@code applink-status-errors.js} that do not point to actual errors, rather than to helper functions.
     */
    private static final Set<String> EXCLUDED_JS_ERRORS_FIELDS = ImmutableSet.of("find", "eachError",
            "isAccessTokenError", "hasErrorCategory");

    @Test
    public void errorsDefinedInJavascriptShouldExistInApplinksErrorType() throws Exception {
        Map<String, Map<String, Object>> jsErrors = loadStatusErrorsJs();
        assertAllJsErrorsCorrect(jsErrors);
        assertAllErrorsMapped(jsErrors);
    }

    @Test
    public void validateErrorTypeI18nKeys() throws IOException {
        Properties errorsI18n = loadAsProperties(STATUS_ERRORS_I18N);

        Set<String> missingKeys = new HashSet<>();
        for (ApplinkErrorType errorType : ApplinkErrorType.values()) {
            String i18nKey = errorType.getI18nKey().getKey();
            if (!errorsI18n.containsKey(i18nKey)) {
                missingKeys.add(i18nKey);
            }
        }
        assertTrue(format("The following keys are missing from applink-status.properties:\n%s ", join(missingKeys, "\n")),
                missingKeys.isEmpty());
    }

    @Test
    public void validateHelpPathsInApplinkStatusErrrorsJs() throws Exception {
        Map<String, Map<String, Object>> jsErrors = loadStatusErrorsJs();
        Set<Object> helpPathKeys = loadAsProperties(HELP_PATHS_KEYS).keySet();

        Set<String> errorsWithInvalidHelpLinkKey = jsErrors.entrySet().stream()
                .filter(e -> e.getValue().get("linkKey") != null)
                .filter(e -> !helpPathKeys.contains(e.getValue().get("linkKey")))
                .map(Map.Entry::getKey)
                .collect(toImmutableSet());
        assertThat("The following status errors have invalid help link key not found in ual-help-paths.properties. " +
                        "Check applink-status-errros.js to fix those: " + errorsWithInvalidHelpLinkKey,
                errorsWithInvalidHelpLinkKey, emptyIterable());

        Set<ApplinkErrorType> errorsWithoutHelpLinkKey = jsErrors.entrySet().stream()
                .filter(e -> e.getValue().get("linkKey") == null)
                .map(Map.Entry::getKey)
                .map(ApplinkErrorType::valueOf)
                .sorted() // sort so actual and expected should be in identical order
                .collect(toImmutableSet());
        assertThat("Mismatch between expected and actual status errors without help link key. Check " +
                        "applink-status-errros.js to fix that",
                errorsWithoutHelpLinkKey, equalTo(ERRORS_WITHOUT_HELP_LINK));
    }

    @Test
    public void validateAllTroubleshootingHelpPathsKeysAreUsedInApplinkStatusErrrorsJs() throws Exception {
        Set<String> allLinkKeysFromErrorsJs = loadStatusErrorsJs().entrySet().stream()
                .map(e -> e.getValue().get("linkKey"))
                .filter(Objects::nonNull)
                .map(Object::toString)
                .sorted()
                .collect(toImmutableSet());
        Set<String> allTroubleshootingHelpPathKeys = loadAsProperties(HELP_PATHS_KEYS).keySet().stream()
                .map(Object::toString)
                .filter(helpPath -> helpPath.startsWith("applinks.docs.diagnostics.troubleshoot"))
                .sorted()
                .collect(toImmutableSet());

        assertThat("Mismatch between expected and actual troubleshooting help link paths. All keys in " +
                        "ual-help-paths.properties with the prefix 'applinks.docs.diagnostics.troubleshoot' should " +
                        "be used in applink-status-errros.js",
                allLinkKeysFromErrorsJs, hasItems(toMatchers(allTroubleshootingHelpPathKeys)));
    }

    private void assertAllJsErrorsCorrect(Map<String, Map<String, Object>> jsErrors) {
        for (Map.Entry<String, Map<String, Object>> errorEntry : jsErrors.entrySet()) {
            ApplinkErrorType error = ApplinkErrorType.valueOf(errorEntry.getKey());
            assertEquals(error.name(), errorEntry.getValue().get("type"));
            assertEquals(error.getCategory().name(), errorEntry.getValue().get("category"));
        }
    }

    private void assertAllErrorsMapped(Map<String, Map<String, Object>> jsErrors) {
        for (ApplinkErrorType applinkError : ApplinkErrorType.values()) {
            if (!EXCLUDED_TYPES.contains(applinkError)) {
                assertThat(jsErrors, hasKey(applinkError.name()));
            }
        }
    }

    /**
     * The applink-status-error.js is the "contract" between java and js. Everything defined in applink-status-error.js
     * must exist in ApplinkErrorType.java.
     * <br>
     * This is because ApplinkErrorType is serialised to json and sent to the browser. Our frontend code creates
     * different dialogs based upon different values sent.
     * <br>
     * The purpose of this separation is to prevent developers misspelling the generated values in js file, and making
     * it easier to test. For example: if a developer uses "connection_refuse" instead of "connection_refused", this
     * test will automatically pick up the misspelling before any integration tests.
     *
     * @return A list of objects (represents as a map) of errors with fields category and type.
     * @throws IOException     occurs when the applink-status-errors.js is moved or no longer exist.
     * @throws ScriptException occurs when developers add more than errors in the applink-status-errors.js
     * @see ApplinkErrorType
     */
    @SuppressWarnings("unchecked")
    private Map<String, Map<String, Object>> loadStatusErrorsJs() throws IOException, ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");

        // since our applink-status-error.js is an AMD module, it uses the define function. We're providing our own
        // define function implementation that returns the underlying object instead of loading the actual js library.
        engine.eval("function define(path, deps, func) { return func(); }");

        // we assign the result of the define function we stated above to a js variable so we can easily inspect its
        // contents.
        engine.eval("var errors = " + IOUtils.toString(new InputStreamReader(getResource(STATUS_ERRORS_CONTRACT))));
        return removeExcludedFields((Map) engine.get("errors"));
    }

    private static Map<String, Map<String, Object>> removeExcludedFields(Map<String, Map<String, Object>> original) {
        return original.entrySet().stream()
                .filter(e -> !EXCLUDED_JS_ERRORS_FIELDS.contains(e.getKey()))
                .collect(entryToMap());
    }

    private static Properties loadAsProperties(String resource) throws IOException {
        Properties properties = new Properties();
        properties.load(getResource(resource));
        return properties;
    }

    private static InputStream getResource(String resource) {
        return ApplinkErrorTypeContractTest.class.getClassLoader().getResourceAsStream(resource);
    }
}