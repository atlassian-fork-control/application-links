package com.atlassian.applinks.internal.status.oauth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthConfigurator;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.internal.common.exception.ConsumerInformationUnavailableException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.test.mock.SimpleServiceExceptionFactory;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.test.mock.ApplinkHelperMocks;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.mock.PermissionValidationMocks;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import com.atlassian.oauth.Consumer;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.hamcrest.HamcrestArgumentMatcher;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.PublicKey;
import java.util.Collections;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDisabledConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.atlassian.applinks.test.matchers.ConsumerMatchers.withConfig;
import static com.atlassian.applinks.test.matchers.ConsumerMatchers.withKey;
import static com.atlassian.applinks.test.matchers.ConsumerMatchers.withName;
import static com.atlassian.applinks.test.matchers.ConsumerMatchers.withPublicKeyThat;
import static com.atlassian.applinks.test.matchers.ConsumerMatchers.withSignatureMethod;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.defaultOAuthConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.disabledConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusWith;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthWithImpersonationConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.threeLoOnlyConfig;
import static com.atlassian.applinks.test.mock.MockOAuthConsumerFactory.TEST_CONSUMER_KEY;
import static com.atlassian.applinks.test.mock.MockOAuthConsumerFactory.TEST_CONSUMER_NAME;
import static com.atlassian.applinks.test.mock.MockOAuthConsumerFactory.createConsumer;
import static com.atlassian.applinks.test.mock.MockOAuthConsumerFactory.createRsaConsumer;
import static com.atlassian.applinks.test.mock.TestApplinkIds.DEFAULT_ID;
import static com.atlassian.applinks.test.mock.TestApplinkIds.ID2;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultOAuthStatusServiceTest {
    private static final ApplicationId TEST_ID = DEFAULT_ID.applicationId();

    @Mock
    private ApplinkHelper applinkHelper;
    @Mock
    private AuthenticationConfigurationManager authenticationConfigurationManager;
    @Mock
    private ConsumerTokenStoreService consumerTokenStoreService;
    @Mock
    private ServiceProviderStoreService serviceProviderStoreService;
    @Spy // for @InjectMocks
    private ServiceExceptionFactory serviceExceptionFactory = new SimpleServiceExceptionFactory();
    @Mock
    private PermissionValidationService permissionValidationService;
    @MockApplink
    private ApplicationLink applicationLink;

    @InjectMocks
    private ApplinkHelperMocks applinkHelperMocks;
    @InjectMocks
    private PermissionValidationMocks permissionValidationMocks;
    @InjectMocks
    private StubbedOAuthConfigurator oAuthConfigurator;

    private DefaultOAuthStatusService oAuthStatusService;

    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);

    @Before
    public void setUpApplink() throws Exception {
        applinkHelperMocks.setUpExistingApplink(TEST_ID, applicationLink);
    }

    @Before
    public void createService() throws Exception {
        oAuthStatusService = new DefaultOAuthStatusService(
                applinkHelper,
                permissionValidationService,
                oAuthConfigurator
        );
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullApplinkIdNotAccepted() throws NoSuchApplinkException {
        oAuthStatusService.getOAuthStatus((ApplicationId) null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullApplinkNotAccepted() {
        oAuthStatusService.getOAuthStatus((ApplicationLink) null);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getStatusForNonExistingApplinkId() throws NoSuchApplinkException {
        applinkHelperMocks.setUpNonExistingApplink(ID2.applicationId());

        oAuthStatusService.getOAuthStatus(ID2.applicationId());
    }

    @Test
    public void getStatusForExistingApplink() throws NoSuchApplinkException {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createConsumer(true, true, true));
        setUpOAuthProviders(true, true, true);

        assertThat(oAuthStatusService.getOAuthStatus(TEST_ID), oAuthStatusWith(
                oAuthWithImpersonationConfig(),
                oAuthWithImpersonationConfig()
        ));
        permissionValidationMocks.verifyNoPermissionChecks();
    }

    @Test
    public void incomingStatusOffGivenConsumerIsNull() {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(null);

        assertThat(oAuthStatusService.getOAuthStatus(applicationLink).getIncoming(), is(disabledConfig()));
    }

    @Test
    public void incomingStatusOffGivenConsumerHas3LoDisabled() {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createConsumer(false, false, false));

        assertThat(oAuthStatusService.getOAuthStatus(applicationLink).getIncoming(), is(disabledConfig()));
    }

    @Test
    public void incomingStatus3LoGivenConsumerHas3LoEnabled() {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createConsumer(true, false, false));

        assertThat(oAuthStatusService.getOAuthStatus(applicationLink).getIncoming(), is(threeLoOnlyConfig()));
    }

    @Test
    public void incomingStatus3LoPlus2LoGivenConsumerHas3LoAndTwoLoEnabled() {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createConsumer(true, true, false));

        assertThat(oAuthStatusService.getOAuthStatus(applicationLink).getIncoming(), is(defaultOAuthConfig()));
    }

    @Test
    public void incomingStatus2LoIGivenConsumerHas2LoIEnabled() {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createConsumer(true, true, true));

        assertThat(oAuthStatusService.getOAuthStatus(applicationLink).getIncoming(), is(oAuthWithImpersonationConfig()));
    }


    @Test
    public void outgoingStatusOffGivenOAuthProviderNotConfigured() {
        // if 3LO is not configured, other providers should not matter
        setUpOAuthProviders(false, true, true);

        assertThat(oAuthStatusService.getOAuthStatus(applicationLink).getOutgoing(), is(disabledConfig()));
    }

    @Test
    public void outgoingStatus3LoGivenOAuthProviderConfigured() {
        setUpOAuthProviders(true, false, false);

        assertThat(oAuthStatusService.getOAuthStatus(applicationLink).getOutgoing(), is(threeLoOnlyConfig()));
    }

    @Test
    public void outgoingStatus3LoPlus2LoGivenOAuthAndTwoLoProviderConfigured() {
        setUpOAuthProviders(true, true, false);

        assertThat(oAuthStatusService.getOAuthStatus(applicationLink).getOutgoing(), is(defaultOAuthConfig()));
    }

    @Test
    public void outgoingStatus2LoIGivenOAuthAndTwoLoIProviderConfigured() {
        setUpOAuthProviders(true, true, true);

        assertThat(oAuthStatusService.getOAuthStatus(applicationLink).getOutgoing(), is(oAuthWithImpersonationConfig()));
    }

    @Test(expected = NoSuchApplinkException.class)
    public void updateStatusForNonExistingApplink() throws ServiceException {
        applinkHelperMocks.setUpNonExistingApplink(ID2.applicationId());

        oAuthStatusService.updateOAuthStatus(ID2.applicationId(), ApplinkOAuthStatus.DEFAULT);
    }

    @Test(expected = PermissionException.class)
    public void updateStatusNonAdmin() throws ServiceException {
        permissionValidationMocks.failedValidateAdmin();

        oAuthStatusService.updateOAuthStatus(TEST_ID, ApplinkOAuthStatus.DEFAULT);
    }

    @Test
    public void updateStatusToDefaultExistingConsumer() throws ServiceException {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createConsumer(true, false, false));

        oAuthStatusService.updateOAuthStatus(TEST_ID, ApplinkOAuthStatus.DEFAULT);

        verifyConsumerAdded(true, true, false);
        verifyOAuthProviders(true, true, false);

        permissionValidationMocks.verifyValidateAdmin("applinks.service.permission.operation.changeoauth");
    }

    @Test
    public void updateStatusToDefaultExistingRsaConsumer() throws ServiceException {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createRsaConsumer(true, false, false));

        oAuthStatusService.updateOAuthStatus(TEST_ID, ApplinkOAuthStatus.DEFAULT);

        verifyRsaConsumerAdded(true, true, false);
        verifyOAuthProviders(true, true, false);

        permissionValidationMocks.verifyValidateAdmin("applinks.service.permission.operation.changeoauth");
    }

    @Test
    public void downgradeStatusToDifferentLevelsExistingRsaConsumer() throws ServiceException {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createRsaConsumer(true, true, true));

        oAuthStatusService.updateOAuthStatus(TEST_ID,
                new ApplinkOAuthStatus(createDefaultOAuthConfig(), createDisabledConfig()));

        verifyRsaConsumerAdded(true, true, false); // downgraded to default from 2LOi
        verifyOAuthProviders(false, false, false); // downgraded to disabled from 2LOi
        // tokens should be purged when outgoing disabled
        verifyConsumerTokensRemoved();
    }

    @Test
    public void disableOutgoingStatusRemoveConsumerTokensFails() throws ServiceException {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createRsaConsumer(true, true, true));
        doThrow(IllegalStateException.class).when(consumerTokenStoreService).removeAllConsumerTokens(applicationLink);

        oAuthStatusService.updateOAuthStatus(TEST_ID, ApplinkOAuthStatus.OFF);

        verifyConsumerRemoved();
        verifyOAuthProviders(false, false, false);
        // tokens should be purged when outgoing disabled, error should be ignored
        verifyConsumerTokensRemoved();
    }

    @Test
    public void disableIncomingStatusExistingRsaConsumer() throws ServiceException {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(createRsaConsumer(true, true, false));

        oAuthStatusService.updateOAuthStatus(TEST_ID, ApplinkOAuthStatus.OFF);

        verifyConsumerTokensRemoved();
        verifyConsumerRemoved();
    }

    @Test
    public void disableIncomingStatusNoExistingConsumer() throws ServiceException {
        // simulate non existing consumer
        doThrow(IllegalStateException.class).when(serviceProviderStoreService).removeConsumer(applicationLink);

        oAuthStatusService.updateOAuthStatus(TEST_ID, ApplinkOAuthStatus.OFF);

        // should still attempt to remove the consumer
        verifyConsumerRemoved();
    }

    @Test
    public void enabledIncomingStatusNoExistingConsumerRemoteFetchSuccessful() throws ServiceException {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(null);
        oAuthConfigurator.setRemoteConsumer(createRsaConsumer(true, false, false));

        oAuthStatusService.updateOAuthStatus(TEST_ID, ApplinkOAuthStatus.DEFAULT);

        verifyRsaConsumerAdded(true, true, false);
    }

    @Test(expected = ConsumerInformationUnavailableException.class)
    public void enabledIncomingStatusNoExistingConsumerRemoteFetchFailed() throws ServiceException {
        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(null);
        oAuthConfigurator.setRemoteConsumer(null); // this will cause exception

        oAuthStatusService.updateOAuthStatus(TEST_ID, ApplinkOAuthStatus.DEFAULT);
    }

    @Test(expected = PermissionException.class)
    public void updateIncomingStatusToTwoLoIRequiresSysadmin() throws ServiceException {
        permissionValidationMocks.failedValidateSysadmin();

        oAuthStatusService.updateOAuthStatus(TEST_ID,
                new ApplinkOAuthStatus(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig()));
    }

    @Test(expected = PermissionException.class)
    public void updateOutgoingStatusToTwoLoIRequiresSysadmin() throws ServiceException {
        permissionValidationMocks.failedValidateSysadmin();

        oAuthStatusService.updateOAuthStatus(TEST_ID,
                new ApplinkOAuthStatus(createDefaultOAuthConfig(), createOAuthWithImpersonationConfig()));
    }

    private void setUpOAuthProviders(boolean threeLoEnabled, boolean twoLoEnabled, boolean twoLoIEnabled) {
        when(authenticationConfigurationManager.isConfigured(TEST_ID, OAuthAuthenticationProvider.class))
                .thenReturn(threeLoEnabled);
        when(authenticationConfigurationManager.isConfigured(TEST_ID, TwoLeggedOAuthAuthenticationProvider.class))
                .thenReturn(twoLoEnabled);
        when(authenticationConfigurationManager.isConfigured(TEST_ID,
                TwoLeggedOAuthWithImpersonationAuthenticationProvider.class))
                .thenReturn(twoLoIEnabled);
    }

    private void verifyConsumerAdded(boolean threeLoEnabled, boolean twoLoEnabled, boolean twoLoIEnabled) {
        verify(serviceProviderStoreService).addConsumer(
                argThat(new HamcrestArgumentMatcher<>(allOf(
                        withName(TEST_CONSUMER_NAME),
                        withKey(TEST_CONSUMER_KEY),
                        withConfig(threeLoEnabled, twoLoEnabled, twoLoIEnabled)))),
                eq(applicationLink));
    }

    private void verifyRsaConsumerAdded(boolean threeLoEnabled, boolean twoLoEnabled, boolean twoLoIEnabled) {
        verify(serviceProviderStoreService).addConsumer(
                argThat(new HamcrestArgumentMatcher<>(allOf(
                        withName(TEST_CONSUMER_NAME),
                        withKey(TEST_CONSUMER_KEY),
                        withSignatureMethod(Consumer.SignatureMethod.RSA_SHA1),
                        withPublicKeyThat(notNullValue(PublicKey.class)),
                        withConfig(threeLoEnabled, twoLoEnabled, twoLoIEnabled)))),
                eq(applicationLink));
    }

    private void verifyConsumerRemoved() {
        verify(serviceProviderStoreService).removeConsumer(applicationLink);
    }

    private void verifyConsumerTokensRemoved() {
        verify(consumerTokenStoreService).removeAllConsumerTokens(applicationLink);
    }

    private void verifyOAuthProviders(boolean threeLoEnabled, boolean twoLoEnabled, boolean twoLoIEnabled) {
        verifyProvider(threeLoEnabled, OAuthAuthenticationProvider.class);
        verifyProvider(twoLoEnabled, TwoLeggedOAuthAuthenticationProvider.class);
        verifyProvider(twoLoIEnabled, TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);
    }

    private void verifyProvider(boolean enabled, Class<? extends AuthenticationProvider> providerClass) {
        if (enabled) {
            verify(authenticationConfigurationManager).registerProvider(TEST_ID, providerClass,
                    Collections.emptyMap());
        } else {
            verify(authenticationConfigurationManager).unregisterProvider(TEST_ID, providerClass);
        }
    }

    public static final class StubbedOAuthConfigurator extends OAuthConfigurator {
        private Consumer remoteConsumer;

        public StubbedOAuthConfigurator(
                AuthenticationConfigurationManager authenticationConfigurationManager,
                ConsumerTokenStoreService consumerTokenStoreService,
                ServiceProviderStoreService serviceProviderStoreService,
                ServiceExceptionFactory serviceExceptionFactory) {
            super(authenticationConfigurationManager,
                    consumerTokenStoreService,
                    serviceProviderStoreService,
                    serviceExceptionFactory);
        }

        void setRemoteConsumer(Consumer consumer) {
            this.remoteConsumer = consumer;
        }

        @Override
        protected Consumer fetchConsumerInformation(ApplicationLink link)
                throws ConsumerInformationUnavailableException {
            if (remoteConsumer != null) {
                return remoteConsumer;
            } else {
                throw new ConsumerInformationUnavailableException("Test");
            }
        }
    }
}


