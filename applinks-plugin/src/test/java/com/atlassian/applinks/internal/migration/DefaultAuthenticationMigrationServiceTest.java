package com.atlassian.applinks.internal.migration;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.auth.trusted.ApplinksTrustedApps;
import com.atlassian.applinks.internal.common.exception.*;
import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.migration.remote.RemoteMigrationHelper;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.status.DefaultLegacyConfig;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.OAuthStatusService;
import com.atlassian.applinks.internal.status.oauth.remote.RemoteOAuthStatusService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultAuthenticationMigrationServiceTest {
    private static final Class<? extends AuthenticationProvider> TRUSTED_PROVIDER = TrustedAppsAuthenticationProvider.class;
    private static final Class<BasicAuthenticationProvider> BASIC_PROVIDER = BasicAuthenticationProvider.class;
    private static final ApplicationId APPLICATION_ID = new ApplicationId(UUID.randomUUID().toString());
    private static final OAuthConfig DISABLED = OAuthConfig.createDisabledConfig();
    private static final OAuthConfig TWO_LEGGED = OAuthConfig.createDefaultOAuthConfig();
    private static final OAuthConfig TWO_LEGGED_WITH_IMPERSONATION = OAuthConfig.createOAuthWithImpersonationConfig();

    @Mock
    private ApplicationLink link;
    @Mock
    private ApplinkHelper applinkHelper;
    @Mock
    private OAuthStatusService oAuthStatusService;
    @Mock
    private RemoteOAuthStatusService remoteOAuthStatusService;
    @Mock
    private RemoteMigrationHelper remoteMigrationHelper;
    @Mock
    private ServiceExceptionFactory serviceExceptionFactory;
    @Mock
    private AuthenticationConfigurationManager authenticationConfigurationManager;
    @Mock
    private PermissionValidationService permissionValidationService;
    @Captor
    private ArgumentCaptor<AuthenticationConfig> authenticationConfigCaptor;

    @InjectMocks
    private DefaultAuthenticationMigrationService authenticationMigrationService;

    @Before
    public void setUp() throws Exception {
        when(link.getId()).thenReturn(APPLICATION_ID);
        when(applinkHelper.getApplicationLink(APPLICATION_ID)).thenReturn(link);
        when(remoteMigrationHelper.getLegacyConfig(link)).thenReturn(new DefaultLegacyConfig());
        when(serviceExceptionFactory.create(eq(InvalidEntityStateException.class), any(I18nKey.class))).thenReturn(new InvalidEntityStateException(""));
    }

    @Test(expected = NoAccessException.class)
    public void migrateToAuthWithoutAdminAccess() throws Exception {
        doThrow(new PermissionException("")).when(permissionValidationService).validateAdmin();
        authenticationMigrationService.migrateToOAuth(APPLICATION_ID);
    }

    @Test(expected = InvalidEntityStateException.class)
    public void migrateWithOAuthMismatch() throws ServiceException {
        givenLocalOAuthConfig().incoming(TWO_LEGGED_WITH_IMPERSONATION).outgoing(TWO_LEGGED).build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().pre(new ApplinkOAuthStatus(DISABLED, TWO_LEGGED)).build();
        authenticationMigrationService.migrateToOAuth(APPLICATION_ID);
    }

    @Test(expected = NoAccessException.class)
    public void migrateToAuthForOutgoingTrustedWithoutLocalSysAdminAccess() throws Exception {
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().build();
        doThrow(new PermissionException("")).when(permissionValidationService).validateSysadmin();

        authenticationMigrationService.migrateToOAuth(APPLICATION_ID);
    }

    @Test(expected = NoAccessException.class)
    public void migrateToAuthForIncomingTrustedWithoutLocalSysAdminAccess() throws Exception {
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().basic(true).build();
        givenIncomingLegacyConfig(true);

        givenRemoteStatus().build();
        doThrow(new PermissionException("")).when(permissionValidationService).validateSysadmin();

        authenticationMigrationService.migrateToOAuth(APPLICATION_ID);
    }

    @Test
    public void migrateToOAuthForOutgoingTrustedWithRemoteSysAdminAccess() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED_WITH_IMPERSONATION));
        assertThat(result.incoming().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.outgoing().isTrustedConfigured(), equalTo(false));
        assertThat(result.incoming().hasLegacy(), equalTo(false));
    }

    @Test(expected = InvalidEntityStateException.class)
    public void migrateToOAuthForOutgoingTrustedWithRemoteSysAdminAccessAndFailToDisableRemoteTrusted() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();
        when(remoteMigrationHelper.disableRemoteTrustedApp(link)).thenReturn(false);

        authenticationMigrationService.migrateToOAuth(APPLICATION_ID);
    }

    @Test
    public void migrateToOAuthWithExistingOAuthInBothDirections() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().incoming(TWO_LEGGED).outgoing(TWO_LEGGED).build();
        givenOutgoingLegacyConfig().basic(true).build();
        givenIncomingLegacyConfig(true);

        givenRemoteStatus().pre(ApplinkOAuthStatus.DEFAULT).post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper, never()).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager).unregisterProvider(APPLICATION_ID, BASIC_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.outgoing().hasLegacy(), equalTo(false));
        assertThat(result.incoming().isTrustedConfigured(), equalTo(true));
    }

    @Test
    public void migrateToOAuthWithExistingIncomingOAuth() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().incoming(TWO_LEGGED).outgoing(DISABLED).build();
        givenOutgoingLegacyConfig().basic(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().pre(new ApplinkOAuthStatus(DISABLED, TWO_LEGGED))
                .post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper).migrate(eq(link), any(AuthenticationStatus.class));
        verify(authenticationConfigurationManager).unregisterProvider(APPLICATION_ID, BASIC_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.outgoing().hasLegacy(), equalTo(false));
        assertThat(result.incoming().hasLegacy(), equalTo(false));
    }

    @Test
    public void migrateToOAuthWithExistingOutgoingOAuth() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().incoming(DISABLED).outgoing(TWO_LEGGED).build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().pre(new ApplinkOAuthStatus(TWO_LEGGED, DISABLED)).post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper, never()).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.outgoing().hasLegacy(), equalTo(false));
        assertThat(result.incoming().hasLegacy(), equalTo(false));
    }

    @Test
    public void migrateToOAuthButFailedToQueryRemoteStatus() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenSuccessfulRemoteMigration();
        givenErrorWhenFetchingRemoteStatus();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper, never()).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager, never()).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED_WITH_IMPERSONATION));
        assertThat(result.incoming().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.outgoing().hasLegacy(), equalTo(true));
        assertThat(result.incoming().hasLegacy(), equalTo(false));
    }

    @Test
    public void migrateToOAuthForOutgoingTrustedWithoutRemoteSysAdminAccess() throws Exception {
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper, never()).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper, never()).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager, never()).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.outgoing().hasLegacy(), equalTo(true));
        assertThat(result.incoming().hasLegacy(), equalTo(false));
    }

    @Test
    public void migrateToOAuthForOutgoingTrustedAndIncomingTrustedWithRemoteSysAdminAccess() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(true);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED_WITH_IMPERSONATION));
        assertThat(result.incoming().getOAuthConfig(), equalTo(TWO_LEGGED_WITH_IMPERSONATION));
        assertThat(result.outgoing().hasLegacy(), equalTo(false));
        assertThat(result.incoming().hasLegacy(), equalTo(true));
    }

    @Test(expected = InvalidEntityStateException.class)
    public void migrateToOAuthForBothTrustedWithRemoteSysAdminAccessAndFailToDisableRemoteTrusted() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(true);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();
        when(remoteMigrationHelper.disableRemoteTrustedApp(link)).thenReturn(false);

        authenticationMigrationService.migrateToOAuth(APPLICATION_ID);
    }

    @Test
    public void migrateToOAuthForOutgoingTrustedAndIncomingTrustedWithoutRemoteSysAdminAccess() throws Exception {
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(true);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper, never()).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper, never()).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager, never()).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.outgoing().hasLegacy(), equalTo(true));
        assertThat(result.incoming().hasLegacy(), equalTo(true));
    }

    @Test
    public void migrateToOAuthForOutgoingTrustedAndBasicWithRemoteSysAdminAccess() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().basic(true).trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);
        verify(authenticationConfigurationManager).unregisterProvider(APPLICATION_ID, BASIC_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED_WITH_IMPERSONATION));
        assertThat(result.incoming().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.outgoing().hasLegacy(), equalTo(false));
        assertThat(result.incoming().hasLegacy(), equalTo(false));
    }

    @Test
    public void migrateToOAuthForOutgoingTrustedAndBasicWithoutRemoteSysAdminAccess() throws Exception {
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().basic(true).trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper, never()).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper, never()).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager, never()).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);
        verify(authenticationConfigurationManager, never()).unregisterProvider(APPLICATION_ID, BASIC_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.outgoing().hasLegacy(), equalTo(true));
        assertThat(result.outgoing().isBasicConfigured(), equalTo(true));
        assertThat(result.outgoing().isTrustedConfigured(), equalTo(true));
        assertThat(result.incoming().hasLegacy(), equalTo(false));
    }

    @Test
    public void migrateToOAuthForOutgoingTrustedAndExistingOAuthWithoutRemoteSysAdminAccess() throws Exception {
        givenLocalOAuthConfig().incoming(TWO_LEGGED).outgoing(TWO_LEGGED).build();
        givenOutgoingLegacyConfig().trusted(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper, never()).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager, never()).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.outgoing().hasLegacy(), equalTo(true));
        assertThat(result.outgoing().isTrustedConfigured(), equalTo(true));
        assertThat(result.incoming().hasLegacy(), equalTo(false));
    }

    @Test
    public void migrateToOAuthForOutgoingBasicWithRemoteSysAdminAccess() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().basic(true).build();
        givenIncomingLegacyConfig(false);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper, never()).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager, never()).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);
        verify(authenticationConfigurationManager).unregisterProvider(APPLICATION_ID, BASIC_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.outgoing().hasLegacy(), equalTo(false));
        assertThat(result.incoming().hasLegacy(), equalTo(false));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void migrateToOAuthForOutgoingBasicWithoutRemoteSysAdminAccess() throws Exception {
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().basic(true).build();
        givenIncomingLegacyConfig(true);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper, never()).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper, never()).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager, never()).unregisterProvider(eq(APPLICATION_ID), any(Class.class));

        assertThat(result.outgoing().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(DISABLED));
        assertThat(result.outgoing().hasLegacy(), equalTo(true));
        assertThat(result.incoming().hasLegacy(), equalTo(true));
    }

    @Test
    public void migrateToOAuthForOutgoingBasicIncomingTrustedWithRemoteSysAdminAccess() throws Exception {
        givenRemoteSysAdminAccess();
        givenLocalOAuthConfig().build();
        givenOutgoingLegacyConfig().basic(true).build();
        givenIncomingLegacyConfig(true);

        givenRemoteStatus().post(fromConfigs(givenSuccessfulRemoteMigration())).build();

        final AuthenticationStatus result = authenticationMigrationService.migrateToOAuth(APPLICATION_ID);

        verify(remoteMigrationHelper).migrate(eq(link), any(AuthenticationStatus.class));
        verify(remoteMigrationHelper).disableRemoteTrustedApp(link);
        verify(authenticationConfigurationManager, never()).unregisterProvider(APPLICATION_ID, TRUSTED_PROVIDER);
        verify(authenticationConfigurationManager).unregisterProvider(APPLICATION_ID, BASIC_PROVIDER);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(TWO_LEGGED_WITH_IMPERSONATION));
        assertThat(result.outgoing().hasLegacy(), equalTo(false));
        assertThat(result.incoming().hasLegacy(), equalTo(true));
    }

    @Test(expected = NoAccessException.class)
    public void getAuthenticationMigrationStatusWithoutPermission() throws NoAccessException, NoSuchApplinkException {
        doThrow(new PermissionException("")).when(permissionValidationService).validateAdmin();
        authenticationMigrationService.getAuthenticationMigrationStatus(link, ApplinkOAuthStatus.DEFAULT);
    }

    @Test
    public void defaultLegacyConfigWhenGetAuthenticationMigrationStatusFailedToContactRemoteServer() throws Exception {
        givenOutgoingLegacyConfig().basic(true).build();
        givenIncomingLegacyConfig(true);
        doThrow(new RemoteMigrationInvalidResponseException("")).when(remoteMigrationHelper).getLegacyConfig(link);

        AuthenticationStatus result = authenticationMigrationService.getAuthenticationMigrationStatus(link, ApplinkOAuthStatus.DEFAULT);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.outgoing().isBasicConfigured(), equalTo(true));
        assertThat(result.outgoing().isTrustedConfigured(), equalTo(false));
        assertThat(result.incoming().isTrustedConfigured(), equalTo(true));
        assertThat(result.incoming().isBasicConfigured(), equalTo(false));
    }

    @Test
    public void getAuthenticationMigrationStatus() throws Exception {
        givenOutgoingLegacyConfig().basic(true).build();
        givenIncomingLegacyConfig(true);
        when(remoteMigrationHelper.getLegacyConfig(link)).thenReturn(new DefaultLegacyConfig().basic(true));

        AuthenticationStatus result = authenticationMigrationService.getAuthenticationMigrationStatus(link, ApplinkOAuthStatus.DEFAULT);

        assertThat(result.outgoing().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.incoming().getOAuthConfig(), equalTo(TWO_LEGGED));
        assertThat(result.outgoing().isBasicConfigured(), equalTo(true));
        assertThat(result.outgoing().isTrustedConfigured(), equalTo(false));
        assertThat(result.incoming().isTrustedConfigured(), equalTo(true));
        assertThat(result.incoming().isBasicConfigured(), equalTo(true));
    }

    @Test
    public void hasRemoteSysAdminAccessWithResponseException() throws Exception {
        when(remoteMigrationHelper.hasSysAdminAccess(link)).thenThrow(new RemoteMigrationInvalidResponseException(""));
        assertFalse(authenticationMigrationService.hasRemoteSysAdminAccess(link));
    }

    @Test(expected = NoAccessException.class)
    public void hasRemoteSysAdminAccessWithoutPermission() throws Exception {
        doThrow(new PermissionException("")).when(permissionValidationService).validateAdmin();
        authenticationMigrationService.hasRemoteSysAdminAccess(link);
    }

    private class OutgoingLegacyConfigBuilder {
        private boolean trusted;
        private boolean basic;

        public OutgoingLegacyConfigBuilder trusted(boolean trusted) {
            this.trusted = trusted;
            return this;
        }

        public OutgoingLegacyConfigBuilder basic(boolean basic) {
            this.basic = basic;
            return this;
        }

        public void build() {
            when(authenticationConfigurationManager.isConfigured(APPLICATION_ID, BASIC_PROVIDER)).thenReturn(basic);
            when(authenticationConfigurationManager.isConfigured(APPLICATION_ID, TRUSTED_PROVIDER)).thenReturn(trusted);
        }
    }

    private class LocalOAuthConfigBuilder {
        private OAuthConfig incoming = DISABLED;
        private OAuthConfig outgoing = DISABLED;

        public LocalOAuthConfigBuilder incoming(OAuthConfig config) {
            this.incoming = config;
            return this;
        }

        public LocalOAuthConfigBuilder outgoing(OAuthConfig config) {
            this.outgoing = config;
            return this;
        }

        public void build() {
            when(oAuthStatusService.getOAuthStatus(link)).thenReturn(new ApplinkOAuthStatus(incoming, outgoing));
        }
    }

    private class RemoteOAuthStatusBuilder {
        private ApplinkOAuthStatus pre;
        private ApplinkOAuthStatus post;

        public RemoteOAuthStatusBuilder() {
            final ApplinkOAuthStatus localStatus = oAuthStatusService.getOAuthStatus(link);
            pre = new ApplinkOAuthStatus(localStatus.getOutgoing(), localStatus.getIncoming());
            post = new ApplinkOAuthStatus(localStatus.getOutgoing(), localStatus.getIncoming());
        }

        public RemoteOAuthStatusBuilder pre(ApplinkOAuthStatus pre) {
            this.pre = pre;
            return this;
        }

        public RemoteOAuthStatusBuilder post(ApplinkOAuthStatus post) {
            this.post = post;
            return this;
        }

        public void build() throws NoAccessException {
            when(remoteOAuthStatusService.fetchOAuthStatus(link)).thenReturn(pre).thenReturn(post);
        }

    }

    private void givenErrorWhenFetchingRemoteStatus() throws NoAccessException {
        when(remoteOAuthStatusService.fetchOAuthStatus(link)).thenThrow(new PermissionException(""));
    }

    private OutgoingLegacyConfigBuilder givenOutgoingLegacyConfig() {
        return new OutgoingLegacyConfigBuilder();
    }

    private void givenIncomingLegacyConfig(boolean trusted) {
        if (trusted) {
            when(link.getProperty(ApplinksTrustedApps.PROPERTY_TRUSTED_APPS_INCOMING_ID)).thenReturn(new Object());
        }
    }

    private LocalOAuthConfigBuilder givenLocalOAuthConfig() {
        return new LocalOAuthConfigBuilder();
    }

    private void givenRemoteSysAdminAccess() throws Exception {
        when(remoteMigrationHelper.disableRemoteTrustedApp(link)).thenReturn(true);
        when(remoteMigrationHelper.hasSysAdminAccess(link)).thenReturn(true);
    }

    private AuthenticationStatus givenSuccessfulRemoteMigration() throws Exception {
        final boolean outgoingBasic = authenticationConfigurationManager.isConfigured(APPLICATION_ID, BASIC_PROVIDER);
        final boolean outgoingTrusted = authenticationConfigurationManager.isConfigured(APPLICATION_ID, TRUSTED_PROVIDER);
        final boolean incomingTrusted = link.getProperty(ApplinksTrustedApps.PROPERTY_TRUSTED_APPS_INCOMING_ID) != null;

        final ApplinkOAuthStatus local = oAuthStatusService.getOAuthStatus(link);
        final OAuthConfig outgoing = local.getOutgoing().isEnabled() ? local.getOutgoing() : (outgoingTrusted ? TWO_LEGGED_WITH_IMPERSONATION : (outgoingBasic ? TWO_LEGGED : DISABLED));
        final OAuthConfig incoming = local.getIncoming().isEnabled() ? local.getIncoming() : (incomingTrusted ? TWO_LEGGED_WITH_IMPERSONATION : DISABLED);

        final AuthenticationStatus configs = new AuthenticationStatus(
                new AuthenticationConfig(incoming, false, incomingTrusted), new AuthenticationConfig(outgoing, outgoingBasic, outgoingTrusted)
        );

        when(remoteMigrationHelper.migrate(any(ApplicationLink.class), any(AuthenticationStatus.class))).thenReturn(configs);
        return configs;
    }

    private ApplinkOAuthStatus fromConfigs(AuthenticationStatus configs) {
        return new ApplinkOAuthStatus(configs.outgoing().getOAuthConfig(), configs.incoming().getOAuthConfig());
    }

    private RemoteOAuthStatusBuilder givenRemoteStatus() throws NoAccessException {
        return new RemoteOAuthStatusBuilder();
    }
}
