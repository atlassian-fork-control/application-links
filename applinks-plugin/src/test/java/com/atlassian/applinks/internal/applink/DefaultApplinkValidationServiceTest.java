package com.atlassian.applinks.internal.applink;

import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.applinks.internal.common.exception.DetailedErrors;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.exception.ValidationException;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.mock.MockI18nResolver;
import com.atlassian.applinks.test.mock.TestApplinkIds;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;

import static com.atlassian.applinks.internal.common.test.matchers.DetailedErrorsMatchers.detailedError;
import static com.atlassian.applinks.internal.common.test.matchers.DetailedErrorsMatchers.withErrorsThat;
import static com.atlassian.applinks.test.matcher.ApplinksMatchers.instanceOfMatching;
import static com.atlassian.applinks.test.mock.TestApplinkIds.DEFAULT_ID;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@SuppressWarnings("unchecked")
public class DefaultApplinkValidationServiceTest {
    private static final String DEFAULT_APPLINK_NAME = "Test applink";
    private static final String DEFAULT_URL = "http://test.com";

    @Mock
    private ApplinkHelper applinkHelper;
    @Mock
    private ReadOnlyApplicationLinkService applicationLinkService;
    @Spy // for @InjectMocks
    private MockI18nResolver i18nResolver = new MockI18nResolver();

    @InjectMocks
    private DefaultApplinkValidationService applinkValidationService;

    @MockApplink(id = TestApplinkIds.ID1, name = DEFAULT_APPLINK_NAME, rpcUrl = DEFAULT_URL, url = DEFAULT_URL)
    private ReadOnlyApplicationLink applink1;
    @MockApplink(id = TestApplinkIds.ID2, name = DEFAULT_APPLINK_NAME + " 2", rpcUrl = DEFAULT_URL + "2-rpc",
            url = DEFAULT_URL + "2-display")
    private ReadOnlyApplicationLink applink2;
    @MockApplink(id = TestApplinkIds.ID3, name = DEFAULT_APPLINK_NAME + " 3", rpcUrl = DEFAULT_URL + "3-rpc",
            url = DEFAULT_URL + "3-display")
    private ReadOnlyApplicationLink applink3;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    @Rule
    public final MockApplinksRule mockApplinks = new MockApplinksRule(this);

    @Before
    public void setUpApplinkHelper() throws ServiceException {
        when(applinkHelper.getReadOnlyApplicationLink(DEFAULT_ID.applicationId())).thenReturn(applink1);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void validateUpdateGivenNullApplink() throws ServiceException {
        applinkValidationService.validateUpdate((ReadOnlyApplicationLink) null, createValidDetails());
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void validateUpdateGivenValidApplinkIdNullApplinkDetails() throws ServiceException {
        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(), null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void validateUpdateGivenValidApplinkNullApplinkDetails() throws ServiceException {
        applinkValidationService.validateUpdate(applink1, null);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void validateUpdateGivenInvalidId() throws ServiceException {
        when(applinkHelper.getReadOnlyApplicationLink(TestApplinkIds.ID2.applicationId()))
                .thenThrow(new NoSuchApplinkException("not found"));

        applinkValidationService.validateUpdate(TestApplinkIds.ID2.applicationId(), createValidDetails());
    }

    @Test
    public void validateUpdateWithApplinkIdGivenValidDetails() throws ServiceException {
        setUpDefaultExistingLinks();

        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(), createValidDetails());

        verify(applicationLinkService).getApplicationLinks();
    }

    @Test
    public void validateUpdateWithApplinkGivenValidDetails() throws ServiceException {
        setUpDefaultExistingLinks();

        applinkValidationService.validateUpdate(applink1, createValidDetails());

        verify(applicationLinkService).getApplicationLinks();
    }

    @Test
    public void validateUpdateGivenSystemLink() throws ServiceException {
        expectedException.expect(ValidationException.class);
        expectedException.expect(instanceOfMatching(DetailedErrors.class, withErrorsThat(
                detailedError("applinks.service.error.validation.applink.system"))));
        when(applink1.isSystem()).thenReturn(true);

        setUpDefaultExistingLinks();

        applinkValidationService.validateUpdate(applink1, createValidDetails());
    }

    @Test
    public void validateUpdateGivenRpcUrlInvalidProtocol() throws ServiceException {
        expectedException.expect(ValidationException.class);
        expectedException.expect(instanceOfMatching(DetailedErrors.class, withErrorsThat(
                detailedError("rpcUrl", "applinks.service.error.validation.applink.url.malformed"))));

        setUpDefaultExistingLinks();

        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(),
                createDetails("nosuchthing://test.com", DEFAULT_URL));
    }

    @Test
    public void validateUpdateGivenRpcUrlNonAbsolute() throws ServiceException {
        expectedException.expect(ValidationException.class);
        expectedException.expect(instanceOfMatching(DetailedErrors.class, withErrorsThat(
                detailedError("rpcUrl", "applinks.service.error.validation.applink.url.nonabsolute"))));

        setUpDefaultExistingLinks();

        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(), createDetails("/test.com", DEFAULT_URL));
    }

    @Test
    public void validateUpdateGivenDisplayUrlInvalidProtocol() throws ServiceException {
        expectedException.expect(ValidationException.class);
        expectedException.expect(instanceOfMatching(DetailedErrors.class, withErrorsThat(
                detailedError("displayUrl", "applinks.service.error.validation.applink.url.malformed"))));

        setUpDefaultExistingLinks();

        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(),
                createDetails(DEFAULT_URL, "nosuchthing://test.com"));
    }

    @Test
    public void validateUpdateGivenDisplayUrlNonAbsolute() throws ServiceException {
        expectedException.expect(ValidationException.class);
        expectedException.expect(instanceOfMatching(DetailedErrors.class, withErrorsThat(
                detailedError("displayUrl", "applinks.service.error.validation.applink.url.nonabsolute"))));

        setUpDefaultExistingLinks();

        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(), createDetails(DEFAULT_URL, "/test.com"));
    }

    @Test
    public void validateUpdateGivenRpcUrlAndDisplayUrlInvalid() throws ServiceException {
        expectedException.expect(ValidationException.class);
        expectedException.expect(instanceOfMatching(DetailedErrors.class, withErrorsThat(
                detailedError("rpcUrl", "applinks.service.error.validation.applink.url.malformed"),
                detailedError("displayUrl", "applinks.service.error.validation.applink.url.malformed")
        )));

        setUpDefaultExistingLinks();

        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(),
                createDetails("nosuchthing://test.com", "nosuchthing://test.com"));
    }

    @Test
    public void validateUpdateGivenDuplicateName() throws ServiceException {
        expectedException.expect(ValidationException.class);
        expectedException.expect(instanceOfMatching(DetailedErrors.class, withErrorsThat(
                detailedError("name", "applinks.service.error.validation.applink.duplicate.name"))));

        setUpDefaultExistingLinks();
        // name as in applink2
        ApplicationLinkDetails duplicateNameDetails = createDetails(DEFAULT_APPLINK_NAME + " 2", DEFAULT_URL,
                DEFAULT_URL);

        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(), duplicateNameDetails);
    }

    @Test
    public void validateUpdateGivenDuplicateRpcUrl() throws ServiceException {
        expectedException.expect(ValidationException.class);
        expectedException.expect(instanceOfMatching(DetailedErrors.class, withErrorsThat(
                detailedError("rpcUrl", "applinks.service.error.validation.applink.duplicate.rpcUrl"))));

        setUpDefaultExistingLinks();
        // rpcUrl as in applink2
        ApplicationLinkDetails duplicateRpcUrlDetails = createDetails(DEFAULT_APPLINK_NAME, DEFAULT_URL + "2-rpc",
                DEFAULT_URL);

        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(), duplicateRpcUrlDetails);
    }

    @Test
    public void validateUpdateGivenDuplicateDisplayUrl() throws ServiceException {
        expectedException.expect(ValidationException.class);
        expectedException.expect(instanceOfMatching(DetailedErrors.class, withErrorsThat(
                detailedError("displayUrl", "applinks.service.error.validation.applink.duplicate.displayUrl"))));

        setUpDefaultExistingLinks();
        // displayUrl as in applink2
        ApplicationLinkDetails duplicateDisplayUrlDetails = createDetails(DEFAULT_APPLINK_NAME, DEFAULT_URL,
                DEFAULT_URL + "2-display");

        applinkValidationService.validateUpdate(DEFAULT_ID.applicationId(), duplicateDisplayUrlDetails);
    }

    private static ApplicationLinkDetails createDetails(String name, String rpcUrl, String displayUrl) {
        return ApplicationLinkDetails.builder()
                .name(name)
                .rpcUrl(URI.create(rpcUrl))
                .displayUrl(URI.create(displayUrl))
                .build();
    }

    private static ApplicationLinkDetails createDetails(String rpcUrl, String displayUrl) {
        return createDetails(DEFAULT_APPLINK_NAME, rpcUrl, displayUrl);
    }

    private static ApplicationLinkDetails createValidDetails() {
        return createDetails(DEFAULT_APPLINK_NAME, DEFAULT_URL, DEFAULT_URL);
    }

    private void setUpDefaultExistingLinks() {
        setUpExistingLinks(applink1, applink2, applink3);
    }

    private void setUpExistingLinks(ReadOnlyApplicationLink... links) {
        when(applicationLinkService.getApplicationLinks()).thenReturn(ImmutableList.copyOf(links));
    }
}
