package com.atlassian.applinks.internal.status.oauth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.core.rest.ManifestResource;
import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.oauth.remote.TwoLeggedOAuthConnectionVerifier;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.test.mock.MatchingMockRequestAnswer;
import com.atlassian.applinks.test.mock.MockApplicationLinkRequest;
import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.mock.TestApplinkIds;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.ResponseException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.ConnectException;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.test.matchers.status.ApplinkErrorMatchers.withType;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyIterable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TwoLeggedOAuthConnectionVerifierTest {
    @Mock
    private AuthenticationConfigurationManager authenticationConfigurationManager;

    @MockApplink
    private ApplicationLink applink;
    @Mock
    private ApplicationLinkRequestFactory requestFactory;

    @InjectMocks
    private TwoLeggedOAuthConnectionVerifier verifier;

    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);
    @Rule
    public final ExpectedException expectedApplinkError = ExpectedException.none();

    private MatchingMockRequestAnswer requestAnswer = new MatchingMockRequestAnswer();

    @Before
    public void setUpRequestResponse() throws Exception {
        when(applink.createAuthenticatedRequestFactory(TwoLeggedOAuthAuthenticationProvider.class))
                .thenReturn(requestFactory);
        when(requestFactory.createRequest(any(), any())).thenAnswer(requestAnswer);
    }

    @Test
    public void noRequestMadeIfNoOutgoingTwoLoConfigured() {
        setUpOutgoingTwoLo(false);

        verifier.verifyOAuthConnection(applink);

        verifyNoRequestsMade();
    }

    @Test
    public void networkErrorConnectionRefused() {
        expectedApplinkError.expect(withType(ApplinkErrorType.CONNECTION_REFUSED));

        setUpOutgoingTwoLoAvailable();
        setUpManifestResponse(new MockApplicationLinkResponse().setResponseException(
                new ResponseException(new ConnectException("Connection refused"))));

        verifier.verifyOAuthConnection(applink);
    }

    @Test
    public void authLevelUnsupported() {
        expectedApplinkError.expect(withType(ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED));

        setUpOutgoingTwoLoAvailable();
        setUpManifestResponse(new MockApplicationLinkResponse()
                .setHeader(ApplinksOAuth.WWW_AUTHENTICATE, "OAuth xxx")
                .setStatus(Response.Status.UNAUTHORIZED));

        verifier.verifyOAuthConnection(applink);
    }

    @Test
    public void unexpectedStatus401NoOAuthHeader() {
        expectedApplinkError.expect(withType(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS));

        setUpOutgoingTwoLoAvailable();
        setUpManifestResponse(new MockApplicationLinkResponse()
                .setHeader(ApplinksOAuth.WWW_AUTHENTICATE, "Basic xxx")
                .setStatus(Response.Status.UNAUTHORIZED));

        verifier.verifyOAuthConnection(applink);
    }

    @Test
    public void unexpectedStatus403() {
        expectedApplinkError.expect(withType(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS));

        setUpOutgoingTwoLoAvailable();
        setUpManifestResponse(new MockApplicationLinkResponse().setStatus(Response.Status.FORBIDDEN));

        verifier.verifyOAuthConnection(applink);
    }

    @Test
    public void workingOAuth() {
        setUpOutgoingTwoLoAvailable();
        setUpManifestResponse(new MockApplicationLinkResponse().setStatus(Response.Status.OK));

        verifier.verifyOAuthConnection(applink);

        verifyOneRequestMade();
    }

    private void setUpManifestResponse(MockApplicationLinkResponse response) {
        requestAnswer.addResponse(MethodType.GET, "/rest/applinks/latest/" + ManifestResource.CONTEXT, response);
    }

    private void setUpOutgoingTwoLoAvailable() {
        setUpOutgoingTwoLo(true);
    }

    private void setUpOutgoingTwoLo(boolean available) {
        when(authenticationConfigurationManager.isConfigured(TestApplinkIds.DEFAULT_ID.applicationId(),
                TwoLeggedOAuthAuthenticationProvider.class)).thenReturn(available);
    }

    private void verifyOneRequestMade() {
        assertThat("Expected 1 executed request to the Manifest resource", requestAnswer.executedRequests(),
                Matchers.<MockApplicationLinkRequest>iterableWithSize(1));
    }

    private void verifyNoRequestsMade() {
        assertThat("Expected no executed requests", requestAnswer.executedRequests(), emptyIterable());
    }
}
