package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntity;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntityListEntity;
import com.atlassian.applinks.internal.status.LegacyConfig;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class QueryLegacyAuthenticationTest {
    @Mock
    private ApplicationLink link;
    @Mock
    private ApplicationLinkRequestFactory factory;
    @Mock
    private ApplicationLinkRequest applinkRequest;
    @Mock
    private Response response;
    private QueryLegacyAuthentication queryLegacyAuthentication;
    private final ApplicationId applicationId = new ApplicationId(UUID.randomUUID().toString());

    @Before
    public void setUp() throws CredentialsRequiredException, ResponseException {
        when(link.createAuthenticatedRequestFactory(TrustedAppsAuthenticationProvider.class)).thenReturn(factory);
        when(factory.createRequest(any(), any())).thenReturn(applinkRequest);
        when(response.getResponseBodyAsString()).thenReturn("");
        givenApplinkRequest();
        doAnswer(invocation -> {
            RemoteActionHandler remoteActionHandler = (RemoteActionHandler) invocation.getArguments()[0];
            remoteActionHandler.handle(response);
            return null;
        }).when(applinkRequest).execute(any(RemoteActionHandler.class));
        queryLegacyAuthentication = new QueryLegacyAuthentication();
    }

    @Test
    public void providerHasNotBeenConfigured() throws CredentialsRequiredException, IOException, ResponseException {
        assertFalse(queryLegacyAuthentication.execute(link, applicationId, BasicAuthenticationProvider.class));
        final LegacyConfig legacyConfig = queryLegacyAuthentication.getLegacyConfig();
        assertFalse(legacyConfig.isBasicConfigured());
        assertFalse(legacyConfig.isTrustedConfigured());
    }

    @Test
    public void remoteHasNoLegacy() throws CredentialsRequiredException, IOException, ResponseException {
        final AuthenticationProviderEntityListEntity listEntity = createListEntity(OAuthAuthenticationProvider.class);

        when(response.isSuccessful()).thenReturn(true);
        when(response.getEntity(AuthenticationProviderEntityListEntity.class)).thenReturn(listEntity);
        queryLegacyAuthentication.execute(link, applicationId, TrustedAppsAuthenticationProvider.class);
        final LegacyConfig legacyConfig = queryLegacyAuthentication.getLegacyConfig();
        assertFalse(legacyConfig.isBasicConfigured());
        assertFalse(legacyConfig.isTrustedConfigured());
    }

    @Test
    public void remoteHasTrustedLegacy() throws CredentialsRequiredException, IOException, ResponseException {
        final AuthenticationProviderEntityListEntity listEntity = createListEntity(TrustedAppsAuthenticationProvider.class, OAuthAuthenticationProvider.class);

        when(response.getStatusCode()).thenReturn(200);
        when(response.getEntity(AuthenticationProviderEntityListEntity.class)).thenReturn(listEntity);
        queryLegacyAuthentication.execute(link, applicationId, TrustedAppsAuthenticationProvider.class);
        final LegacyConfig legacyConfig = queryLegacyAuthentication.getLegacyConfig();
        assertFalse(legacyConfig.isBasicConfigured());
        assertTrue(legacyConfig.isTrustedConfigured());
    }

    @Test
    public void remoteHasTrustedAndBasicLegacy() throws CredentialsRequiredException, IOException, ResponseException {
        final AuthenticationProviderEntityListEntity listEntity = createListEntity(
                TrustedAppsAuthenticationProvider.class,
                OAuthAuthenticationProvider.class,
                BasicAuthenticationProvider.class);

        when(response.getStatusCode()).thenReturn(200);
        when(response.getEntity(AuthenticationProviderEntityListEntity.class)).thenReturn(listEntity);
        queryLegacyAuthentication.execute(link, applicationId, TrustedAppsAuthenticationProvider.class);
        final LegacyConfig legacyConfig = queryLegacyAuthentication.getLegacyConfig();
        assertTrue(legacyConfig.isBasicConfigured());
        assertTrue(legacyConfig.isTrustedConfigured());
    }

    private void givenApplinkRequest() {
        when(applinkRequest.setHeader(any(), any())).thenReturn(applinkRequest);
        when(applinkRequest.addHeader(any(), any())).thenReturn(applinkRequest);
        when(applinkRequest.setConnectionTimeout(anyInt())).thenReturn(applinkRequest);
        when(applinkRequest.setSoTimeout(anyInt())).thenReturn(applinkRequest);
        when(applinkRequest.setFollowRedirects(anyBoolean())).thenReturn(applinkRequest);
    }

    private AuthenticationProviderEntityListEntity createListEntity(Class<? extends AuthenticationProvider>... clazzes) {
        return new AuthenticationProviderEntityListEntity(
                Arrays.stream(clazzes).map(clazz -> new AuthenticationProviderEntity(null, null, clazz.getName(), null)).collect(Collectors.toList())
        );

    }
}