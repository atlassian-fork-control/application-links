package com.atlassian.applinks.internal.web;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.exception.InvalidApplicationIdException;
import com.atlassian.applinks.internal.common.exception.InvalidRequestException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.test.mock.SimpleServiceExceptionFactory;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.mock.TestApplinkIds;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import static com.atlassian.applinks.test.mock.TestApplinkIds.DEFAULT_ID;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultWebApplinkHelperTest {
    @Mock
    private ApplinkHelper applinkHelper;
    @Spy
    private SimpleServiceExceptionFactory serviceExceptionFactory = new SimpleServiceExceptionFactory();

    @MockApplink(id = TestApplinkIds.ID1)
    private ApplicationLink applink;

    private MockHttpServletRequest request = new MockHttpServletRequest();

    @InjectMocks
    private DefaultWebApplinkHelper webApplinkHelper;

    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);

    @Test(expected = InvalidRequestException.class)
    public void getApplicationLinkGivenNullPathInfo() throws ServiceException {
        request.setPathInfo(null);

        webApplinkHelper.getApplicationLink(request);
    }

    @Test(expected = InvalidRequestException.class)
    public void getApplicationLinkGivenEmptyPathInfo() throws ServiceException {
        request.setPathInfo("");

        webApplinkHelper.getApplicationLink(request);
    }

    @Test(expected = InvalidRequestException.class)
    public void getApplicationLinkGivenPathInfoWithNoComponents() throws ServiceException {
        request.setPathInfo("/");

        webApplinkHelper.getApplicationLink(request);
    }

    @Test(expected = InvalidApplicationIdException.class)
    public void getApplicationLinkGivenPathInfoWithInvalidApplicationId() throws ServiceException {
        request.setPathInfo("/blah/some/request");

        webApplinkHelper.getApplicationLink(request);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getApplicationLinkGivenPathInfoNotExistingApplicationId() throws ServiceException {
        when(applinkHelper.getApplicationLink(TestApplinkIds.ID2.applicationId()))
                .thenThrow(new NoSuchApplinkException("Nope"));

        request.setPathInfo(TestApplinkIds.ID2.id() + "/some/request");

        webApplinkHelper.getApplicationLink(request);
    }

    @Test
    public void getApplicationLinkGivenPathExistingApplicationIdWithLeadingSlash() throws ServiceException {
        setUpExistingApplink();

        request.setPathInfo("/" + DEFAULT_ID.id() + "/some/request");

        assertSame(applink, webApplinkHelper.getApplicationLink(request));
    }

    @Test
    public void getApplicationLinkGivenPathExistingApplicationIdWithNoLeadingSlash() throws ServiceException {
        setUpExistingApplink();

        request.setPathInfo(DEFAULT_ID.id() + "/some/request");

        assertSame(applink, webApplinkHelper.getApplicationLink(request));
    }

    private void setUpExistingApplink() throws NoSuchApplinkException {
        when(applinkHelper.getApplicationLink(DEFAULT_ID.applicationId())).thenReturn(applink);
    }
}
