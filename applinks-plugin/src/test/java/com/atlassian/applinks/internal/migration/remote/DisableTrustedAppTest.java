package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DisableTrustedAppTest {
    @Mock
    private ApplicationLink link;
    @Mock
    private ApplicationLinkRequestFactory factory;
    @Mock
    private ApplicationLinkRequest applinkRequest;
    @Mock
    private Response response;
    private final ApplicationId applicationId = new ApplicationId(UUID.randomUUID().toString());

    @Test
    public void execute() throws Exception {
        when(link.createAuthenticatedRequestFactory(TrustedAppsAuthenticationProvider.class)).thenReturn(factory);
        when(factory.createRequest(any(Request.MethodType.class), any(String.class))).thenReturn(applinkRequest);
        when(response.getResponseBodyAsString()).thenReturn("");
        when(response.getStatusCode()).thenReturn(200);

        doAnswer(invocation -> {
            RemoteActionHandler actionHandler = (RemoteActionHandler) invocation.getArguments()[0];
            actionHandler.handle(response);
            return null;
        }).when(applinkRequest).execute(any(RemoteActionHandler.class));

        assertTrue(DisableTrustedApp.INSTANCE.execute(link, applicationId, TrustedAppsAuthenticationProvider.class));

        verify(factory).createRequest(Request.MethodType.DELETE, DisableTrustedApp.TRUSTED_AUTOCONFIG_PATH + applicationId.toString());
        verify(applinkRequest).setHeader("X-Atlassian-Token", "no-check");
        verify(applinkRequest).setConnectionTimeout(TryWithAuthentication.TIME_OUT_IN_SECONDS * 1000);
        verify(applinkRequest).setSoTimeout(TryWithAuthentication.TIME_OUT_IN_SECONDS * 1000);
        verify(applinkRequest).execute(any(RemoteActionHandler.class));
    }

    @Test
    public void executeWithInvalidFactory() throws Exception {
        when(link.createAuthenticatedRequestFactory(TrustedAppsAuthenticationProvider.class)).thenReturn(null);
        assertFalse(DisableTrustedApp.INSTANCE.execute(link, applicationId, TrustedAppsAuthenticationProvider.class));
    }

    @Test
    public void executeReturnWithContent() throws Exception {
        when(link.createAuthenticatedRequestFactory(TrustedAppsAuthenticationProvider.class)).thenReturn(factory);
        when(factory.createRequest(any(Request.MethodType.class), any(String.class))).thenReturn(applinkRequest);
        when(response.getResponseBodyAsString()).thenReturn("blah");
        when(response.getStatusCode()).thenReturn(200);

        doAnswer(invocation -> {
            RemoteActionHandler actionHandler = (RemoteActionHandler) invocation.getArguments()[0];
            actionHandler.handle(response);
            return null;
        }).when(applinkRequest).execute(any(RemoteActionHandler.class));

        assertFalse(DisableTrustedApp.INSTANCE.execute(link, applicationId, TrustedAppsAuthenticationProvider.class));
    }


}