package com.atlassian.applinks.internal.rest.interceptor;

import com.atlassian.applinks.internal.common.exception.DetailedError;
import com.atlassian.applinks.internal.common.exception.DetailedErrors;
import com.atlassian.applinks.internal.common.exception.MockServiceException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.common.exception.SimpleDetailedError;
import com.atlassian.applinks.internal.common.exception.SimpleDetailedErrors;
import com.atlassian.applinks.internal.rest.model.RestError;
import com.atlassian.applinks.internal.rest.model.RestErrors;
import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.google.common.collect.ImmutableList;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.core.HttpRequestContext;
import com.sun.jersey.api.core.HttpResponseContext;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.Response;
import java.lang.reflect.InvocationTargetException;

import static com.atlassian.applinks.test.matchers.JaxRsResponseMatchers.withEntityThat;
import static com.atlassian.applinks.test.matchers.JaxRsResponseMatchers.withStatus;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ServiceExceptionInterceptorTest {
    @Mock
    private MethodInvocation invocation;
    @Mock
    private HttpContext httpContext;
    @Mock
    private HttpRequestContext requestContext;
    @Mock
    private HttpResponseContext responseContext;

    // we need to use captor for better information in case of an error - argThat uses toString() which doesn't help
    // in case of JerseyRequest
    @Captor
    private ArgumentCaptor<Response> responseCaptor;

    private ServiceExceptionInterceptor interceptor = new ServiceExceptionInterceptor();

    @Before
    public void setUpInvocation() {
        when(invocation.getHttpContext()).thenReturn(httpContext);
        when(httpContext.getRequest()).thenReturn(requestContext);
        when(httpContext.getResponse()).thenReturn(responseContext);
    }

    @Test
    public void shouldUseFallback400() throws Exception {
        doThrow(new InvocationTargetException(new ServiceExceptionWithoutMapping("Test"))).when(invocation).invoke();

        interceptor.intercept(invocation);

        assertResponseStatus(Response.Status.BAD_REQUEST);
    }

    @Test
    public void shouldHandleNestedServiceExceptions() throws Exception {
        doThrow(new InvocationTargetException(new RuntimeException(new PermissionException("Test"))))
                .when(invocation).invoke();

        interceptor.intercept(invocation);

        // should extract PermissionException out of the causal chain
        assertResponse(Response.Status.FORBIDDEN,
                equalTo(new RestErrors(Response.Status.FORBIDDEN, new RestError(null, "Test", PermissionException.class.getName() + ": Test"))));
    }

    @Test
    public void shouldHandleServiceExceptionsDetailedError() throws Exception {
        DetailedError error = new SimpleDetailedError("test", "Test summary", null);
        doThrow(new InvocationTargetException(new RuntimeException(
                new DetailedErrorServiceException(error, "Some other message")))).when(invocation).invoke();

        interceptor.intercept(invocation);

        // no mapping -> 400
        assertResponse(Response.Status.BAD_REQUEST,
                equalTo(new RestErrors(Response.Status.BAD_REQUEST, new RestError("test", "Test summary", null))));
    }

    @Test
    public void shouldHandleServiceExceptionsDetailedErrors() throws Exception {
        DetailedErrors errors = new SimpleDetailedErrors.Builder()
                .error("test1", "Test error 1", "Test details 1")
                .error("test2", "Test error 2", "Test details 2")
                .build();

        doThrow(new InvocationTargetException(new RuntimeException(
                new DetailedErrorsServiceException(errors, "Some other message")))).when(invocation).invoke();

        interceptor.intercept(invocation);

        // no mapping -> 400
        assertResponse(Response.Status.BAD_REQUEST,
                equalTo(new RestErrors(Response.Status.BAD_REQUEST, ImmutableList.of(
                        new RestError("test1", "Test error 1", singletonList("Test details 1")),
                        new RestError("test2", "Test error 2", singletonList("Test details 2"))))));
    }

    @Test(expected = InvocationTargetException.class)
    public void shouldPropagateNonServiceExceptions() throws Exception {
        doThrow(new InvocationTargetException(new RuntimeException("Not a service exception")))
                .when(invocation).invoke();

        interceptor.intercept(invocation);
    }

    private void assertResponseStatus(Response.Status expected) {
        assertResponse(withStatus(expected));
    }

    private void assertResponse(Response.Status expectedStatus, Matcher<?> entityMatcher) {
        assertResponse(allOf(withStatus(expectedStatus), withEntityThat(entityMatcher)));
    }

    private void assertResponse(Matcher<Response> responseMatcher) {
        verify(responseContext).setResponse(responseCaptor.capture());
        assertThat(responseCaptor.getValue(), responseMatcher);
    }

    private static class ServiceExceptionWithoutMapping extends MockServiceException {
        ServiceExceptionWithoutMapping(@Nullable String message) {
            super(message);
        }
    }

    private static final class DetailedErrorServiceException extends MockServiceException implements DetailedError {
        private final DetailedError detailedError;

        private DetailedErrorServiceException(DetailedError detailedError, String message) {
            super(message);
            this.detailedError = detailedError;
        }

        @Nullable
        @Override
        public String getContext() {
            return detailedError.getContext();
        }

        @Nonnull
        @Override
        public String getSummary() {
            return detailedError.getSummary();
        }

        @Nullable
        @Override
        public String getDetails() {
            return detailedError.getDetails();
        }
    }

    private static final class DetailedErrorsServiceException extends MockServiceException implements DetailedErrors {
        private final DetailedErrors detailedErrors;

        private DetailedErrorsServiceException(DetailedErrors detailedErrors, String message) {
            super(message);
            this.detailedErrors = detailedErrors;
        }

        @Nonnull
        @Override
        public Iterable<DetailedError> getErrors() {
            return detailedErrors.getErrors();
        }

        @Override
        public boolean hasErrors() {
            return detailedErrors.hasErrors();
        }
    }
}
