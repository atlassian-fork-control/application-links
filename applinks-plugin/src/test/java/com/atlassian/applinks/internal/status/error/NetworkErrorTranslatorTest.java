package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import com.atlassian.applinks.internal.common.net.ResponseContentException;
import com.atlassian.applinks.internal.status.remote.ResponseApplinkStatusException;
import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseStatusException;
import org.apache.http.conn.HttpHostConnectException;
import org.junit.Test;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import java.net.ConnectException;
import java.net.UnknownHostException;

import static com.atlassian.applinks.internal.status.error.NetworkErrorTranslator.toApplinkError;
import static com.atlassian.applinks.internal.status.error.NetworkErrorTranslator.toApplinkErrorException;
import static com.atlassian.applinks.test.matcher.ApplinksMatchers.instanceOfMatching;
import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static com.atlassian.applinks.test.matchers.status.ApplinkErrorMatchers.responseErrorWith;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
public class NetworkErrorTranslatorTest {
    public static final String MESSAGE = "Message";

    @SuppressWarnings("ConstantConditions")
    @Test
    public void toApplinkErrorExceptionNullErrorNotAccepted() throws Exception {
        try {
            toApplinkErrorException(null, MESSAGE);
            fail();
        }
        catch (NullPointerException | IllegalArgumentException ignored) {}

    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void toApplinkErrorNullErrorNotAccepted() throws Exception {
        try {
            toApplinkError(null, MESSAGE);
            fail();
        }
        catch (NullPointerException | IllegalArgumentException ignored) {}
    }


    @Test
    public void toApplinkErrorExceptionReturnsUnknownErrorIfThrowableHasNoCause() throws Exception {
        assertEquals(ApplinkErrorType.UNKNOWN, toApplinkErrorException(new ResponseException(), MESSAGE).getType());
    }

    @Test
    public void toApplinkErrorReturnsUnknownErrorIfThrowableHasNoCause() throws Exception {
        assertEquals(ApplinkErrorType.UNKNOWN, toApplinkError(new ResponseException(), MESSAGE).getType());
    }

    @Test
    public void toApplinkErrorExceptionReturnsUnknownErrorIfCauseDoesNotMatchAnyRemoteStatusException() throws Exception {
        ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(new RuntimeException()), MESSAGE);

        assertEquals(ApplinkErrorType.UNKNOWN, remoteStatusException.getType());
    }

    @Test
    public void toApplinkErrorReturnsUnknownErrorIfCauseDoesNotMatchAnyRemoteStatusException() throws Exception {
        ApplinkError applinkError = toApplinkError(new ResponseException(new RuntimeException()), MESSAGE);

        assertEquals(ApplinkErrorType.UNKNOWN, applinkError.getType());
    }

    @Test
    public void toApplinkErrorReturnsApplinkErrorIfPresentInTheCausalChain() throws Exception {
        ApplinkError applinkError = toApplinkError(new ResponseException(
                new SimpleApplinkStatusException(ApplinkErrorType.GENERIC_LINK, "Something",
                        new ConnectException("Connection refused"))), MESSAGE);

        assertEquals(ApplinkErrorType.GENERIC_LINK, applinkError.getType());
        assertEquals("Something", applinkError.getDetails());
    }

    @Test
    public void toApplinkErrorReturnsApplinkResponseErrorIfPresentInTheCausalChain() throws Exception {
        Response response = new MockApplicationLinkResponse().setStatus(Status.BAD_REQUEST);

        ApplinkError applinkError = toApplinkError(new ResponseException(
                new ResponseApplinkStatusException(new UnexpectedResponseStatusError(response),
                        new ConnectException("Connection refused"))), MESSAGE);

        assertEquals(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS, applinkError.getType());
        assertEquals("400: Bad Request", applinkError.getDetails());
        assertThat(applinkError, instanceOf(ResponseApplinkError.class));
    }

    @Test
    public void toApplinkErrorExceptionShouldMatchUnknownHostException() throws Exception {
        ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(new UnknownHostException()), MESSAGE);

        assertEquals(ApplinkErrorType.UNKNOWN_HOST, remoteStatusException.getType());
        assertThat(remoteStatusException.getDetails(), startsWith(UnknownHostException.class.getName()));
    }

    @Test
    public void toApplinkErrorShouldMatchUnknownHostException() throws Exception {
        ApplinkError applinkError = toApplinkError(new ResponseException(new UnknownHostException()), MESSAGE);

        assertEquals(ApplinkErrorType.UNKNOWN_HOST, applinkError.getType());
        assertThat(applinkError.getDetails(), startsWith(UnknownHostException.class.getName()));
    }

    @Test
    public void toApplinkErrorExceptionShouldMatchConnectionRefused() throws Exception {
        ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(new ConnectException("Connection refused")), MESSAGE);

        assertEquals(ApplinkErrorType.CONNECTION_REFUSED, remoteStatusException.getType());
        assertThat(remoteStatusException.getDetails(), startsWith(ConnectException.class.getName()));
    }

    @Test
    public void toApplinkErrorShouldMatchDirectConnectionRefused() throws Exception {
        ApplinkError applinkError = toApplinkError(new ConnectException("Connection refused"), MESSAGE);

        assertEquals(ApplinkErrorType.CONNECTION_REFUSED, applinkError.getType());
        assertThat(applinkError.getDetails(), startsWith(ConnectException.class.getName()));
    }

    @Test
    public void toApplinkErrorShouldMatchConnectionRefused() throws Exception {
        ApplinkError applinkError = toApplinkError(
                new ResponseException(new ConnectException("Connection refused")), MESSAGE);

        assertEquals(ApplinkErrorType.CONNECTION_REFUSED, applinkError.getType());
        assertThat(applinkError.getDetails(), startsWith(ConnectException.class.getName()));
    }

    @Test
    public void toApplinkErrorExceptionShouldMatchNestedConnectionRefused() throws Exception {
        ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(
                        new HttpHostConnectException(
                                new ConnectException("Connection refused"), null, null)), MESSAGE);

        assertEquals(ApplinkErrorType.CONNECTION_REFUSED, remoteStatusException.getType());
        // HttpHostConnectException extends from ConnectException
        assertThat(remoteStatusException.getDetails(), startsWith(HttpHostConnectException.class.getName()));
    }

    @Test
    public void toApplinkErrorExceptionShouldMatchConnectionRefusedNestedWithinRuntimeException() throws Exception {
        ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new RuntimeException(
                        new RuntimeException(
                                new ConnectException("Connection refused"))), MESSAGE);

        assertEquals(ApplinkErrorType.CONNECTION_REFUSED, remoteStatusException.getType());
        assertThat(remoteStatusException.getDetails(), startsWith(ConnectException.class.getName()));
    }

    @Test
    public void toApplinkErrorExceptionShouldNotMatchConnectionRefusedWithoutMatchingMessage() throws Exception {
        ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new RuntimeException(
                        new RuntimeException(
                                new ConnectException("Connection timed out"))), MESSAGE);

        assertEquals(ApplinkErrorType.UNKNOWN, remoteStatusException.getType());
        assertThat(remoteStatusException.getDetails(), startsWith(ConnectException.class.getName()));
    }

    @Test
    public void toApplinkErrorExceptionShouldMatchResponseContentError() throws Exception {
        Response mockResponse = new MockApplicationLinkResponse()
                .setStatus(Status.OK)
                .setResponseBody("Some body")
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN);

        ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new RuntimeException(
                        new Exception(
                                new ResponseContentException(mockResponse))), MESSAGE);

        assertEquals(ApplinkErrorType.UNEXPECTED_RESPONSE, remoteStatusException.getType());
        assertThat(remoteStatusException.getDetails(), nullValue());
        assertThat(remoteStatusException, instanceOfMatching(ResponseApplinkError.class,
                responseErrorWith(Status.OK, "Some body", MediaType.TEXT_PLAIN)));
    }

    @Test
    public void toApplinkErrorExceptionShouldMatchResponseStatusError() throws Exception {
        Response mockResponse = new MockApplicationLinkResponse()
                .setStatus(Status.CONFLICT)
                .setResponseBody("Some body")
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN);

        ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new RuntimeException(
                        new Exception(
                                new ResponseStatusException("Conflict", mockResponse))), MESSAGE);

        assertEquals(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS, remoteStatusException.getType());
        assertThat(remoteStatusException.getDetails(),
                containsInOrder(Integer.toString(Status.CONFLICT.getStatusCode()), Status.CONFLICT.getReasonPhrase()));
        assertThat(remoteStatusException, instanceOfMatching(ResponseApplinkError.class,
                responseErrorWith(Status.CONFLICT, "Some body", MediaType.TEXT_PLAIN)));
    }

    @Test
    public void toApplinkErrorExceptionGivenResponseStatusExceptionWithNullResponse() throws Exception {
        ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new RuntimeException(
                        new Exception(
                                new ResponseStatusException("Boom!", null))), MESSAGE);

        assertEquals(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS, remoteStatusException.getType());
        assertEquals("<UNKNOWN STATUS>", remoteStatusException.getDetails());
        // should not create a ResponseApplinkError if there's no response
        assertThat(remoteStatusException, not(instanceOf(ResponseApplinkError.class)));
    }

    @Test
    public void toApplinkErrorExceptionShouldMatchSSLHandshakeException() throws Exception {
        final ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(new SSLHandshakeException("handshake error")), MESSAGE);

        assertEquals(ApplinkErrorType.SSL_UNTRUSTED, remoteStatusException.getType());
    }

    @Test
    public void toApplinkErrorExceptionShouldMatchSSLPeerUnverifiedException() throws Exception {
        final ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(new SSLPeerUnverifiedException("hostname unmatched")), MESSAGE);

        assertEquals(ApplinkErrorType.SSL_HOSTNAME_UNMATCHED, remoteStatusException.getType());
    }

    @Test
    public void toApplinkErrorExceptionShouldMatchCertificateError() throws Exception {
        final ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(new SSLException("certificate error")), MESSAGE);

        assertEquals(ApplinkErrorType.SSL_UNMATCHED, remoteStatusException.getType());
        assertThat(remoteStatusException.getDetails(), startsWith(SSLException.class.getName()));
    }

    @Test
    public void toApplinkErrorSslExceptionWithNullMessage() throws Exception {
        final ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(new SSLException((String) null)), MESSAGE);

        assertEquals(ApplinkErrorType.UNKNOWN, remoteStatusException.getType());
        assertThat(remoteStatusException.getDetails(), startsWith(ResponseException.class.getName()));
    }

    @Test
    public void toApplinkErrorSslExceptionWithNonMatchingMessage() throws Exception {
        final ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(new SSLException("Something different")), MESSAGE);

        assertEquals(ApplinkErrorType.UNKNOWN, remoteStatusException.getType());
        assertThat(remoteStatusException.getDetails(), startsWith(ResponseException.class.getName()));
    }

    @Test
    public void toApplinkErrorGenericOAuthProblem() throws Exception {
        final ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(
                        new OAuthMessageProblemException("Something different", "some_problem", null, null)), MESSAGE);

        assertEquals(ApplinkErrorType.OAUTH_PROBLEM, remoteStatusException.getType());
        assertEquals(remoteStatusException.getDetails(), "some_problem");
    }

    @Test
    public void toApplinkErrorOAuthTimestampRefused() throws Exception {
        final ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(
                        new OAuthMessageProblemException("Timestamp problem", ApplinksOAuth.PROBLEM_TIMESTAMP_REFUSED, null, null)), MESSAGE);

        assertEquals(ApplinkErrorType.OAUTH_TIMESTAMP_REFUSED, remoteStatusException.getType());
        assertEquals(remoteStatusException.getDetails(), ApplinksOAuth.PROBLEM_TIMESTAMP_REFUSED);
    }

    @Test
    public void toApplinkErrorOAuthSignatureInvalid() throws Exception {
        final ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(
                        new OAuthMessageProblemException("Signature invalid problem", ApplinksOAuth.PROBLEM_SIGNATURE_INVALID, null, null)), MESSAGE);

        assertEquals(ApplinkErrorType.OAUTH_SIGNATURE_INVALID, remoteStatusException.getType());
        assertEquals(remoteStatusException.getDetails(), ApplinksOAuth.PROBLEM_SIGNATURE_INVALID);
    }

}
