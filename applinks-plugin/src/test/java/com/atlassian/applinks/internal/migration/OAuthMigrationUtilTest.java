package com.atlassian.applinks.internal.migration;

import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import org.junit.Test;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDisabledConfig;
import static com.atlassian.applinks.internal.migration.OAuthMigrationUtil.isOAuthConfigured;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class OAuthMigrationUtilTest {

    @Test
    public void shouldBeConfiguredIfBothIncomingAndOutgoingOAuthExist() throws Exception {
        assertTrue(isOAuthConfigured(givenAuthenticationStatus(ApplinkOAuthStatus.DEFAULT, true, true)));
        assertTrue(isOAuthConfigured(givenAuthenticationStatus(ApplinkOAuthStatus.DEFAULT, true, false)));
        assertTrue(isOAuthConfigured(givenAuthenticationStatus(ApplinkOAuthStatus.DEFAULT, false, true)));
        assertTrue(isOAuthConfigured(givenAuthenticationStatus(ApplinkOAuthStatus.DEFAULT, false, false)));
    }

    @Test
    public void shouldBeConfiguredIfOAuthIsOneOneSideAndNoAuthOnOtherSide() throws Exception {
        // single connection on incoming
        assertTrue(isOAuthConfigured(givenAuthenticationStatus(new ApplinkOAuthStatus(createDefaultOAuthConfig(), createDisabledConfig()), false, false)));
        assertTrue(isOAuthConfigured(givenAuthenticationStatus(new ApplinkOAuthStatus(createDefaultOAuthConfig(), createDisabledConfig()), true, false)));

        //single connection on outgoing
        assertTrue(isOAuthConfigured(givenAuthenticationStatus(new ApplinkOAuthStatus(createDisabledConfig(), createDefaultOAuthConfig()), false, false)));
        assertTrue(isOAuthConfigured(givenAuthenticationStatus(new ApplinkOAuthStatus(createDisabledConfig(), createDefaultOAuthConfig()), false, true)));
    }

    @Test
    public void shouldBeNotConfiguredIfOAuthIsOneOneSideAndLegacyOnOtherSide() throws Exception {
        // oauth incoming, legacy outgoing
        assertFalse(isOAuthConfigured(givenAuthenticationStatus(new ApplinkOAuthStatus(createDefaultOAuthConfig(), createDisabledConfig()), false, true)));

        // oauth outgoing, legacy incoming
        assertFalse(isOAuthConfigured(givenAuthenticationStatus(new ApplinkOAuthStatus(createDisabledConfig(), createDefaultOAuthConfig()), true, false)));
    }

    @Test
    public void shouldBeNotConfiguredIfNoOAuthAndLegacyOnEitherSide() throws Exception {
        assertFalse(isOAuthConfigured(givenAuthenticationStatus(new ApplinkOAuthStatus(createDisabledConfig(), createDisabledConfig()), false, false)));
    }

    private AuthenticationStatus givenAuthenticationStatus(final ApplinkOAuthStatus localStatus, boolean incomingLegacy, boolean outgoingLegacy) {
        return new AuthenticationStatus(new AuthenticationConfig().oauth(localStatus.getIncoming()).basicConfigured(incomingLegacy),
                new AuthenticationConfig().oauth(localStatus.getOutgoing()).basicConfigured(outgoingLegacy));
    }

}