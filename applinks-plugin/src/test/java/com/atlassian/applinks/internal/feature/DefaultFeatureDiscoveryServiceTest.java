package com.atlassian.applinks.internal.feature;

import com.atlassian.applinks.internal.common.exception.InvalidFeatureKeyException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.test.mock.SimpleServiceExceptionFactory;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.test.mock.PermissionValidationMocks;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.atlassian.sal.core.usersettings.DefaultUserSettings;
import com.google.common.base.Function;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultFeatureDiscoveryServiceTest {
    private static final UserKey TEST_USER_KEY = new UserKey("test");

    @Mock
    private PermissionValidationService permissionValidationService;
    @Spy // make it work with @InjectMocks
    private SimpleServiceExceptionFactory serviceExceptionFactory = new SimpleServiceExceptionFactory();
    @Mock
    private UserManager userManager;
    @Mock
    private UserSettingsService userSettingsService;

    @InjectMocks
    private DefaultFeatureDiscoveryService featureDiscoveryService;
    @InjectMocks
    private PermissionValidationMocks permissionValidationMocks;

    private UserSettings updatedSettings;

    @SuppressWarnings("unchecked")
    @Before
    public void setUpUserSettingsUpdate() {
        doAnswer(invocation -> {
            Function<UserSettingsBuilder, UserSettings> updateFunction =
                    (Function<UserSettingsBuilder, UserSettings>) invocation.getArguments()[1];
            updatedSettings = updateFunction.apply(DefaultUserSettings.builder());
            return null;
        }).when(userSettingsService).updateUserSettings(eq(TEST_USER_KEY), any(Function.class));
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void discoverFeatureGivenNullFeatureKey() throws ServiceException {
        featureDiscoveryService.discover(null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void isDiscoveredFeatureGivenNullFeatureKey() throws ServiceException {
        featureDiscoveryService.isDiscovered(null);
    }

    @Test(expected = NotAuthenticatedException.class)
    public void discoverRequiresAuthenticatedUser() throws ServiceException {
        permissionValidationMocks.failedValidateAuthenticated();

        featureDiscoveryService.discover("featurekey");
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void discoverGivenBlankKey() throws ServiceException {
        setUpAuthenticatedUser();

        featureDiscoveryService.discover("  ");
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void discoverGivenTooLongKey() throws ServiceException {
        setUpAuthenticatedUser();

        // key >100 chars
        featureDiscoveryService.discover(StringUtils.repeat("featurekey", 11));
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void discoverGivenKeyWithSlash() throws ServiceException {
        setUpAuthenticatedUser();

        featureDiscoveryService.discover("test/key");
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void discoverGivenKeyWithBraces() throws ServiceException {
        setUpAuthenticatedUser();

        featureDiscoveryService.discover("test[key]");
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void isDiscoveredGivenBlankKey() throws ServiceException {
        setUpAuthenticatedUser();

        featureDiscoveryService.isDiscovered("  ");
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void isDiscoveredGivenTooLongKey() throws ServiceException {
        setUpAuthenticatedUser();

        // key >100 chars
        featureDiscoveryService.isDiscovered(StringUtils.repeat("featurekey", 11));
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void isDiscoveredGivenKeyWithSlash() throws ServiceException {
        setUpAuthenticatedUser();

        featureDiscoveryService.isDiscovered("test/key");
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void isDiscoveredGivenKeyWithBraces() throws ServiceException {
        setUpAuthenticatedUser();

        featureDiscoveryService.isDiscovered("test[key]");
    }

    @Test
    public void discoverGivenValidFeatureKey() throws ServiceException {
        setUpAuthenticatedUser();

        featureDiscoveryService.discover("test.key-that_is.valid");

        assertThat(updatedSettings.getKeys(), contains("applinks.featurediscovery.test.key-that_is.valid"));
    }

    @Test
    public void discoverStoresLowercase() throws ServiceException {
        setUpAuthenticatedUser();

        featureDiscoveryService.discover("TEST.KEY");

        assertTrue(updatedSettings.getBoolean("applinks.featurediscovery.test.key").getOrElse(false));
    }

    @Test
    public void isDiscoveredTrueGivenValidFeatureKey() throws ServiceException {
        setUpAuthenticatedUser();
        setUpStoredSettings(DefaultUserSettings.builder()
                .put("applinks.featurediscovery.test.key-that_is.valid", true)
                .put("applinks.featurediscovery.some.other.key", true)
                .put("not.applinks.related", false)
                .build());

        assertTrue(featureDiscoveryService.isDiscovered("test.key-that_is.valid"));
    }

    @Test
    public void isDiscoveredTrueEvenIfValueStoredAsFalse() throws ServiceException {
        setUpAuthenticatedUser();
        setUpStoredSettings(DefaultUserSettings.builder()
                .put("applinks.featurediscovery.test.key", false)
                .build());

        assertTrue(featureDiscoveryService.isDiscovered("test.key"));
    }

    @Test
    public void isDiscoveredFalseGivenValidFeatureKey() throws ServiceException {
        setUpAuthenticatedUser();
        setUpStoredSettings(DefaultUserSettings.builder()
                .put("applinks.featurediscovery.test-key", true)
                .put("applinks.featurediscovery.some.other.key", true)
                .put("not.applinks.related", true)
                .build());

        assertFalse(featureDiscoveryService.isDiscovered("test.key"));
    }

    @Test
    public void isDiscoveredCaseInsensitive() throws ServiceException {
        setUpAuthenticatedUser();
        setUpStoredSettings(DefaultUserSettings.builder()
                .put("applinks.featurediscovery.test.key", true)
                .build());

        assertTrue(featureDiscoveryService.isDiscovered("TEST.KEY"));
    }

    @Test
    public void getAllDiscoveredKeys() throws ServiceException {
        setUpAuthenticatedUser();
        setUpStoredSettings(DefaultUserSettings.builder()
                .put("applinks.featurediscovery.test-key", true)
                .put("applinks.featurediscovery.test.key", true)
                .put("applinks.featurediscovery.test_key", false) // won't happen but still test for it
                .put("some.other.key", true)
                .put("another.non.applinks", false)
                .build());

        assertThat(featureDiscoveryService.getAllDiscoveredFeatureKeys(), containsInAnyOrder(
                "test-key", "test.key", "test_key"
        ));
    }


    private void setUpAuthenticatedUser() throws NoAccessException {
        when(userManager.getRemoteUserKey()).thenReturn(TEST_USER_KEY);
        permissionValidationMocks.failedValidateAdmin();
        permissionValidationMocks.failedValidateSysadmin();
    }

    private void setUpStoredSettings(UserSettings storedSettings) {
        when(userSettingsService.getUserSettings(TEST_USER_KEY)).thenReturn(storedSettings);
    }

}
