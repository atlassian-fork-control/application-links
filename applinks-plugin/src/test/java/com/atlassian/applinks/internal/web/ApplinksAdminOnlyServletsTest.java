package com.atlassian.applinks.internal.web;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.applinks.ui.auth.AuthenticatorContainerServlet;
import com.atlassian.applinks.ui.velocity.ListApplicationLinksContext;
import com.atlassian.applinks.ui.velocity.VelocityContextFactory;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Theories.class)
public class ApplinksAdminOnlyServletsTest {
    @DataPoints
    public static AdminOnlyTestServlet[] testAdminOnlyServlets = AdminOnlyTestServlet.values();

    private static I18nResolver i18nResolver;
    private static MessageFactory messageFactory;
    private static TemplateRenderer templateRenderer;
    private static WebResourceManager webResourceManager;
    private static ApplicationLinkService applicationLinkService;
    private static ManifestRetriever manifestRetriever;
    private static PluginAccessor pluginAccessor;
    private static AdminUIAuthenticator adminUIAuthenticator;
    private static InternalHostApplication internalHostApplication;
    private static DocumentationLinker documentationLinker;
    private static LoginUriProvider loginUriProvider;
    private static WebSudoManager webSudoManager;
    private static VelocityContextFactory velocityContextFactory;
    private static XsrfTokenAccessor xsrfTokenAccessor;
    private static XsrfTokenValidator xsrfTokenValidator;
    private static UserManager userManager;
    private static ApplinksFeatureService applinksFeatureService;
    private static EventPublisher eventPublisher;

    private HttpServletRequest request;
    private HttpServletResponse response;

    ListApplicationLinksContext listApplicationLinksContext;

    @Before
    public void setUp() {
        i18nResolver = mock(I18nResolver.class);
        messageFactory = mock(MessageFactory.class);
        templateRenderer = mock(TemplateRenderer.class);
        webResourceManager = mock(WebResourceManager.class);
        applicationLinkService = mock(ApplicationLinkService.class);
        manifestRetriever = mock(ManifestRetriever.class);
        pluginAccessor = mock(PluginAccessor.class);
        adminUIAuthenticator = mock(AdminUIAuthenticator.class);
        internalHostApplication = mock(InternalHostApplication.class);
        documentationLinker = mock(DocumentationLinker.class);
        loginUriProvider = mock(LoginUriProvider.class);
        webSudoManager = mock(WebSudoManager.class);
        velocityContextFactory = mock(VelocityContextFactory.class);
        xsrfTokenAccessor = mock(XsrfTokenAccessor.class);
        xsrfTokenValidator = mock(XsrfTokenValidator.class);
        userManager = mock(UserManager.class);
        applinksFeatureService = mock(ApplinksFeatureService.class);
        eventPublisher = mock(EventPublisher.class);

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        listApplicationLinksContext = mock(ListApplicationLinksContext.class);

        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(request.getServletPath()).thenReturn("servlet-path");
        when(xsrfTokenValidator.validateFormEncodedToken(request)).thenReturn(true);
        when(userManager.getRemoteUsername()).thenReturn("admin");
        when(userManager.isSystemAdmin(any(String.class))).thenReturn(true);
        when(velocityContextFactory.buildListApplicationLinksContext(request)).thenReturn(listApplicationLinksContext);
    }

    @Theory
    public void verifyThatAdminAccessIsCheckedForAdminOnlyServlet(AdminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsAdmin();
        servlet.getServlet().service(request, response);

        verify(adminUIAuthenticator).checkAdminUIAccessBySessionOrCurrentUser(request);
    }

    @Theory
    public void verifyThatWebSudoRequestIsExecutedForAdminOnlyServlet(AdminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsAdmin();
        servlet.getServlet().service(request, response);

        verify(webSudoManager).willExecuteWebSudoRequest(request);
    }

    @Theory
    public void verifyWebSudoGivenControlWhenRequestRequiresSudoAuth(AdminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsAdmin();
        doThrow(new WebSudoSessionException("blah")).when(webSudoManager).willExecuteWebSudoRequest(request);
        servlet.getServlet().service(request, response);

        verify(webSudoManager).enforceWebSudoProtection(request, response);
    }

    enum AdminOnlyTestServlet {
        LIST_APPLICATION_LINKS_SERVLET_GET {
            @Override
            HttpServlet getServlet() {
                return new ListApplicationLinksServlet(i18nResolver, messageFactory, templateRenderer,
                        webResourceManager, adminUIAuthenticator, internalHostApplication,
                        documentationLinker, loginUriProvider, velocityContextFactory, webSudoManager,
                        xsrfTokenAccessor, xsrfTokenValidator, applinksFeatureService, eventPublisher);
            }

            @Override
            String getMethod() {
                return "GET";
            }
        },

        AUTHENTICATOR_CONTAINER_SERVLET_GET {
            @Override
            HttpServlet getServlet() {
                return new AuthenticatorContainerServlet(i18nResolver, messageFactory, templateRenderer, webResourceManager,
                        applicationLinkService, internalHostApplication, manifestRetriever, pluginAccessor,
                        adminUIAuthenticator, loginUriProvider, documentationLinker,
                        webSudoManager, xsrfTokenAccessor, xsrfTokenValidator, userManager);
            }

            @Override
            String getMethod() {
                return "GET";
            }
        };

        abstract HttpServlet getServlet();

        abstract String getMethod();
    }

    private void whenUserIsLoggedInAsAdmin() {
        when(adminUIAuthenticator.checkAdminUIAccessBySessionOrCurrentUser(request)).thenReturn(true);
    }
}