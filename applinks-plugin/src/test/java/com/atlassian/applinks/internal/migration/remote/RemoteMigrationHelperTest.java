package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.AuthorisationURIGenerator;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthAutoConfigurator;
import com.atlassian.applinks.internal.common.exception.RemoteMigrationInvalidResponseException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.migration.AuthenticationConfig;
import com.atlassian.applinks.internal.migration.AuthenticationStatus;
import com.atlassian.applinks.internal.status.DefaultLegacyConfig;
import com.atlassian.applinks.internal.status.LegacyConfig;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.sal.api.net.ResponseException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RemoteMigrationHelperTest {
    private static final ApplicationId APPLINK_ID = new ApplicationId(UUID.randomUUID().toString());
    private static final Class<TrustedAppsAuthenticationProvider> TRUSTED_PROVIDER_CLASS = TrustedAppsAuthenticationProvider.class;

    @Mock
    private ApplicationLink link;
    @Mock
    private OAuthAutoConfigurator authAutoConfigurator;
    @Mock
    private ServiceExceptionFactory serviceExceptionFactory;
    @Mock
    private InternalHostApplication internalHostApplication;
    @Mock
    private TryWithAuthentication disableTrusted;
    @Mock
    private TryWithAuthentication querySysAdminAccess;
    @Mock
    private QueryLegacyAuthentication queryLegacyAuthentication;
    @Mock
    private QueryLegacyAuthentication.Factory queryLegacyAuthenticationFactory;
    @Mock
    private OAuthConfigMigrator oAuthConfigMigrator;
    @Mock
    private OAuthConfigMigrator.Factory oauthConfiguratorFactory;
    @Captor
    private ArgumentCaptor<OAuthConfig> incomingCaptor;
    @Captor
    private ArgumentCaptor<OAuthConfig> outgoingCaptor;

    @InjectMocks
    private RemoteMigrationHelper remoteMigrationHelper;

    private static Field DISABLE_TRUSTED_FIELD;
    private static Field QUERY_SYS_ADMIN_ACCESS_FIELD;
    private static Field QUERY_LEGACY_AUTHENTICATION_FACTORY_FIELD;
    private static Field OAUTH_CONFIGURATOR_FACTORY_FIELD;

    private static Field removeFinal(Field field) throws NoSuchFieldException, IllegalAccessException {
        field.setAccessible(true);
        final Field modifierField = Field.class.getDeclaredField("modifiers");
        modifierField.setAccessible(true);
        modifierField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        return field;
    }

    @BeforeClass
    public static void beforeClass() throws IllegalAccessException, NoSuchFieldException {
        DISABLE_TRUSTED_FIELD = removeFinal(RemoteMigrationHelper.class.getDeclaredField("DISABLE_TRUSTED"));
        QUERY_SYS_ADMIN_ACCESS_FIELD = removeFinal(RemoteMigrationHelper.class.getDeclaredField("QUERY_SYS_ADMIN_ACCESS"));
        QUERY_LEGACY_AUTHENTICATION_FACTORY_FIELD = removeFinal(RemoteMigrationHelper.class.getDeclaredField("QUERY_LEGACY_AUTHENTICATION_FACTORY"));
        QUERY_LEGACY_AUTHENTICATION_FACTORY_FIELD = removeFinal(RemoteMigrationHelper.class.getDeclaredField("QUERY_LEGACY_AUTHENTICATION_FACTORY"));
        OAUTH_CONFIGURATOR_FACTORY_FIELD = removeFinal(RemoteMigrationHelper.class.getDeclaredField("OAUTH_CONFIGURATOR_FACTORY"));
    }

    @Before
    public void setUp() throws Exception {
        DISABLE_TRUSTED_FIELD.set(remoteMigrationHelper, disableTrusted);
        QUERY_SYS_ADMIN_ACCESS_FIELD.set(remoteMigrationHelper, querySysAdminAccess);
        QUERY_LEGACY_AUTHENTICATION_FACTORY_FIELD.set(remoteMigrationHelper, queryLegacyAuthenticationFactory);
        OAUTH_CONFIGURATOR_FACTORY_FIELD.set(remoteMigrationHelper, oauthConfiguratorFactory);

        when(internalHostApplication.getId()).thenReturn(APPLINK_ID);
        when(queryLegacyAuthenticationFactory.getInstance()).thenReturn(queryLegacyAuthentication);
        when(oauthConfiguratorFactory.getInstance(any(OAuthAutoConfigurator.class), any(OAuthConfig.class), any(OAuthConfig.class))).thenReturn(oAuthConfigMigrator);
    }

    @Test
    public void disableRemoteTrustedApp() throws Exception {
        when(disableTrusted.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenReturn(false);
        when(disableTrusted.execute(link, APPLINK_ID, TRUSTED_PROVIDER_CLASS)).thenReturn(true);
        assertTrue(remoteMigrationHelper.disableRemoteTrustedApp(link));
        verify(disableTrusted, never()).execute(link, APPLINK_ID, TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);
        verify(disableTrusted, times(1)).execute(link, APPLINK_ID, TRUSTED_PROVIDER_CLASS);
    }

    @Test
    public void hasSysAdminAccess() throws Exception {
        when(disableTrusted.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenReturn(false);
        when(querySysAdminAccess.execute(link, APPLINK_ID, TRUSTED_PROVIDER_CLASS)).thenReturn(true);
        assertTrue(remoteMigrationHelper.hasSysAdminAccess(link));
        verify(disableTrusted, never()).execute(link, APPLINK_ID, TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);
        verify(querySysAdminAccess, times(1)).execute(link, APPLINK_ID, TRUSTED_PROVIDER_CLASS);
    }

    @Test
    public void getLegacyConfigSuccessful() throws Exception {
        when(queryLegacyAuthentication.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenReturn(true);
        when(queryLegacyAuthentication.getLegacyConfig()).thenReturn(new DefaultLegacyConfig().basic(true).trusted(true));

        final LegacyConfig legacy = remoteMigrationHelper.getLegacyConfig(link);

        assertTrue(legacy.isBasicConfigured());
        assertTrue(legacy.isTrustedConfigured());
    }

    @Test
    public void getLegacyConfigUnsuccessful() throws Exception {
        when(queryLegacyAuthentication.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenReturn(false);

        final LegacyConfig legacy = remoteMigrationHelper.getLegacyConfig(link);

        assertFalse(legacy.isBasicConfigured());
        assertFalse(legacy.isTrustedConfigured());
    }

    @Test
    public void migrateMapBasicTo2LO() throws Exception {
        AuthenticationStatus configs = new AuthenticationStatus(
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), true, false),
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), true, false)
        );

        when(oAuthConfigMigrator.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenReturn(false);
        remoteMigrationHelper.migrate(link, configs);
        verify(oauthConfiguratorFactory).getInstance(eq(authAutoConfigurator), incomingCaptor.capture(), outgoingCaptor.capture());
        assertThat(incomingCaptor.getValue(), equalTo(OAuthConfig.createDefaultOAuthConfig()));
        assertThat(outgoingCaptor.getValue(), equalTo(OAuthConfig.createDefaultOAuthConfig()));
    }

    @Test
    public void migrateMapTrustedTo2LOi() throws Exception {
        AuthenticationStatus configs = new AuthenticationStatus(
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), false, true),
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), false, true)
        );

        when(oAuthConfigMigrator.execute(link, APPLINK_ID, TRUSTED_PROVIDER_CLASS)).thenReturn(false);
        remoteMigrationHelper.migrate(link, configs);
        verify(oauthConfiguratorFactory).getInstance(eq(authAutoConfigurator), incomingCaptor.capture(), outgoingCaptor.capture());
        assertThat(incomingCaptor.getValue(), equalTo(OAuthConfig.createOAuthWithImpersonationConfig()));
        assertThat(outgoingCaptor.getValue(), equalTo(OAuthConfig.createOAuthWithImpersonationConfig()));
    }

    @Test
    public void migrateMapTrustedTo2LOiEvenWhenBasicConfigured() throws Exception {
        AuthenticationStatus configs = new AuthenticationStatus(
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), true, true),
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), true, true)
        );

        when(oAuthConfigMigrator.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenReturn(false);
        remoteMigrationHelper.migrate(link, configs);
        verify(oauthConfiguratorFactory).getInstance(eq(authAutoConfigurator), incomingCaptor.capture(), outgoingCaptor.capture());
        assertThat(incomingCaptor.getValue(), equalTo(OAuthConfig.createOAuthWithImpersonationConfig()));
        assertThat(outgoingCaptor.getValue(), equalTo(OAuthConfig.createOAuthWithImpersonationConfig()));
    }

    @Test
    public void migrateMaintainOAuthIfConfigured() throws Exception {
        AuthenticationStatus configs = new AuthenticationStatus(
                new AuthenticationConfig(OAuthConfig.createOAuthWithImpersonationConfig(), true, false),
                new AuthenticationConfig(OAuthConfig.createOAuthWithImpersonationConfig(), true, false)
        );

        when(oAuthConfigMigrator.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenReturn(false);
        remoteMigrationHelper.migrate(link, configs);
        verify(oauthConfiguratorFactory).getInstance(eq(authAutoConfigurator), incomingCaptor.capture(), outgoingCaptor.capture());
        assertThat(incomingCaptor.getValue(), equalTo(OAuthConfig.createOAuthWithImpersonationConfig()));
        assertThat(outgoingCaptor.getValue(), equalTo(OAuthConfig.createOAuthWithImpersonationConfig()));
    }

    @Test
    public void migrateDisabledConfigurations() throws Exception {
        AuthenticationStatus configs = new AuthenticationStatus(
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), false, false),
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), false, false)
        );

        remoteMigrationHelper.migrate(link, configs);
        verify(oauthConfiguratorFactory).getInstance(eq(authAutoConfigurator), incomingCaptor.capture(), outgoingCaptor.capture());
        assertThat(incomingCaptor.getValue(), equalTo(OAuthConfig.createDisabledConfig()));
        assertThat(outgoingCaptor.getValue(), equalTo(OAuthConfig.createDisabledConfig()));
    }

    @Test
    public void willNotMigrateIfOutgoingHasNoLegacyConfiguration() throws Exception {
        AuthenticationStatus configs = new AuthenticationStatus(
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), true, true),
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), false, false)
        );

        final AuthenticationStatus result = remoteMigrationHelper.migrate(link, configs);
        verify(oAuthConfigMigrator, never()).execute(any(ApplicationLink.class), any(ApplicationId.class), any(ApplicationLinkRequestFactory.class));
        assertThat(result.incoming().getOAuthConfig(), equalTo(OAuthConfig.createDisabledConfig()));
        assertThat(result.outgoing().getOAuthConfig(), equalTo(OAuthConfig.createDisabledConfig()));
    }

    @Test
    public void successfulMigrationShouldReturnModifiedConfigs() throws Exception {
        AuthenticationStatus configs = new AuthenticationStatus(
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), true, false),
                new AuthenticationConfig(OAuthConfig.createDisabledConfig(), true, false)
        );

        when(oAuthConfigMigrator.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenReturn(true);
        final AuthenticationStatus result = remoteMigrationHelper.migrate(link, configs);
        verify(oauthConfiguratorFactory).getInstance(eq(authAutoConfigurator), incomingCaptor.capture(), outgoingCaptor.capture());
        assertThat(incomingCaptor.getValue(), equalTo(result.incoming().getOAuthConfig()));
        assertThat(outgoingCaptor.getValue(), equalTo(result.outgoing().getOAuthConfig()));
    }

    @Test
    public void tryWithAuthenticationWithCredentialsRequiredException() throws Exception {
        final AuthorisationURIGenerator authorisationURIGenerator = mock(AuthorisationURIGenerator.class);
        final CredentialsRequiredException credentialException = new CredentialsRequiredException(authorisationURIGenerator, "");
        doThrow(credentialException).when(disableTrusted).execute(link, APPLINK_ID, BasicAuthenticationProvider.class);
        doThrow(credentialException).when(disableTrusted).execute(link, APPLINK_ID, TRUSTED_PROVIDER_CLASS);
        assertFalse(remoteMigrationHelper.tryWithAuthentications(link, disableTrusted));
    }

    @Test
    public void tryWithAuthenticationForAllProviders() throws Exception {
        when(disableTrusted.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenReturn(false);
        when(disableTrusted.execute(link, APPLINK_ID, TRUSTED_PROVIDER_CLASS)).thenReturn(false);
        when(disableTrusted.execute(link, APPLINK_ID, TwoLeggedOAuthWithImpersonationAuthenticationProvider.class)).thenReturn(false);
        when(disableTrusted.execute(link, APPLINK_ID, TwoLeggedOAuthAuthenticationProvider.class)).thenReturn(false);
        when(disableTrusted.execute(link, APPLINK_ID, OAuthAuthenticationProvider.class)).thenReturn(true);
        assertTrue(remoteMigrationHelper.tryWithAuthentications(link, disableTrusted));

        verify(disableTrusted).execute(link, APPLINK_ID, BasicAuthenticationProvider.class);
        verify(disableTrusted).execute(link, APPLINK_ID, TRUSTED_PROVIDER_CLASS);
        verify(disableTrusted).execute(link, APPLINK_ID, TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);
        verify(disableTrusted).execute(link, APPLINK_ID, TwoLeggedOAuthAuthenticationProvider.class);
        verify(disableTrusted).execute(link, APPLINK_ID, OAuthAuthenticationProvider.class);
    }

    @Test(expected = RemoteMigrationInvalidResponseException.class)
    public void tryWithAuthenticationShouldWrapResponseException() throws Exception {
        when(disableTrusted.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenThrow(new ResponseException(""));
        when(serviceExceptionFactory.create(eq(RemoteMigrationInvalidResponseException.class), any(Exception.class))).thenReturn(new RemoteMigrationInvalidResponseException(""));
        remoteMigrationHelper.tryWithAuthentications(link, disableTrusted);
    }

    @Test(expected = RemoteMigrationInvalidResponseException.class)
    public void tryWithAuthenticationShouldWrapAuthenticationConfigurationException() throws Exception {
        when(disableTrusted.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenThrow(new AuthenticationConfigurationException(""));
        when(serviceExceptionFactory.create(eq(RemoteMigrationInvalidResponseException.class), any(Exception.class))).thenReturn(new RemoteMigrationInvalidResponseException(""));
        remoteMigrationHelper.tryWithAuthentications(link, disableTrusted);
    }

    @Test(expected = RemoteMigrationInvalidResponseException.class)
    public void tryWithAuthenticationShouldWrapIOException() throws Exception {
        when(disableTrusted.execute(link, APPLINK_ID, BasicAuthenticationProvider.class)).thenThrow(new IOException(""));
        when(serviceExceptionFactory.create(eq(RemoteMigrationInvalidResponseException.class), any(Exception.class))).thenReturn(new RemoteMigrationInvalidResponseException(""));
        remoteMigrationHelper.tryWithAuthentications(link, disableTrusted);
    }
}