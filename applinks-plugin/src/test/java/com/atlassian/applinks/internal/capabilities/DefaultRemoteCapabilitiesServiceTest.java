package com.atlassian.applinks.internal.capabilities;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.applinks.api.event.ApplicationLinkDeletedEvent;
import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.core.manifest.AppLinksManifestDownloader;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.cache.ApplinksRequestCache;
import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.common.event.ManifestDownloadFailedEvent;
import com.atlassian.applinks.internal.common.event.ManifestDownloadedEvent;
import com.atlassian.applinks.internal.common.exception.InvalidValueException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.lang.ApplinksEnums;
import com.atlassian.applinks.internal.common.test.mock.SimpleServiceExceptionFactory;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.SimpleApplinkError;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.test.mock.ApplinkHelperMocks;
import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.mock.MockManifest;
import com.atlassian.applinks.test.mock.MockRequestAnswer;
import com.atlassian.applinks.test.mock.PermissionValidationMocks;
import com.atlassian.applinks.test.mock.TestApplinkIds;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import com.atlassian.cache.CacheFactory;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response.Status;
import java.net.ConnectException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities.STATUS_API;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withMethodType;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withUrlThat;
import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static com.atlassian.applinks.test.matchers.capabilities.ApplicationVersionMatchers.hasVersionNumber;
import static com.atlassian.applinks.test.matchers.status.ApplinkErrorMatchers.withDetailsThat;
import static com.atlassian.applinks.test.matchers.status.ApplinkErrorMatchers.withType;
import static com.atlassian.applinks.test.mock.TestApplinkIds.DEFAULT_ID;
import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultRemoteCapabilitiesServiceTest {
    private static final String TEST_URL = "http://test.com";
    private static final URI TEST_URI = URI.create(TEST_URL);

    @Mock
    private ApplicationLinkService applicationLinkService;
    @Mock
    private ApplinkHelper applinkHelper;
    @Mock
    private ApplinksRequestCache applinksRequestCache;
    @Mock
    private EventPublisher eventPublisher;
    @Spy
    private AppLinksManifestDownloader manifestDownloader = new TestManifestDownloader();
    @Mock
    private PermissionValidationService permissionValidationService;
    @Spy
    private ServiceExceptionFactory serviceExceptionFactory = new SimpleServiceExceptionFactory();
    @Spy
    private CacheFactory cacheFactory = new MemoryCacheManager();

    @MockApplink(id = TestApplinkIds.ID1, rpcUrl = TEST_URL)
    private ApplicationLink mockApplink;
    @MockApplink(id = TestApplinkIds.ID2, rpcUrl = "http://applink2.com")
    private ApplicationLink mockApplink2;
    @MockApplink(id = TestApplinkIds.ID3, rpcUrl = "http://applink3.com")
    private ApplicationLink mockApplink3;
    @MockApplink(id = TestApplinkIds.ID4, rpcUrl = "http://applink4.com")
    private ApplicationLink mockApplink4;
    @Mock
    private ApplicationLinkRequestFactory applicationLinkRequestFactory;
    @Mock
    private ApplinksRequestCache.Cache<ApplicationId, DefaultRemoteCapabilitiesService.CachedCapabilities> capabilitiesRequestCache;

    @Captor
    private ArgumentCaptor<DefaultRemoteCapabilitiesService.CachedCapabilities> requestCapabilitiesCaptor;

    @InjectMocks
    private ApplinkHelperMocks applinkHelperMocks;
    @InjectMocks
    private PermissionValidationMocks permissionValidationMocks;
    @InjectMocks
    private DefaultRemoteCapabilitiesService remoteCapabilitiesService;

    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);

    private final MockRequestAnswer mockRequestAnswer = MockRequestAnswer.create();

    @Before
    public void setUpRequestCreation() throws Exception {
        when(mockApplink.createAuthenticatedRequestFactory(Anonymous.class)).thenReturn(applicationLinkRequestFactory);
        when(applicationLinkRequestFactory.createRequest(any(), any()))
                .thenAnswer(mockRequestAnswer);
    }

    @Before
    public void setUpCapabilitiesRequestCache() throws Exception {
        when(applinksRequestCache.getCache(DefaultRemoteCapabilitiesService.REQUEST_CACHE_KEY, ApplicationId.class,
                DefaultRemoteCapabilitiesService.CachedCapabilities.class)).thenReturn(capabilitiesRequestCache);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getCapabilitiesNonExistingApplinkId() throws Exception {
        applinkHelperMocks.setUpNonExistingApplink(DEFAULT_ID);

        remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());
    }

    @Test(expected = NoSuchApplinkException.class)
    public void getCapabilitiesWithMaxAgeNonExistingApplinkId() throws Exception {
        applinkHelperMocks.setUpNonExistingApplink(DEFAULT_ID);

        remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId(), 1L, TimeUnit.DAYS);
    }

    @Test(expected = InvalidValueException.class)
    public void getCapabilitiesInvalidMaxAge() throws Exception {
        setUpExistingApplink();

        remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId(), -1L, TimeUnit.DAYS);
    }

    @Test(expected = NotAuthenticatedException.class)
    public void getCapabilitiesNoUserContext() throws Exception {
        setUpExistingApplink();
        permissionValidationMocks.failedValidateAuthenticated();

        remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());
    }

    @Test(expected = NotAuthenticatedException.class)
    public void getCapabilitiesWithMaxAgeNoUserContext() throws Exception {
        setUpExistingApplink();
        permissionValidationMocks.failedValidateAuthenticated();

        remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId(), 1L, TimeUnit.DAYS);
    }

    @Test(expected = PermissionException.class)
    public void getCapabilitiesNonAdmin() throws Exception {
        setUpExistingApplink();
        permissionValidationMocks.failedValidateAdmin();

        remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());
    }

    @Test(expected = PermissionException.class)
    public void getCapabilitiesWithMaxAgeNonAdmin() throws Exception {
        setUpExistingApplink();
        permissionValidationMocks.failedValidateAdmin();

        remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId(), 1L, TimeUnit.DAYS);
    }

    @Test
    public void getCapabilitiesGivenGenericLink() throws Exception {
        setUpExistingApplink();
        setUpGenericApplinkType();

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertEmptyErrorCapabilities(capabilities, ApplinkErrorType.GENERIC_LINK);
        assertEmptyCache();
    }

    @Test
    public void getCapabilitiesGivenNonAtlassianLink() throws Exception {
        setUpExistingApplink();
        setUpNonAtlassianApplinkType();

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertEmptyErrorCapabilities(capabilities, ApplinkErrorType.NON_ATLASSIAN);
        assertEmptyCache();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenGenericLink() throws Exception {
        setUpExistingApplink();
        setUpGenericApplinkType();

        RemoteApplicationCapabilities capabilities =
                remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId(), 1L, TimeUnit.DAYS);

        assertEmptyErrorCapabilities(capabilities, ApplinkErrorType.GENERIC_LINK);
        assertEmptyCache();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenNonAtlassianLink() throws Exception {
        setUpExistingApplink();
        setUpNonAtlassianApplinkType();

        RemoteApplicationCapabilities capabilities =
                remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId(), 1L, TimeUnit.DAYS);

        assertEmptyErrorCapabilities(capabilities, ApplinkErrorType.NON_ATLASSIAN);
        assertEmptyCache();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestFoundNewUiVersionCapabilitiesOk()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version contains capabilities (5.0.4+)
        //  - capabilities result OK
        // Expected:
        //  - successful capabilities with no error
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.1")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createOkCapabilitiesResponse(EnumSet.of(STATUS_API)));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), contains(STATUS_API));
        assertNoError(capabilities);
        assertCachedValue(capabilities);
        assertOneRequestToCapabilities();
        permissionValidationMocks.verifyValidateAdmin();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestFoundNewUiVersionCapabilitiesOkButSomeUnknown()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version contains capabilities (5.0.4+)
        //  - capabilities result OK, containing some future capabilities unknown on this instance
        // Expected:
        //  - successful capabilities with no error
        //  - unknown remote capabilities are ignored (filtered out)
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("10.0.2")
                .setApplinksVersion("12.0.4"));
        mockRequestAnswer.addResponse(createOkCapabilitiesResponseForNames(ImmutableList.of(
                ApplinksCapabilities.STATUS_API.name(),
                "MAGIC",
                "UNICORNS",
                "RAINBOWS"
        )));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(10, 0, 2));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(12, 0, 4));
        assertThat(capabilities.getCapabilities(), contains(STATUS_API));
        assertNoError(capabilities);
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestFoundNewUiVersionCapabilitiesNotFound()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version should contain capabilities (5.0.4+)
        //  - capabilities result NOT_FOUND
        // Expected:
        //  - capabilities with proper versions, empty capabilities and no error
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.1")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createNotFoundCapabilitiesResponse("Resource not found"));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertNoError(capabilities);
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestFoundNewUiVersionCapabilitiesNetworkError()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version should contain capabilities (5.0.4+)
        //  - capabilities request fails with unknown host error
        // Expected:
        //  - error capabilities UNKNOWN_HOST with proper versions and empty capabilities
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.1")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createResponseWithNetworkError(new ResponseException(
                new UnknownHostException("Unknown host")
        )));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNKNOWN_HOST,
                containsInOrder(UnknownHostException.class.getSimpleName(), "Unknown host"));
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestFoundNewUiVersionInvalidCapabilitiesResponseContents()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version should contain capabilities (5.0.4+)
        //  - capabilities request returns OK but unexpected contents
        // Expected:
        //  - error capabilities UNEXPECTED_RESPONSE with proper versions and empty capabilities
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.1")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createOkCapabilitiesResponse("not a valid JSON"));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNEXPECTED_RESPONSE, isEmptyOrNullString());
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestFoundNewUiVersionInvalidCapabilitiesResponseStatus()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version should contain capabilities (5.0.4+)
        //  - capabilities request returns unexpected status
        // Expected:
        //  - error capabilities UNEXPECTED_RESPONSE_STATUS with proper versions and empty capabilities
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.1")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createCapabilitiesResponse(Status.CONFLICT, "wrong response"));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS, containsString("409"));
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestFoundPreNewUiVersion()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version does not contain capabilities (pre-5.0.4)
        // Expected:
        //  - successful remote capabilities with no error (empty applinks capabilities)
        //  - remote capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.4"));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 4));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertNoError(capabilities);
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestNotFound()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download failed due to connection refused
        // Expected:
        //  - error remote capabilities CONNECTION REFUSED with no versions nor capabilities
        //  - remote capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenThrow(new ManifestNotFoundException("Manifest fail",
                new ResponseException(
                        new ConnectException("Connection refused"))));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertNull(capabilities.getApplicationVersion());
        assertNull(capabilities.getApplinksVersion());
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.CONNECTION_REFUSED,
                containsInOrder(ConnectException.class.getSimpleName(), "Connection refused"));
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestInvalidNullApplicationVersion()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest downloaded but does not have application version
        // Expected:
        //  - error remote capabilities UNEXPECTED RESPONSE with no versions nor capabilities
        //  - remote capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setApplinksVersion("5.0.5"));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertNull(capabilities.getApplicationVersion());
        assertNull(capabilities.getApplinksVersion());
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNEXPECTED_RESPONSE,
                containsInOrder("applicationVersion=null", "applinksVersion=5.0.5"));
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkEmptyCacheManifestInvalidNullApplinksVersion()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest downloaded but does not have Applinks version
        // Expected:
        //  - error remote capabilities UNEXPECTED RESPONSE with no versions nor capabilities
        //  - remote capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0"));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertNull(capabilities.getApplicationVersion());
        assertNull(capabilities.getApplinksVersion());
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNEXPECTED_RESPONSE,
                containsInOrder("applicationVersion=7.0.0", "applinksVersion=null"));
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesGivenAtlassianLinkExistingCacheEntry()
            throws Exception {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - no request cache entry
        //  - Atlassian applink
        //  - cache entry exists (but old)
        // Expected:
        //  - capabilities returned from cache
        //  - remote capabilities in cache unchanged
        //  - no requests are made to manifest or capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.of(ApplinksCapabilities.STATUS_API))
                .build();
        cacheCapabilities(existingCapabilities, 30, TimeUnit.DAYS);

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertSame(existingCapabilities, capabilities);
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        verifyZeroInteractions(manifestDownloader);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkEmptyCacheManifestFoundNewUiVersionCapabilitiesOk()
            throws Exception {
        // Testing getCapabilities(id, maxAge, unit)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version contains capabilities (5.0.4+)
        //  - capabilities result OK
        // Expected:
        //  - successful capabilities with no error
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.1")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createOkCapabilitiesResponse(EnumSet.of(STATUS_API)));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 1, TimeUnit.DAYS);

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), contains(STATUS_API));
        assertNoError(capabilities);
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
        permissionValidationMocks.verifyValidateAdmin();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkEmptyCacheManifestFoundNewUiVersionCapabilitiesNetworkError()
            throws Exception {
        // Testing getCapabilities(id, maxAge, unit)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version contains capabilities (5.0.4+)
        //  - capabilities request fails with connection refused
        // Expected:
        //  - error capabilities CONNECTION_REFUSED with proper versions and empty capabilities
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.1")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createResponseWithNetworkError(
                new ResponseException(
                        new ConnectException("Connection refused"))));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 1, TimeUnit.DAYS);

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.CONNECTION_REFUSED,
                containsInOrder(ConnectException.class.getSimpleName(), "Connection refused"));
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkEmptyCacheManifestFoundPreNewUiVersion()
            throws Exception {
        // Testing getCapabilities(id, maxAge, unit)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download successful
        //  - remote version does not contain capabilities (pre-5.0.4)
        // Expected:
        //  - successful remote capabilities with no error (empty applinks capabilities)
        //  - capabilities are cached
        //  - no request to remote capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("6.4.8")
                .setApplinksVersion("4.3.5"));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 1, TimeUnit.DAYS);

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(6, 4, 8));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(4, 3, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertNoError(capabilities);
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkEmptyCacheManifestNotFound()
            throws Exception {
        // Testing getCapabilities(id, maxAge, unit)
        // Preconditions:
        //  - no request cache entry
        //  - cache entry does not exist
        //  - manifest download failed due to connection refused
        // Expected:
        //  - error remote capabilities CONNECTION REFUSED with no versions nor capabilities
        //  - remote capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenThrow(new ManifestNotFoundException("Manifest fail",
                new ResponseException(
                        new ConnectException("Connection refused"))));

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 1, TimeUnit.DAYS);

        assertNull(capabilities.getApplicationVersion());
        assertNull(capabilities.getApplinksVersion());
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.CONNECTION_REFUSED,
                containsInOrder(ConnectException.class.getSimpleName(), "Connection refused"));
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkExistingNonStaleCacheEntry()
            throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - no request cache entry
        //  - Atlassian applink
        //  - cache entry exists and is not stale for given maxAge
        // Expected:
        //  - capabilities returned from cache
        //  - remote capabilities in cache unchanged
        //  - no requests are made to manifest or capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.of(ApplinksCapabilities.STATUS_API))
                .build();
        cacheCapabilities(existingCapabilities, 50, TimeUnit.MINUTES);

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 60, TimeUnit.MINUTES);

        assertSame(existingCapabilities, capabilities);
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        verifyZeroInteractions(manifestDownloader);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkExistingStaleCacheEntryNoErrorNoVersionsChange()
            throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - no request cache entry
        //  - Atlassian applink
        //  - cache entry exists (no error) and is stale for given maxAge
        //  - manifest download successful
        //  - versions in manifest unchanged compared to cache
        // Expected:
        //  - cache lastUpdate updated
        //  - otherwise no changes to cached values
        //  - no request is made to capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.of(ApplinksCapabilities.STATUS_API))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));
        long originalLastUpdated = getLastUpdatedFromCache();

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 50, TimeUnit.MINUTES);

        assertEquals(existingCapabilities, capabilities);
        assertCachedValue(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertRequestCacheUpdatedWith(capabilities);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkExistingStaleCacheEntryWithErrorNoVersionsChangeNewUiVersionCapabilitiesOk()
            throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - no request cache entry
        //  - Atlassian applink
        //  - cache entry exists and is stale for given maxAge
        //  - existing cache entry contains error CONNECTION_REFUSED
        //  - manifest download successful
        //  - versions in manifest unchanged compared to cache
        //  - capabilities result OK
        // Expected:
        //  - full update of the cached values including remote capabilities
        //  - previous error overridden to null
        //  - request is made to download manifest and get capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .error(new SimpleApplinkError(ApplinkErrorType.CONNECTION_REFUSED))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createOkCapabilitiesResponse(EnumSet.of(ApplinksCapabilities.STATUS_API)));
        long originalLastUpdated = getLastUpdatedFromCache();

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 50, TimeUnit.MINUTES);

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), contains(ApplinksCapabilities.STATUS_API));
        assertNoError(capabilities);
        assertCachedValue(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkExistingStaleCacheEntryWithErrorNoVersionsChangeNewUiVersionCapabilitiesError()
            throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - no request cache entry
        //  - Atlassian applink
        //  - cache entry exists and is stale for given maxAge
        //  - existing cache entry contains error CONNECTION_REFUSED
        //  - manifest download successful
        //  - versions in manifest unchanged compared to cache
        //  - capabilities result error UNKNOWN HOST
        // Expected:
        //  - full update of the cached values including remote capabilities (to empty)
        //  - previous error overridden to UNKNOWN HOST
        //  - request is made to download manifest and get capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .error(new SimpleApplinkError(ApplinkErrorType.CONNECTION_REFUSED))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createResponseWithNetworkError(
                new ResponseException(
                        new UnknownHostException(TEST_URL))));
        long originalLastUpdated = getLastUpdatedFromCache();

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 50, TimeUnit.MINUTES);

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNKNOWN_HOST,
                containsInOrder(UnknownHostException.class.getSimpleName(), TEST_URL));
        assertCachedValue(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkExistingStaleCacheEntryWithNoErrorVersionsChangedNewUiVersionCapabilitiesOk()
            throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - Atlassian applink
        //  - no request cache entry
        //  - cache entry exists and is stale for given maxAge
        //  - existing cache entry contains no error
        //  - manifest download successful
        //  - versions in manifest changed compared to cache entry
        //  - capabilities result OK
        // Expected:
        //  - full update of the cached values including remote capabilities
        //  - no error
        //  - request is made to download manifest and get capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("4.3.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        // this is unrealistic (application version not changed compared to original, but applinks version did), but
        // we want to make sure that Applinks versions is ultimately used to determine whether the capabilities cache
        // should be updated or not
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createOkCapabilitiesResponse(EnumSet.of(STATUS_API)));
        long originalLastUpdated = getLastUpdatedFromCache();

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 50, TimeUnit.MINUTES);

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), contains(STATUS_API));
        assertNoError(capabilities);
        assertCachedValue(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkExistingStaleCacheEntryWithNoErrorVersionsChangedNewUiVersionCapabilitiesError()
            throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - Atlassian applink
        //  - no request cache entry
        //  - cache entry exists and is stale for given maxAge
        //  - existing cache entry contains no error
        //  - manifest download successful
        //  - applinks version in manifest changed compared to cache entry
        //  - capabilities result error UNKNOWN HOST
        // Expected:
        //  - full update of the cached values including remote capabilities
        //  - previous no error overridden to UNKNOWN HOST
        //  - request is made to download manifest and get capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("6.4.8"))
                .applinksVersion(ApplicationVersion.parse("4.3.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));
        mockRequestAnswer.addResponse(createResponseWithNetworkError(
                new ResponseException(
                        new UnknownHostException(TEST_URL))));
        long originalLastUpdated = getLastUpdatedFromCache();

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 50, TimeUnit.MINUTES);

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNKNOWN_HOST,
                containsInOrder(UnknownHostException.class.getSimpleName(), TEST_URL));
        assertCachedValue(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertRequestCacheUpdatedWith(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkExistingStaleCacheEntryWithErrorVersionsChangedPreNewUiVersion()
            throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - Atlassian applink
        //  - no request cache entry
        //  - cache entry exists and is stale for given maxAge
        //  - existing cache entry contains error UNEXPECTED RESPONSE
        //  - manifest download successful
        //  - applinks version in manifest changed compared to cache entry
        //  - remote version does not contain capabilities (pre-5.0.4)
        // Expected:
        //  - full update of the cached values including remote capabilities (to empty)
        //  - previous error overridden to no error
        //  - no request to remote capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("6.4.1"))
                .applinksVersion(ApplicationVersion.parse("4.3.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .error(new SimpleApplinkError(ApplinkErrorType.UNEXPECTED_RESPONSE))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenReturn(new MockManifest(DEFAULT_ID)
                .setVersion("6.4.8")
                .setApplinksVersion("4.3.6"));
        long originalLastUpdated = getLastUpdatedFromCache();

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 50, TimeUnit.MINUTES);

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(6, 4, 8));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(4, 3, 6));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertNoError(capabilities);
        assertCachedValue(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertRequestCacheUpdatedWith(capabilities);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenAtlassianLinkExistingStaleCacheEntryWithNoErrorManifestNotFound()
            throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - Atlassian applink
        //  - no request cache entry
        //  - cache entry exists and is stale for given maxAge
        //  - existing cache entry contains no error
        //  - manifest download fails with CONNECTION REFUSED
        // Expected:
        //  - previous values for versions and capabilities are preserved
        //  - previous no error overridden to no error CONNECTION REFUSED
        //  - no request to remote capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.of(STATUS_API))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        when(manifestDownloader.downloadNoEvent(TEST_URI)).thenThrow(new ManifestNotFoundException("Manifest fail",
                new ResponseException(
                        new ConnectException("Connection refused"))));
        long originalLastUpdated = getLastUpdatedFromCache();

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 50, TimeUnit.MINUTES);

        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), contains(STATUS_API));
        assertHasError(capabilities, ApplinkErrorType.CONNECTION_REFUSED,
                containsInOrder(ConnectException.class.getSimpleName(), "Connection refused"));
        assertCachedValue(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertRequestCacheUpdatedWith(capabilities);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesGivenExistingRequestCacheEntry() throws ServiceException {
        // Testing getCapabilities(id)
        // Preconditions:
        //  - Atlassian applink
        //  - request cache entry exists
        // Expected:
        //  - request cache entry should be used
        //  - no cache access and no requests

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.of(STATUS_API))
                .build();
        // should not matter how old it is
        setUpCapabilitiesInRequestCache(existingCapabilities, 10, TimeUnit.DAYS);

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(DEFAULT_ID.applicationId());

        assertSame(existingCapabilities, capabilities);
        assertRequestCacheNotUpdated();
        verifyZeroInteractions(manifestDownloader);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenExistingStaleRequestCacheEntry() throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - Atlassian applink
        //  - request cache entry exists but stale
        //  - cache entry exists and is not stale for given maxAge
        // Expected:
        //  - original request cache entry is not used
        //  - existing cache entry is used
        //  - request cache is updated with the cached value
        //  - no request to remote capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities requestCachedCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.of(STATUS_API))
                .build();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.1"))
                .applinksVersion(ApplicationVersion.parse("5.0.6"))
                .capabilities(EnumSet.of(STATUS_API))
                .build();
        setUpCapabilitiesInRequestCache(requestCachedCapabilities, 60, TimeUnit.MINUTES);
        cacheCapabilities(existingCapabilities, 30, TimeUnit.MINUTES);

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 50, TimeUnit.MINUTES);

        assertSame(existingCapabilities, capabilities);
        assertCachedValue(capabilities);
        assertRequestCacheUpdatedWith(capabilities);
        verifyZeroInteractions(manifestDownloader);
        assertNoRequests();
    }

    @Test
    public void getCapabilitiesWithMaxAgeGivenExistingNonStaleRequestCacheEntry() throws Exception {
        // Testing getCapabilities(id, maxAge, units)
        // Preconditions:
        //  - Atlassian applink
        //  - request cache entry exists and is not stale for given maxAge
        //  - cache entry exists and is not stale for given maxAge
        // Expected:
        //  - request cache entry is used
        //  - existing cache entry is ignored (assumed up to date)
        //  - no cache access and no requests

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities requestCachedCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.of(STATUS_API))
                .build();
        RemoteApplicationCapabilities cachedCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.1"))
                .applinksVersion(ApplicationVersion.parse("5.0.6"))
                .capabilities(EnumSet.of(STATUS_API))
                .build();
        setUpCapabilitiesInRequestCache(requestCachedCapabilities, 40, TimeUnit.MINUTES);
        cacheCapabilities(cachedCapabilities, 30, TimeUnit.MINUTES);

        RemoteApplicationCapabilities capabilities = remoteCapabilitiesService.getCapabilities(
                DEFAULT_ID.applicationId(), 50, TimeUnit.MINUTES);

        assertSame(requestCachedCapabilities, capabilities);
        assertRequestCacheNotUpdated();
        verifyZeroInteractions(manifestDownloader);
        assertNoRequests();
    }


    @Test
    public void onManifestDownloadedGivenEmptyCacheNewUiVersionCapabilitiesOk()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry does not exist
        //  - remote version contains capabilities (5.0.4+)
        //  - capabilities result OK
        // Expected:
        //  - successful capabilities with no error
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        mockRequestAnswer.addResponse(createOkCapabilitiesResponse(EnumSet.of(STATUS_API)));

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.1")
                .setApplinksVersion("5.0.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), contains(STATUS_API));
        assertNoError(capabilities);
        assertOneRequestToCapabilities();
    }

    @Test
    public void onManifestDownloadedGivenEmptyCacheNewUiVersionCapabilitiesNetworkError()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry does not exist
        //  - remote version contains capabilities (5.0.4+)
        //  - capabilities request fails with connection refused
        // Expected:
        //  - error capabilities CONNECTION_REFUSED with proper versions and empty capabilities
        //  - capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        mockRequestAnswer.addResponse(createResponseWithNetworkError(
                new ResponseException(
                        new ConnectException("Connection refused"))));

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.1")
                .setApplinksVersion("5.0.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.CONNECTION_REFUSED,
                containsInOrder(ConnectException.class.getSimpleName(), "Connection refused"));
        assertOneRequestToCapabilities();
    }

    @Test
    public void onManifestDownloadedGivenEmptyCachePreNewUiVersion()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry does not exist
        //  - remote version does not contain capabilities (pre-5.0.4)
        // Expected:
        //  - successful remote capabilities with no error (empty applinks capabilities)
        //  - capabilities are cached
        //  - no request to remote capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("6.4.8")
                .setApplinksVersion("4.3.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(6, 4, 8));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(4, 3, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertNoError(capabilities);
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadedGivenEmptyCacheManifestInvalidNullApplinksVersion()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry does not exist
        //  - manifest does not have Applinks version
        // Expected:
        //  - error remote capabilities UNEXPECTED RESPONSE with no versions nor capabilities
        //  - remote capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID).setVersion("6.4.8"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertNull(capabilities.getApplicationVersion());
        assertNull(capabilities.getApplinksVersion());
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNEXPECTED_RESPONSE,
                containsInOrder("applicationVersion=6.4.8", "applinksVersion=null"));
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadedGivenEmptyCacheManifestInvalidApplicationVersionNull()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry does not exist
        //  - manifest does not have Application version
        // Expected:
        //  - error remote capabilities UNEXPECTED RESPONSE with no versions nor capabilities
        //  - remote capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID).setApplinksVersion("4.3.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertNull(capabilities.getApplicationVersion());
        assertNull(capabilities.getApplinksVersion());
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNEXPECTED_RESPONSE,
                containsInOrder("applicationVersion=null", "applinksVersion=4.3.5"));
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadedGivenEmptyCacheManifestInvalidApplicationVersion()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry does not exist
        //  - manifest has invalid application version
        // Expected:
        //  - error remote capabilities UNEXPECTED RESPONSE with no versions nor capabilities
        //  - remote capabilities are cached

        setUpExistingApplink();
        setUpAtlassianApplinkType();

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("m01")
                .setApplinksVersion("4.3.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertNull(capabilities.getApplicationVersion());
        assertNull(capabilities.getApplinksVersion());
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNEXPECTED_RESPONSE,
                containsInOrder("applicationVersion=m01", "applinksVersion=4.3.5"));
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadedGivenExistingCacheEntryNoErrorNoVersionsChanged()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry exists (no error)
        //  - versions in manifest unchanged compared to cache
        // Expected:
        //  - cache lastUpdate updated
        //  - otherwise no changes to cached values
        //  - no request is made to capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.of(ApplinksCapabilities.STATUS_API))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        long originalLastUpdated = getLastUpdatedFromCache();

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertEquals(existingCapabilities, capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadedGivenExistingCacheEntryWithErrorNoVersionsChangeNewUiVersionCapabilitiesOk()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry exists
        //  - existing cache entry contains error CONNECTION_REFUSED
        //  - versions in manifest unchanged compared to cache
        //  - capabilities result OK
        // Expected:
        //  - full update of the cached values including remote capabilities
        //  - previous error overridden to null
        //  - request is made to get capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .error(new SimpleApplinkError(ApplinkErrorType.CONNECTION_REFUSED))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        mockRequestAnswer.addResponse(createOkCapabilitiesResponse(EnumSet.of(ApplinksCapabilities.STATUS_API)));
        long originalLastUpdated = getLastUpdatedFromCache();

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), contains(ApplinksCapabilities.STATUS_API));
        assertNoError(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertOneRequestToCapabilities();
    }

    @Test
    public void onManifestDownloadedGivenExistingCacheEntryWithErrorNoVersionsChangeNewUiVersionCapabilitiesError()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry exists
        //  - existing cache entry contains error CONNECTION_REFUSED
        //  - versions in manifest unchanged compared to cache
        //  - capabilities result error UNKNOWN HOST
        // Expected:
        //  - full update of the cached values including remote capabilities (to empty)
        //  - previous error overridden to UNKNOWN HOST
        //  - request is made to get capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("5.0.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .error(new SimpleApplinkError(ApplinkErrorType.CONNECTION_REFUSED))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        mockRequestAnswer.addResponse(createResponseWithNetworkError(
                new ResponseException(
                        new UnknownHostException(TEST_URL))));
        long originalLastUpdated = getLastUpdatedFromCache();

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNKNOWN_HOST,
                containsInOrder(UnknownHostException.class.getSimpleName(), TEST_URL));
        assertCachedValueUpdated(originalLastUpdated);
        assertOneRequestToCapabilities();
    }

    @Test
    public void onManifestDownloadedGivenExistingCacheEntryWithNoErrorVersionsChangedNewUiVersionCapabilitiesOk()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry exists
        //  - existing cache entry contains no error
        //  - versions in manifest changed compared to cache entry
        //  - capabilities result OK
        // Expected:
        //  - full update of the cached values including remote capabilities
        //  - no error
        //  - request is made to get capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("7.0.0"))
                .applinksVersion(ApplicationVersion.parse("4.3.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        mockRequestAnswer.addResponse(createOkCapabilitiesResponse(EnumSet.of(STATUS_API)));
        long originalLastUpdated = getLastUpdatedFromCache();

        // this is unrealistic (application version not changed compared to original, but applinks version did), but
        // we want to make sure that Applinks versions is ultimately used to determine whether the capabilities cache
        // should be updated or not
        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), contains(STATUS_API));
        assertNoError(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertOneRequestToCapabilities();
    }

    @Test
    public void onManifestDownloadedGivenExistingCacheEntryWithNoErrorVersionsChangedNewUiVersionCapabilitiesError()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry exists
        //  - existing cache entry contains no error
        //  - applinks version in manifest changed compared to cache entry
        //  - capabilities result error UNKNOWN HOST
        // Expected:
        //  - full update of the cached values including remote capabilities
        //  - previous no error overridden to UNKNOWN HOST
        //  - request is made to get capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("6.4.8"))
                .applinksVersion(ApplicationVersion.parse("4.3.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        mockRequestAnswer.addResponse(createResponseWithNetworkError(
                new ResponseException(
                        new UnknownHostException(TEST_URL))));
        long originalLastUpdated = getLastUpdatedFromCache();

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("7.0.0")
                .setApplinksVersion("5.0.5"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(7, 0, 0));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(5, 0, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.UNKNOWN_HOST,
                containsInOrder(UnknownHostException.class.getSimpleName(), TEST_URL));
        assertCachedValueUpdated(originalLastUpdated);
        assertOneRequestToCapabilities();
    }

    @Test
    public void onManifestDownloadedGivenExistingCacheEntryWithErrorVersionsChangedPreNewUiVersion()
            throws Exception {
        // Testing onManifestDownloaded
        // Preconditions:
        //  - cache entry exists
        //  - existing cache entry contains error UNEXPECTED RESPONSE
        //  - applinks version in manifest changed compared to cache entry
        //  - remote version does not contain capabilities (pre-5.0.4)
        // Expected:
        //  - full update of the cached values including remote capabilities (to empty)
        //  - previous error overridden to no error
        //  - no request to remote capabilities

        setUpExistingApplink();
        setUpAtlassianApplinkType();
        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("6.4.1"))
                .applinksVersion(ApplicationVersion.parse("4.3.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .error(new SimpleApplinkError(ApplinkErrorType.UNEXPECTED_RESPONSE))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        long originalLastUpdated = getLastUpdatedFromCache();

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("6.4.8")
                .setApplinksVersion("4.3.6"));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(6, 4, 8));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(4, 3, 6));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertNoError(capabilities);
        assertCachedValueUpdated(originalLastUpdated);
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadedApplinkDoesNotExist() {
        // Testing onManifestDownloaded
        // preconditions: application ID does not exist
        // expected: cache entry is not added, no requests made

        applinkHelperMocks.setUpNonExistingApplink(DEFAULT_ID);

        raiseManifestDownloaded(new MockManifest(DEFAULT_ID)
                .setVersion("6.4.8")
                .setApplinksVersion("4.3.6"));

        assertEmptyCache();
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadFailedGivenNoMatchingApplink() throws Exception {
        // Testing onManifestDownloadFailed
        // Preconditions:
        //  - cache entry does not exist
        //  - manifest download failed with error CONNECTION REFUSED
        //  - no applink with matching application URL exists
        // Expected:
        //  - no cache update
        //  - no request to remote capabilities

        when(applicationLinkService.getApplicationLinks()).thenReturn(
                ImmutableList.of(mockApplink2, mockApplink3, mockApplink4));

        remoteCapabilitiesService.onManifestDownloadFailed(
                new ManifestDownloadFailedEvent(TEST_URI, new ResponseException(
                        new ConnectException("Connection refused"))));

        assertEmptyCache();
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadFailedGivenMatchingApplinkEmptyCache() throws Exception {
        // Testing onManifestDownloadFailed
        // Preconditions:
        //  - cache entry does not exist
        //  - manifest download failed with error CONNECTION REFUSED for a matching applink
        // Expected:
        //  - new empty cache entry with error CONNECTION REFUSED
        //  - no request to remote capabilities

        when(applicationLinkService.getApplicationLinks()).thenReturn(
                ImmutableList.of(mockApplink, mockApplink2, mockApplink3, mockApplink4));

        remoteCapabilitiesService.onManifestDownloadFailed(
                new ManifestDownloadFailedEvent(TEST_URI, new ResponseException(
                        new ConnectException("Connection refused"))));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertNull(capabilities.getApplicationVersion());
        assertNull(capabilities.getApplinksVersion());
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.CONNECTION_REFUSED,
                containsInOrder(ConnectException.class.getSimpleName(), "Connection refused"));
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadFailedGivenMatchingApplinkExistingCacheEntryWithNoError() throws Exception {
        // Testing onManifestDownloadFailed
        // Preconditions:
        //  - existing cache entry with no error
        //  - manifest download failed with error CONNECTION REFUSED for a matching applink
        // Expected:
        //  - updated cache entry with previous values and (new) error CONNECTION REFUSED
        //  - no request to remote capabilities

        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("6.4.1"))
                .applinksVersion(ApplicationVersion.parse("4.3.5"))
                .capabilities(EnumSet.of(STATUS_API))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        when(applicationLinkService.getApplicationLinks()).thenReturn(
                ImmutableList.of(mockApplink, mockApplink2, mockApplink3, mockApplink4));
        long originalLastUpdated = getLastUpdatedFromCache();

        remoteCapabilitiesService.onManifestDownloadFailed(
                new ManifestDownloadFailedEvent(TEST_URI, new ResponseException(
                        new ConnectException("Connection refused"))));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(6, 4, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(4, 3, 5));
        assertThat(capabilities.getCapabilities(), contains(STATUS_API));
        assertHasError(capabilities, ApplinkErrorType.CONNECTION_REFUSED,
                containsInOrder(ConnectException.class.getSimpleName(), "Connection refused"));
        assertCachedValueUpdated(originalLastUpdated);
        assertNoRequests();
    }

    @Test
    public void onManifestDownloadFailedGivenMatchingApplinkExistingCacheEntryWithError() throws Exception {
        // Testing onManifestDownloadFailed
        // Preconditions:
        //  - existing cache entry with error UNKNOWN HOST
        //  - manifest download failed with error CONNECTION REFUSED for a matching applink
        // Expected:
        //  - updated cache entry with previous values
        //  - error overridden with CONNECTION REFUSED
        //  - no request to remote capabilities

        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("6.4.1"))
                .applinksVersion(ApplicationVersion.parse("4.3.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .error(new SimpleApplinkError(ApplinkErrorType.UNKNOWN_HOST))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);
        when(applicationLinkService.getApplicationLinks()).thenReturn(
                ImmutableList.of(mockApplink, mockApplink2, mockApplink3, mockApplink4));
        long originalLastUpdated = getLastUpdatedFromCache();

        remoteCapabilitiesService.onManifestDownloadFailed(
                new ManifestDownloadFailedEvent(TEST_URI, new ResponseException(
                        new ConnectException("Connection refused"))));

        RemoteApplicationCapabilities capabilities = getCapabilitiesFromCache();
        assertThat(capabilities.getApplicationVersion(), hasVersionNumber(6, 4, 1));
        assertThat(capabilities.getApplinksVersion(), hasVersionNumber(4, 3, 5));
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertHasError(capabilities, ApplinkErrorType.CONNECTION_REFUSED,
                containsInOrder(ConnectException.class.getSimpleName(), "Connection refused"));
        assertCachedValueUpdated(originalLastUpdated);
        assertNoRequests();
    }


    private static void assertEmptyErrorCapabilities(RemoteApplicationCapabilities capabilities, ApplinkErrorType error) {
        assertEmptyErrorCapabilities(capabilities, error, null);
    }

    private static void assertEmptyErrorCapabilities(RemoteApplicationCapabilities capabilities, ApplinkErrorType error,
                                                     String errorDetails) {
        assertNull(capabilities.getApplicationVersion());
        assertNull(capabilities.getApplinksVersion());
        assertThat(capabilities.getCapabilities(), emptyIterable());
        assertNotNull(capabilities.getError());
        assertEquals(error, capabilities.getError().getType());
        assertEquals(errorDetails, capabilities.getError().getDetails());
    }

    private static void assertNoError(RemoteApplicationCapabilities capabilities) {
        assertFalse("Expected hasError to return false", capabilities.hasError());
        assertNull("Expected no error", capabilities.getError());
    }

    private static void assertHasError(RemoteApplicationCapabilities capabilities, ApplinkErrorType errorType,
                                       Matcher<String> detailsMatcher) {
        assertTrue("Expected hasError to return true", capabilities.hasError());
        assertNotNull("Expected an error", capabilities.getError());
        assertThat(capabilities.getError(), withType(errorType));
        assertThat(capabilities.getError(), withDetailsThat(detailsMatcher));
    }

    private static MockApplicationLinkResponse createOkCapabilitiesResponse(Set<ApplinksCapabilities> capabilities) {
        return createOkCapabilitiesResponseForNames(toEnumNames(capabilities));
    }

    private static MockApplicationLinkResponse createOkCapabilitiesResponseForNames(List<String> capabilities) {
        return new MockApplicationLinkResponse()
                .setStatus(Status.OK)
                .setEntity(capabilities);
    }

    private static MockApplicationLinkResponse createOkCapabilitiesResponse(String body) {
        return createCapabilitiesResponse(Status.OK, body);
    }

    private static MockApplicationLinkResponse createNotFoundCapabilitiesResponse(String body) {
        return createCapabilitiesResponse(Status.NOT_FOUND, body);
    }

    private static MockApplicationLinkResponse createCapabilitiesResponse(Status status, String body) {
        return new MockApplicationLinkResponse()
                .setStatus(status)
                .setEntity(body)
                .setResponseBody(body);
    }

    private static MockApplicationLinkResponse createResponseWithNetworkError(ResponseException error) {
        return new MockApplicationLinkResponse().setResponseException(error);
    }

    @Test
    public void cacheClearedOnApplinkDeleted() {
        // Testing onApplinkDeleted
        // preconditions: ApplicationLinDeletedEvent raised for existing cache entry
        // expected: cache entry for the application ID has been removed

        RemoteApplicationCapabilities existingCapabilities = new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("6.4.1"))
                .applinksVersion(ApplicationVersion.parse("4.3.5"))
                .capabilities(EnumSet.noneOf(ApplinksCapabilities.class))
                .error(new SimpleApplinkError(ApplinkErrorType.UNEXPECTED_RESPONSE))
                .build();
        cacheCapabilities(existingCapabilities, 60, TimeUnit.MINUTES);

        remoteCapabilitiesService.onApplinkDeleted(new ApplicationLinkDeletedEvent(mockApplink));

        assertEmptyCache();
    }    // could do with Enums.stringConverter except Confluence needs to upgrade Guava <sigh>

    private static List<String> toEnumNames(Set<ApplinksCapabilities> capabilities) {
        return ImmutableList.copyOf(transform(capabilities, ApplinksEnums.toName()));
    }

    private void assertOneRequestToCapabilities() {
        assertThat(mockRequestAnswer.executedRequests(), contains(allOf(
                withMethodType(Request.MethodType.GET),
                withUrlThat(containsInOrder("rest", "applinks", "latest", "capabilities"))
        )));
    }

    private void assertCachedValueUpdated(long originalLastUpdated) {
        assertThat("lastUpdated should have been updated in cache",
                getLastUpdatedFromCache(), greaterThan(originalLastUpdated));
    }

    private void assertNoRequests() {
        assertThat(mockRequestAnswer.executedRequests(), emptyIterable());
    }

    private void assertEmptyCache() {
        assertThat(remoteCapabilitiesService.getCapabilitiesCache().getKeys(), emptyIterable());
    }

    private void assertRequestCacheUpdatedWith(RemoteApplicationCapabilities capabilities) {
        verify(capabilitiesRequestCache).put(eq(DEFAULT_ID.applicationId()), requestCapabilitiesCaptor.capture());
        assertEquals(requestCapabilitiesCaptor.getValue().capabilities, capabilities);
    }

    private void assertRequestCacheNotUpdated() {
        verify(capabilitiesRequestCache).get(DEFAULT_ID.applicationId());
        verifyNoMoreInteractions(capabilitiesRequestCache);
    }

    @SuppressWarnings("ConstantConditions")
    private void assertCachedValue(RemoteApplicationCapabilities capabilities) {
        assertTrue(remoteCapabilitiesService.getCapabilitiesCache().containsKey(DEFAULT_ID.applicationId()));
        assertEquals(capabilities,
                remoteCapabilitiesService.getCapabilitiesCache().get(DEFAULT_ID.applicationId()).capabilities);
    }

    private void cacheCapabilities(RemoteApplicationCapabilities existingCapabilities, long updatedBeforeNow,
                                   TimeUnit unit) {
        long updated = System.currentTimeMillis() - unit.toMillis(updatedBeforeNow);
        remoteCapabilitiesService.getCapabilitiesCache().put(DEFAULT_ID.applicationId(),
                new DefaultRemoteCapabilitiesService.CachedCapabilities(existingCapabilities, updated));
    }

    @SuppressWarnings("ConstantConditions")
    private RemoteApplicationCapabilities getCapabilitiesFromCache() {
        return remoteCapabilitiesService.getCapabilitiesCache().get(DEFAULT_ID.applicationId()).capabilities;
    }

    @SuppressWarnings("ConstantConditions")
    private long getLastUpdatedFromCache() {
        return remoteCapabilitiesService.getCapabilitiesCache().get(DEFAULT_ID.applicationId()).lastUpdated;
    }

    private void raiseManifestDownloaded(Manifest manifest) {
        remoteCapabilitiesService.onManifestDownloaded(new ManifestDownloadedEvent(manifest));
    }

    private void setUpCapabilitiesInRequestCache(RemoteApplicationCapabilities existingCapabilities,
                                                 long updatedBeforeNow,
                                                 TimeUnit unit) {
        long updated = System.currentTimeMillis() - unit.toMillis(updatedBeforeNow);
        when(capabilitiesRequestCache.get(DEFAULT_ID.applicationId()))
                .thenReturn(new DefaultRemoteCapabilitiesService.CachedCapabilities(existingCapabilities, updated));
    }

    private void setUpExistingApplink() {
        applinkHelperMocks.setUpExistingApplink(DEFAULT_ID, mockApplink);
    }

    private void setUpGenericApplinkType() {
        ApplicationType type = mock(GenericApplicationType.class);
        when(mockApplink.getType()).thenReturn(type);
    }

    private void setUpNonAtlassianApplinkType() {
        ApplicationType nonAtlassian = mock(ApplicationType.class);
        when(mockApplink.getType()).thenReturn(nonAtlassian);
    }

    private void setUpAtlassianApplinkType() {
        ApplicationType atlassianType = mock(ApplicationType.class,
                withSettings().extraInterfaces(BuiltinApplinksType.class));
        when(mockApplink.getType()).thenReturn(atlassianType);
    }

    @SuppressWarnings("ConstantConditions")
    public static class TestManifestDownloader extends AppLinksManifestDownloader {
        public TestManifestDownloader() {
            super(null, null, null, null);
        }

        @Override
        public Manifest download(URI url) throws ManifestNotFoundException {
            return null;
        }

        @Override
        public Manifest downloadNoEvent(URI url) throws ManifestNotFoundException {
            return null;
        }
    }
}
