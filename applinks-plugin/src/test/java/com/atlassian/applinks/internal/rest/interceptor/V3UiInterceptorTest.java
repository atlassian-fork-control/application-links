package com.atlassian.applinks.internal.rest.interceptor;

import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.core.HttpResponseContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.hamcrest.HamcrestArgumentMatcher;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static com.atlassian.applinks.test.matchers.JaxRsResponseMatchers.withStatus;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class V3UiInterceptorTest {
    @Mock
    private ApplinksFeatureService applinksFeatureService;

    @InjectMocks
    private V3UiInterceptor interceptor;

    @Test
    public void shouldAllowToProceedWhenNewUiIsOn() throws Exception {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.V3_UI)).thenReturn(true);
        MethodInvocation invocation = mock(MethodInvocation.class);

        interceptor.intercept(invocation);

        verify(invocation).invoke();
        verifyNoMoreInteractions(invocation);
    }

    @Test
    public void shouldReturn404WhenNewUiIsOff() throws Exception {
        when(applinksFeatureService.isEnabled(ApplinksFeatures.V3_UI)).thenReturn(false);

        MethodInvocation invocation = mock(MethodInvocation.class);
        HttpContext httpContext = mock(HttpContext.class);
        HttpResponseContext responseContext = mock(HttpResponseContext.class);

        when(invocation.getHttpContext()).thenReturn(httpContext);
        when(httpContext.getResponse()).thenReturn(responseContext);

        interceptor.intercept(invocation);

        verify(responseContext).setResponse(argThat(new HamcrestArgumentMatcher<>(withStatus(Response.Status.NOT_FOUND))));
        verifyNoMoreInteractions(responseContext);
    }
}
