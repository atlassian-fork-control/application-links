package com.atlassian.applinks.internal.permission;

import com.atlassian.applinks.core.ElevatedPermissionsService;
import com.atlassian.applinks.core.ElevatedPermissionsServiceImpl;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.test.mock.SimpleServiceExceptionFactory;
import com.atlassian.applinks.test.mock.MockI18nResolver;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.applinks.internal.common.i18n.I18nKey.newI18nKey;
import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SalPermissionValidationServiceTest {
    private static final String TEST_USERNAME = "test";
    private static final UserKey TEST_USER = new UserKey(TEST_USERNAME);

    private static final String TEST_OPERATION_KEY = "test.operation";
    private static final String DEFAULT_OPERATION_KEY = "applinks.service.error.access.defaultoperation";
    public static final String ADMIN_KEY = "applinks.service.permission.admin";
    public static final String SYSADMIN_KEY = "applinks.service.permission.sysadmin";

    @Spy // make it work with @InjectMocks, append args to the result
    private I18nResolver i18nResolver = new MockI18nResolver(true);
    @Spy
    private ElevatedPermissionsService elevatedPermissions = new ElevatedPermissionsServiceImpl();
    @Mock
    private UserManager userManager;
    @Spy // make it work with @InjectMocks, append args to the result
    private ServiceExceptionFactory serviceExceptionFactory = new SimpleServiceExceptionFactory(true);

    @InjectMocks
    private SalPermissionValidationService permissionValidationService;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();


    // ------ User ------

    @Test
    public void validateAuthenticatedGivenNoCurrentAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE,
                "applinks.service.error.access.defaultoperation"));

        permissionValidationService.validateAuthenticated();
    }

    @Test
    public void validateAuthenticatedWithCustomOperationI18nGivenNoCurrentAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE, TEST_OPERATION_KEY));

        permissionValidationService.validateAuthenticated(newI18nKey("test.operation"));
    }

    @Test
    public void validateAuthenticatedGivenCurrentAuthenticatedUser() throws NoAccessException {
        setUpCurrentUser();

        permissionValidationService.validateAuthenticated();
    }

    @Test
    public void validateAuthenticatedGivenCurrentAdminUser() throws NoAccessException {
        setUpCurrentUser();
        setUpAdminUser();

        permissionValidationService.validateAuthenticated();
    }

    @Test
    public void validateAuthenticatedGivenCurrentSystemAdminUser() throws NoAccessException {
        setUpCurrentUser();
        setUpSysAdminUser();

        permissionValidationService.validateAuthenticated();
    }

    @Test
    public void validateAuthenticatedGivenNoCurrentUserPermissionsElevatedToUser() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.USER, () -> {
            permissionValidationService.validateAuthenticated();
            return null;
        });
    }

    @Test
    public void validateAuthenticatedGivenNoCurrentUserPermissionsElevatedToAdmin() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.ADMIN, () -> {
            permissionValidationService.validateAuthenticated();
            return null;
        });
    }

    @Test
    public void validateAuthenticatedGivenNoCurrentUserPermissionsElevatedToSysadmin() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.ADMIN, () -> {
            permissionValidationService.validateAuthenticated();
            return null;
        });
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void validateAuthenticatedGivenNullI18nKey() throws NoAccessException {
        permissionValidationService.validateAuthenticated((I18nKey) null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void validateAuthenticatedGivenNullUserAndI18nKey() throws NoAccessException {
        permissionValidationService.validateAuthenticated(null, null);
    }

    @Test
    public void validateAuthenticatedGivenNoAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE,
                DEFAULT_OPERATION_KEY));

        permissionValidationService.validateAuthenticated((UserKey) null);
    }

    @Test
    public void validateAuthenticatedWithCustomOperationI18nGivenNoAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE, TEST_OPERATION_KEY));

        permissionValidationService.validateAuthenticated(null, newI18nKey(TEST_OPERATION_KEY));
    }

    @Test
    public void validateAuthenticatedGivenAuthenticatedUser() throws NoAccessException {
        permissionValidationService.validateAuthenticated(TEST_USER);
    }

    @Test
    public void validateAuthenticatedGivenAdminUser() throws NoAccessException {
        setUpAdminUser();

        permissionValidationService.validateAuthenticated(TEST_USER);
    }

    @Test
    public void validateAuthenticatedGivenSystemAdminUser() throws NoAccessException {
        setUpSysAdminUser();

        permissionValidationService.validateAuthenticated(TEST_USER);
    }

    @Test(expected = NotAuthenticatedException.class)
    public void validateAuthenticatedGivenNoUserPermissionsElevatedToUser() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.USER, () -> {
            permissionValidationService.validateAuthenticated((UserKey) null);
            return null;
        });
    }


    // ------ Admin ------

    @Test
    public void validateAdminGivenNoCurrentAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE,
                DEFAULT_OPERATION_KEY));

        permissionValidationService.validateAdmin();
    }

    @Test
    public void validateAdminGivenCurrentAuthenticatedUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, ADMIN_KEY,
                DEFAULT_OPERATION_KEY));

        setUpCurrentUser();

        permissionValidationService.validateAdmin();
    }

    @Test
    public void validateAdminWithCustomOperationI18nGivenCurrentAuthenticatedUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, ADMIN_KEY,
                TEST_OPERATION_KEY));

        setUpCurrentUser();

        permissionValidationService.validateAdmin(newI18nKey("test.operation"));
    }

    @Test
    public void validateAdminGivenCurrentAdminUser() throws NoAccessException {
        setUpCurrentUser();
        setUpAdminUser();

        permissionValidationService.validateAdmin();
    }

    @Test
    public void validateAdminGivenCurrentSystemAdminUser() throws NoAccessException {
        setUpCurrentUser();
        setUpSysAdminUser();

        permissionValidationService.validateAdmin();
    }

    @Test(expected = NotAuthenticatedException.class)
    public void validateAdminGivenNoCurrentUserPermissionsElevatedToUser() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.USER, () -> {
            permissionValidationService.validateAdmin();
            return null;
        });
    }

    @Test
    public void validateAdminGivenNoCurrentUserPermissionsElevatedToAdmin() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.ADMIN, () -> {
            permissionValidationService.validateAdmin();
            return null;
        });
    }

    @Test
    public void validateAdminGivenNoCurrentUserPermissionsElevatedToSysadmin() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.ADMIN, () -> {
            permissionValidationService.validateAdmin();
            return null;
        });
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void validateAdminGivenNullI18nKey() throws NoAccessException {
        permissionValidationService.validateAdmin((I18nKey) null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void validateAdminGivenNullUserAndI18nKey() throws NoAccessException {
        permissionValidationService.validateAdmin(null, null);
    }

    @Test
    public void validateAdminGivenNoAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE,
                DEFAULT_OPERATION_KEY));

        permissionValidationService.validateAdmin((UserKey) null);
    }

    @Test
    public void validateAdminWithCustomOperationI18nGivenNoAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE, TEST_OPERATION_KEY));

        permissionValidationService.validateAdmin(null, newI18nKey(TEST_OPERATION_KEY));
    }

    @Test
    public void validateAdminGivenAuthenticatedUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, ADMIN_KEY,
                DEFAULT_OPERATION_KEY));

        permissionValidationService.validateAdmin(TEST_USER);
    }

    @Test
    public void validateAdminWithCustomOperationI18nGivenAuthenticatedUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, ADMIN_KEY,
                TEST_OPERATION_KEY));

        permissionValidationService.validateAdmin(TEST_USER, newI18nKey(TEST_OPERATION_KEY));
    }

    @Test
    public void validateAdminGivenAdminUser() throws NoAccessException {
        setUpAdminUser();

        permissionValidationService.validateAdmin(TEST_USER);
    }

    @Test
    public void validateAdminGivenSystemAdminUser() throws NoAccessException {
        setUpSysAdminUser();

        permissionValidationService.validateAdmin(TEST_USER);
    }

    @Test(expected = NotAuthenticatedException.class)
    public void validateAdminGivenNoUserPermissionsElevatedToAdmin() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.ADMIN, () -> {
            permissionValidationService.validateAdmin((UserKey) null);
            return null;
        });
    }


    // ------ System Admin ------

    @Test
    public void validateSystemAdminGivenNoCurrentAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE,
                "applinks.service.error.access.defaultoperation"));

        permissionValidationService.validateSysadmin();
    }

    @Test
    public void validateSystemAdminGivenCurrentAuthenticatedUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE,
                "applinks.service.permission.sysadmin", "applinks.service.error.access.defaultoperation"));

        setUpCurrentUser();

        permissionValidationService.validateSysadmin();
    }

    @Test
    public void validateSystemAdminWithCustomOperationI18nGivenCurrentAuthenticatedUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, SYSADMIN_KEY,
                TEST_OPERATION_KEY));

        setUpCurrentUser();

        permissionValidationService.validateSysadmin(newI18nKey(TEST_OPERATION_KEY));
    }

    @Test
    public void validateSystemAdminGivenCurrentAdminUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, SYSADMIN_KEY,
                DEFAULT_OPERATION_KEY));

        setUpCurrentUser();
        setUpAdminUser();

        permissionValidationService.validateSysadmin();
    }

    @Test
    public void validateSystemAdminWithCustomOperationI18nGivenCurrentAdminUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, SYSADMIN_KEY,
                TEST_OPERATION_KEY));

        setUpCurrentUser();
        setUpAdminUser();

        permissionValidationService.validateSysadmin(newI18nKey(TEST_OPERATION_KEY));
    }

    @Test
    public void validateSystemAdminGivenCurrentSystemAdminUser() throws NoAccessException {
        setUpCurrentUser();
        setUpSysAdminUser();

        permissionValidationService.validateSysadmin();
    }

    @Test(expected = NotAuthenticatedException.class)
    public void validateSysadminGivenNoCurrentUserPermissionsElevatedToUser() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.USER, () -> {
            permissionValidationService.validateSysadmin();
            return null;
        });
    }

    @Test(expected = NotAuthenticatedException.class)
    public void validateSysadminGivenNoCurrentUserPermissionsElevatedToAdmin() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.ADMIN, () -> {
            permissionValidationService.validateSysadmin();
            return null;
        });
    }

    @Test
    public void validateSysadminGivenNoCurrentUserPermissionsElevatedToSysadmin() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.SYSADMIN, () -> {
            permissionValidationService.validateSysadmin();
            return null;
        });
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void validateSystemAdminGivenNullI18nKey() throws NoAccessException {
        permissionValidationService.validateSysadmin((I18nKey) null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void validateSystemAdminGivenNullUserAndI18nKey() throws NoAccessException {
        permissionValidationService.validateSysadmin(null, null);
    }

    @Test
    public void validateSystemAdminGivenNoAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE,
                DEFAULT_OPERATION_KEY));

        permissionValidationService.validateSysadmin((UserKey) null);
    }

    @Test
    public void validateSystemAdminWithCustomOperationI18nGivenNoAuthenticatedUser() throws NoAccessException {
        expectedException.expect(NotAuthenticatedException.class);
        expectedException.expectMessage(containsInOrder(NotAuthenticatedException.DEFAULT_MESSAGE, TEST_OPERATION_KEY));

        permissionValidationService.validateSysadmin(null, newI18nKey(TEST_OPERATION_KEY));
    }

    @Test
    public void validateSystemAdminGivenAuthenticatedUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, SYSADMIN_KEY,
                DEFAULT_OPERATION_KEY));

        permissionValidationService.validateSysadmin(TEST_USER);
    }

    @Test
    public void validateSystemAdminWithCustomOperationI18nGivenAuthenticatedUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, SYSADMIN_KEY,
                TEST_OPERATION_KEY));

        permissionValidationService.validateSysadmin(TEST_USER, newI18nKey(TEST_OPERATION_KEY));
    }

    @Test
    public void validateSystemAdminGivenAdminUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, SYSADMIN_KEY,
                DEFAULT_OPERATION_KEY));

        setUpAdminUser();

        permissionValidationService.validateSysadmin(TEST_USER);
    }

    @Test
    public void validateSystemAdminWithCustomOperationI18nGivenAdminUser() throws NoAccessException {
        expectedException.expect(PermissionException.class);
        expectedException.expectMessage(containsInOrder(PermissionException.DEFAULT_MESSAGE, SYSADMIN_KEY,
                TEST_OPERATION_KEY));

        setUpAdminUser();

        permissionValidationService.validateSysadmin(TEST_USER, newI18nKey(TEST_OPERATION_KEY));
    }

    @Test
    public void validateSystemAdminGivenSystemAdminUser() throws NoAccessException {
        setUpSysAdminUser();

        permissionValidationService.validateSysadmin(TEST_USER);
    }

    @Test(expected = NotAuthenticatedException.class)
    public void validateSysadminGivenNoUserPermissionsElevatedToSysadmin() throws Exception {
        elevatedPermissions.executeAs(PermissionLevel.SYSADMIN, () -> {
            permissionValidationService.validateSysadmin((UserKey) null);
            return null;
        });
    }


    private void setUpCurrentUser() {
        when(userManager.getRemoteUserKey()).thenReturn(TEST_USER);
    }

    private void setUpAdminUser() {
        when(userManager.isAdmin(TEST_USER)).thenReturn(true);
    }

    private void setUpSysAdminUser() {
        when(userManager.isAdmin(TEST_USER)).thenReturn(true);
        when(userManager.isSystemAdmin(TEST_USER)).thenReturn(true);
    }
}
