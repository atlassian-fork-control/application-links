package com.atlassian.applinks.ui.auth;

import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.applinks.ui.XsrfProtectedServlet;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SysAdminFilterTest {
    @Mock
    AdminUIAuthenticator uiAuthenticator;
    @Mock
    I18nResolver i18nResolver;
    @Mock
    LoginUriProvider loginUriProvider;
    @Mock
    ApplicationProperties applicationProperties;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;

    SysAdminFilter filter;

    @Before
    public void setUp() {
        filter = new SysAdminFilter(uiAuthenticator, i18nResolver, loginUriProvider, applicationProperties);

        when(request.getContextPath()).thenReturn("http://localhost");
        when(request.getServletPath()).thenReturn("path");
        when(i18nResolver.getText("applinks.error.only.sysadmin.operation")).thenReturn("sysadmin only");

        when(applicationProperties.getBaseUrl(any())).thenReturn("http://localhost/");
        when(loginUriProvider.getLoginUriForRole(any(), any(), any())).thenReturn(URIUtil.uncheckedToUri("http://localhost"));
    }

    @Test
    public void verifyRequestFromLinkCreationWizardWillResultInError() throws Exception {
        when(request.getHeader(XsrfProtectedServlet.OVERRIDE_HEADER_NAME)).thenReturn(XsrfProtectedServlet.OVERRIDE_HEADER_VALUE);
        filter.handleAccessDenied(request, response);
        verify(response).sendError(HttpServletResponse.SC_FORBIDDEN, "sysadmin only");
    }

    @Test
    public void verifyRequestFromBrowserWillResultInRedirection() throws Exception {
        when(request.getHeader(XsrfProtectedServlet.OVERRIDE_HEADER_NAME)).thenReturn(null);
        filter.handleAccessDenied(request, response);
        verify(response).sendRedirect(any());
    }
}