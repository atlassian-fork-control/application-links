package com.atlassian.applinks.ui;

import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.applinks.ui.velocity.VelocityContextFactory;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ListEntityLinksServletTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private MessageFactory messageFactory;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private WebResourceManager webResourceManager;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private InternalHostApplication internalHostApplication;
    @Mock
    private DocumentationLinker documentationLinker;
    @Mock
    private LoginUriProvider loginUriProvider;
    @Mock
    private AdminUIAuthenticator adminUIAuthenticator;
    @Mock
    private VelocityContextFactory velocityContextFactory;
    @Mock
    private EntityLinksContextFactory entityLinksContextFactory;
    @Mock
    private AppLinkPluginUtil appLinkPluginUtil;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private WebSudoManager webSudoManager;
    @Mock
    private XsrfTokenAccessor xsrfTokenAccessor;
    @Mock
    private XsrfTokenValidator xsrfTokenValidator;
    @Mock
    private EventPublisher eventPublisher;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;

    @InjectMocks
    private ListEntityLinksServlet listEntityLinksServlet;

    @Test
    public void shouldRenderSoyTemplate() throws ServletException, IOException {
        String moduleKey = "module-key";
        Map<String, Object> context = Collections.emptyMap();
        String projectKey = "proj";
        String type = "c.a.a.BitBucketType";
        PrintWriter writer = mock(PrintWriter.class);

        when(request.getMethod()).thenReturn("GET");
        when(response.getWriter()).thenReturn(writer);
        when(request.getPathInfo()).thenReturn(type + "/" + projectKey);
        when(appLinkPluginUtil.completeModuleKey("entitylinks-react-ui")).thenReturn(moduleKey);
        when(entityLinksContextFactory.createContext(type,projectKey)).thenReturn(context);

        listEntityLinksServlet.service(request, response);

        verify(soyTemplateRenderer).render(writer, moduleKey,
                "applinks.internal.entitylinks.entitylinkPage", context);
    }
}