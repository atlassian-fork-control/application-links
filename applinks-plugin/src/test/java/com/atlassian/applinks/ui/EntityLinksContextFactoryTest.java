package com.atlassian.applinks.ui;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationTypeVisitor;
import com.atlassian.applinks.api.application.bitbucket.BitbucketApplicationType;
import com.atlassian.applinks.api.application.bitbucket.BitbucketProjectEntityType;
import com.atlassian.applinks.api.application.fecru.FishEyeCrucibleProjectEntityType;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.api.application.jira.JiraProjectEntityType;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.rest.applink.data.RestApplinkDataProviders;
import com.atlassian.applinks.spi.link.MutatingEntityLinkService;
import com.atlassian.applinks.test.mock.MockI18nResolver;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.ImmutableMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EntityLinksContextFactoryTest {

    private static final String PROJECT_KEY = "tcejorp";
    private static final String ENTITY_TYPE_ID = "bucketFullOfBits";

    @InjectMocks
    private EntityLinksContextFactory factory;
    @Mock
    private InternalHostApplication internalHostApplication;
    @Mock
    private InternalTypeAccessor typeAccessor;
    @Mock
    private MutatingEntityLinkService mutatingEntityLinkService;
    @Mock
    private ApplicationLinkService applicationLinkService;
    @Mock
    private RestApplinkDataProviders dataProviders;
    @Spy
    private I18nResolver i18nResolver = new MockI18nResolver();
    @Spy
    private MessageFactory messageFactory = new MessageFactory(i18nResolver);

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void createContextAddsMetaForBitbucket() throws IOException {
        when(internalHostApplication.doesEntityExist(any(), any())).thenReturn(true);
        when(internalHostApplication.canManageEntityLinksFor(any())).thenReturn(true);
        BitbucketApplicationType applicationType = mock(BitbucketApplicationType.class);
        when(applicationType.getI18nKey()).thenReturn("app.type.i18n");
        doAnswer(invocation -> {
            ApplicationTypeVisitor visitor = invocation.getArgument(0);
            return visitor.visit(applicationType);
        }).when(applicationType).accept(any());
        when(internalHostApplication.getType()).thenReturn(applicationType);
        when(typeAccessor.loadEntityType(ENTITY_TYPE_ID)).thenReturn(mock(BitbucketProjectEntityType.class));

        Map<String, Object> context = factory.createContext(ENTITY_TYPE_ID, PROJECT_KEY);

        assertEquals(context.get("meta"), ImmutableMap.<String, Object>builder()
                .put("projectKey", PROJECT_KEY)
                .put("decorator", "bitbucket.project.settings")
                .put("activeTab", "project-settings-entity-links")
                .build());
    }

    @Test
    public void createContextAddsMetaForJira() throws IOException {
        when(internalHostApplication.doesEntityExist(any(), any())).thenReturn(true);
        when(internalHostApplication.canManageEntityLinksFor(any())).thenReturn(true);
        JiraApplicationType applicationType = mock(JiraApplicationType.class);
        when(applicationType.getI18nKey()).thenReturn("app.type.i18n");
        doAnswer(invocation -> {
            ApplicationTypeVisitor visitor = invocation.getArgument(0);
            return visitor.visit(applicationType);
        }).when(applicationType).accept(any());
        when(internalHostApplication.getType()).thenReturn(applicationType);
        when(typeAccessor.loadEntityType("jira")).thenReturn(mock(JiraProjectEntityType.class));

        Map<String, Object> context = factory.createContext("jira", PROJECT_KEY);

        assertEquals(context.get("meta"), ImmutableMap.<String, Object>builder()
                .put("projectKey", PROJECT_KEY)
                .put("decorator", "admin")
                .put("admin.active.section", "atl.jira.proj.config/projectgroup4")
                .put("admin.active.tab", "view_project_links")
                .build());
    }
    
    @Test
    public void createContextChecksIfEntityDoesNotExists() throws IOException {
        thrown.expectMessage("No entity exists with key kej of type crowd");

        factory.createContext("crowd", "kej");
    }

    @Test
    public void createContextNotPermitted() throws IOException {
        when(typeAccessor.loadEntityType("fecru")).thenReturn(mock(FishEyeCrucibleProjectEntityType.class));
        when(internalHostApplication.doesEntityExist(any(), any())).thenReturn(true);
        when(internalHostApplication.canManageEntityLinksFor(any())).thenReturn(false);

        thrown.expectMessage("applinks.entity.list.no.manage.permission");

        factory.createContext("fecru", "key");
    }
}