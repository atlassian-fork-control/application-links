package com.atlassian.applinks.test.matchers;

import com.atlassian.applinks.internal.authentication.AuthorisationUriAware;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import java.net.URI;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.Matchers.is;

/**
 * @since 5.0
 */
public final class AuthorisationUriAwareMatchers {
    private AuthorisationUriAwareMatchers() {
        throw new UnsupportedOperationException("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<AuthorisationUriAware> withAuthorisationUri(@Nullable URI expectedUri) {
        return withAuthorisationUriThat(is(expectedUri));
    }

    @Nonnull
    public static Matcher<AuthorisationUriAware> withAuthorisationUriThat(
            @Nonnull Matcher<URI> authorisationUriMatcher) {
        checkNotNull(authorisationUriMatcher, "authorisationUriMatcher");
        return new FeatureMatcher<AuthorisationUriAware, URI>(authorisationUriMatcher, "authorisationUri",
                "authorisation URI that") {
            @Override
            protected URI featureValueOf(AuthorisationUriAware actual) {
                return actual.getAuthorisationUriGenerator().getAuthorisationURI();
            }
        };
    }

}
