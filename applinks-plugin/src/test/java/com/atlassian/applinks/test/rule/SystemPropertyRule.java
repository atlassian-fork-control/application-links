package com.atlassian.applinks.test.rule;

import com.google.common.collect.Sets;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Set;
import javax.annotation.concurrent.NotThreadSafe;

/**
 * Allows to set system properties of choice, which will be removed after the test has finished.
 *
 * @since 4.3
 */
@NotThreadSafe
public class SystemPropertyRule extends TestWatcher {
    private final Set<String> propertyKeys = Sets.newHashSet();

    public void setProperty(String key, String value) {
        propertyKeys.add(key);
        System.setProperty(key, value);
    }

    @Override
    protected void finished(Description description) {
        for (String key : propertyKeys) {
            System.clearProperty(key);
        }
    }
}
