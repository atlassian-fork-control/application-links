package com.atlassian.applinks.test.mock;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.when;

public class ApplinkHelperMocks {
    private final ApplinkHelper applinkHelper;

    public ApplinkHelperMocks(@Nonnull ApplinkHelper applinkHelper) {
        this.applinkHelper = requireNonNull(applinkHelper, "applinkHelper");
    }

    public void setUpExistingApplink(@Nonnull TestApplinkIds testApplink, @Nonnull ApplicationLink link) {
        setUpExistingApplink(testApplink.applicationId(), link);
    }

    public void setUpExistingApplink(@Nonnull ApplicationId id, @Nonnull ApplicationLink link) {
        checkNotNull(id, "id");
        checkNotNull(link, "link");

        try {
            when(applinkHelper.getApplicationLink(id)).thenReturn(link);
        } catch (NoSuchApplinkException e) {
            throw new AssertionError("Not a Mockito mock: " + applinkHelper);
        }
    }

    public void setUpNonExistingApplink(@Nonnull TestApplinkIds testApplink) {
        setUpNonExistingApplink(testApplink.applicationId());
    }

    public void setUpNonExistingApplink(@Nonnull ApplicationId id) {
        checkNotNull(id, "id");

        try {
            when(applinkHelper.getApplicationLink(id)).thenThrow(new NoSuchApplinkException("No such applink: " + id));
        } catch (NoSuchApplinkException e) {
            throw new AssertionError("Not a Mockito mock: " + applinkHelper);
        }
    }
}
