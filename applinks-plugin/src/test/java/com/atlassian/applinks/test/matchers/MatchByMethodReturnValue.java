package com.atlassian.applinks.test.matchers;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.mockito.ArgumentMatcher;

import static org.mockito.ArgumentMatchers.argThat;

public class MatchByMethodReturnValue {
    public static class HasGetterWithValueMatcher<C, V> implements ArgumentMatcher<C> {

        private final String getterName;
        private final V getterValue;

        /**
         * @param getterName  the name of the getter to check.
         * @param getterValue the value of the getter to match on.
         * @param clazz       the class that owns the getter.
         */
        public HasGetterWithValueMatcher(final Class<C> clazz, final String getterName, final V getterValue) {
            this.getterName = getterName;
            this.getterValue = getterValue;
        }

        @Override
        public boolean matches(Object argument) {
            try {
                Method method = argument.getClass().getMethod(this.getterName);
                Object methodValue = method.invoke(argument);

                if (!methodValue.getClass().equals(getPropClass())) {
                    return false;
                }

                V value = (V) methodValue;

                if (getterValue == null && value != null) {
                    return false;
                } else if (getterValue instanceof Object[]) {
                    return Arrays.equals((Object[]) getterValue, (Object[]) value);
                } else {
                    return getterValue.equals(value);
                }
            } catch (Exception e) {
                return false;
            }
        }

        private Class<V> getPropClass() {
            return (Class<V>) getterValue.getClass();
        }

        @Override
        public String toString() {
            return String.format("<Has Getter '%s' of value '%s' matcher>", this.getterName, this.getterValue);
        }
    }

    /**
     * Match on the return value of the specified method in instances of the specified class.
     *
     * @param <C>
     * @param <V>         the value of the getter to filter by.
     * @param owningClass the class of the object we want to check
     * @param getterName  the name of the getter to filter by
     * @param getterValue
     */
    public static <C, V> C hasMethodWithReturnValue(String getterName, V getterValue, Class<C> owningClass) {
        return argThat(new HasGetterWithValueMatcher<>(owningClass, getterName, getterValue));
    }
}