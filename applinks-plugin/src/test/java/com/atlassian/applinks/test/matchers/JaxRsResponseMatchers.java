package com.atlassian.applinks.test.matchers;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.Matchers.is;

public final class JaxRsResponseMatchers {
    private JaxRsResponseMatchers() {
        throw new UnsupportedOperationException("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<Response> withEntity(@Nonnull Object expectedEntity) {
        checkNotNull(expectedEntity, "entity");

        return withEntityThat(is(expectedEntity));
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static Matcher<Response> withEntityThat(@Nonnull Matcher<?> entityMatcher) {
        checkNotNull(entityMatcher, "entityMatcher");

        return new FeatureMatcher<Response, Object>((Matcher) entityMatcher, "entity", "entity") {
            @Override
            protected Object featureValueOf(Response response) {
                return response.getEntity();
            }
        };
    }

    @Nonnull
    public static Matcher<Response> withStatus(@Nonnull Response.Status expectedStatus) {
        checkNotNull(expectedStatus, "expectedStatus");

        return withStatusThat(is(expectedStatus));
    }

    @Nonnull
    public static Matcher<Response> withStatusThat(@Nonnull Matcher<Response.Status> statusMatcher) {
        checkNotNull(statusMatcher, "statusMatcher");

        return new FeatureMatcher<Response, Response.Status>(statusMatcher, "status", "status") {
            @Override
            protected Response.Status featureValueOf(Response response) {
                return Response.Status.fromStatusCode(response.getStatus());
            }
        };
    }
}
