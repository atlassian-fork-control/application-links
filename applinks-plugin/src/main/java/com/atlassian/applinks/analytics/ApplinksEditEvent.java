package com.atlassian.applinks.analytics;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.application.ApplicationTypes;

import javax.annotation.Nonnull;

@EventName("applinks.edit")
public class ApplinksEditEvent {
    private final String applicationId;
    private final String applicationType;

    private ApplinksEditEvent(final Builder builder) {
        this.applicationId = builder.applicationId;
        this.applicationType = builder.applicationType;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public static class Builder {
        private final String applicationType;
        private final String applicationId;

        public Builder(@Nonnull final ApplicationLink applink) {
            applicationId = applink.getId().get();
            applicationType = ApplicationTypes.resolveApplicationTypeId(applink.getType());
        }

        public ApplinksEditEvent build() {
            return new ApplinksEditEvent(this);
        }
    }
}
