package com.atlassian.applinks.analytics;

import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.event.api.EventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ApplinkStatusPublisher {

    private static final Logger log = LoggerFactory.getLogger(ApplinkStatusPublisher.class);

    private final ReadOnlyApplicationLinkService readOnlyApplicationLinkService;
    private final EventPublisher eventPublisher;
    private final ApplinkStatusEventBuilderFactory applinkStatusEventBuilderFactory;

    @Autowired
    public ApplinkStatusPublisher(
            final ReadOnlyApplicationLinkService readOnlyApplicationLinkService,
            final EventPublisher eventPublisher,
            final ApplinkStatusEventBuilderFactory applinkStatusEventBuilderFactory) {
        this.readOnlyApplicationLinkService = readOnlyApplicationLinkService;
        this.eventPublisher = eventPublisher;
        this.applinkStatusEventBuilderFactory = applinkStatusEventBuilderFactory;
    }

    public void publishApplinkStatus() {

        final ApplinkStatusEventBuilderFactory.Builder builder = applinkStatusEventBuilderFactory.createBuilder();

        readOnlyApplicationLinkService.getApplicationLinks().forEach(builder::addApplink);

        eventPublisher.publish(builder.buildMainEvent());
        for (ApplinkStatusEventBuilderFactory.ApplinkStatusApplinkEvent event: builder.buildApplinkEvents()) {
            eventPublisher.publish(event);
        }
    }
}
