package com.atlassian.applinks.analytics;

import com.atlassian.analytics.api.annotations.EventName;

@EventName("applinks.view.entitylinks.admin")
public class EntityLinksAdminViewEvent {
    private final String typeId;
    private final String entityKey;

    public EntityLinksAdminViewEvent(String typeId, String entityKey) {
        this.typeId = typeId;
        this.entityKey = entityKey;
    }

    public String getTypeId() {
        return typeId;
    }

    public String getEntityKey() {
        return entityKey;
    }
}
