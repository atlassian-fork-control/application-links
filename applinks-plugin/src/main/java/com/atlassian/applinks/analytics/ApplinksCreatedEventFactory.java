package com.atlassian.applinks.analytics;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.sal.api.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import static java.util.Objects.requireNonNull;


@ParametersAreNonnullByDefault
public class ApplinksCreatedEventFactory {

    private final ApplicationProperties applicationProperties;
    private final InternalHostApplication internalHostApplication;

    public enum EVENT_STATUS {
        FAILURE,
        WARNING,
        SUCCESS
    }

    public enum FAILURE_REASON {
        ALREADY_CONFIGURED,
        ALREADY_CONFIGURED_UNDER_DIFFERENT_URL,
        INVALID_URL,
        LINK_TO_SELF,
        NO_DOUBLE_SLASHES,
        NO_RESPONSE,
        NULL_MANIFEST,
        REDIRECT,
        TYPE_NOT_INSTALLED
    }

    @Autowired
    public ApplinksCreatedEventFactory(
            final ApplicationProperties applicationProperties,
            final InternalHostApplication internalHostApplication
    ) {

        this.applicationProperties = applicationProperties;
        this.internalHostApplication = internalHostApplication;
    }

    public ApplinksCreatedEvent createFailEvent(FAILURE_REASON reason) {
        return createEvent(EVENT_STATUS.FAILURE, reason.name());
    }

    public ApplinksCreatedEvent createWarningEvent(FAILURE_REASON reason) {
        return createEvent(EVENT_STATUS.WARNING, reason.name());
    }

    public ApplinksCreatedEvent createSuccessEvent() {
        return createEvent(EVENT_STATUS.SUCCESS, null);
    }

    private ApplinksCreatedEvent createEvent(EVENT_STATUS status, @Nullable String reasonName) {
        return new ApplinksCreatedEvent(
                applicationProperties.getPlatformId(),
                internalHostApplication.getId().get(),
                reasonName,
                status.name()
        );
    }

    @EventName("applinks.created")
    @ParametersAreNonnullByDefault
    static class ApplinksCreatedEvent {

        @Nonnull
        private final String applicationId;
        @Nonnull
        private final String product;
        @Nullable
        private final String reason;
        @Nonnull
        private final String status;

        public ApplinksCreatedEvent(String product, String applicationId,
                                    @Nullable String reason, String status) {
            this.applicationId = requireNonNull(applicationId, "applicationId can't be null");
            this.product = requireNonNull(product, "product can't be null");
            this.reason = reason;
            this.status = requireNonNull(status, "status can't be null");
        }

        @Nonnull
        public String getApplicationId() {
            return applicationId;
        }

        @Nonnull
        public String getProduct() {
            return product;
        }

        @Nullable
        public String getReason() {
            return reason;
        }

        @Nonnull
        public String getStatus() {
            return status;
        }
    }
}
