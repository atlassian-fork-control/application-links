package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthAutoConfigurator;
import com.atlassian.applinks.internal.common.exception.RemoteMigrationInvalidResponseException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.migration.AuthenticationConfig;
import com.atlassian.applinks.internal.migration.AuthenticationStatus;
import com.atlassian.applinks.internal.status.DefaultLegacyConfig;
import com.atlassian.applinks.internal.status.LegacyConfig;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.io.IOException;

public class RemoteMigrationHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteMigrationHelper.class);

    // The following constants created to aid in unit testing. They should have next to zero performance impact.
    private static final TryWithAuthentication DISABLE_TRUSTED = DisableTrustedApp.INSTANCE;
    private static final TryWithAuthentication QUERY_SYS_ADMIN_ACCESS = QuerySysAdminAccess.INSTANCE;
    private static final QueryLegacyAuthentication.Factory QUERY_LEGACY_AUTHENTICATION_FACTORY = new QueryLegacyAuthentication.Factory();
    private static final OAuthConfigMigrator.Factory OAUTH_CONFIGURATOR_FACTORY = new OAuthConfigMigrator.Factory();

    private final OAuthAutoConfigurator oauthAutoConfigurator;
    private final ServiceExceptionFactory serviceExceptionFactory;
    private final InternalHostApplication internalHostApplication;

    @Autowired
    public RemoteMigrationHelper(final OAuthAutoConfigurator oauthAutoConfigurator,
                                 final ServiceExceptionFactory serviceExceptionFactory,
                                 final InternalHostApplication internalHostApplication) {
        this.oauthAutoConfigurator = oauthAutoConfigurator;
        this.serviceExceptionFactory = serviceExceptionFactory;
        this.internalHostApplication = internalHostApplication;
    }

    public boolean disableRemoteTrustedApp(@Nonnull final ApplicationLink applicationLink) throws RemoteMigrationInvalidResponseException {
        return tryWithAuthentications(applicationLink, DISABLE_TRUSTED);
    }

    public boolean hasSysAdminAccess(@Nonnull final ApplicationLink applicationLink) throws RemoteMigrationInvalidResponseException {
        return tryWithAuthentications(applicationLink, QUERY_SYS_ADMIN_ACCESS);
    }

    @Nonnull
    public LegacyConfig getLegacyConfig(@Nonnull final ApplicationLink applicationLink) throws RemoteMigrationInvalidResponseException {
        QueryLegacyAuthentication queryLegacyAuthentication = QUERY_LEGACY_AUTHENTICATION_FACTORY.getInstance();
        if (tryWithAuthentications(applicationLink, queryLegacyAuthentication)) {
            return queryLegacyAuthentication.getLegacyConfig();
        }
        return new DefaultLegacyConfig();
    }

    @Nonnull
    public AuthenticationStatus migrate(@Nonnull final ApplicationLink applicationLink, @Nonnull final AuthenticationStatus status) throws RemoteMigrationInvalidResponseException {
        OAuthConfig incomingOAuth = createMigrationOAuthConfig(status.incoming());
        OAuthConfig outgoingOAuth = createMigrationOAuthConfig(status.outgoing());
        final OAuthConfigMigrator oauthConfigMigrator = OAUTH_CONFIGURATOR_FACTORY.getInstance(oauthAutoConfigurator, incomingOAuth, outgoingOAuth);
        if (tryWithAuthentications(applicationLink, oauthConfigMigrator)) {
            return status.outgoing(status.outgoing().oauth(outgoingOAuth)).incoming(status.incoming().oauth(incomingOAuth));
        }
        return status;
    }

    private OAuthConfig createMigrationOAuthConfig(AuthenticationConfig config) {
        if (config.isOAuthConfigured()) {
            return config.getOAuthConfig();
        }
        if (config.isTrustedConfigured()) {
            return OAuthConfig.createOAuthWithImpersonationConfig();
        }
        if (config.isBasicConfigured()) {
            return OAuthConfig.createDefaultOAuthConfig();
        }
        return config.getOAuthConfig();
    }

    @VisibleForTesting
    protected boolean tryWithAuthentications(final ApplicationLink applicationLink,
                                             final TryWithAuthentication tryWithAuthentication) throws RemoteMigrationInvalidResponseException {
        final ApplicationId localApplicationId = internalHostApplication.getId();
        try {
            final TryWithCredentials tryWithCredentials = new TryWithCredentials(tryWithAuthentication);
            boolean addedRemotely = tryWithCredentials.execute(applicationLink, localApplicationId, BasicAuthenticationProvider.class);
            addedRemotely = addedRemotely || tryWithCredentials.execute(applicationLink, localApplicationId, TrustedAppsAuthenticationProvider.class);
            addedRemotely = addedRemotely || tryWithCredentials.execute(applicationLink, localApplicationId, TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);
            addedRemotely = addedRemotely || tryWithCredentials.execute(applicationLink, localApplicationId, TwoLeggedOAuthAuthenticationProvider.class);
            addedRemotely = addedRemotely || tryWithCredentials.execute(applicationLink, localApplicationId, OAuthAuthenticationProvider.class);
            return addedRemotely;
        } catch (ResponseException | IOException | AuthenticationConfigurationException ex) {
            throw serviceExceptionFactory.create(RemoteMigrationInvalidResponseException.class, ex);
        }
    }

}
