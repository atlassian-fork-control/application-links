package com.atlassian.applinks.internal.web;

import com.atlassian.applinks.analytics.ApplinksEditEvent;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.exception.InvalidEntityStateException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.web.AbstractApplinksServiceServlet;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.support.ApplinkStatusValidationService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.applinks.internal.common.i18n.I18nKey.newI18nKey;
import static java.util.Collections.singletonList;

/**
 * Servlet backing the V3 "Edit Application Link" screen.
 *
 * @since 5.0
 */
public class EditApplinkServlet extends AbstractApplinksServiceServlet {
    private static final String WEB_RESOURCE_CONTEXT = "applinks.edit.v3";

    private final ApplinkStatusValidationService applinkStatusValidationService;
    private final EventPublisher eventPublisher;
    private final PermissionValidationService permissionValidationService;
    private final ServiceExceptionFactory serviceExceptionFactory;
    private final WebApplinkHelper webApplinkHelper;
    private final WebSudoManager webSudoManager;

    public EditApplinkServlet(AppLinkPluginUtil appLinkPluginUtil,
                              I18nResolver i18nResolver,
                              InternalHostApplication internalHostApplication,
                              LoginUriProvider loginProvider,
                              SoyTemplateRenderer soyTemplateRenderer,
                              PageBuilderService pageBuilderService,
                              ApplinkStatusValidationService applinkStatusValidationService,
                              EventPublisher eventPublisher,
                              PermissionValidationService permissionValidationService,
                              ServiceExceptionFactory serviceExceptionFactory,
                              WebApplinkHelper webApplinkHelper,
                              WebSudoManager webSudoManager) {
        super(appLinkPluginUtil, i18nResolver, internalHostApplication, loginProvider, soyTemplateRenderer,
                pageBuilderService);
        this.applinkStatusValidationService = applinkStatusValidationService;
        this.eventPublisher = eventPublisher;
        this.permissionValidationService = permissionValidationService;
        this.serviceExceptionFactory = serviceExceptionFactory;
        this.webApplinkHelper = webApplinkHelper;
        this.webSudoManager = webSudoManager;
    }

    @Override
    protected void doServiceGet(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response)
            throws ServiceException, IOException, ServletException {
        try {
            webSudoManager.willExecuteWebSudoRequest(request);
            permissionValidationService.validateAdmin();
            ApplicationLink applink = webApplinkHelper.getApplicationLink(request);
            validateApplink(applink);
            publishAnalytics(applink);
            render(request, response, "page-applink-edit", "applinks.page.applink.edit.main", ImmutableMap.of(
                    "applink", applink,
                    "applinkId", applink.getId().get(),
                    PARAM_TITLE, applink.getName()));
        } catch (WebSudoSessionException wse) {
            webSudoManager.enforceWebSudoProtection(request, response);
        }
    }

    @Nonnull
    @Override
    protected Iterable<String> webResourceContexts() {
        return singletonList(WEB_RESOURCE_CONTEXT);
    }

    @Nullable
    @Override
    protected String getPageInitializer(@Nonnull HttpServletRequest request) {
        return "applinks/page/applink-edit";
    }


    private void publishAnalytics(ApplicationLink applink) {
        eventPublisher.publish(new ApplinksEditEvent.Builder(applink).build());
    }

    private void validateApplink(ApplicationLink applink) throws ServiceException {
        try {
            applinkStatusValidationService.checkEditable(applink);
        } catch (ApplinkStatusException e) {
            String statusError = i18nResolver.getText(e.getType().getI18nKey());
            throw serviceExceptionFactory.raise(InvalidEntityStateException.class,
                    newI18nKey("applinks.service.error.applink.edit.invalidstatus", applink.getId(), statusError));
        }
    }
}
