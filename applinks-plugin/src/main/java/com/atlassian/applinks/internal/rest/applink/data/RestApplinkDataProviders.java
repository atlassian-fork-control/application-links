package com.atlassian.applinks.internal.rest.applink.data;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;

/**
 * @since 5.0
 */
public class RestApplinkDataProviders {
    final Map<String, RestApplinkDataProvider> providers;

    @Autowired
    public RestApplinkDataProviders(List<RestApplinkDataProvider> providers) {
        this.providers = mapProviders(providers);
    }

    private Map<String, RestApplinkDataProvider> mapProviders(Iterable<RestApplinkDataProvider> providers) {
        ImmutableMap.Builder<String, RestApplinkDataProvider> mappedProviders = ImmutableMap.builder();
        for (RestApplinkDataProvider provider : providers) {
            for (String key : provider.getSupportedKeys()) {
                mappedProviders.put(key, provider);
            }
        }
        return mappedProviders.build();
    }

    public RestApplinkDataProvider getProvider(@Nonnull String key) {
        return providers.get(key);
    }
}
