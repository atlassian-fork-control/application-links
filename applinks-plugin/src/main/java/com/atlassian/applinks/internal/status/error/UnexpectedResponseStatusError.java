package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.internal.common.net.HttpUtils;
import com.atlassian.sal.api.net.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @see ApplinkErrorType#UNEXPECTED_RESPONSE_STATUS
 * @since 5.0
 */
public class UnexpectedResponseStatusError extends AbstractResponseApplinkError {
    public UnexpectedResponseStatusError(@Nonnull Response response) {
        super(response);
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS;
    }

    @Nullable
    @Override
    public String getDetails() {
        // e.g. "400: Bad request"
        return HttpUtils.toStatusString(getStatusCode());
    }
}
