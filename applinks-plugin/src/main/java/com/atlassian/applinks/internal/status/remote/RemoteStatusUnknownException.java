package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkErrors;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.google.common.base.Predicate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.SocketException;

import static com.atlassian.applinks.internal.status.error.ApplinkErrors.toDetails;

/**
 * Raised if the remote connection required to fetch Applink status has failed due to an unknown problem.
 *
 * @see ApplinkErrorType#UNKNOWN
 * @since 4.3
 */
public class RemoteStatusUnknownException extends ApplinkStatusException {
    private static final Package JAVA_NET_PACKAGE = SocketException.class.getPackage();

    private static final Predicate<Throwable> IN_JAVA_NET_PACKAGE = new Predicate<Throwable>() {
        @Override
        public boolean apply(Throwable input) {
            return JAVA_NET_PACKAGE.equals(input.getClass().getPackage());
        }
    };

    public RemoteStatusUnknownException() {
    }

    public RemoteStatusUnknownException(@Nullable String message) {
        super(message);
    }

    public RemoteStatusUnknownException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return ApplinkErrorType.UNKNOWN;
    }

    @Nullable
    @Override
    public String getDetails() {
        Throwable networkCause = ApplinkErrors.findCauseMatching(this, IN_JAVA_NET_PACKAGE);
        if (networkCause != null) {
            return toDetails(networkCause);
        }
        if (getCause() != null) {
            return toDetails(getCause());
        } else {
            return null;
        }
    }
}
