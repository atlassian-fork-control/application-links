package com.atlassian.applinks.internal.status.error;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;


/**
 * Simple {@link ApplinkError} with no extra properties.
 *
 * @since 5.0
 */
public final class SimpleApplinkError implements ApplinkError {
    private final ApplinkErrorType errorType;
    private final String error;

    public SimpleApplinkError(@Nonnull ApplinkErrorType errorType) {
        this(errorType, null);
    }

    public SimpleApplinkError(@Nonnull ApplinkErrorType errorType, @Nullable String error) {
        this.errorType = requireNonNull(errorType, "errorType");
        this.error = error;
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return errorType;
    }

    @Nullable
    @Override
    public String getDetails() {
        return error;
    }

    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplinkErrorVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
