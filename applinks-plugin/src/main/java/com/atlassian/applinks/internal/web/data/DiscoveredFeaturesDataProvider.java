package com.atlassian.applinks.internal.web.data;

import com.atlassian.applinks.internal.common.json.JacksonJsonableMarshaller;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.feature.FeatureDiscoveryService;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import java.util.Collections;
import java.util.Set;

/**
 * Injects information about the discovered features into the client-side.
 * For more information how context providers work check the linked documentation.
 *
 * @see <a href="https://developer.atlassian.com/docs/advanced-topics/adding-data-providers-to-your-plugin">
 * Data provider documentation</a>
 * @since 4.3
 */
public class DiscoveredFeaturesDataProvider implements WebResourceDataProvider {
    private final UserManager userManager;
    private final FeatureDiscoveryService featureDiscoveryService;

    public DiscoveredFeaturesDataProvider(UserManager userManager, FeatureDiscoveryService featureDiscoveryService) {
        this.userManager = userManager;
        this.featureDiscoveryService = featureDiscoveryService;
    }

    @Override
    public Jsonable get() {
        return JacksonJsonableMarshaller.INSTANCE.marshal(getDiscoveredFeatureKeys());
    }

    private Set<String> getDiscoveredFeatureKeys() {
        UserKey userKey = userManager.getRemoteUserKey();
        try {
            return (userKey != null) ?
                    featureDiscoveryService.getAllDiscoveredFeatureKeys() :
                    Collections.<String>emptySet();
        } catch (NotAuthenticatedException e) {
            // should never happen
            throw new IllegalStateException("FeatureDiscoveryService threw authentication exception despite existing " +
                    "user context: " + userKey, e);
        }
    }
}
