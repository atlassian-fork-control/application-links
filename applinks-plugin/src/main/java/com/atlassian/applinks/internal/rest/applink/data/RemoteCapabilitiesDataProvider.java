package com.atlassian.applinks.internal.rest.applink.data;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.rest.model.capabilities.RestRemoteApplicationCapabilities;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.lang.String.format;

/**
 * Provides remote capabilities information about Applinks.
 *
 * @see com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService
 * @since 5.0
 */
public class RemoteCapabilitiesDataProvider extends AbstractRestApplinkDataProvider {
    public static final String CAPABILITIES = "capabilities";
    public static final String APPLICATION_VERSION = "applicationVersion";

    private final RemoteCapabilitiesService remoteCapabilitiesService;

    @Autowired
    public RemoteCapabilitiesDataProvider(RemoteCapabilitiesService remoteCapabilitiesService) {
        super(ImmutableSet.of(CAPABILITIES, APPLICATION_VERSION));
        this.remoteCapabilitiesService = remoteCapabilitiesService;
    }

    @Nullable
    @Override
    public Object provide(@Nonnull String key, @Nonnull ApplicationLink applink) throws ServiceException {
        if (CAPABILITIES.equals(key)) {
            return new RestRemoteApplicationCapabilities(remoteCapabilitiesService.getCapabilities(applink));
        } else if (APPLICATION_VERSION.equals(key)) {
            ApplicationVersion version = remoteCapabilitiesService.getCapabilities(applink).getApplicationVersion();
            return version != null ? version.getVersionString() : null;
        }
        throw new IllegalArgumentException(format("Unsupported key: '%s'", key));
    }
}
