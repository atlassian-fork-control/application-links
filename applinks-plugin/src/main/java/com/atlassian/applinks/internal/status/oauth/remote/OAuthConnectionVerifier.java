package com.atlassian.applinks.internal.status.oauth.remote;

import com.atlassian.annotations.Internal;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.permission.Unrestricted;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;

import javax.annotation.Nonnull;

/**
 * Responsible for verifying that an OAuth connection to a specific application link is functional.
 *
 * @since 5.0
 */
@Internal
@Unrestricted("Internal component, services using this component should take care of enforcing appropriate permission level")
public interface OAuthConnectionVerifier {

    /**
     * Verify that there is a functional OAuth connection to {@code applicationLink}. If there is no outgoing OAuth configured for given
     * link, no check will be performed. Note: this will in most cases result in a network request to the remote linked application.
     *
     * @param applicationLink application link to verify
     * @throws ApplinkStatusException if there was any network problem, including any OAuth issue. This will <i>most commonly</i> be a
     *                                {@link com.atlassian.applinks.internal.status.error.ApplinkErrorCategory#NETWORK_ERROR network error}, but could in some cases be
     *                                some other error type.
     */
    void verifyOAuthConnection(@Nonnull ApplicationLink applicationLink) throws ApplinkStatusException;
}
