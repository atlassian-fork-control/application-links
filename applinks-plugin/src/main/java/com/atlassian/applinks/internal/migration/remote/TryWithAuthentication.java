package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.annotations.Internal;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.sal.api.net.ResponseException;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * This allows execution of many remote migration actions such as deleting trusted configuration using different
 * Authentication Providers until we find one with remote admin access or sysadmin access
 *
 * @see com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider
 * @see com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider
 */
@Internal
abstract class TryWithAuthentication {
    public static int TIME_OUT_IN_SECONDS = 15;

    protected abstract boolean execute(@Nonnull final ApplicationLink applicationLink,
                                       @Nonnull final ApplicationId applicationId,
                                       @Nonnull final ApplicationLinkRequestFactory factory)
            throws IOException, CredentialsRequiredException, ResponseException, AuthenticationConfigurationException;

    /**
     * Execute a remote action with a given provider.
     *
     * @param applicationLink the application link
     * @param applicationId   the local application id
     * @param providerClass   the authentication provider class
     * @return true if the action succeeded.
     * @throws IOException                          network errors.
     * @throws CredentialsRequiredException         our provider has no credentials
     * @throws ResponseException                    the remote action fails due to network errors
     * @throws AuthenticationConfigurationException the remote action fails to configure authentications
     */
    boolean execute(@Nonnull final ApplicationLink applicationLink,
                    @Nonnull final ApplicationId applicationId,
                    @Nonnull final Class<? extends AuthenticationProvider> providerClass)
            throws IOException, CredentialsRequiredException, ResponseException, AuthenticationConfigurationException {
        final ApplicationLinkRequestFactory factory = applicationLink.createAuthenticatedRequestFactory(providerClass);
        return factory != null && execute(applicationLink, applicationId, factory);
    }
}

