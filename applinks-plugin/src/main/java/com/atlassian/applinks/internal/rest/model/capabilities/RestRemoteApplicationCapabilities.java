package com.atlassian.applinks.internal.rest.model.capabilities;

import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.internal.rest.model.ReadOnlyRestRepresentation;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkError;

import javax.annotation.Nonnull;

/**
 * @since 5.0
 */
public class RestRemoteApplicationCapabilities extends BaseRestEntity
        implements ReadOnlyRestRepresentation<RemoteApplicationCapabilities> {
    public static final String APPLICATION_VERSION = "applicationVersion";
    public static final String APPLINKS_VERSION = "applinksVersion";
    public static final String CAPABILITIES = "capabilities";
    public static final String ERROR = "error";

    @SuppressWarnings("unused") // for Jackson
    public RestRemoteApplicationCapabilities() {
    }

    @SuppressWarnings("ConstantConditions")
    public RestRemoteApplicationCapabilities(@Nonnull RemoteApplicationCapabilities capabilities) {
        putAs(APPLICATION_VERSION, capabilities.getApplicationVersion(), RestApplicationVersion.class);
        putAs(APPLINKS_VERSION, capabilities.getApplinksVersion(), RestApplicationVersion.class);
        put(CAPABILITIES, capabilities.getCapabilities());
        if (capabilities.hasError()) {
            put(ERROR, capabilities.getError().accept(new RestApplinkError.Visitor()));
        }
    }
}
