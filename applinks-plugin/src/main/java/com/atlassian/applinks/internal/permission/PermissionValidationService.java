package com.atlassian.applinks.internal.permission;

import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.sal.api.user.UserKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Provides API to enforce user/admin/sysadmin permissions for logged in users.
 *
 * @since 4.3
 */
public interface PermissionValidationService {
    void validateAuthenticated() throws NotAuthenticatedException;

    void validateAuthenticated(@Nonnull I18nKey operationI18n) throws NotAuthenticatedException;

    void validateAuthenticated(@Nullable UserKey user) throws NotAuthenticatedException;

    void validateAuthenticated(@Nullable UserKey user, @Nonnull I18nKey operationI18n) throws NotAuthenticatedException;


    void validateAdmin() throws NoAccessException;

    void validateAdmin(@Nonnull I18nKey operationI18n) throws NoAccessException;

    void validateAdmin(@Nullable UserKey user) throws NoAccessException;

    void validateAdmin(@Nullable UserKey user, @Nonnull I18nKey operationI18n) throws NoAccessException;


    void validateSysadmin() throws NoAccessException;

    void validateSysadmin(@Nonnull I18nKey operationI18n) throws NoAccessException;

    void validateSysadmin(@Nullable UserKey user) throws NoAccessException;

    void validateSysadmin(@Nullable UserKey user, @Nonnull I18nKey operationI18n) throws NoAccessException;
}
