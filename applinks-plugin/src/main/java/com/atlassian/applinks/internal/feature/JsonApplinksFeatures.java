package com.atlassian.applinks.internal.feature;

import com.atlassian.applinks.internal.rest.model.BaseRestEntity;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.rest.model.BaseRestEntity.createSingleFieldEntity;

/**
 * Factory of JSON representations of {@link ApplinksFeatures}.
 */
public final class JsonApplinksFeatures {
    private final ApplinksFeatureService featureService;

    public JsonApplinksFeatures(ApplinksFeatureService featureService) {
        this.featureService = featureService;
    }

    @Nonnull
    public BaseRestEntity isEnabled(@Nonnull ApplinksFeatures feature) {
        return createSingleFieldEntity(feature.name(), featureService.isEnabled(feature));
    }

    @Nonnull
    public BaseRestEntity allFeatures() {
        BaseRestEntity.Builder allFeatures = new BaseRestEntity.Builder();
        for (ApplinksFeatures feature : ApplinksFeatures.values()) {
            allFeatures.add(feature.name(), featureService.isEnabled(feature));
        }

        return allFeatures.build();
    }
}
