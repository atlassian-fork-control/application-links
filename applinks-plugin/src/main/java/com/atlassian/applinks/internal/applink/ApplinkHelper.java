package com.atlassian.applinks.internal.applink;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.permission.Unrestricted;
import com.atlassian.applinks.spi.link.MutableApplicationLink;

import javax.annotation.Nonnull;

/**
 * Simple component for retrieving and manipulating Applinks and throwing appropriate service exceptions in case the
 * requested operation fails.
 *
 * @since 4.3
 */
@Unrestricted("Clients using this component are responsible for enforcing appropriate permissions")
public interface ApplinkHelper {
    /**
     * Get application link by ID, or otherwise raise a service exception.
     *
     * @param id ID to find the application link by
     * @return application link
     * @throws NoSuchApplinkException if the retrieval fails for any reason
     */
    @Nonnull
    ApplicationLink getApplicationLink(@Nonnull ApplicationId id) throws NoSuchApplinkException;

    /**
     * Get application link by ID that is suitable for update, or otherwise raise a service exception.
     *
     * @param id ID to find the application link by
     * @return mutable application link
     * @throws NoSuchApplinkException if the retrieval fails for any reason
     * @see MutableApplicationLink
     */
    @Nonnull
    MutableApplicationLink getMutableApplicationLink(@Nonnull ApplicationId id) throws NoSuchApplinkException;

    /**
     * Get read-only, immutable application link by ID, or otherwise raise a service exception. This is the most
     * performant applink version if the intended usage is read-only.
     *
     * @param id ID to find the application link by
     * @return read-only application link
     * @throws NoSuchApplinkException if the retrieval fails for any reason
     * @see ReadOnlyApplicationLink
     */
    @Nonnull
    ReadOnlyApplicationLink getReadOnlyApplicationLink(@Nonnull ApplicationId id) throws NoSuchApplinkException;

    /**
     * Make application link with {@code id} primary, or otherwise raise a service exception
     *
     * @param id application ID
     * @throws NoSuchApplinkException if {@code id} does not correspond to an existing application link or the
     *                                application link cannot be retrieved
     */
    void makePrimary(@Nonnull ApplicationId id) throws NoSuchApplinkException;
}
