package com.atlassian.applinks.internal.status.support;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.exception.InvalidArgumentException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.migration.AuthenticationMigrationService;
import com.atlassian.applinks.internal.migration.AuthenticationStatus;
import com.atlassian.applinks.internal.migration.OAuthMigrationUtil;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.error.SimpleApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;


/**
 * Default implementation of {@link ApplinkStatusValidationService}.
 *
 * @since 5.0
 */
public class DefaultApplinkStatusValidationService implements ApplinkStatusValidationService {
    // Minimum supported remote application version for status UI
    private static final ApplicationVersion MIN_VERSION = ApplicationVersion.parse("4.0.13");

    private final ApplinkCompatibilityVerifier compatibilityVerifier;
    private final RemoteCapabilitiesService remoteCapabilitiesService;
    private final AuthenticationMigrationService authenticationMigrationService;

    @Autowired
    public DefaultApplinkStatusValidationService(ApplinkCompatibilityVerifier compatibilityVerifier,
                                                 AuthenticationMigrationService authenticationMigrationService,
                                                 RemoteCapabilitiesService remoteCapabilitiesService) {
        this.compatibilityVerifier = compatibilityVerifier;
        this.authenticationMigrationService = authenticationMigrationService;
        this.remoteCapabilitiesService = remoteCapabilitiesService;
    }

    @Override
    public void checkLocalCompatibility(@Nonnull final ApplicationLink link) {
        ApplinkErrorType compatibilityError = compatibilityVerifier.verifyLocalCompatibility(link);
        if (compatibilityError != null) {
            throw new StatusError(compatibilityError);
        }
    }

    @Override
    public void checkVersionCompatibility(@Nonnull final ApplicationLink link) throws NoAccessException {
        RemoteApplicationCapabilities capabilities = getCapabilities(link);
        // if applinksVersion == null it probably means the remote application cannot be contacted, has invalid version
        // or is non-Atlassian, therefore checking version compatibility is not applicable
        if (capabilities.getApplinksVersion() != null) {
            if (capabilities.getApplinksVersion().compareTo(MIN_VERSION) < 0) {
                throw new StatusError(ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE);
            }
        }
    }

    @Override
    public void checkOAuthSupportedCompatibility(@Nonnull final ApplinkOAuthStatus status) {
        checkOAuthSupported(status.getIncoming());
        checkOAuthSupported(status.getOutgoing());
    }

    @Override
    public void checkOAuthMismatch(@Nonnull final ApplinkOAuthStatus localStatus, @Nonnull final ApplinkOAuthStatus remoteStatus) {
        requireNonNull(localStatus, "localAuthentication");
        requireNonNull(localStatus, "remoteAuthentication");

        if (!localStatus.matches(remoteStatus)) {
            throw new StatusError(ApplinkErrorType.AUTH_LEVEL_MISMATCH);
        }
    }

    @Override
    public void checkLegacyAuthentication(@Nonnull final ApplicationLink link,
                                          @Nonnull final ApplinkOAuthStatus localStatus,
                                          @Nonnull final ApplinkOAuthStatus remoteStatus) throws NoSuchApplinkException, NoAccessException {
        requireNonNull(localStatus, "localStatus");
        requireNonNull(remoteStatus, "remoteStatus");
        final AuthenticationStatus authenticationStatus = authenticationMigrationService.getAuthenticationMigrationStatus(link, localStatus);

        if (authenticationStatus.incoming().hasLegacy() || authenticationStatus.outgoing().hasLegacy()) {
            boolean hasRemoteSysAdminAccess = authenticationMigrationService.hasRemoteSysAdminAccess(link);
            boolean oAuthConfigured = OAuthMigrationUtil.isOAuthConfigured(authenticationStatus);

            if (hasRemoteSysAdminAccess) {
                if (oAuthConfigured) {
                    throw new StatusError(ApplinkErrorType.LEGACY_REMOVAL);
                }
                throw new StatusError(ApplinkErrorType.LEGACY_UPDATE);
            }
            if (oAuthConfigured) {
                // We want to automatically remove basic when there is no trusted configured
                if (authenticationStatus.outgoing().isBasicConfigured() && noTrustedConfigured(authenticationStatus)) {
                    throw new StatusError(ApplinkErrorType.LEGACY_REMOVAL);
                }
                if (authenticationStatus.incoming().isTrustedConfigured() && noBasicConfigured(authenticationStatus) && noMigrationApi(link)) {
                    throw new StatusError(ApplinkErrorType.MANUAL_LEGACY_REMOVAL_WITH_OLD_EDIT);
                }
                throw new StatusError(ApplinkErrorType.MANUAL_LEGACY_REMOVAL);
            }
            throw new StatusError(ApplinkErrorType.MANUAL_LEGACY_UPDATE);
        }
    }

    private boolean noMigrationApi(final ApplicationLink link) throws NoAccessException {
        return !getCapabilities(link).getCapabilities().contains(ApplinksCapabilities.MIGRATION_API);
    }

    private boolean noBasicConfigured(final AuthenticationStatus authenticationStatus) {
        return !(authenticationStatus.outgoing().isBasicConfigured() || authenticationStatus.incoming().isBasicConfigured());
    }

    private boolean noTrustedConfigured(final AuthenticationStatus authenticationStatus) {
        return !(authenticationStatus.outgoing().isTrustedConfigured() || authenticationStatus.incoming().isTrustedConfigured());
    }

    @Override
    public void checkDisabled(@Nullable final ApplinkOAuthStatus localStatus, @Nullable final ApplinkOAuthStatus remoteStatus) {
        final ApplinkOAuthStatus local = localStatus == null ? ApplinkOAuthStatus.OFF : localStatus;
        if (local.equals(remoteStatus) && local.equals(ApplinkOAuthStatus.OFF)) {
            throw new StatusError(ApplinkErrorType.DISABLED);
        }
    }

    @Override
    public void checkEditable(@Nonnull ApplicationLink applink)
            throws NoAccessException, ApplinkStatusException {
        // check for all incompatible statuses, as they will prevent us from being able to
        // successfully edit the Applink
        checkLocalCompatibility(applink);
        checkVersionCompatibility(applink);
    }

    private void checkOAuthSupported(@Nonnull final OAuthConfig config) {
        if (config.isEnabled()) {
            if (!config.isTwoLoEnabled()) {
                throw new StatusError(ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED);
            }
        }
    }

    private RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationLink link) throws NoAccessException {
        try {
            return remoteCapabilitiesService.getCapabilities(link, 1, TimeUnit.HOURS);
        } catch (InvalidArgumentException e) {
            throw new AssertionError("Unexpected InvalidArgumentException", e);
        }
    }

    static final class StatusError extends SimpleApplinkStatusException {
        public StatusError(ApplinkErrorType errorType) {
            super(errorType);
        }

        @Override
        public synchronized Throwable fillInStackTrace() {
            // skip filling in the stack trace as it's expensive
            return this;
        }
    }
}
