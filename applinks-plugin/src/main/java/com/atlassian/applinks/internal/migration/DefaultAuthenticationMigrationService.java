package com.atlassian.applinks.internal.migration;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.auth.trusted.ApplinksTrustedApps;
import com.atlassian.applinks.internal.common.exception.*;
import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.applinks.internal.migration.remote.RemoteMigrationHelper;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.status.DefaultLegacyConfig;
import com.atlassian.applinks.internal.status.LegacyConfig;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.OAuthStatusService;
import com.atlassian.applinks.internal.status.oauth.remote.RemoteOAuthStatusService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class DefaultAuthenticationMigrationService implements AuthenticationMigrationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAuthenticationMigrationService.class);
    private final ApplinkHelper applinkHelper;
    private final OAuthStatusService oAuthStatusService;
    private final ServiceExceptionFactory serviceExceptionFactory;
    private final PermissionValidationService permissionValidationService;
    private final AuthenticationConfigurationManager authConfigManager;
    private final RemoteOAuthStatusService remoteOAuthStatusService;
    private final RemoteMigrationHelper remoteMigrationHelper;

    @Autowired
    public DefaultAuthenticationMigrationService(final ApplinkHelper applinkHelper,
                                                 final OAuthStatusService oAuthStatusService,
                                                 final RemoteOAuthStatusService remoteOAuthStatusService,
                                                 final RemoteMigrationHelper remoteMigrationHelper,
                                                 final ServiceExceptionFactory serviceExceptionFactory,
                                                 final PermissionValidationService permissionValidationService,
                                                 final AuthenticationConfigurationManager authenticationConfigurationManager) {
        this.applinkHelper = applinkHelper;
        this.oAuthStatusService = oAuthStatusService;
        this.remoteOAuthStatusService = remoteOAuthStatusService;
        this.remoteMigrationHelper = remoteMigrationHelper;
        this.serviceExceptionFactory = serviceExceptionFactory;
        this.permissionValidationService = permissionValidationService;
        this.authConfigManager = authenticationConfigurationManager;
    }

    @Override
    @Nonnull
    public AuthenticationStatus migrateToOAuth(@Nonnull final ApplicationId applicationId) throws ServiceException {
        requireNonNull(applicationId, "applicationId");
        permissionValidationService.validateAdmin();

        final ApplicationLink link = applinkHelper.getApplicationLink(applicationId);
        AuthenticationStatus configs = getAuthenticationMigrationStatus(link);
        final ApplinkOAuthStatus remoteOAuthStatus = fetchRemoteOAuthStatus(link);

        MigrationState migrationState = new MigrationState(link, configs, remoteOAuthStatus, hasRemoteSysAdminAccess(link));
        migrationState = migrate(migrationState);

        migrationState = migrationState.remoteOAuthStatus(fetchRemoteOAuthStatus(link));
        final Boolean disableRemoteTrustedAttempted = removeInbound(migrationState);
        migrationState = removeOutbound(migrationState, disableRemoteTrustedAttempted);

        return migrationState.authenticationStatus;
    }

    @Override
    public boolean hasRemoteSysAdminAccess(@Nonnull final ApplicationLink link) throws NoSuchApplinkException, NoAccessException {
        try {
            requireNonNull(link, "link");
            permissionValidationService.validateAdmin();
            return remoteMigrationHelper.hasSysAdminAccess(link);
        } catch (RemoteMigrationInvalidResponseException e) {
            LOGGER.debug("Failed to check for remote sys admin access ", e);
            return false;
        }
    }

    private MigrationState migrate(final MigrationState migrationState) throws ServiceException {
        if (migrationState.authenticationStatus.outgoing().isTrustedConfigured() || migrationState.authenticationStatus.incoming().isTrustedConfigured()) {
            permissionValidationService.validateSysadmin();
        }

        // we want oauth between local and remote to match. If they mismatch, they should be detected and handled by the user prior to migration.
        if (oAuthMismatch(migrationState)) {
            throw serviceExceptionFactory.create(InvalidEntityStateException.class, I18nKey.newI18nKey("applinks.service.error.oauth.mismatch.during.migration"));
        }

        // we don't want to perform expensive migration if the OAuths are already configured
        if (migrationState.remoteSysAdmin && !OAuthMigrationUtil.isOAuthConfigured(migrationState.authenticationStatus)) {
            return migrationState.authenticationStatus(remoteMigrationHelper.migrate(migrationState.link, migrationState.authenticationStatus));
        }

        return migrationState;
    }

    private boolean oAuthMismatch(final MigrationState migrationState) {
        final ApplinkOAuthStatus localOAuthStatus = new ApplinkOAuthStatus(migrationState.authenticationStatus.incoming().getOAuthConfig(),
                migrationState.authenticationStatus.outgoing().getOAuthConfig());
        return !localOAuthStatus.matches(migrationState.remoteOAuthStatus);
    }

    private LegacyConfig getLocalLegacyConfig(@Nonnull final ApplicationLink link) {
        return new DefaultLegacyConfig().basic(authConfigManager.isConfigured(link.getId(), BasicAuthenticationProvider.class))
                .trusted(authConfigManager.isConfigured(link.getId(), TrustedAppsAuthenticationProvider.class));
    }

    private MigrationState removeOutbound(final MigrationState migrationState, final Boolean disableRemoteTrustedAttempted) throws ServiceException {
        AuthenticationConfig outgoing = migrationState.authenticationStatus.outgoing();
        if (outgoing.isOAuthConfigured() && migrationState.remoteOAuthStatus.getIncoming().isEnabled()) {
            if (outgoing.isTrustedConfigured()) {
                final boolean remoteTrustedRemoved = disableRemoteTrustedAttempted == null ? remoteMigrationHelper.disableRemoteTrustedApp(migrationState.link) : disableRemoteTrustedAttempted;
                checkTrustedRemoved(migrationState, remoteTrustedRemoved);
                if (remoteTrustedRemoved) {
                    removeProvider(migrationState.link, TrustedAppsAuthenticationProvider.class);
                }
                outgoing = outgoing.trustedConfigured(!remoteTrustedRemoved);
            }
            removeProvider(migrationState.link, BasicAuthenticationProvider.class);
            outgoing = outgoing.basicConfigured(false);
        }
        return migrationState.authenticationStatus(migrationState.authenticationStatus.outgoing(outgoing));
    }

    private void checkTrustedRemoved(final MigrationState migrationState, boolean remoteTrustedRemoved) throws ServiceException {
        // if we cannot disable remote trusted even if we're sys admin on the remote application, something went wrong.
        if (migrationState.remoteSysAdmin && !remoteTrustedRemoved) {
            throw serviceExceptionFactory.create(InvalidEntityStateException.class, I18nKey.newI18nKey("applinks.service.error.remote.disable.trusted.invalid"));
        }
    }

    private Boolean removeInbound(final MigrationState migrationState) throws ServiceException {
        AuthenticationConfig incoming = migrationState.authenticationStatus.incoming();
        if (incoming.isOAuthConfigured() && migrationState.remoteOAuthStatus.getOutgoing().isEnabled()) {
            if (incoming.isTrustedConfigured()) {
                boolean remoteTrustedDisabled = remoteMigrationHelper.disableRemoteTrustedApp(migrationState.link);
                checkTrustedRemoved(migrationState, remoteTrustedDisabled);
                return remoteTrustedDisabled;
            }
        }

        //we have not attempted to disable remote trusted.
        return null;
    }

    private AuthenticationStatus getAuthenticationMigrationStatus(@Nonnull final ApplicationLink link,
                                                                  @Nonnull final ApplinkOAuthStatus localOAuthStatus,
                                                                  @Nonnull final LegacyConfig remoteLegacyConfig) {
        requireNonNull(localOAuthStatus, "localOAuthStatus");
        requireNonNull(remoteLegacyConfig, "remoteLegacyConfig");
        final LegacyConfig localLegacyConfig = getLocalLegacyConfig(link);

        AuthenticationConfig outgoing = new AuthenticationConfig(
                localOAuthStatus.getOutgoing(),
                localLegacyConfig.isBasicConfigured(),
                localLegacyConfig.isTrustedConfigured());

        AuthenticationConfig incoming = new AuthenticationConfig(
                localOAuthStatus.getIncoming(),
                remoteLegacyConfig.isBasicConfigured(),
                link.getProperty(ApplinksTrustedApps.PROPERTY_TRUSTED_APPS_INCOMING_ID) != null);

        return new AuthenticationStatus(incoming, outgoing);
    }

    @Override
    @Nonnull
    public AuthenticationStatus getAuthenticationMigrationStatus(@Nonnull final ApplicationLink link,
                                                                 @Nonnull final ApplinkOAuthStatus localOAuthStatus) throws NoSuchApplinkException, NoAccessException {
        requireNonNull(localOAuthStatus, "localOAuthStatus");
        permissionValidationService.validateAdmin();
        LegacyConfig remoteLegacyConfig;
        try {
            remoteLegacyConfig = remoteMigrationHelper.getLegacyConfig(link);
        } catch (RemoteMigrationInvalidResponseException e) {
            remoteLegacyConfig = new DefaultLegacyConfig();
        }

        return getAuthenticationMigrationStatus(link, localOAuthStatus, remoteLegacyConfig);
    }

    private AuthenticationStatus getAuthenticationMigrationStatus(final ApplicationLink link) throws RemoteMigrationInvalidResponseException {
        final ApplinkOAuthStatus oauthStatus = oAuthStatusService.getOAuthStatus(link);
        return getAuthenticationMigrationStatus(link, oauthStatus, remoteMigrationHelper.getLegacyConfig(link));
    }

    private ApplinkOAuthStatus fetchRemoteOAuthStatus(final ApplicationLink link) {
        try {
            return remoteOAuthStatusService.fetchOAuthStatus(link);
        } catch (ApplinkStatusException | NoAccessException ex) {
            LOGGER.debug("Failed to fetch remote oauth status.", ex);
            return ApplinkOAuthStatus.OFF;
        }
    }

    private void removeProvider(final ApplicationLink applicationLink, Class<? extends AuthenticationProvider> providerClass) {
        authConfigManager.unregisterProvider(applicationLink.getId(), providerClass);
    }

    private class MigrationState {
        final ApplicationLink link;
        final AuthenticationStatus authenticationStatus;
        final boolean remoteSysAdmin;
        final ApplinkOAuthStatus remoteOAuthStatus;

        public MigrationState(@Nonnull final ApplicationLink link,
                              @Nonnull final AuthenticationStatus authenticationStatus,
                              @Nonnull final ApplinkOAuthStatus remoteOAuthStatus,
                              boolean remoteSysAdmin) {
            this.link = Objects.requireNonNull(link, "link");
            this.authenticationStatus = Objects.requireNonNull(authenticationStatus, "authenticationStatus");
            this.remoteOAuthStatus = Objects.requireNonNull(remoteOAuthStatus, "remoteOAuthStatus");
            this.remoteSysAdmin = remoteSysAdmin;
        }

        @Nonnull
        public MigrationState authenticationStatus(@Nonnull final AuthenticationStatus authenticationStatus) {
            Objects.requireNonNull(authenticationStatus, "authenticationStatus");
            return new MigrationState(link, authenticationStatus, remoteOAuthStatus, remoteSysAdmin);
        }

        @Nonnull
        public MigrationState remoteOAuthStatus(@Nonnull final ApplinkOAuthStatus remoteOAuthStatus) {
            Objects.requireNonNull(remoteOAuthStatus, "remoteOAuthStatus");
            return new MigrationState(link, authenticationStatus, remoteOAuthStatus, remoteSysAdmin);
        }
    }
}
