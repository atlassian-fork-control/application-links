package com.atlassian.applinks.internal.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.status.error.ApplinkError;

import java.util.EnumSet;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;


/**
 * @since 5.0
 */
public class RemoteCapabilitiesError implements RemoteApplicationCapabilities {
    private final ApplinkError error;

    public RemoteCapabilitiesError(@Nonnull ApplinkError error) {
        this.error = requireNonNull(error, "error");
    }

    @Nullable
    @Override
    public ApplicationVersion getApplicationVersion() {
        return null;
    }

    @Nullable
    @Override
    public ApplicationVersion getApplinksVersion() {
        return null;
    }

    @Nonnull
    @Override
    public Set<ApplinksCapabilities> getCapabilities() {
        return EnumSet.noneOf(ApplinksCapabilities.class);
    }

    @Nonnull
    @Override
    public ApplinkError getError() {
        return error;
    }

    @Override
    public boolean hasError() {
        return true;
    }
}
