package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Raised if the remote application is not linked to the local application initiating the request.
 *
 * @since 4.3
 */
public class NoRemoteApplinkException extends ApplinkStatusException implements ApplinkError {
    public NoRemoteApplinkException(@Nullable String message) {
        super(message);
    }

    public NoRemoteApplinkException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return ApplinkErrorType.NO_REMOTE_APPLINK;
    }

    @Nullable
    @Override
    public String getDetails() {
        return null;
    }
}
