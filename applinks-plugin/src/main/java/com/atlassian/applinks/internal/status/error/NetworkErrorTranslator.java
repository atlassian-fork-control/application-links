package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.net.ResponseContentException;
import com.atlassian.applinks.internal.status.remote.RemoteNetworkException;
import com.atlassian.applinks.internal.status.remote.RemoteStatusUnknownException;
import com.atlassian.applinks.internal.status.remote.ResponseApplinkStatusException;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseStatusException;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.List;

import static com.atlassian.applinks.internal.status.error.ApplinkErrorType.CONNECTION_REFUSED;
import static com.atlassian.applinks.internal.status.error.ApplinkErrorType.OAUTH_SIGNATURE_INVALID;
import static com.atlassian.applinks.internal.status.error.ApplinkErrorType.OAUTH_TIMESTAMP_REFUSED;
import static com.atlassian.applinks.internal.status.error.ApplinkErrorType.SSL_HOSTNAME_UNMATCHED;
import static com.atlassian.applinks.internal.status.error.ApplinkErrorType.SSL_UNMATCHED;
import static com.atlassian.applinks.internal.status.error.ApplinkErrorType.SSL_UNTRUSTED;
import static com.atlassian.applinks.internal.status.error.ApplinkErrorType.UNKNOWN_HOST;
import static com.atlassian.applinks.internal.status.error.ApplinkErrors.findCauseMatching;
import static com.atlassian.applinks.internal.status.error.ApplinkErrors.findCauseOfType;
import static com.atlassian.applinks.internal.status.error.OAuthErrorMatchers.matchOAuthProblem;
import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.instanceOf;

/**
 * Inspects arbitrary exceptions to match them to instances of {@link ApplinkStatusException} and {@link ApplinkError}.
 *
 * @see ApplinkErrorCategory#NETWORK_ERROR
 * @see ApplinkErrorCategory#UNKNOWN
 * @since 5.0
 */
public final class NetworkErrorTranslator {
    private static final List<RemoteExceptionMatcher> MATCHERS = ImmutableList.<RemoteExceptionMatcher>builder()
            // search for pre-existing applink errors first
            .add(ApplinkErrorMatcher.INSTANCE)

            // generic network problems
            .add(byTypeAndFuzzyMessageMatcher(CONNECTION_REFUSED, ConnectException.class,
                    "Connection refused"))
            .add(byTypeMatcher(UNKNOWN_HOST, UnknownHostException.class))
            .add(byTypeMatcher(SSL_UNTRUSTED, SSLHandshakeException.class))
            .add(byTypeMatcher(SSL_HOSTNAME_UNMATCHED, SSLPeerUnverifiedException.class))
            .add(byTypeAndFuzzyMessageMatcher(SSL_UNMATCHED, SSLException.class, "certificate"))

            // OAuth problems
            .add(matchOAuthProblem(OAUTH_TIMESTAMP_REFUSED, ApplinksOAuth.PROBLEM_TIMESTAMP_REFUSED))
            .add(matchOAuthProblem(OAUTH_SIGNATURE_INVALID, ApplinksOAuth.PROBLEM_SIGNATURE_INVALID))
            // fallback OAuth matcher, add more specific OAuth matchers _above_
            .add(OAuthErrorMatchers.fallback())

            // fallback matchers for unexpected response content/status, more specific matchers should be _above_ those
            .add(ResponseContentMatcher.INSTANCE)
            .add(ResponseStatusMatcher.INSTANCE)
            .build();

    private NetworkErrorTranslator() {
        //do not instantiate
    }

    /**
     * Match exception {@code e} to a corresponding {@code ApplinkError}. If no match is found,
     * {@link ApplinkErrorCategory#UNKNOWN unknown error is returned}
     *
     * @param e       exception to match
     * @param message optional message to feed to the returned error
     * @return {@code RemoteStatusException} matching {@code e}, or {@code RemoteStatusUnknownException}
     */
    @Nonnull
    public static ApplinkStatusException toApplinkErrorException(@Nonnull Throwable e, @Nullable String message) {
        for (RemoteExceptionMatcher matcher : MATCHERS) {
            if (matcher.matches(e)) {
                return matcher.createMatchingError(message, e);
            }
        }
        return new RemoteStatusUnknownException(message, e);
    }

    /**
     * Equivalent to {@link #toApplinkErrorException(Throwable, String)} but returns {@link ApplinkErrorType} to avoid
     * compiler warning for clients who are only interested in the error, and not exception to throw.
     *
     * @param e       exception to match
     * @param message optional message to feed to the returned error
     * @return {@code ApplinkErrorType} matching {@code e}, or the {@link ApplinkErrorType#UNKNOWN unknown error type}
     */
    @Nonnull
    public static ApplinkError toApplinkError(@Nonnull Throwable e, @Nullable String message) {
        return toApplinkErrorException(e, message);
    }

    private static Predicate<Throwable> withMessageMatching(final String expectedMessage) {
        return new Predicate<Throwable>() {
            @Override
            public boolean apply(Throwable error) {
                return expectedMessage.equalsIgnoreCase(error.getMessage());
            }
        };
    }

    private static Predicate<Throwable> withMessageContaining(final String expectedMessage) {
        return new Predicate<Throwable>() {
            @Override
            public boolean apply(Throwable error) {
                return error.getMessage() != null &&
                        error.getMessage().toLowerCase().contains(expectedMessage.toLowerCase());
            }
        };
    }

    private static RemoteExceptionMatcher byTypeMatcher(ApplinkErrorType applinkErrorType,
                                                        Class<? extends Throwable> expectedType) {
        return new ByTypeMatcher(applinkErrorType, expectedType);
    }

    private static RemoteExceptionMatcher byTypeAndExactMessageMatcher(ApplinkErrorType applinkErrorType,
                                                                       Class<? extends Throwable> expectedType,
                                                                       String expectedMessage) {
        return new ByTypeAndExactMessageMatcher(applinkErrorType, expectedType, expectedMessage);
    }

    private static RemoteExceptionMatcher byTypeAndFuzzyMessageMatcher(ApplinkErrorType applinkErrorType,
                                                                       Class<? extends Throwable> expectedType,
                                                                       String expectedMessage) {
        return new ByTypeAndFuzzyMessageMatcher(applinkErrorType, expectedType, expectedMessage);
    }

    interface RemoteExceptionMatcher {
        boolean matches(Throwable original);

        ApplinkStatusException createMatchingError(String message, Throwable original);
    }

    abstract static class NetworkExceptionMatcher implements RemoteExceptionMatcher {
        protected final ApplinkErrorType applinkErrorType;
        protected final Class<? extends Throwable> underlyingErrorType;

        protected NetworkExceptionMatcher(ApplinkErrorType applinkErrorType,
                                          Class<? extends Throwable> underlyingErrorType) {
            this.applinkErrorType = applinkErrorType;
            this.underlyingErrorType = underlyingErrorType;
        }

        public ApplinkStatusException createMatchingError(String message, Throwable original) {
            return new RemoteNetworkException(applinkErrorType, underlyingErrorType, message, original);
        }
    }

    static class ByTypeMatcher extends NetworkExceptionMatcher {
        protected final Class<? extends Throwable> expectedType;

        ByTypeMatcher(ApplinkErrorType applinkErrorType, Class<? extends Throwable> expectedType) {
            super(applinkErrorType, expectedType);
            this.expectedType = expectedType;
        }

        @Override
        @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
        public boolean matches(Throwable original) {
            return findCauseOfType(original, expectedType) != null;
        }
    }

    static class ByTypeAndExactMessageMatcher extends ByTypeMatcher {
        private final String expectedMessage;

        ByTypeAndExactMessageMatcher(ApplinkErrorType applinkErrorType, Class<? extends Throwable> expectedType,
                                     String expectedMessage) {
            super(applinkErrorType, expectedType);
            this.expectedMessage = expectedMessage;
        }

        @Override
        @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
        public boolean matches(Throwable original) {
            return findCauseMatching(original, and(
                    instanceOf(expectedType),
                    withMessageMatching(expectedMessage)
            )) != null;
        }
    }

    static class ByTypeAndFuzzyMessageMatcher extends ByTypeMatcher {
        private final String expectedMessage;

        ByTypeAndFuzzyMessageMatcher(ApplinkErrorType applinkErrorType, Class<? extends Throwable> expectedType,
                                     String expectedMessage) {
            super(applinkErrorType, expectedType);
            this.expectedMessage = expectedMessage;
        }

        @Override
        @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
        public boolean matches(Throwable original) {
            return findCauseMatching(original, and(
                    instanceOf(expectedType),
                    withMessageContaining(expectedMessage)
            )) != null;
        }
    }

    /**
     * Returns {@link ApplinkError} if found in the original stack trace.
     */
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    static class ApplinkErrorMatcher implements RemoteExceptionMatcher {
        static final ApplinkErrorMatcher INSTANCE = new ApplinkErrorMatcher();

        @Override
        public boolean matches(Throwable original) {
            return findApplinkError(original) != null;
        }

        @Override
        @SuppressWarnings("ConstantConditions")
        public ApplinkStatusException createMatchingError(String message, Throwable original) {
            return findApplinkError(original).accept(new ApplinkErrorExceptionFactory());
        }

        private ApplinkError findApplinkError(Throwable original) {
            return (ApplinkError) findCauseMatching(original, instanceOf(ApplinkError.class));
        }
    }

    /**
     * Converts {@code ResponseContentException} into a corresponding {@code UNEXPECTED_RESPONSE} error.
     */
    static class ResponseContentMatcher extends ByTypeMatcher {
        static final ResponseContentMatcher INSTANCE = new ResponseContentMatcher();

        public ResponseContentMatcher() {
            super(ApplinkErrorType.UNEXPECTED_RESPONSE, ResponseContentException.class);
        }

        @Override
        @SuppressWarnings("ConstantConditions")
        public ApplinkStatusException createMatchingError(String message, Throwable original) {
            ResponseContentException responseException = findCauseOfType(original, ResponseContentException.class);
            return new ResponseApplinkStatusException(
                    new UnexpectedResponseError(responseException.getResponse()), responseException);
        }
    }

    /**
     * Converts {@code ResponseStatusException} into a corresponding {@code UNEXPECTED_RESPONSE_STATUS} error.
     */
    static class ResponseStatusMatcher extends ByTypeMatcher {
        static final ResponseStatusMatcher INSTANCE = new ResponseStatusMatcher();

        public ResponseStatusMatcher() {
            super(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS, ResponseStatusException.class);
        }

        @Override
        @SuppressWarnings("ConstantConditions")
        public ApplinkStatusException createMatchingError(String message, Throwable original) {
            ResponseStatusException statusException = findCauseOfType(original, ResponseStatusException.class);
            Response response = statusException.getResponse();
            // response should generally always be available, but it's subject to well-behaved response handlers which
            // is sometimes outside of our control, so let's fall back to an "unknown" status in case there's no
            // response to figure it out from
            return response != null ?
                    new ResponseApplinkStatusException(new UnexpectedResponseStatusError(response), statusException) :
                    new SimpleApplinkStatusException(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS, "<UNKNOWN STATUS>", statusException);
        }
    }
}
