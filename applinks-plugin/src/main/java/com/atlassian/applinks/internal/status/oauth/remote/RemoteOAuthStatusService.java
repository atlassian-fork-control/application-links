package com.atlassian.applinks.internal.status.oauth.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Restricted;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;

import javax.annotation.Nonnull;

/**
 * Fetches the remote OAuth status from the linked application. Restricted to administrator users due to significant
 * network IO this API may produce.
 *
 * @since 4.3
 */
@Restricted(PermissionLevel.ADMIN)
public interface RemoteOAuthStatusService {
    /**
     * Retrieve remote OAuth configuration status for given Application {@code id}.
     *
     * @param id application ID to examine
     * @return remote OAuth config status of the applink with {@code id}
     * @throws NoSuchApplinkException if the corresponding applink could not be retrieved
     * @throws ApplinkStatusException if the operation failed
     * @throws NoAccessException      if user is non-admin
     * @see ApplinkStatusException and its subclasses for various possible reasons of the remote OAuth status failures
     */
    @Nonnull
    ApplinkOAuthStatus fetchOAuthStatus(@Nonnull ApplicationId id)
            throws NoSuchApplinkException, ApplinkStatusException, NoAccessException;

    /**
     * Retrieve remote OAuth configuration status of {@code link}.
     *
     * @param link applink to examine
     * @return remote OAuth config status of the applink
     * @throws ApplinkStatusException if the operation failed
     * @throws NoAccessException      if user is non-admin
     * @see ApplinkStatusException and its subclasses for various possible reasons of the remote OAuth status failures
     */
    @Nonnull
    ApplinkOAuthStatus fetchOAuthStatus(@Nonnull ApplicationLink link) throws ApplinkStatusException, NoAccessException;

}
