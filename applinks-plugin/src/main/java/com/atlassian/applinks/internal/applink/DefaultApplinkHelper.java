package com.atlassian.applinks.internal.applink;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class DefaultApplinkHelper implements ApplinkHelper {
    private final ApplicationLinkService applicationLinkService;
    private final MutatingApplicationLinkService mutatingApplicationLinkService;
    private final ReadOnlyApplicationLinkService readOnlyApplicationLinkService;
    private final ServiceExceptionFactory serviceExceptionFactory;

    @Autowired
    public DefaultApplinkHelper(ApplicationLinkService applicationLinkService,
                                MutatingApplicationLinkService mutatingApplicationLinkService,
                                ReadOnlyApplicationLinkService readOnlyApplicationLinkService,
                                ServiceExceptionFactory serviceExceptionFactory) {
        this.applicationLinkService = applicationLinkService;
        this.mutatingApplicationLinkService = mutatingApplicationLinkService;
        this.readOnlyApplicationLinkService = readOnlyApplicationLinkService;
        this.serviceExceptionFactory = serviceExceptionFactory;
    }

    @Override
    @Nonnull
    public ApplicationLink getApplicationLink(@Nonnull ApplicationId id) throws NoSuchApplinkException {
        requireNonNull(id, "id");
        try {
            ApplicationLink link = applicationLinkService.getApplicationLink(id);
            checkExists(id, link);
            return link;
        } catch (TypeNotInstalledException e) {
            throw typeNotInstalled(e);
        }
    }

    @Nonnull
    @Override
    public MutableApplicationLink getMutableApplicationLink(@Nonnull ApplicationId id) throws NoSuchApplinkException {
        requireNonNull(id, "id");
        try {
            MutableApplicationLink link = mutatingApplicationLinkService.getApplicationLink(id);
            checkExists(id, link);
            return link;
        } catch (TypeNotInstalledException e) {
            throw typeNotInstalled(e);
        }
    }

    @Nonnull
    @Override
    public ReadOnlyApplicationLink getReadOnlyApplicationLink(@Nonnull ApplicationId id) throws NoSuchApplinkException {
        requireNonNull(id, "id");
        ReadOnlyApplicationLink link = readOnlyApplicationLinkService.getApplicationLink(id);
        checkExists(id, link);
        return link;
    }

    @Override
    public void makePrimary(@Nonnull ApplicationId id) throws NoSuchApplinkException {
        requireNonNull(id, "id");
        try {
            mutatingApplicationLinkService.makePrimary(id);
        } catch (IllegalArgumentException e) {
            throw serviceExceptionFactory.raise(NoSuchApplinkException.class, id);
        } catch (TypeNotInstalledException e) {
            throw typeNotInstalled(e);
        }
    }

    private void checkExists(@Nonnull ApplicationId id, ReadOnlyApplicationLink link) throws NoSuchApplinkException {
        if (link == null) {
            throw serviceExceptionFactory.raise(NoSuchApplinkException.class, id);
        }
    }

    private NoSuchApplinkException typeNotInstalled(TypeNotInstalledException e) throws NoSuchApplinkException {
        return serviceExceptionFactory.raise(NoSuchApplinkException.class,
                I18nKey.newI18nKey("applinks.service.error.nosuchentity.apptype", e.getName(), e.getType()), e);
    }
}
