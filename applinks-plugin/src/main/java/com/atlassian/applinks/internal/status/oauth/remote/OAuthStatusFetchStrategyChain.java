package com.atlassian.applinks.internal.status.oauth.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.ImmutableList;

import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Arrays.asList;

/**
 * Chain of {@link OAuthStatusFetchStrategy fetch strategies} applied consecutively until one succeeds (returns a
 * non-{@code null} status).
 *
 * @since 5.0
 */
public class OAuthStatusFetchStrategyChain implements OAuthStatusFetchStrategy {
    private final List<OAuthStatusFetchStrategy> strategies;

    public OAuthStatusFetchStrategyChain(@Nonnull List<OAuthStatusFetchStrategy> strategies) {
        this.strategies = ImmutableList.copyOf(strategies);
    }

    public OAuthStatusFetchStrategyChain(@Nonnull OAuthStatusFetchStrategy... strategies) {
        this(asList(strategies));
    }

    @Nullable
    @Override
    public ApplinkOAuthStatus fetch(@Nonnull ApplicationId localId, @Nonnull ApplicationLink applink)
            throws ApplinkStatusException, ResponseException {
        for (OAuthStatusFetchStrategy strategy : strategies) {
            ApplinkOAuthStatus result = strategy.fetch(localId, applink);
            if (result != null) {
                return result;
            }
        }
        return null;
    }
}
