package com.atlassian.applinks.internal.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.status.error.ApplinkError;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public class DefaultRemoteCapabilities implements RemoteApplicationCapabilities {
    private final ApplicationVersion applicationVersion;
    private final ApplicationVersion applinksVersion;
    private final Set<ApplinksCapabilities> capabilities;
    private final ApplinkError error;

    private DefaultRemoteCapabilities(Builder builder) {
        this.applicationVersion = builder.applicationVersion;
        this.applinksVersion = builder.applinksVersion;
        this.capabilities = builder.capabilities;
        this.error = builder.error;
    }

    @Nullable
    @Override
    public ApplicationVersion getApplicationVersion() {
        return applicationVersion;
    }

    @Nullable
    @Override
    public ApplicationVersion getApplinksVersion() {
        return applinksVersion;
    }

    @Nonnull
    @Override
    public Set<ApplinksCapabilities> getCapabilities() {
        return capabilities;
    }

    @Nullable
    @Override
    public ApplinkError getError() {
        return error;
    }

    @Override
    public boolean hasError() {
        return error != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final DefaultRemoteCapabilities that = (DefaultRemoteCapabilities) o;
        return Objects.equals(applicationVersion, that.applicationVersion) &&
                Objects.equals(applinksVersion, that.applinksVersion) &&
                Objects.equals(capabilities, that.capabilities) &&
                Objects.equals(error, that.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(applicationVersion, applinksVersion, capabilities, error);
    }

    public static class Builder {
        private ApplicationVersion applicationVersion;
        private ApplicationVersion applinksVersion;

        private Set<ApplinksCapabilities> capabilities = EnumSet.noneOf(ApplinksCapabilities.class);
        private ApplinkError error;

        public Builder() {
        }

        public Builder(@Nonnull RemoteApplicationCapabilities that) {
            this.applicationVersion = that.getApplicationVersion();
            this.applinksVersion = that.getApplinksVersion();
            this.capabilities = that.getCapabilities().isEmpty() ?
                    EnumSet.noneOf(ApplinksCapabilities.class) :
                    EnumSet.copyOf(that.getCapabilities());
            this.error = that.getError();
        }

        @Nonnull
        public Builder applicationVersion(@Nullable ApplicationVersion applicationVersion) {
            this.applicationVersion = applicationVersion;

            return this;
        }

        @Nonnull
        public Builder applinksVersion(@Nullable ApplicationVersion applinksVersion) {
            this.applinksVersion = applinksVersion;

            return this;
        }

        @Nonnull
        public Builder capabilities(@Nonnull Set<ApplinksCapabilities> capabilities) {
            this.capabilities = requireNonNull(capabilities, "capabilities");

            return this;
        }

        @Nonnull
        public Builder error(@Nullable ApplinkError error) {
            this.error = error;

            return this;
        }

        @Nonnull
        public DefaultRemoteCapabilities build() {
            return new DefaultRemoteCapabilities(this);
        }
    }

}
