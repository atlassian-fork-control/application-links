package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntity;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntityListEntity;
import com.atlassian.applinks.internal.status.DefaultLegacyConfig;
import com.atlassian.applinks.internal.status.LegacyConfig;
import com.atlassian.applinks.internal.rest.RestUrlBuilder;
import com.atlassian.applinks.internal.rest.RestVersion;
import com.atlassian.applinks.internal.rest.client.AuthorisationUriAwareRequest;
import com.atlassian.applinks.internal.rest.client.RestRequestBuilder;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

class QueryLegacyAuthentication extends TryWithAuthentication {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryLegacyAuthentication.class);
    private DefaultLegacyConfig legacyConfig = new DefaultLegacyConfig();

    @Override
    protected boolean execute(@Nonnull final ApplicationLink applicationLink,
                              @Nonnull final ApplicationId applicationId,
                              @Nonnull final ApplicationLinkRequestFactory factory)
            throws IOException, CredentialsRequiredException, ResponseException, AuthenticationConfigurationException {
        return false;
    }

    @Override
    public boolean execute(@Nonnull final ApplicationLink applicationLink,
                           @Nonnull final ApplicationId applicationId,
                           @Nonnull final Class<? extends AuthenticationProvider> providerClass)
            throws IOException, CredentialsRequiredException, ResponseException {
        final RestUrlBuilder restUrlBuilder = new RestUrlBuilder()
                .module("applinks")
                .version(RestVersion.LATEST)
                .addPath("applicationlink")
                .addApplicationId(applicationId)
                .addPath("authentication/provider");
        final Optional<AuthorisationUriAwareRequest> request = new RestRequestBuilder(applicationLink)
                .authentication(providerClass)
                .methodType(Request.MethodType.GET)
                .url(restUrlBuilder)
                .buildOptional();
        if (!request.isPresent()) {
            return false;
        }
        final QueryLegacyAuthenticationHandler handler = new QueryLegacyAuthenticationHandler();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(Request.MethodType.GET.name() + " " + applicationLink.getRpcUrl() + restUrlBuilder.toString());
        }
        request.get().execute(handler);
        return handler.isSuccessful();
    }

    private class QueryLegacyAuthenticationHandler extends RemoteActionHandler {

        @Override
        public void handle(final Response response) throws ResponseException {
            super.handle(response);
            if (isSuccessful()) {
                final AuthenticationProviderEntityListEntity listEntity = response.getEntity(AuthenticationProviderEntityListEntity.class);
                final List<AuthenticationProviderEntity> providers = listEntity.getAuthenticationProviders();
                if (providers != null) {
                    for (AuthenticationProviderEntity provider : providers) {
                        if (BasicAuthenticationProvider.class.getName().equals(provider.getProvider())) {
                            legacyConfig = legacyConfig.basic(true);
                        }
                        if (TrustedAppsAuthenticationProvider.class.getName().equals(provider.getProvider())) {
                            legacyConfig = legacyConfig.trusted(true);
                        }
                    }
                }
            }
        }
    }

    public LegacyConfig getLegacyConfig() {
        return legacyConfig;
    }

    protected static class Factory {
        QueryLegacyAuthentication getInstance() {
            return new QueryLegacyAuthentication();
        }
    }

}

