package com.atlassian.applinks.internal.rest.applink.data;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.support.ApplinkStatusValidationService;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.lang.String.format;

/**
 * Provides extra data indicating whether the Applink is editable.
 *
 * @since 5.2
 */
public class EditableDataProvider extends AbstractRestApplinkDataProvider {
    public static final String EDITABLE = "editable";
    public static final String V3_EDITABLE = "v3Editable";

    private final ApplinkStatusValidationService applinkStatusValidationService;

    @Autowired
    public EditableDataProvider(ApplinkStatusValidationService applinkStatusValidationService) {
        super(ImmutableSet.of(EDITABLE, V3_EDITABLE));
        this.applinkStatusValidationService = applinkStatusValidationService;
    }

    @Nullable
    @Override
    public Object provide(@Nonnull String key, @Nonnull ApplicationLink applink) throws ServiceException {
        if (EDITABLE.equals(key)) {
            // only system links are currently not editable
            return !applink.isSystem();
        } else if (V3_EDITABLE.equals(key)) {
            try {
                applinkStatusValidationService.checkEditable(applink);
                return true;
            } catch (ApplinkStatusException e) {
                return false;
            }
        }
        throw new IllegalArgumentException(format("Unsupported key: '%s'", key));
    }
}
