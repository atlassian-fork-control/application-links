package com.atlassian.applinks.internal.applink;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ValidationException;
import com.atlassian.applinks.internal.common.exception.ValidationExceptionBuilder;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.sal.api.message.I18nResolver;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.MalformedURLException;
import java.net.URI;
import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.common.applink.ApplicationLinks.withDisplayUrl;
import static com.atlassian.applinks.internal.common.applink.ApplicationLinks.withId;
import static com.atlassian.applinks.internal.common.applink.ApplicationLinks.withName;
import static com.atlassian.applinks.internal.common.applink.ApplicationLinks.withRpcUrl;
import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toImmutableList;
import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toStream;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * @since 5.1
 */
public class DefaultApplinkValidationService implements ApplinkValidationService {
    private final ApplinkHelper applinkHelper;
    private final ReadOnlyApplicationLinkService applicationLinkService;
    private final I18nResolver i18nResolver;

    @Autowired
    public DefaultApplinkValidationService(ApplinkHelper applinkHelper,
                                           ReadOnlyApplicationLinkService applicationLinkService,
                                           I18nResolver i18nResolver) {
        this.applinkHelper = applinkHelper;
        this.applicationLinkService = applicationLinkService;
        this.i18nResolver = i18nResolver;
    }

    @Override
    public void validateUpdate(@Nonnull ApplicationId applicationId, @Nonnull ApplicationLinkDetails details)
            throws NoSuchApplinkException, ValidationException {

        validateUpdate(applinkHelper.getReadOnlyApplicationLink(applicationId), details);
    }

    @Override
    public void validateUpdate(@Nonnull ReadOnlyApplicationLink applink, @Nonnull ApplicationLinkDetails details)
            throws ValidationException {
        requireNonNull(applink, "applink");
        requireNonNull(details, "details");

        ValidationExceptionBuilder validationBuilder = new ValidationExceptionBuilder(i18nResolver);
        Iterable<ReadOnlyApplicationLink> existingLinks = getExistingLinks(applink.getId());

        validateSystemLink(applink, validationBuilder);
        validateUri("rpcUrl", details.getRpcUrl(), validationBuilder);
        validateUri("displayUrl", details.getDisplayUrl(), validationBuilder);
        validateDuplicates(details, existingLinks, validationBuilder);

        if (validationBuilder.hasErrors()) {
            throw validationBuilder.origin(details).build();
        }
    }

    private void validateNotBlank(String fieldName, String fieldValue, ValidationExceptionBuilder validationBuilder) {
        if (isBlank(fieldValue)) {
            validationBuilder.error(fieldName, "applinks.rest.invalidrepresentation", fieldName);
        }
    }

    private void validateSystemLink(ReadOnlyApplicationLink applink, ValidationExceptionBuilder validationBuilder) {
        if (applink.isSystem()) {
            validationBuilder.error("applinks.service.error.validation.applink.system");
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void validateUri(String context, URI uri, ValidationExceptionBuilder validationBuilder) {
        validateNotBlank(context, uri.toString(), validationBuilder);
        try {
            uri.toURL();
        } catch (MalformedURLException e) {
            validationBuilder.error(context, "applinks.service.error.validation.applink.url.malformed");
        } catch (IllegalArgumentException e) {
            validationBuilder.error(context, "applinks.service.error.validation.applink.url.nonabsolute");
        }
    }

    private void validateDuplicates(ApplicationLinkDetails details, Iterable<ReadOnlyApplicationLink> existingLinks,
                                    ValidationExceptionBuilder validationBuilder) {
        if (toStream(existingLinks).anyMatch(withName(details.getName()))) {
            validationBuilder.error("name", "applinks.service.error.validation.applink.duplicate.name",
                    details.getName());
        }
        if (toStream(existingLinks).anyMatch(withRpcUrl(details.getRpcUrl()))) {
            validationBuilder.error("rpcUrl", "applinks.service.error.validation.applink.duplicate.rpcUrl");
        }
        if (toStream(existingLinks).anyMatch(withDisplayUrl(details.getDisplayUrl()))) {
            validationBuilder.error("displayUrl", "applinks.service.error.validation.applink.duplicate.displayUrl");
        }
    }

    private Iterable<ReadOnlyApplicationLink> getExistingLinks(ApplicationId applicationId) {
        return toStream(applicationLinkService.getApplicationLinks())
                .filter(withId(applicationId).negate())
                .collect(toImmutableList());
    }
}
