package com.atlassian.applinks.internal.rest.feature;

import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.feature.FeatureDiscoveryService;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.rest.interceptor.ServiceExceptionInterceptor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import java.util.Collections;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 * Feature discovery resource. For consistency all responses from this resource are JSON arrays with either a single
 * feature key that was referred to by the request, or all feature keys discovered by the user making the request.
 *
 * @since 4.3
 */
@AnonymousAllowed
@Path("feature-discovery")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@InterceptorChain({ContextInterceptor.class, ServiceExceptionInterceptor.class, NoCacheHeaderInterceptor.class})
public class FeatureDiscoveryResource {
    private static final String FEATURE_KEY_PARAM = "featureKey";
    private static final String FEATURE_KEY_URI_TEMPLATE = "{" + FEATURE_KEY_PARAM + "}";

    private final FeatureDiscoveryService featureDiscoveryService;

    public FeatureDiscoveryResource(FeatureDiscoveryService featureDiscoveryService) {
        this.featureDiscoveryService = featureDiscoveryService;
    }

    @GET
    @Path(FEATURE_KEY_URI_TEMPLATE)
    public Response isDiscovered(@PathParam(FEATURE_KEY_PARAM) String featureKey) throws ServiceException {
        return featureDiscoveryService.isDiscovered(featureKey) ?
                Response.ok(featureKeyRestEntity(featureKey)).build() :
                Response.status(NOT_FOUND).entity(featureKeyRestEntity(featureKey)).build();
    }

    @GET
    public Response getAllDiscoveredFeatures() throws ServiceException {
        return Response.ok(featureDiscoveryService.getAllDiscoveredFeatureKeys()).build();
    }

    @PUT
    @Path(FEATURE_KEY_URI_TEMPLATE)
    public Response discover(@PathParam(FEATURE_KEY_PARAM) String featureKey) throws ServiceException {
        featureDiscoveryService.discover(featureKey);
        return Response.ok(featureKeyRestEntity(featureKey)).build();
    }

    /**
     * @param featureKey feature key
     * @return singleton set with the feature key that will get transformed into a singleton JSON array
     */
    private static Set<String> featureKeyRestEntity(String featureKey) {
        return Collections.singleton(featureKey);
    }
}
