package com.atlassian.applinks.internal.rest.model.migration;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.migration.AuthenticationStatus;
import com.atlassian.applinks.internal.rest.model.ApplinksRestRepresentation;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;

import java.util.Objects;
import java.util.Set;

public class RestAuthenticationStatus extends ApplinksRestRepresentation {

    @VisibleForTesting
    public static final String OUTGOING = "outgoing";
    @VisibleForTesting
    public static final String INCOMING = "incoming";
    @VisibleForTesting
    public static final String CAPABILITIES = "capabilities";

    private RestAuthenticationConfig outgoing;
    private RestAuthenticationConfig incoming;
    private Set<ApplinksCapabilities> capabilities;

    //for Jackson
    public RestAuthenticationStatus() {
    }

    public RestAuthenticationStatus(@Nonnull final AuthenticationStatus authenticationStatus, @Nonnull final RemoteApplicationCapabilities remoteCapabilities) {
        Objects.requireNonNull(authenticationStatus, "authenticationConfigs");
        Objects.requireNonNull(remoteCapabilities, "remoteCapabilities");

        this.outgoing = new RestAuthenticationConfig(authenticationStatus.outgoing());
        this.incoming = new RestAuthenticationConfig(authenticationStatus.incoming());
        this.capabilities = remoteCapabilities.getCapabilities();
    }
}
