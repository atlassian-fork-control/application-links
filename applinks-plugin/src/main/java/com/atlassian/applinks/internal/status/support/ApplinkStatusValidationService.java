package com.atlassian.applinks.internal.status.support;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Restricted;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.remote.RemoteVersionIncompatibleException;

import javax.annotation.Nonnull;

public interface ApplinkStatusValidationService {
    void checkLocalCompatibility(@Nonnull final ApplicationLink link) throws ApplinkStatusException;

    /**
     * Find the remote Application links version and ensure that is compatible with our version. Versions earlier than
     * 4.0.0 will not be supported in status discovery.
     *
     * @param link the application link to the remote version
     * @throws NoAccessException if the user has no admin access
     * @throws RemoteVersionIncompatibleException if remote version is incompatible
     */
    @Restricted(PermissionLevel.ADMIN)
    void checkVersionCompatibility(@Nonnull final ApplicationLink link) throws NoAccessException, RemoteVersionIncompatibleException;

    void checkOAuthSupportedCompatibility(@Nonnull final ApplinkOAuthStatus status) throws ApplinkStatusException;

    void checkOAuthMismatch(@Nonnull final ApplinkOAuthStatus localStatus, @Nonnull final ApplinkOAuthStatus remoteStatus) throws ApplinkStatusException;

    void checkDisabled(@Nonnull final ApplinkOAuthStatus localStatus, @Nonnull final ApplinkOAuthStatus remoteStatus) throws ApplinkStatusException;

    /**
     * Check whether the current application link is configured with a legacy authentication such as Trusted and/or Basic.
     * If so it may be a candidate for authentication migration.
     *
     * @param link         the application link to the remote application
     * @param localStatus  the local OAuth configuration
     * @param remoteStatus the remote OAuth configuration
     * @throws NoSuchApplinkException for invalid applink
     * @see com.atlassian.applinks.internal.migration.AuthenticationMigrationService
     * @since 5.2
     */
    @Restricted(PermissionLevel.ADMIN)
    void checkLegacyAuthentication(@Nonnull final ApplicationLink link,
                                   @Nonnull final ApplinkOAuthStatus localStatus,
                                   @Nonnull final ApplinkOAuthStatus remoteStatus) throws NoSuchApplinkException, NoAccessException;

    /**
     * Check whether {@code applink} is V3-editable based on its current status.
     *
     * @param applink applink to check
     * @throws NoAccessException      if the user has no admin access
     * @throws ApplinkStatusException containing the current status, if the current status is considered not editable
     * @see com.atlassian.applinks.internal.status.oauth.OAuthStatusService
     */
    @Restricted(PermissionLevel.ADMIN)
    void checkEditable(@Nonnull ApplicationLink applink) throws NoAccessException, ApplinkStatusException;
}

