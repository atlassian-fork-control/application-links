package com.atlassian.applinks.internal.web;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.exception.InvalidApplicationIdException;
import com.atlassian.applinks.internal.common.exception.InvalidRequestException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.applinks.internal.common.exception.InvalidApplicationIdException.invalidIdI18nKey;
import static com.atlassian.applinks.internal.common.i18n.I18nKey.newI18nKey;
import static com.atlassian.applinks.internal.common.net.Uris.toComponents;
import static com.google.common.collect.Iterables.getFirst;

/**
 * @since 5.1
 */
public class DefaultWebApplinkHelper implements WebApplinkHelper {
    private final ApplinkHelper applinkHelper;
    private final ServiceExceptionFactory serviceExceptionFactory;

    @Autowired
    public DefaultWebApplinkHelper(ApplinkHelper applinkHelper, ServiceExceptionFactory serviceExceptionFactory) {
        this.applinkHelper = applinkHelper;
        this.serviceExceptionFactory = serviceExceptionFactory;
    }

    @Nonnull
    @Override
    public ApplicationLink getApplicationLink(@Nonnull HttpServletRequest request)
            throws InvalidRequestException, InvalidApplicationIdException, NoSuchApplinkException {
        String applinkId = getFirst(toComponents(request.getPathInfo()), null);
        if (applinkId == null) {
            throw serviceExceptionFactory.raise(InvalidRequestException.class,
                    newI18nKey("applinks.service.error.request.invalid.noapplinkid"));
        }
        try {
            return applinkHelper.getApplicationLink(new ApplicationId(applinkId));
        } catch (IllegalArgumentException e) {
            throw serviceExceptionFactory.raise(InvalidApplicationIdException.class, invalidIdI18nKey(applinkId), e);
        }
    }
}
