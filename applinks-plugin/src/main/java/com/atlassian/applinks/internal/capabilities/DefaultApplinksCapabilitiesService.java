package com.atlassian.applinks.internal.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;

import javax.annotation.Nonnull;
import java.util.EnumSet;
import java.util.Set;

public class DefaultApplinksCapabilitiesService implements ApplinksCapabilitiesService {
    private static boolean isDisabled(ApplinksCapabilities capability) {
        // disable capability only if there's a defined system property explicitly set to "false"
        String property = System.getProperty(capability.key);
        return property != null && Boolean.FALSE.toString().equalsIgnoreCase(property);
    }

    @Nonnull
    @Override
    public Set<ApplinksCapabilities> getCapabilities() {
        // get all capabilities first, as most likely all will be enabled
        // then iterate and remove those that are explicitly disabled
        EnumSet<ApplinksCapabilities> capabilities = EnumSet.allOf(ApplinksCapabilities.class);
        for (ApplinksCapabilities capability : ApplinksCapabilities.values()) {
            if (isDisabled(capability)) {
                capabilities.remove(capability);
            }
        }
        return capabilities;
    }
}
