package com.atlassian.applinks.internal.web;

import com.atlassian.applinks.analytics.ApplinksAdminViewEvent;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.internal.common.net.ResponseHeaderUtil;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.ui.AbstractAppLinksAdminOnlyServlet;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.applinks.ui.velocity.ListApplicationLinksContext;
import com.atlassian.applinks.ui.velocity.VelocityContextFactory;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.annotations.VisibleForTesting;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;

/**
 * A servlet that renders a velocity template to display all configured linked applications.
 *
 * @since 3.0
 */
public class ListApplicationLinksServlet extends AbstractAppLinksAdminOnlyServlet {
    public static final String LIST_APPLICATION_LINKS_URL = "plugins/servlet/applinks/listApplicationLinks";

    @VisibleForTesting
    static final String V2_TEMPLATE_PATH = "com/atlassian/applinks/ui/admin/list_application_links.vm";
    @VisibleForTesting
    static final String V3_TEMPLATE_PATH = "com/atlassian/applinks/ui/admin/list_application_links_agent.vm";

    private static final String V2_WEB_RESOURCE_CONTEXT = "applinks.list.application.links";
    private static final String V3_WEB_RESOURCE_CONTEXT = "applinks.list.application.links.agent";

    private final VelocityContextFactory velocityContextFactory;
    private final WebSudoManager webSudoManager;
    private final ApplinksFeatureService applinksFeatureService;
    private final EventPublisher eventPublisher;

    public ListApplicationLinksServlet(I18nResolver i18nResolver,
                                       MessageFactory messageFactory,
                                       TemplateRenderer templateRenderer,
                                       WebResourceManager webResourceManager,
                                       AdminUIAuthenticator adminUIAuthenticator,
                                       InternalHostApplication internalHostApplication,
                                       DocumentationLinker documentationLinker,
                                       LoginUriProvider loginUriProvider,
                                       VelocityContextFactory velocityContextFactory,
                                       WebSudoManager webSudoManager,
                                       XsrfTokenAccessor xsrfTokenAccessor,
                                       XsrfTokenValidator xsrfTokenValidator,
                                       ApplinksFeatureService applinksFeatureService,
                                       EventPublisher eventPublisher) {
        super(i18nResolver, messageFactory, templateRenderer, webResourceManager, adminUIAuthenticator,
                documentationLinker, loginUriProvider, internalHostApplication,
                xsrfTokenAccessor, xsrfTokenValidator);
        this.velocityContextFactory = velocityContextFactory;
        this.webSudoManager = webSudoManager;
        this.applinksFeatureService = applinksFeatureService;
        this.eventPublisher = eventPublisher;
    }

    @Override
    protected List<String> getRequiredWebResourceContexts() {
        final boolean uiv3Enabled = applinksFeatureService.isEnabled(ApplinksFeatures.V3_UI);
        return singletonList(uiv3Enabled ? V3_WEB_RESOURCE_CONTEXT : V2_WEB_RESOURCE_CONTEXT);
    }

    @Override
    protected void doService(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        super.doService(request, response);
        ResponseHeaderUtil.preventCrossFrameClickJacking(response);
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        try {
            // Enable web sudo protection if needed and if the app we are running in supports it
            webSudoManager.willExecuteWebSudoRequest(request);

            publishAnalytics();

            ListApplicationLinksContext context = velocityContextFactory.buildListApplicationLinksContext(request);

            final boolean v3ui_enabled = applinksFeatureService.isEnabled(ApplinksFeatures.V3_UI);
            render(
                    v3ui_enabled ? V3_TEMPLATE_PATH : V2_TEMPLATE_PATH,
                    singletonMap("context", context), request, response);

        } catch (WebSudoSessionException wse) {
            webSudoManager.enforceWebSudoProtection(request, response);
        }
    }

    private void publishAnalytics() {
        eventPublisher.publish(new ApplinksAdminViewEvent());
    }
}
