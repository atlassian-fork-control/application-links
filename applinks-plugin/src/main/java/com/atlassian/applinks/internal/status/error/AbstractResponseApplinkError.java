package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.internal.common.lang.StringTruncator;
import com.atlassian.sal.api.net.Response;
import com.google.common.base.Strings;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.HttpHeaders;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * Abstract implementation of {@link ResponseApplinkError}, providing default conversion of SAL response into an
 * {@link ResponseApplinkError}.
 * <p>
 * Subclasses are expected to provide error type and details.
 * </p>
 *
 * @since 5.0
 */
public abstract class AbstractResponseApplinkError implements ResponseApplinkError {
    private static final Logger log = LoggerFactory.getLogger(AbstractResponseApplinkError.class);

    public static final ContentType FALLBACK_CONTENT_TYPE = ContentType.create(TEXT_PLAIN, UTF_8);
    public static final int MAX_RESPONSE_SIZE = 500;

    private final int statusCode;
    private final String body;
    private final String contentType;

    @SuppressWarnings("ConstantConditions")
    public AbstractResponseApplinkError(@Nonnull Response response) {
        this.statusCode = response.getStatusCode();
        this.body = Strings.emptyToNull(getTruncatedBody(response));
        this.contentType = this.body != null ?
                getContentType(response).getMimeType() :
                null;
    }

    @Override
    public int getStatusCode() {
        return statusCode;
    }

    @Nullable
    @Override
    public String getBody() {
        return body;
    }

    @Nullable
    @Override
    public String getContentType() {
        return contentType;
    }

    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplinkErrorVisitor<T> visitor) {
        return visitor.visit(this);
    }

    private String getTruncatedBody(@Nonnull Response response) {
        try (Reader contents = new InputStreamReader(response.getResponseBodyAsStream(), getContentEncoding(response))) {
            return StringTruncator.forInput(contents)
                    .maxLines(MAX_RESPONSE_SIZE)
                    .maxCharsInLine(MAX_RESPONSE_SIZE)
                    .truncate();
        } catch (Exception e) {
            // at this stage we're already constructing an error (that was likely due a connection problem), so it is
            // somewhat expected that reading the response contents may fail
            log.debug("Could not retrieve response body", e);
            return "<Could not retrieve response body>";
        }
    }

    // would be better to use Guava's MediaType here, but we're still on Guava 11.x thanks to Confluence <sigh>
    private static ContentType getContentType(Response response) {
        try {
            String contentTypeValue = response.getHeader(HttpHeaders.CONTENT_TYPE);
            if (contentTypeValue != null) {
                return ContentType.parse(contentTypeValue);
            } else {
                return FALLBACK_CONTENT_TYPE;
            }
        } catch (Exception ignored) {
            return FALLBACK_CONTENT_TYPE;
        }
    }

    private static Charset getContentEncoding(Response response) {
        Charset c = getContentType(response).getCharset();
        return c == null ? FALLBACK_CONTENT_TYPE.getCharset() : c;
    }
}
