package com.atlassian.applinks.internal.migration;

import com.atlassian.annotations.Internal;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Restricted;
import com.atlassian.applinks.internal.status.LegacyConfig;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;

import javax.annotation.Nonnull;

/**
 * Service to migrate legacy authentication such as Trusted and Basic to OAuth.
 *
 * @since 5.2
 */
@Internal
@Restricted(value = PermissionLevel.ADMIN)
public interface AuthenticationMigrationService {
    // Must be sysadmin to migrate trusted to 2LO impersonation
    @Restricted(value = {PermissionLevel.ADMIN, PermissionLevel.SYSADMIN})
    @Nonnull
    AuthenticationStatus migrateToOAuth(@Nonnull ApplicationId applicationId) throws ServiceException;

    /**
     * Determine if the current user has remote system administration access. If he/she does, it will allow us to
     * migrate the legacy authentication automatically.
     *
     * @param link the application link to inspect
     * @return true if the current user has remote system admin access.
     * @throws NoSuchApplinkException for invalid applink
     * @throws NoAccessException if the user has no admin access
     */
    boolean hasRemoteSysAdminAccess(@Nonnull ApplicationLink link) throws NoSuchApplinkException, NoAccessException;

    /**
     * Get the local authentication migration status. This includes, the local legacy outgoing legacy authentication (trusted/basic)
     * and the local legacy incoming legacy authentication. Since we cannot determine the basic authentication status for incoming,
     * we attempt to query the remote application to find out its outgoing basic authentication.
     *
     * @param link             application link
     * @param localOAuthStatus the local OAuth status for incoming and outgoing connection
     * @return AuthenticationStatus which contains OAuth and legacy configs for incoming and outgoing.
     * @throws NoSuchApplinkException for invalid applink
     * @throws NoAccessException if the user has no admin access
     */
    @Nonnull
    AuthenticationStatus getAuthenticationMigrationStatus(@Nonnull final ApplicationLink link,
                                                          @Nonnull final ApplinkOAuthStatus localOAuthStatus) throws NoSuchApplinkException, NoAccessException;
}
