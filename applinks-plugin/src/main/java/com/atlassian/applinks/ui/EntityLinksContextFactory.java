package com.atlassian.applinks.ui;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.ApplicationTypeVisitor;
import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.api.application.bamboo.BambooApplicationType;
import com.atlassian.applinks.api.application.bitbucket.BitbucketApplicationType;
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.api.application.crowd.CrowdApplicationType;
import com.atlassian.applinks.api.application.fecru.FishEyeCrucibleApplicationType;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.api.application.refapp.RefAppApplicationType;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.rest.model.EntityLinkEntity;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.rest.applink.data.RestApplinkDataProviders;
import com.atlassian.applinks.internal.rest.model.applink.RestExtendedApplicationLink;
import com.atlassian.applinks.spi.link.MutatingEntityLinkService;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.ImmutableMap;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.StreamSupport;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * This factory creates a contextmap for the entitylinkspage.
 *
 * @since 6.1
 */
@Component
@ParametersAreNonnullByDefault
public class EntityLinksContextFactory {

    private static final String APPLINK_ICON_KEY = "iconUri";
    private static final Logger LOG = LoggerFactory.getLogger(EntityLinksContextFactory.class);


    private final InternalTypeAccessor typeAccessor;
    private final MutatingEntityLinkService mutatingEntityLinkService;
    private final ApplicationLinkService applicationLinkService;
    private final I18nResolver i18nResolver;
    private final InternalHostApplication internalHostApplication;
    private final MessageFactory messageFactory;
    private final RestApplinkDataProviders dataProviders;

    @Autowired
    public EntityLinksContextFactory(InternalTypeAccessor typeAccessor,
                                     MutatingEntityLinkService mutatingEntityLinkService,
                                     ApplicationLinkService applicationLinkService,
                                     I18nResolver i18nResolver,
                                     InternalHostApplication internalHostApplication,
                                     MessageFactory messageFactory,
                                     RestApplinkDataProviders dataProviders) {
        this.typeAccessor = requireNonNull(typeAccessor, "typeAccessor");
        this.mutatingEntityLinkService = requireNonNull(mutatingEntityLinkService, "entityLinksService");
        this.applicationLinkService = requireNonNull(applicationLinkService, "applinksService");
        this.i18nResolver = requireNonNull(i18nResolver, "i18nResolver");
        this.internalHostApplication = requireNonNull(internalHostApplication, "internalHostApplication");
        this.messageFactory = requireNonNull(messageFactory, "messageFactory");
        this.dataProviders = requireNonNull(dataProviders, "dataProviders");
    }

    /**
     * Create the contextmap for a given project and entityKey for the current application.
     *
     * @param typeId     the id of the entityType
     * @param projectKey the projectKey
     * @return a contextmap
     * @throws IOException when the data cannot be serialized
     */
    public Map<String, Object> createContext(String typeId, String projectKey) throws IOException {
        EntityType entityType = typeAccessor.loadEntityType(typeId);
        if (entityType == null || !internalHostApplication.doesEntityExist(projectKey, entityType.getClass())) {
            throw new AbstractApplinksServlet.BadRequestException(messageFactory.newLocalizedMessage(
                    String.format("No entity exists with key %s of type %s", projectKey, typeId)));
        }
        if (!internalHostApplication.canManageEntityLinksFor(
                internalHostApplication.toEntityReference(projectKey, entityType.getClass()))) {
            throw new AbstractApplinksServlet.UnauthorizedException(
                    messageFactory.newI18nMessage("applinks.entity.list.no.manage.permission", projectKey, entityType.getClass().getName()));
        }

        return new ImmutableMap.Builder<String, Object>()
                .put("projectKey", projectKey)
                .put("meta", createMeta(projectKey))
                .put("data", createDataJson(projectKey, entityType))
                .build();
    }

    private String createDataJson(String projectKey, EntityType entityType) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setAnnotationIntrospector(new JaxbAnnotationIntrospector());
        objectMapper.setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
        return objectMapper.writeValueAsString(ImmutableMap.of(
                "entityLinks", getEntityLinks(projectKey, entityType),
                "applicationLinks", getApplicationLinks(),
                "currentApp", i18nResolver.getText(internalHostApplication.getType().getI18nKey()),
                "projectKey", projectKey,
                "type", entityType.getClass().getName()
        ));
    }

    private List<RestExtendedApplicationLink> getApplicationLinks() {
        return StreamSupport.stream(applicationLinkService.getApplicationLinks().spliterator(), false)
                .map(link -> new RestExtendedApplicationLink(link, Collections.emptySet(), createData(link)))
                .collect(toList());
    }

    private Map<String, Object> createData(ApplicationLink link) {
        try {
            return ImmutableMap.<String, Object>builder()
                    .put(APPLINK_ICON_KEY, dataProviders.getProvider(APPLINK_ICON_KEY).provide(APPLINK_ICON_KEY, link))
                    .build();
        } catch (ServiceException e) {
            LOG.error("Did not enhance data for applink map because of:", e);
            return Collections.emptyMap();
        }
    }

    private List<EntityLinkEntity> getEntityLinks(String projectKey, EntityType entityType) {
        return StreamSupport.stream(mutatingEntityLinkService.getEntityLinksForKey(projectKey, entityType.getClass()).spliterator(), false)
                .map(EntityLinkEntity::new)
                .collect(toList());
    }

    private Map<String, Object> createMeta(String projectKey) {
        final ImmutableMap.Builder<String, Object> mapBuilder = new ImmutableMap.Builder<>();
        return internalHostApplication.getType().accept(new ApplicationTypeVisitor<ImmutableMap.Builder<String, Object>>() {
            @Override
            public ImmutableMap.Builder<String, Object> visit(@Nonnull BambooApplicationType type) {
                return mapBuilder.put("decorator", "atl.general");
            }

            @Override
            public ImmutableMap.Builder<String, Object> visit(@Nonnull BitbucketApplicationType type) {
                return mapBuilder.put("projectKey", projectKey)
                        .put("decorator", "bitbucket.project.settings")
                        .put("activeTab", "project-settings-entity-links");
            }

            @Override
            public ImmutableMap.Builder<String, Object> visit(@Nonnull ConfluenceApplicationType type) {
                return mapBuilder.put("decorator", "atl.admin");
            }

            @Override
            public ImmutableMap.Builder<String, Object> visit(@Nonnull CrowdApplicationType type) {
                return mapBuilder.put("decorator", "atl.admin");
            }

            @Override
            public ImmutableMap.Builder<String, Object> visit(@Nonnull FishEyeCrucibleApplicationType type) {
                return mapBuilder.put("decorator", "atl.admin");
            }

            @Override
            public ImmutableMap.Builder<String, Object> visit(@Nonnull GenericApplicationType type) {
                return mapBuilder.put("decorator", "atl.admin");
            }

            @Override
            public ImmutableMap.Builder<String, Object> visit(@Nonnull JiraApplicationType type) {
                return mapBuilder.put("projectKey", projectKey)
                        .put("decorator", "admin")
                        .put("admin.active.section", "atl.jira.proj.config/projectgroup4")
                        .put("admin.active.tab", "view_project_links");
            }

            @Override
            public ImmutableMap.Builder<String, Object> visit(@Nonnull RefAppApplicationType type) {
                return mapBuilder.put("decorator", "atl.admin");
            }

            @Override
            public ImmutableMap.Builder<String, Object> visitDefault(@Nonnull ApplicationType applicationType) {
                return mapBuilder.put("decorator", "atl.admin");
            }
        }).build();
    }
}
