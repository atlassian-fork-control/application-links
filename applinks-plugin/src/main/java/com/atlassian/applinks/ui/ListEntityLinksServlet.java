package com.atlassian.applinks.ui;

import com.atlassian.applinks.analytics.EntityLinksAdminViewEvent;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static javax.ws.rs.core.MediaType.TEXT_HTML;

/**
 * A servlet that renders a velocity template to display all configured entity links.
 *
 * @since 3.0
 */
public class ListEntityLinksServlet extends AbstractApplinksServlet {

    private static final String DEFAULT_CONTENT_TYPE = ContentType.create(TEXT_HTML, UTF_8).toString();
    private static final String MODULE_KEY = "entitylinks-react-ui";
    private static final String TEMPLATE_DESCRIPTOR = "applinks.internal.entitylinks.entitylinkPage";

    private final AppLinkPluginUtil appLinkPluginUtil;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final WebSudoManager webSudoManager;
    private final EntityLinksContextFactory entityLinksContextFactory;
    private final EventPublisher eventPublisher;

    public ListEntityLinksServlet(I18nResolver i18nResolver,
                                  MessageFactory messageFactory,
                                  TemplateRenderer templateRenderer,
                                  WebResourceManager webResourceManager,
                                  DocumentationLinker documentationLinker,
                                  LoginUriProvider loginUriProvider,
                                  InternalHostApplication internalHostApplication,
                                  XsrfTokenAccessor xsrfTokenAccessor,
                                  XsrfTokenValidator xsrfTokenValidator,
                                  AppLinkPluginUtil appLinkPluginUtil,
                                  AdminUIAuthenticator adminUIAuthenticator,
                                  SoyTemplateRenderer soyTemplateRenderer,
                                  WebSudoManager webSudoManager,
                                  EntityLinksContextFactory entityLinksContextFactory,
                                  final EventPublisher eventPublisher) {
        super(i18nResolver, messageFactory, templateRenderer, webResourceManager, documentationLinker, loginUriProvider,
                internalHostApplication, adminUIAuthenticator, xsrfTokenAccessor, xsrfTokenValidator);

        this.appLinkPluginUtil = appLinkPluginUtil;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.webSudoManager = webSudoManager;
        this.entityLinksContextFactory = entityLinksContextFactory;
        this.eventPublisher = eventPublisher;
    }

    @Override
    protected List<String> getRequiredWebResources() {
        return singletonList(WEB_RESOURCE_KEY + "list-entity-links");
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        try {
            // Enable web sudo protection if needed and if the app we are running in supports it
            webSudoManager.willExecuteWebSudoRequest(request);
            final String[] pathParams = extractParams(request);
            final String typeId = pathParams[pathParams.length - 2];
            final String projectKey = pathParams[pathParams.length - 1];

            publishAnalytics(typeId, projectKey);


            response.setContentType(DEFAULT_CONTENT_TYPE);
            soyTemplateRenderer.render(response.getWriter(), appLinkPluginUtil.completeModuleKey(MODULE_KEY),
                    TEMPLATE_DESCRIPTOR, entityLinksContextFactory.createContext(typeId, projectKey));
        } catch (WebSudoSessionException wse) {
            webSudoManager.enforceWebSudoProtection(request, response);
        }
    }

    private String[] extractParams(final HttpServletRequest request) {
        final String[] pathParams = StringUtils.split(request.getPathInfo(), '/');

        if (pathParams.length < 2) {
            throw new AbstractApplinksServlet.BadRequestException(messageFactory.newLocalizedMessage(
                    "Servlet URL should be of form /listEntityLinks/{entity-type}/{entity-key}"));
        }
        return pathParams;
    }

    private void publishAnalytics(String typeId, String key) {
        eventPublisher.publish(new EntityLinksAdminViewEvent(typeId, key));
    }
}