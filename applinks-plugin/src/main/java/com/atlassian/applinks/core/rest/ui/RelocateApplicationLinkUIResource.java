package com.atlassian.applinks.core.rest.ui;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.manifest.AppLinksManifestDownloader;
import com.atlassian.applinks.core.rest.AbstractResource;
import com.atlassian.applinks.core.rest.auth.AdminApplicationLinksInterceptor;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ApplicationStatus;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.sun.jersey.spi.resource.Singleton;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.core.rest.util.RestUtil.forbidden;
import static com.atlassian.applinks.core.rest.util.RestUtil.noContent;
import static com.atlassian.applinks.core.rest.util.RestUtil.notFound;
import static com.atlassian.applinks.core.rest.util.RestUtil.serverError;

/**
 * Modifies an application link's RPC URL.
 *
 * @since 3.0
 */
@Path("relocateApplicationlink")
@Api
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
@InterceptorChain({ContextInterceptor.class, AdminApplicationLinksInterceptor.class, NoCacheHeaderInterceptor.class})
public class RelocateApplicationLinkUIResource extends AbstractResource {
    private static final Logger LOG = LoggerFactory.getLogger(RelocateApplicationLinkUIResource.class);

    private final MutatingApplicationLinkService applicationLinkService;
    private final ManifestRetriever manifestRetriever;
    private final AppLinksManifestDownloader manifestDownloader;
    private final I18nResolver i18nResolver;
    private final UserManager userManager;

    public RelocateApplicationLinkUIResource(
            final RestUrlBuilder restUrlBuilder,
            final MutatingApplicationLinkService applicationLinkService,
            final I18nResolver i18nResolver,
            final ManifestRetriever manifestRetriever,
            final AppLinksManifestDownloader manifestDownloader,
            final InternalTypeAccessor internalTypeAccessor,
            final RequestFactory requestFactory,
            final UserManager userManager) {
        super(restUrlBuilder, internalTypeAccessor, requestFactory, applicationLinkService);
        this.applicationLinkService = applicationLinkService;
        this.i18nResolver = i18nResolver;
        this.manifestRetriever = manifestRetriever;
        this.manifestDownloader = manifestDownloader;
        this.userManager = userManager;
    }

    /**
     * Status codes:
     * <ul>
     * <li>204: Updated successfully (this is the only mutating operation)</li>
     * <li>400: The application running at the new URL has a different application type than the existing applink</li>
     * <li>404: The specified server ID doesn't have an application link on this server</li>
     * <li>409: The server located at the specified URL is not responding, resubmit with "?nowarning=true" to update anyway (display URL will be set to RPC URL)"</li>
     * </ul>
     *
     * A sysadmin credential is required to relocate an Application Link.
     *
     * @throws TypeNotInstalledException will be translated into a 400.
     */
    @POST
    @ApiOperation(value ="Update the RPC URL of the remote application",
            authorizations = @Authorization("SysAdmin")
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "Updated successfully (this is the only mutating operation)"),
            @ApiResponse(code = 400, message = "The application running at the new URL has a different application type than the existing applink"),
            @ApiResponse(code = 404, message = "The specified server ID doesn't have an application link on this server"),
            @ApiResponse(code = 409, message = "The server located at the specified URL is not responding, resubmit with \"?nowarning=true\" to force the update (display URL will be set to RPC URL)")
    })
    @Path("{applinkId}")
    public Response relocate(@PathParam("applinkId") final String applicationId,
                             @QueryParam("newUrl") final String urlString,
                             @QueryParam("nowarning") final boolean nowarning)
            throws TypeNotInstalledException {
        // Only sysadmin can perform relocation.
        if (!userManager.isSystemAdmin(userManager.getRemoteUsername())) {
            return forbidden(i18nResolver.getText("applinks.error.only.sysadmin.operation"));
        }

        final MutableApplicationLink link = applicationLinkService.getApplicationLink(new ApplicationId(applicationId));

        //REST-186: This is a workaround for an apparent issue in REST 2.6. Uncaught exceptions used to result in JSON
        //payloads, but now they are resulting in plain text.
        URI url;
        try {
            url = URIUtil.uncheckedToUri(urlString);
        } catch (RuntimeException e) {
            return serverError(e.getMessage());
        }

        if (link == null) {
            return notFound(i18nResolver.getText("applinks.notfound", applicationId));
        } else {
            if (manifestRetriever.getApplicationStatus(url, link.getType()) == ApplicationStatus.UNAVAILABLE) {
                if (nowarning) {
                    return update(link, url, url);
                } else {
                    /**
                     * IMPLEMENTATION NOTE:
                     *
                     * The restfulness of this pattern is debatable:
                     * http://stackoverflow.com/questions/2539394/rest-http-delete-and-parameters
                     * However, as is often the case with rest, few
                     * alternatives are suggested.
                     */
                    return Response
                            .status(409)
                            .build();
                }
            } else {
                URI displayUrl = url;
                try {
                    final Manifest manifest = manifestDownloader.download(url);
                    if (typeAccessor.loadApplicationType(manifest.getTypeId()).equals(link.getType())) {
                        displayUrl = manifest.getUrl();
                    } else {
                        return Response
                                .status(400)
                                .entity(i18nResolver.getText("applinks.error.relocate.type", urlString,
                                        i18nResolver.getText(typeAccessor.loadApplicationType(manifest.getTypeId()).getI18nKey()),
                                        i18nResolver.getText(link.getType().getI18nKey())))
                                .build();
                    }
                } catch (ManifestNotFoundException e) {
                    // the peer is online, but doesn't serve a manifest: must be non-UAL, that's ok
                }
                return update(link, url, displayUrl);
            }
        }
    }

    private Response update(final MutableApplicationLink link, final URI rpcUrl, final URI displayUrl) {
        link.update(ApplicationLinkDetails
                .builder(link)
                .rpcUrl(rpcUrl)
                .displayUrl(displayUrl)
                .build());
        LOG.info("Changed RPC URL from {} to {} and display URL from {} to {} for ApplicationLink {} .",
                link.getRpcUrl(), rpcUrl, link.getDisplayUrl(), displayUrl, link.getId());
        return noContent(); // returning the updated applink might be more restful
    }
}
