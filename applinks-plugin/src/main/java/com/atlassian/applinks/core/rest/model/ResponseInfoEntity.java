package com.atlassian.applinks.core.rest.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

/**
 */
@XmlRootElement(name = "responseInfo")
public class ResponseInfoEntity {
    @XmlElement(name = "warning")
    private String warning;

    /**
     * An identifier of the type of warning being returned, such as the i18n code.
     */
    @XmlElement(name = "code")
    private String code;
    private Map<String, String> params;

    public ResponseInfoEntity() {
    }

    public ResponseInfoEntity(final String code, final String warning) {
        this.warning = warning;
        this.code = code;
    }

    public ResponseInfoEntity(final String code, final String warning, final Map<String, String> params) {
        this.warning = warning;
        this.code = code;
        this.params = params;
    }

    public String getWarning() {
        return warning;
    }

    public String getCode() {
        return code;
    }

    public Map<String, String> getParams() {
        return params;
    }
}
