package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.ImpersonatingAuthenticationProvider;
import com.atlassian.applinks.api.auth.NonImpersonatingAuthenticationProvider;
import com.atlassian.applinks.core.auth.ApplicationLinkRequestFactoryFactory;

import javax.annotation.concurrent.Immutable;
import java.net.URI;

import static java.util.Objects.requireNonNull;

/**
 * @since 4.0
 */
@Immutable
public final class ImmutableApplicationLink implements ReadOnlyApplicationLink, ApplicationLink {
    private final ApplicationId applicationId;
    private final ApplicationType applicationType;
    private final String name;
    private final URI displayUrl;
    private final URI rpcUrl;
    private final boolean isPrimary;
    private final boolean isSystem;
    private final ApplicationLinkRequestFactoryFactory requestFactoryFactory;

    public ImmutableApplicationLink(final ApplicationLink that, final ApplicationLinkRequestFactoryFactory requestFactoryFactory) {
        requireNonNull(that, "that");
        requireNonNull(requestFactoryFactory, "requestFactoryFactory");
        this.applicationId = that.getId();
        this.applicationType = that.getType();
        this.name = that.getName();
        this.displayUrl = that.getDisplayUrl();
        this.rpcUrl = that.getRpcUrl();
        this.isPrimary = that.isPrimary();
        this.isSystem = that.isSystem();
        this.requestFactoryFactory = requestFactoryFactory;
    }

    @Override
    public ApplicationId getId() {
        return applicationId;
    }

    @Override
    public ApplicationType getType() {
        return applicationType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public URI getDisplayUrl() {
        return displayUrl;
    }

    @Override
    public URI getRpcUrl() {
        return rpcUrl;
    }

    @Override
    public boolean isPrimary() {
        return isPrimary;
    }

    @Override
    public boolean isSystem() {
        return isSystem;
    }

    @Override
    public ApplicationLinkRequestFactory createAuthenticatedRequestFactory() {
        return requestFactoryFactory.getApplicationLinkRequestFactory(this);
    }

    @Override
    public ApplicationLinkRequestFactory createAuthenticatedRequestFactory(final Class<? extends AuthenticationProvider> providerClass) {
        return requestFactoryFactory.getApplicationLinkRequestFactory(this, providerClass);
    }

    @Override
    public ApplicationLinkRequestFactory createImpersonatingAuthenticatedRequestFactory() {
        return requestFactoryFactory.getApplicationLinkRequestFactory(this, ImpersonatingAuthenticationProvider.class);
    }

    @Override
    public ApplicationLinkRequestFactory createNonImpersonatingAuthenticatedRequestFactory() {
        return requestFactoryFactory.getApplicationLinkRequestFactory(this, NonImpersonatingAuthenticationProvider.class);
    }

    @Override
    public Object getProperty(final String key) {
        return null;
    }

    @Override
    public Object putProperty(final String key, final Object value) {
        throw new UnsupportedOperationException("putProperty not allowed on immutable applink");
    }

    @Override
    public Object removeProperty(final String key) {
        throw new UnsupportedOperationException("removeProperty not allowed on immutable applink");
    }

    @Override
    public String toString() {
        return String.format("%s (%s) %s %s", name, applicationId, rpcUrl, applicationType);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ImmutableApplicationLink that = (ImmutableApplicationLink) o;

        return applicationId != null ? applicationId.equals(that.applicationId) : that.applicationId == null;

    }

    @Override
    public int hashCode() {
        return applicationId != null ? applicationId.hashCode() : 0;
    }

}
