package com.atlassian.applinks.core;

import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class DefaultAppLinkPluginUtil implements AppLinkPluginUtil {
    private static final String VERSION_OVERRIDE_PROPERTY = "applinks.version.override";
    private static final Logger LOG = LoggerFactory.getLogger(DefaultAppLinkPluginUtil.class.getName());
    private final String pluginKey;
    private final Version version;

    @Autowired
    public DefaultAppLinkPluginUtil(final BundleContext bundleContext) {
        final Bundle bundle = bundleContext.getBundle();
        pluginKey = OsgiHeaderUtil.getPluginKey(bundle);
        version = bundle.getVersion();
    }

    @Nonnull
    public String getPluginKey() {
        return pluginKey;
    }

    @Nonnull
    @Override
    public String completeModuleKey(@Nonnull String moduleKey) {
        requireNonNull(moduleKey, "moduleKey");
        return pluginKey + ":" + moduleKey;
    }

    @Nonnull
    public Version getVersion() {
        String versionOverride = System.getProperty(VERSION_OVERRIDE_PROPERTY);
        if (versionOverride != null) {
            try {
                return new Version(versionOverride);
            } catch (Exception e) {
                LOG.debug("System Property '{}' contains an invalid version string, using version from OSGi",
                        VERSION_OVERRIDE_PROPERTY);
            }
        }
        return version;
    }
}