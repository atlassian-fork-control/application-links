package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Restricted;

import javax.annotation.Nonnull;

/**
 * Provides detailed information about status of Application Links in the system.
 *
 * @see ApplinkStatus
 * @since 4.3
 */
@Restricted(PermissionLevel.ADMIN)
public interface ApplinkStatusService {
    /**
     * Retrieve detailed status of an applink with {@code id}. This method call raises an event to notify all interested
     * listeners about the latest applink status.
     *
     * @param id ID of the application link
     * @return detailed status
     * @throws NoAccessException      if the calling user does not have {@link PermissionLevel#ADMIN administrator}
     *                                permission
     * @throws NoSuchApplinkException if applinks with given {@code id} does not exist or cannot be loaded
     */
    @Nonnull
    ApplinkStatus getApplinkStatus(@Nonnull ApplicationId id) throws NoAccessException, NoSuchApplinkException;
}
