package com.atlassian.applinks.core.property;

import com.atlassian.applinks.api.PropertySet;
import com.atlassian.applinks.core.util.RequestUtil;
import com.atlassian.applinks.spi.application.TypeId;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.Objects.requireNonNull;

/**
 * A wrapper around a property set that stores properties for one application link.
 *
 * @since 3.0
 */
public class ApplicationLinkProperties {
    private static final String AUTH_PROVIDER_PREFIX = "auth";
    private static final EnumSet<Property> STANDARD_PROPERTIES = EnumSet.allOf(Property.class);
    private final Lock customPropertyWriteLock = new ReentrantLock();
    private final Lock authenticationProviderWriteLock = new ReentrantLock();
    private static final Logger LOG = LoggerFactory.getLogger(ApplicationLinkProperties.class.getName());

    private static final Logger log = LoggerFactory.getLogger(ApplicationLinkProperties.class);

    /*Public for testing*/
    public enum Property {
        TYPE("type"),
        NAME("name"),
        DISPLAY_URL("display.url"),
        RPC_URL("rpc.url"),
        PRIMARY("primary"),
        AUTH_PROVIDER_KEYS("providerKeys"),
        SYSTEM("system"),
        PROPERTY_KEYS("propertyKeys");

        private final String key;

        Property(String key) {
            this.key = key;
        }

        public String key() {
            return key;
        }
    }

    private final PropertySet applinksAdminPropertySet;
    private final PropertySet applinksPropertySet;

    public ApplicationLinkProperties(final PropertySet applinksAdminPropertySet, final PropertySet applinksPropertySet) {
        this.applinksAdminPropertySet = applinksAdminPropertySet;
        this.applinksPropertySet = applinksPropertySet;
    }

    /**
     * Copies all values from the supplied
     * {@link com.atlassian.applinks.core.property.ApplicationLinkProperties}
     * object to this instance.
     *
     * @param props must not be {@code null}.
     */
    public void setProperties(final ApplicationLinkProperties props) {
        requireNonNull(props, "props must not be null");
        for (final Property propertyKey : EnumSet.allOf(Property.class)) {
            final Object value = props.applinksAdminPropertySet.getProperty(propertyKey.key());
            if (value != null) {
                logCopyingProperties(propertyKey, value, props);
                applinksAdminPropertySet.putProperty(propertyKey.key(), value);
                if (propertyKey == Property.PROPERTY_KEYS) {
                    customPropertyWriteLock.lock();
                    try {
                        // custom user properties that are stored in applinksPropertySet
                        for (final String customPropertyKey : (List<String>) value) {
                            Object customPropertyValue = props.applinksPropertySet.getProperty(customPropertyKey);
                            logCopyingChildProperty(propertyKey.key, customPropertyKey, customPropertyValue, props);
                            applinksPropertySet.putProperty(customPropertyKey, customPropertyValue);
                        }
                    } finally {
                        customPropertyWriteLock.unlock();
                    }
                } else if (propertyKey == Property.AUTH_PROVIDER_KEYS) {
                    authenticationProviderWriteLock.lock();
                    try {
                        for (final String authProviderKey : (List<String>) value) {
                            Object authProviderValue = props.applinksAdminPropertySet.getProperty(hashedAuthProviderKey(authProviderKey));
                            logCopyingChildProperty(propertyKey.key(), authProviderKey, authProviderValue, props);
                            applinksAdminPropertySet.putProperty(hashedAuthProviderKey(authProviderKey), authProviderValue);
                        }
                    } finally {
                        authenticationProviderWriteLock.unlock();
                    }
                }
            }
        }
    }

    private String hashedAuthProviderKey(final String authProviderKey) {
        return AUTH_PROVIDER_PREFIX + "." + createHashedProviderKey(authProviderKey);
    }

    private String createHashedProviderKey(final String providerKey) {
        String hashedValue = DigestUtils.md5Hex(providerKey);
        return hashedValue;
    }

    public TypeId getType() {
        final String id = (String) applinksAdminPropertySet.getProperty(Property.TYPE.key());
        return id != null ? new TypeId(id) : null;
    }

    public void setType(final TypeId type) {
        logSettingProperty(Property.TYPE.key(), type.get());
        applinksAdminPropertySet.putProperty(Property.TYPE.key(), type.get());
    }

    public String getName() {
        return (String) applinksAdminPropertySet.getProperty(Property.NAME.key());
    }

    public void setName(final String name) {
        logSettingProperty(Property.NAME.key(), name.trim());
        applinksAdminPropertySet.putProperty(Property.NAME.key(), name.trim());
    }

    public URI getDisplayUrl() {
        return getUri(Property.DISPLAY_URL.key());
    }

    private void checkURLValid(URI uri) {
        if (uri != null) {
            try {
                final URL urlToValidate = uri.toURL();
                final String urlProtocol = urlToValidate.getProtocol();
                final boolean isValidURL = RequestUtil.HTTP_SCHEME.equals(urlProtocol) || RequestUtil.HTTPS_SCHEME.equals(urlProtocol);
                if (!isValidURL) {
                    throw new IllegalArgumentException("URL " + uri.toString() + " is not valid");
                }
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException("URL " + uri.toString() + " is not valid");
            }
        }
    }

    public void setDisplayUrl(final URI url) {
        checkURLValid(url);
        logSettingProperty(Property.DISPLAY_URL.key(), url);
        setUri(Property.DISPLAY_URL.key(), url);
    }

    public URI getRpcUrl() {
        return getUri(Property.RPC_URL.key());
    }

    public void setRpcUrl(final URI url) {
        checkURLValid(url);
        logSettingProperty(Property.RPC_URL.key(), url);
        setUri(Property.RPC_URL.key(), url);
    }

    public boolean isPrimary() {
        return Boolean.parseBoolean((String) applinksAdminPropertySet.getProperty(Property.PRIMARY.key()));
    }

    public void setIsPrimary(final boolean isPrimary) {
        logSettingProperty(Property.PRIMARY.key(), isPrimary);
        applinksAdminPropertySet.putProperty(Property.PRIMARY.key(), String.valueOf(isPrimary));
    }

    public boolean isSystem() {
        return Boolean.parseBoolean((String) applinksAdminPropertySet.getProperty(Property.SYSTEM.key())) ||
                Boolean.parseBoolean((String) applinksPropertySet.getProperty(Property.SYSTEM.key()));
    }

    public void setSystem(final boolean isSystem) {
        logSettingProperty(Property.SYSTEM.key(), isSystem);
        applinksAdminPropertySet.putProperty(Property.SYSTEM.key(), String.valueOf(isSystem));
        applinksPropertySet.putProperty(Property.SYSTEM.key(), String.valueOf(isSystem));
    }

    public void remove() {
        //Delete authentication provider configurations first
        try {
            authenticationProviderWriteLock.lock();
            final List<String> providerKeys = getProviderKeys();
            for (final String providerKey : providerKeys) {
                String hashedProviderKey = hashedAuthProviderKey(providerKey);
                logRemovingHashedProperty("ProviderConfig", providerKey, hashedProviderKey, new Function<String, Object>() {
                    @Override
                    public Object apply(@Nullable String hashedProviderKey) {
                        return applinksAdminPropertySet.getProperty(hashedProviderKey);
                    }
                });
                applinksAdminPropertySet.removeProperty(hashedProviderKey);
            }
            setProviderKeys(Collections.<String>emptyList());
        } finally {
            authenticationProviderWriteLock.unlock();
        }
        //Now we delete the properties of this application link.
        try {
            customPropertyWriteLock.lock();
            for (final String key : getCustomPropertyKeys()) {
                logRemovingProperty(key, new Function<String, Object>() {
                    @Override
                    public Object apply(@Nullable String key) {
                        return applinksPropertySet.getProperty(key);
                    }
                });
                applinksPropertySet.removeProperty(key);
            }
            setPropertyKeys(Collections.<String>emptyList());
        } finally {
            customPropertyWriteLock.unlock();
        }
        //Now we delete the properties of this application link itself.
        for (Property standardProperty : STANDARD_PROPERTIES) {
            logRemovingProperty(standardProperty.key(), new Function<String, Object>() {
                @Override
                public Object apply(@Nullable String key) {
                    return applinksAdminPropertySet.getProperty(key);
                }
            });
            applinksAdminPropertySet.removeProperty(standardProperty.key());
        }
        // remove the extra system flag
        applinksPropertySet.removeProperty(Property.SYSTEM.key());
    }

    public void setProviderConfig(final String providerKey, final Map<String, String> config) {
        String hashedProviderKey = hashedAuthProviderKey(providerKey);
        logSettingHashedProperty("ProviderConfig", providerKey, hashedProviderKey, toProperties(config));
        applinksAdminPropertySet.putProperty(hashedProviderKey, toProperties(config));
        try {
            authenticationProviderWriteLock.lock();
            final List<String> providerKeys = getProviderKeys();
            if (!providerKeys.contains(providerKey)) {
                providerKeys.add(providerKey);
                setProviderKeys(providerKeys);
            }
        } finally {
            authenticationProviderWriteLock.unlock();
        }
    }

    public void removeProviderConfig(final String providerKey) {
        logRemovingHashedProperty("ProviderConfig", providerKey, hashedAuthProviderKey(providerKey), new Function<String, Object>() {
            @Override
            public Object apply(@Nullable String key) {
                return applinksAdminPropertySet.getProperty(key);
            }
        });

        applinksAdminPropertySet.removeProperty(hashedAuthProviderKey(providerKey));
        try {
            authenticationProviderWriteLock.lock();
            final List<String> providerKeys = getProviderKeys();
            providerKeys.remove(providerKey);
            setProviderKeys(providerKeys);
        } finally {
            authenticationProviderWriteLock.unlock();
        }
    }

    public Map<String, String> getProviderConfig(final String providerKey) {
        log.debug("Getting provider config; key: {}", providerKey);
        final Object obj = applinksAdminPropertySet.getProperty(hashedAuthProviderKey(providerKey));
        if (obj == null || !(obj instanceof Properties)) {
            return null;
        } else {
            return toMap((Properties) obj);
        }
    }

    /**
     * public access to facilitate upgrade tasks
     *
     * @return a {@link List} of authentication provider keys registered to the application link
     */
    @SuppressWarnings("unchecked")
    public List<String> getProviderKeys() {
        List<String> list = (List<String>) applinksAdminPropertySet.getProperty(Property.AUTH_PROVIDER_KEYS.key());
        if (list == null) {
            list = new ArrayList<String>();
        }
        return list;
    }

    public Object getProperty(final String key) {
        return applinksPropertySet.getProperty(key);
    }

    public Object putProperty(final String key, final Object value) {
        logPuttingProperty(key, value);
        final Object oldValue = applinksPropertySet.putProperty(key, value);
        try {
            customPropertyWriteLock.lock();
            final List<String> propertyKeys = getCustomPropertyKeys();
            if (!propertyKeys.contains(key)) {
                propertyKeys.add(key);
                setPropertyKeys(propertyKeys);
            }
        } finally {
            customPropertyWriteLock.unlock();
        }
        return oldValue;
    }

    public Object removeProperty(final String key) {
        logRemovingProperty(key, new Function<String, Object>() {
            @Override
            public Object apply(@Nullable String key) {
                return applinksPropertySet.getProperty(key);
            }
        });
        final Object removedValue = applinksPropertySet.removeProperty(key);
        if (removedValue != null) {
            try {
                customPropertyWriteLock.lock();
                final List<String> properties = getCustomPropertyKeys();
                properties.remove(key);
                setPropertyKeys(properties);
            } finally {
                customPropertyWriteLock.unlock();
            }
        }
        return removedValue;
    }

    private List<String> getCustomPropertyKeys() {
        List<String> customPropertyKeys = (List<String>) applinksAdminPropertySet.getProperty(Property.PROPERTY_KEYS.key());
        if (customPropertyKeys == null) {
            customPropertyKeys = new ArrayList<>();
        }
        return customPropertyKeys;
    }

    private void setPropertyKeys(List<String> customPropertyKeys) {
        logSettingProperty(Property.PROPERTY_KEYS.key(), customPropertyKeys);
        applinksAdminPropertySet.putProperty(Property.PROPERTY_KEYS.key(), customPropertyKeys);
    }

    private void setUri(final String key, final URI uri) {
        applinksAdminPropertySet.putProperty(key, uri != null ? uri.toString() : null);
    }

    private URI getUri(final String key) {
        final String uri = (String) applinksAdminPropertySet.getProperty(key);
        if (uri == null) {
            return null;
        }
        try {
            return new URI(uri);
        } catch (URISyntaxException e) {
            // this will only happen if someone has manually hacked up the property store, and set an invalid URI
            throw new RuntimeException(String.format("Failed to deserialise stored %s URI (%s) reason: %s",
                    key, uri, e.getReason()));
        }
    }

    private void setProviderKeys(final List<String> providerKeys) {
        logSettingProperty(Property.AUTH_PROVIDER_KEYS.key(), providerKeys);
        applinksAdminPropertySet.putProperty(Property.AUTH_PROVIDER_KEYS.key(), providerKeys);
    }

    public boolean authProviderIsConfigured(final String providerKey) {
        return getProviderKeys().contains(providerKey);
    }

    private Properties toProperties(final Map<String, String> map) {
        final Properties props = new Properties();
        props.putAll(map);
        return props;
    }

    @SuppressWarnings("unchecked")
    private Map<String, String> toMap(final Properties props) {
        return Collections.unmodifiableMap(new HashMap<String, String>((Map) props));
    }

    /**
     * Log setting a property.
     * Should be called from setters.
     */
    private void logSettingProperty(String property, Object value) {
        this.logStoringProperty("Setting", property, value);
    }

    /**
     * Log putting a property.
     */
    private void logPuttingProperty(final String property, final Object value) {
        this.logStoringProperty("Putting", property, value);
    }

    /**
     * Log storing a property with some context about where it was bing stored from.
     */
    private void logStoringProperty(final String context, final String property, final Object value) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("%s %s property with value [%s]", context, property, value.toString());
            LOG.debug(message);
        }
    }

    private void logCopyingProperties(Property Key, Object value, ApplicationLinkProperties applicationLinkProperties) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("Copying key [%s] with value [%s] from link to [%s]", Key.key(), value, applicationLinkProperties.getRpcUrl().toASCIIString());
            LOG.debug(message);
        }
    }

    private void logCopyingChildProperty(String childType, String property, Object value, ApplicationLinkProperties applicationLinkProperties) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("Copying %s property [%s] with value [%s] from link to [%s]", childType, property, value, applicationLinkProperties.getRpcUrl().toASCIIString());
            LOG.debug(message);
        }
    }

    private void logRemovingHashedProperty(String context, String property, String hashedProperty, Function<String, Object> valueFunction) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("Removing %s property [%s] hashedProperty [%s] with value [%s]", context, property, hashedProperty, valueFunction.apply(hashedProperty));
            LOG.debug(message);
        }
    }

    private void logRemovingProperty(String property, Function<String, Object> valueFunction) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("Removing property [%s] with value [%s]", property, valueFunction.apply(property));
            LOG.debug(message);
        }
    }

    private void logSettingHashedProperty(String property, String hashedProperty, Function<String, Object> valueFunction) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("Setting property [%s] hashedProperty [%s] with value [%s]", property, hashedProperty, valueFunction.apply(hashedProperty));
            LOG.debug(message);
        }
    }

    private void logSettingHashedProperty(String context, String property, String hashedProperty, Object value) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("Setting %s property [%s] hashedProperty [%s] with value [%s]", context, property, hashedProperty, value);
            LOG.debug(message);
        }
    }

}
