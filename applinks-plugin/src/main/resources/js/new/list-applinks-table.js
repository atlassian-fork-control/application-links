/**
 * NOTE: this file implements Applinks V2 UI, which is no longer in use. Unfortunately some parts of it are still
 * used in V3 UI and therefore it cannot be simply removed without making sure it doesn't break any important 
 * functionality in V3.
 */

AJS.$(document).bind(AppLinks.Event.PREREADY, function() {
    var $ = AJS.$;

    var eventSuccessI18n = {
        "i18n-update": function(applicationName) {
            return AJS.I18n.getText('applinks.link.update.success', applicationName)
        },
        "i18n-relocate": function(applicationName) {
            return AJS.I18n.getText('applinks.link.relocate.success', applicationName)
        },
        "i18n-upgrade": function(applicationName) {
            return AJS.I18n.getText('applinks.link.upgrade.success', applicationName)
        },
        "i18n-delete": function(applicationName) {
            return AJS.I18n.getText('applinks.link.delete.success', applicationName)
        },
        "i18n-primary": function(applicationName) {
            return AJS.I18n.getText('applinks.link.primary.success', applicationName)
        }
    };

    //Bind success messages
    $.each(['update', 'relocate', 'upgrade', 'delete', 'primary'], function(i, current) {
        AppLinks.UI.bind(current, function(event, applicationName) {
            AppLinks.UI.showInfoBox($('<span>').text(eventSuccessI18n['i18n-' + current](applicationName)));
        });
    });

    $.extend(AppLinks.UI || {}, {
        listApplicationLinks: function(appLinkIdOrName, operation, status) {

            // if we are about to redirect don't bother refreshing the list
            if(AppLinks.Creation.aboutToRedirect()) {
                return;
            }

            //otherwise refresh the list
            var applicationList = $('#applicationsList');
            applicationList.removeClass("fully-loaded");
            $('.no-links').hide();
            $('.links-loading').show();
            $('.relocate-warning').closest('.aui-message.aui-message-warning').remove();
            $('.upgrade-warning').closest('.aui-message.aui-message-warning').remove();
            $('#application-links-table').hide();
            applicationList.empty();
            AppLinks.SPI.getAllLinksWithAuthInfo(function(applicationList) {
                var tableBody = $('#applicationsList'),
                    applicationLinksAndAuthInfo = applicationList.list,
                    createRow = function(application, isSystem) {
                        var extensions = {
                            typeLabel: AppLinks.I18n.getApplicationTypeName(application.typeId)
                        };
                        var row;
                        if (isSystem) {
                            row = $(AJS.template.load('al-system-row')
                                .fill($.extend(extensions, application))
                                .fillHtml({applicationId: application.id})
                                .fillHtml({iconTag: application.iconUrl ?
                                    AJS.template.load('al-row-application-icon').fill({iconUrl: application.iconUrl}).toString() :
                                    ""})
                                .toString());
                        } else {
                            row = $(AJS.template.load('al-row')
                                .fill($.extend(extensions, application))
                                .fillHtml({applicationId: application.id})
                                .fillHtml({iconTag: application.iconUrl ?
                                    AJS.template.load('al-row-application-icon').fill({iconUrl: application.iconUrl}).toString() :
                                    ""})
                                .toString());
                            row.find(".app-delete-link").click({application: application}, AppLinks.bindDeleteLink);
                            row.find(".app-edit-link").click({application: application}, AppLinks.bindEditAppLink);

                            if (!application.isPrimary) {
                                row.find(".app-toggleprimary-action").show();
                                row.find(".app-toggleprimary-link").on('click', function(e) {
                                    e.preventDefault();
                                    AppLinks.UI.hideInfoBox();
                                    AppLinks.SPI.makePrimary(application, function() {
                                        AppLinks.UI.listApplicationLinks(application.id, 'primary');
                                    });
                                });
                            } else {
                                row.find(".app-toggleprimary-action").hide();
                            }

                            if ( $('#ual.list-links').data("sysadminFlag")) {
                                AppLinks.SPI.getApplicationLinkState(application.id,  function(result) {
                                    if (result.appLinkState === 'OFFLINE') {
                                        AppLinks.Warnings.relocateApplinkWarning(application);
                                    }
                                    else if (result.appLinkState !== 'OK') {
                                        AppLinks.Warnings.upgradeApplinkWarning(application, result.appLinkState);
                                    }
                                }, function(data) {
                                    var message = AppLinks.parseError(data);
                                    AppLinks.UI.showErrorBox(message);
                                });
                            }
                        }

                        var toAttrib = function(key, value) {
                            return (typeof value === "undefined" || value === null || value === "") ?
                                "" : (key + "='" + value + "'");
                        };
                        var actions = row.find("ul.app-actions");
                        for (var i = 0; i < application.webItems.length; i++) {
                            var wi = application.webItems[i];
                            // render the icon html (if needed)
                            if (wi.iconUrl) {
                                wi.icon = AJS.template.load('al-row-op-webitem-icon').fill(wi).toString()
                            } else {
                                wi.icon = "";
                            }
                            wi.id = toAttrib("id", wi.id);
                            wi.tooltip = toAttrib("tooltip", wi.tooltip);
                            wi.styleClass = toAttrib("styleClass", wi.styleClass);
                            wi.accessKey = toAttrib("accessKey", wi.accessKey);
                            // add the rendered web-item to the row
                            actions.append(AJS.template.load('al-row-op-webitem').fill(wi).toString());
                        }
                        for (var x = 0; x < application.webPanels.length; x++) {
                            var wp = application.webPanels[x];
                            actions.append($("<li>| " + wp.html + "</li>"));
                        }

                        return row;
                    };

                var modifiedApplicationLinkAndAuthInfo;
                $('.links-loading').hide();
                if (applicationLinksAndAuthInfo.length == 0) {
                    $('#application-links-table').hide();
                    $('.no-links').show();
                    $('#add-first-application-link').on('click', function(e) {
                        e.preventDefault();
                        $('#add-application-link').click();
                    });
                } else {
                    $('#application-links-table').show();
                    $('.no-links').hide();
                }

                var multipleTypes = false,
                    tempTypeId;
                for (var x = 0, xx = applicationLinksAndAuthInfo.length; x < xx; x++) {
                    var appInfo = applicationLinksAndAuthInfo[x],
                        application = appInfo.application;
                    application.hasIncoming = appInfo.hasIncomingAuthenticationProviders;
                    application.hasOutgoing = appInfo.hasOutgoingAuthenticationProviders;
                    application.webItems = appInfo.webItems;
                    application.webPanels = appInfo.webPanels;
                    if (!multipleTypes && tempTypeId && tempTypeId == application.typeId) {
                        multipleTypes = true;
                    }
                    tempTypeId = application.typeId;
                    if (appLinkIdOrName && application.id == appLinkIdOrName) {
                        modifiedApplicationLinkAndAuthInfo = appInfo;
                    }

                    tableBody.append(createRow(application, appInfo.isSystem));
                }

                if (multipleTypes) {
                    $('.primary-column').show();
                } else {
                    $('.primary-column').hide();
                }

                if (appLinkIdOrName) {
                    AppLinks.UI.trigger(operation, appLinkIdOrName, operation, status);
                    if (operation == 'new') {
                        var message;
                        if (!modifiedApplicationLinkAndAuthInfo || !modifiedApplicationLinkAndAuthInfo.configuredOutboundAuthenticators || modifiedApplicationLinkAndAuthInfo.configuredOutboundAuthenticators.length == 0) {
                            message = AJS.I18n.getText("applinks.link.create.success.no.authentication", AppLinks.UI.sanitiseHTML(modifiedApplicationLinkAndAuthInfo.application.name));
                        } else if (modifiedApplicationLinkAndAuthInfo.configuredOutboundAuthenticators && modifiedApplicationLinkAndAuthInfo.configuredOutboundAuthenticators.length == 1) {
                            message = AJS.I18n.getText("applinks.link.create.success.with.authentication", AppLinks.UI.sanitiseHTML(modifiedApplicationLinkAndAuthInfo.application.name), AppLinks.I18n.getAuthenticationTypeName(modifiedApplicationLinkAndAuthInfo.configuredOutboundAuthenticators[0]));
                        } else {
                            message = AJS.I18n.getText("applinks.link.create.success.authentication.types", AppLinks.UI.sanitiseHTML(modifiedApplicationLinkAndAuthInfo.application.name), modifiedApplicationLinkAndAuthInfo.configuredOutboundAuthenticators.length);
                        }

                        if (!status.autoConfigurationSuccessful) {
                            message += " " + AJS.I18n.getText("applinks.link.create.autoconfiguration.failed");
                        }
                        AppLinks.UI.showInfoBox(message);

                        if (modifiedApplicationLinkAndAuthInfo.configuredOutboundAuthenticators.length == 0) {
                            $(".page-info").append(" " + AJS.I18n.getText("applinks.link.create.configure.authentication", "<a href='#' id='edit-new-link'>", "</a>"));
                        }

                        if (AppLinks.SPI.showCreateEntityLinkSuggestion()) {
                            /**
                             * Now render the suggestion for the next step:
                             * "Next you should link a Charlie to a FishEye Repository or Crucible Project. You can do this from the
                             * Charlie admin page."
                             */
                            var localEntityTypeString = AppLinks.UI.prettyJoin(localEntityTypeIdStrings,
                                function(key) {
                                    return AppLinks.I18n.getEntityTypeName(key);
                                }, AJS.I18n.getText("applinks.or"));
                            var remoteEntityTypeString = AppLinks.UI.prettyJoin(modifiedApplicationLinkAndAuthInfo.entityTypeIdStrings,
                                function(key) {
                                    return AppLinks.I18n.getEntityTypeName(key);
                                }, AJS.I18n.getText("applinks.or"));
                            var suggestion = AJS.I18n.getText("applinks.link.create.linksuggestion",
                                localEntityTypeString, remoteEntityTypeString);
                            $(".page-info").append("<br>" + suggestion);
                        }

                        $('#edit-new-link').on('click', function(e) {
                            e.preventDefault();
                            AppLinks.UI.hideInfoBox();
                            AppLinks.editAppLink(modifiedApplicationLinkAndAuthInfo.application, 'undefined', false, function(updated) {
                                AppLinks.UI.listApplicationLinks(updated.id, 'update');
                                return true;
                            }, function() {
                                AppLinks.UI.listApplicationLinks();
                                return true;
                            });
                        });
                    } else if (operation == 'upgrade') {
                        if (status && status.message && $.isArray(status.message) && status.message.length > 0) {
                            AppLinks.UI.showWarningBox(status.message);
                        }
                    }
                }
                tableBody.addClass("fully-loaded");
            }, function(data) {
                $('.links-loading').hide();
                var message = AppLinks.parseError(data);
                AppLinks.UI.showErrorBox(message);
            });

            AppLinks.UI.refreshOrphanedTrust();
        }
    });
});