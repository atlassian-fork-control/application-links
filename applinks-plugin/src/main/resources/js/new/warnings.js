(function($){
AppLinks.Warnings = {
    upgradeApplinkWarning: function(application, appLinkState) {
        var upgradeLink = applinks.templates.list.getUpgradeWarning({name: application.name, id: application.id});
        AppLinks.UI.showWarningBox(upgradeLink);

        $('.upgrade-' + application.id).click(appLinkState, function (e) {
            var upgradeDialogSettings;
            e.preventDefault();
            AppLinks.UI.hideInfoBox();
            if (e.data === 'UPGRADED_TO_UAL') {
                upgradeDialogSettings = {
                    application: application,
                    description: AJS.I18n.getText('applinks.ual.upgrade.description', application.name),
                    manifest: {
                        name: application.name,
                        typeId: application.typeId
                    },
                    successCallback: function (upgradedApplicationLink) {
                        // called when the upgrade completed successfully
                        AppLinks.UI.listApplicationLinks(upgradedApplicationLink.applicationLink.name, 'upgrade', upgradedApplicationLink);
                    }
                };
                AppLinks.showUpgradeLinkToUALDialog(upgradeDialogSettings);
            } else {
                var localEntityTypeString = AppLinks.UI.prettyJoin(localEntityTypeIdStrings,
                    function (key) {
                        return AppLinks.I18n.getEntityTypeName(key);
                    }, AJS.I18n.getText('applinks.and'));
                upgradeDialogSettings = {
                    application: application,
                    description: AJS.I18n.getText('applinks.legacy.upgrade.description', application.name, localEntityTypeString),
                    helpKey: 'applinks.docs.upgrade.application.link',
                    submit: function (success, error) {
                        AppLinks.SPI.legacyUpgrade(application, success, error);
                    },
                    callback: function (newApplicationId) {
                        application.applicationId = newApplicationId;
                        AppLinks.UI.listApplicationLinks(application.name, 'upgrade');
                    }
                };
                AppLinks.showUpgradeLinkDialog(upgradeDialogSettings);
            }
        });
    },
    relocateApplinkWarning: function(application) {
        var relocateLink;
        AppLinks.UI.showWarningBox(applinks.templates.list.getRelocateWarning({name:application.name, id:application.id, rpcUrl: application.rpcUrl}));
        relocateLink = $('.relocate-' + application.id);
        relocateLink.on('click', function(e) {
            e.preventDefault();
            AppLinks.UI.hideInfoBox();
            var relocateDialogSettings = {
                description: AJS.I18n.getText('applinks.relocate.long', application.name),
                application: application,
                helpKey: 'applinks.docs.relocate.application.link',
                doRelocate: function(newUrl, success, error) {
                    AppLinks.SPI.relocate(application, newUrl, false, success, error);
                },
                doForceRelocate: function(newUrl, success, error) {
                    AppLinks.SPI.relocate(application, newUrl, true, success, error);
                },
                callback: function() {
                    AppLinks.UI.listApplicationLinks(application.name, 'relocate');
                }
            };
            AppLinks.showRelocateLinkDialog(relocateDialogSettings);
        });
    }
};
})(AJS.$);