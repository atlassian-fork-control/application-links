AJS.$(document).bind(AppLinks.Event.PREREADY, function() {
    AppLinks = AJS.$.extend(window.AppLinks || {}, {
        confirmDialog: function(title, message, confirm, cancel) {
            if (!cancel) cancel = function() {};

            var popup = new AJS.Dialog({
                width: 450,
                height: 180,
                id: 'confirm-dialog',
                onCancel: cancel
            });

            popup.addHeader(title);
            popup.addPanel(title);
            popup.getCurrentPanel().html(message);

            var complete = function(callback) {
                popup.remove();
                callback.call(popup);
            };

            popup.addButton(AJS.params.statusDialogAddButtonLabel || AJS.I18n.getText('applinks.confirm'), function() {
                complete(confirm);
            }, "confirm");
            popup.addButton(AJS.params.statusDialogCancelButtonLabel || AJS.I18n.getText('applinks.cancel'), function() {
                complete(cancel);
            }, "cancel");

            popup.show();
            popup.popup.element.addClass('aui-dialog-content-ready');
        }
    });

});
