#* @vtlvariable name="i18n" type="com.atlassian.sal.api.message.I18nResolver" *#
#* @vtlvariable name="context" type="com.atlassian.applinks.ui.velocity.ListApplicationLinksContext" *#
#* @vtlvariable name="docLinker" type="com.atlassian.applinks.internal.common.docs.DocumentationLinker" *#
#parse("/com/atlassian/applinks/core/common/_help_link.vm")
#parse("/com/atlassian/applinks/core/common/_xsrf_token_element.vm")

<html>
<head>
    <title>${i18n.getText("applinks.list.title")}</title>
    <meta name="decorator" content="atl.admin">
    <meta name="admin.active.tab" content="configure-application-links"/>
</head>
<body>
    <div id="ual" class="ual-$context.applicationType list-links applinks-admin-v3" data-sysadmin-flag="${context.isSysadmin()}">
        <script type="text/javascript">
            var nonAppLinksApplicationTypes = $context.nonAppLinksApplicationTypes;
            var applicationName = "$context.applicationName";
            var localEntityTypeIdStrings = $context.localEntityTypeIdStrings;
        </script>

        <header class="aui-page-header">
            <div class="aui-page-header-inner">
                <div class="aui-page-header-main applinks-page-header">
                    <h2>${i18n.getText("applinks.configure.title")} #help('applinks.docs.administration.guide')</h2>
                </div>
                <div class="aui-page-header-actions">
                        <div class="aui-buttons">
                            <div class="aui-button aui-button-link" id="applinks-feedback-button" style="display: none;">
                                <span class="aui-icon jira-projects-feedback-collector-icon"></span>
                                ${i18n.getText("applinks.givefeedback")}
                            </div>
                        </div>
                </div>
            </div>
        </header>

        #parse("com/atlassian/applinks/ui/admin/common_header.vm")

        <form id="createApplicationLink" name="createApplicationLink" method="post" action="#" class="aui">
            <div class="field-group v3-url-input">
                <input type="text" class="text long-field" name="applinks-url-entered" id="applinks-url-entered" title="${i18n.getText(" applinks.app.url.v2")}" placeholder="${i18n.getText("applinks.app.url.placeholder")}">
                <input type="hidden" class="text long-field" name="applinks-url-corrected" id="applinks-url-corrected">
                <button id="applinks-create-button" class="aui-button aui-button-primary">${i18n.getText("applinks.create.createButton")}</button>
            </div>
        </form>

        <div id="agent-page">
            <div class="content"></div>
        </div>

        ## the create-new-applink wizard:
        <div id="create-application-link-container" style="display: none;">
            #if (!${context.isSysadmin()})
            #set ($steps = 2)
            #end
            #parse("com/atlassian/applinks/ui/admin/applinkwizard/nonual.vm")
            #if (!${context.isSysadmin()})
            #set ($submit = true)
            #end
            #parse("com/atlassian/applinks/ui/admin/applinkwizard/reciprocal_link.vm")
            #if (${context.isSysadmin()})
            #parse("com/atlassian/applinks/ui/admin/applinkwizard/authentication.vm")
            #end
        </div>

        ## the non-ual to ual upgrade wizard:
        <div id="upgrade-application-link-container" style="display: none;">
            #parse("com/atlassian/applinks/ui/admin/applinkwizard/upgradeinfo.vm")
            #parse("com/atlassian/applinks/ui/admin/applinkwizard/reciprocal_link.vm")
            #parse("com/atlassian/applinks/ui/admin/applinkwizard/authentication.vm")
        </div>

        ## the upgrade orphaned trust configs wizard:
        <div id="upgrade-orphaned-trust-container" style="display: none;">
            #set ($title = "applinks.orphaned.trust.upgrade.link")
            #set ($steps = 2)
            #parse("com/atlassian/applinks/ui/admin/applinkwizard/urlinput.vm")
            #parse("com/atlassian/applinks/ui/admin/applinkwizard/nonual.vm")
            #set ($submit = true)
            #parse("com/atlassian/applinks/ui/admin/applinkwizard/reciprocal_link.vm")
        </div>

        <div id="editAppLinkDialog" style="display: none;">
            <div id="applinkDetails" title="${i18n.getText('applinks.details.long')}" submit="true">
                <div id="applinkDetailsPage" title="${i18n.getText('applinks.details.short')}" panel="true">
                    <form action="#" method="post" class="aui" id="application-link-details-form" onSubmit="return false;">
                        #xsrfTokenElement()
                        <div class="field-group">
                            <label for="applicationName">${i18n.getText("applinks.details.appname")}<span class="aui-icon icon-required"></span></label>
                            <input class="text medium-field" id="applicationName" type="text" title="${i18n.getText('applinks.details.appname.description')}" autocomplete='off'/>
                        </div>
                        <div class="field-group">
                            <label for="applicationTypeValue">${i18n.getText("applinks.details.application.type")}</label>
                            <span class="field-value" id="applicationTypeValue"></span>
                            <input type="hidden" id="appId"/>
                        </div>
                        <div class="field-group">
                            <label for="rpc-url">${i18n.getText("applinks.app.url")}</label>
                            <span class="field-value" id="rpc-url" name="rpc-url" title="${i18n.getText('applinks.app.url')}"></span>

                            <div class="description">${i18n.getText("applinks.details.url")}</div>
                        </div>
                        <div class="field-group">
                            <label for="display-url">${i18n.getText("applinks.details.display.url")}<span class="aui-icon icon-required"></span></label>
                            <input class="text long-field" id="display-url" type="text" title="${i18n.getText('applinks.details.display.url.description')}" autocomplete='off'/>

                            <div class="description">${i18n.getText("applinks.details.display.url.description")}</div>
                        </div>
                        <div class="update-validation-errors"></div>
                    </form>
                </div>
                <div id="outgoing-authentication-page" title="${i18n.getText('applinks.outgoing.auth')}" panel="true" class="auth-panel">
                    <iframe id="outgoing-auth" class="auth-frame" frameborder="0"></iframe>
                </div>
                <div id="incoming-authentication-page" title="${i18n.getText('applinks.incoming.auth')}" panel="true" class="auth-panel">
                    <iframe id="incoming-auth" class="auth-frame" frameborder="0"></iframe>
                </div>
            </div>
        </div>

        #parse("com/atlassian/applinks/ui/admin/relocate_link_dialog.vm")
        #parse("com/atlassian/applinks/ui/admin/upgrade_link_dialog.vm") ## legacy upgrade

        #if (!$context.orphanedTrustCertificates.empty)
        #parse("com/atlassian/applinks/ui/admin/orphaned_trust_dialog.vm")
        #end

        #parse("com/atlassian/applinks/ui/admin/delete_link_dialog.vm")
        #parse("com/atlassian/applinks/ui/admin/auth_dialog.vm")
    </div>

    <script type="text/x-template" title="al-row"></script>
    <script type="text/x-template" title="al-row-application-icon">
        <img src='{iconUrl}' alt="" width=16px height=16px>
    </script>
    <script type="text/x-template" title="al-row-op-webitem">
        <li>
            | <a {id} href='{url}' {styleClass} {tooltip} {accessKey}>{icon} {label}</a>
        </li>
    </script>
    <script type="text/x-template" title="al-row-op-webitem-icon">
        <img src='{iconUrl}' width=16px height=16px' alt="">
    </script>
    ## For confluence
    <content tag="selectedWebItem" hidden>applinks-configure-application-links</content>
    <content tag="bodyClass" hidden>applinks-page</content>
    <script type="text/javascript">
        require('applinks/page/v3').init();
    </script>
</body>
</html>
