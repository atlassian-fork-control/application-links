# I18n messages for V3 status and diagnostics UI
# NOTE: title messages need to conform to the pattern: applinks.status.<error_type_lowercase>.title (this is enforced by a unit test)

# Status lozenges
applinks.admin.v3.error.INCOMPATIBLE=Status unavailable
applinks.admin.v3.error.NON_ATLASSIAN=Non-Atlassian
applinks.admin.v3.error.SYSTEM=System
applinks.admin.v3.error.NETWORK_ERROR=Network error
applinks.admin.v3.error.ACCESS_ERROR.INSUFFICIENT_REMOTE_PERMISSION=Access denied
applinks.admin.v3.error.CONFIG_ERROR=Config error
applinks.admin.v3.error.DEPRECATED=Deprecated
applinks.admin.v3.error.DISABLED=Disabled
applinks.admin.v3.connected=Connected

# Auth token missing
applinks.status.local_auth_token_required.title=Authenticate to see status
applinks.status.remote_auth_token_required.title=Authenticate to see status


# Diagnostics dialogs contents
applinks.status.troubleshoot.short=Troubleshoot
applinks.status.troubleshoot.full=Troubleshoot this problem

applinks.status.connection_refused.title=The remote application is not responding
applinks.status.connection_refused.description=The connection to {0} was refused. <a href="{1}" target="_blank">{2}</a> \
  may be down or blocked by a firewall. Alternatively, the Application URL for the remote application may be incorrect or may have \
  changed recently.

applinks.status.unknown_host.title=Unable to reach the remote application
applinks.status.unknown_host.description=We couldn''t resolve the hostname at {0}. There may be a DNS configuration \
  problem, or the Application URL for the remote application may be incorrect or may have changed recently.

applinks.status.oauth_problem.title=OAuth problem
applinks.status.oauth_problem.description=There is an issue with the OAuth configuration of the connection to \
  <a href="{0}" target="_blank">{1}</a>.

applinks.status.oauth_timestamp_refused.title=The system clocks are not synchronized
applinks.status.oauth_timestamp_refused.description=This server has a different system time from \
  <a href="{0}" target="_blank">{1}</a>, which prevents the applications from authenticating.

applinks.status.oauth_signature_invalid.title=Invalid OAuth signature
applinks.status.oauth_signature_invalid.description=We couldn''t connect to <a href="{0}" target="_blank">{1}</a>, possibly \
  because that instance is behind a misconfigured proxy.

applinks.status.unexpected_response.title=Unexpected response
applinks.status.unexpected_response.description=We received an unexpected response from \
  <a href="{0}" target="_blank">{1}</a>. If the remote application is behind a proxy, check that the application is \
  operational. Troubleshoot this for other possible causes.
applinks.status.unexpected_response.checklogs=To see the full response content look at your application logs.

applinks.status.auth_level_mismatch.title=OAuth mismatch
applinks.status.auth_level_mismatch.incoming=incoming
applinks.status.auth_level_mismatch.outgoing=outgoing
applinks.status.auth_level_mismatch.impersonation=OAuth with impersonation
applinks.status.auth_level_mismatch.disabled.oauth.description=The local {0} connection is disabled, however <a href="{1}" target="_blank">{2}</a> uses {3}.
applinks.status.auth_level_mismatch.oauth.oauth.description=The local {0} connection uses {1}, however <a href="{2}" target="_blank">{3}</a> uses {4}.
applinks.status.auth_level_mismatch.oauth.disabled.description=The local {0} connection uses {1}, however it''s disabled on <a href="{2}" target="_blank">{3}</a>.
applinks.status.auth_level_mismatch.suggest.description=You''ll need to edit each connection to make sure the authentication \
  methods are the same.

applinks.status.unknown.title=Network error
applinks.status.unknown.description=We couldn''t connect to {0} at {1}. This might have been caused by a network \
  error or another configuration error.

applinks.status.auth_level_unsupported.title=Unsupported OAuth level
applinks.status.auth_level_unsupported.description=The current OAuth configuration prevents integrations from working. \
  You need to update your configuration to {0}OAuth{1} or {2}OAuth with impersonation{3}.

applinks.status.ssl_untrusted.title=The remote certificate can''t be trusted
applinks.status.ssl_untrusted.description=<a href={0} target="_blank">{1}</a> may be using a self-signed SSL certificate or a certificate that was issued by a certificate authority that isn''t known locally.

applinks.status.ssl_hostname_unmatched.title=The remote certificate doesn''t match the remote host name
applinks.status.ssl_hostname_unmatched.description=The SSL certificate used by <a href={0} target="_blank">{1}</a> can''t be trusted because the certificate''s host name doesn''t match the URL {2}.

applinks.status.ssl_unmatched.title=Unable to verify remote certificate details
applinks.status.ssl_unmatched.description=The SSL certificate used by <a href={0} target="_blank">{1}</a> can''t be trusted because details inside the certificate can''t be verified. Check that the remote certificate has the correct details.

applinks.status.system_link.title=Internal System Link
applinks.status.system_link.description=This is an internal link to a Cloud server, created automatically by the system. System internal links are read-only and cannot be edited.

applinks.status.generic_link.title=Link to external third party
applinks.status.generic_link.description=This is an application link to an external third party. We are unable to retrieve the status to third parties. If you are experiencing any problems with your connection ensure that your OAuth configuration is correct.

applinks.status.remote_version_incompatible.title=Unable to retrieve status
applinks.status.remote_version_incompatible.description=We''re unable to determine the status of this link, because the version of {0} is a little too old. The link may actually be good. \
    You can upgrade <a href="{1}" target="_blank">{0}</a> to the latest version of {2} to get status and diagnostic capabilities.

applinks.status.non_atlassian.title=Link to external third party
applinks.status.non_atlassian.description=This is an application link to an external third party. We are unable to retrieve the status to third parties, so if you are experiencing any problems with your connection reach out to the third party directly.

applinks.status.no_remote_applink.title=No remote application link
applinks.status.no_remote_applink.description=We can''t connect with <a href={0} target="_blank">{1}</a>  because a reciprocal application link is missing on that server. \
  You''ll need to delete and then re-create this application link.

applinks.status.no_outgoing_auth.title=No outgoing authentication
applinks.status.no_outgoing_auth.description=We can''t display the status for this application link, because outgoing authentication is disabled. You can {0}enable outgoing authentication{1} to see the status.

applinks.status.insufficient_remote_permission.title=Access denied

applinks.status.insufficient_remote_permission.2LOi=We couldn''t retrieve the status from the remote application because your user account doesn''t have admin privileges on {0}.
applinks.status.insufficient_remote_permission.2LOi.ext=To see the status, ask for admin access for your account on <a href="{0}" target="_blank">{1}</a>.

applinks.status.insufficient_remote_permission.3LO=We couldn''t retrieve the status from the remote application because you don''t have admin privileges on {0}.
applinks.status.insufficient_remote_permission.3LO.ext=To see the status, log out of <a href="{0}" target="_blank">{1}</a> and then {2}authenticate{3} as an admin on {1}.

# those error type keys are not used in the dialogs/messages, but included to ensure correctness of
# ApplinkErrorType#getI18nKey
applinks.status.unexpected_response_status.title=Unexpected response status
applinks.status.disabled.title=Application Link is disabled

#migration status dialog messages
applinks.status.legacy_update.title=Deprecated authentication type
applinks.status.legacy_update.description.1=This application link uses either the Trusted Applications or Basic Access authentication type, both of which are deprecated.
applinks.status.legacy_update.description.2=You should update this link to use OAuth authentication, the industry standard.

applinks.status.legacy_removal.title=Deprecated authentication type
applinks.status.legacy_removal.description.1=As well as OAuth, this application link still uses either the Trusted Applications or Basic Access authentication type, both of which are deprecated.
applinks.status.legacy_removal.description.2=You should update this link to use OAuth authentication, the industry standard.

applinks.status.manual_legacy_update.title=Deprecated authentication type
applinks.status.manual_legacy_update.description.1=This application link uses either the Trusted Applications or Basic Access authentication type, both of which are deprecated.
applinks.status.manual_legacy_update.description.2=You should manually update this link to use OAuth authentication, the industry standard.

applinks.status.manual_legacy_removal.title=Deprecated authentication type
applinks.status.manual_legacy_removal.description.1=This application link uses a deprecated authentication type.
applinks.status.manual_legacy_removal.description.2=You should click <b>Update locally</b> to move to OAuth authentication. Doing this will remove Trusted Applications authentication from this end of the link.
applinks.status.manual_legacy_removal.description.3=You''ll then need to manually remove Trusted Applications authentication from the <a href={0} target="_blank">{1}</a> end of the link.

applinks.status.manual_legacy_removal_with_old_edit.title=Deprecated authentication type
applinks.status.manual_legacy_removal_with_old_edit.description.1=This application link uses a deprecated authentication type.
applinks.status.manual_legacy_removal_with_old_edit.description.2=You''ll need to manually remove Trusted Applications authentication from the <a href={0} target="_blank">{1}</a> end of the link.

appinks.status.manual.migration.temporary.admin.suggestion=You might be able to automatically update this link, if you obtain temporary sysadmin permissions on <a href={0} target="_blank">{1}</a> and then refresh this page.

applinks.update.locally=Update locally

