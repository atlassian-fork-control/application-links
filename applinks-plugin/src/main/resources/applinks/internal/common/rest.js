define('applinks/common/rest', [
    'applinks/lib/aui',
    'applinks/common/preconditions',
    'applinks/common/rest-request',
    'applinks/common/rest-oauth'
], function(
    AJS,
    Preconditions,
    ApplinksRestRequest,
    ApplinksOAuthRest
) {
    var PARAM_DATA = 'data';

    function ApplinksRestModule(version) {
        Preconditions.hasValue(version, 'version');
        this._version = version;
    }

    ApplinksRestModule.prototype.baseUrl = function() {
        return AJS.contextPath() + '/rest/applinks/' + this._version;
    };

    ApplinksRestModule.prototype.withPath = function(path) {
        return this.baseUrl() + '/' + path;
    };

    function ApplinksV1RestModule() {
        this.module = new ApplinksRestModule('1.0');
    }

    ApplinksV1RestModule.prototype.applink = function(applinkId) {
        Preconditions.hasValue(applinkId, 'applinkId');
        return new ApplinksRestRequest(this.module.withPath('applicationlink/' + applinkId));
    };

    function ApplinksV2RestModule() {
        this.module = new ApplinksRestModule('2.0');
    }

    function ApplinksV3RestModule() {
        this.module = new ApplinksRestModule('3.0');
    }

    ApplinksV3RestModule.prototype.applink = function(applinkId, dataKeys) {
        Preconditions.hasValue(applinkId, 'applinkId');
        var request = new ApplinksRestRequest(this.module.withPath('applinks/' + applinkId));
        return _addDataKeys(request, dataKeys);
    };

    ApplinksV3RestModule.prototype.applinks = function(dataKeys) {
        var request = new ApplinksRestRequest(this.module.withPath('applinks'));
        return _addDataKeys(request, dataKeys);
    };

    ApplinksV3RestModule.prototype.featureDiscovery = function(featureKey) {
        Preconditions.hasValue(featureKey, 'featureKey');
        return new ApplinksRestRequest(this.module.withPath('feature-discovery/' + featureKey));
    };

    ApplinksV3RestModule.prototype.features = function(featureName) {
        Preconditions.hasValue(featureName, 'featureName');
        return new ApplinksRestRequest(this.module.withPath('features/' + featureName));
    };

    ApplinksV3RestModule.prototype.status = function(applinkId) {
        Preconditions.hasValue(applinkId, 'applinkId');
        return new ApplinksRestRequest(this.module.withPath('status/' + applinkId));
    };

    ApplinksV3RestModule.prototype.oAuthStatus = function(applinkId) {
        Preconditions.hasValue(applinkId, 'applinkId');
        return new ApplinksRestRequest(this.module.withPath('status/' + applinkId + '/oauth'));

    };

    ApplinksV3RestModule.prototype.migration = function(applinkId) {
        Preconditions.hasValue(applinkId, 'applinkId');
        return new ApplinksRestRequest(this.module.withPath('migration/' + applinkId));
    };

    function _addDataKeys(request, dataKeys) {
        if (dataKeys) {
            Preconditions.isArray(dataKeys, 'dataKeys');
            _.each(dataKeys, function (dataKey) {
                request = request.queryParam(PARAM_DATA, dataKey);
            });
        }
        return request;
    }

    var V3Module = new ApplinksV3RestModule();

    /**
     * Data keys supported by the Applinks V3 resource
     */
    V3Module.ApplinkData = {
        APPLICATION_VERSION: 'applicationVersion',
        ATLASSIAN: 'atlassian',
        CAPABILITIES: 'capabilities',
        CONFIG_URL: 'configUrl',
        ICON_URI: 'iconUri',
        EDITABLE: 'editable',
        V3_EDITABLE: 'v3Editable',
        WEB_ITEMS: 'webItems'
    };

    return {
        V1: new ApplinksV1RestModule(),
        V2: new ApplinksV2RestModule(),
        V3: V3Module,
        OAuth: ApplinksOAuthRest
    }
});
