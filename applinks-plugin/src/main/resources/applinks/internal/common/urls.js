define('applinks/common/urls', [
    'applinks/lib/aui',
    'applinks/lib/lodash',
    'applinks/common/products'
], function(
    AJS,
    _,
    Products
) {
    var APPLINKS_SERVLET_PATH = '/plugins/servlet/applinks';

    var baseServletPath = _.memoize(function() {
        return AJS.contextPath() + APPLINKS_SERVLET_PATH;
    });

    /**
     * Utility functions for generating URLs in JS, including a link to applinks admin screen.
     */
    var ApplinksUrls = {

        /**
         * Generate a URL from the base and parameters.
         * @param baseUrl the baseUrl
         * @param parametersMap a map of parameters to add, in the form of { parameterName :  parameterValue }
         * @return {String}
         */
        generateUrl: function(baseUrl, parametersMap) {
            var redirectUrl = baseUrl;

            if(_.isUndefined(parametersMap)) {
                return redirectUrl;
            } else {
                _.each(parametersMap, function(value, key) {
                    if(redirectUrl.indexOf('?') < 0) {
                        redirectUrl = redirectUrl + '?';
                    } else {
                        redirectUrl = redirectUrl + '&';
                    }
                    redirectUrl = redirectUrl + key + "=" + encodeURIComponent(JSON.stringify(value))
                });

                return redirectUrl;
            }
        }
    };

    ApplinksUrls.Local = {

        admin: function(params) {
            var url = baseServletPath() + '/listApplicationLinks';
            return ApplinksUrls.generateUrl(url, params);
        },
        edit: function(applinkId, params) {
            var url = baseServletPath() + '/edit/' + applinkId;
            return ApplinksUrls.generateUrl(url, params);
        }
    };

    ApplinksUrls.Remote = {

        /**
         * Generate a URL to remote Applinks Admin screen. For compatibility it needs to accept `applicationTypeId`.
         *
         * @param remoteBaseUrl
         * @param applicationTypeId
         * @param params extra URL params
         * @returns {String}
         */
        admin: function(remoteBaseUrl, applicationTypeId, params) {
            var suffix = applicationTypeId === Products.CONFLUENCE ?
                '/admin/listapplicationlinks.action' :
            APPLINKS_SERVLET_PATH + '/listApplicationLinks';
            var url = remoteBaseUrl + suffix;

            return ApplinksUrls.generateUrl(url, params);
        }
    };

    return ApplinksUrls;
});