define('applinks/common/response-handlers', [
    'applinks/lib/aui',
    'applinks/lib/console',
    'applinks/lib/window',
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/common/preconditions',
    'applinks/common/dialog-helper',
    'applinks/common/response-status'
], function(
    AJS,
    console,
    window,
    $,
    _,
    Preconditions,
    DialogHelper,
    ResponseStatus
) {
    var ERROR_DIALOG_ID = 'applinks-error-dialog';
    var ERROR_DIALOG_CLOSE_BTN_ID = ERROR_DIALOG_ID + '-close-button';

    function _toArray() {
        return _.isArray(arguments[0]) ? arguments[0] : _.toArray(arguments);
    }

    function _parseResponseText(request) {
        if (!request.responseText) {
            return null;
        }
        try {
            return JSON.parse(request.responseText)
        } catch(error) {
            console.warn('Unable to parse REST error response text - not a JSON response.');
            return null;
        }
    }

    function _showDialog(request) {
        if (DialogHelper.isAnyDialogOpen()) {
            console.debug('Not showing REST error dialog - another modal dialog already open');
            return;
        }
        var jsonResponse = _parseResponseText(request);
        var responseErrors = jsonResponse && jsonResponse.errors && jsonResponse.errors.length > 0 ? jsonResponse.errors : null;
        var dialogContents = applinks.common.resterror.dialogContents({id: ERROR_DIALOG_ID, errors: responseErrors});

        $(DialogHelper.getContainer()).append(dialogContents);
        var dialog = AJS.dialog2('#' + ERROR_DIALOG_ID);

        $('#'+ERROR_DIALOG_CLOSE_BTN_ID).on('click', function(e) {
            e.preventDefault();
            dialog.hide();
            dialog.remove();
        });
        dialog.show();
    }

    /**
     * Create a success handler for ajax calls decorated with expected statuses.
     *
     * @param {Array} arguments can be an array of statuses or statuses as multiple arguments, e.g. :
     *          done([ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND])
     *          done(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND)
     *
     * @returns {Function(data, textStatus, request)} success handler for ajax call decorated with expected statuses
     */
    function done() {
        var statuses = _toArray.apply(this, arguments);

        return function(data, textStatus, request) {
            if (!_.some(statuses, function(status) { return status.matches(request) })) {
                console.error('Unexpected response status: ' + request.status + ': ' + request.responseText);
            }
        }
    }

    /**
     * Create a failure handler for ajax calls decorated with expected statuses.
     *
     * @param {Array} arguments can be an array of statuses or statuses as multiple arguments, e.g. :
     *          fail([ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND])
     *          fail(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND)
     *
     * @returns {Function(request, textStatus, error)} failure handler for ajax call decorated with expected statuses
     */
    function fail() {
        var statuses = _toArray.apply(this, arguments);

        return function(request, textStatus, error) {
            if (!_.some(statuses, function (status) { return status.matches(request) })) {
                console.error('Unexpected response status: ' + request.status + ': ' + request.responseText);

                if (ResponseStatus.UNAUTHORIZED.matches(request)) {
                    window.location.reload();
                } else {
                    _showDialog(request);
                }
            }
        }
    }

    return {
        done: done,
        fail: fail
    };
});
