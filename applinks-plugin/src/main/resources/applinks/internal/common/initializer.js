define('applinks/common/initializer', [
    'applinks/lib/jquery',
    'applinks/lib/lodash'
], function(
    $,
    _
) {
    function _callInitCallback(module) {
        if (module && _.isFunction(module.init)) {
            module.init();
        }
        if (module && _.isFunction(module.initialize)) {
            module.initialize();
        }
    }

    return {

        /**
         * Init each module in the arguments list. If the module contains an `init` or `initialize` function,
         * it will be called immediately. If both functions are present, both will be called.
         *
         * @param arguments list modules to initialize
         */
        init: function() {
            _.each(arguments, function(module) {
                _callInitCallback(module);
            });

        },

        /**
         * Init each module in the arguments list on DOM ready. If the module contains an `init` or `initialize`
         * function, it will be called on DOM ready event. Those functions should contain code that performs UI
         * initialization dependent on the state of the DOM. If both functions are present, both will be called.
         *
         * @param arguments list modules to initialize
         */
        initOnDomReady: function() {
            _.each(arguments, function(module) {
                $(function() {
                    _callInitCallback(module);
                });
            });
        }
    }
});
