define('applinks/common/browser', [
    'applinks/lib/window'
], function(Window) {

    var browser = {
        isMicrosoft: function () {
            var userAgent = Window.navigator.userAgent;
            return userAgent.indexOf("MSIE") > 0 ||     // IE < 11
                !!userAgent.match(/Trident\/7\./) || // IE == 11
                userAgent.indexOf('Edge/') > 0;      // Edge
        }
    };
    return browser;
});