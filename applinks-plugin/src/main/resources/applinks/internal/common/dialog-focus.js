define('applinks/common/dialog-focus', [
    'applinks/lib/lodash',
    'applinks/common/preconditions'
], function(
    _,
    Preconditions
){
    return {
        defaultFocus: function($el) {
            Preconditions.hasValue($el, '$el');
            _.defer(function() {
                $el.find('.applinks-default-focus').first().focus();
            });
        }
    }
});
