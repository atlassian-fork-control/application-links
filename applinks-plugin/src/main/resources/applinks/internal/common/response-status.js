define('applinks/common/response-status', [
    'applinks/lib/lodash'
], function(
    _
) {

    function StatusFamily(familyCode) {
        this.familyCode = familyCode;
    }

    StatusFamily.prototype.matches = function(code) {
        return Math.floor(_getStatusCode(code) / 100) == this.familyCode;
    };

    var StatusFamilies = {
        INFORMATIONAL: new StatusFamily(1),
        SUCCESSFUL: new StatusFamily(2),
        REDIRECTION: new StatusFamily(3),
        CLIENT_ERROR: new StatusFamily(4),
        SERVER_ERROR: new StatusFamily(5),

        forCode: function(code) {
            return _.find(StatusFamilies.all(), function(family) {
                return family.matches(code);
            });
        },

        all: function() {
            return [
                StatusFamilies.INFORMATIONAL,
                StatusFamilies.SUCCESSFUL,
                StatusFamilies.REDIRECTION,
                StatusFamilies.CLIENT_ERROR,
                StatusFamilies.SERVER_ERROR
            ];
        }
    };

    function ResponseStatus(code) {
        this.code = code;
        this.family = StatusFamilies.forCode(code);
    }

    ResponseStatus.prototype.matches = function(code) {
        return _getStatusCode(code) == this.code;
    };

    function _getStatusCode(object) {
        if (object && object.status && _.isNumber(object.status)) {
            return object.status;
        } else if (_.isNumber(object)) {
            return object;
        } else {
            return 0;
        }
    }

    return {
        OK: new ResponseStatus(200),
        CREATED: new ResponseStatus(201),
        NO_CONTENT: new ResponseStatus(204),

        BAD_REQUEST: new ResponseStatus(400),
        UNAUTHORIZED: new ResponseStatus(401),
        FORBIDDEN: new ResponseStatus(403),
        NOT_FOUND: new ResponseStatus(404),
        CONFLICT: new ResponseStatus(409),

        SERVER_ERROR: new ResponseStatus(500),

        Family: StatusFamilies
    }
});
