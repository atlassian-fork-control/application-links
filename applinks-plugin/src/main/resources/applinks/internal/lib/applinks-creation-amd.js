/**
 * Define the Applinks creation resources  as AMD module to facilitate unit testing of some modules
 */
define('applinks/lib/creation', function() {
    return {
        get: function () {
            //required since code that depends on this global is run before the global is defined,
            // can be eliminated once this module is made properly AMD
            return AppLinks.Creation;
        }
    }
});