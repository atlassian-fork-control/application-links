// shim for the Applinks.Creation global, to be transformed into a good AMD law abiding citizen
define('applinks/feature/creation', function () {
    var creation = window.AppLinks.Creation;
    creation.isInProgress = function () {
        // if creation is in progress, the creation log array will be available
        var creationLog = creation.getSubmittedStatusLog();
        return creationLog && creationLog.length;
    };
    return creation;
});