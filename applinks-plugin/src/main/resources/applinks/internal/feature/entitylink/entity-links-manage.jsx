import React from 'react';
import ReactDOM from 'react-dom';
import EntityLinks from './entity-links';

export function initLinks(el, data) {
    const { entityLinks, applicationLinks, currentApp, projectKey, type } = data;
    ReactDOM.render(
        <EntityLinks
            entityLinks={entityLinks}
            applicationLinks={applicationLinks}
            currentApp={currentApp}
            projectKey={projectKey}
            type={type}
        />,
        el
    );
}
