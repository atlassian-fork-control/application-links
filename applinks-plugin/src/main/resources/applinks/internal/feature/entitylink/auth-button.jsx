import { I18n } from '@atlassian/wrm-react-i18n';
import Button from '@atlaskit/button';
import React, { Component } from 'react';

const windowName = 'com_atlassian_applinks_authentication';

export function authorize(url, callback) {
    let authWindow;
    window.oauthCallback = {
        success: () => {
            console.log('sucessfully authenticated');
            if (callback) {
                callback();
            }
            authWindow.close();
        },
    };
    authWindow = window.open(url, windowName);
}

export default class AuthButton extends Component {
    authorize() {
        return authorize(this.props.url, this.props.successCallback);
    }

    render() {
        return (
            <Button appearance="primary" className="auth-button" onClick={e => this.authorize()}>
                {I18n.getText('entitylinks.dialog.authorize',
                    this.props.currentApp
                )}
            </Button>
        );
    }
}
