import { I18n } from '@atlassian/wrm-react-i18n';
import React, { Component } from 'react';
import SectionMessage from '@atlaskit/section-message';
import ModalDialog, { ModalFooter } from '@atlaskit/modal-dialog';
import Button, { ButtonGroup } from '@atlaskit/button';
import { canDeleteEntityLinkResponse, deleteEntityLink, PermissionCode } from './EntitylinksRest';
import Form, { CheckboxField } from '@atlaskit/form';
import { Checkbox } from '@atlaskit/checkbox';
import { authorize } from './auth-button';
import Spinner from '@atlaskit/spinner';

const AuthStatus = {
    UNKNOWN: 'unknown',
    SUCCESSFULL: 'successfull',
    REQUIRED: 'required',
};
export default class DeleteEntityLinkDialogForm extends Component {
    constructor(props) {
        super(props);
        this.state = { authStatus: AuthStatus.UNKNOWN };
    }

    componentDidMount() {
        this.checkTwoWayDelete();
    }

    checkTwoWayDelete() {
        canDeleteEntityLinkResponse(this.props.entityLink, this.props.type, this.props.projectKey)
            .then(response => response.json())
            .then(data => {
                const permissionCode = data.code;
                if (
                    PermissionCode.CREDENTIALS_REQUIRED === permissionCode ||
                    PermissionCode.AUTHENTICATION_FAILED === permissionCode
                ) {
                    this.setState({
                        authUrl: data.url,
                        authStatus: AuthStatus.REQUIRED,
                    });
                } else {
                    this.setState({
                        twoWayAllowed: permissionCode === 'ALLOWED',
                        authStatus: AuthStatus.SUCCESSFULL,
                    });
                }
            });
    }

    getFooterButtons() {
        return () => (
            <ModalFooter>
                <span />
                <ButtonGroup>
                    <Button appearance="danger" type="submit" className="delete-entitylink">
                        {I18n.getText('entitylinks.dialog.delete')}
                    </Button>
                    <Button appearance="subtle-link" onClick={this.props.onClose}>
                        {I18n.getText('entitylinks.dialog.cancel')}
                    </Button>
                </ButtonGroup>
            </ModalFooter>
        );
    }

    onDeleteSubmit(data) {
        const deleteReciprocal = data.twowayselect || false;
        this.deleteLink(deleteReciprocal);
    }

    deleteLink(twoWay) {
        const { entityLink, type, projectKey } = this.props;
        deleteEntityLink(entityLink, type, projectKey, twoWay).then(response => {
            if (response.status === 200) {
                this.props.onDelete(entityLink);
            }
        });
    }

    getBody() {
        const { twoWayAllowed, authStatus } = this.state;
        switch (authStatus) {
            case AuthStatus.SUCCESSFULL:
                return (
                    <CheckboxField name="twowayselect" defaultIsChecked={twoWayAllowed}>
                        {({ fieldProps }) => (
                            <Checkbox
                                {...fieldProps}
                                isDisabled={!twoWayAllowed}
                                label={I18n.getText(
                                    'entitylinks.delete.dialog.checkbox.twoway.label',
                                    this.props.currentApp,
                                    this.props.outgoingApplicationName
                                )}
                            />
                        )}
                    </CheckboxField>
                );
            case AuthStatus.REQUIRED:
                debugger;
                return (
                    <div className="info-auth-container">
                        <SectionMessage
                            appearance="info"
                            actions={[
                                {
                                    text: I18n.getText('entitylinks.dialog.authorize', this.props.currentApp),
                                    onClick: () =>
                                        authorize(this.state.authUrl, () =>
                                            this.checkTwoWayDelete()
                                        ),
                                    key: 'authorize-link',
                                },
                            ]}
                        >
                            <p>
                                {I18n.getText('entitylinks.delete.dialog.auth.message.summary',
                                    this.props.currentApp,
                                    this.props.outgoingApplicationName
                                )}
                            </p>
                            <p>
                                {I18n.getText('entitylinks.delete.dialog.auth.message.main',
                                    this.props.currentApp,
                                    this.props.outgoingApplicationName
                                )}
                            </p>
                        </SectionMessage>
                    </div>
                );
            default:
                return (
                    <div className="info-auth-container">
                        <SectionMessage appearance="info">
                            <p>
                                {I18n.getText('entitylinks.delete.dialog.auth.message.waiting')}{' '}
                                <Spinner size="small" />
                            </p>
                        </SectionMessage>
                    </div>
                );
        }
    }

    render() {
        const { entityLink, projectKey } = this.props;
        return (
            <ModalDialog
                heading={I18n.getText(
                    'entitylinks.delete.dialog.title'
                )}
                autoFocus={true}
                onClose={this.props.onClose}
                components={{
                    Container: ({ children, className }) => (
                        <Form onSubmit={data => this.onDeleteSubmit(data)}>
                            {({ formProps }) => (
                                <form {...formProps} className={className}>
                                    {children}
                                </form>
                            )}
                        </Form>
                    ),
                    Footer: this.getFooterButtons(),
                }}
            >
                <p>
                    {I18n.getText(
                        'entitylinks.delete.dialog.entity.link.text',
                        entityLink.name,
                        this.props.outgoingApplicationName
                    )}
                </p>
                {this.getBody()}
            </ModalDialog>
        );
    }
}
