define('applinks/feature/oauth-picker', [
    'aui/select',
    'applinks/lib/lodash',
    'applinks/lib/jquery',
    'applinks/common/preconditions',
    'applinks/feature/oauth-picker-model'
], function(
    AUISelect,
    _,
    $,
    Preconditions,
    OAuthPickerModel
) {
    var oauthEnabled = new OAuthPickerModel(true, true, false);
    var oauthImpersonationEnabled = new OAuthPickerModel(true, true, true);
    var oauthDisabled = new OAuthPickerModel(false, false, false);

    /**
     * Creates a dropdown to pick OAuth authentication level.
     *
     * @param container DOM element or a JQuery selector.
     * @param componentId Html id for the dropdown
     * @param oauthPickerModel OAuthPickerModel object instance for the current selected OAuth level
     * @constructor
     */
    function OAuthPicker(container, componentId, oauthPickerModel) {
        Preconditions.hasValue(container, 'container');
        Preconditions.hasValue(componentId, 'componentId');
        Preconditions.hasValue(oauthPickerModel, 'oauthPickerModel');

        this.componentId = componentId;
        this.$container = $(container);
        this.$container.append(this._createDropdown(componentId, oauthPickerModel));
    }

    OAuthPicker.prototype._createDropdown = function(componentId, oauthPickerModel) {
        return applinks.feature.oauth.dropdown({
            componentId: componentId,
            authenticationTypes: [oauthEnabled, oauthImpersonationEnabled, oauthDisabled],
            currentAuthType: oauthPickerModel
        });
    };

    OAuthPicker.prototype.getModel = function() {
        return OAuthPickerModel.fromId(parseInt(this.getSelectedValue()));
    };

    OAuthPicker.prototype.getSelectedValue = function() {
        var dropdown = this.getDropdown()[0];
        return dropdown && dropdown.value;
    };

    OAuthPicker.prototype.getDropdown = function() {
        return this.$container.find('.'+ this.componentId + '-dropdown');
    };

    OAuthPicker.prototype.onChangeHandler = function(callback, data) {
        this.$container.find('#'+ this.componentId + '-id').on('change', data, callback);
    };

    return {
        View: OAuthPicker
    };
});
