define('applinks/feature/status/oauth-matcher', [
    'applinks/common/preconditions'
], function(
    Preconditions
) {
    function getAuthLevel(config) {
        Preconditions.hasValue(config);
        if (config.enabled && config.twoLoEnabled && config.twoLoImpersonationEnabled) {
            return 'oauthWithImpersonation';
        }
        if (config.enabled && !config.twoLoImpersonationEnabled) {
            return 'oauth';
        }
        if (!config.enabled && !config.twoLoEnabled && !config.twoLoImpersonationEnabled) {
            return 'disabled';
        }
        return undefined;
    }

    function matchLocalAndRemote(status) {
        var local = status.localAuthentication;
        var remote = status.remoteAuthentication;
        Preconditions.hasValue(local);
        Preconditions.hasValue(remote);
        var config = {
            'incoming' : {
                'local': getAuthLevel(local.incoming),
                'remote': getAuthLevel(remote.outgoing)
            },
            'outgoing' : {
                'local': getAuthLevel(local.outgoing),
                'remote': getAuthLevel(remote.incoming)
            }
        };
        if (config.incoming.local === config.incoming.remote && config.outgoing.local === config.outgoing.remote) {
            return undefined;
        }
        return config;
    }

    return matchLocalAndRemote;
});
