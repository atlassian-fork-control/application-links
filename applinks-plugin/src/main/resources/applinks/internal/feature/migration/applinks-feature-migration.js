define('applinks/feature/migration', [
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/lib/aui',
    'applinks/common/dialog-helper',
    'applinks/common/preconditions',
    'applinks/common/rest'
], function(
    $,
    _,
    AJS,
    DialogHelper,
    Preconditions,
    ApplinksRest
) {

    function disableLocalTrustedAuth(applink) {
        $.ajax({
            url: AJS.contextPath() + '/plugins/servlet/applinks/auth/conf/trusted/autoconfig/' + applink.id,
            type: 'DELETE'
        }).always(function() {
            applink.fetchStatus();
        });
    }

    function createLocalRemovalDialog(applink) {
        var dialogId = 'removal-dialog-' + applink.id;
        var dialogContents = applinks.migration.localRemovalDialogContent({
            id: dialogId,
            displayUrl: applink.getAdminUrl(),
            displayName: applink.get("name")
        });

        $(DialogHelper.getContainer()).append(dialogContents);
        var dialog = AJS.dialog2('#' + dialogId);

        $('#' + dialogId + ' button').on('click', function() {
            dialog.hide();
            dialog.remove();
        });

        dialog.id = dialogId;
        dialog.closeButtonId = dialogId + '-close-button';
        dialog.okButtonId = dialogId + '-ok-button';
        return dialog;
    }

    function hasMigrationApi(config) {
        return _.some(config.capabilities, function(capability) {
            return capability === 'MIGRATION_API';
        });
    }

    function migrate(applink, disableLocalTrustedPredicate) {
        ApplinksRest.V3.migration(applink.id).post().done(function(config) {
            if (disableLocalTrustedPredicate(config)) {
                disableLocalTrustedAuth(applink);
            } else {
                applink.fetchStatus();
            }
        }).fail(function () {
            applink.fetchStatus();
        });
    }

    function showLoadingSpinner(applink) {
        // switch status lozenge to a loading spinner
        applink.set({statusLoaded: false});
    }

    return {
        migrate: function(applink) {
            // this should only run for applink with automatic migration status, i.e LEGACY_UPDATE and LEGACY_REMOVAL
            Preconditions.hasValue(applink, 'applink');
            showLoadingSpinner(applink);
            migrate(applink, function(config) {
                return config.incoming.oauth && config.incoming.trusted;
            });
        },
        removeLocal: function(applink) {
            Preconditions.hasValue(applink, 'applink');
            var dialog = createLocalRemovalDialog(applink);
            $('#' + dialog.okButtonId).on('click', function() {
                showLoadingSpinner(applink);
                migrate(applink, hasMigrationApi);
            });
            dialog.show();
        }
    }
});