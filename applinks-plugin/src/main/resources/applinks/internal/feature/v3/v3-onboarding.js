define('applinks/feature/v3/onboarding', [
    'applinks/lib/aui',
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/common/features',
    'applinks/feature/creation',
    'applinks/common/events',
    'applinks/feature/v3/ui'

], function(
    AJS,
    $,
    _,
    ApplinksFeatures,
    ApplinksCreation,
    ApplinksEvents,
    V3Ui

) {
    return {
        init: function () {

            $(document).bind(ApplinksEvents.READY, function () {
                if (!ApplinksFeatures.isDiscovered(ApplinksFeatures.V3_UI) && !ApplinksCreation.isInProgress() && V3Ui.isOn() ) {
                    $('body').append(applinks.feature.onboarding.v3.onboardingDialog());
                    var v3OnboardingDialog  =  AJS.dialog2('#v3-ui-feature-discovery-dialog');
                    v3OnboardingDialog.show();
                    ApplinksFeatures.discover(ApplinksFeatures.V3_UI);
                    $('#v3-onboarding-dialog-close-button').on('click', function () {
                        v3OnboardingDialog.hide();
                        $(document).trigger(ApplinksEvents.V3_ONBOARDING_FINISHED);
                        $('body').attr('data-v3-onboarding-dialog-finished', 'finished' );
                    });
                } else {
                    $(document).trigger(ApplinksEvents.V3_ONBOARDING_FINISHED);
                    $('body').attr('data-v3-onboarding-dialog-finished', 'finished');
                }
                $('body').attr('data-v3-onboarding-dialog-code-loaded', ''); //used for testing
            });
        }
    }
});
