define('applinks/feature/v3/feedback-collector', [
    'applinks/lib/jquery',
    'applinks/lib/window'
], function(
    $,
    window
) {
    return {
        init : function() {
            /**
             * Handles the displaying of feedback dialogs
             */
            var applinksFeedbackButton = $("#applinks-feedback-button");

            // fetch the collector scripts
            // Feedback button feedback collector
            $.ajax({
                url: "https://jira.atlassian.com/s/e0149ca37bab64d8016b53497b961ef3-T/en_UKdzil1d/65000/131/1.4.25/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-UK&collectorId=35047165",
                type: "get",
                cache: true,
                dataType: "script",
                success: enableFeedbackButton
            });

            // callback functions
            function enableFeedbackButton() {
                window.ATL_JQ_PAGE_PROPS = $.extend(window.ATL_JQ_PAGE_PROPS, {
                    '35047165': {
                        triggerFunction: function (showCollectorDialog) {
                            applinksFeedbackButton.on('click', function (e) {
                                e.preventDefault();
                                showCollectorDialog();
                            });
                        }
                    }
                });
                $("#applinks-feedback-button").show();
            }
        }
    }
});

