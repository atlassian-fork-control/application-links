define('applinks/model/applink', [
    'applinks/lib/lodash',
    'applinks/lib/backbone',
    'applinks/common/products',
    'applinks/common/response-handlers',
    'applinks/common/response-status',
    'applinks/common/rest'
], function(
    _,
    Backbone,
    ApplinksProducts,
    ResponseHandlers,
    ResponseStatus,
    ApplinksRest
){
    function getExpectedStatuses(options) {
        return options && options.expectedStatuses || [ResponseStatus.Family.SUCCESSFUL];
    }

    var DataKeys = ApplinksRest.V3.ApplinkData;

    var ApplinkModel = Backbone.Model.extend({

        defaults: {
            statusLoaded: false
        },

        /**
         * @param attributes initial model attributes
         * @param options Backbone options containing extra Applink-specific attributes:
         *                `dataKeys`: data keys to fetch extra data while fetching the applink
         */
        initialize: function(attributes, options) {
            this.dataKeys = options && options.dataKeys;
        },

        url: function() {
            return ApplinksRest.V3.applink(this.id, this.dataKeys).getUrl();
        },

        // overridden Backbone methods

        destroy: function(options) {
            var expectedStatuses = getExpectedStatuses(options);
            return Backbone.Model.prototype.destroy.call(this, options)
                .done(ResponseHandlers.done(expectedStatuses))
                .fail(ResponseHandlers.fail(expectedStatuses));
        },

        fetch: function(options) {
            var expectedStatuses = getExpectedStatuses(options);
            return Backbone.Model.prototype.fetch.call(this, options)
                .done(ResponseHandlers.done(expectedStatuses))
                .fail(ResponseHandlers.fail(expectedStatuses));
        },

        save: function(attributes, options) {
            var expectedStatuses = getExpectedStatuses(options);
            return Backbone.Model.prototype.save.call(this, attributes, options)
                .done(ResponseHandlers.done(expectedStatuses))
                .fail(ResponseHandlers.fail(expectedStatuses));
        },

        // Applink-specific API

        /**
         * For compatibility with the legacy "applinks-model-status" model. This simply uses the injected applink data
         * "configUrl"
         *
         * @returns {string}
         */
        getAdminUrl: function() {
            return this.getData(DataKeys.CONFIG_URL);
        },

        /**
         * I18n application type name 
         *
         * @returns {string}
         */
        getTypeName: function() {
            return ApplinksProducts.getTypeName(this.get('type'));
        },
        
        /**
         * @param dataKey optional param to get value of a specific data key. Use ApplinkModel.DataKeys for that
         * @returns {object} `data` object for this applink containing extra injected data from the server. To control
         * contents of this use `dataKeys` option when instantiating this model.
         */
        getData: function(dataKey) {
            var data = this.get('data') || {};
            return dataKey ? data[dataKey] : data;
        },

        /**
         * Fetch status for this applink, once fetched this will be reflected in the `statusLoaded` and
         * `status` attributes of this model.
         */
        fetchStatus: function() {
            var self = this;
            // reset previous status
            self.set('statusLoaded', false);
            self.set('status', null);
            return ApplinksRest.V3.status(this.id).get()
                .done(function(data) {
                    self.set('statusLoaded', true);
                    self.set('status', data);
                });
        },

        /**
         * Mark application link as primary, including a blocking request with a corresponding server-side update.
         *
         * @param options Backbone options for the request
         */
        makePrimary: function (options) {
            var model = this.toServerJSON();
            model.primary = true;
            return this.save(model, _.extend({}, options, {
                wait: true
            }));
        },

        /**
         * @return JSON object suitable for server update, removing all client-side attributes redundant for data
         * update; use it for any server-side operation (e.g. `save`)
         */
        toServerJSON: function () {
            var model = this.toJSON();

            delete model.status;
            delete model.statusLoaded;
            delete model.data;

            return model;
        }
    });

    /**
     * Applink Model collection.
     */
    var CollectionModel = Backbone.Collection.extend({
        model: ApplinkModel,

        /**
         * @param models initial model array
         * @param options Backbone options containing extra Applink-specific attributes:
         *                `dataKeys`: data keys to fetch extra data while fetching the applink
         */
        initialize: function(models, options) {
            this.dataKeys = options && options.dataKeys;
        },

        url: function() {
            return ApplinksRest.V3.applinks(this.dataKeys).getUrl();
        },

        fetch: function(options) {
            var expectedStatuses = getExpectedStatuses(options);
            return Backbone.Collection.prototype.fetch.call(this, options)
                .done(ResponseHandlers.done(expectedStatuses))
                .fail(ResponseHandlers.fail(expectedStatuses));
        }
    });

    return {
        Applink: ApplinkModel,
        Collection: CollectionModel,
        DataKeys: DataKeys
    }
});
