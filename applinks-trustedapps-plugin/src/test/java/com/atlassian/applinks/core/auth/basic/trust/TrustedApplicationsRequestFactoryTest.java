package com.atlassian.applinks.core.auth.basic.trust;

import java.net.URI;
import java.net.URISyntaxException;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.security.auth.trustedapps.CurrentApplication;

import com.atlassian.applinks.trusted.auth.TrustedApplicationsRequestFactory;
import com.atlassian.applinks.trusted.auth.TrustedRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Tests for the trusted applications request factory.
 */
public class TrustedApplicationsRequestFactoryTest {

    private CurrentApplication currentApplication;
    private RequestFactory requestFactory;
    private UserManager userManager;

    @Before
    public void createMocks() {
        currentApplication = Mockito.mock(CurrentApplication.class);
        requestFactory = Mockito.mock(RequestFactory.class);
        userManager = Mockito.mock(UserManager.class);
    }

    @Test
    public void willCreateTrustedApplicationsRequest() {
        TrustedApplicationsRequestFactory factory = new TrustedApplicationsRequestFactory(
                currentApplication, requestFactory, userManager);

        Request request = Mockito.mock(Request.class);

        Mockito.when(request.setFollowRedirects(false)).thenReturn(request);
        Mockito.when(requestFactory.createRequest(Request.MethodType.GET, "http://www.test.com")).thenReturn(request);
        Mockito.when(userManager.getRemoteUsername()).thenReturn("admin");
        ApplicationLinkRequest applicationLinkRequest = factory.createRequest(Request.MethodType.GET, "http://www.test.com");

        Assert.assertTrue(applicationLinkRequest instanceof TrustedRequest);
        Mockito.verify(requestFactory).createRequest(Request.MethodType.GET, "http://www.test.com");
        Mockito.verify(userManager).getRemoteUsername();
    }

    @Test
    public void willNotReturnAuthorizationUrl() {
        TrustedApplicationsRequestFactory factory = new TrustedApplicationsRequestFactory(
                currentApplication, requestFactory, userManager);
        Assert.assertNull(factory.getAuthorisationURI());
    }

    @Test
    public void willNotReturnAuthorizationUrlFromExisting() throws URISyntaxException {
        TrustedApplicationsRequestFactory factory = new TrustedApplicationsRequestFactory(
                currentApplication, requestFactory, userManager);
        Assert.assertNull(factory.getAuthorisationURI(new URI("http://www.test.com")));
    }
}
