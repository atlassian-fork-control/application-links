package com.atlassian.applinks.core.auth.basic.trust;

import com.atlassian.applinks.trusted.auth.TrustedRequest;
import com.atlassian.applinks.trusted.auth.TrustedResponseHandler;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.security.auth.trustedapps.TrustedApplicationUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for the trusted response handler.
 */
public class TrustedResponseHandlerTest {
    private static final String BASE_URL = "http://localhost:8080/";
    private static final String REQUEST_URL = BASE_URL + "servlet";

    private ResponseHandler<Response> applicationLinkResponseHandler;
    private TrustedRequest wrappedRequest;

    @Before
    public void createMocks() {
        applicationLinkResponseHandler = mock(ResponseHandler.class);
        wrappedRequest = mock(TrustedRequest.class);
    }

    @Test
    public void shouldHandleNormalResponse() throws ResponseException {
        TrustedResponseHandler handler = new TrustedResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, true);
        Response response = mock(Response.class);
        when(response.getStatusCode()).thenReturn(200);

        handler.handle(response);

        verify(applicationLinkResponseHandler).handle(response);
    }

    @Test
    public void shouldHandleRedirectResponse() throws ResponseException {
        TrustedResponseHandler handler = new TrustedResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, true);

        Response response = mock(Response.class);
        when(response.getHeader("location")).thenReturn("redirected");
        when(response.getStatusCode()).thenReturn(302);

        handler.handle(response);

        verify(wrappedRequest).setUrl(BASE_URL + "redirected");
        verify(wrappedRequest).execute(handler);
    }

    @Test
    public void shouldHandleResponseNormallyForEmptyLocationRedirect() throws ResponseException {
        TrustedResponseHandler handler = new TrustedResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, true);

        Response response = mock(Response.class);
        when(response.getStatusCode()).thenReturn(302);
        when(response.getHeader("location")).thenReturn(null);

        handler.handle(response);

        verify(applicationLinkResponseHandler).handle(response);
        verify(wrappedRequest, Mockito.never()).setUrl("new location");
        verify(wrappedRequest, Mockito.never()).execute(handler);
    }

    @Test
    public void shouldNotFollowRedirectsWhenFollowRedirectsIsFalse() throws ResponseException {
        TrustedResponseHandler handler = new TrustedResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, false);

        Response response = mock(Response.class);
        when(response.getHeader("location")).thenReturn("new location");
        when(response.getStatusCode()).thenReturn(302);

        handler.handle(response);

        verify(applicationLinkResponseHandler).handle(response);
        verify(wrappedRequest, Mockito.never()).setUrl("new location");
        verify(wrappedRequest, Mockito.never()).execute(handler);
    }

    @Test
    public void shouldNotFollowRedirectsWhenMaximumReached() throws ResponseException {
        TrustedResponseHandler handler = new TrustedResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, true);

        Response response = mock(Response.class);
        when(response.getHeader("location")).thenReturn("redirected");
        when(response.getStatusCode()).thenReturn(302);

        handler.handle(response);
        handler.handle(response);
        handler.handle(response);
        handler.handle(response);

        // Note, the final response handler is called once, after the redirect behaviour is invoked 3 times.
        verify(applicationLinkResponseHandler, times(1)).handle(response);
        verify(wrappedRequest, times(3)).setUrl(BASE_URL + "redirected");
        verify(wrappedRequest, times(3)).execute(handler);
    }

    @Test
    public void shouldHandleErrorResponse() throws ResponseException {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put(TrustedApplicationUtils.Header.Response.ERROR, "There was an error");

        TrustedResponseHandler handler = new TrustedResponseHandler(REQUEST_URL, applicationLinkResponseHandler,
                wrappedRequest, true);

        Response response = mock(Response.class);
        when(response.getStatusCode()).thenReturn(400);
        when(response.getHeaders()).thenReturn(headers);

        handler.handle(response);

        verify(applicationLinkResponseHandler).handle(response);
    }
}