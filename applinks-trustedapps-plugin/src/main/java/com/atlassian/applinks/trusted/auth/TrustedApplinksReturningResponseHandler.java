package com.atlassian.applinks.trusted.auth;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.core.auth.AbstractApplicationLinkResponseHandler;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;

/**
 * Response handler for trusted applinks requests.
 *
 * @since 3.11.0
 */
public class TrustedApplinksReturningResponseHandler<T extends Response, R>
        extends AbstractApplicationLinkResponseHandler
        implements ReturningResponseHandler<Response, R> {
    private ReturningResponseHandler<? super Response, R> returningResponseHandler;

    public TrustedApplinksReturningResponseHandler(final String url,
                                                   final ReturningResponseHandler<? super Response, R> returningResponseHandler,
                                                   final ApplicationLinkRequest wrappedRequest,
                                                   final boolean followRedirects) {
        super(url, wrappedRequest, followRedirects);
        this.returningResponseHandler = returningResponseHandler;
    }

    public R handle(final Response response) throws ResponseException {
        return (followRedirects && redirectHelper.responseShouldRedirect(response))
                ? followRedirects(response) : handleNormally(response);
    }

    private R followRedirects(Response response) throws ResponseException {
        wrappedRequest.setUrl(redirectHelper.getNextRedirectLocation(response));
        return wrappedRequest.executeAndReturn(this);
    }

    private R handleNormally(Response response) throws ResponseException {
        return returningResponseHandler.handle(response);
    }
}