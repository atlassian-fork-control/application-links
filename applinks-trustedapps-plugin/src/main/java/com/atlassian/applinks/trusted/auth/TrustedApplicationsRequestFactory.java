package com.atlassian.applinks.trusted.auth;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.security.auth.trustedapps.CurrentApplication;

import java.net.URI;

import static java.util.Objects.requireNonNull;

/**
 * Provider of trusted application requests.
 * Note, no authorisation url will ever be returned from this, this type of authentication either works
 * or fails.
 *
 * @since 3.11.0
 */
public class TrustedApplicationsRequestFactory implements ApplicationLinkRequestFactory {
    private final CurrentApplication currentApplication;
    private final RequestFactory requestFactory;
    private final UserManager userManager;

    public TrustedApplicationsRequestFactory(final CurrentApplication currentApplication,
                                             final RequestFactory requestFactory,
                                             final UserManager userManager) {
        this.currentApplication = requireNonNull(currentApplication, "currentApplication");
        this.requestFactory = requireNonNull(requestFactory, "requestFactory");
        this.userManager = requireNonNull(userManager, "userManager");
    }

    public ApplicationLinkRequest createRequest(final Request.MethodType methodType, final String url) {
        final Request request = requestFactory.createRequest(methodType, url);
        final String username = requireNonNull(userManager.getRemoteUsername(), "You have to be logged in to use trusted authentication.");
        return new TrustedRequest(url, request, currentApplication, username);
    }

    public URI getAuthorisationURI(final URI callback) {
        return null;
    }

    public URI getAuthorisationURI() {
        return null;
    }
}