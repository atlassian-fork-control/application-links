package com.atlassian.applinks.trusted.auth;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.core.auth.AbstractApplicationLinkResponseHandler;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;

/**
 * Response handler for trusted applinks requests.
 *
 * @since 3.11.0
 */
public class TrustedResponseHandler<T extends Response>
        extends AbstractApplicationLinkResponseHandler
        implements ResponseHandler<Response> {
    private final ResponseHandler<Response> responseHandler;

    public TrustedResponseHandler(
            final String url,
            final ResponseHandler<Response> responseHandler,
            final ApplicationLinkRequest wrappedRequest,
            final boolean followRedirects) {
        super(url, wrappedRequest, followRedirects);
        this.responseHandler = responseHandler;
    }

    public void handle(final Response response) throws ResponseException {
        if (followRedirects && redirectHelper.responseShouldRedirect(response)) {
            wrappedRequest.setUrl(redirectHelper.getNextRedirectLocation(response));
            wrappedRequest.execute(this);
        } else {
            responseHandler.handle(response);
        }
    }
}