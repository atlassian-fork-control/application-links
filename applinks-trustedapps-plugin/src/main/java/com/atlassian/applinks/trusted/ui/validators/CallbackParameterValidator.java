package com.atlassian.applinks.trusted.ui.validators;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;

/**
 * Extension of {@link com.atlassian.applinks.ui.validators.CallbackParameterValidator} used to fors BND/OSGi Import-Package to bring in the package.
 *
 * @since 5.0.0
 */
public class CallbackParameterValidator extends com.atlassian.applinks.ui.validators.CallbackParameterValidator {
    public CallbackParameterValidator(MessageFactory messageFactory, InternalHostApplication internalHostApplication, ApplicationLinkService applicationLinkService) {
        super(messageFactory, internalHostApplication, applicationLinkService);
    }
}
