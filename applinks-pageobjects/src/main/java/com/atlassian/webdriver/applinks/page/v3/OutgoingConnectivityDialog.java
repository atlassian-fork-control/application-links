package com.atlassian.webdriver.applinks.page.v3;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class OutgoingConnectivityDialog extends ConnectivityDialog {
    @ElementBy(id = "oauth-mismatch-right-dialog")
    private PageElement dialog;

    @Override
    protected PageElement getDialog() {
        return dialog;
    }
}
