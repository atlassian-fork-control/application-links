package com.atlassian.webdriver.applinks.util;

import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.base.Function;

/**
 * Utility function to return page element text
 */
public class GetTextFunction implements Function<PageElement, String> {
    public String apply(PageElement pageElement) {
        return pageElement.getText();
    }
}
