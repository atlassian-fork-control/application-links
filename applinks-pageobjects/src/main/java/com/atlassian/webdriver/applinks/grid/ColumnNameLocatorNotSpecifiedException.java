package com.atlassian.webdriver.applinks.grid;

/**
 * Thrown when attempting to locate via column name when a grid does not have column names
 */
public class ColumnNameLocatorNotSpecifiedException extends UnsupportedOperationException {
    public ColumnNameLocatorNotSpecifiedException() {
        super("No column name locator specified for this grid");
    }
}
