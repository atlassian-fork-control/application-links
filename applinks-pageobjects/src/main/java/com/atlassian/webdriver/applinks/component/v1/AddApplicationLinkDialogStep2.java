package com.atlassian.webdriver.applinks.component.v1;

import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.component.FindUtils;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import org.openqa.selenium.By;

public class AddApplicationLinkDialogStep2 extends AbstractAddApplicationLinkDialogStep {
    @ElementBy(cssSelector = "#add-application-link-dialog #reciprocalLink")
    CheckboxElement reciprocalLinkCheckbox;

    @ElementBy(cssSelector = "#add-application-link-dialog #reciprocal-link-username")
    PageElement reciprocalLinkUsernameTextBox;

    @ElementBy(cssSelector = "#add-application-link-dialog #reciprocal-link-password")
    PageElement reciprocalLinkPasswordTextBox;

    @ElementBy(cssSelector = "#add-application-link-dialog #reciprocal-rpc-url")
    PageElement reciprocalRpcUrlTextBox;

    /**
     * Configure two-way link and then clicks next to go to the next step. This only works in it is a 3-step wizard.
     *
     * @param remoteApplicationUsername admin username of the remote application
     * @param remoteApplicationPassword admin password of the remote application
     * @param reciprocalRpcUrl          reciprocal url which points to the current instance, null means use the default
     * @return -
     */
    public AddApplicationLinkDialogStep3 configureTwoWayLink(String remoteApplicationUsername,
                                                             String remoteApplicationPassword,
                                                             String reciprocalRpcUrl) {
        return configureTwoWayLink(remoteApplicationUsername, remoteApplicationPassword, reciprocalRpcUrl, AddApplicationLinkDialogStep3.class);
    }

    /**
     * @param remoteApplicationUsername admin username of the remote application
     * @param remoteApplicationPassword admin password of the remote application
     * @param reciprocalRpcUrl          reciprocal url which points to the current instance, null means use the default
     * @param nextPageClass             the next page where to go
     * @return -
     */
    public <T> T configureTwoWayLink(String remoteApplicationUsername, String remoteApplicationPassword, String reciprocalRpcUrl, Class<T> nextPageClass) {
        enterLinkParameters(remoteApplicationUsername, remoteApplicationPassword, reciprocalRpcUrl);

        return clickNext(nextPageClass);
    }

    /**
     * Configure two-way link and then clicks create. This only works in it is a 2-step wizard.
     *
     * @param remoteApplicationUsername admin username of the remote application
     * @param remoteApplicationPassword admin password of the remote application
     * @param reciprocalRpcUrl          reciprocal url which points to the current instance, null means use the default
     * @return -
     */
    public ListApplicationLinkPage configureTwoWayLinkThenCreate(String remoteApplicationUsername,
                                                                 String remoteApplicationPassword,
                                                                 String reciprocalRpcUrl) {
        enterLinkParameters(remoteApplicationUsername, remoteApplicationPassword, reciprocalRpcUrl);

        return clickCreate();
    }

    private void enterLinkParameters(String remoteApplicationUsername, String remoteApplicationPassword, String reciprocalRpcUrl) {
        Poller.waitUntilTrue(reciprocalRpcUrlTextBox.timed().isVisible());

        reciprocalLinkCheckbox.check();

        reciprocalLinkUsernameTextBox.clear();
        reciprocalLinkUsernameTextBox.type(remoteApplicationUsername);

        reciprocalLinkPasswordTextBox.clear();
        reciprocalLinkPasswordTextBox.type(remoteApplicationPassword);

        if (reciprocalRpcUrl != null) {
            reciprocalRpcUrlTextBox.clear();
            reciprocalRpcUrlTextBox.type(reciprocalRpcUrl);
        }
    }

    public ListApplicationLinkPage configureOneWayLink() {
        return configureOneWayLink(ListApplicationLinkPage.class);
    }

    public <T> T configureOneWayLink(Class<T> nextPageClass) {
        Poller.waitUntilTrue(reciprocalLinkCheckbox.timed().isVisible());
        reciprocalLinkCheckbox.uncheck();

        return clickNext(nextPageClass);
    }

    public ListApplicationLinkPage configureOneWayLinkAsAdmin() {
        Poller.waitUntilTrue(reciprocalLinkCheckbox.timed().isVisible());
        reciprocalLinkCheckbox.uncheck();

        FindUtils.findVisibleBy(By.cssSelector("#add-application-link-dialog .wizard-submit"), elementFinder).click();

        return pageBinder.bind(ListApplicationLinkPage.class);
    }

    private <T> T clickNext(Class<T> nextPageClass) {
        // find the visible next button and click it.
        FindUtils.findVisibleBy(By.cssSelector("#add-application-link-dialog .applinks-next-button"), elementFinder)
                .click();

        return pageBinder.bind(nextPageClass);
    }

}
