package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElements;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import org.openqa.selenium.By;

public class BasicAccessAuthenticationSection extends AbstractAuthenticationSection {
    @ElementBy(name = "password2")
    private PageElement confirmPasswordInput;
    @ElementBy(className = "button")
    private PageElement enableDisbleButton;
    @ElementBy(name = "password1")
    private PageElement passwordInput;
    @ElementBy(id = "basicUsername")
    private PageElement usernameInput;

    private XsrfWarning xsrfWarning;

    public BasicAccessAuthenticationSection enable() {
        if (this.isNotConfigured()) {
            throw new RuntimeException("Already Enabled");
        }

        enableDisbleButton.click();

        return this;
    }

    public BasicAccessAuthenticationSection disable() {
        if (this.isConfigured()) {
            enableDisbleButton.click();
        }
        return this;
    }

    public XsrfWarning getXsrfWarning() {
        if (xsrfWarning == null) {
            xsrfWarning = pageBinder.bind(XsrfWarning.class);
        }
        return xsrfWarning;
    }

    public boolean isConfigured() {
        PageElement status = elementFinder.find(By.className("status-configured"));
        Poller.waitUntilTrue(status.timed().isVisible());

        return "Configured".equals(status.getText());
    }

    public boolean isNotConfigured() {
        PageElement status = elementFinder.find(By.className("status-not-configured"));
        Poller.waitUntilTrue(status.timed().isVisible());

        return "Configured".equals(status.getText());
    }

    public BasicAccessAuthenticationSection modifyAtlToken() {
        elementFinder.find(By.tagName(PageElements.BODY)).javascript()
                .execute("AJS.$('.auth-config input[name=\"atl_token\"]').val('hacker!')");

        return this;
    }

    public BasicAccessAuthenticationSection setPassword(String password) {
        Poller.waitUntilTrue(passwordInput.timed().isVisible());

        passwordInput.type(password);
        confirmPasswordInput.type(password);

        return this;
    }

    public BasicAccessAuthenticationSection setUsername(String username) {
        Poller.waitUntilTrue(usernameInput.withTimeout(TimeoutType.PAGE_LOAD).timed().isVisible());
        usernameInput.type(username);

        return this;
    }
}
