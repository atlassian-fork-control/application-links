package com.atlassian.webdriver.applinks.component.v3;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.page.v3.V3EditApplinkPage;
import com.google.common.base.MoreObjects;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.function.Predicate;

import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toImmutableList;
import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toStream;
import static com.atlassian.pageobjects.elements.PageElements.hasClass;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.component.v3.TroubleshootingPageUtils.clickAndCloseNewWindow;
import static com.google.common.base.Predicates.not;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.core.Is.is;

public class ApplinkRow {
    private static final By APPLINKS_DROPDOWN_BUTTON_LOCATOR = By.className("applinks-dropdown-button");

    @Inject
    protected Timeouts timeouts;
    @Inject
    protected PageElementFinder pageElementFinder;
    @Inject
    protected PageBinder pageBinder;

    protected final PageElement container;

    public ApplinkRow(@Nonnull PageElement container) {
        this.container = requireNonNull(container, "container");
    }

    @Nonnull
    public static Predicate<ApplinkRow> hasApplinkWithId(@Nonnull final ApplicationId id) {
        requireNonNull(id, "id");
        return row -> id.equals(row.getId());
    }

    public static Matcher<ApplinkRow> withId(ApplicationId id) {
        return hasApplinkIdThat(is(id));
    }

    public static Matcher<ApplinkRow> hasApplinkIdThat(final Matcher<ApplicationId> matcher) {
        return new FeatureMatcher<ApplinkRow, ApplicationId>(matcher, "id that matches", "id") {
            @Override
            protected ApplicationId featureValueOf(ApplinkRow actual) {
                return actual.getId();
            }
        };
    }

    @WaitUntil
    public void waitUntilLoaded() {
        waitUntilTrue(container.find(By.className("application-icon")).timed().isPresent());
        waitUntilTrue(getApplicationInfo().timed().isPresent());
        waitUntilTrue(getStatusContainer().timed().isPresent());
        waitUntilTrue(getActions().timed().isPresent());
    }

    public String getName() {
        return getApplicationInfo().find(By.className("applinks-name")).getText();
    }

    @Nonnull
    public ApplicationId getId() {
        return new ApplicationId(this.container.getAttribute("id"));
    }

    @Nullable
    public String getDisplayUrl() {
        return getApplicationInfo().find(By.className("applinks-name")).getAttribute("data-displayurl");
    }

    @Nullable
    public String getRpcUrl() {
        return getApplicationInfo().find(By.className("applinks-name")).getAttribute("data-rpcurl");
    }

    @Nonnull
    public ApplinkStatusLozenge getStatus() {
        return pageBinder.bind(ApplinkStatusLozenge.class, getStatusContainer());
    }

    @Nonnull
    public TimedQuery<String> getStatusText() {
        return getStatus().getText();
    }

    @Nonnull
    public TimedQuery<String> getVersion() {
        return getVersionContainer().timed().getText();
    }

    public TimedCondition hasActions() {
        return findActionsTrigger().timed().isPresent();
    }

    @Nonnull
    public TimedCondition hasVersion() {
        return getVersionContainer().timed().isPresent();
    }

    @Nonnull
    public TimedCondition isStatusLoaded() {
        return getStatusContainer()
                .withTimeout(TimeoutType.SLOW_PAGE_LOAD)
                .timed()
                .hasAttribute("data-status-loaded", Boolean.TRUE.toString());
    }

    @Nonnull
    public TimedCondition isStatusExpanded() {
        return getStatus()
                .getStatusLozenge()
                .withTimeout(TimeoutType.COMPONENT_LOAD)
                .timed()
                .hasAttribute("aria-expanded", Boolean.TRUE.toString());
    }

    /**
     * @return Timed Query for isPrimary
     */
    public TimedQuery<Boolean> isPrimary() {
        return getApplicationInfo().find(By.className("primary-lozenge")).timed().isPresent();
    }

    @Nonnull
    public V3ApplinkActionDropDown openActionsDropdown() {
        waitUntilTrue(findActionsTrigger().timed().isVisible());
        findActionsTrigger().click();

        V3ApplinkActionDropDown actionDropDown = pageBinder.bind(V3ApplinkActionDropDown.class, findActionsDropdown());
        waitUntilTrue(actionDropDown.isOpen());
        return actionDropDown;
    }

    public void delete() {
        openActionsDropdown().delete();
    }

    public void setPrimary() {
        openActionsDropdown().makePrimary();
    }

    protected PageElement getEditAction() {
        return container.find(By.className("edit"));
    }

    @Nonnull
    public TimedCondition hasEdit() {
        return getEditAction().timed().isVisible();
    }

    /**
     * Will only work if the link supports new V3 edit screen.
     */
    @Nonnull
    public V3EditApplinkPage edit() {
        ApplicationId id = getId();
        waitUntilTrue(hasEdit());
        getEditAction().click();
        return pageBinder.bind(V3EditApplinkPage.class, id);
    }

    /**
     * Will only work if the link does not support the new V3 edit screen.
     */
    @Nonnull
    public ApplicationDetailsSection legacyEdit() {
        waitUntilTrue(hasEdit());
        getEditAction().click();
        ApplicationDetailsSection legacyEdit = pageBinder.bind(ApplicationDetailsSection.class);
        waitUntilTrue(legacyEdit.isOpen());
        return legacyEdit;
    }


    protected PageElement getApplicationInfo() {
        return container.find(By.className("id-badge"));
    }

    protected PageElement getTypeVersionContainer() {
        return container.find(By.className("applinks-type-version"));
    }

    protected PageElement getVersionContainer() {
        return getTypeVersionContainer().find(By.className("applinks-version"));
    }

    protected PageElement findActionsTrigger() {
        return getActions().find(APPLINKS_DROPDOWN_BUTTON_LOCATOR);
    }

    protected PageElement findActionsDropdown() {
        return pageElementFinder.find(By.id("dropdown-button-" + getId()));
    }

    protected PageElement getStatusContainer() {
        return container.find(By.className("status")).withTimeout(TimeoutType.AJAX_ACTION);
    }

    protected PageElement getActions() {
        return container.find(By.className("actions"));
    }

    /**
     * Open the diagnostics dialog, assuming it wasn't open already.
     *
     * @return the diagnostics dialog
     */
    @Nonnull
    public DiagnosticsDialog openDiagnosticsDialog() {
        PageElement dismissFeatureDiscovery = pageElementFinder.find(By.id("bb-rebrand-dismiss"));
        if (dismissFeatureDiscovery.isPresent() && dismissFeatureDiscovery.isVisible()) {
            //The feature discovery dialog may prevent us from opening the status dialog. Dismiss it if present
            dismissFeatureDiscovery.javascript().mouse().click();
            waitUntilFalse(dismissFeatureDiscovery.timed().isVisible());
        }

        if (!isStatusExpanded().now()) {
            getStatus().toggle();
            waitUntilTrue(isStatusExpanded());
        }

        PageElement dialogContainer = pageElementFinder.find(By.className("applinks-status-dialog"));

        return pageBinder.bind(DiagnosticsDialog.class, dialogContainer, getId());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", getId())
                .add("name", getName())
                .add("version", getVersion().now())
                .toString();
    }

    public static class DiagnosticsDialog {
        @Inject
        protected PageBinder pageBinder;
        @Inject
        protected WebDriver driver;
        @Inject
        protected Timeouts timeouts;

        static final String CLASS_ACTIONS = "applink-status-dialog-actions";

        private final PageElement diagnosticsDialogContainer;

        private ApplicationId id;

        public DiagnosticsDialog(PageElement diagnosticsDialogContainer, ApplicationId id) {
            this.diagnosticsDialogContainer = diagnosticsDialogContainer;
            this.id = id;
        }

        @Nullable
        public String getTroubleshootingUrl() {
            return getTroubleshootingLink().getAttribute("href");
        }

        @Nullable
        public String getTitle() {
            return diagnosticsDialogContainer.find(By.className("applinks-status-dialog-title")).getText();
        }

        @Nullable
        public String getEditButtonText() {
            return getEditButtonElement().getText();
        }

        @Nullable
        public String getUpdateButtonText() {
            return getUpdateButtonElement().getText();
        }

        @Nonnull
        public Iterable<String> getContents() {
            return toStream(getContentElements())
                    .map(PageElement::getText)
                    .collect(toImmutableList());
        }

        @Nonnull
        public TimedCondition hasTroubleshootingLink() {
            return getTroubleshootingLink().timed().isPresent();
        }

        public TimedCondition isOpen() {
            return diagnosticsDialogContainer.timed().isVisible();
        }

        /**
         * This will only work if the primary action is close
         */
        public void close() {
            diagnosticsDialogContainer.find(By.className("applinks-status-close")).click();
            waitUntilFalse(isOpen());
        }

        /**
         * This will only work if the primary action is edit
         */
        @Nonnull
        public V3EditApplinkPage edit() {
            getEditButtonElement().click();
            return pageBinder.bind(V3EditApplinkPage.class, id);
        }

        public void openTroubleshootingPage() {
            clickAndCloseNewWindow(() -> getTroubleshootingLink().javascript().mouse().click(), driver, timeouts);
        }

        @Nonnull
        protected Iterable<PageElement> getContentElements() {
            return diagnosticsDialogContainer.search().by(By.tagName("p"))
                    .filter(not(hasClass(CLASS_ACTIONS)))
                    .now();
        }

        @Nonnull
        protected PageElement getTroubleshootingLink() {
            return diagnosticsDialogContainer.find(By.className("troubleshoot-link"));
        }

        @Nonnull
        protected PageElement getEditButtonElement() {
            return diagnosticsDialogContainer.find(By.className("edit-url-button"));
        }

        @Nonnull
        protected PageElement getUpdateButtonElement() {
            return diagnosticsDialogContainer.find(By.className("applinks-status-update"));
        }
    }
}
