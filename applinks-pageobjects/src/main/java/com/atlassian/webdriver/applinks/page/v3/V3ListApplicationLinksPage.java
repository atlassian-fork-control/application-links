package com.atlassian.webdriver.applinks.page.v3;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElements;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.search.AnyQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.applinks.component.OrphanedTrustRelationshipsDialog;
import com.atlassian.webdriver.applinks.component.v3.ApplinkRow;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.common.lang.FunctionalInterfaces.toGuavaPredicate;
import static com.atlassian.pageobjects.elements.PageElements.hasClass;
import static com.atlassian.pageobjects.elements.query.Conditions.and;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.component.v3.ApplinkRow.hasApplinkWithId;
import static com.google.common.base.Predicates.not;
import static java.util.Objects.requireNonNull;

/**
 * Represents the V3 "Status UI" application links config page.
 *
 * @since 4.3
 */
public class V3ListApplicationLinksPage extends ApplinkAbstractPage {
    // static rows do not contain Applinks data, currently added when the Applinks list is empty
    private static final String STATIC_ROW_CLASS = "static";

    @ElementBy(id = "applinks-lab-switchback")
    protected PageElement switchBack;
    @ElementBy(id = "agent-page", timeoutType = TimeoutType.PAGE_LOAD)
    protected PageElement pageContainer;
    @ElementBy(id = "agent-table")
    protected PageElement applinksTable;
    @ElementBy(id = "applinks-create-button")
    protected PageElement createApplinkButton;
    @ElementBy(id = "main-v3-ui-onboarding-panel")
    protected PageElement v3UiOnboardingDialog;
    @ElementBy(className = "orphaned-trust-warning")
    protected PageElement orphanedTrustWarning;
    @ElementBy(id = "show-orphaned-trust")
    protected PageElement orphanedTrustDialogButton;

    private final boolean withLegacyEdit;

    public V3ListApplicationLinksPage() {
        this(false);
    }

    public V3ListApplicationLinksPage(boolean withLegacyEdit) {
        this.withLegacyEdit = withLegacyEdit;
    }

    @Override
    public String getUrl() {
        String url = "/plugins/servlet/applinks/listApplicationLinks";
        return withLegacyEdit ? url + "?legacyEdit=true" : url;
    }

    @WaitUntil
    public void waitUntilLoaded() {
        waitUntilTrue(pageContainer.timed().isPresent());
        waitUntilTrue(createApplinkButton.timed().isPresent());
        waitUntilTrue(isLoaded());
        waitUntilTrue(allStatusesLoaded());
    }

    /**
     * Indicates when the list of application links has initially loaded. The V3 UI assigns "applinks-loaded" once
     * initial list of applinks loads. {@link #allStatusesLoaded()}, in turn, ensures that statuses for all applinks
     * have been fetched asynchronously after the applinks list has initially loaded (or after any status has been
     * fetched subsequent to the initial page load).
     *
     * @return timed condition for an initially loaded applinks list in the V3 UI
     * @see #allStatusesLoaded()
     */
    @Nonnull
    public TimedCondition isLoaded() {
        return applinksTable.withTimeout(TimeoutType.PAGE_LOAD).timed().hasClass("applinks-loaded");
    }

    /**
     * @return timed condition that ensures that statuses for applinks have been fetched. This is when the page is
     * "fully loaded"
     * @see #isLoaded()
     */
    @Nonnull
    @SuppressWarnings("unchecked")
    public TimedCondition allStatusesLoaded() {
        Iterable<TimedQuery<Boolean>> allLoaded = (Iterable) rowsQuery().map(ApplinkRow::isStatusLoaded).get();
        // and() doesn't handle empty lists unfortunately, at this stage we assume that the applinks list length won't
        // change
        return Iterables.isEmpty(allLoaded) ? Conditions.alwaysTrue() : and(allLoaded);
    }

    /**
     * @return timed condition to wait for the switch back to V2 to be present
     * @see ListApplicationLinkPage#switchToV3Ui()
     */
    @Nonnull
    public TimedCondition hasSwitchBack() {
        return switchBack.withTimeout(TimeoutType.PAGE_LOAD).timed().isPresent();

    }

    /**
     * @return timed condition that returns true if the applinks list is empty (there is no applinks)
     */
    @Nonnull
    public TimedCondition isEmpty() {
        return applinksTable.withTimeout(TimeoutType.PAGE_LOAD).timed().hasAttribute("data-empty", "empty");
    }

    @Nonnull
    public TimedCondition hasOrphanedTrustWarning() {
        return and(orphanedTrustWarning.timed().isVisible(), orphanedTrustDialogButton.timed().isVisible());
    }

    @Nonnull
    public TimedCondition onboardingDialogPresent() {
        return v3UiOnboardingDialog.withTimeout(TimeoutType.DEFAULT).timed().isVisible();
    }

    @Nonnull
    public TimedCondition onboardingCheckHasRun() {
        return elementFinder.find(By.tagName("body")).timed().hasAttribute("data-v3-onboarding-dialog-code-loaded", "");
    }

    @Nonnull
    public TimedQuery<Iterable<ApplinkRow>> getApplinksRows() {
        return rowsQuery().timed();
    }

    @Nonnull
    public ApplinkRow getApplinkRow(@Nonnull TestApplink.Side side) {
        return getApplinkRow(side.applicationId());
    }

    @Nonnull
    public ApplinkRow getApplinkRow(@Nonnull ApplicationId id) {
        AnyQuery<ApplinkRow> byId = rowsQuery().filter(toGuavaPredicate(hasApplinkWithId(id)));
        waitUntilTrue(byId.hasResult());
        return requireNonNull(byId.first()); // should not be null
    }

    @Nonnull
    public OrphanedTrustRelationshipsDialog openOrphanedTrustDialog() {
        waitUntilTrue("Orphaned trust warning must be present to open orphaned trust dialog", hasOrphanedTrustWarning());
        orphanedTrustDialogButton.click();
        OrphanedTrustRelationshipsDialog orphanedTrustDialog = pageBinder.bind(OrphanedTrustRelationshipsDialog.class);
        waitUntilTrue(orphanedTrustDialog.isOpen());
        return orphanedTrustDialog;
    }

    @Nonnull
    protected AnyQuery<ApplinkRow> rowsQuery() {
        return applinksTable.search()
                .by(By.tagName("tbody"))
                .by(By.tagName(PageElements.TR))
                .filter(not(hasClass(STATIC_ROW_CLASS))) // exclude static rows, those are non-applinks rows
                .bindTo(ApplinkRow.class);
    }
}
