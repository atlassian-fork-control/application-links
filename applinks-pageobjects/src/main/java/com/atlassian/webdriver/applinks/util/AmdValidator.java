package com.atlassian.webdriver.applinks.util;

import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.Elements;
import com.google.common.base.Joiner;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * Represents a page with which has the attribute {@code data-amd-modules-loaded} set to true if AMD modules have loaded
 * successfully. The attribute will only be set in pages which have the smoke-test-amd-modules.js file included in their
 * context with the applinks-test plugin installed.
 *
 * @since 4.3
 */
public class AmdValidator {
    @Inject
    protected PageElementFinder elementFinder;

    @Nonnull
    public TimedCondition amdModulesLoaded() {
        return elementFinder.find(By.tagName(Elements.TAG_BODY)).timed().hasAttribute("data-amd-modules-loaded", "true");
    }

    public <T> T execute(@Nonnull Class<T> expectedReturnType, @Nonnull String moduleId, @Nonnull String functionName,
                         @Nonnull String... args) {
        requireNonNull(expectedReturnType, "expectedReturnType");

        String script = format("return require('%s').%s(%s)", moduleId, functionName, Joiner.on(',').join(args));
        return elementFinder.find(By.tagName(Elements.TAG_BODY)).javascript().execute(expectedReturnType, script);
    }
}
