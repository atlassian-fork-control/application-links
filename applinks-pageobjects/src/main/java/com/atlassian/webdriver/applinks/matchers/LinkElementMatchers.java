package com.atlassian.webdriver.applinks.matchers;

import com.atlassian.webdriver.applinks.element.ClickableElement;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.hamcrest.Matchers.is;

/**
 * @since 5.2
 */
public final class LinkElementMatchers {
    private LinkElementMatchers() {
        // do not instantiate
    }

    @Nonnull
    public static Matcher<ClickableElement> withTextThat(@Nonnull Matcher<String> textMatcher) {
        return new FeatureMatcher<ClickableElement, String>(textMatcher, "link text", "text") {
            @Override
            protected String featureValueOf(ClickableElement actual) {
                return actual.getText();
            }
        };
    }

    @Nonnull
    public static Matcher<ClickableElement> withText(@Nullable String expectedText) {
        return withTextThat(is(expectedText));
    }

    @Nonnull
    public static Matcher<ClickableElement> withUrlThat(@Nonnull Matcher<String> urlMatcher) {
        return new FeatureMatcher<ClickableElement, String>(urlMatcher, "URL target", "url") {
            @Override
            protected String featureValueOf(ClickableElement actual) {
                return actual.getUrl();
            }
        };
    }

    @Nonnull
    public static Matcher<ClickableElement> withUrl(@Nullable String expectedUrl) {
        return withUrlThat(is(expectedUrl));
    }
}
