package com.atlassian.webdriver.applinks.component.v2;

/**
 * Represents the dialog displaying information to the user to allow them to manually create the reciprocal link
 * in the v2 workflow.
 *
 * @since v4.0.0
 */
public class ManualReciprocalLinkDialog extends PauseDialog {
}
