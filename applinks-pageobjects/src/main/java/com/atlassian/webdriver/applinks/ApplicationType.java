package com.atlassian.webdriver.applinks;

import com.google.common.base.Predicate;

import static com.google.common.collect.Iterators.forArray;
import static com.google.common.collect.Iterators.find;

public enum ApplicationType {
    GENERIC_APPLICATION("Generic Application"),
    BAMBOO("Bamboo"),
    FECRU("Fisheye / Crucible"),
    JIRA("Jira"),
    CONFLUENCE("Confluence");

    private final String value;

    private ApplicationType(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    public static ApplicationType forValue(final String value) {
        return find(forArray(values()), new Predicate<ApplicationType>() {
            public boolean apply(ApplicationType input) {
                return input.value.equals(value);
            }
        });
    }
}
