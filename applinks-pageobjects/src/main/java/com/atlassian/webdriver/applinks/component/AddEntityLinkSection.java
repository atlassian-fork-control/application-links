package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.applinks.util.GetTextFunction;
import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.util.AuiBlanketUtil.waitUntilBlanketNotVisible;
import static com.google.common.collect.Lists.transform;

public class AddEntityLinkSection {

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(cssSelector = "#add-entity-link-wizard .wizard-submit")
    private PageElement submitButton;

    @ElementBy(id = "add-entity-link-entity")
    private PageElement keyField;

    @ElementBy(className = "aui-blanket")
    private PageElement auiBlanket;

    public AddEntityLinkSection submit() {
        waitUntilTrue(submitButton.timed().isVisible());
        submitButton.click();
        waitUntilBlanketNotVisible(elementFinder.find(By.className("aui-blanket")));
        return this;
    }

    public boolean hasOneUnlinkedCharlieText() {
        PageElement availableEntitiesMessage = elementFinder.find(By.id("available-entities-msg"));
        Poller.waitUntil(availableEntitiesMessage.timed().getText(),
                Matchers.containsString("There is one un-linked 'Charlie' available for this user."));
        return true;
    }

    public List<String> getRemoteEntityLinkOptions() {
        waitUntilTrue(keyField.timed().isVisible());
        keyField.type(Keys.ARROW_DOWN);
        // wait for first element remote-entity
        waitUntilTrue(elementFinder.find(By.className("remote-entity"), TimeoutType.DIALOG_LOAD).timed().isVisible());
        return transform(elementFinder.findAll(By.className("remote-entity")), new GetTextFunction());
    }
}