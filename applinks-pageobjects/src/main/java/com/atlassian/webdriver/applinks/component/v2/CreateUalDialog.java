package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.aui.auipageobjects.AuiCheckbox;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import org.openqa.selenium.By;

/**
 * Represents the dialog to create an Atlassian to Atlassian applink.
 *
 * @since v4.0.0
 */
public class CreateUalDialog extends AbstractCreationDialog {
    @ElementBy(id = "applinks-dialog")
    PageElement dialog;

    @ElementBy(id = "isAdmin")
    AuiCheckbox isAdminCheckBox;

    @ElementBy(id = "userbase")
    AuiCheckbox sharedUserbaseCheckBox;

    @ElementBy(className = "appDetail")
    PageElement appDetail;

    public Boolean isPresent() {
        try {
            Poller.waitUntilTrue(dialog.timed().isVisible());
            return true;
        } catch (AssertionError e) {
            return false;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    public AppDetail getFromAppDetail() {
        if (isPresent()) {
            return new AppDetail(elementFinder.findAll(By.className("appDetail")).get(0));
        }
        return null;
    }

    public AppDetail getToAppDetail() {
        if (isPresent()) {
            return (AppDetail) elementFinder.findAll(By.className("appDetail")).get(1);
        }
        return null;
    }

    public PauseDialog clickContinueToCreateUalLink() {
        Poller.waitUntilTrue(continueButton.timed().isVisible());

        final Class nextDialogClass;

        if ((isAdminCheckBox.isPresent() && isAdminCheckBox.isSelected())
                || !isAdminCheckBox.isPresent()) {
            nextDialogClass = RedirectingReciprocalLinkDialog.class;
        } else {
            nextDialogClass = ManualReciprocalLinkDialog.class;
        }

        continueButton.click();
        Poller.waitUntilFalse(continueButton.timed().isVisible());
        return (PauseDialog) pageBinder.bind(nextDialogClass);
    }

    public CreateUalDialog unCheckAdmin() {
        Poller.waitUntilTrue(isAdminCheckBox.timed().isPresent());
        Poller.waitUntilTrue(isAdminCheckBox.timed().isVisible());
        if (isAdminCheckBox.isSelected()) {
            isAdminCheckBox.click();
        }
        return this;
    }

    public CreateUalDialog checkAdmin() {
        Poller.waitUntilTrue(isAdminCheckBox.timed().isPresent());
        Poller.waitUntilTrue(isAdminCheckBox.timed().isVisible());
        if (!isAdminCheckBox.isSelected()) {
            isAdminCheckBox.click();
        }

        return this;
    }

    public boolean adminIsChecked() {
        Poller.waitUntilTrue(isAdminCheckBox.timed().isPresent());
        Poller.waitUntilTrue(isAdminCheckBox.timed().isVisible());
        return isAdminCheckBox.isSelected();
    }

    public CreateUalDialog unCheckSharedUserbase() {
        Poller.waitUntilTrue(sharedUserbaseCheckBox.timed().isVisible());
        if (sharedUserbaseCheckBox.isSelected()) {
            sharedUserbaseCheckBox.click();
        }
        return this;
    }

    public CreateUalDialog checkSharedUserbase() {
        Poller.waitUntilTrue(sharedUserbaseCheckBox.timed().isVisible());
        if (!sharedUserbaseCheckBox.isSelected()) {
            sharedUserbaseCheckBox.click();
        }

        return this;
    }

    public boolean sharedUserBaseIsChecked() {
        Poller.waitUntilTrue(sharedUserbaseCheckBox.timed().isVisible());
        return sharedUserbaseCheckBox.isSelected();
    }

    public boolean sharedUserBaseIsPresent() {
        // wait until the admin checkbox is visible, then check for the shared userbase one.
        Poller.waitUntilTrue(isAdminCheckBox.timed().isVisible());
        return sharedUserbaseCheckBox.isPresent();
    }
}
