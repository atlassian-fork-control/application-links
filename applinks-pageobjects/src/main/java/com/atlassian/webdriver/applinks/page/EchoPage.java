package com.atlassian.webdriver.applinks.page;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import javax.annotation.Nonnull;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.Objects.requireNonNull;

/**
 * Represents the "echo" page. Use it to quickly land the browser in the URL and domain of a specific product for
 * operations that require it, e.g. to retrieve/add cookies. See {@code EchoPageServlet}.
 *
 * @since 4.3
 */
public class EchoPage extends ApplinkAbstractPage {
    @ElementBy(id = "applinks-echo")
    protected PageElement idElement;

    private final boolean waitForLoad;

    public EchoPage() {
        this(true);
    }

    public EchoPage(boolean waitForLoad) {
        this.waitForLoad = waitForLoad;
    }

    public static EchoPage goToIfNeeded(@Nonnull TestedProduct<WebDriverTester> product) {
        requireNonNull(product, "product");
        EchoPage page = product.getPageBinder().bind(EchoPage.class, false);
        if (isAt(page, product)) {
            return page;
        } else {
            return product.visit(EchoPage.class);
        }
    }

    @Override
    public String getUrl() {
        return "/plugins/servlet/applinks-tests/echo";
    }

    @WaitUntil
    public void waitUntilLoaded() {
        if (waitForLoad) {
            waitUntilTrue(isAt());
        }
    }

    public TimedCondition isAt() {
        return idElement.timed().isPresent();
    }

    private static boolean isAt(EchoPage page, TestedProduct<WebDriverTester> product) {
        return product.getTester().getDriver().getCurrentUrl().startsWith(product.getProductInstance().getBaseUrl())
                && page.isAt().now();
    }
}
