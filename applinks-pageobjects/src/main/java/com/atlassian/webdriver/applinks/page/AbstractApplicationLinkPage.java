package com.atlassian.webdriver.applinks.page;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.component.DeleteApplicationLinkDialog;
import com.atlassian.webdriver.applinks.component.RelocateApplicationLinkDialog;
import com.atlassian.webdriver.applinks.grid.GridFinder;
import com.atlassian.webdriver.applinks.grid.GridFinderFactory;
import com.atlassian.webdriver.applinks.grid.GridFinderRow;
import com.atlassian.webdriver.applinks.grid.GridFinderRowCreator;
import org.hamcrest.Matchers;
import org.openqa.selenium.By;

import java.util.List;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.SLOW_PAGE_LOAD;

/**
 * Abstract representation of an Application Links admin page.
 * Represents aspects that are common gto v4 and v3 pages.
 *
 * @since v4.0.0
 */
public abstract class AbstractApplicationLinkPage extends ApplinkAbstractPage {
    public static final String URL = "/plugins/servlet/applinks/listApplicationLinks";

    @Inject
    protected GridFinderFactory gridFactory;

    @ElementBy(id = "applicationsList", timeoutType = TimeoutType.PAGE_LOAD)
    protected PageElement applicationsListTable;

    @ElementBy(id = "edit-application-link-dialog")
    protected PageElement editApplicationLinkDialog;

    @ElementBy(cssSelector = ".applinks-message-bar")
    protected PageElement messageBar;

    @ElementBy(cssSelector = ".applinks-message-bar .aui-message")
    protected PageElement messageBarFirstMessage;

    @ElementBy(className = "page-info")
    protected PageElement pageInfoDiv;

    @ElementBy(className = "page-warning")
    protected PageElement pageWarningDiv;

    @ElementBy(className = "links-loading-message")
    protected PageElement linksLoadingMessage;

    @ElementBy(id = "applinks-create-button")
    protected PageElement createApplinkButton;

    public String getUrl() {
        return URL;
    }

    // Page
    public boolean isCurrentPage() {
        return driver.getCurrentUrl().endsWith(getUrl());
    }

    @WaitUntil
    protected void waitUntilLoaded() {
        Poller.waitUntilTrue(applicationsListTable.timed().isPresent());
        Poller.waitUntilTrue(createApplinkButton.timed().isPresent());
    }

    // Table
    public TimedCondition isApplinksListFullyLoaded() {
        return applicationsListTable.withTimeout(SLOW_PAGE_LOAD).timed().hasClass("fully-loaded");
    }

    // Editing
    public ApplicationDetailsSection configureApplicationLink(String linkUrl) {
        waitUntilTrue(isApplinksListFullyLoaded());
        PageElement row = elementFinder.find(By.id("ual-row-" + linkUrl));
        if (!row.isPresent()) {
            throw new IllegalStateException("cannot configure app that doesn't exist:[" + linkUrl + "]");
        }

        row.find(By.className("app-edit-link")).click();
        waitUntilTrue(editApplicationLinkDialog.timed().isVisible());
        return pageBinder.bind(ApplicationDetailsSection.class);
    }

    // Messaging
    public TimedElement getMessageBarFirstMessageTimed() {
        return this.messageBarFirstMessage.timed();
    }

    public String getPageWarning() {
        waitUntilTrue(pageWarningDiv.timed().isVisible());
        return pageWarningDiv.getText();
    }

    // relocation
    public RelocateApplicationLinkDialog relocate(String name) {
        getRelocateLink(name).click();
        return pageBinder.bind(RelocateApplicationLinkDialog.class);
    }

    public String getRelocateLinkText(String name) {
        return getRelocateLink(name).getText();
    }

    private PageElement getRelocateLink(String name) {
        Poller.waitUntilTrue(getRelocateLinksGridUtil().hasRow(0, Matchers.containsString("'" + name + "'")));
        return getRelocateLinksGridUtil().inRow(0, Matchers.containsString("'" + name + "'")).find(By.className("relocate-warning"));
    }

    private GridFinder getRelocateLinksGridUtil() {
        return gridFactory.create(messageBar, null, By.className("page-warning"), By.tagName("div"));
    }

    public TimedElement getLinksLoadingMessageTimed() {
        return this.linksLoadingMessage.timed();
    }

    /**
     * Returns a {@link TimedCondition} which can be used to wait until the message bar is shown.
     *
     * @return a wait-ready condition
     */
    public TimedCondition isFirstMessageShown() {
        Poller.waitUntilTrue(this.getMessageBarFirstMessageTimed().isPresent());
        return this.getMessageBarFirstMessageTimed().isVisible();
    }

    public String getFirstMessage() {
        Poller.waitUntilTrue(this.isFirstMessageShown());
        return messageBarFirstMessage.getText();
    }

    public List<ApplicationLinkEntryRow> getApplicationLinks() {
        return inApplicationLinks().allRows();
    }

    public TimedQuery<List<ApplicationLinkEntryRow>> getApplicationLinksTimed() {
        // loading the links table can be as slow as page load
        return inApplicationLinks().allRowsTimed(TimeoutType.PAGE_LOAD);
    }

    public GridFinder<ApplicationLinkEntryRow> inApplicationLinks() {
        return gridFactory.createTable(elementFinder.find(By.id("application-links-table")),
                new GridFinderRowCreator<ApplicationLinkEntryRow>() {
                    public ApplicationLinkEntryRow create(GridFinder<ApplicationLinkEntryRow> gridFinder, PageElement pageElement) {
                        return new ApplicationLinkEntryRow(gridFinder, pageElement);
                    }
                }
        );
    }

    public DeleteApplicationLinkDialog deleteApplicationLink(String url) {
        PageElement linkRow = elementFinder.find(By.id("ual-row-" + url));
        if (!linkRow.isPresent()) {
            throw new IllegalStateException("No Application Link exists for URL: " + url);
        }

        linkRow.find(By.className("app-delete-link")).click();
        return pageBinder.bind(DeleteApplicationLinkDialog.class);
    }

    public static class ApplicationLinkEntryRow extends GridFinderRow {
        public ApplicationLinkEntryRow(GridFinder<ApplicationLinkEntryRow> gridFinder, PageElement pageElement) {
            super(gridFinder, pageElement);
        }

        public String getName() {
            return findCell("name").getText();
        }

        public String getApplicationType() {
            return findCell("type").getText();
        }

        public String getApplicationId() {
            return findAttribute("application-id");
        }

        public String getApplicationUrl() {
            return findCell("applicationUrl").getText();
        }

        public String getDisplayUrl() {
            return findCell("displayUrl").getText();
        }

        public String getIncomingAuthentication() {
            return findCell("incoming-authentication").getText();
        }

        public String getOutgoingAuthentication() {
            return findCell("outgoing-authentication").getText();
        }

        public boolean canMakePrimary() {
            return find(By.className("app-toggleprimary-link")).isVisible();
        }

        public boolean isReadonly() {
            return "true".equals(findAttribute("data-readonly"));
        }

        @Override
        public String toString() {
            return this.getName() + ":" + this.getApplicationUrl();
        }
    }

}
