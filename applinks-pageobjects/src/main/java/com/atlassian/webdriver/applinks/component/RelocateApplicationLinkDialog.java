package com.atlassian.webdriver.applinks.component;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;

public class RelocateApplicationLinkDialog {
    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "relocate-url")
    private PageElement relocateUrlField;

    @ElementBy(className = "applinks-next-button")
    private PageElement nextButton;

    @ElementBy(id = "relocate-error")
    private PageElement relocateErrorElement;

    public RelocateApplicationLinkDialog setRelocateUrl(String relocateUrl) {
        relocateUrlField.clear();
        relocateUrlField.type(relocateUrl);
        return this;
    }

    public RelocateApplicationLinkDialog nextExpectingError() {
        nextButton.click();
        return this;
    }

    public String getRelocateErrorText() {
        Poller.waitUntilTrue(relocateErrorElement.timed().isVisible());
        return relocateErrorElement.getText();
    }
}
