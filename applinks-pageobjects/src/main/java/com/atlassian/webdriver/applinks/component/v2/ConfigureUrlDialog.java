package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;

/**
 * Represents the dialog shown to a user when there is a problem with the entered URL, e.g. redirection, un-contactable etc
 *
 * @since v4.0.0
 */
public class ConfigureUrlDialog extends AbstractDialog {
    @ElementBy(id = "applinks-configure-url-form")
    protected PageElement urlDialogForm;
    @ElementBy(id = "application-url-userdefined")
    protected PageElement userDefinedUrl;
    @ElementBy(id = "application-url-corrected")
    protected PageElement correctedUrl;
    @ElementBy(id = "use-application-url-userdefined")
    protected PageElement useEnteredUrl;

    @ElementBy(className = "urlError")
    protected PageElement warning;

    @Override
    public TimedQuery<Boolean> isAt() {
        return urlDialogForm.timed().isVisible();
    }

    public CreateNonUalDialog clickContinueAndOpenNonUalDialog() {
        this.clickContinue();
        return this.getCreateNonUalDialog();
    }

    public CreateNonUalDialog getCreateNonUalDialog() {
        return pageBinder.bind(CreateNonUalDialog.class);
    }

    public TimedElement getUserDefinedUrlTextBoxTimed() {
        return this.userDefinedUrl.timed();
    }

    public TimedElement getCorrectedUrlTextBoxTimed() {
        return this.correctedUrl.timed();
    }

    public void setCorrectedUrl(String url) {
        this.setFieldValue(this.correctedUrl, url);
    }

    public TimedElement getUseEnteredUrlCheckBoxTimed() {
        return this.useEnteredUrl.timed();
    }

    public String getWarning() {
        Poller.waitUntilTrue(warning.timed().isPresent());
        Poller.waitUntilTrue(warning.timed().isVisible());
        return warning.getText();
    }
}
