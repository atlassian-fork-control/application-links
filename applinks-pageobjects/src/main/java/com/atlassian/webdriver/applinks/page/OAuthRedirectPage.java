package com.atlassian.webdriver.applinks.page;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * Page object for OAuth redirect page.
 *
 * @since v5.0
 */
public class OAuthRedirectPage extends ApplinkAbstractPage {

    @ElementBy(name = "approve")
    PageElement approveButton;

    @ElementBy(id = "os_username")
    PageElement usernameTextBox;

    @ElementBy(id = "os_password")
    PageElement passwordTextBox;

    @ElementBy(id = "os_login")
    PageElement loginButton;

    @ElementBy(tagName = "body")
    PageElement body;

    public String getUrl() {
        return "/plugins/servlet/applinks/applinks-tests/oauth-redirect";
    }

    public OAuthRedirectPage approve(String username, String password) {
        //These are hacks because for some reason the browser asked to authenticate again,
        //When trying to reproduce this, I can't. It seems that I can access this page manually without authenticating.
        //Any ideas why this happens?
        if (approveButton.isPresent() == false) {
            usernameTextBox.type(username);
            passwordTextBox.type(password);
            loginButton.click();
        }

        approveButton.click();

        return pageBinder.bind(OAuthRedirectPage.class);
    }

    public String getOAuthResponse() {
        return body.getText();
    }

}
