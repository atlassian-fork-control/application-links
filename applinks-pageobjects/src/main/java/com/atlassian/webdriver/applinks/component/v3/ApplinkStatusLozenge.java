package com.atlassian.webdriver.applinks.component.v3;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.applinks.externalcomponent.OAuthConfirmPage;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Conditions.isEqual;
import static com.atlassian.pageobjects.elements.query.Conditions.or;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * @since 5.2
 */
public class ApplinkStatusLozenge {
    public static final String CONNECTED = "CONNECTED";
    public static final String ERROR_INCOMPATIBLE = "STATUS UNAVAILABLE";
    public static final String ERROR_NON_ATLASSIAN = "NON-ATLASSIAN";
    public static final String ERROR_SYSTEM = "SYSTEM";
    public static final String ERROR_NETWORK = "NETWORK ERROR";
    public static final String ERROR_ACCESS = "ACCESS DENIED";
    public static final String ERROR_CONFIG = "CONFIG ERROR";
    public static final String ERROR_DEPRECATED = "DEPRECATED";
    public static final String DISABLED = "DISABLED";

    @Inject
    private PageBinder pageBinder;

    private final PageElement statusContainer;

    public ApplinkStatusLozenge(PageElement statusContainer) {
        this.statusContainer = statusContainer;
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public TimedCondition isPresent() {
        // either the lozenge, or the auth link are present
        return or(getStatusLozenge().timed().isPresent(), getRemoteAuthenticationRequiredLink().timed().isPresent());
    }

    @Nonnull
    public TimedQuery<String> getText() {
        return getStatusLozenge().timed().getText();
    }

    @Nonnull
    public TimedCondition isRemoteAuthenticationRequired() {
        return getRemoteAuthenticationRequiredLink().timed().isPresent();
    }

    @Nonnull
    public TimedCondition isWorking() {
        return hasStatus(CONNECTED);
    }

    @Nonnull
    public TimedCondition hasStatus(String statusText) {
        return isEqual(statusText, getText());
    }

    @Nonnull
    public TimedCondition isClickable() {
        return isEqual("button", getStatusLozenge().timed().getTagName());
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public OAuthConfirmPage<V3ListApplicationLinksPage> startRemoteAuthentication() {
        waitUntilTrue(isRemoteAuthenticationRequired());
        getRemoteAuthenticationRequiredLink().click();

        V3ListApplicationLinksPage currentPage = pageBinder.bind(V3ListApplicationLinksPage.class);
        return pageBinder.bind(OAuthConfirmPage.class, currentPage);
    }

    /**
     * Click the status lozenge to trigger the diagnostics dialog to open
     */
    public void toggle() {
        PageElement statusLozenge = getStatusLozenge();
        waitUntilTrue(statusLozenge.timed().isVisible());
        statusLozenge.click();
    }

    protected PageElement getStatusLozenge() {
        return statusContainer.find(By.className("status-lozenge"));
    }

    protected PageElement getRemoteAuthenticationRequiredLink() {
        return statusContainer.find(By.className("status-remote-auth-required"));
    }
}
