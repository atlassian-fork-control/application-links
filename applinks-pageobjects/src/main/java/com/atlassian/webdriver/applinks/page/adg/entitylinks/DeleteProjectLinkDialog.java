package com.atlassian.webdriver.applinks.page.adg.entitylinks;

import com.atlassian.applinks.api.EntityType;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;

import javax.inject.Inject;

import static org.hamcrest.Matchers.is;

public class DeleteProjectLinkDialog extends AbstractAtlaskitModalDialog {

    @Inject
    protected PageBinder pageBinder;

    @ElementBy(name = "twowayselect")
    protected PageElement twoWayDeleteCheckbox;

    @ElementBy(className = "delete-entitylink")
    protected PageElement deleteEntity;

    private final Class<? extends EntityType> entityType;
    private final String key;

    public DeleteProjectLinkDialog(Class<? extends EntityType> entityType, String key) {
        this.entityType = entityType;
        this.key = key;
    }

    public ConfigureEntityLinksPage confirm() {
        deleteEntity.click();
        waitUntilDialogClosed();
        return pageBinder.bind(ConfigureEntityLinksPage.class, entityType, key);
    }

    @WaitUntil
    private void waitUntilDialogIsReady() {
        Poller.waitUntil(twoWayDeleteCheckbox.timed().isPresent(), is(true));
    }
}
