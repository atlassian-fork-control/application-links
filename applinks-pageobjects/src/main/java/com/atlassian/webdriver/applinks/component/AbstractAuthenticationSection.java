package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * Base class for all V1/V2 Edit Dialog sections.
 */
public abstract class AbstractAuthenticationSection {
    @Inject
    protected WebDriver webDriver;
    @Inject
    protected PageBinder pageBinder;
    @Inject
    protected PageElementFinder elementFinder;

    @ElementBy(id = "edit-application-link-dialog")
    private PageElement dialogContainer;

    @Nonnull
    public TimedCondition isOpen() {
        return getDialogContainer().timed().isVisible();
    }

    /**
     * Closes the dialog and immediately returns. It is up to the discretion of the calling test to determine whether
     * to (and how to) wait for the application link list page to load fully before continuing execution.
     * <br>
     * Note: Failure to wait, in some situations, seems to result in a red box appearing on the list page that just
     * says "error" (all lowercase) but produces no server-side stack trace.
     */
    public void close() {
        getCloseLink().click();
    }

    /**
     * Closes the dialog <i>and</i> waits for the application link list page to load fully before returning.
     *
     * @return the fully-loaded list page
     */
    public ListApplicationLinkPage safeClose() {
        close();

        return pageBinder.bind(ListApplicationLinkPage.class);
    }

    @Nonnull
    protected PageElement findInDialog(By by) {
        return getDialogContainer().find(by);
    }

    @Nonnull
    protected PageElement getCloseLink() {
        return findInDialog(By.className("applinks-cancel-link"));
    }

    @Nonnull
    protected PageElement getDialogContainer() {
        topFrame();
        return dialogContainer;
    }

    protected void topFrame() {
        webDriver.switchTo().defaultContent();
    }
}
