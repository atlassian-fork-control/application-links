package com.atlassian.webdriver.applinks.externalcomponent;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.applinks.component.AbstractApplinkChainedComponent;

import javax.inject.Inject;

/**
 * * Page Object for UPM web login
 */
public class WebLoginPage<T> extends AbstractApplinkChainedComponent<T> {
    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(id = "os_username")
    private PageElement usernameField;

    @ElementBy(id = "os_password")
    private PageElement passwordField;

    @ElementBy(id = "os_login")
    private PageElement submitButton;

    public WebLoginPage(T nextPage) {
        super(nextPage);
    }

    public T handleWebLoginIfRequired(String username, String password) {
        if (usernameField.isPresent()) {
            usernameField.type(username);
            passwordField.type(password);
            submitButton.click();
        }

        if (usernameField.isPresent()) {
            throw new IllegalStateException("should already be logged in by now");
        }

        return nextPage;
    }
}
