package com.atlassian.webdriver.applinks.externalpage;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.applinks.externalcomponent.WebSudoPage;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * Page Object for UPM charlie management
 */
public class CharlieManagementPage extends ApplinkAbstractPage {
    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    @ElementBy(name = "key")
    private PageElement keyField;

    @ElementBy(name = "name")
    private PageElement nameField;

    @ElementBy(xpath = "//input[@value='Add']")
    private PageElement addButton;

    public String getUrl() {
        return "/plugins/servlet/charlieadmin";
    }

    public CharlieManagementPage handleWebSudoIfRequired(String webSudoPassword) {
        pageBinder.bind(WebSudoPage.class).handleIfRequired(webSudoPassword);
        return this;
    }

    public CharlieManagementPage add(String key, String name) {
        keyField.type(key);
        nameField.type(name);
        addButton.click();
        return this;
    }

    public PageElement getLandingPageLink(String charlieKey) {
        return elementFinder.find(By.linkText(charlieKey));
    }
}