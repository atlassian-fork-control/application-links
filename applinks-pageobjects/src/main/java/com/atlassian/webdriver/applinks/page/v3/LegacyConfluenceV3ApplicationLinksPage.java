package com.atlassian.webdriver.applinks.page.v3;

/**
 * Represents the V3 application links config page accessed via the legacy, now removed Confluence action.
 *
 * @since 4.3
 */
public class LegacyConfluenceV3ApplicationLinksPage extends V3ListApplicationLinksPage {
    @Override
    public String getUrl() {
        return "/admin/listapplicationlinks.action";
    }
}
