package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.PageElements.BODY;

public class TrustedApplicationAuthenticationSection extends AbstractAuthenticationSection {
    @ElementBy(id = "ipPatternsInput")
    private PageElement ipPatternsInput;
    @ElementBy(name = "atl_token")
    private PageElement tokenField;
    @ElementBy(id = "auth-trusted-action-update")
    private PageElement updateButton;
    @ElementBy(id = "auth-trusted-action-enable")
    private PageElement enableButton;
    @ElementBy(id = "auth-trusted-action-configure")
    private PageElement disableButton;

    private XsrfWarning xsrfWarning;

    public String getIpPatterns() {
        Poller.waitUntilTrue(ipPatternsInput.withTimeout(TimeoutType.PAGE_LOAD).timed().isVisible());

        return ipPatternsInput.getValue();
    }

    /**
     * Retrieves a PageObject for the XSRF token warning.
     * <br>
     * Note: This property will only be available if {@link #modifyAtlToken()} was called before {@link #update()}.
     *
     * @return the XSRF warning PageObject
     */
    public XsrfWarning getXsrfWarning() {
        if (xsrfWarning == null) {
            xsrfWarning = pageBinder.bind(XsrfWarning.class);
        }
        return xsrfWarning;
    }

    /**
     * Executes JavaScript against the page, updating the 'atl_token' value for the form. This simulates a malicious
     * user attempting to hack the page.
     *
     * @return this
     */
    public TrustedApplicationAuthenticationSection modifyAtlToken() {
        elementFinder.find(By.tagName(BODY)).javascript()
                .execute("AJS.$('.auth-config .edit input[name=\"atl_token\"]').val('hacker!')");

        return this;
    }

    public TrustedApplicationAuthenticationSection setIpPatterns(String ipPatterns) {
        Poller.waitUntilTrue(ipPatternsInput.withTimeout(TimeoutType.PAGE_LOAD).timed().isVisible());
        ipPatternsInput.type(ipPatterns);

        return this;
    }

    public TrustedApplicationAuthenticationSection update() {
        updateButton.click();

        return this;
    }

    public TrustedApplicationAuthenticationSection enable() {
        Poller.waitUntilTrue(enableButton.timed().isVisible());
        enableButton.click();
        return this;
    }

    public TrustedApplicationAuthenticationSection disable() {
        Poller.waitUntilTrue(disableButton.timed().isVisible());
        disableButton.click();
        return this;
    }
}
