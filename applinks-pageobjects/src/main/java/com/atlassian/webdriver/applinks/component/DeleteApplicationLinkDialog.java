package com.atlassian.webdriver.applinks.component;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.externalcomponent.OAuthConfirmPage;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import javax.inject.Inject;

public class DeleteApplicationLinkDialog {
    @Inject
    private PageElementFinder finder;
    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "delete-application-link-dialog")
    private PageElement dialog;
    @ElementBy(className = "delete-applink-loading")
    private PageElement loading;
    @ElementBy(id = "no-delete-reciprocal-link")
    private PageElement noReciprocalRadio;
    @ElementBy(id = "delete-reciprocal-applink-text")
    private PageElement reciprocalText;

    /**
     * Deletes a one-way link. After the link is deleted, a wait is enforced to ensure the dialog closes.
     * <br>
     * Note: Two-way links have a secondary confirmation step, so attempting to delete a two-way link with this
     * method will result in an exception.
     */
    public void deleteOneWayLink() {
        clickConfirm();

        Poller.waitUntilFalse(dialog.timed().isVisible());
    }

    /**
     * Deletes a two-way link on both sides. This matches the defaults in the UI, where, for two-way links, the
     * radio button for deleting both sides is checked by default.
     *
     * @see #deleteTwoWayLink(boolean)
     */
    public void deleteTwoWayLink() {
        deleteTwoWayLink(true);
    }

    /**
     * Deletes a two-way link, optionally deleting it only from one side. After the link is deleted, a wait is
     * enforced to ensure the dialog closes.
     *
     * @param bothSides {@code true} to delete both sides of the link (default in the UI); {@code false} to
     *                  delete the link only on the local side
     */
    public void deleteTwoWayLink(boolean bothSides) {
        deleteTwoWayLink(bothSides, null);
    }

    /**
     * Deletes a two-way link, optionally deleting it only from one side. After the link is deleted, a wait is
     * enforced to ensure the dialog closes.
     *
     * @param bothSides     {@code true} to delete both sides of the link (default in the UI); {@code false} to
     *                      delete the link only on the local side
     * @param authToConfirm must be specified if the link requires remote authentication to delete on both sides (e.g.
     *                      for OAuth 3LO without previous authentication)
     */
    public void deleteTwoWayLink(boolean bothSides, @Nullable TestAuthentication authToConfirm) {
        //Click the initial confirm button to start the process
        clickConfirm();

        if (authToConfirm != null) {
            dialog.find(By.id("authenticate-link")).click();
            pageBinder.bind(OAuthConfirmPage.class, Void.class).confirmHandlingWebLoginIfRequired(authToConfirm);
        }

        //Wait for the initial confirmation screen to change to the confirmation screen for deleting both sides of
        //the link, or just one
        Poller.waitUntilTrue(reciprocalText.timed().isVisible());
        if (!bothSides) {
            noReciprocalRadio.select();
        }

        //Once the radio button is correct (deleting both sides is the default, so if we're doing that there's no
        //need to click a radio button), the steps to finish up are the same as deleting a one-way link.
        deleteOneWayLink();
    }

    private void clickConfirm() {
        //Note: The confirm button is not cached as an @ElementBy PageElement because in the deleteTwoWayLink() case,
        //      the reference becomes stale and causes an error.
        FindUtils.findVisibleBy(By.cssSelector("#delete-application-link-dialog .wizard-submit"), finder).click();
    }

    /**
     * Enforces that the deletion dialog becomes visible and the loading indicator is no longer visible.
     */
    @WaitUntil
    private void waitFor() {
        Poller.waitUntilTrue(dialog.timed().isVisible());
        Poller.waitUntilFalse(loading.timed().isVisible());
    }
}
