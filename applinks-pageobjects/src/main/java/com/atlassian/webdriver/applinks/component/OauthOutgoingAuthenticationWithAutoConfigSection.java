package com.atlassian.webdriver.applinks.component;

import com.atlassian.aui.auipageobjects.AuiCheckbox;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class OauthOutgoingAuthenticationWithAutoConfigSection extends ConfigureApplicationSection {
    @ElementBy(id = "auth-oauth-action-disable")
    private PageElement disableButton;

    @ElementBy(id = "auth-oauth-action-enable")
    private PageElement enableButton;

    @ElementBy(id = "outgoing-2lo-section")
    private PageElement outgoing2LOSection;

    @ElementBy(id = "suggest-enabling-two-lo")
    private PageElement suggestEnablingTwoLODiv;

    @ElementBy(id = "suggest-disabling-two-lo")
    private PageElement suggestDisablingTwoLODiv;

    /**
     * @since 4.0.15
     */
    @ElementBy(id = "suggest-disabling-two-loi")
    private PageElement suggestDisablingTwoLOiDiv;

    @ElementBy(id = "outgoing-two-lo-enabled")
    private AuiCheckbox outgoingTwoLoEnableCheckBox;

    /**
     * @since 4.0.15
     */
    @ElementBy(id = "outgoing-two-loi-enabled")
    private AuiCheckbox outgoingTwoLoiEnableCheckBox;

    @ElementBy(id = "outgoing-two-lo-action-update")
    private PageElement outgoingTwoLOUpdateButton;

    @ElementBy(id = "outgoing-two-lo-update-success")
    private PageElement outgoingTwoLOUpdateSuccessDiv;

    @ElementBy(id = "outgoing-2lo-footer")
    private PageElement outgoingTwoLOFooterDiv;

    @ElementBy(cssSelector = ".status-configured")
    private PageElement configuredStatus;

    @ElementBy(cssSelector = ".status-not-configured")
    private PageElement notConfiguredStatus;

    // un-prefixed "error" matches AUI 5.x, prefixed matches AUI 6.x+
    @ElementBy(cssSelector = ".aui-message.error, .aui-message.aui-message-error")
    private PageElement errorMessage;

    public OauthIncomingAuthenticationWithAutoConfigSection disable() {
        disableButton.click();

        return pageBinder.bind(OauthIncomingAuthenticationWithAutoConfigSection.class);
    }

    public OauthIncomingAuthenticationWithAutoConfigSection enable() {
        enableButton.click();

        return pageBinder.bind(OauthIncomingAuthenticationWithAutoConfigSection.class);
    }

    public boolean enableButtonIsVisible() {
        Poller.waitUntilTrue(enableButton.timed()
                .isPresent());
        return enableButton.isVisible();
    }

    public boolean disableButtonIsVisible() {
        Poller.waitUntilTrue(disableButton.timed()
                .isPresent());
        return disableButton.isVisible();
    }

    public boolean isConfigured() {
        return "Configured".equals(configuredStatus.getText());
    }

    public boolean isNotConfigured() {
        return "Not Configured".equals(notConfiguredStatus.getText());
    }

    public boolean isOutgoing2LOEnabled() {
        return outgoingTwoLoEnableCheckBox.isSelected();
    }

    /**
     * @since 4.0.15
     */
    public boolean isOutgoing2LOiEnabled() {
        return outgoingTwoLoiEnableCheckBox.isSelected();
    }

    public TimedCondition isOutgoing2LOEnabledTimed() {
        return outgoingTwoLoEnableCheckBox.timed()
                .isSelected();
    }

    /**
     * @since 4.0.15
     */
    public TimedCondition isOutgoing2LOiEnabledTimed() {
        return outgoingTwoLoiEnableCheckBox.timed()
                .isSelected();
    }

    public OauthOutgoingAuthenticationWithAutoConfigSection checkOutgoing2LO() {
        outgoingTwoLoEnableCheckBox.check();
        return this;
    }

    /**
     * @since 4.0.15
     */
    public OauthOutgoingAuthenticationWithAutoConfigSection checkOutgoing2LOi() {
        outgoingTwoLoiEnableCheckBox.check();
        return this;
    }

    public OauthOutgoingAuthenticationWithAutoConfigSection uncheckOutgoing2LO() {
        outgoingTwoLoEnableCheckBox.uncheck();
        return this;
    }

    /**
     * @since 4.0.15
     */
    public OauthOutgoingAuthenticationWithAutoConfigSection uncheckOutgoing2LOi() {
        outgoingTwoLoiEnableCheckBox.uncheck();
        return this;
    }

    public OauthOutgoingAuthenticationWithAutoConfigSection clickUpdate2LOConfig() {
        outgoingTwoLOUpdateButton.click();
        return pageBinder.bind(OauthOutgoingAuthenticationWithAutoConfigSection.class);
    }

    public boolean isDisablingOutgoing2LOSuggestionMessagePresent() {
        Poller.waitUntilTrue(outgoingTwoLOFooterDiv.timed()
                .isPresent());
        return suggestDisablingTwoLODiv.isPresent() && suggestDisablingTwoLODiv.isVisible();
    }

    /**
     * @since 4.0.15
     */
    public boolean isDisablingOutgoing2LOiSuggestionMessagePresent() {
        Poller.waitUntilTrue(outgoingTwoLOFooterDiv.timed()
                .isPresent());
        return suggestDisablingTwoLOiDiv.isPresent() && suggestDisablingTwoLOiDiv.isVisible();
    }

    public boolean isEnablingOutgoing2LOSuggestionMessagePresent() {
        Poller.waitUntilTrue(outgoingTwoLOFooterDiv.timed()
                .isPresent());
        return suggestEnablingTwoLODiv.isPresent() && suggestEnablingTwoLODiv.isVisible();
    }

    public boolean isOutgoing2LOUpdateMessagePresent() {
        Poller.waitUntilTrue(outgoingTwoLOFooterDiv.timed()
                .isPresent());
        return outgoingTwoLOUpdateSuccessDiv.isPresent() && outgoingTwoLOUpdateSuccessDiv.isVisible();
    }

    public boolean isOutgoingErrorMessagePresent() {
        Poller.waitUntilTrue(errorMessage.timed().isPresent());
        return errorMessage.isPresent() && errorMessage.isVisible();
    }

    /**
     * @since 4.0.15
     */
    public boolean outgoing2LOiCheckboxIsVisible() {
        Poller.waitUntilTrue(outgoingTwoLoEnableCheckBox.timed()
                .isPresent());
        return outgoingTwoLoEnableCheckBox.isVisible();
    }
}
