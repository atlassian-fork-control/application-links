package com.atlassian.webdriver.applinks.component.v1;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;

public class AddApplicationLinkDialogStep3 extends AbstractAddApplicationLinkDialogStep {
    @ElementBy(cssSelector = "#add-application-link-dialog #sameUser")
    private PageElement sameUserRadioButton;

    @ElementBy(cssSelector = "#add-application-link-dialog #differentUser")
    private PageElement differentUserRadioButton;

    @ElementBy(cssSelector = "#add-application-link-dialog #trusted")
    private PageElement trustedRadioButton;

    @ElementBy(cssSelector = "#add-application-link-dialog #notTrusted")
    private PageElement notTrustedRadioButton;

    @ElementBy(cssSelector = "#add-application-link-dialog .wizard-submit")
    private PageElement createButton;

    @ElementBy(id = "applicationsList")
    private PageElement applicationsListTable;

    /**
     * Clicks the Submit button on the wizard step without updating any fields on the form beforehand.
     *
     * @return a page containing the current list of application links
     */
    public ListApplicationLinkPage acceptDefaults() {
        waitUntilPageIsFullyLoaded();

        return clickCreate();
    }

    public ListApplicationLinkPage selectSameUserBaseAndTrustedLink() {
        waitUntilPageIsFullyLoaded();

        sameUserRadioButton.click();
        trustedRadioButton.click();

        return clickCreate();
    }

    public ListApplicationLinkPage selectDifferentUserBaseAndTrustedLink() {
        waitUntilPageIsFullyLoaded();

        differentUserRadioButton.click();
        trustedRadioButton.click();

        return clickCreate();
    }

    public ListApplicationLinkPage selectSameUserBaseAndNoTrustedLink() {
        waitUntilPageIsFullyLoaded();

        sameUserRadioButton.click();
        notTrustedRadioButton.click();

        return clickCreate();
    }

    public ListApplicationLinkPage selectDifferentUserBaseAndNoTrustedLink() {
        waitUntilPageIsFullyLoaded();

        differentUserRadioButton.click();
        notTrustedRadioButton.click();

        return clickCreate();
    }

    private void waitUntilPageIsFullyLoaded() {
        // this button appears very last in the page.
        Poller.waitUntilTrue(notTrustedRadioButton.withTimeout(TimeoutType.PAGE_LOAD).timed().isVisible());
    }
}