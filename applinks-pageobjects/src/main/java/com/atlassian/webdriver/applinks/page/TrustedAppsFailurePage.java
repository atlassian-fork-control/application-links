package com.atlassian.webdriver.applinks.page;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * Page representing a known failure point for a request using TA.
 *
 * @since 3.11.0
 */
public class TrustedAppsFailurePage extends ApplinkAbstractPage {
    @ElementBy(tagName = "body")
    PageElement body;

    public String getUrl() {
        return "/plugins/servlet/applinks/tests/trusted-fail/get";
    }

    public String getTrustedResponse() {
        return body.getText();
    }
}