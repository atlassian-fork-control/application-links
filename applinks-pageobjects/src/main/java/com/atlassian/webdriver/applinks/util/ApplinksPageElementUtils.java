package com.atlassian.webdriver.applinks.util;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Every WebDriver test module needs one of those!
 *
 * @since 5.0
 */
public final class ApplinksPageElementUtils {
    private ApplinksPageElementUtils() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    /**
     * Make element {@link PageElement#isVisible() visible} to {@code WebDriver} if it isn't already. This is sometimes
     * necessary as {@code WebDriver} (in some environments) gets confused about visibility of HTML elements manipulated
     * by {@code skate}.
     *
     * @param element element to make visible
     * @return the element, now visible
     * @throws AssertionError if the element could not be made {@link PageElement#isVisible() visible}
     */
    @Nonnull
    public static PageElement makeVisible(@Nonnull PageElement element) {
        requireNonNull(element, "element");
        if (element.isVisible()) {
            return element;
        } else {
            element.javascript().execute("jQuery(arguments[0]).css('visibility', 'visible')");
            Poller.waitUntilTrue("Failed to make element visible", element.timed().isVisible());
            return element;
        }
    }

    @Nonnull
    public static PageElement jsClick(@Nonnull PageElement element) {
        requireNonNull(element, "element").javascript().execute("jQuery(arguments[0]).click();");
        return element;
    }
}

