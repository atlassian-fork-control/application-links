package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;

/**
 * Represents the generic information dialog used in the v2 workflow.
 *
 * @since v4.0.0
 */
public class InformationDialog extends AbstractDialog {
    @ElementBy(id = "applinks-information-form")
    private PageElement form;

    @ElementBy(id = "applinks-warning-message")
    PageElement warningMessage;

    @ElementBy(id = "applinks-information-message")
    PageElement informationMessage;

    @Override
    public TimedQuery<Boolean> isAt() {
        return form.withTimeout(TimeoutType.SLOW_PAGE_LOAD).timed().isVisible();
    }

    public String getInformation() {
        Poller.waitUntilTrue(informationMessage.timed().isVisible());

        return informationMessage.getText();
    }

    public String getWarning() {
        Poller.waitUntilTrue(warningMessage.timed().isVisible());

        return warningMessage.getText();
    }
}
