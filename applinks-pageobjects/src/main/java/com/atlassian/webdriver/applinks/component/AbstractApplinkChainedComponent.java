package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.AtlassianWebDriver;

import javax.inject.Inject;

/**
 * Abstract superclass for components (eg popups) that should can return the page they were called from on completion
 * (eg clicking a submit button)
 *
 * @param <T> Type of page object returned on completion
 */
public abstract class AbstractApplinkChainedComponent<T> {
    protected final T nextPage;

    @Inject
    protected AtlassianWebDriver driver;

    @Inject
    PageElementFinder elementFinder;

    @Inject
    protected PageBinder pageBinder;

    public AbstractApplinkChainedComponent(T nextPage) {
        this.nextPage = nextPage;
    }
}