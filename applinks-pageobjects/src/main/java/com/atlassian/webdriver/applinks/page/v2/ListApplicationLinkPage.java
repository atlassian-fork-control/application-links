package com.atlassian.webdriver.applinks.page.v2;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.applinks.component.v2.ConfigureUrlDialog;
import com.atlassian.webdriver.applinks.component.v2.ConfirmUrlDialog;
import com.atlassian.webdriver.applinks.component.v2.CreateNonUalDialog;
import com.atlassian.webdriver.applinks.component.v2.CreateUalDialog;
import com.atlassian.webdriver.applinks.component.v2.IncompatibleApplinksVersionDialog;
import com.atlassian.webdriver.applinks.component.v2.InformationDialog;
import com.atlassian.webdriver.applinks.page.AbstractApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import com.atlassian.webdriver.utils.by.ByDataAttribute;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Extension to ListApplicationLinkPage supporting the V2 workflow.
 *
 * @since v4.0
 */
public class ListApplicationLinkPage extends AbstractApplicationLinkPage {
    @Inject
    protected WebDriver driver;

    @ElementBy(id = "applinks-create-button")
    protected PageElement createNewLinkButton;

    @ElementBy(id = "applinks-url-entered")
    protected PageElement enteredApplicationUrlTextBox;

    @ElementBy(tagName = "aui-spinner")
    protected PageElement spinner;

    @ElementBy(className = "no-links")
    protected PageElement addFirstApplicationLink;

    @ElementBy(className = "applinks-lab-message")
    protected PageElement v3Switch;

    @WaitUntil
    protected void waitUntilLoaded() {
        Poller.waitUntilTrue(isApplinksListFullyLoaded());
    }

    public TimedCondition hasV3UiSwitch() {
        return v3Switch.timed().isPresent();
    }

    public CreateUalDialog getCreateUalDialog() {
        return pageBinder.bind(CreateUalDialog.class);
    }

    public InformationDialog getInformationDialog() {
        return pageBinder.bind(InformationDialog.class);
    }

    public ConfigureUrlDialog getConfigureUrlDialog() {
        return pageBinder.bind(ConfigureUrlDialog.class);
    }

    public IncompatibleApplinksVersionDialog getIncompatibleApplinksVersionDialog() {
        return pageBinder.bind(IncompatibleApplinksVersionDialog.class);
    }

    /**
     * Switch to the V3 UI.
     */
    public V3ListApplicationLinksPage switchToV3Ui() {
        Poller.waitUntilTrue(v3Switch.timed().isPresent());
        v3Switch.click();
        return pageBinder.bind(V3ListApplicationLinksPage.class);
    }

    public void clickCreateNewLink() {
        Poller.waitUntilTrue(Conditions.and(createNewLinkButton.timed().isPresent(), createNewLinkButton.timed().isEnabled()));
        createNewLinkButton.click();
    }

    public CreateUalDialog clickCreateNewLinkAndOpenDialog() {
        this.clickCreateNewLink();
        return pageBinder.bind(CreateUalDialog.class);
    }

    public ConfirmUrlDialog clickCreateNewLinkAndOpenConfirmUrlDialog() {
        this.clickCreateNewLink();
        return pageBinder.bind(ConfirmUrlDialog.class);
    }

    public CreateNonUalDialog clickCreateNewLinkAndOpenNonUalDialog() {
        this.clickCreateNewLink();
        return pageBinder.bind(CreateNonUalDialog.class);
    }

    public InformationDialog clickCreateNewLinkAndOpenErrorDialog() {
        this.clickCreateNewLink();
        return pageBinder.bind(InformationDialog.class);
    }

    public ConfigureUrlDialog clickCreateNewLinkAndOpenConfigureUrlDialog() {
        this.clickCreateNewLink();
        return pageBinder.bind(ConfigureUrlDialog.class);
    }

    public String getApplicationUrl() {
        Poller.waitUntilTrue(enteredApplicationUrlTextBox.timed().isVisible());

        return enteredApplicationUrlTextBox.getValue();
    }

    public ListApplicationLinkPage setApplicationUrl(String applicationUrl) {
        Poller.waitUntilTrue(enteredApplicationUrlTextBox.timed().isVisible());
        String currentUrl = this.getApplicationUrl();
        enteredApplicationUrlTextBox.type(applicationUrl);
        return this;
    }

    public ListApplicationLinkPage clearApplicationUrl() {
        Poller.waitUntilTrue(enteredApplicationUrlTextBox.timed().isVisible());
        String currentUrl = this.getApplicationUrl();
        // backspace so that the jquery events fire correctly.
        for (int i = 0; i < currentUrl.length(); i++) {
            driver.findElement(By.id("applinks-url-entered")).sendKeys(Keys.BACK_SPACE);
        }
        return this;
    }

    public InformationDialog loadUrlReciprocalManifestUnobtainable(String url) {
        driver.navigate().to(url);
        return pageBinder.bind(InformationDialog.class);
    }

    public TimedCondition createNewLinkButtonIsEnabledTimed() {
        return createNewLinkButton.timed().isEnabled();
    }

    public TimedElement getApplicationUrlTextBoxTimed() {
        return this.enteredApplicationUrlTextBox.timed();
    }

    public TimedElement getCreateNewLinkButtonTimed() {
        return this.createNewLinkButton.timed();
    }

    /**
     * Checks if create application link button contains spinner
     *
     * @return true if create application link button contains spinner, false otherwise.
     */
    public Boolean spinnerIsPresentInCreateApplinkButton() {
        return this.isElementPresent(By.cssSelector("#applinks-create-button aui-spinner"));
    }

    public boolean isElementPresent(By locatorKey) {
        try {
            return !driver.findElements(locatorKey).isEmpty();
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    public TimedElement getSpinnerTimed() {
        return this.spinner.timed();
    }

    public String getAddFirstApplicationLinkText() {
        return addFirstApplicationLink.getText();
    }

    public String getPageInfo() {
        waitUntilTrue(isApplinksListFullyLoaded());

        waitUntilTrue(pageInfoDiv.timed().isVisible());
        return pageInfoDiv.getText();
    }

    public void clickHelpLink(String helpLinkKey) {
        elementFinder.find(ByDataAttribute.byData("help-link-key", helpLinkKey)).click();
    }
}
