package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

public class OauthOutgoingAuthenticationViewModeSection extends ConfigureApplicationSection {
    @ElementBy(id = "name")
    private PageElement serviceProviderNameText;

    @ElementBy(id = "consumerKey")
    private PageElement consumerKeyText;

    @ElementBy(id = "sharedSecret")
    private PageElement sharedSecretText;

    @ElementBy(id = "description")
    private PageElement descriptionText;

    @ElementBy(id = "requestTokenUrl")
    private PageElement requestTokenUrlText;

    @ElementBy(id = "accessTokenUrl")
    private PageElement accessTokenUrlText;

    @ElementBy(id = "authorizeUrl")
    private PageElement authorizeUrlText;

    @ElementBy(id = "delete")
    PageElement deleteButton;

    public String getServiceProviderName() {
        return serviceProviderNameText.getText();
    }

    public String getConsumerKey() {
        return consumerKeyText.getText();
    }

    public String getDescription() {
        return descriptionText.getText();
    }

    public String getRequestTokenUrl() {
        return requestTokenUrlText.getText();
    }

    public String getAccessTokenUrl() {
        return accessTokenUrlText.getText();
    }

    public String getAuthorizeUrl() {
        return authorizeUrlText.getText();
    }

    public OauthOutgoingAuthenticationEditModeSection delete() {
        Poller.waitUntilTrue(deleteButton.timed().isVisible());
        deleteButton.click();
        return pageBinder.bind(OauthOutgoingAuthenticationEditModeSection.class);
    }
}
