package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;

/**
 * Represents the dialog to configure an incoming generic link.
 *
 * @since v4.0.0
 */
public class CreateNonUalIncomingDialog extends AbstractCreationDialog {
    @ElementBy(id = "consumerKey")
    PageElement consumerKeyTextBox;

    @ElementBy(id = "consumerName")
    PageElement consumerNameTextBox;

    @ElementBy(id = "publicKey")
    PageElement publicKeyTextBox;

    public TimedQuery<Boolean> isAt() {
        return publicKeyTextBox.timed().isPresent();
    }

    public CreateNonUalIncomingDialog setConsumerKey(String value) {
        setFieldValue(consumerKeyTextBox, value);
        return this;
    }

    public CreateNonUalIncomingDialog setConsumerName(String value) {
        setFieldValue(consumerNameTextBox, value);
        return this;
    }

    public CreateNonUalIncomingDialog setPublicKey(String value) {
        setFieldValue(publicKeyTextBox, value);
        return this;
    }

    public ListApplicationLinkPage clickContinueBackToAdminPage() throws Exception {
        Poller.waitUntilTrue(continueButton.timed().isVisible());

        continueButton.click();
        Poller.waitUntilFalse(continueButton.timed().isVisible());
        return pageBinder.bind(ListApplicationLinkPage.class);
    }
}
