package com.atlassian.webdriver.applinks.component.v1;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.applinks.component.FindUtils;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;

import org.openqa.selenium.By;

public abstract class AbstractAddApplicationLinkDialogStep {
    @Inject
    AtlassianWebDriver driver;

    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder elementFinder;

    /**
     * Clicks the create button in the wizard. This method expects the button to be visible at calling time.
     */
    protected ListApplicationLinkPage clickCreate() {
        PageElement createButton = FindUtils.findVisibleBy(By.cssSelector("#add-application-link-dialog .wizard-submit"), elementFinder);

        // this will dirty the application list table.
        driver.executeScript("jQuery(\"#applicationsList\").removeClass(\"fully-loaded\")");

        createButton.click();
        Poller.waitUntilFalse(createButton.timed().isVisible());

        //After creating a new link, in addition to the standard waits in the ListApplicationLinkPage, wait for the
        //message frame to be shown to indicate that the link has been created.
        ListApplicationLinkPage listAppLinkPage = pageBinder.bind(ListApplicationLinkPage.class);
        Poller.waitUntilTrue(listAppLinkPage.isFirstMessageShown());

        return listAppLinkPage;
    }
}
